#include "global.h"
#include "GhostArrowRow.h"
#include "RageUtil.h"
#include "PrefsManager.h"
#include "ArrowEffects.h"
#include "NoteSkinManager.h"
#include "GameState.h"
#include "PrefsManager.h"
#include "NoteFieldPositioning.h"
#include "Game.h"

GhostArrowRow::GhostArrowRow()
{
	m_iNumCols = 0;
}
#include "RageLog.h"
void GhostArrowRow::Load( PlayerNumber pn, CString NoteSkin, float fYReverseOffset, bool bUseRoutine )
{
	Unload();

	m_PlayerNumber = pn;
	m_fYReverseOffsetPixels = fYReverseOffset;

	const Style* pStyle = GAMESTATE->GetCurrentStyle();

	m_iNumCols = pStyle->m_iColsPerPlayer;

	// init arrows
	for( int c=0; c<m_iNumCols; c++ )
	{
		NoteFieldMode &mode = g_NoteFieldMode[pn];
		CString Button(mode.GhostButtonNames[c]);
		if( Button == "" )
			Button = GAMESTATE->GetCurrentGame()->ColToButtonName( c );

		CString sRoutineButton(Button);
		if( bUseRoutine && NOTESKIN->GetMetricB(NoteSkin, Button, "EnableRoutineExplosion") )
			sRoutineButton = "Routine " + Button;

		//m_TapGhost.push_back( unique_ptr<GhostArrow>(new GhostArrow()) );
		//m_TapGhost[c]->SetName( "TapGhostArrow" );
		//m_TapGhost[c]->Init( pn );
		//m_TapGhost[c]->Load( NoteSkin, sRoutineButton, "Tap Note" );

		m_GhostDim.push_back( unique_ptr<GhostArrow>(new GhostArrow()) );
		m_GhostDim[c]->SetName( "GhostArrowDim" );
		m_GhostDim[c]->Init( pn );
		m_GhostDim[c]->Load( NoteSkin, sRoutineButton, "Tap Explosion Dim" );

		m_GhostBright.push_back( unique_ptr<GhostArrow>(new GhostArrow()) );
		m_GhostBright[c]->SetName( "GhostArrowBright" );
		m_GhostBright[c]->Init( pn );
		m_GhostBright[c]->Load( NoteSkin, sRoutineButton, "Tap Explosion Bright" );

		FOREACH_HoldType( ht )
		{
			m_HoldGhost[ht].push_back( unique_ptr<HoldGhostArrow>(new HoldGhostArrow()) );
			m_HoldGhost[ht][c]->SetName( HoldTypeToString(ht) + "GhostArrow" );
			m_HoldGhost[ht][c]->Load( NoteSkin, sRoutineButton, HoldTypeToString(ht) + " Explosion", ht );
		}
	}
}

void GhostArrowRow::Unload()
{
	//m_TapGhost.clear();
	m_GhostDim.clear();
	m_GhostBright.clear();
	FOREACH_HoldType( ht )
		m_HoldGhost[ht].clear();

	m_iNumCols = 0;
}


void GhostArrowRow::Update( float fDeltaTime )
{
	const float fZ = ArrowGetZPos( m_PlayerNumber, 0 );
	const float fZoom = ArrowGetZoom( m_PlayerNumber );
	const float fR = ReceptorArrowGetRotation( m_PlayerNumber );

	for( int c=0; c<m_iNumCols; c++ )
	{
		const float fX = ArrowGetXPos( m_PlayerNumber, c, 0 );
		const float fY = ArrowGetYPos( m_PlayerNumber, c, 0, m_fYReverseOffsetPixels );

		//m_TapGhost[c]->Update( fDeltaTime );
		//m_TapGhost[c]->SetX( fX );
		//m_TapGhost[c]->SetY( fY );
		//m_TapGhost[c]->SetZ( fZ );
		//m_TapGhost[c]->SetZoom( fZoom );
		//m_TapGhost[c]->SetRotationZ( fR );

		m_GhostDim[c]->Update( fDeltaTime );
		m_GhostDim[c]->SetX( fX );
		m_GhostDim[c]->SetY( fY );
		m_GhostDim[c]->SetZ( fZ );
		m_GhostDim[c]->SetZoom( fZoom );
		m_GhostDim[c]->SetRotationZ( fR );

		m_GhostBright[c]->Update( fDeltaTime );
		m_GhostBright[c]->SetX( fX );
		m_GhostBright[c]->SetY( fY );
		m_GhostBright[c]->SetZ( fZ );
		m_GhostBright[c]->SetZoom( fZoom );
		m_GhostBright[c]->SetRotationZ( fR );

		FOREACH_HoldType( ht )
		{
			m_HoldGhost[ht][c]->Update( fDeltaTime );
			m_HoldGhost[ht][c]->SetX( fX );
			m_HoldGhost[ht][c]->SetY( fY );
			m_HoldGhost[ht][c]->SetZ( fZ );
			m_HoldGhost[ht][c]->SetZoom( fZoom );
			m_HoldGhost[ht][c]->SetRotationZ( fR );
		}
	}
}

void GhostArrowRow::DrawPrimitives()
{
	for( int c=0; c<m_iNumCols; c++ )
	{
		int col = GAMESTATE->m_viNSDrawOrder[m_PlayerNumber][c];

		g_NoteFieldMode[m_PlayerNumber].BeginDrawTrack(col);

		//m_TapGhost[col]->Draw();
		m_GhostDim[col]->Draw();
		m_GhostBright[col]->Draw();
		FOREACH_HoldType( ht )
			m_HoldGhost[ht][col]->Draw();

		g_NoteFieldMode[m_PlayerNumber].EndDrawTrack(col);
	}
}

void GhostArrowRow::DidTapNote( int iCol, TapNoteScore score )
{
	ASSERT( iCol >= 0  &&  iCol < m_iNumCols );

	//m_TapGhost[iCol]->Step( score );
	m_GhostBright[iCol]->Step( score );
	m_GhostDim[iCol]->Step( score );
}

void GhostArrowRow::DidTapNote( int iCol, TapNoteScore score, bool bBright )
{
	ASSERT( iCol >= 0  &&  iCol < m_iNumCols );

	//m_TapGhost[iCol]->Step( score );

	if( bBright )
		m_GhostBright[iCol]->Step( score );
	else
		m_GhostDim[iCol]->Step( score );
}

void GhostArrowRow::SetHoldIsActive( const HoldNote& hn )
{
	m_HoldGhost[hn.subtype][hn.iTrack]->SetHoldIsActive( true );
}

void GhostArrowRow::CopyTweening( const GhostArrowRow &from )
{
	for( int c=0; c<m_iNumCols; c++ )
	{
		//m_TapGhost[c]->CopyTweening( *from.m_TapGhost[c] );
		m_GhostDim[c]->CopyTweening( *from.m_GhostDim[c] );
		m_GhostBright[c]->CopyTweening( *from.m_GhostBright[c] );
		FOREACH_HoldType( ht )
			m_HoldGhost[ht][c]->CopyTweening( *from.m_HoldGhost[ht][c] );
	}

	ActorFrame::CopyTweening( from );
}

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * Contains code (c) 2001-2004 Chris Danford.
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
