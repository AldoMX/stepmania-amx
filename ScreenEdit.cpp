#include "global.h"
#include "ScreenEdit.h"
#include "PrefsManager.h"
#include "SongManager.h"
#include "ScreenManager.h"
#include "GameConstantsAndTypes.h"
#include "PrefsManager.h"
#include "RageLog.h"
#include "GameSoundManager.h"
#include "GameState.h"
#include "InputMapper.h"
#include "RageLog.h"
#include "ThemeManager.h"
#include "ScreenMiniMenu.h"
#include "NoteSkinManager.h"
#include "Steps.h"
#include <utility>
#include "NoteFieldPositioning.h"
#include "arch/arch.h"
#include "NoteDataUtil.h"
#include "SongUtil.h"
#include "StepsUtil.h"
#include "Foreach.h"
#include "Song.h"
#include "ScoreKeeperMAX2.h"
#include "ScoreKeeperPump.h"
#include "CommonMetrics.h"
#include "ArrowEffects.h"

const float RECORD_HOLD_ROLL_SECONDS = 0.3f;

//
// Defines specific to GameScreenTitleMenu
//
#define DEBUG_X			(SCREEN_LEFT + 10)
#define DEBUG_Y			(CENTER_Y-100)

#define SHORTCUTS_X		(CENTER_X - 150)
#define SHORTCUTS_Y		(CENTER_Y)

#define HELP_X			(SCREEN_LEFT)
#define HELP_Y			(CENTER_Y)

#define HELP_TEXT_X		(SCREEN_LEFT + 4)
#define HELP_TEXT_Y		(40)

#define INFO_X			(SCREEN_RIGHT)
#define INFO_Y			(CENTER_Y)

#define INFO_TEXT_X		(SCREEN_RIGHT - 114)
#define INFO_TEXT_Y		(40)

#define MENU_WIDTH		(110)
#define EDIT_X			(CENTER_X)
#define EDIT_GRAY_Y_STANDARD	(SCREEN_TOP+60)
#define EDIT_GRAY_Y_REVERSE		(SCREEN_BOTTOM-60)

#define PLAYER_X			(CENTER_X)
#define PLAYER_Y			(CENTER_Y)
#define PLAYER_HEIGHT		(360)
#define PLAYER_Y_STANDARD	(PLAYER_Y-PLAYER_HEIGHT/2)
#define PLAYER_Y_REVERSE	(PLAYER_Y+PLAYER_HEIGHT/2)

#define ACTION_MENU_ITEM_X			(CENTER_X-200)
#define ACTION_MENU_ITEM_START_Y	(SCREEN_TOP + 24)
#define ACTION_MENU_ITEM_SPACING_Y	(18)

#define NAMING_MENU_ITEM_X			(CENTER_X-200)
#define NAMING_MENU_ITEM_START_Y	(SCREEN_TOP + 24)
#define NAMING_MENU_ITEM_SPACING_Y	(18)

CachedThemeMetricF	TICK_EARLY_SECONDS	("ScreenGameplay","TickEarlySeconds");

const ScreenMessage SM_BackFromMainMenu				= (ScreenMessage)(SM_User+1);
const ScreenMessage SM_BackFromAreaMenu				= (ScreenMessage)(SM_User+2);
const ScreenMessage SM_BackFromEditNotesStatistics	= (ScreenMessage)(SM_User+3);
const ScreenMessage SM_BackFromEditOptions			= (ScreenMessage)(SM_User+4);
const ScreenMessage SM_BackFromEditSongInfo			= (ScreenMessage)(SM_User+5);
const ScreenMessage SM_BackFromBGChange				= (ScreenMessage)(SM_User+6);
const ScreenMessage SM_BackFromPlayerOptions		= (ScreenMessage)(SM_User+7);
const ScreenMessage SM_BackFromSongOptions			= (ScreenMessage)(SM_User+8);
const ScreenMessage SM_BackFromInsertAttack			= (ScreenMessage)(SM_User+9);
const ScreenMessage SM_BackFromInsertAttackModifiers= (ScreenMessage)(SM_User+10);
const ScreenMessage SM_BackFromPrefs				= (ScreenMessage)(SM_User+11);
const ScreenMessage SM_BackFromCourseModeMenu		= (ScreenMessage)(SM_User+12);
const ScreenMessage SM_DoReloadFromDisk				= (ScreenMessage)(SM_User+13);
const ScreenMessage SM_DoUpdateTextInfo				= (ScreenMessage)(SM_User+14);
const ScreenMessage SM_BackFromTimingMenu			= (ScreenMessage)(SM_User+15);
const ScreenMessage SM_BackFromInsertNotes			= (ScreenMessage)(SM_User+16);
const ScreenMessage SM_BackFromHoldRoll				= (ScreenMessage)(SM_User+17);

const CString HELP_TEXT =
	"Up/Down:\n    change beat\n"
	"Left/Right:\n    change snap\n"
	"Number keys:\n    add/remove\n    tap note\n"
	"Shift + Numkeys:\n    swap between\n    notes\n"
	"Create hold note:\n    Hold a number\n    while moving\n    Up or Down\n"
	"Create a roll:\n    Hold a number\n    and SHIFT\n    while moving\n    Up or Down\n"
	"Space bar:\n    Set area marker\n"
	"Enter:\n    Area Menu\n"
	"Escape:\n    Main Menu\n"
	"F3:\n    Timing Menu\n"
	"F1:\n    Show\n    keyboard\n    shortcuts\n";

static const MenuRow g_KeyboardShortcutsItems[] =
{
	{ "PgUp/PgDn: jump measure",												false, 0, { NULL } },
	{ "Home/End: jump to first/last beat",										false, 0, { NULL } },
	{ "Ctrl + Up/Down/PgUp/PgDn/Home/End: Change zoom",							false, 0, { NULL } },
	{ "Shift + Up/Down/PgUp/PgDn/Home/End: Drag area marker",					false, 0, { NULL } },
	{ "P: Play selection",														false, 0, { NULL } },
	{ "Ctrl + P: Play whole song",												false, 0, { NULL } },
	{ "Shift + P: Play current beat to end",									false, 0, { NULL } },
	{ "Ctrl + R: Record",														false, 0, { NULL } },
	{ "F4: Toggle assist tick",													false, 0, { NULL } },
	{ "F5/F6: Next/prev steps of same StepsType",								false, 0, { NULL } },
	{ "F7/F8 + None/Shift/Ctrl/Alt: Decrease/increase BPM at cur beat",			false, 0, { NULL } },
	{ "F9/F10 + None/Shift/Ctrl/Alt: Decrease/increase stop at cur beat",		false, 0, { NULL } },
	{ "F11/F12 + None/Shift/Ctrl/Alt: Decrease/increase music offset",			false, 0, { NULL } },
	{ ", and . + None/Shift/Ctrl/Alt: Decrease/increase tickcount at cur beat",	false, 0, { NULL } },
		/* XXX: This would be better as a single submenu, to let people tweak
		 * and play the sample several times (without having to re-enter the
		 * menu each time), so it doesn't use a whole bunch of hotkeys. */
	{ "[ and ] + None/Shift/Ctrl/Alt: Decrease/increase sample music start",	false, 0, { NULL } },
	{ "{ and } + None/Shift/Ctrl/Alt: Decrease/increase sample music length",	false, 0, { NULL } },
	{ "M: Play sample music",													false, 0, { NULL } },
	{ "B: Add/Edit Background Change",											false, 0, { NULL } },
	{ "Insert: Insert beat and shift down",										false, 0, { NULL } },
	{ "Ctrl + Insert: Shift BPM/tick changes and stops down one beat",			false, 0, { NULL } },
	{ "Delete: Delete beat and shift up",										false, 0, { NULL } },
	{ "Ctrl + Delete: Shift BPM/tick changes and stops up one beat",			false, 0, { NULL } },
	{ "Shift + number: Swap between notes (mine, lift, hidden, etc.)",			false, 0, { NULL } },
	{ "Ctrl + number: Add or Remove Note Menu",									false, 0, { NULL } },
	{ "Alt + number: Add to/remove from right half",							false, 0, { NULL } },
	{ NULL, true, 0, { NULL } }
};
static Menu g_KeyboardShortcuts( "Keyboard Shortcuts", g_KeyboardShortcutsItems );

static const MenuRow g_MainMenuItems[] =
{
	{ "Edit Steps Statistics",		true, 0, { NULL } },
	{ "Play Whole Song",			true, 0, { NULL } },
	{ "Play Current Beat To End",	true, 0, { NULL } },
	{ "Save",						true, 0, { NULL } },
	{ "Reload from disk",			true, 0, { NULL } },
	{ "Load/Delete Backups",		false, 0, { NULL } },
	{ "Player Options",				true, 0, { NULL } },
	{ "Song Options",				true, 0, { NULL } },
	{ "Edit Song Info",				true, 0, { NULL } },
	{ "Timing Menu",				true, 0, { NULL } },
	{ "Add/Edit BG Change",			true, 0, { NULL } },
	{ "Play preview music",			true, 0, { NULL } },
	{ "Preferences",				true, 0, { NULL } },
	{ "Exit (discards changes since last save)",true, 0, { NULL } },
	{ NULL, true, 0, { NULL } }
};
static Menu g_MainMenu( "Main Menu", g_MainMenuItems );

static const MenuRow g_AreaMenuItems[] =
{
	{ "Cut",						true, 0, { NULL } },
	{ "Copy",						true, 0, { NULL } },
	{ "Paste at current beat",		true, 0, { NULL } },
	{ "Paste at begin marker",		true, 0, { NULL } },
	{ "Clear",						true, 0, { NULL } },
	{ "Quantize",					true, 0, { "4TH","8TH","12TH","16TH","24TH","32ND","48TH","64TH" } },
	{ "Turn",						true, 0, { "Backwards","Mirror","Left","Right","Shuffle","Super Shuffle" } },
	{ "Transform",					true, 0, { "NoHolds","NoRolls","NoMines","NoShocks","NoPotions","NoLifts","NoHidden", "Mines To Shocks","HalfMines","HalfShocks","HalfPotions","Little","Wide","Big","Quick","BMRize","Skippy","Mines","Shocks","Potions","Lifts","Echo","Stomp","Planted","Floored","Twister","NoJumps","NoHands","NoQuads" } },
	{ "Alter",						true, 0, { "Backwards","Swap Sides","Copy Left To Right","Copy Right To Left","Clear Left","Clear Right","Collapse To One","Collapse Left","Shift Left","Shift Right" } },
	{ "Tempo",						true, 0, { "Compress 2x","Compress 3->2","Compress 4->3","Expand 3->4","Expand 2->3","Expand 2x" } },
	{ "Play selection",				true, 0, { NULL } },
	{ "Record in selection",		true, 0, { NULL } },
	{ "Insert beat and shift down",	true, 0, { NULL } },
	{ "Delete beat and shift up",	true, 0, { NULL } },
	{ "Shift pauses and BPM changes down",
									true, 0, { NULL } },
	{ "Shift pauses and BPM changes up",
									true, 0, { NULL } },
	{ "Convert beats to pause",		true, 0, { NULL } },
	{ "Convert pause to beats",		true, 0, { NULL } },
	{ NULL, true, 0, { NULL } }
};
static Menu g_AreaMenu( "Area Menu", g_AreaMenuItems );

static const MenuRow g_EditNotesStatisticsItems[] =
{
	{ "Players",					true,  0, { "1","2","3","4","5","6","7","8" } },
	{ "Difficulty",					true,  0, { "BEGINNER","EASY","MEDIUM","HARD","CHALLENGE","EDIT" } },
	{ "Meter",						true,  0, { "1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25" } },
	{ "Description",				true,  0, { NULL } },
	{ "Predicted Meter",			false, 0, { NULL } },
	{ "Tap Steps",					false, 0, { NULL } },
	{ "Hold Steps",					false, 0, { NULL } },
	{ "Rolls",						false, 0, { NULL } },
	{ "Stream",						false, 0, { NULL } },
	{ "Voltage",					false, 0, { NULL } },
	{ "Air",						false, 0, { NULL } },
	{ "Freeze",						false, 0, { NULL } },
	{ "Chaos",						false, 0, { NULL } },
	{ NULL, true, 0, { NULL } }
};
static Menu g_EditNotesStatistics( "Statistics", g_EditNotesStatisticsItems );

static const MenuRow g_EditSongInfoItems[] =
{
	{ "Main title",					true, 0, { NULL } },
	{ "Sub title",					true, 0, { NULL } },
	{ "Artist",						true, 0, { NULL } },
	{ "Credit",						true, 0, { NULL } },
	{ "Genre",						true, 0, { NULL } },
	{ "Main title transliteration",	true, 0, { NULL } },
	{ "Sub title transliteration",	true, 0, { NULL } },
	{ "Artist transliteration",		true, 0, { NULL } },
	{ NULL, true, 0, { NULL } }
};
static Menu g_EditSongInfo( "Edit Song Info", g_EditSongInfoItems );

static const MenuRow g_BGChangeItems[] =
{
	{ "Rate (applies to new adds)",			true, 10, { "0%","10%","20%","30%","40%","50%","60%","70%","80%","90%","100%","120%","140%","160%","180%","200%" } },
	{ "Fade Last (applies to new adds)",	true, 0, { "NO","YES" } },
	{ "Rewind Movie (applies to new adds)",	true, 0, { "NO","YES" } },
	{ "Loop (applies to new adds)",			true, 1, { "NO","YES" } },
	{ "Add Change to random",				true, 0, { NULL } },
	{ "Add Change to song BGAnimation",		true, 0, { NULL } },
	{ "Add Change to song Movie",			true, 0, { NULL } },
	{ "Add Change to song Still",			true, 0, { NULL } },
	{ "Add Change to global Random Movie",	true, 0, { NULL } },
	{ "Add Change to global BGAnimation",	true, 0, { NULL } },
	{ "Add Change to global Visualization",	true, 0, { NULL } },
	{ "Remove Change",						true, 0, { NULL } },
	{ NULL, false, 0, { NULL } }
};
static Menu g_BGChange( "Background Change", g_BGChangeItems );

static const MenuRow g_PrefsItems[] =
{
	{ "Show BGChanges during play/record",	true, 0, { "NO","YES" } },
	{ "Enable shift as area selector",		true, 0, { "NO","YES" } },
	{ "Display every supported style",		true, 0, { "NO","YES" } },
	{ "Save last edited song",				true, 0, { "NO","YES" } },
	{ "Save last edited difficulty",		true, 0, { "NO","YES" } },
	{ "Smooth scrolling",					true, 0, { "NO","YES" } },
	{ "Use gameplay keys to add notes",		true, 0, { "NO","YES" } },
	{ "Show combo during play",				true, 0, { "NO","YES" } },
	{ "Show snap beats",					true, 0, { "NO","YES" } },
	{ "Save timing in rows",				true, 0, { "NO","YES" } },
	{ "Highlight ends at last second",		true, 0, { "NO","YES" } },
	{ "Default save format",				false, 0, { "SMA" } },
	{ NULL, false, 0, { NULL } }
};
static Menu g_Prefs( "Preferences", g_PrefsItems );

static const MenuRow g_InsertAttackItems[] =
{
	{ "Duration seconds",					true, 3, { "5","10","15","20","25","30","35","40","45" } },
	{ "Set modifiers",						true, 0, { "PRESS START" } },
	{ NULL, true, 0, { NULL } }
};
static Menu g_InsertAttack( "Insert Attack", g_InsertAttackItems );

static const MenuRow g_CourseModeItems[] =
{
	{ "Play mods from course",				true, 0, { NULL } },
	{ NULL, true, 0, { NULL } }
};
static Menu g_CourseMode( "Course Display", g_CourseModeItems );

static const MenuRow g_TimingMenuItems[] =
{
	{ "Editor Mode",			true, 0, { "Steps","Background" } },
	{ "----------------------",	false, 0, { NULL } },
	{ "  BPM",					true, 0, { NULL } },
	{ "  Stop/Delay",			true, 0, { NULL } },
	{ "----------------------",	false, 0, { NULL } },
	{ "  Rows per Beat",		false, 0, { "192" } },
	{ "  Beats per Measure",	false, 0, { "4" } },
	{ "----------------------",	false, 0, { NULL } },
	{ "  Preview Start",		true, 0, { NULL } },
	{ "  Preview Length",		true, 0, { NULL } },
	{ "----------------------",	false, 0, { NULL } },
	{ "  Beat 0 Offset",		true, 0, { NULL } },
	{ "----------------------",	false, 0, { NULL } },
	{ "  Multiplier",			true, 0, { NULL } },
	{ "  Tickcount",			true, 0, { "0","1","2","3","4","6","8","12","16","24","32","48","64","96","192" } },
	{ "  Fake Area",			true, 0, { NULL } },
	{ "  Speed Area",			true, 0, { NULL } },
	{ "  Speed",				true, 0, { NULL } },
	{ NULL, true, 0, { NULL } }
};
static Menu g_TimingMenu( "Timing Menu", g_TimingMenuItems );

static const MenuRow g_InsertNotesItems[] =
{
	{ "Tap",					true, 0, { NULL } },
	{ "Mine",					true, 0, { NULL } },
	{ "Shock",					true, 0, { NULL } },
	{ "Potion",					true, 0, { NULL } },
	{ "Lift",					true, 0, { NULL } },
	{ "Hidden",					true, 0, { NULL } },
	{ "Remove",					true, 0, { NULL } },
	{ NULL, true, 0, { NULL } }
};
static Menu g_InsertNotes( "Add Note", g_InsertNotesItems );

static const MenuRow g_HoldRollItems[] =
{
	{ "Change Hold to Roll",		true, 0, { NULL } },
	{ "Change Roll to Hold",		true, 0, { NULL } },
	{ "Remove",						true, 0, { NULL } },
	{ NULL, true, 0, { NULL } }
};
static Menu g_HoldRoll( "Hold and Roll Notes", g_HoldRollItems );

// HACK: need to remember the track we're inserting on so
// that we can lay the attack note after coming back from
// menus.
int g_iLastInsertAttackTrack = -1;
float g_fLastInsertAttackDurationSeconds = -1;

ScreenEdit::ScreenEdit( CString sName ) : Screen( sName )
{
	LOG->Trace( "ScreenEdit::ScreenEdit()" );

	m_PlayerNumber = GAMESTATE->m_MasterPlayerNumber;
	m_uCurrentPlayer = 1;
	m_uCurrentItem = TapNote::mine;

	// Reset AutoPlay detection!
	// I think this is unnecessary, but let's play it safe anyways
	GAMESTATE->m_bUsedAutoPlay[m_PlayerNumber] = false;
	GAMESTATE->m_iNotesInAutoPlay[m_PlayerNumber] = 0;

	/* We do this ourself. */
	SOUND->HandleSongTimer( false );

	TICK_EARLY_SECONDS.Refresh();

	// set both players to joined so the credit message doesn't show
	FOREACH_PlayerNumber( p )
		GAMESTATE->m_bSideIsJoined[p] = true;
	SCREENMAN->RefreshCreditsMessages();

	m_pSong = GAMESTATE->m_pCurSong;
	m_pSteps = GAMESTATE->m_pCurSteps[m_PlayerNumber];
	m_pAttacksFromCourse = NULL;

	NoteData noteData;
	m_pSteps->GetNoteData( &noteData );

	// Init the judgment labels here
	// In edit, just use the stock labels
	m_Player.InitJudgment( m_PlayerNumber, DIFFICULTY_INVALID );

	GAMESTATE->m_EditMode = GAMESTATE->MODE_EDITING;
	GAMESTATE->m_bPlaying = false;
	GAMESTATE->m_bPastHereWeGo = false;
	GAMESTATE->m_bEditing = true;

	GAMESTATE->m_fPlayerBeat[m_PlayerNumber] = 0;
	m_fTrailingBeat = GAMESTATE->m_fPlayerBeat[m_PlayerNumber];

	GAMESTATE->m_PlayerOptions[m_PlayerNumber].m_fScrollSpeed = 1;

	GAMESTATE->m_SongOptions.m_fMusicRate = 1;

	// Not all games have a noteskin named "note" ...
	//if( NOTESKIN->DoesNoteSkinExist("note") )
	//	GAMESTATE->m_PlayerOptions[m_PlayerNumber].m_sNoteSkin = "note";	// change noteskin before loading all of the edit Actors

	GAMESTATE->ResetNoteSkins();
	GAMESTATE->StoreSelectedOptions();

	m_BGAnimation.LoadFromAniDir( THEME->GetPathB(m_sName, "background") );

	shiftAnchor = -1;

	m_NoteFieldEdit.SetXY( EDIT_X, PLAYER_Y );
	m_NoteFieldEdit.SetZoom( 0.5f );
	m_NoteFieldEdit.Load( &noteData, m_PlayerNumber );
	m_NoteFieldEdit.UpdateDrawingArea( -240, 800, PLAYER_HEIGHT*2 );
	m_uNumPlayers = m_NoteFieldEdit.GetNumPlayers();

	m_rectRecordBack.StretchTo( RectF(SCREEN_LEFT, SCREEN_TOP, SCREEN_RIGHT, SCREEN_BOTTOM) );
	m_rectRecordBack.SetDiffuse( RageColor(0,0,0,0) );

	m_NoteFieldRecord.SetXY( EDIT_X, PLAYER_Y );
	m_NoteFieldRecord.SetZoom( 1.0f );
	m_NoteFieldRecord.Load( &noteData, m_PlayerNumber );
	m_NoteFieldRecord.UpdateDrawingArea( -150, 350, 350 );

	m_Clipboard.Config( m_NoteFieldEdit );

	GAMESTATE->m_PlayerOptions[m_PlayerNumber].Init();	// don't allow weird options in editor.  It doesn't handle reverse well.

	// Set NoteSkin to note if available.
	// Change noteskin back to default before loading player.
	// TODO - Aldo_MX: Note with routine support
	if( m_uNumPlayers < 2 && NOTESKIN->DoesNoteSkinExist("note") )
		GAMESTATE->m_PlayerOptions[m_PlayerNumber].m_sNoteSkin = "note";

	GAMESTATE->ResetNoteSkins();

	/* XXX: Do we actually have to send real note data here, and to m_NoteFieldRecord?
	 * (We load again on play/record.) */
	m_Player.Load( m_PlayerNumber, &noteData, NULL, NULL, NULL, NULL, NULL, NULL, NULL );
	GAMESTATE->m_PlayerController[m_PlayerNumber] = PC_HUMAN;
	m_Player.SetX( PLAYER_X );
	/* Why was this here?  Nothing ever sets Player Y values; this was causing
	 * the display in play mode to be offset half a screen down. */
//	m_Player.SetXY( PLAYER_X, PLAYER_Y );

	m_In.Load( THEME->GetPathB(m_sName, "in") );
	m_In.StartTransitioning();

	m_Out.Load( THEME->GetPathB(m_sName, "out") );

	m_sprHelp.Load( THEME->GetPathG(m_sName, "help") );
	m_sprHelp.SetHorizAlign( Actor::align_left );
	m_sprHelp.SetXY( HELP_X, HELP_Y );

	m_textHelp.LoadFromFont( THEME->GetPathF("Common", "normal") );
	m_textHelp.SetXY( HELP_TEXT_X, HELP_TEXT_Y );
	m_textHelp.SetHorizAlign( Actor::align_left );
	m_textHelp.SetVertAlign( Actor::align_top );
	m_textHelp.SetZoom( 0.475f );
	m_textHelp.SetText( HELP_TEXT );
	m_textHelp.SetShadowLength( 0 );

	m_sprInfo.Load( THEME->GetPathG(m_sName, "Info") );
	m_sprInfo.SetHorizAlign( Actor::align_right );
	m_sprInfo.SetXY( INFO_X, INFO_Y );

	m_textInfo.LoadFromFont( THEME->GetPathF("Common", "normal") );
	m_textInfo.SetXY( INFO_TEXT_X, INFO_TEXT_Y );
	m_textInfo.SetHorizAlign( Actor::align_left );
	m_textInfo.SetVertAlign( Actor::align_top );
	m_textInfo.SetZoom( 0.475f );
	m_textInfo.SetShadowLength( 0 );
	//m_textInfo.SetText();	// set this below every frame

	m_soundChangeLine.Load( THEME->GetPathS(m_sName, "line") );
	m_soundChangeSnap.Load( THEME->GetPathS(m_sName, "snap") );
	m_soundMarker.Load(		THEME->GetPathS(m_sName, "marker") );

	m_soundMusic.Load(m_pSong->GetMusicPath());

	m_soundAssistTick.Load(	THEME->GetPathS(m_sName, "assist tick") );

	m_SnapDisplay.SetXY( EDIT_X, PLAYER_Y_STANDARD );
	m_SnapDisplay.Load( m_PlayerNumber );
	m_SnapDisplay.SetZoom( 0.5f );

	this->HandleScreenMessage( SM_DoUpdateTextInfo );

	m_bBGEditMode = false;
	m_bLastBeat = false;

	m_uCurrentCol = 0;
	m_uCurrentPlayerRow = 0;
	m_iCurrentHold = 0;

	m_timerAutoSave.SetZero();
	m_timerAutoSave.Touch();
	m_bCanAutoSave = false;
}

ScreenEdit::~ScreenEdit()
{
	AutoSave();
	LOG->Trace( "ScreenEdit::~ScreenEdit()" );
	m_soundMusic.StopPlaying();
}

void ScreenEdit::AutoSave()
{
	if( !m_bCanAutoSave )
		return;

	m_bCanAutoSave = false;

	m_pSteps->SetNoteData( &m_NoteFieldEdit );
	m_pSong->ReCalculateRadarValuesAndLastBeat();
	m_pSong->TranslateTitles();

	CString sGetFilename;
	m_pSong->AutoSave( sGetFilename );
	SCREENMAN->SystemMessage( "Saved Automatically to '" + sGetFilename + "'." );

	SOUND->PlayOnce( THEME->GetPathS(m_sName, "autosave") );
}

void ScreenEdit::ResetAutoSave()
{
	if( m_bCanAutoSave )
		return;

	m_bCanAutoSave = true;
	m_timerAutoSave.Touch();
}

// play assist ticks
void ScreenEdit::PlayTicks()
{
	if( !GAMESTATE->m_SongOptions.m_bAssistTick || GAMESTATE->m_EditMode != GAMESTATE->MODE_PLAYING )
		return;

	/* Sound cards have a latency between when a sample is Play()ed and when the sound
	 * will start coming out the speaker.  Compensate for this by boosting fPositionSeconds
	 * ahead.  This is just to make sure that we request the sound early enough for it to
	 * come out on time; the actual precise timing is handled by SetStartTime. */
	float fPositionSeconds = GAMESTATE->m_fMusicSeconds;
	fPositionSeconds += SOUND->GetPlayLatency() + (float)TICK_EARLY_SECONDS;
	const float fSongBeat = GAMESTATE->m_pCurSteps[m_PlayerNumber]->m_Timing.GetBeatFromElapsedTime( fPositionSeconds );

	const int iSongRow = max( 0, BeatToNoteRow( fSongBeat ) );
	int iTickRow = -1;
	static int iRowLastCrossed = -1;

	if( iSongRow == 0 )
	{
		if( m_Player.IsThereATapOrHoldHeadAtRow( iSongRow ) )
			iTickRow = iSongRow;
	}
	else
	{
		if( iSongRow < iRowLastCrossed )
			iRowLastCrossed = iSongRow;

		for( int r=iRowLastCrossed+1; r<=iSongRow; r++ )  // for each index we crossed since the last update
			if( m_Player.IsThereATapOrHoldHeadAtRow( r ) )
				iTickRow = r;
	}

	iRowLastCrossed = iSongRow;

	if( iTickRow != -1 )
	{
		const float fTickBeat = NoteRowToBeat( iTickRow );
		const float fTickSecond = GAMESTATE->m_pCurSteps[m_PlayerNumber]->m_Timing.GetElapsedTimeFromBeat( fTickBeat );
		float fSecondsUntil = fTickSecond - GAMESTATE->m_fMusicSeconds;
		fSecondsUntil /= m_soundMusic.GetPlaybackRate(); /* 2x music rate means the time until the tick is halved */

		RageSoundParams p;
		p.StartTime = GAMESTATE->m_LastBeatUpdate + (fSecondsUntil - (float)TICK_EARLY_SECONDS);
		m_soundAssistTick.Play( &p );
	}
}

void ScreenEdit::PlayPreviewMusic()
{
	SOUND->PlayMusic("");
	SOUND->PlayMusic( m_pSong->GetMusicPath(), false,
		m_pSong->m_fMusicSampleStartSeconds,
		m_pSong->m_fMusicSampleLengthSeconds,
		1.5f );
}

void ScreenEdit::Update( float fDeltaTime )
{
	if( m_soundMusic.IsPlaying() )
	{
		RageTimer tm;
		const float fSeconds = m_soundMusic.GetPositionSeconds( NULL, &tm );
		GAMESTATE->UpdateSongPosition( fSeconds, m_pSong->m_Timing, tm );
		GAMESTATE->UpdatePlayerPosition( m_PlayerNumber, m_pSteps->m_Timing );
	}

	if( GAMESTATE->m_EditMode == GAMESTATE->MODE_RECORDING )
	{
		// add or extend holds and rolls

		for( int t=0; t<GAMESTATE->GetCurrentStyle()->m_iColsPerPlayer; t++ )	// for each track
		{
			StyleInput StyleI( m_PlayerNumber, t );
			float fSecsHeld = INPUTMAPPER->GetSecsHeld( StyleI );

			if( fSecsHeld > RECORD_HOLD_ROLL_SECONDS && GAMESTATE->m_fPlayerBeat[m_PlayerNumber] > 0 )
			{
				// add or extend hold
				const float fHoldStartSeconds = m_soundMusic.GetPositionSeconds() - fSecsHeld;

				float fStartBeat = max( 0, m_pSteps->m_Timing.GetBeatFromElapsedTime( fHoldStartSeconds ) );
				float fEndBeat = max( fStartBeat, GAMESTATE->m_fPlayerBeat[m_PlayerNumber] );

				// Round hold start and end to the nearest snap interval
				fStartBeat = froundf( fStartBeat, NoteTypeToBeat(m_SnapDisplay.GetNoteType()) );
				fEndBeat = froundf( fEndBeat, NoteTypeToBeat(m_SnapDisplay.GetNoteType()) );

				// create a new hold note
				HoldNote newHN(t, BeatToNoteRow(fStartBeat), BeatToNoteRowRounded(fEndBeat));
				newHN.subtype = INPUTFILTER->GetKeyFlagShift() != HOLDING_NONE ? HOLD_TYPE_ROLL : HOLD_TYPE_DANCE;
				newHN.playerNumber = (unsigned char)m_uCurrentPlayer;
				m_NoteFieldRecord.AddHoldNote( newHN );
			}
		}
	}

	if( GAMESTATE->m_EditMode == GAMESTATE->MODE_RECORDING  ||  GAMESTATE->m_EditMode == GAMESTATE->MODE_PLAYING )
	{
		if( PREFSMAN->m_bEditorShowBGChangesPlay )
		{
			m_Background.Update( fDeltaTime );
			m_Foreground.Update( fDeltaTime );
		}

		// check for end of playback/record

		if( GAMESTATE->m_fPlayerBeat[m_PlayerNumber] > ( PREFSMAN->m_bSelectLastSecond && m_bLastBeat ? m_NoteFieldEdit.m_fEndMarker : m_NoteFieldEdit.m_fEndMarker + fBEATS_PER_MEASURE ) )		// give a one measure lead out
		{
			m_bLastBeat = false;
			if( GAMESTATE->m_EditMode == GAMESTATE->MODE_RECORDING )
			{
				TransitionFromRecordToEdit();
			}
			else if( GAMESTATE->m_EditMode == GAMESTATE->MODE_PLAYING )
			{
				TransitionToEdit();
			}

			GAMESTATE->m_fPlayerBeat[m_PlayerNumber] = m_NoteFieldEdit.m_fEndMarker;
		}
	}

	m_BGAnimation.Update( fDeltaTime );
	m_SnapDisplay.Update( fDeltaTime );
	m_NoteFieldEdit.Update( fDeltaTime );
	m_In.Update( fDeltaTime );
	m_Out.Update( fDeltaTime );
	m_sprHelp.Update( fDeltaTime );
	m_textHelp.Update( fDeltaTime );
	m_sprInfo.Update( fDeltaTime );
	m_textInfo.Update( fDeltaTime );

	m_rectRecordBack.Update( fDeltaTime );

	if( GAMESTATE->m_EditMode == GAMESTATE->MODE_RECORDING )
		m_NoteFieldRecord.Update( fDeltaTime );

	if( GAMESTATE->m_EditMode == GAMESTATE->MODE_PLAYING )
		m_Player.Update( fDeltaTime );

	//LOG->Trace( "ScreenEdit::Update(%f)", fDeltaTime );
	Screen::Update( fDeltaTime );

	// Update trailing beat
	if( PREFSMAN->m_bEditorSmoothScrolling && GAMESTATE->m_EditMode != GAMESTATE->MODE_PLAYING )
	{
		float fDelta = GAMESTATE->m_fPlayerBeat[m_PlayerNumber] - m_fTrailingBeat;

		if( fabsf(fDelta) < 0.01 )
			m_fTrailingBeat = GAMESTATE->m_fPlayerBeat[m_PlayerNumber];	// snap
		else
		{
			float fSign = fDelta / fabsf(fDelta);
			float fMoveDelta = fSign*fDeltaTime*40 / GAMESTATE->m_CurrentPlayerOptions[m_PlayerNumber].m_fScrollSpeed;
			if( fabsf(fMoveDelta) > fabsf(fDelta) )
				fMoveDelta = fDelta;
			m_fTrailingBeat += fMoveDelta;

			if( fabsf(fDelta) > 10 )
			{
				/* We're far off; move faster.  Be sure to not overshoot. */
				fMoveDelta = fDelta * fDeltaTime*5;
				float fNewDelta = GAMESTATE->m_fPlayerBeat[m_PlayerNumber] - m_fTrailingBeat;
				if( fabsf(fMoveDelta) > fabsf(fNewDelta) )
					fMoveDelta = fNewDelta;
				m_fTrailingBeat += fMoveDelta;
			}
		}
	}
	else
		m_fTrailingBeat = GAMESTATE->m_fPlayerBeat[m_PlayerNumber];

	PlayTicks();

	if( m_bCanAutoSave && GAMESTATE->m_EditMode == GAMESTATE->MODE_EDITING )
	{
		if( PREFSMAN->m_fAutoSaveEverySeconds > 0.f && m_timerAutoSave.Ago() >= PREFSMAN->m_fAutoSaveEverySeconds )
			AutoSave();
	}
}

void ScreenEdit::UpdateTextInfo()
{
	int iNumTapNotes = m_NoteFieldEdit.GetNumTapNotes();
	int iNumHoldNotes = m_NoteFieldEdit.GetNumHoldNotes();
	int iNumRollNotes = m_NoteFieldEdit.GetNumRollNotes();

	CString sSnapBeat = "";
	if( PREFSMAN->m_bShowSnapBeats )
	{
		switch( m_SnapDisplay.GetNoteType() )
		{
		case NOTE_TYPE_4TH:		sSnapBeat = "Beat: 1st\n";		break;
		case NOTE_TYPE_8TH:		sSnapBeat = "Beat: 2nd\n";		break;
		case NOTE_TYPE_12TH:	sSnapBeat = "Beat: 3rd\n";		break;
		case NOTE_TYPE_16TH:	sSnapBeat = "Beat: 4th\n";		break;
		case NOTE_TYPE_24TH:	sSnapBeat = "Beat: 6th\n";		break;
		case NOTE_TYPE_32ND:	sSnapBeat = "Beat: 8th\n";		break;
		case NOTE_TYPE_48TH:	sSnapBeat = "Beat: 12th\n";		break;
		case NOTE_TYPE_64TH:	sSnapBeat = "Beat: 16th\n";		break;
		case NOTE_TYPE_96TH:	sSnapBeat = "Beat: 24th\n";		break;
		case NOTE_TYPE_128TH:	sSnapBeat = "Beat: 32nd\n";		break;
		case NOTE_TYPE_192ND:	sSnapBeat = "Beat: 48th\n";		break;
		case NOTE_TYPE_256TH:	sSnapBeat = "Beat: 64th\n";		break;
		case NOTE_TYPE_384TH:	sSnapBeat = "Beat: 96th\n";		break;
		case NOTE_TYPE_768TH:	sSnapBeat = "Beat: 192nd\n";	break;
		default: ASSERT(0);
		}
	}

	CString sText;
	// check Editor.ini for a few of these
	// entries used: CurBeatPlaces, CurSecPlaces, OffsetPlaces,
	//               PreviewStartPlaces, PreviewLengthPlaces
	/* No need for that (unneeded options is just a maintenance pain).  If you want
	 * more precision here, add it.  I doubt there's a need for precise preview output,
	 * though (it'd be nearly inaudible at the millisecond level, and it's approximate
	 * anyway). */
	sText += ssprintf( "Current Row:\n     %d\n",			BeatToNoteRow(GAMESTATE->m_fPlayerBeat[m_PlayerNumber]) );
	sText += ssprintf( "Current Beat:\n     %.3f\n",		GAMESTATE->m_fPlayerBeat[m_PlayerNumber] );
	sText += ssprintf( "Current Second:\n     %.3f\n",		m_bBGEditMode ? m_pSong->m_Timing.GetElapsedTimeFromBeat(GAMESTATE->m_fPlayerBeat[m_PlayerNumber]) : m_pSteps->m_Timing.GetElapsedTimeFromBeat(GAMESTATE->m_fPlayerBeat[m_PlayerNumber]) );
	sText += ssprintf( "Snap to:\n%s%s\n%s",				PREFSMAN->m_bShowSnapBeats ? "Measure: " : "     ", NoteTypeToString( m_SnapDisplay.GetNoteType() ).c_str(), sSnapBeat.c_str() );
	sText += ssprintf( "Selection begin:\n     %s\n",		m_NoteFieldEdit.m_fBeginMarker==-1 ? "not set" : ssprintf("%.2f",m_NoteFieldEdit.m_fBeginMarker).c_str() );
	sText += ssprintf( "Selection end:\n     %s\n",			m_NoteFieldEdit.m_fEndMarker==-1 ? "not set" : ssprintf("%.2f",m_NoteFieldEdit.m_fEndMarker).c_str() );
	sText += ssprintf( "Difficulty:\n     %s\n",			DifficultyToString( m_pSteps->GetDifficulty() ).c_str() );
	sText += ssprintf( "Description:\n     %s\n",			m_bBGEditMode ? "-background-" : GAMESTATE->m_pCurSteps[m_PlayerNumber] ? GAMESTATE->m_pCurSteps[m_PlayerNumber]->GetDescription().c_str() : "no description" );
	sText += ssprintf( "Main title:\n     %s\n",			m_pSong->m_sMainTitle.c_str() );
	sText += ssprintf( "Sub title:\n     %s\n",				m_pSong->m_sSubTitle.c_str() );
	sText += ssprintf( "Tap Steps:\n     %d\n",				iNumTapNotes );
	sText += ssprintf( "Hold Steps:\n     %d\n",			iNumHoldNotes );
	sText += ssprintf( "Rolls:\n     %d\n",					iNumRollNotes );
	sText += ssprintf( "Beat 0 Offset:\n     %.3f secs\n",	m_bBGEditMode ? m_pSong->m_Timing.m_fBeat0Offset : m_pSteps->m_Timing.m_fBeat0Offset );
	sText += ssprintf( "Preview Start:\n     %.2f secs\n",	m_pSong->m_fMusicSampleStartSeconds );
	sText += ssprintf( "Preview Length:\n     %.2f secs\n",	m_pSong->m_fMusicSampleLengthSeconds );

	m_textInfo.SetText( sText );
}

void ScreenEdit::DrawPrimitives()
{
//	m_rectRecordBack.Draw();

	switch( GAMESTATE->m_EditMode )
	{
	case GameState::MODE_BACKGROUND:
	case GameState::MODE_EDITING:
		{
			m_BGAnimation.Draw();
			m_sprHelp.Draw();
			m_textHelp.Draw();
			m_sprInfo.Draw();
			m_textInfo.Draw();
			m_SnapDisplay.Draw();

			// HACK:  Make NoteFieldEdit draw using the trailing beat
			float fPlayerBeat = GAMESTATE->m_fPlayerBeat[m_PlayerNumber];	// save song beat
			GAMESTATE->m_fPlayerBeat[m_PlayerNumber] = m_fTrailingBeat;	// put trailing beat in effect
			m_NoteFieldEdit.Draw();
			GAMESTATE->m_fPlayerBeat[m_PlayerNumber] = fPlayerBeat;	// restore real song beat

			m_In.Draw();
			m_Out.Draw();
		}
		break;

	case GameState::MODE_RECORDING:
		if( PREFSMAN->m_bEditorShowBGChangesPlay )
			m_Background.Draw();
		else
			m_BGAnimation.Draw();

		m_NoteFieldRecord.Draw();
		if( PREFSMAN->m_bEditorShowBGChangesPlay )
			m_Foreground.Draw();
		break;

	case GameState::MODE_PLAYING:
		if( PREFSMAN->m_bEditorShowBGChangesPlay )
			m_Background.Draw();
		else
			m_BGAnimation.Draw();

		m_Player.Draw();
		if( PREFSMAN->m_bEditorShowBGChangesPlay )
			m_Foreground.Draw();
		break;

	default:
		ASSERT(0);
	}

	Screen::DrawPrimitives();
}

void ScreenEdit::Input( const DeviceInput& DeviceI, const InputEventType type, const GameInput &GameI, const MenuInput &MenuI, const StyleInput &StyleI )
{
//	LOG->Trace( "ScreenEdit::Input()" );

	if( m_In.IsTransitioning() || m_Out.IsTransitioning() )
		return;

	switch( GAMESTATE->m_EditMode )
	{
	case GameState::MODE_BACKGROUND:
	case GameState::MODE_EDITING:		InputEdit( DeviceI, type, GameI, MenuI, StyleI );	break;
	case GameState::MODE_RECORDING:		InputRecord( DeviceI, type, GameI, MenuI, StyleI );	break;
	case GameState::MODE_PLAYING:		InputPlay( DeviceI, type, GameI, MenuI, StyleI );	break;
	default:	ASSERT(0);
	}

	/* Make sure the displayed time is up-to-date after possibly changing something,
	 * so it doesn't feel lagged. */
	UpdateTextInfo();
}

void ScreenEdit::InputEdit( const DeviceInput& DeviceI, const InputEventType type, const GameInput &GameI, const MenuInput &MenuI, const StyleInput &StyleI )
{
	if( DeviceI.device != DEVICE_KEYBOARD )
		return;

	if( type == IET_LEVEL_CHANGED )
		return;		// don't care

	if(type == IET_RELEASE)
	{
		switch( DeviceI.button ) {
		case KEY_LSHIFT:
		case KEY_RSHIFT:
			shiftAnchor = -1;
			break;
		}
		return;
	}

	const int iKeyFlag = INPUTFILTER->GetModifierKeyFlag();

	// If a gameplay key is pressed (ex. ZQSEC in PIU), add a Note/Hold accordignly
	if( PREFSMAN->m_bGameplayKeysInEditor && type == IET_FIRST_PRESS && StyleI.player == m_PlayerNumber )
	{
		if( InputNote( StyleI.col, iKeyFlag ) )
			return;
	}

	switch( DeviceI.button )
	{
	case KEY_C1:
	case KEY_C2:
	case KEY_C3:
	case KEY_C4:
	case KEY_C5:
	case KEY_C6:
	case KEY_C7:
	case KEY_C8:
	case KEY_C9:
	case KEY_C0:
		{
			if( type != IET_FIRST_PRESS )
				break;	// We only care about first presses

			int iCol = DeviceI.button == KEY_C0 ? 9 : DeviceI.button - KEY_C1;

			// Alt + number = input to right half
			if( iKeyFlag & HOLDING_ALT )
				iCol += m_NoteFieldEdit.GetNumTracks()/2;

			if( InputNote( iCol, iKeyFlag ) )
				return;
		}
		break;
	case KEY_UP:
	case KEY_DOWN:
	case KEY_PGUP:
	case KEY_PGDN:
	case KEY_HOME:
	case KEY_END:
		{
			if( iKeyFlag & HOLDING_CTRL )
			{
				float& fScrollSpeed = GAMESTATE->m_PlayerOptions[m_PlayerNumber].m_fScrollSpeed;
				float fNewScrollSpeed = fScrollSpeed;

				if( DeviceI.button == KEY_HOME )
					fNewScrollSpeed = 1;
				else if( DeviceI.button == KEY_PGDN )
				{
					if(			fScrollSpeed < .5	)	fNewScrollSpeed = .5;
					else if(	fScrollSpeed < 1	)	fNewScrollSpeed = 1;
					else if(	fScrollSpeed < 2	)	fNewScrollSpeed = 2;
					else if(	fScrollSpeed < 4	)	fNewScrollSpeed = 4;
					else								fNewScrollSpeed = 8;
				}
				else if( DeviceI.button == KEY_DOWN )
				{
					if(			fScrollSpeed < .25	)	fNewScrollSpeed = .25;
					else if(	fScrollSpeed < .5	)	fNewScrollSpeed = .5;
					else if(	fScrollSpeed < .75	)	fNewScrollSpeed = .75;
					else if(	fScrollSpeed < 1	)	fNewScrollSpeed = 1;
					else if(	fScrollSpeed < 1.5	)	fNewScrollSpeed = 1.5;
					else if(	fScrollSpeed < 2	)	fNewScrollSpeed = 2;
					else if(	fScrollSpeed < 2.5	)	fNewScrollSpeed = 2.5;
					else if(	fScrollSpeed < 3	)	fNewScrollSpeed = 3;
					else if(	fScrollSpeed < 3.5	)	fNewScrollSpeed = 3.5;
					else if(	fScrollSpeed < 4	)	fNewScrollSpeed = 4;
					else if(	fScrollSpeed < 5	)	fNewScrollSpeed = 5;
					else if(	fScrollSpeed < 6	)	fNewScrollSpeed = 6;
					else								fNewScrollSpeed = 8;
				}
				else if( DeviceI.button == KEY_END )
					fNewScrollSpeed = 4;
				else if( DeviceI.button == KEY_PGUP )
				{
					if(			fScrollSpeed > 4	)	fNewScrollSpeed = 4;
					else if(	fScrollSpeed > 2	)	fNewScrollSpeed = 2;
					else if(	fScrollSpeed > 1	)	fNewScrollSpeed = 1;
					else if(	fScrollSpeed > .5	)	fNewScrollSpeed = .5;
					else								fNewScrollSpeed = .25;
				}
				else if( DeviceI.button == KEY_UP )
				{
					if(			fScrollSpeed > 8	)	fNewScrollSpeed = 8;
					else if(	fScrollSpeed > 6	)	fNewScrollSpeed = 6;
					else if(	fScrollSpeed > 5	)	fNewScrollSpeed = 5;
					else if(	fScrollSpeed > 4	)	fNewScrollSpeed = 4;
					else if(	fScrollSpeed > 3.5	)	fNewScrollSpeed = 3.5;
					else if(	fScrollSpeed > 3	)	fNewScrollSpeed = 3;
					else if(	fScrollSpeed > 2.5	)	fNewScrollSpeed = 2.5;
					else if(	fScrollSpeed > 2	)	fNewScrollSpeed = 2;
					else if(	fScrollSpeed > 1.5	)	fNewScrollSpeed = 1.5;
					else if(	fScrollSpeed > 1	)	fNewScrollSpeed = 1;
					else if(	fScrollSpeed > .75	)	fNewScrollSpeed = .75;
					else if(	fScrollSpeed > .5	)	fNewScrollSpeed = .5;
					else								fNewScrollSpeed = .25;
				}

				if( fNewScrollSpeed != fScrollSpeed )
				{
					fScrollSpeed = fNewScrollSpeed;
					m_soundMarker.Play();
					GAMESTATE->StoreSelectedOptions();
				}
				break;
			}

			float fBeatsToMove=0.f;
			switch( DeviceI.button )
			{
			case KEY_UP:
			case KEY_DOWN:
				fBeatsToMove = NoteTypeToBeat( m_SnapDisplay.GetNoteType() );
				if( DeviceI.button == KEY_UP )
					fBeatsToMove *= -1;
				break;
			case KEY_PGUP:
			case KEY_PGDN:
				fBeatsToMove = fBEATS_PER_MEASURE;
				if( DeviceI.button == KEY_PGUP )
					fBeatsToMove *= -1;
				break;
			case KEY_HOME:
				GAMESTATE->m_fPlayerBeat[m_PlayerNumber] = 0;
				m_soundChangeLine.Play();
				break;
			case KEY_END:
				GAMESTATE->m_fPlayerBeat[m_PlayerNumber] = PREFSMAN->m_bSelectLastSecond ? truncf( m_pSteps->m_Timing.GetBeatFromElapsedTime( GAMESTATE->m_pCurSong->m_fMusicLengthSeconds ) ) : m_NoteFieldEdit.GetLastBeat();
				m_soundChangeLine.Play();
				break;
			}

			const float fStartBeat = GAMESTATE->m_fPlayerBeat[m_PlayerNumber];
			const float fEndBeat = GAMESTATE->m_fPlayerBeat[m_PlayerNumber] + fBeatsToMove;

			bool bPlacedHold = false;
			// check to see if they're holding a button
			for( int n=0; n<=9; n++ )	// for each number key
			{
				int iCol = n;

				// Alt + number = input to right half
				if( iKeyFlag & HOLDING_ALT )
					iCol += m_NoteFieldEdit.GetNumTracks()/2;

				if( iCol >= m_NoteFieldEdit.GetNumTracks() )
					continue;	// skip

				int b = n < 9 ? KEY_C1+n : KEY_C0;
				const DeviceInput di(DEVICE_KEYBOARD, b);

				if( !INPUTFILTER->IsBeingPressed(di) )
				 	continue;

				// create a new hold note
				HoldNote newHN( iCol, BeatToNoteRowRounded(min(fStartBeat, fEndBeat)), BeatToNoteRowRounded(max(fStartBeat, fEndBeat)));
				newHN.subtype = (iKeyFlag & HOLDING_SHIFT) != HOLDING_NONE ? HOLD_TYPE_ROLL : HOLD_TYPE_DANCE;
				newHN.playerNumber = (unsigned char)m_uCurrentPlayer;
				newHN.iStartRow = max(newHN.iStartRow, 0);
				newHN.iEndRow = max(newHN.iEndRow, 0);
				m_NoteFieldEdit.AddHoldNote( newHN );
				ResetAutoSave();
				bPlacedHold = true;
			}

			if( iKeyFlag & HOLDING_SHIFT )
			{
				if( PREFSMAN->m_bEditShiftSelector && !bPlacedHold )
				{
					/* Shift is being held.
					*
					* If this is the first time we've moved since shift was depressed,
					* the old position (before this move) becomes the start pos: */

					if(shiftAnchor == -1 )
						shiftAnchor = fStartBeat;

					if(fEndBeat == shiftAnchor)
					{
						/* We're back at the anchor, so we have nothing selected. */
						m_NoteFieldEdit.m_fBeginMarker = m_NoteFieldEdit.m_fEndMarker = -1;
					}
					else
					{
						m_NoteFieldEdit.m_fBeginMarker = shiftAnchor;
						m_NoteFieldEdit.m_fEndMarker = fEndBeat;
						if(m_NoteFieldEdit.m_fBeginMarker > m_NoteFieldEdit.m_fEndMarker)
							swap(m_NoteFieldEdit.m_fBeginMarker, m_NoteFieldEdit.m_fEndMarker);
					}
				}
			}

			GAMESTATE->m_fPlayerBeat[m_PlayerNumber] += fBeatsToMove;
			GAMESTATE->m_fPlayerBeat[m_PlayerNumber] = max( GAMESTATE->m_fPlayerBeat[m_PlayerNumber], 0 );
			GAMESTATE->m_fPlayerBeat[m_PlayerNumber] = froundf( GAMESTATE->m_fPlayerBeat[m_PlayerNumber], NoteTypeToBeat(m_SnapDisplay.GetNoteType()) );
			m_soundChangeLine.Play();
		}
		break;
	case KEY_LEFT:
		if( m_SnapDisplay.PrevSnapMode() )
			OnSnapModeChange();
		break;
	case KEY_RIGHT:
		if( m_SnapDisplay.NextSnapMode() )
			OnSnapModeChange();
		break;
	case KEY_SPACE:
		if( m_NoteFieldEdit.m_fBeginMarker==-1 && m_NoteFieldEdit.m_fEndMarker==-1 )
		{
			// lay begin marker
			m_NoteFieldEdit.m_fBeginMarker = GAMESTATE->m_fPlayerBeat[m_PlayerNumber];
		}
		else if( m_NoteFieldEdit.m_fEndMarker==-1 )	// only begin marker is laid
		{
			if( GAMESTATE->m_fPlayerBeat[m_PlayerNumber] == m_NoteFieldEdit.m_fBeginMarker )
			{
				m_NoteFieldEdit.m_fBeginMarker = -1;
			}
			else
			{
				m_NoteFieldEdit.m_fEndMarker = max( m_NoteFieldEdit.m_fBeginMarker, GAMESTATE->m_fPlayerBeat[m_PlayerNumber] );
				m_NoteFieldEdit.m_fBeginMarker = min( m_NoteFieldEdit.m_fBeginMarker, GAMESTATE->m_fPlayerBeat[m_PlayerNumber] );
			}
		}
		else	// both markers are laid
		{
			m_NoteFieldEdit.m_fBeginMarker = GAMESTATE->m_fPlayerBeat[m_PlayerNumber];
			m_NoteFieldEdit.m_fEndMarker = -1;
		}
		m_soundMarker.Play();
		break;
	case KEY_ENTER:
	case KEY_KP_ENTER:
		{
			// update enabled/disabled in g_AreaMenu
			bool bAreaSelected = m_NoteFieldEdit.m_fBeginMarker!=-1 && m_NoteFieldEdit.m_fEndMarker!=-1;
			g_AreaMenu.rows[cut].enabled = bAreaSelected;
			g_AreaMenu.rows[copy].enabled = bAreaSelected;
			g_AreaMenu.rows[paste_at_current_beat].enabled = this->m_Clipboard.GetLastBeat() != 0;
			g_AreaMenu.rows[paste_at_begin_marker].enabled = this->m_Clipboard.GetLastBeat() != 0 && m_NoteFieldEdit.m_fBeginMarker!=-1;
			g_AreaMenu.rows[clear].enabled = bAreaSelected;
			g_AreaMenu.rows[quantize].enabled = bAreaSelected;
			g_AreaMenu.rows[turn].enabled = bAreaSelected;
			g_AreaMenu.rows[transform].enabled = bAreaSelected;
			g_AreaMenu.rows[alter].enabled = bAreaSelected;
			g_AreaMenu.rows[tempo].enabled = bAreaSelected;
			g_AreaMenu.rows[play].enabled = bAreaSelected;
			g_AreaMenu.rows[record].enabled = bAreaSelected;
			g_AreaMenu.rows[convert_beat_to_pause].enabled = bAreaSelected;
			SCREENMAN->MiniMenu( &g_AreaMenu, SM_BackFromAreaMenu );
		}
		break;
	case KEY_ESC:
		g_MainMenu.rows[edit_notes_statistics].enabled = !m_bBGEditMode;
		g_MainMenu.rows[edit_bg_change].enabled = m_bBGEditMode;
		SCREENMAN->MiniMenu( &g_MainMenu, SM_BackFromMainMenu );
		break;

	case KEY_F1:
		SCREENMAN->MiniMenu( &g_KeyboardShortcuts, SM_None );
		break;
	case KEY_F3:
	{
		bool bDelay = false;

		float fBeat = GAMESTATE->m_fPlayerBeat[m_PlayerNumber],
			fBPM = ( m_bBGEditMode ? m_pSong->m_Timing.GetBPMAtBeat( fBeat ) : m_pSteps->m_Timing.GetBPMAtBeat( fBeat ) ),
			fStopSeconds = ( m_bBGEditMode ? m_pSong->m_Timing.GetStopAtBeat( fBeat, bDelay ) : m_pSteps->m_Timing.GetStopAtBeat( fBeat, bDelay ) );

		g_TimingMenu.rows[timing_mode].defaultChoice = (int)m_bBGEditMode;

		g_TimingMenu.rows[timing_bpm].choices.resize(1);	g_TimingMenu.rows[timing_bpm].choices[0]	= ssprintf("%.3f",fBPM);
		g_TimingMenu.rows[timing_stop].choices.resize(1);	g_TimingMenu.rows[timing_stop].choices[0]	= ssprintf("%.3f%s",fStopSeconds, bDelay ? "d" : "");

		// TODO: Rows per Beat
		// TODO: Beats per Measure

		g_TimingMenu.rows[timing_sample_start].choices.resize(1);	g_TimingMenu.rows[timing_sample_start].choices[0]	= ssprintf("%.3f",m_pSong->m_fMusicSampleStartSeconds);
		g_TimingMenu.rows[timing_sample_length].choices.resize(1);	g_TimingMenu.rows[timing_sample_length].choices[0]	= ssprintf("%.3f",m_pSong->m_fMusicSampleLengthSeconds);

		g_TimingMenu.rows[timing_offset].choices.resize(1);	g_TimingMenu.rows[timing_offset].choices[0]	= ssprintf("%.3f",m_bBGEditMode ? m_pSong->m_Timing.m_fBeat0Offset : m_pSteps->m_Timing.m_fBeat0Offset);

		g_TimingMenu.rows[timing_mult].enabled = g_TimingMenu.rows[timing_tick].enabled = g_TimingMenu.rows[timing_fake].enabled = g_TimingMenu.rows[timing_speed_area].enabled = g_TimingMenu.rows[timing_speed].enabled = !m_bBGEditMode;

		if( m_bBGEditMode )
		{
			g_TimingMenu.rows[timing_mult].defaultChoice = g_TimingMenu.rows[timing_tick].defaultChoice = g_TimingMenu.rows[timing_fake].defaultChoice = g_TimingMenu.rows[timing_speed_area].defaultChoice = g_TimingMenu.rows[timing_speed].defaultChoice = 0;
			g_TimingMenu.rows[timing_mult].choices[0] = g_TimingMenu.rows[timing_tick].choices[0] = g_TimingMenu.rows[timing_fake].choices[0] = g_TimingMenu.rows[timing_speed_area].choices[0] = g_TimingMenu.rows[timing_speed].choices[0] = "n/a";
		}
		else
		{
			MultiplierSegment& mlsg = m_pSteps->m_Timing.GetMultiplierSegmentAtBeat( fBeat );
			AreaSegment* fksg = m_pSteps->m_Timing.IsFakeAreaAtBeat( fBeat ) ? &m_pSteps->m_Timing.GetFakeSegmentAtBeat( fBeat ) : NULL;
			SpeedAreaSegment* sasg = m_pSteps->m_Timing.IsSpeedAreaAtBeat( fBeat ) ? &m_pSteps->m_Timing.GetSpeedAreaSegmentAtBeat( fBeat ) : NULL;
			SpeedSegment& spsg = m_pSteps->m_Timing.GetSpeedSegmentAtBeat( fBeat );
			
			g_TimingMenu.rows[timing_mult].choices.resize(1);
			g_TimingMenu.rows[timing_fake].choices.resize(1);
			g_TimingMenu.rows[timing_speed_area].choices.resize(1);
			g_TimingMenu.rows[timing_speed].choices.resize(1);

			g_TimingMenu.rows[timing_tick].choices[0]		= "0";
			g_TimingMenu.rows[timing_fake].choices[0]		= ssprintf("%.2f", fksg ? fksg->m_fBeats : 0.f);
			g_TimingMenu.rows[timing_speed_area].choices[0]	= ssprintf("%.2f", sasg ? sasg->m_fBeats : 0.f);
			g_TimingMenu.rows[timing_speed].choices[0]		= ssprintf (
																"%.3f%s%s",
																spsg.m_fToSpeed,
																(
																	spsg.m_fBeats < 0 ?
																	ssprintf(", %.3fs",spsg.m_fBeats*-1).c_str() :
																	ssprintf(", %.3f",spsg.m_fBeats).c_str()
																),
																spsg.m_fFactor == 1.f ? "" : ( spsg.m_fFactor > 1.f ? "-" : "+" )
															);
			g_TimingMenu.rows[timing_mult].choices[0]		= ssprintf(
																"C: %d%s, L: %.3f%s, S: %.3f%s",
																mlsg.m_uHit,
																mlsg.m_uHit != mlsg.m_uMiss ? "!" : "",
																mlsg.m_fLifeHit,
																mlsg.m_fLifeHit != mlsg.m_fLifeMiss ? "!" : "",
																mlsg.m_fScoreHit,
																mlsg.m_fScoreHit != mlsg.m_fScoreMiss ? "!" : ""
															);

			switch( m_pSteps->m_Timing.GetTickcountAtBeat( fBeat ) )
			{
			case 0:
				g_TimingMenu.rows[timing_tick].defaultChoice = 0;
				break;
			case 1:
				g_TimingMenu.rows[timing_tick].defaultChoice = 1;
				break;
			case 2:
				g_TimingMenu.rows[timing_tick].defaultChoice = 2;
				break;
			case 3:
				g_TimingMenu.rows[timing_tick].defaultChoice = 3;
				break;
			case 4:
				g_TimingMenu.rows[timing_tick].defaultChoice = 4;
				break;
			case 6:
				g_TimingMenu.rows[timing_tick].defaultChoice = 5;
				break;
			case 8:
				g_TimingMenu.rows[timing_tick].defaultChoice = 6;
				break;
			case 12:
				g_TimingMenu.rows[timing_tick].defaultChoice = 7;
				break;
			case 16:
				g_TimingMenu.rows[timing_tick].defaultChoice = 8;
				break;
			case 24:
				g_TimingMenu.rows[timing_tick].defaultChoice = 9;
				break;
			case 32:
				g_TimingMenu.rows[timing_tick].defaultChoice = 10;
				break;
			case 48:
				g_TimingMenu.rows[timing_tick].defaultChoice = 11;
				break;
			case 64:
				g_TimingMenu.rows[timing_tick].defaultChoice = 12;
				break;
			case 96:
				g_TimingMenu.rows[timing_tick].defaultChoice = 13;
				break;
			case 192:
				g_TimingMenu.rows[timing_tick].defaultChoice = 14;
				break;
			default:
				ASSERT(0);
			}
		}

		SCREENMAN->MiniMenu( &g_TimingMenu, SM_BackFromTimingMenu );
	}
		break;
	case KEY_F4:
		GAMESTATE->m_SongOptions.m_bAssistTick ^= 1;
		break;
	case KEY_F5:
	case KEY_F6:
		{
			// save current steps in memory
			Steps* pSteps = GAMESTATE->m_pCurSteps[m_PlayerNumber];
			ASSERT( pSteps );
			pSteps->SetNoteData( &m_NoteFieldEdit );
			ResetAutoSave();

			// Get all Steps of this StepsType
			StepsType st = pSteps->m_StepsType;
			vector<Steps*> vSteps;
			GAMESTATE->m_pCurSong->GetSteps( vSteps, st );

			// Find out what index the current Steps are
			vector<Steps*>::iterator it = find( vSteps.begin(), vSteps.end(), pSteps );
			ASSERT( it != vSteps.end() );

			switch( DeviceI.button )
			{
			case KEY_F5:
				if( it==vSteps.begin() )
				{
					SCREENMAN->PlayInvalidSound();
					return;
				}
				it--;
				break;
			case KEY_F6:
				it++;
				if( it==vSteps.end() )
				{
					SCREENMAN->PlayInvalidSound();
					return;
				}
				break;
			default:	ASSERT(0);	return;
			}

			pSteps = *it;
			GAMESTATE->m_pCurSteps[m_PlayerNumber] = m_pSteps = pSteps;
			pSteps->GetNoteData( &m_NoteFieldEdit );
			if( m_uNumPlayers != m_NoteFieldEdit.GetNumPlayers() )
			{
				bool bReload = m_uNumPlayers == 1 || m_NoteFieldEdit.GetNumPlayers() == 1;
				m_uNumPlayers = m_NoteFieldEdit.GetNumPlayers();
				m_uCurrentPlayer = min(m_uCurrentPlayer, m_uNumPlayers);
				if( bReload )
					m_NoteFieldEdit.Reload();
			}
			SCREENMAN->SystemMessage( ssprintf(
				"Switched to %s %s '%s'",
				StepsTypeToString( pSteps->m_StepsType ).c_str(),
				DifficultyToString( pSteps->GetDifficulty() ).c_str(),
				pSteps->GetDescription().c_str() ) );
			SOUND->PlayOnce( THEME->GetPathS(m_sName, "switch") );
		}
		break;
	case KEY_F7:
	case KEY_F8:
		{
			// MD 11/02/03 - start referring to Editor.ini entries
			// entries here: BPMDelta, BPMDeltaFine
			float fBPM = m_bBGEditMode ? m_pSong->m_Timing.GetBPMAtBeat( GAMESTATE->m_fPlayerBeat[m_PlayerNumber] ) : m_pSteps->m_Timing.GetBPMAtBeat( GAMESTATE->m_fPlayerBeat[m_PlayerNumber] );
			float fDeltaBPM;
			switch( DeviceI.button )
			{
			case KEY_F7:	fDeltaBPM = - 0.010f;		break;
			case KEY_F8:	fDeltaBPM = + 0.010f;		break;
			default:	ASSERT(0);						return;
			}
			if( iKeyFlag & HOLDING_SHIFT )
				fDeltaBPM *= 100;
			if( iKeyFlag & HOLDING_CTRL )
				fDeltaBPM *= 10;
			if( iKeyFlag & HOLDING_ALT )
				fDeltaBPM /= 10;
			else switch( type )
			{
			case IET_SLOW_REPEAT:	fDeltaBPM *= 10;	break;
			case IET_FAST_REPEAT:	fDeltaBPM *= 40;	break;
			}

			if( m_bBGEditMode )
				m_pSong->m_Timing.SetBPMAtBeat( GAMESTATE->m_fPlayerBeat[m_PlayerNumber], fBPM + fDeltaBPM );
			else
				m_pSteps->m_Timing.SetBPMAtBeat( GAMESTATE->m_fPlayerBeat[m_PlayerNumber], fBPM + fDeltaBPM );

			ResetAutoSave();
		}
		break;
	case KEY_F9:
	case KEY_F10:
		{
			// MD 11/02/03 - start referring to Editor.ini entries
			// entries here: StopDelta, StopDeltaFine
			float fStop = m_bBGEditMode ? m_pSong->m_Timing.GetStopLengthAtBeat( GAMESTATE->m_fPlayerBeat[m_PlayerNumber] ) : m_pSteps->m_Timing.GetStopLengthAtBeat( GAMESTATE->m_fPlayerBeat[m_PlayerNumber] );
			float fStopDelta;
			switch( DeviceI.button )
			{
			case KEY_F9:	fStopDelta = -0.010f;		break;
			case KEY_F10:	fStopDelta = +0.010f;		break;
			default:	ASSERT(0);						return;
			}
			if( iKeyFlag & HOLDING_CTRL )
				fStopDelta *= 10;
			if( iKeyFlag & HOLDING_ALT )
				fStopDelta /= 10;
			switch( type )
			{
			case IET_SLOW_REPEAT:	fStopDelta *= 10;	break;
			case IET_FAST_REPEAT:	fStopDelta *= 40;	break;
			}

			if( m_bBGEditMode )
				m_pSong->m_Timing.SetStopAtBeat( GAMESTATE->m_fPlayerBeat[m_PlayerNumber], fStop + fStopDelta, (iKeyFlag & HOLDING_SHIFT) != HOLDING_NONE );
			else
				m_pSteps->m_Timing.SetStopAtBeat( GAMESTATE->m_fPlayerBeat[m_PlayerNumber], fStop + fStopDelta, (iKeyFlag & HOLDING_SHIFT) != HOLDING_NONE );

			ResetAutoSave();
		}
		break;
	case KEY_F11:
	case KEY_F12:
		{
			// MD 11/02/03 - start referring to Editor.ini entries
			// entries here: OffsetDelta, OffsetDeltaFine
			float fOffsetDelta = 0;
			switch( DeviceI.button )
			{
			case KEY_F11:	fOffsetDelta -= 0.010f;		break;
			case KEY_F12:	fOffsetDelta += 0.010f;		break;
			default:	ASSERT(0);						return;
			}
			if( iKeyFlag & HOLDING_SHIFT )
				fOffsetDelta *= 100;
			if( iKeyFlag & HOLDING_CTRL )
				fOffsetDelta *= 10;
			if( iKeyFlag & HOLDING_ALT )
				fOffsetDelta /= 10;
			switch( type )
			{
			case IET_SLOW_REPEAT:	fOffsetDelta *= 10;	break;
			case IET_FAST_REPEAT:	fOffsetDelta *= 40;	break;
			}

			m_pSteps->m_Timing.m_fBeat0Offset += fOffsetDelta;
			ResetAutoSave();
		}
		break;
	case KEY_COMMA:
	case KEY_PERIOD:
		if( !m_bBGEditMode )
		{
			int iTickcount = m_pSteps->m_Timing.GetTickcountAtBeat( GAMESTATE->m_fPlayerBeat[m_PlayerNumber] );
			int iDeltaTick = 0;
			switch( DeviceI.button )
			{
			case KEY_COMMA:		iDeltaTick = -1;		break;
			case KEY_PERIOD:	iDeltaTick = +1;		break;
			default:	ASSERT(0);						return;
			}

			// Ugly code time! Yay!
			int iNewTickcount = iTickcount;

			if( iDeltaTick == +1 )
			{
				switch( iTickcount )
				{
				case 0:		iNewTickcount = 1;		break;
				case 1:		iNewTickcount = 2;		break;
				case 2:		iNewTickcount = 3;		break;
				case 3:		iNewTickcount = 4;		break;
				case 4:		iNewTickcount = 6;		break;
				case 6:		iNewTickcount = 8;		break;
				case 8:		iNewTickcount = 12;		break;
				case 12:	iNewTickcount = 16;		break;
				case 16:	iNewTickcount = 24;		break;
				case 24:	iNewTickcount = 32;		break;
				case 32:	iNewTickcount = 48;		break;
				case 48:	iNewTickcount = 64;		break;
				case 64:	iNewTickcount = 96;		break;
				case 96:	iNewTickcount = 192;	break;
				default:	break;
				}
			}
			else if( iDeltaTick == -1 )
			{
				switch( iTickcount )
				{
				case 1:		iNewTickcount = 0;		break;
				case 2:		iNewTickcount = 1;		break;
				case 3:		iNewTickcount = 2;		break;
				case 4:		iNewTickcount = 3;		break;
				case 6:		iNewTickcount = 4;		break;
				case 8:		iNewTickcount = 6;		break;
				case 12:	iNewTickcount = 8;		break;
				case 16:	iNewTickcount = 12;		break;
				case 24:	iNewTickcount = 16;		break;
				case 32:	iNewTickcount = 24;		break;
				case 48:	iNewTickcount = 32;		break;
				case 64:	iNewTickcount = 48;		break;
				case 96:	iNewTickcount = 64;		break;
				case 192:	iNewTickcount = 96;		break;
				default:	break;
				}
			}

			// No negative tickcounts, and no tickcounts over the maximum number of rows per beat!
			if( iNewTickcount != iTickcount )
				m_pSteps->m_Timing.SetTickcountAtBeat( GAMESTATE->m_fPlayerBeat[m_PlayerNumber], iNewTickcount );

			ResetAutoSave();
		}
		break;
	case KEY_LBRACKET:
	case KEY_RBRACKET:
		{
			// MD 11/02/03 - start referring to Editor.ini entries
			// entries here: SampleLengthDelta, SampleLengthDeltaFine
			//				 SampleStartDelta, SampleStartDeltaFine
			float fDelta;
			switch( DeviceI.button )
			{
			case KEY_LBRACKET:		fDelta = -0.01f;	break;
			case KEY_RBRACKET:		fDelta = +0.01f;	break;
			default:	ASSERT(0);						return;
			}
			if( iKeyFlag & HOLDING_CTRL )
				fDelta *= 10;
			if( iKeyFlag & HOLDING_ALT )
				fDelta /= 10;
			switch( type )
			{
			case IET_SLOW_REPEAT:	fDelta *= 10;	break;
			case IET_FAST_REPEAT:	fDelta *= 40;	break;
			}

			if( iKeyFlag & HOLDING_SHIFT )
			{
				m_pSong->m_fMusicSampleLengthSeconds += fDelta;
				m_pSong->m_fMusicSampleLengthSeconds = max(m_pSong->m_fMusicSampleLengthSeconds,0);
			}
			else
			{
				m_pSong->m_fMusicSampleStartSeconds += fDelta;
				m_pSong->m_fMusicSampleStartSeconds = max(m_pSong->m_fMusicSampleStartSeconds,0);
			}

			ResetAutoSave();
		}
		break;
	case KEY_Cm:
		PlayPreviewMusic();
		break;
	case KEY_Cb:
		HandleMainMenuChoice( edit_bg_change, NULL );
		break;
	case KEY_Cc:
	{
		g_CourseMode.rows[0].choices.clear();
		g_CourseMode.rows[0].choices.push_back( "OFF" );
		g_CourseMode.rows[0].defaultChoice = 0;

		vector<Course*> courses;
		SONGMAN->GetAllCourses( courses, false );
		for( unsigned i = 0; i < courses.size(); ++i )
		{
			const Course *crs = courses[i];

			bool bUsesThisSong = false;
			for( unsigned e = 0; e < crs->m_entries.size(); ++e )
			{
				if( crs->m_entries[e].type != COURSE_ENTRY_FIXED )
					continue;
				if( crs->m_entries[e].pSong != m_pSong )
					continue;
				bUsesThisSong = true;
			}

			if( bUsesThisSong )
			{
				g_CourseMode.rows[0].choices.push_back( crs->GetFullDisplayTitle() );
				if( crs == m_pAttacksFromCourse )
					g_CourseMode.rows[0].defaultChoice = g_CourseMode.rows[0].choices.size()-1;
			}
		}

		SCREENMAN->MiniMenu( &g_CourseMode, SM_BackFromCourseModeMenu );
		break;
	}
	case KEY_Cp:
		{
			if( iKeyFlag & HOLDING_CTRL )
				HandleMainMenuChoice( play_whole_song, NULL );
			else if( iKeyFlag & HOLDING_SHIFT )
				HandleMainMenuChoice( play_current_beat_to_end, NULL );
			else if( m_NoteFieldEdit.m_fBeginMarker!=-1 && m_NoteFieldEdit.m_fEndMarker!=-1 )
				HandleAreaMenuChoice( play, NULL );
		}
		break;
	case KEY_Cr:
		{
			if( iKeyFlag & HOLDING_CTRL )
				if( m_NoteFieldEdit.m_fBeginMarker!=-1 && m_NoteFieldEdit.m_fEndMarker!=-1 )
					HandleAreaMenuChoice( record, NULL );
		}
		break;
	case KEY_INSERT:
			if( iKeyFlag & HOLDING_CTRL )
				HandleAreaMenuChoice( shift_pauses_forward, NULL );
			else
				HandleAreaMenuChoice( insert_and_shift, NULL );
			SCREENMAN->PlayInvalidSound();
		break;
	case KEY_DEL:
			if( iKeyFlag & HOLDING_CTRL )
				HandleAreaMenuChoice( shift_pauses_backward, NULL );
			else
				HandleAreaMenuChoice( delete_and_shift, NULL );
			SCREENMAN->PlayInvalidSound();
		break;
	}
}

void ScreenEdit::InputRecord( const DeviceInput& DeviceI, const InputEventType type, const GameInput &GameI, const MenuInput &MenuI, const StyleInput &StyleI )
{
	if( DeviceI.device == DEVICE_KEYBOARD  &&  DeviceI.button == KEY_ESC )
	{
		TransitionFromRecordToEdit();
		return;
	}

	if( StyleI.player != m_PlayerNumber )
		return;		// ignore

	const int iCol = StyleI.col;

	switch( type )
	{
	case IET_FIRST_PRESS:
		{
			// Add a tap

			float fBeat = GAMESTATE->m_fPlayerBeat[m_PlayerNumber];
			fBeat = froundf( fBeat, NoteTypeToBeat(m_SnapDisplay.GetNoteType()) );

			const int iRow = BeatToNoteRowRounded( fBeat );
			if( iRow < 0 )
				break;

			m_NoteFieldRecord.SetTapNote(iCol, iRow, TAP_ORIGINAL_TAP);
			m_NoteFieldRecord.Step( iCol, TNS_MARVELOUS );
		}
		break;
	case IET_SLOW_REPEAT:
	case IET_FAST_REPEAT:
	case IET_RELEASE:
		// don't add or extend holds and rolls here
		break;
	}
}

void ScreenEdit::InputPlay( const DeviceInput& DeviceI, const InputEventType type, const GameInput &GameI, const MenuInput &MenuI, const StyleInput &StyleI )
{
	// We need to be checking for both first presses and releases here, not just first presses. Otherwise, lift
	// notes will not function at all in the test playback.
	//if ( type != IET_FIRST_PRESS )
	if ( type != IET_RELEASE && type != IET_FIRST_PRESS )
		return;

	if( DeviceI.device == DEVICE_KEYBOARD )
	{
		switch( DeviceI.button )
		{
		case KEY_ESC:
			if( type == IET_FIRST_PRESS )
			{
				TransitionToEdit();
			}
			break;
		case KEY_F4:
			if( type == IET_FIRST_PRESS )
			{
				GAMESTATE->m_SongOptions.m_bAssistTick ^= 1;
			}
			break;
		case KEY_F8:
			if( type == IET_FIRST_PRESS )
			{
				PREFSMAN->m_bAutoPlay = !PREFSMAN->m_bAutoPlay;
				FOREACH_PlayerNumber( p )
					if( GAMESTATE->IsHumanPlayer(p) )
						GAMESTATE->m_PlayerController[p] = PREFSMAN->m_bAutoPlay?PC_AUTOPLAY:PC_HUMAN;
			}
			break;
		case KEY_F11:
		case KEY_F12:
		{
			// MD 11/02/03 - start referring to Editor.ini entries
			// entries here: OffsetDelta, OffsetDeltaFine
			float fOffsetDelta = 0;
			switch( DeviceI.button )
			{
			case KEY_F11:	fOffsetDelta -= 0.010f;		break;
			case KEY_F12:	fOffsetDelta += 0.010f;		break;
			default:	ASSERT(0);						return;
			}
			const int iKeyFlag = INPUTFILTER->GetModifierKeyFlag();
			if( iKeyFlag & HOLDING_SHIFT )
				fOffsetDelta *= 100;
			if( iKeyFlag & HOLDING_CTRL )
				fOffsetDelta *= 10;
			if( iKeyFlag & HOLDING_ALT )
				fOffsetDelta /= 10;
			switch( type )
			{
			case IET_SLOW_REPEAT:	fOffsetDelta *= 10;	break;
			case IET_FAST_REPEAT:	fOffsetDelta *= 40;	break;
			}

			if( m_bBGEditMode )
				m_pSong->m_Timing.m_fBeat0Offset += fOffsetDelta;
			else
				m_pSteps->m_Timing.m_fBeat0Offset += fOffsetDelta;
		}
		break;
		}
	}

	if( StyleI.player == m_PlayerNumber )
	{
		if( !PREFSMAN->m_bAutoPlay )
		{
			if (type == IET_RELEASE)
				m_Player.Release( StyleI.col, DeviceI.ts );
			else
				m_Player.Step( StyleI.col, DeviceI.ts );
		}
	}

}

bool ScreenEdit::InputNote( const int iCol, const int iKeyFlag )
{
	if( iCol >= m_NoteFieldEdit.GetNumTracks() )	// this button is in the range of columns for this Style
		return false;

	ResetAutoSave();

	const float fPlayerBeat = GAMESTATE->m_fPlayerBeat[m_PlayerNumber];
	const int iPlayerRow = BeatToNoteRowRounded( fPlayerBeat );

	// check for to see if the user intended to remove a HoldNote
	for( int i=0; i<m_NoteFieldEdit.GetNumHoldsAndRolls(); i++ )	// for each HoldNote
	{
		const HoldNote &hn = m_NoteFieldEdit.GetHoldNote(i);
		if( iCol == hn.iTrack  &&		// the notes correspond
			hn.RowIsInRange(iPlayerRow) )	// the cursor lies within this HoldNote
		{
			if( iKeyFlag & HOLDING_MENU )
			{
				m_iCurrentHold = i;
				SCREENMAN->MiniMenu( &g_HoldRoll, SM_BackFromHoldRoll );
			}
			else if( iKeyFlag & HOLDING_CTRL )
			{
				HoldNote &hn = m_NoteFieldEdit.GetHoldNote(i);
				if( ++hn.playerNumber > m_NoteFieldEdit.GetNumPlayers() )
					hn.playerNumber = (unsigned char)1;
				m_uCurrentPlayer = hn.playerNumber;
			}
			else if( iKeyFlag & HOLDING_SHIFT )
			{
				HoldNote &hn = m_NoteFieldEdit.GetHoldNote(i);
				hn.subtype = hn.subtype == HOLD_TYPE_DANCE ? HOLD_TYPE_ROLL : HOLD_TYPE_DANCE;
			}
			else
				m_NoteFieldEdit.RemoveHoldNote( i );

			return true;
		}
	}

	// Note Menu
	if( iKeyFlag & HOLDING_MENU )
	{
		m_uCurrentCol = iCol;
		m_uCurrentPlayerRow = iPlayerRow;
		SCREENMAN->MiniMenu( &g_InsertNotes, SM_BackFromInsertNotes );
	}

	else
	{
		TapNote tn = m_NoteFieldEdit.GetTapNote(iCol, iPlayerRow);

		// Hold LShift to swap between notes
		if( iKeyFlag & HOLDING_LSHIFT )
		{
			switch( tn.drawType )
			{
			case TapNote::mine:		tn.drawType = tn.type = TapNote::shock;					break;
			case TapNote::shock:	tn.drawType = tn.type = TapNote::potion;				break;
			case TapNote::potion:	tn.drawType = tn.type = TapNote::lift;					break;
			case TapNote::lift:		tn.drawType = tn.type = TapNote::hidden;				break;
			case TapNote::hidden:	tn.drawType = tn.type = TapNote::mine;					break;
			default:				tn.drawType = tn.type = (unsigned char)m_uCurrentItem;	break;
			}
			m_uCurrentItem = tn.type;
			m_NoteFieldEdit.SetTapNote( iCol, iPlayerRow, tn );
		}

		// Hold RShift to swap between notes
		else if( iKeyFlag & HOLDING_RSHIFT )
		{
			switch( tn.drawType )
			{
			case TapNote::mine:		tn.drawType = tn.type = TapNote::hidden;				break;
			case TapNote::shock:	tn.drawType = tn.type = TapNote::mine;					break;
			case TapNote::potion:	tn.drawType = tn.type = TapNote::shock;					break;
			case TapNote::lift:		tn.drawType = tn.type = TapNote::potion;				break;
			case TapNote::hidden:	tn.drawType = tn.type = TapNote::lift;					break;
			default:				tn.drawType = tn.type = (unsigned char)m_uCurrentItem;	break;
			}
			m_uCurrentItem = tn.type;
			m_NoteFieldEdit.SetTapNote( iCol, iPlayerRow, tn );
		}

		else if( tn.type == TapNote::tap )
		{
			if( iKeyFlag & HOLDING_CTRL )
			{
				if( iKeyFlag & HOLDING_LCTRL )
				{
					if( ++tn.playerNumber > m_NoteFieldEdit.GetNumPlayers() )
						tn.playerNumber = (unsigned char)1;
				}
				else if( iKeyFlag & HOLDING_RCTRL )
				{
					if( --tn.playerNumber < 1 )
						tn.playerNumber = (unsigned char)m_NoteFieldEdit.GetNumPlayers();
				}

				m_uCurrentPlayer = tn.playerNumber;
				m_NoteFieldEdit.SetTapNote( iCol, iPlayerRow, tn );
			}
			else
				m_NoteFieldEdit.SetTapNote( iCol, iPlayerRow, TAP_EMPTY );
		}

		else if( tn.type == TapNote::empty )
		{
			TapNote tn = TAP_ORIGINAL_TAP;
			tn.playerNumber = (unsigned char)m_uCurrentPlayer;
			m_NoteFieldEdit.SetTapNote( iCol, iPlayerRow, tn );
		}

		else
			m_NoteFieldEdit.SetTapNote( iCol, iPlayerRow, TAP_EMPTY );
	}

	return true;
}

// Switch to editing.
void ScreenEdit::TransitionToEdit()
{
	/* Important: people will stop playing, change the BG and start again; make sure we reload */
	m_Background.Unload();
	m_Foreground.Unload();

	GAMESTATE->m_EditMode = m_bBGEditMode ? GAMESTATE->MODE_BACKGROUND : GAMESTATE->MODE_EDITING;
	GAMESTATE->m_bPlaying = false;
	GAMESTATE->m_bPastHereWeGo = false;
	m_soundMusic.StopPlaying();
	m_soundAssistTick.StopPlaying(); /* Stop any queued assist ticks. */
	m_rectRecordBack.StopTweening();
	m_rectRecordBack.BeginTweening( 0.5f );
	m_rectRecordBack.SetDiffuse( RageColor(0,0,0,0) );

	/* Make sure we're snapped. */
	GAMESTATE->m_fPlayerBeat[m_PlayerNumber] = froundf( GAMESTATE->m_fPlayerBeat[m_PlayerNumber], NoteTypeToBeat(m_SnapDisplay.GetNoteType()) );

	/* Playing and recording have lead-ins, which may start before beat 0;
	 * make sure we don't stay there if we escaped out early. */
	GAMESTATE->m_fPlayerBeat[m_PlayerNumber] = max( GAMESTATE->m_fPlayerBeat[m_PlayerNumber], 0 );

	/* Stop displaying course attacks, if any. */
	GAMESTATE->RemoveAllActiveAttacks();
	GAMESTATE->RebuildPlayerOptionsFromActiveAttacks( m_PlayerNumber );
	GAMESTATE->m_CurrentPlayerOptions[m_PlayerNumber] = GAMESTATE->m_PlayerOptions[m_PlayerNumber];

	if( PREFSMAN->m_bEditorCombo )
		SAFE_DELETE( m_ScoreKeeper );
}

void ScreenEdit::TransitionFromRecordToEdit()
{
	TransitionToEdit();
	ResetAutoSave();

	int iNoteIndexBegin = BeatToNoteRowRounded( m_NoteFieldEdit.m_fBeginMarker );
	int iNoteIndexEnd = BeatToNoteRowRounded( m_NoteFieldEdit.m_fEndMarker );

	// delete old TapNotes in the range
	m_NoteFieldEdit.ClearRange( iNoteIndexBegin, iNoteIndexEnd );
	m_NoteFieldEdit.CopyRange( &m_NoteFieldRecord, iNoteIndexBegin, iNoteIndexEnd, iNoteIndexBegin );
}

/* Helper for SM_DoReloadFromDisk */
static bool g_DoReload;
void ReloadFromDisk( void *p )
{
	g_DoReload = true;
}

void ScreenEdit::HandleScreenMessage( const ScreenMessage SM )
{
	switch( SM )
	{
	case SM_GoToNextScreen:
		// Reload song from disk to discard changes.
		SONGMAN->RevertFromDisk( GAMESTATE->m_pCurSong, true );

		/* We might do something with m_pSteps (eg. UpdateTextInfo) before we end up
		 * in ScreenEditMenu, and m_pSteps might be invalid due to RevertFromDisk. */
		m_pSteps = GAMESTATE->m_pCurSteps[m_PlayerNumber];

		SCREENMAN->SetNewScreen( "ScreenEditMenu" );
		break;
	case SM_MenuOperator:
		SCREENMAN->SetNewScreen( INITIAL_SCREEN );
		break;
	case SM_BackFromMainMenu:
		HandleMainMenuChoice( (MainMenuChoice)ScreenMiniMenu::s_iLastLine, ScreenMiniMenu::s_iLastAnswers );
		break;
	case SM_BackFromAreaMenu:
		HandleAreaMenuChoice( (AreaMenuChoice)ScreenMiniMenu::s_iLastLine, ScreenMiniMenu::s_iLastAnswers );
		break;
	case SM_BackFromEditNotesStatistics:
		HandleEditNotesStatisticsChoice( (EditNotesStatisticsChoice)ScreenMiniMenu::s_iLastLine, ScreenMiniMenu::s_iLastAnswers );
		break;
	case SM_BackFromEditSongInfo:
		HandleEditSongInfoChoice( (EditSongInfoChoice)ScreenMiniMenu::s_iLastLine, ScreenMiniMenu::s_iLastAnswers );
		break;
	case SM_BackFromBGChange:
		HandleBGChangeChoice( (BGChangeChoice)ScreenMiniMenu::s_iLastLine, ScreenMiniMenu::s_iLastAnswers );
		break;
	case SM_BackFromPrefs:
		PREFSMAN->m_bEditorShowBGChangesPlay = !!ScreenMiniMenu::s_iLastAnswers[pref_show_bgs_play];
		PREFSMAN->m_bEditShiftSelector = !!ScreenMiniMenu::s_iLastAnswers[pref_shift_seelctor];
		PREFSMAN->m_bShowAllStyles = !!ScreenMiniMenu::s_iLastAnswers[pref_show_all_styles];
		PREFSMAN->m_bLastEditedSong = !!ScreenMiniMenu::s_iLastAnswers[pref_save_last_song];
		PREFSMAN->m_bLastEditedDifficulty = !!ScreenMiniMenu::s_iLastAnswers[pref_save_last_diff];
		PREFSMAN->m_bEditorSmoothScrolling = !!ScreenMiniMenu::s_iLastAnswers[pref_smooth_scroll];
		PREFSMAN->m_bGameplayKeysInEditor = !!ScreenMiniMenu::s_iLastAnswers[pref_gameplay_keys];
		PREFSMAN->m_bEditorCombo = !!ScreenMiniMenu::s_iLastAnswers[pref_show_combo];
		PREFSMAN->m_bShowSnapBeats = !!ScreenMiniMenu::s_iLastAnswers[pref_snap_beats];
		PREFSMAN->m_bTimingAsRows = !!ScreenMiniMenu::s_iLastAnswers[pref_timing_in_rows];
		PREFSMAN->m_bSelectLastSecond = !!ScreenMiniMenu::s_iLastAnswers[pref_highlight_end];

		PREFSMAN->SaveGlobalPrefsToDisk();
		SCREENMAN->SystemMessage( "Preferences saved." );
		break;
	case SM_BackFromCourseModeMenu:
	{
		const int num = ScreenMiniMenu::s_iLastAnswers[0];
		m_pAttacksFromCourse = NULL;
		if( num != 0 )
		{
			const CString name = g_CourseMode.rows[0].choices[num];
			m_pAttacksFromCourse = SONGMAN->FindCourse( name );
			ASSERT( m_pAttacksFromCourse );
		}
		break;
	}
	case SM_BackFromPlayerOptions:
	case SM_BackFromSongOptions:
		// coming back from PlayerOptions or SongOptions
		GAMESTATE->StoreSelectedOptions();

		// stop any music that screen may have been playing
		SOUND->StopMusic();

		break;
	case SM_BackFromInsertAttack:
		{
			int iDurationChoice = ScreenMiniMenu::s_iLastAnswers[0];
			g_fLastInsertAttackDurationSeconds = strtof( g_InsertAttackItems[0].choices[iDurationChoice], NULL );
			GAMESTATE->StoreSelectedOptions();	// save so that we don't lose the options chosen for edit and playback
			SCREENMAN->AddNewScreenToTop( "ScreenPlayerOptions", SM_BackFromInsertAttackModifiers );
		}
		break;
	case SM_BackFromInsertAttackModifiers:
		{
			/* PlayerOptions poChosen = GAMESTATE->m_PlayerOptions[m_PlayerNumber];
			CString sMods = poChosen.GetString();
			const int iSongIndex = BeatToNoteRowRounded( GAMESTATE->m_fPlayerBeat[m_PlayerNumber] );

			Attack attack;
			attack.level = ATTACK_LEVEL_1;	// does this matter?
			attack.fSecsRemaining = g_fLastInsertAttackDurationSeconds;
			attack.sModifier = sMods;

			m_NoteFieldEdit.SetTapAttackNote( g_iLastInsertAttackTrack, iSongIndex, attack ); */
			GAMESTATE->RestoreSelectedOptions();	// restore the edit and playback options
		}
		break;
	case SM_DoReloadFromDisk:
	{
		if( !g_DoReload )
			return;

		const StepsType st = m_pSteps->m_StepsType;
		StepsID id;
		id.FromSteps( m_pSteps );

		GAMESTATE->m_pCurSteps[m_PlayerNumber] = NULL; /* make RevertFromDisk not try to reset it */
		SONGMAN->RevertFromDisk( GAMESTATE->m_pCurSong );

		CString sMessage = "Reloaded from disk.";
		Steps *pSteps = id.ToSteps( GAMESTATE->m_pCurSong, true );

		// Don't allow an autogen match.  This can't be what they chose to
		// edit originally because autogen steps are hidden.
		if( pSteps && pSteps->IsAutogen() )
			pSteps = NULL;

		/* If we couldn't find the steps we were on before, warn and use the first available. */
		if( pSteps == NULL )
		{
			pSteps = GAMESTATE->m_pCurSong->GetStepsByDifficulty( st, DIFFICULTY_INVALID, false );

			if( pSteps )
				sMessage = ssprintf( "old steps not found; changed to %s.",
					DifficultyToString(pSteps->GetDifficulty()).c_str() );
		}

		/* If we still couldn't find any steps, then all steps of the current StepsType
		 * were removed.  Don't create them; only do that in EditMenu. */
		if( pSteps == NULL )
		{
			SCREENMAN->SetNewScreen( "ScreenEditMenu" );
			return;
		}

		SCREENMAN->SystemMessage( sMessage );

		m_pSteps = GAMESTATE->m_pCurSteps[m_PlayerNumber] = pSteps;
		m_pSteps->GetNoteData( &m_NoteFieldEdit );
		ArrowEffects::Init(m_PlayerNumber);
		m_bCanAutoSave = false;
		break;
	}
	case SM_DoUpdateTextInfo:
		this->PostScreenMessage( SM_DoUpdateTextInfo, 0.5f );
		UpdateTextInfo();
		break;

	case SM_GainFocus:
		/* We do this ourself. */
		SOUND->HandleSongTimer( false );

		/* When another screen comes up, RageSounds takes over the sound timer.  When we come
		 * back, put the timer back to where it was. */
		GAMESTATE->m_fPlayerBeat[m_PlayerNumber] = m_fTrailingBeat;
		break;
	case SM_LoseFocus:
		/* Snap the trailing beat, in case we lose focus while tweening. */
		m_fTrailingBeat = GAMESTATE->m_fPlayerBeat[m_PlayerNumber];
		break;
	case SM_BackFromTimingMenu:
		HandleTimingMenuChoice( (TimingMenuChoice)ScreenMiniMenu::s_iLastLine, ScreenMiniMenu::s_iLastAnswers );
		break;
	case SM_BackFromInsertNotes:
		HandleInsertNotesChoice( (InsertNotesChoice)ScreenMiniMenu::s_iLastLine, ScreenMiniMenu::s_iLastAnswers );
		break;
	case SM_BackFromHoldRoll:
		HandleHoldRollChoice( (HoldRollChoice)ScreenMiniMenu::s_iLastLine, ScreenMiniMenu::s_iLastAnswers );
		break;
	}
}

void ScreenEdit::OnSnapModeChange()
{
	m_soundChangeSnap.Play();

	NoteType nt = m_SnapDisplay.GetNoteType();
	int iStepIndex = BeatToNoteRowRounded( GAMESTATE->m_fPlayerBeat[m_PlayerNumber] );
	int iElementsPerNoteType = BeatToNoteRowRounded( NoteTypeToBeat(nt) );
	int iStepIndexHangover = iStepIndex % iElementsPerNoteType;
	GAMESTATE->m_fPlayerBeat[m_PlayerNumber] -= NoteRowToBeat( iStepIndexHangover );
}

// Helper function for below

// Begin helper functions for InputEdit

void ChangeDescription( CString sNew )
{
	Steps* pSteps = GAMESTATE->m_pCurSteps[GAMESTATE->m_MasterPlayerNumber];
	pSteps->SetDescription(sNew);
}

void ChangeMainTitle( CString sNew )
{
	Song* pSong = GAMESTATE->m_pCurSong;
	pSong->m_sMainTitle = sNew;
}

void ChangeSubTitle( CString sNew )
{
	Song* pSong = GAMESTATE->m_pCurSong;
	pSong->m_sSubTitle = sNew;
}

void ChangeArtist( CString sNew )
{
	Song* pSong = GAMESTATE->m_pCurSong;
	pSong->m_sArtist = sNew;
}

void ChangeCredit( CString sNew )
{
	Steps* pSteps = GAMESTATE->m_pCurSteps[GAMESTATE->m_MasterPlayerNumber];
	pSteps->m_sCredit = sNew;
}

void ChangeGenre( CString sNew )
{
	Song* pSong = GAMESTATE->m_pCurSong;
	pSong->m_sGenre = sNew;
}

void ChangeMainTitleTranslit( CString sNew )
{
	Song* pSong = GAMESTATE->m_pCurSong;
	pSong->m_sMainTitleTranslit = sNew;
}

void ChangeSubTitleTranslit( CString sNew )
{
	Song* pSong = GAMESTATE->m_pCurSong;
	pSong->m_sSubTitleTranslit = sNew;
}

void ChangeArtistTranslit( CString sNew )
{
	Song* pSong = GAMESTATE->m_pCurSong;
	pSong->m_sArtistTranslit = sNew;
}


// End helper functions

void ScreenEdit::HandleMainMenuChoice( MainMenuChoice c, int* iAnswers )
{
	switch( c )
	{
		case edit_notes_statistics:
			{
				/* XXX: If the difficulty is changed from EDIT, and pSteps->WasLoadedFromProfile()
				 * is true, we should warn that the steps will no longer be saved to the profile. */
				Steps* pSteps = GAMESTATE->m_pCurSteps[m_PlayerNumber];
				float fMusicSeconds = m_soundMusic.GetLengthSeconds();

				g_EditNotesStatistics.rows[players].defaultChoice = clamp( (int)m_uNumPlayers-1, 0, 7 );
				g_EditNotesStatistics.rows[difficulty].defaultChoice = pSteps->GetDifficulty();
				g_EditNotesStatistics.rows[meter].defaultChoice = clamp( pSteps->GetMeter()-1, 0, 24 );

				g_EditNotesStatistics.rows[predict_meter].choices.resize(1);g_EditNotesStatistics.rows[predict_meter].choices[0] = ssprintf("%f",pSteps->PredictMeter());
				g_EditNotesStatistics.rows[description].choices.resize(1);	g_EditNotesStatistics.rows[description].choices[0] = pSteps->GetDescription();
				g_EditNotesStatistics.rows[tap_notes].choices.resize(1);	g_EditNotesStatistics.rows[tap_notes].choices[0] = ssprintf("%d", m_NoteFieldEdit.GetNumTapNotes());
				g_EditNotesStatistics.rows[hold_notes].choices.resize(1);	g_EditNotesStatistics.rows[hold_notes].choices[0] = ssprintf("%d", m_NoteFieldEdit.GetNumHoldNotes());
				g_EditNotesStatistics.rows[roll_notes].choices.resize(1);	g_EditNotesStatistics.rows[roll_notes].choices[0] = ssprintf("%d", m_NoteFieldEdit.GetNumRollNotes());

				g_EditNotesStatistics.rows[stream].choices.resize(1);		g_EditNotesStatistics.rows[stream].choices[0] = ssprintf("%f", NoteDataUtil::GetStreamRadarValue(m_NoteFieldEdit,fMusicSeconds));
				g_EditNotesStatistics.rows[voltage].choices.resize(1);		g_EditNotesStatistics.rows[voltage].choices[0] = ssprintf("%f", NoteDataUtil::GetVoltageRadarValue(m_NoteFieldEdit,fMusicSeconds));
				g_EditNotesStatistics.rows[air].choices.resize(1);			g_EditNotesStatistics.rows[air].choices[0] = ssprintf("%f", NoteDataUtil::GetAirRadarValue(m_NoteFieldEdit,fMusicSeconds));
				g_EditNotesStatistics.rows[freeze].choices.resize(1);		g_EditNotesStatistics.rows[freeze].choices[0] = ssprintf("%f", NoteDataUtil::GetFreezeRadarValue(m_NoteFieldEdit,fMusicSeconds));
				g_EditNotesStatistics.rows[chaos].choices.resize(1);		g_EditNotesStatistics.rows[chaos].choices[0] = ssprintf("%f", NoteDataUtil::GetChaosRadarValue(m_NoteFieldEdit,fMusicSeconds));

				SCREENMAN->MiniMenu( &g_EditNotesStatistics, SM_BackFromEditNotesStatistics );
			}
			break;
		case play_whole_song:
			{
				m_NoteFieldEdit.m_fBeginMarker = 0;
				m_NoteFieldEdit.m_fEndMarker = PREFSMAN->m_bSelectLastSecond ? truncf( m_pSteps->m_Timing.GetBeatFromElapsedTime( GAMESTATE->m_pCurSong->m_fMusicLengthSeconds ) ) : m_NoteFieldEdit.GetLastBeat();
				m_bLastBeat = true;
				HandleAreaMenuChoice( play, NULL );
			}
			break;
		case play_current_beat_to_end:
			{
				m_NoteFieldEdit.m_fBeginMarker = GAMESTATE->m_fPlayerBeat[m_PlayerNumber];
				m_NoteFieldEdit.m_fEndMarker = PREFSMAN->m_bSelectLastSecond ? truncf( m_pSteps->m_Timing.GetBeatFromElapsedTime( GAMESTATE->m_pCurSong->m_fMusicLengthSeconds ) ) : m_NoteFieldEdit.GetLastBeat();
				m_bLastBeat = true;
				HandleAreaMenuChoice( play, NULL );
			}
			break;
		case save:
			{
				// copy edit into current Steps
				Steps* pSteps = GAMESTATE->m_pCurSteps[m_PlayerNumber];
				ASSERT( pSteps );
				pSteps->SetNoteData( &m_NoteFieldEdit );
				GAMESTATE->m_pCurSong->TidyUpBeforeSave();
				GAMESTATE->m_pCurSong->Save();
				m_bCanAutoSave = false;

				// we shouldn't say we're saving a DWI if we're on any game besides
				// dance, it just looks tacky and people may be wondering where the
				// DWI file is :-)
				if ((int)pSteps->m_StepsType <= (int)STEPS_TYPE_DANCE_SOLO)
					SCREENMAN->SystemMessage( "Saved as SM and DWI." );
				else
					SCREENMAN->SystemMessage( "Saved as SM." );
				SOUND->PlayOnce( THEME->GetPathS(m_sName, "save") );
			}
			break;
		case reload:
			g_DoReload = false;
			SCREENMAN->Prompt( SM_DoReloadFromDisk,
				"Do you want to reload from disk?\n\nThis will destroy all changes.",
				true, false, ReloadFromDisk, NULL, NULL );
			break;
		case player_options:
			SCREENMAN->AddNewScreenToTop( "ScreenPlayerOptions", SM_BackFromPlayerOptions );
			break;
		case song_options:
			SCREENMAN->AddNewScreenToTop( "ScreenSongOptions", SM_BackFromSongOptions );
			break;
		case edit_song_info:
			{
				g_EditSongInfo.rows[main_title].choices.resize(1);					g_EditSongInfo.rows[main_title].choices[0] = m_pSong->m_sMainTitle;
				g_EditSongInfo.rows[sub_title].choices.resize(1);					g_EditSongInfo.rows[sub_title].choices[0] = m_pSong->m_sSubTitle;
				g_EditSongInfo.rows[artist].choices.resize(1);						g_EditSongInfo.rows[artist].choices[0] = m_pSong->m_sArtist;
				g_EditSongInfo.rows[credit].choices.resize(1);						g_EditSongInfo.rows[credit].choices[0] = m_pSteps->m_sCredit;
				g_EditSongInfo.rows[main_title_transliteration].choices.resize(1);	g_EditSongInfo.rows[main_title_transliteration].choices[0] = m_pSong->m_sMainTitleTranslit;
				g_EditSongInfo.rows[sub_title_transliteration].choices.resize(1);	g_EditSongInfo.rows[sub_title_transliteration].choices[0] = m_pSong->m_sSubTitleTranslit;
				g_EditSongInfo.rows[artist_transliteration].choices.resize(1);		g_EditSongInfo.rows[artist_transliteration].choices[0] = m_pSong->m_sArtistTranslit;

				SCREENMAN->MiniMenu( &g_EditSongInfo, SM_BackFromEditSongInfo );
			}
			break;
		case edit_bg_change:
			if( GAMESTATE->m_EditMode == GAMESTATE->MODE_BACKGROUND )
			{
				//
				// Fill in option names
				//
				g_BGChange.rows[add_song_bganimation].choices.clear();
				GetDirListing( m_pSong->GetSongDir() + "*", g_BGChange.rows[add_song_bganimation].choices, true );

				g_BGChange.rows[add_song_movie].choices.clear();
				GetVideoDirListing( m_pSong->GetSongDir() + "*", g_BGChange.rows[add_song_movie].choices );

				g_BGChange.rows[add_song_still].choices.clear();
				GetPictureDirListing( m_pSong->GetSongDir() + "*", g_BGChange.rows[add_song_still].choices );

				g_BGChange.rows[add_global_random_movie].choices.clear();
				GetVideoDirListing( RANDOMMOVIES_DIR + "*", g_BGChange.rows[add_global_random_movie].choices );

				g_BGChange.rows[add_global_bganimation].choices.clear();
				GetDirListing( BG_ANIMS_DIR + "*", g_BGChange.rows[add_global_bganimation].choices, true );

				g_BGChange.rows[add_global_visualization].choices.clear();
				GetVideoDirListing( VISUALIZATIONS_DIR + "*", g_BGChange.rows[add_global_visualization].choices );

				//
				// Fill in line enabled/disabled
				//
				bool bAlreadyBGChangeHere = false;
				BGChange bgChange;
				FOREACH( BGChange, m_pSong->m_BGChanges, bgc )
				{
					if( bgc->m_fBeat == GAMESTATE->m_fPlayerBeat[m_PlayerNumber] )
					{
						bAlreadyBGChangeHere = true;
						bgChange = *bgc;
					}
				}

				g_BGChange.rows[add_random].enabled = true;
				g_BGChange.rows[add_song_bganimation].enabled = g_BGChange.rows[add_song_bganimation].choices.size() > 0;
				g_BGChange.rows[add_song_movie].enabled = g_BGChange.rows[add_song_movie].choices.size() > 0;
				g_BGChange.rows[add_song_still].enabled = g_BGChange.rows[add_song_still].choices.size() > 0;
				g_BGChange.rows[add_global_random_movie].enabled = g_BGChange.rows[add_global_random_movie].choices.size() > 0;
				g_BGChange.rows[add_global_bganimation].enabled = g_BGChange.rows[add_global_bganimation].choices.size() > 0;
				g_BGChange.rows[add_global_visualization].enabled = g_BGChange.rows[add_global_visualization].choices.size() > 0;
				g_BGChange.rows[delete_change].enabled = bAlreadyBGChangeHere;


				// set default choices
				g_BGChange.rows[rate].						SetDefaultChoiceIfPresent( ssprintf("%2.0f%%",bgChange.m_fRate*100) );
				g_BGChange.rows[fade_last].defaultChoice	= bgChange.m_bFadeLast ? 1 : 0;
				g_BGChange.rows[rewind_movie].defaultChoice = bgChange.m_bRewindMovie ? 1 : 0;
				g_BGChange.rows[loop].defaultChoice			= bgChange.m_bLoop ? 1 : 0;
				g_BGChange.rows[add_song_bganimation].		SetDefaultChoiceIfPresent( bgChange.m_sBGName );
				g_BGChange.rows[add_song_movie].			SetDefaultChoiceIfPresent( bgChange.m_sBGName );
				g_BGChange.rows[add_song_still].			SetDefaultChoiceIfPresent( bgChange.m_sBGName );
				g_BGChange.rows[add_global_random_movie].	SetDefaultChoiceIfPresent( bgChange.m_sBGName );
				g_BGChange.rows[add_global_bganimation].	SetDefaultChoiceIfPresent( bgChange.m_sBGName );
				g_BGChange.rows[add_global_visualization].	SetDefaultChoiceIfPresent( bgChange.m_sBGName );


				SCREENMAN->MiniMenu( &g_BGChange, SM_BackFromBGChange );
			}
			break;
		case preferences:
			g_Prefs.rows[pref_show_bgs_play].defaultChoice = PREFSMAN->m_bEditorShowBGChangesPlay;
			g_Prefs.rows[pref_shift_seelctor].defaultChoice = PREFSMAN->m_bEditShiftSelector;
			g_Prefs.rows[pref_show_all_styles].defaultChoice = PREFSMAN->m_bShowAllStyles;
			g_Prefs.rows[pref_save_last_song].defaultChoice = PREFSMAN->m_bLastEditedSong;
			g_Prefs.rows[pref_save_last_diff].defaultChoice = PREFSMAN->m_bLastEditedDifficulty;
			g_Prefs.rows[pref_smooth_scroll].defaultChoice = PREFSMAN->m_bEditorSmoothScrolling;
			g_Prefs.rows[pref_gameplay_keys].defaultChoice = PREFSMAN->m_bGameplayKeysInEditor;
			g_Prefs.rows[pref_show_combo].defaultChoice = PREFSMAN->m_bEditorCombo;
			g_Prefs.rows[pref_snap_beats].defaultChoice = PREFSMAN->m_bShowSnapBeats;
			g_Prefs.rows[pref_timing_in_rows].defaultChoice = PREFSMAN->m_bTimingAsRows;
			g_Prefs.rows[pref_highlight_end].defaultChoice = PREFSMAN->m_bSelectLastSecond;

			SCREENMAN->MiniMenu( &g_Prefs, SM_BackFromPrefs );
			break;

		case play_preview_music:
			PlayPreviewMusic();
			break;
		case exit:
			AutoSave();
			m_Out.StartTransitioning( SM_GoToNextScreen );
			break;
		case timing_menu:
		{
			bool bDelay = false;

			float fBeat = GAMESTATE->m_fPlayerBeat[m_PlayerNumber],
				fBPM = ( m_bBGEditMode ? m_pSong->m_Timing.GetBPMAtBeat( fBeat ) : m_pSteps->m_Timing.GetBPMAtBeat( fBeat ) ),
				fStopSeconds = ( m_bBGEditMode ? m_pSong->m_Timing.GetStopAtBeat( fBeat, bDelay ) : m_pSteps->m_Timing.GetStopAtBeat( fBeat, bDelay ) );

			g_TimingMenu.rows[timing_mode].defaultChoice = (int)m_bBGEditMode;

			g_TimingMenu.rows[timing_bpm].choices.resize(1);	g_TimingMenu.rows[timing_bpm].choices[0]	= ssprintf("%.3f",fBPM);
			g_TimingMenu.rows[timing_stop].choices.resize(1);	g_TimingMenu.rows[timing_stop].choices[0]	= ssprintf("%.3f%s",fStopSeconds, bDelay ? "d" : "");

			// TODO: Rows per Beat
			// TODO: Beats per Measure

			g_TimingMenu.rows[timing_sample_start].choices.resize(1);	g_TimingMenu.rows[timing_sample_start].choices[0]	= ssprintf("%.3f",m_pSong->m_fMusicSampleStartSeconds);
			g_TimingMenu.rows[timing_sample_length].choices.resize(1);	g_TimingMenu.rows[timing_sample_length].choices[0]	= ssprintf("%.3f",m_pSong->m_fMusicSampleLengthSeconds);

			g_TimingMenu.rows[timing_offset].choices.resize(1);	g_TimingMenu.rows[timing_offset].choices[0]	= ssprintf("%.3f",m_bBGEditMode ? m_pSong->m_Timing.m_fBeat0Offset : m_pSteps->m_Timing.m_fBeat0Offset);

			g_TimingMenu.rows[timing_mult].enabled = g_TimingMenu.rows[timing_tick].enabled = g_TimingMenu.rows[timing_fake].enabled = g_TimingMenu.rows[timing_speed_area].enabled = g_TimingMenu.rows[timing_speed].enabled = !m_bBGEditMode;

			if( m_bBGEditMode )
			{
				g_TimingMenu.rows[timing_mult].defaultChoice = g_TimingMenu.rows[timing_tick].defaultChoice = g_TimingMenu.rows[timing_fake].defaultChoice = g_TimingMenu.rows[timing_speed_area].defaultChoice = g_TimingMenu.rows[timing_speed].defaultChoice = 0;
				g_TimingMenu.rows[timing_mult].choices[0] = g_TimingMenu.rows[timing_tick].choices[0] = g_TimingMenu.rows[timing_fake].choices[0] = g_TimingMenu.rows[timing_speed_area].choices[0] = g_TimingMenu.rows[timing_speed].choices[0] = "n/a";
			}
			else
			{
				MultiplierSegment& mlsg = m_pSteps->m_Timing.GetMultiplierSegmentAtBeat( fBeat );
				AreaSegment* fksg = m_pSteps->m_Timing.IsFakeAreaAtBeat( fBeat ) ? &m_pSteps->m_Timing.GetFakeSegmentAtBeat( fBeat ) : NULL;
				SpeedAreaSegment* sasg = m_pSteps->m_Timing.IsSpeedAreaAtBeat( fBeat ) ? &m_pSteps->m_Timing.GetSpeedAreaSegmentAtBeat( fBeat ) : NULL;
				SpeedSegment& spsg = m_pSteps->m_Timing.GetSpeedSegmentAtBeat( fBeat );
			
				g_TimingMenu.rows[timing_mult].choices.resize(1);
				g_TimingMenu.rows[timing_fake].choices.resize(1);
				g_TimingMenu.rows[timing_speed_area].choices.resize(1);
				g_TimingMenu.rows[timing_speed].choices.resize(1);

				g_TimingMenu.rows[timing_tick].choices[0]		= "0";
				g_TimingMenu.rows[timing_fake].choices[0]		= ssprintf("%.2f", fksg ? fksg->m_fBeats : 0.f);
				g_TimingMenu.rows[timing_speed_area].choices[0]	= ssprintf("%.2f", sasg ? sasg->m_fBeats : 0.f);
				g_TimingMenu.rows[timing_speed].choices[0]		= ssprintf (
																	"%.3f%s%s",
																	spsg.m_fToSpeed,
																	(
																		spsg.m_fBeats < 0 ?
																		ssprintf(", %.3fs",spsg.m_fBeats*-1).c_str() :
																		ssprintf(", %.3f",spsg.m_fBeats).c_str()
																	),
																	spsg.m_fFactor == 1.f ? "" : ( spsg.m_fFactor > 1.f ? "-" : "+" )
																);
				g_TimingMenu.rows[timing_mult].choices[0]		= ssprintf(
																	"C: %d%s, L: %.3f%s, S: %.3f%s",
																	mlsg.m_uHit,
																	mlsg.m_uHit != mlsg.m_uMiss ? "!" : "",
																	mlsg.m_fLifeHit,
																	mlsg.m_fLifeHit != mlsg.m_fLifeMiss ? "!" : "",
																	mlsg.m_fScoreHit,
																	mlsg.m_fScoreHit != mlsg.m_fScoreMiss ? "!" : ""
																);

				switch( m_pSteps->m_Timing.GetTickcountAtBeat( fBeat ) )
				{
				case 0:
					g_TimingMenu.rows[timing_tick].defaultChoice = 0;
					break;
				case 1:
					g_TimingMenu.rows[timing_tick].defaultChoice = 1;
					break;
				case 2:
					g_TimingMenu.rows[timing_tick].defaultChoice = 2;
					break;
				case 3:
					g_TimingMenu.rows[timing_tick].defaultChoice = 3;
					break;
				case 4:
					g_TimingMenu.rows[timing_tick].defaultChoice = 4;
					break;
				case 6:
					g_TimingMenu.rows[timing_tick].defaultChoice = 5;
					break;
				case 8:
					g_TimingMenu.rows[timing_tick].defaultChoice = 6;
					break;
				case 12:
					g_TimingMenu.rows[timing_tick].defaultChoice = 7;
					break;
				case 16:
					g_TimingMenu.rows[timing_tick].defaultChoice = 8;
					break;
				case 24:
					g_TimingMenu.rows[timing_tick].defaultChoice = 9;
					break;
				case 32:
					g_TimingMenu.rows[timing_tick].defaultChoice = 10;
					break;
				case 48:
					g_TimingMenu.rows[timing_tick].defaultChoice = 11;
					break;
				case 64:
					g_TimingMenu.rows[timing_tick].defaultChoice = 12;
					break;
				case 96:
					g_TimingMenu.rows[timing_tick].defaultChoice = 13;
					break;
				case 192:
					g_TimingMenu.rows[timing_tick].defaultChoice = 14;
					break;
				default:
					ASSERT(0);
				}
			}

			SCREENMAN->MiniMenu( &g_TimingMenu, SM_BackFromTimingMenu );
		}
			break;
		default:
			ASSERT(0);
	};
}

void ScreenEdit::HandleAreaMenuChoice( AreaMenuChoice c, int* iAnswers )
{
	switch( c )
	{
		case cut:
			{
				HandleAreaMenuChoice( copy, NULL );
				HandleAreaMenuChoice( clear, NULL );
			}
			break;
		case copy:
			{
				ASSERT( m_NoteFieldEdit.m_fBeginMarker!=-1 && m_NoteFieldEdit.m_fEndMarker!=-1 );
				int iFirstRow = BeatToNoteRowRounded( m_NoteFieldEdit.m_fBeginMarker );
				int iLastRow  = BeatToNoteRowRounded( m_NoteFieldEdit.m_fEndMarker );
				m_Clipboard.ClearAll();
				m_Clipboard.CopyRange( &m_NoteFieldEdit, iFirstRow, iLastRow );
			}
			break;
		case paste_at_current_beat:
			{
				int iSrcFirstRow = 0;
				int iSrcLastRow  = BeatToNoteRowRounded( m_Clipboard.GetLastBeat() );
				int iDestFirstRow = BeatToNoteRowRounded( GAMESTATE->m_fPlayerBeat[m_PlayerNumber] );
				m_NoteFieldEdit.CopyRange( &m_Clipboard, iSrcFirstRow, iSrcLastRow, iDestFirstRow );
				ResetAutoSave();
			}
			break;
		case paste_at_begin_marker:
			{
				ASSERT( m_NoteFieldEdit.m_fBeginMarker!=-1 );
				int iSrcFirstRow = 0;
				int iSrcLastRow  = BeatToNoteRowRounded( m_Clipboard.GetLastBeat() );
				int iDestFirstRow = BeatToNoteRowRounded( m_NoteFieldEdit.m_fBeginMarker );
				m_NoteFieldEdit.CopyRange( &m_Clipboard, iSrcFirstRow, iSrcLastRow, iDestFirstRow );
				ResetAutoSave();
			}
			break;
		case clear:
			{
				ASSERT( m_NoteFieldEdit.m_fBeginMarker!=-1 && m_NoteFieldEdit.m_fEndMarker!=-1 );
				int iFirstRow = BeatToNoteRowRounded( m_NoteFieldEdit.m_fBeginMarker );
				int iLastRow  = BeatToNoteRowRounded( m_NoteFieldEdit.m_fEndMarker );
				m_NoteFieldEdit.ClearRange( iFirstRow, iLastRow );
				ResetAutoSave();
			}
			break;
		case quantize:
			{
				NoteType nt = (NoteType)iAnswers[c];
				NoteDataUtil::SnapToNearestNoteType( m_NoteFieldEdit, nt, nt, m_NoteFieldEdit.m_fBeginMarker, m_NoteFieldEdit.m_fEndMarker );
				ResetAutoSave();
			}
			break;
		case turn:
			{
				const NoteData OldClipboard( m_Clipboard );
				HandleAreaMenuChoice( cut, NULL );

				StepsType st = GAMESTATE->GetCurrentStyle()->m_StepsType;
				TurnType tt = (TurnType)iAnswers[c];
				switch( tt )
				{
				case backwards:		NoteDataUtil::Turn( m_Clipboard, st, NoteDataUtil::backwards );		break;
				case mirror:		NoteDataUtil::Turn( m_Clipboard, st, NoteDataUtil::mirror );		break;
				case left:			NoteDataUtil::Turn( m_Clipboard, st, NoteDataUtil::left );			break;
				case right:			NoteDataUtil::Turn( m_Clipboard, st, NoteDataUtil::right );			break;
				case shuffle:		NoteDataUtil::Turn( m_Clipboard, st, NoteDataUtil::shuffle );		break;
				case super_shuffle:	NoteDataUtil::Turn( m_Clipboard, st, NoteDataUtil::super_shuffle );	break;
				default:		ASSERT(0);
				}

				HandleAreaMenuChoice( paste_at_begin_marker, NULL );
				m_Clipboard = OldClipboard;
			}
			break;
		case transform:
			{
				float fBeginBeat = m_NoteFieldEdit.m_fBeginMarker;
				float fEndBeat = m_NoteFieldEdit.m_fEndMarker;
				TransformType tt = (TransformType)iAnswers[c];
				StepsType st = GAMESTATE->GetCurrentStyle()->m_StepsType;
				switch( tt )
				{
				case norolls:		NoteDataUtil::RemoveRollNotes( m_NoteFieldEdit, fBeginBeat, fEndBeat );		break;
				case noholds:		NoteDataUtil::RemoveHoldNotes( m_NoteFieldEdit, fBeginBeat, fEndBeat );		break;
				case nomines:		NoteDataUtil::RemoveMines( m_NoteFieldEdit, fBeginBeat, fEndBeat );			break;
				case mineshocks:	NoteDataUtil::MinesToShocks( m_NoteFieldEdit, fBeginBeat, fEndBeat );		break;
				case halfmines:		NoteDataUtil::HalfMines( m_NoteFieldEdit, fBeginBeat, fEndBeat );			break;
				case noshocks:		NoteDataUtil::RemoveShocks( m_NoteFieldEdit, fBeginBeat, fEndBeat );		break;
				case halfshocks:	NoteDataUtil::HalfShocks( m_NoteFieldEdit, fBeginBeat, fEndBeat );			break;
				case nopotions:		NoteDataUtil::RemovePotions( m_NoteFieldEdit, fBeginBeat, fEndBeat );		break;
				case halfpotions:	NoteDataUtil::HalfPotions( m_NoteFieldEdit, fBeginBeat, fEndBeat );			break;
				case nolifts:		NoteDataUtil::RemoveLiftNotes( m_NoteFieldEdit, fBeginBeat, fEndBeat );		break;
				case nohidden:		NoteDataUtil::RemoveHiddenNotes( m_NoteFieldEdit, fBeginBeat, fEndBeat );	break;
				case little:		NoteDataUtil::Little( m_NoteFieldEdit, fBeginBeat, fEndBeat );				break;
				case wide:			NoteDataUtil::Wide( m_NoteFieldEdit, fBeginBeat, fEndBeat );				break;
				case big:			NoteDataUtil::Big( m_NoteFieldEdit, fBeginBeat, fEndBeat );					break;
				case quick:			NoteDataUtil::Quick( m_NoteFieldEdit, fBeginBeat, fEndBeat );				break;
				case bmrize:		NoteDataUtil::BMRize( m_NoteFieldEdit, fBeginBeat, fEndBeat );				break;
				case skippy:		NoteDataUtil::Skippy( m_NoteFieldEdit, fBeginBeat, fEndBeat );				break;
				case mines:			NoteDataUtil::AddMines( m_NoteFieldEdit, fBeginBeat, fEndBeat );			break;
				case shocks:		NoteDataUtil::AddShocks( m_NoteFieldEdit, fBeginBeat, fEndBeat );			break;
				case potions:		NoteDataUtil::AddPotions( m_NoteFieldEdit, fBeginBeat, fEndBeat );			break;
				case lifts:			NoteDataUtil::AddLiftNotes( m_NoteFieldEdit, fBeginBeat, fEndBeat );		break;
				case echo:			NoteDataUtil::Echo( m_NoteFieldEdit, fBeginBeat, fEndBeat );				break;
				case stomp:			NoteDataUtil::Stomp( m_NoteFieldEdit, st, fBeginBeat, fEndBeat );			break;
				case planted:		NoteDataUtil::Planted( m_NoteFieldEdit, fBeginBeat, fEndBeat );				break;
				case floored:		NoteDataUtil::Floored( m_NoteFieldEdit, fBeginBeat, fEndBeat );				break;
				case twister:		NoteDataUtil::Twister( m_NoteFieldEdit, fBeginBeat, fEndBeat );				break;
				case nojumps:		NoteDataUtil::RemoveJumps( m_NoteFieldEdit, fBeginBeat, fEndBeat );			break;
				case nohands:		NoteDataUtil::RemoveHands( m_NoteFieldEdit, fBeginBeat, fEndBeat );			break;
				case noquads:		NoteDataUtil::RemoveQuads( m_NoteFieldEdit, fBeginBeat, fEndBeat );			break;
				default:			ASSERT(0);
				}

				// bake in the additions
				NoteDataUtil::ConvertAdditionsToRegular( m_NoteFieldEdit );
				ResetAutoSave();
			}
			break;
		case alter:
			{
				const NoteData OldClipboard( m_Clipboard );
				HandleAreaMenuChoice( cut, NULL );

				AlterType at = (AlterType)iAnswers[c];
				switch( at )
				{
				case backwards:				NoteDataUtil::Backwards( m_Clipboard );			break;
				case swap_sides:			NoteDataUtil::SwapSides( m_Clipboard );			break;
				case copy_left_to_right:	NoteDataUtil::CopyLeftToRight( m_Clipboard );	break;
				case copy_right_to_left:	NoteDataUtil::CopyRightToLeft( m_Clipboard );	break;
				case clear_left:			NoteDataUtil::ClearLeft( m_Clipboard );			break;
				case clear_right:			NoteDataUtil::ClearRight( m_Clipboard );		break;
				case collapse_to_one:		NoteDataUtil::CollapseToOne( m_Clipboard );		break;
				case collapse_left:			NoteDataUtil::CollapseLeft( m_Clipboard );		break;
				case shift_left:			NoteDataUtil::ShiftLeft( m_Clipboard );			break;
				case shift_right:			NoteDataUtil::ShiftRight( m_Clipboard );		break;
				default:		ASSERT(0);
				}

				HandleAreaMenuChoice( paste_at_begin_marker, NULL );
				m_Clipboard = OldClipboard;
			}
			break;
		case tempo:
			{
				// This affects all steps.
				const NoteData OldClipboard( m_Clipboard );
				HandleAreaMenuChoice( cut, NULL );

				AlterType at = (AlterType)iAnswers[c];
				float fScale = -1;
				switch( at )
				{
				case compress_2x:	fScale = 0.5f;		break;
				case compress_3_2:	fScale = 2.0f/3;	break;
				case compress_4_3:	fScale = 0.75f;		break;
				case expand_4_3:	fScale = 4.0f/3;	break;
				case expand_3_2:	fScale = 1.5f;		break;
				case expand_2x:		fScale = 2;			break;
				// TODO - Aldo_MX: Arbitrary Tempo
				default:	ASSERT(0);	return;
				}

				m_Clipboard.ConvertBackTo2sAnd3sAnd4s();
				NoteDataUtil::Scale( m_Clipboard, fScale );
				m_Clipboard.Convert2sAnd3sAnd4s();

				//float fOldClipboardEndBeat = m_NoteFieldEdit.m_fEndMarker;
				float fOldClipboardBeats = m_NoteFieldEdit.m_fEndMarker - m_NoteFieldEdit.m_fBeginMarker;
				float fNewClipboardBeats = fOldClipboardBeats * fScale;
				float fDeltaBeats = fNewClipboardBeats - fOldClipboardBeats;
				float fNewClipboardEndBeat = m_NoteFieldEdit.m_fBeginMarker + fNewClipboardBeats;

				if( m_bBGEditMode )
					m_pSong->m_Timing.ScaleRegion( fScale, m_NoteFieldEdit.m_fBeginMarker, m_NoteFieldEdit.m_fEndMarker );
				else
				{
					NoteDataUtil::ShiftBeats( m_NoteFieldEdit, m_NoteFieldEdit.m_fBeginMarker, fDeltaBeats );
					m_pSteps->m_Timing.ScaleRegion( fScale, m_NoteFieldEdit.m_fBeginMarker, m_NoteFieldEdit.m_fEndMarker );
				}

				HandleAreaMenuChoice( paste_at_begin_marker, NULL );

				m_NoteFieldEdit.m_fEndMarker = fNewClipboardEndBeat;

				if( m_bBGEditMode )
				{
					float fOldBPM = m_pSong->m_Timing.GetBPMAtBeat( m_NoteFieldEdit.m_fBeginMarker );
					float fNewBPM = fOldBPM * fScale;
					m_pSong->m_Timing.SetBPMAtBeat( m_NoteFieldEdit.m_fBeginMarker, fNewBPM );
					m_pSong->m_Timing.SetBPMAtBeat( fNewClipboardEndBeat, fOldBPM );
				}
				else
				{
					float fOldBPM = m_pSteps->m_Timing.GetBPMAtBeat( m_NoteFieldEdit.m_fBeginMarker );
					float fNewBPM = fOldBPM * fScale;
					m_pSteps->m_Timing.SetBPMAtBeat( m_NoteFieldEdit.m_fBeginMarker, fNewBPM );
					m_pSteps->m_Timing.SetBPMAtBeat( fNewClipboardEndBeat, fOldBPM );
				}
			}
			break;
		case play:
			{
				ASSERT( m_NoteFieldEdit.m_fBeginMarker!=-1 && m_NoteFieldEdit.m_fEndMarker!=-1 );

				if( PREFSMAN->m_bAutoSaveBeforePlay )
					AutoSave();

				SOUND->PlayMusic("");

				GAMESTATE->m_EditMode = GAMESTATE->MODE_PLAYING;
				GAMESTATE->m_bPlaying = true;
				GAMESTATE->m_bPastHereWeGo = true;
				GAMESTATE->ResetAutoPlay();

				FOREACH_PlayerNumber( pn )
					GAMESTATE->m_fNotePressedTime[pn] = 0.f;

				/* Reset the note skin, in case preferences have changed. */
				GAMESTATE->ResetNoteSkins();

				/* Give a 1 measure lead-in.  Set this before loading Player, so it knows
				 * where we're starting. */
				// TODO - Aldo_MX: PumpMania's Lead-In
				float fSeconds = m_bBGEditMode ? m_pSong->m_Timing.GetElapsedTimeFromBeat( m_NoteFieldEdit.m_fBeginMarker - fBEATS_PER_MEASURE ) : m_pSteps->m_Timing.GetElapsedTimeFromBeat( m_NoteFieldEdit.m_fBeginMarker - fBEATS_PER_MEASURE );
				GAMESTATE->UpdateSongPosition( fSeconds, m_pSong->m_Timing );
				GAMESTATE->UpdatePlayerPosition( m_PlayerNumber, m_pSteps->m_Timing );

				SetupCourseAttacks();

				if( PREFSMAN->m_bEditorCombo )
				{
					m_apSongs.clear();
					m_apSongs.push_back( GAMESTATE->m_pCurSong );

					m_apSteps.clear();
					m_apSteps.push_back( GAMESTATE->m_pCurSteps[m_PlayerNumber] );

					m_apAttacks.clear();
					m_apAttacks.push_back( GAMESTATE->m_pCurSteps[m_PlayerNumber]->m_Attacks );

					switch( PREFSMAN->m_iScoringType )
					{
					case PrefsManager::SCORING_MAX2:
					case PrefsManager::SCORING_NOVA:
					case PrefsManager::SCORING_NOVA2:
					case PrefsManager::SCORING_5TH:
					case PrefsManager::SCORING_HYBRID:
					case PrefsManager::SCORING_CUSTOM:
						m_ScoreKeeper = new ScoreKeeperMAX2( m_apSongs, m_apSteps, m_apAttacks, m_PlayerNumber );
						break;
					case PrefsManager::SCORING_PIU_NX2:
					case PrefsManager::SCORING_PIU_ZERO:
					case PrefsManager::SCORING_PIU_PREX3:
					case PrefsManager::SCORING_PIU_EXTRA:
						m_ScoreKeeper = new ScoreKeeperPump( m_PlayerNumber );
						break;
					default:
						ASSERT(0);
					}
				}
				else
					m_ScoreKeeper = NULL;

				m_Player.Load( m_PlayerNumber, &m_NoteFieldEdit, NULL, NULL, NULL, NULL, NULL, m_ScoreKeeper, NULL );

				GAMESTATE->m_PlayerController[m_PlayerNumber] = PREFSMAN->m_bAutoPlay ? PC_AUTOPLAY : PC_HUMAN;

				m_rectRecordBack.StopTweening();
				m_rectRecordBack.BeginTweening( 0.5f );
				m_rectRecordBack.SetDiffuse( RageColor(0,0,0,0.8f) );

				const float fStartSeconds = m_bBGEditMode ? m_pSong->m_Timing.GetElapsedTimeFromBeat(GAMESTATE->m_fSongBeat) : m_pSteps->m_Timing.GetElapsedTimeFromBeat(GAMESTATE->m_fPlayerBeat[m_PlayerNumber]);
				LOG->Trace( "Starting playback at %f", fStartSeconds );

				if( PREFSMAN->m_bEditorShowBGChangesPlay )
				{
					/* FirstBeat affects backgrounds, so commit changes to memory (not to disk)
					 * and recalc it. */
					Steps* pSteps = GAMESTATE->m_pCurSteps[m_PlayerNumber];
					ASSERT( pSteps );
					pSteps->SetNoteData( &m_NoteFieldEdit );
					m_pSong->ReCalculateRadarValuesAndLastBeat();

					m_Background.Unload();
					m_Background.LoadFromSong( m_pSong );

					m_Foreground.Unload();
					m_Foreground.LoadFromSong( m_pSong );
				}

				RageSoundParams p;
				p.SetPlaybackRate( GAMESTATE->m_SongOptions.m_fMusicRate );
				p.m_StartSecond = fStartSeconds;
				p.AccurateSync = true;
				p.StopMode = RageSoundParams::M_CONTINUE;
				m_soundMusic.Play( &p );
			}
			break;
		case record:
			{
				ASSERT( m_NoteFieldEdit.m_fBeginMarker!=-1 && m_NoteFieldEdit.m_fEndMarker!=-1 );

				SOUND->PlayMusic("");

				GAMESTATE->m_EditMode = GAMESTATE->MODE_RECORDING;
				GAMESTATE->m_bPlaying = true;
				GAMESTATE->m_bPastHereWeGo = true;

				/* Reset the note skin, in case preferences have changed. */
				GAMESTATE->ResetNoteSkins();

				// initialize m_NoteFieldRecord
				m_NoteFieldRecord.Load( &m_NoteFieldEdit, m_PlayerNumber );
				m_NoteFieldRecord.UpdateDrawingArea( -150, 350, 350 );
				m_NoteFieldRecord.SetNumTracks( m_NoteFieldEdit.GetNumTracks() );

				m_rectRecordBack.StopTweening();
				m_rectRecordBack.BeginTweening( 0.5f );
				m_rectRecordBack.SetDiffuse( RageColor(0,0,0,0.8f) );

				GAMESTATE->m_fPlayerBeat[m_PlayerNumber] = m_NoteFieldEdit.m_fBeginMarker - 4;	// give a 1 measure lead-in

				if( m_bBGEditMode )
					GAMESTATE->m_fSongBeat = GAMESTATE->m_fPlayerBeat[m_PlayerNumber];	// give a 1 measure lead-in

				float fStartSeconds = m_bBGEditMode ? m_pSong->m_Timing.GetElapsedTimeFromBeat(GAMESTATE->m_fSongBeat) : m_pSteps->m_Timing.GetElapsedTimeFromBeat(GAMESTATE->m_fPlayerBeat[m_PlayerNumber]);
				LOG->Trace( "Starting playback at %f", fStartSeconds );

				RageSoundParams p;
				p.SetPlaybackRate( GAMESTATE->m_SongOptions.m_fMusicRate );
				p.m_StartSecond = fStartSeconds;
				p.AccurateSync = true;
				p.StopMode = RageSoundParams::M_CONTINUE;
				m_soundMusic.Play( &p );
			}
			break;
		case insert_and_shift:
			NoteDataUtil::ShiftBeats( m_NoteFieldEdit, GAMESTATE->m_fPlayerBeat[m_PlayerNumber], 1 );
			ResetAutoSave();
			break;
		case delete_and_shift:
			NoteDataUtil::ShiftBeats( m_NoteFieldEdit, GAMESTATE->m_fPlayerBeat[m_PlayerNumber], -1 );
			ResetAutoSave();
			break;
		case shift_pauses_forward:
			if( m_bBGEditMode )
				m_pSong->m_Timing.ShiftBeats( GAMESTATE->m_fPlayerBeat[m_PlayerNumber], 1 );
			else
				m_pSteps->m_Timing.ShiftBeats( GAMESTATE->m_fPlayerBeat[m_PlayerNumber], 1 );
			ResetAutoSave();
			break;
		case shift_pauses_backward:
			if( m_bBGEditMode )
				m_pSong->m_Timing.ShiftBeats( GAMESTATE->m_fPlayerBeat[m_PlayerNumber], -1 );
			else
				m_pSteps->m_Timing.ShiftBeats( GAMESTATE->m_fPlayerBeat[m_PlayerNumber], -1 );
			ResetAutoSave();
			break;
		// MD 11/02/03 - Converting selected region to a pause of the same length.
		case convert_beat_to_pause:
			ResetAutoSave();
			// TODO - Aldo_MX: A veces se pegan los stops
			if( m_bBGEditMode )
			{
				ASSERT( m_NoteFieldEdit.m_fBeginMarker!=-1 && m_NoteFieldEdit.m_fEndMarker!=-1 );
				// This was written horribly, using beats and not converting to time at all.
				float fMarkerStart = m_pSong->m_Timing.GetElapsedTimeFromBeat(m_NoteFieldEdit.m_fBeginMarker);
				float fMarkerEnd = m_pSong->m_Timing.GetElapsedTimeFromBeat(m_NoteFieldEdit.m_fEndMarker);
				float fStopLength = fMarkerEnd - fMarkerStart;
				// be sure not to clobber the row at the start - a row at the end
				// can be dropped safely, though
				NoteDataUtil::ShiftBeats(
					m_NoteFieldEdit,
					m_NoteFieldEdit.m_fBeginMarker + ( INPUTFILTER->GetKeyFlagShift() ? 0 : 0.003f ),
					(-m_NoteFieldEdit.m_fEndMarker+m_NoteFieldEdit.m_fBeginMarker)
				);
				m_pSong->m_Timing.ShiftBeats(
					m_NoteFieldEdit.m_fBeginMarker + ( INPUTFILTER->GetKeyFlagShift() ? 0 : 0.003f ),
					(-m_NoteFieldEdit.m_fEndMarker+m_NoteFieldEdit.m_fBeginMarker)
				);
				unsigned i;
				for( i=0; i<m_pSong->m_Timing.m_StopSegments.size(); i++ )
				{
					float fStart = m_pSong->m_Timing.GetElapsedTimeFromBeat(m_pSong->m_Timing.m_StopSegments[i].m_fBeat);
					float fEnd = fStart + m_pSong->m_Timing.m_StopSegments[i].m_fSeconds;
					if( fStart > fMarkerEnd || fEnd < fMarkerStart )
						continue;
					else
					{
						if (fStart > fMarkerStart)
							m_pSong->m_Timing.m_StopSegments[i].m_fBeat = m_NoteFieldEdit.m_fBeginMarker;
						m_pSong->m_Timing.m_StopSegments[i].m_fSeconds = fStopLength;
						break;
					}
				}

				if( i == m_pSong->m_Timing.m_StopSegments.size() )	// there is no BPMSegment at the current beat
					m_pSong->m_Timing.SetStopAtBeat( m_NoteFieldEdit.m_fBeginMarker, fStopLength, INPUTFILTER->GetKeyFlagShift() != HOLDING_NONE );
				m_NoteFieldEdit.m_fEndMarker = -1;
				break;
			}
			else
			{
				ASSERT( m_NoteFieldEdit.m_fBeginMarker!=-1 && m_NoteFieldEdit.m_fEndMarker!=-1 );
				// This was written horribly, using beats and not converting to time at all.
				float fMarkerStart = m_pSteps->m_Timing.GetElapsedTimeFromBeat(m_NoteFieldEdit.m_fBeginMarker);
				float fMarkerEnd = m_pSteps->m_Timing.GetElapsedTimeFromBeat(m_NoteFieldEdit.m_fEndMarker);
				float fStopLength = fMarkerEnd - fMarkerStart;
				// be sure not to clobber the row at the start - a row at the end
				// can be dropped safely, though
				NoteDataUtil::ShiftBeats(
					m_NoteFieldEdit,
					m_NoteFieldEdit.m_fBeginMarker + ( INPUTFILTER->GetKeyFlagShift() ? 0 : 0.003f ),
					(-m_NoteFieldEdit.m_fEndMarker+m_NoteFieldEdit.m_fBeginMarker)
				);
				m_pSteps->m_Timing.ShiftBeats(
					m_NoteFieldEdit.m_fBeginMarker + ( INPUTFILTER->GetKeyFlagShift() ? 0 : 0.003f ),
					(-m_NoteFieldEdit.m_fEndMarker+m_NoteFieldEdit.m_fBeginMarker)
				);
				unsigned i;
				for( i=0; i<m_pSteps->m_Timing.m_StopSegments.size(); i++ )
				{
					float fStart = m_pSteps->m_Timing.GetElapsedTimeFromBeat(m_pSteps->m_Timing.m_StopSegments[i].m_fBeat);
					float fEnd = fStart + m_pSteps->m_Timing.m_StopSegments[i].m_fSeconds;
					if( fStart > fMarkerEnd || fEnd < fMarkerStart )
						continue;
					else
					{
						if (fStart > fMarkerStart)
							m_pSteps->m_Timing.m_StopSegments[i].m_fBeat = m_NoteFieldEdit.m_fBeginMarker;
						m_pSteps->m_Timing.m_StopSegments[i].m_fSeconds = fStopLength;
						break;
					}
				}

				if( i == m_pSteps->m_Timing.m_StopSegments.size() )	// there is no BPMSegment at the current beat
					m_pSteps->m_Timing.SetStopAtBeat( m_NoteFieldEdit.m_fBeginMarker, fStopLength, INPUTFILTER->GetKeyFlagShift() != HOLDING_NONE );
				m_NoteFieldEdit.m_fEndMarker = -1;
				break;
			}
		// MD 11/02/03 - Converting a pause at the current beat into beats.
		//    I know this will break holds that cross the pause.  Anyone who
		//    wants to rewrite this to fix that behavior is welcome to - I'm
		//    not sure how exactly to do it without making this a lot longer
		//    than it is.
		// NOTE: Fixed this so that holds aren't broken by it.  Working in 2s and
		// 3s makes this work better, too. :-)  It sorta makes you wonder WHY we
		// don't bring it into 2s and 3s when we bring up the editor.
		case convert_pause_to_beat:
			ResetAutoSave();
			if( m_bBGEditMode )
			{
				float fBPMatPause = m_pSong->m_Timing.GetBPMAtBeat( GAMESTATE->m_fPlayerBeat[m_PlayerNumber] );
				unsigned i;
				for( i=0; i<m_pSong->m_Timing.m_StopSegments.size(); i++ )
				{
					if( m_pSong->m_Timing.m_StopSegments[i].m_fBeat == GAMESTATE->m_fPlayerBeat[m_PlayerNumber] )
						break;
				}

				if( i == m_pSong->m_Timing.m_StopSegments.size() )	// there is no BPMSegment at the current beat
					break;
				else	// StopSegment being modified is m_Timing.m_StopSegments[i]
				{
					float fStopLength = m_pSong->m_Timing.m_StopSegments[i].m_fSeconds;
					bool bDelay = m_pSong->m_Timing.m_StopSegments[i].m_bDelay;
					m_pSong->m_Timing.m_StopSegments.erase(
						m_pSong->m_Timing.m_StopSegments.begin()+i,
						m_pSong->m_Timing.m_StopSegments.begin()+i+1
					);
					fStopLength *= fBPMatPause;
					fStopLength /= 60;
					// don't move the step from where it is, just move everything later
					m_NoteFieldEdit.ConvertBackTo2sAnd3sAnd4s();
					NoteDataUtil::ShiftBeats(
						m_NoteFieldEdit,
						GAMESTATE->m_fPlayerBeat[m_PlayerNumber] + ( bDelay ? 0 : 0.003f ),
						fStopLength
					);
					m_pSong->m_Timing.ShiftBeats(
						GAMESTATE->m_fPlayerBeat[m_PlayerNumber] + ( bDelay ? 0 : 0.003f ),
						fStopLength
					);
					m_NoteFieldEdit.Convert2sAnd3sAnd4s();

				}
			}
			else
			{
				float fBPMatPause = m_pSteps->m_Timing.GetBPMAtBeat( GAMESTATE->m_fPlayerBeat[m_PlayerNumber] );
				unsigned i;
				for( i=0; i<m_pSteps->m_Timing.m_StopSegments.size(); i++ )
				{
					if( m_pSteps->m_Timing.m_StopSegments[i].m_fBeat == GAMESTATE->m_fPlayerBeat[m_PlayerNumber] )
						break;
				}

				if( i == m_pSteps->m_Timing.m_StopSegments.size() )	// there is no BPMSegment at the current beat
					break;
				else	// StopSegment being modified is m_Timing.m_StopSegments[i]
				{
					float fStopLength = m_pSteps->m_Timing.m_StopSegments[i].m_fSeconds;
					bool bDelay = m_pSteps->m_Timing.m_StopSegments[i].m_bDelay;
					m_pSteps->m_Timing.m_StopSegments.erase(
						m_pSteps->m_Timing.m_StopSegments.begin()+i,
						m_pSteps->m_Timing.m_StopSegments.begin()+i+1
					);
					fStopLength *= fBPMatPause;
					fStopLength /= 60;
					// don't move the step from where it is, just move everything later
					m_NoteFieldEdit.ConvertBackTo2sAnd3sAnd4s();
					NoteDataUtil::ShiftBeats(
						m_NoteFieldEdit,
						GAMESTATE->m_fPlayerBeat[m_PlayerNumber] + ( bDelay ? 0 : 0.003f ),
						fStopLength
					);
					m_pSteps->m_Timing.ShiftBeats(
						GAMESTATE->m_fPlayerBeat[m_PlayerNumber] + ( bDelay ? 0 : 0.003f ),
						fStopLength
					);
					m_NoteFieldEdit.Convert2sAnd3sAnd4s();

				}
			// Hello and welcome to I FEEL STUPID :-)
			break;
			}
		default:
			ASSERT(0);
	};

}

void ScreenEdit::HandleEditNotesStatisticsChoice( EditNotesStatisticsChoice c, int* iAnswers )
{
	Steps* pSteps = GAMESTATE->m_pCurSteps[m_PlayerNumber];

	if( (unsigned)iAnswers[players]+1 != m_uNumPlayers )
	{
		unsigned uNewPlayers = iAnswers[players]+1;
		if( uNewPlayers != m_uNumPlayers )
		{
			bool bReload = m_uNumPlayers == 1 || uNewPlayers == 1;
			m_NoteFieldEdit.SetNumPlayers(uNewPlayers);
			m_uNumPlayers = uNewPlayers;
			if( bReload )
				m_NoteFieldEdit.Reload();
			ResetAutoSave();
		}
	}

	pSteps->SetDifficulty( (Difficulty)iAnswers[difficulty] );
	pSteps->SetMeter( iAnswers[meter]+1 );

	switch( c )
	{
	case description:
		SCREENMAN->TextEntry( SM_None, "Edit notes description.\nPress Enter to confirm, ESC to cancel.", m_pSteps->GetDescription(), ChangeDescription, NULL );
		break;
	}
}

void ScreenEdit::HandleEditSongInfoChoice( EditSongInfoChoice c, int* iAnswers )
{
	switch( c )
	{
	case main_title:
		SCREENMAN->TextEntry( SM_None, "Edit main title.\nPress Enter to confirm, ESC to cancel.", m_pSong->m_sMainTitle, ChangeMainTitle, NULL );
		break;
	case sub_title:
		SCREENMAN->TextEntry( SM_None, "Edit sub title.\nPress Enter to confirm, ESC to cancel.", m_pSong->m_sSubTitle, ChangeSubTitle, NULL );
		break;
	case artist:
		SCREENMAN->TextEntry( SM_None, "Edit artist.\nPress Enter to confirm, ESC to cancel.", m_pSong->m_sArtist, ChangeArtist, NULL );
		break;
	case credit:
		SCREENMAN->TextEntry( SM_None, "Edit credit.\nPress Enter to confirm, ESC to cancel.", m_pSteps->m_sCredit, ChangeCredit, NULL );
		break;
	case genre:
		SCREENMAN->TextEntry( SM_None, "Edit genre.\nPress Enter to confirm, ESC to cancel.", m_pSong->m_sGenre, ChangeGenre, NULL );
		break;
	case main_title_transliteration:
		SCREENMAN->TextEntry( SM_None, "Edit main title transliteration.\nPress Enter to confirm, ESC to cancel.", m_pSong->m_sMainTitleTranslit, ChangeMainTitleTranslit, NULL );
		break;
	case sub_title_transliteration:
		SCREENMAN->TextEntry( SM_None, "Edit sub title transliteration.\nPress Enter to confirm, ESC to cancel.", m_pSong->m_sSubTitleTranslit, ChangeSubTitleTranslit, NULL );
		break;
	case artist_transliteration:
		SCREENMAN->TextEntry( SM_None, "Edit artist transliteration.\nPress Enter to confirm, ESC to cancel.", m_pSong->m_sArtistTranslit, ChangeArtistTranslit, NULL );
		break;
	default:
		ASSERT(0);
	};
}

void ScreenEdit::HandleBGChangeChoice( BGChangeChoice c, int* iAnswers )
{
	BGChange newChange;

	FOREACH( BGChange, m_pSong->m_BGChanges, iter )
	{
		if( iter->m_fBeat == GAMESTATE->m_fPlayerBeat[m_PlayerNumber] )
		{
			newChange = *iter;
			// delete the old change.  We'll add a new one below.
			m_pSong->m_BGChanges.erase( iter );
			break;
		}
	}

	newChange.m_fBeat = GAMESTATE->m_fPlayerBeat[m_PlayerNumber];

	switch( c )
	{
	case add_random:
		newChange.m_sBGName = "-random-";
		break;
	case add_song_bganimation:
	case add_song_movie:
	case add_song_still:
	case add_global_random_movie:
	case add_global_bganimation:
	case add_global_visualization:
		newChange.m_sBGName = g_BGChange.rows[c].choices[iAnswers[c]];
		break;
	case delete_change:
		newChange.m_sBGName = "";
		break;
	default:
		break;
	};

	newChange.m_fRate = strtof( g_BGChange.rows[rate].choices[iAnswers[rate]], NULL )/100.f;
	newChange.m_bFadeLast = !!iAnswers[fade_last];
	newChange.m_bRewindMovie = !!iAnswers[rewind_movie];
	newChange.m_bLoop = !!iAnswers[loop];

	if( newChange.m_sBGName != "" )
		m_pSong->AddBGChange( newChange );
}

void ScreenEdit::SetupCourseAttacks()
{
	/* This is the first beat that can be changed without it being visible.  Until
	 * we draw for the first time, any beat can be changed. */
	GAMESTATE->m_fLastDrawnBeat[m_PlayerNumber] = -100;

	// Put course options into effect.
	GAMESTATE->m_ModsToApply[m_PlayerNumber].clear();
	GAMESTATE->RemoveActiveAttacksForPlayer( m_PlayerNumber );


	if( m_pAttacksFromCourse )
	{
		m_pAttacksFromCourse->LoadFromCRSFile( m_pAttacksFromCourse->m_sPath );

		AttackArray Attacks;
		for( unsigned e = 0; e < m_pAttacksFromCourse->m_entries.size(); ++e )
		{
			if( m_pAttacksFromCourse->m_entries[e].type != COURSE_ENTRY_FIXED )
				continue;
			if( m_pAttacksFromCourse->m_entries[e].pSong != m_pSong )
				continue;

			Attacks = m_pAttacksFromCourse->m_entries[e].attacks;
			break;
		}

		for( unsigned i=0; i<Attacks.size(); ++i )
			GAMESTATE->LaunchAttack( m_PlayerNumber, Attacks[i] );
	}
	GAMESTATE->RebuildPlayerOptionsFromActiveAttacks( m_PlayerNumber );
}

void AddSongBPM( CString sNew )
{
	if( sNew.empty() )
		sNew = "0";

	sNew.MakeLower();

	if( sNew == "r" )
		sNew = "0";

	Song* pSong = GAMESTATE->m_pCurSong;
	float fBeat = GAMESTATE->m_fPlayerBeat[GAMESTATE->m_MasterPlayerNumber];

	float fNewBPM = strtof( sNew, NULL );
	float fCurrentBPM = pSong->m_Timing.GetBPMAtBeat( fBeat );

	if( fNewBPM == fCurrentBPM )
		return;

	pSong->m_Timing.SetBPMAtBeat( fBeat, fNewBPM );
}

void AddSongStop( CString sNew )
{
	if( sNew.empty() )
		sNew = "0";

	sNew.MakeLower();

	if( sNew == "r" )
		sNew = "0";

	bool bDelay = sNew.Right(1) == "d";

	Song* pSong = GAMESTATE->m_pCurSong;
	float fBeat = GAMESTATE->m_fPlayerBeat[GAMESTATE->m_MasterPlayerNumber];

	float fFreezeSeconds = strtof( sNew, NULL );

	pSong->m_Timing.SetStopAtBeat( fBeat, fFreezeSeconds, bDelay );
}

void AddBPM( CString sNew )
{
	if( sNew.empty() )
		sNew = "0";

	sNew.MakeLower();

	if( sNew == "r" )
		sNew = "0";

	Steps* pSteps = GAMESTATE->m_pCurSteps[GAMESTATE->m_MasterPlayerNumber];
	float fBeat = GAMESTATE->m_fPlayerBeat[GAMESTATE->m_MasterPlayerNumber];

	float fNewBPM = strtof( sNew, NULL );
	float fCurrentBPM = pSteps->m_Timing.GetBPMAtBeat( fBeat );

	if( fNewBPM == fCurrentBPM )
		return;

	pSteps->m_Timing.SetBPMAtBeat( fBeat, fNewBPM );
}

void AddStop( CString sNew )
{
	if( sNew.empty() )
		sNew = "0";

	sNew.MakeLower();

	if( sNew == "r" )
		sNew = "0";

	bool bDelay = sNew.Right(1) == "d";

	Steps* pSteps = GAMESTATE->m_pCurSteps[GAMESTATE->m_MasterPlayerNumber];
	float fBeat = GAMESTATE->m_fPlayerBeat[GAMESTATE->m_MasterPlayerNumber];

	float fFreezeSeconds = strtof( sNew, NULL );

	pSteps->m_Timing.SetStopAtBeat( fBeat, fFreezeSeconds, bDelay );
}

void AddSpeed( CString sNew )
{
	if( sNew.empty() )
		sNew = "0";

	sNew.MakeLower();

	Steps* pSteps = GAMESTATE->m_pCurSteps[GAMESTATE->m_MasterPlayerNumber];
	float fBeat = GAMESTATE->m_fPlayerBeat[GAMESTATE->m_MasterPlayerNumber];

	if( sNew == "r" )
	{
		pSteps->m_Timing.DeleteSpeedAtBeat( fBeat );
		return;
	}

	CStringArray asSpeed;
	split( sNew, ",", asSpeed, true );

	for( unsigned s = 0; s<asSpeed.size(); s++ )
	{
		TrimLeft(asSpeed[s]);
		TrimRight(asSpeed[s]);
	}

	SpeedSegment& curr_sseg = pSteps->m_Timing.GetSpeedSegmentAtBeat( fBeat );
	SpeedSegment ss( -1, 1.f, 4.f, 1.f );
	if( curr_sseg.m_fBeat == fBeat )
		ss = curr_sseg;

	switch( asSpeed.size() )
	{
	default:
	case 3:
		ss.m_fFactor = strtof( asSpeed[2], NULL );
	case 2:
		ss.m_fBeats = strtof( asSpeed[1], NULL );
		if( asSpeed[1].Right(1) == "s" )
			ss.m_fBeats *= -1;
	case 1:
		ss.m_fToSpeed = strtof( asSpeed[0], NULL );
		break;
	case 0:
		if( ss.m_fBeat != -1 )
			pSteps->m_Timing.DeleteSpeedAtBeat( fBeat );
		return;
	}

	pSteps->m_Timing.SetSpeedAtBeat( fBeat, ss.m_fToSpeed, ss.m_fBeats, ss.m_fFactor );
}

void AddMultiplier( CString sNew )
{
	if( sNew.empty() )
		sNew = "0";

	Steps* pSteps = GAMESTATE->m_pCurSteps[GAMESTATE->m_MasterPlayerNumber];
	float fBeat = GAMESTATE->m_fPlayerBeat[GAMESTATE->m_MasterPlayerNumber];

	sNew.MakeLower();

	if( sNew == "r" )
	{
		pSteps->m_Timing.DeleteMultiplierAtBeat( fBeat );
		return;
	}

	CStringArray sMult;
	split( sNew, ",", sMult );

	for( CStringArray::iterator it = sMult.begin(); it != sMult.end(); ++it )
	{
		TrimLeft(*it);
		TrimRight(*it);
	}

	MultiplierSegment& curr_mseg = pSteps->m_Timing.GetMultiplierSegmentAtBeat( fBeat );
	MultiplierSegment ms( -1, 1, 1, 1.f, 1.f, 1.f, 1.f );
	if( curr_mseg.m_fBeat == fBeat )
		ms = curr_mseg;

	switch( sMult.size() )
	{
	default:
	case 6:
		ms.m_fScoreMiss = strtof( sMult[5], NULL );
		ms.m_fLifeMiss = strtof( sMult[4], NULL );
		ms.m_fScoreHit = strtof( sMult[3], NULL );
		ms.m_fLifeHit = strtof( sMult[2], NULL );
		ms.m_uMiss = atoi( sMult[1] );
		ms.m_uHit = atoi( sMult[0] );
		break;
	case 5:
		ms.m_fScoreMiss = ms.m_fLifeMiss = strtof( sMult[4], NULL );
		ms.m_fScoreHit = strtof( sMult[3], NULL );
		ms.m_fLifeHit = strtof( sMult[2], NULL );
		ms.m_uMiss = atoi( sMult[1] );
		ms.m_uHit = atoi( sMult[0] );
		break;
	case 4:
		ms.m_fLifeHit = ms.m_fLifeMiss = strtof( sMult[3], NULL );
		ms.m_fScoreHit = ms.m_fScoreMiss = strtof( sMult[2], NULL );
		ms.m_uMiss = atoi( sMult[1] );
		ms.m_uHit = atoi( sMult[0] );
		break;
	case 3:
		ms.m_fScoreHit = ms.m_fScoreMiss = ms.m_fLifeHit = ms.m_fLifeMiss = strtof( sMult[2], NULL );
		ms.m_uMiss = atoi( sMult[1] );
		ms.m_uHit = atoi( sMult[0] );
		break;
	case 2:
		ms.m_fScoreHit = ms.m_fScoreMiss = ms.m_fLifeHit = ms.m_fLifeMiss = strtof( sMult[1], NULL );
		ms.m_uHit = ms.m_uMiss = atoi( sMult[0] );
		break;
	case 1:
		ms.m_fScoreMiss = ms.m_fLifeMiss = ms.m_fScoreHit = ms.m_fLifeHit = strtof( sMult[0], NULL );
		ms.m_uMiss = ms.m_uHit = atoi( sMult[0] );
		break;
	case 0:
		if( ms.m_fBeat != -1 )
			pSteps->m_Timing.DeleteMultiplierAtBeat( fBeat );
		return;
	}

	pSteps->m_Timing.SetMultiplierAtBeat( fBeat, ms.m_uHit, ms.m_uMiss, ms.m_fLifeHit, ms.m_fScoreHit, ms.m_fLifeMiss, ms.m_fScoreMiss );
}

float fFakeBeat;

void AddFake( CString sNew )
{
	if( sNew.empty() )
		sNew = "0";

	sNew.MakeLower();

	if( sNew == "r" )
		sNew = "0";

	Steps* pSteps = GAMESTATE->m_pCurSteps[GAMESTATE->m_MasterPlayerNumber];
	float fBeat = fFakeBeat;

	float fNewFake = strtof( sNew, NULL );

	pSteps->m_Timing.SetFakeAtBeat( fBeat, fNewFake );
}

float fSpeedAreaBeat;

void AddSpeedArea( CString sNew )
{
	if( sNew.empty() )
		sNew = "0";

	sNew.MakeLower();

	if( sNew == "r" )
		sNew = "0";

	Steps* pSteps = GAMESTATE->m_pCurSteps[GAMESTATE->m_MasterPlayerNumber];
	float fBeat = fSpeedAreaBeat;

	float fNewSpeedArea = strtof( sNew, NULL );

	pSteps->m_Timing.SetSpeedAreaAtBeat( fBeat, 0.f, 0.f, fNewSpeedArea, 0.f, 1.f );
}

void ChangeSongMusicOffset( CString sNew )
{
	if( sNew.empty() )
		sNew = "0";

	sNew.MakeLower();

	bool bAllSteps = sNew.Right(1) == "a";

	float fBeat0Offset = strtof( sNew, NULL );

	Song* pSong = GAMESTATE->m_pCurSong;
	pSong->m_Timing.m_fBeat0Offset = fBeat0Offset;

	if( bAllSteps )
	{
		const vector<Steps*> sIter = pSong->GetAllSteps();
		for( unsigned i=0; i<sIter.size(); i++ )
		{
			if( sIter[i]->IsAutogen() )
				continue;

			sIter[i]->m_Timing.m_fBeat0Offset = fBeat0Offset;
		}
	}
}

void ChangeMusicOffset( CString sNew )
{
	if( sNew.empty() )
		sNew = "0";

	sNew.MakeLower();

	bool bAllSteps = sNew.Right(1) == "a";

	float fBeat0Offset = strtof( sNew, NULL );

	if( bAllSteps )
	{
		Song* pSong = GAMESTATE->m_pCurSong;
		pSong->m_Timing.m_fBeat0Offset = fBeat0Offset;

		const vector<Steps*> sIter = pSong->GetAllSteps();
		for( unsigned i=0; i<sIter.size(); i++ )
		{
			if( sIter[i]->IsAutogen() )
				continue;

			sIter[i]->m_Timing.m_fBeat0Offset = fBeat0Offset;
		}
	}
	else
	{
		Steps* pSteps = GAMESTATE->m_pCurSteps[GAMESTATE->m_MasterPlayerNumber];
		pSteps->m_Timing.m_fBeat0Offset = fBeat0Offset;
	}
}

void ChangeSampleStart( CString sNew )
{
	Song* pSong = GAMESTATE->m_pCurSong;

	if( sNew.empty() )
		sNew = "0";

	sNew.MakeLower();

	float fMusicSampleStartSeconds = abs( strtof( sNew, NULL ) );

	if( fMusicSampleStartSeconds != pSong->m_fMusicSampleStartSeconds || sNew == "d" ) {
		if( pSong->m_fMusicSampleStartSeconds + pSong->m_fMusicSampleLengthSeconds > pSong->m_fMusicLengthSeconds || sNew == "d" )
		{
			pSong->m_fMusicSampleStartSeconds = pSong->m_Timing.GetElapsedTimeFromBeat( 100 );

			if( pSong->m_fMusicSampleStartSeconds + pSong->m_fMusicSampleLengthSeconds > pSong->m_fMusicLengthSeconds )
			{
				// fix for BAG and other slow songs
				int iBeat = (int)(pSong->m_Timing.m_fLastBeat/2);
				/* Er.  I see that this truncates the beat down to a multiple
				* of 10, but what's the logic behind doing that?  (It'd make
				* sense to use a multiple of 4, so we try to line up to a
				* measure ...) -glenn */
				iBeat = iBeat - (iBeat % 4);
				pSong->m_fMusicSampleStartSeconds = pSong->m_Timing.GetElapsedTimeFromBeat( (float)iBeat );
			}
		}
		else
			pSong->m_fMusicSampleStartSeconds = fMusicSampleStartSeconds;
	}
}

void ChangeSampleLength( CString sNew )
{
	Song* pSong = GAMESTATE->m_pCurSong;

	sNew.MakeLower();
	float fMusicSampleLengthSeconds = abs( strtof( sNew, NULL ) );

	if( fMusicSampleLengthSeconds == 0 || sNew == "d" )
		pSong->m_fMusicSampleLengthSeconds = DEFAULT_MUSIC_SAMPLE_LENGTH;
	else if( fMusicSampleLengthSeconds != pSong->m_fMusicSampleLengthSeconds )
		pSong->m_fMusicSampleLengthSeconds = fMusicSampleLengthSeconds;
}

void ScreenEdit::HandleTimingMenuChoice( TimingMenuChoice ch, int* iAnswers )
{
	float fBeat = GAMESTATE->m_fPlayerBeat[m_PlayerNumber];
	switch( ch )
	{
	// TODO: Timing Mode
	case timing_mode:
		m_bBGEditMode = !( iAnswers[ch] == 0 );
		GAMESTATE->m_EditMode = m_bBGEditMode ? GAMESTATE->MODE_BACKGROUND : GAMESTATE->MODE_EDITING;
		return;

	case timing_bpm:
		{
			float fBPM = m_bBGEditMode ? m_pSong->m_Timing.GetBPMAtBeat(fBeat) : m_pSteps->m_Timing.GetBPMAtBeat(fBeat);

			SCREENMAN->TextEntry( SM_None,
				ssprintf("BPM at current beat (%.3f).\n0 or R to remove it.\n\nCurrent BPM: %.3f\n\nPress Enter to confirm, ESC to cancel.", fBeat, fBPM ),
				"", m_bBGEditMode ? AddSongBPM : AddBPM, NULL );
			ResetAutoSave();
			break;
		}
	case timing_stop:
		{
			TimingData* timing = m_bBGEditMode ? &m_pSong->m_Timing : &m_pSteps->m_Timing;
			StopSegment* ss = NULL;
			if( timing->GetStopLengthAtBeat(fBeat) > 0 )
				ss = &timing->GetStopSegmentAtBeat( fBeat );

			SCREENMAN->TextEntry( SM_None,
				ssprintf("Stop at current beat (%.3f).\n0 or R to remove it.\n\nAppend a 'd' to the end to set a delay.\nEx. 0.12d\n\nCurrent Stop: %.3f%s\n\nPress Enter to confirm, ESC to cancel.", fBeat, ss ? ss->m_fSeconds : 0.f, ss && ss->m_bDelay ? "d" : "" ),
				"", m_bBGEditMode ? AddSongStop : AddStop, NULL );
			ResetAutoSave();
			break;
		}

	// TODO: Rows per Beat & Beats per Measure
	case timing_rpb:
	case timing_bpme:
		return;

	case timing_sample_start:
		SCREENMAN->TextEntry( SM_None, "Preview Music Start in seconds.\n\nD for default\n\nPress Enter to confirm, ESC to cancel.", "", ChangeSampleStart, NULL );
		ResetAutoSave();
		break;
	case timing_sample_length:
		SCREENMAN->TextEntry( SM_None, "Preview Music Length in seconds.\n\nD for default\n\nPress Enter to confirm, ESC to cancel.", "", ChangeSampleLength, NULL );
		ResetAutoSave();
		break;

	case timing_offset:
		SCREENMAN->TextEntry( SM_None, "Beat 0 Offset in seconds.\n\nAppend an 'a' to the end to apply it every difficulty.\nEx. 1.5a\nPress Enter to confirm, ESC to cancel.", "", m_bBGEditMode ? ChangeSongMusicOffset : ChangeMusicOffset, NULL );
		ResetAutoSave();
		break;

	case timing_fake:
		{
			AreaSegment* fk = m_pSteps->m_Timing.IsFakeAreaAtBeat( fBeat ) ? &m_pSteps->m_Timing.GetFakeSegmentAtBeat( fBeat ) : NULL;
			fFakeBeat = fk ? fk->m_fBeat : fBeat;

			SCREENMAN->TextEntry( SM_None,
				ssprintf( "Fake length at beat %.2f.\nR removes the area.\n\nLength in Beats: %.3f\n\nPress Enter to confirm, ESC to cancel.", fFakeBeat, fk ? fk->m_fBeats : 0 ),
				"", AddFake, NULL );
			ResetAutoSave();
			break;
		}

	case timing_speed_area:
		{
			SpeedAreaSegment* sa = m_pSteps->m_Timing.IsSpeedAreaAtBeat( fBeat ) ? &m_pSteps->m_Timing.GetSpeedAreaSegmentAtBeat( fBeat ) : NULL;
			fSpeedAreaBeat = sa ? sa->m_fBeat : fBeat;

			SCREENMAN->TextEntry( SM_None,
				ssprintf( "Mystery Block length at beat %.2f.\nR removes the mystery block.\n\nLength in Beats: %.3f\n\nPress Enter to confirm, ESC to cancel.", fSpeedAreaBeat, sa ? sa->m_fBeats : 0 ),
				"", AddSpeedArea, NULL );
			ResetAutoSave();
			break;
		}

	case timing_tick:
		{
			switch( iAnswers[ch] )
			{
			case 0:
				m_pSteps->m_Timing.SetTickcountAtBeat( fBeat, 0 );
				break;
			case 1:
				m_pSteps->m_Timing.SetTickcountAtBeat( fBeat, 1 );
				break;
			case 2:
				m_pSteps->m_Timing.SetTickcountAtBeat( fBeat, 2 );
				break;
			case 3:
				m_pSteps->m_Timing.SetTickcountAtBeat( fBeat, 3 );
				break;
			case 4:
				m_pSteps->m_Timing.SetTickcountAtBeat( fBeat, 4 );
				break;
			case 5:
				m_pSteps->m_Timing.SetTickcountAtBeat( fBeat, 6 );
				break;
			case 6:
				m_pSteps->m_Timing.SetTickcountAtBeat( fBeat, 8 );
				break;
			case 7:
				m_pSteps->m_Timing.SetTickcountAtBeat( fBeat, 12 );
				break;
			case 8:
				m_pSteps->m_Timing.SetTickcountAtBeat( fBeat, 16 );
				break;
			case 9:
				m_pSteps->m_Timing.SetTickcountAtBeat( fBeat, 24 );
				break;
			case 10:
				m_pSteps->m_Timing.SetTickcountAtBeat( fBeat, 32 );
				break;
			case 11:
				m_pSteps->m_Timing.SetTickcountAtBeat( fBeat, 48 );
				break;
			case 12:
				m_pSteps->m_Timing.SetTickcountAtBeat( fBeat, 64 );
				break;
			case 13:
				m_pSteps->m_Timing.SetTickcountAtBeat( fBeat, 96 );
				break;
			case 14:
				m_pSteps->m_Timing.SetTickcountAtBeat( fBeat, 192 );
				break;
			default:
				ASSERT(0);
			}
			//unsigned uTick = m_pSteps->m_Timing.GetTickcountAtBeat(fBeat);

			//SCREENMAN->TextEntry( SM_None,
			//	ssprintf( "Tickcount at current beat (%.3f).\nR to remove it.\n\nMust be a divisor of 192 or zero.\n\nCurrent Tickcount: %u\n\nPress Enter to confirm, ESC to cancel.", fBeat, uTick ),
			//	"", AddTick, NULL );
			ResetAutoSave();
		break;
		}
	case timing_mult:
		{
			MultiplierSegment& ml = m_pSteps->m_Timing.GetMultiplierSegmentAtBeat( fBeat );

			SCREENMAN->TextEntry( SM_None,
				ssprintf( "Multiplier at current beat (%.3f).\nR to remove it.\n\n1 to 6 values separated with commas.\nEx. 3,1,1.5,1.0\n\nCombo Hit: %u, Combo Miss: %u\nLife Hit: %.3f, Score Hit: %.3f,\nLife Miss: %.3f, Score Miss: %.3f\n\nPress Enter to confirm, ESC to cancel.", fBeat, ml.m_uHit, ml.m_uMiss, ml.m_fLifeHit, ml.m_fScoreHit, ml.m_fLifeMiss, ml.m_fScoreMiss ),
				"", AddMultiplier, NULL );
			ResetAutoSave();
			break;
		}
	case timing_speed:
		{
			SpeedSegment& ss = m_pSteps->m_Timing.GetSpeedSegmentAtBeat( fBeat );

			SCREENMAN->TextEntry( SM_None,
				ssprintf( "Speed at current beat (%.3f).\nR to remove it.\n\n1 to 3 values separated with commas.\nEx. 1.5,2.0s,0.5\nAppend an 's' to the 2nd value for seconds.\n\nCurrent Speed: %.3f\nLength in %s: %.3f\nApproaching Speed: %.3f%s\n\nPress Enter to confirm, ESC to cancel.", fBeat, ss.m_fToSpeed, ss.m_fBeats < 0 ? "Seconds" : "Beats", fabsf(ss.m_fBeats), ss.m_fFactor, ss.m_fFactor == 1.f ? "" : ss.m_fFactor > 1.f ? " (Slower)" : " (Faster)" ),
				"", AddSpeed, NULL );
			ResetAutoSave();
			break;
		}

	case timing_spacer1:
	case timing_spacer2:
	case timing_spacer3:
	case timing_spacer4:
	case timing_spacer5:
		return;

	default:
		ASSERT(0);
	};
}

void ScreenEdit::HandleInsertNotesChoice( InsertNotesChoice c, int* iAnswers )
{
	TapNote note;
	switch( c )
	{
	case tap:
		note = TAP_ORIGINAL_TAP;
		note.playerNumber = (unsigned char)m_uCurrentPlayer;
		break;
	case mine:			note = TAP_ORIGINAL_MINE;			break;
	case shock:			note = TAP_ORIGINAL_SHOCK;			break;
	case potion:		note = TAP_ORIGINAL_POTION;			break;
	case lift:			note = TAP_ORIGINAL_LIFT;			break;
	case hidden:		note = TAP_ORIGINAL_HIDDEN;			break;
	case remove_note:
	default:			note = TAP_EMPTY;
	}
	m_uCurrentItem = note.type;
	m_NoteFieldEdit.SetTapNote( m_uCurrentCol, m_uCurrentPlayerRow, note );
	m_uCurrentCol = 0;
	m_uCurrentPlayerRow = 0;
	ResetAutoSave();
}

void ScreenEdit::HandleHoldRollChoice( HoldRollChoice c, int* iAnswers )
{
	ASSERT( m_iCurrentHold >= 0 );

	HoldNote &hn = m_NoteFieldEdit.GetHoldNote( m_iCurrentHold );
	switch( c )
	{
	case hold2roll:
		hn.subtype = HOLD_TYPE_ROLL;
		break;
	case roll2hold:
		hn.subtype = HOLD_TYPE_DANCE;
		break;
	case remove_holdroll:
		m_NoteFieldEdit.RemoveHoldNote( m_iCurrentHold );
	}
	m_iCurrentHold = -1;
	ResetAutoSave();
}

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * Contains code (c) 2001-2004 Chris Danford.
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
