#include "global.h"
#include "NotesLoaderSM.h"
#include "RageException.h"
#include "MsdFile.h"
#include "RageLog.h"
#include "RageUtil.h"
#include "SongManager.h"
#include "RageFileManager.h"
#include "PrefsManager.h"
#include "NoteTypes.h"

#define MAX_EDIT_SIZE_BYTES  20*1024	// 20 KB

TimingData	m_Timing;

void SMLoader::LoadFromSMTokens(
	CString sStepsType,
	CString sDescription,
	CString sCredit,
	CString sDifficulty,
	CString sMeter,
	CStringArray sAttackData,
	CString sRadarValues,
	CString sNoteData,
	Steps &out
)
{
	TrimLeft(sStepsType);
	TrimRight(sStepsType);
	TrimLeft(sDescription);
	TrimRight(sDescription);
	TrimLeft(sCredit);
	TrimRight(sCredit);
	TrimLeft(sDifficulty);
	TrimRight(sDifficulty);

	//LOG->Trace( "Steps::LoadFromSMTokens()" );

	out.m_bLoadedFromSM = true;
	out.m_bHiddenDifficulty = false;	// By default, difficulty is shown
	out.m_bHasAttacks = false;	// Set this flag here

	out.m_StepsType = StringToStepsType2(sStepsType);
	out.SetDescription(sDescription);
	out.SetDifficulty(StringToDifficulty( sDifficulty ));
	out.m_sCredit = sCredit;

	// HACK:  We used to store SMANIAC as DIFFICULTY_HARD with special description.
	// Now, it has its own DIFFICULTY_CHALLENGE
	if( sDescription.CompareNoCase("smaniac") == 0 )
		out.SetDifficulty( DIFFICULTY_CHALLENGE );
	// HACK:  We used to store CHALLENGE as DIFFICULTY_HARD with special description.
	// Now, it has its own DIFFICULTY_CHALLENGE
	if( sDescription.CompareNoCase("challenge") == 0 )
		out.SetDifficulty( DIFFICULTY_CHALLENGE );

	if( stricmp(sMeter.Right(1),"*") == 0 )
		out.m_bHiddenDifficulty = true;

	out.SetMeter( atoi( sMeter ) );

	CStringArray saValues;
	split( sRadarValues, ",", saValues, true );
	if( saValues.size() == NUM_RADAR_CATEGORIES )
	{
		RadarValues v;
		FOREACH_RadarCategory(rc)
			v[rc] = strtof( saValues[rc], NULL );
		out.SetRadarValues( v );
	}

	out.SetSMNoteData( sNoteData );

	// Attack Data
	{
		Attack attack;
		int iNumAttacks = 0;
		float end = -9999;

		for( unsigned j=0; j < sAttackData.size(); ++j )
		{
			CStringArray sBits;
			split( sAttackData[j], "=", sBits, false );
			if( sBits.size() < 2 )
				continue;

			TrimLeft( sBits[0] );
			TrimRight( sBits[0] );
			if( !sBits[0].CompareNoCase("TIME") )
				attack.fStartSecond = strtof( sBits[1], NULL );
			else if( !sBits[0].CompareNoCase("LEN") )
				attack.fSecsRemaining = strtof( sBits[1], NULL );
			else if( !sBits[0].CompareNoCase("END") )
				end = strtof( sBits[1], NULL );
			else if( !sBits[0].CompareNoCase("MODS") )
			{
				attack.sModifier = sBits[1];

				if( end != -9999 )
				{
					attack.fSecsRemaining = end - attack.fStartSecond;
					end = -9999;
				}

				if( attack.fSecsRemaining < 0.0f )
				{
					LOG->Warn( "Steps \"%s\" have an attack with a nonpositive length: %s", sDescription.c_str(), sBits[1].c_str() );
					attack.fSecsRemaining = 0.0f;
				}

				out.m_Attacks.push_back( attack );
				iNumAttacks++;
			}
		}

		if( iNumAttacks > 0 )
			out.m_bHasAttacks = true;
	}
}

void SMLoader::GetApplicableFiles( CString sPath, CStringArray &out )
{
	GetDirListing( sPath + CString("*.sma"), out );
	GetDirListing( sPath + CString("*.sm"), out );
}

bool SMLoader::LoadTimingFromFile( const CString &fn, TimingData &out )
{
	MsdFile msd;
	if( !msd.ReadFile( fn ) )
	{
		LOG->Warn( "Couldn't load %s, \"%s\"", fn.c_str(), msd.GetError().c_str() );
		return false;
	}

	LoadTimingFromSMFile( msd, out );
	out.m_sFile = fn;
	return true;
}

bool SMLoader::LoadStepTimingFromFile( const CString &fn, TimingData &out, StepsType type, Difficulty diff, int meter, CString& description )
{
	MsdFile msd;
	if( !msd.ReadFile( fn ) )
	{
		LOG->Warn( "Couldn't load %s, \"%s\"", fn.c_str(), msd.GetError().c_str() );
		return false;
	}

	LoadStepTimingFromSMFile( msd, out, type, diff, meter, description );
	out.m_sFile = fn;
	return true;
}

void SMLoader::LoadTimingFromSMFile( const MsdFile &msd, TimingData &out )
{
	m_Timing.Reset();

	CString sTokens[NUM_TIMING_TOKENS];
	bool bTokenLoaded[NUM_TIMING_TOKENS], bEraseStops = true;

	FOREACH_TimingToken( tt )
	{
		sTokens[tt].clear();
		bTokenLoaded[tt] = false;
	}

	// Now, begin loading actual timing data
	for( unsigned i=0; i<msd.GetNumValues(); i++ )
	{
		const MsdFile::value_t &sParams = msd.GetValue(i);
		const CString sValueName = sParams[0];

		// SM files do not have split timing, stop if we find a #NOTES tag
		if (0 == stricmp(sValueName, "NOTES"))
			break;

		else if( 0==stricmp(sValueName,"OFFSET") )
		{
			if( !bTokenLoaded[TIMING_OFFSET] )
			{
				sTokens[TIMING_OFFSET] = sParams[1];
				bTokenLoaded[TIMING_OFFSET] = true;
			}
		}

		else if( 0==stricmp(sValueName,"BPMS") )
		{
			if (bTokenLoaded[TIMING_BPM])
			{
				// If we've already loaded the BPMs, then we are trying to load
				// the BPM changes from a step, so we can stop now
				break;
			}
			else
			{
				sTokens[TIMING_BPM] = sParams[1];
				bTokenLoaded[TIMING_BPM] = true;
			}
		}

		else if( 0==stricmp(sValueName,"STOPS") || 0==stricmp(sValueName,"FREEZES") )
		{
			if( !bTokenLoaded[TIMING_STOP] )
			{
				sTokens[TIMING_STOP] = sParams[1];
				bTokenLoaded[TIMING_STOP] = true;
			}
		}

		else if( 0==stricmp(sValueName,"DELAYS") )
		{
			if( !bTokenLoaded[TIMING_DELAY] )
			{
				sTokens[TIMING_DELAY] = sParams[1];
				bTokenLoaded[TIMING_DELAY] = true;
			}
		}

		else if( 0==stricmp(sValueName,"TICKCOUNT") )
		{
			if( !bTokenLoaded[TIMING_TICKCOUNT] )
			{
				sTokens[TIMING_TICKCOUNT] = sParams[1];
				bTokenLoaded[TIMING_TICKCOUNT] = true;
			}
		}

		else if( 0==stricmp(sValueName,"SPEED") )
		{
			if( !bTokenLoaded[TIMING_SPEED] )
			{
				sTokens[TIMING_SPEED] = sParams[1];
				bTokenLoaded[TIMING_SPEED] = true;
			}
		}

		else if( 0==stricmp(sValueName,"MULTIPLIER") )
		{
			if( !bTokenLoaded[TIMING_MULTIPLIER] )
			{
				sTokens[TIMING_MULTIPLIER] = sParams[1];
				bTokenLoaded[TIMING_MULTIPLIER] = true;
			}
		}

		else if( 0==stricmp(sValueName,"FAKES") )
		{
			if( !bTokenLoaded[TIMING_FAKE] )
			{
				sTokens[TIMING_FAKE] = sParams[1];
				bTokenLoaded[TIMING_FAKE] = true;
			}
		}

		else if( 0==stricmp(sValueName,"SPDAREAS") )
		{
			if( !bTokenLoaded[TIMING_SPEED_AREA] )
			{
				sTokens[TIMING_SPEED_AREA] = sParams[1];
				bTokenLoaded[TIMING_SPEED_AREA] = true;
			}
		}
	}

	FOREACH_TimingToken( tt )
	{
		if( bTokenLoaded[tt] )
		{
			switch( tt )
			{
			case TIMING_STOP:
			case TIMING_DELAY:
				if( bEraseStops )
				{
					m_Timing.m_StopSegments.clear();
					bEraseStops = false;
				}
			}

			switch( tt )
			{
			case TIMING_OFFSET:		LoadOffset( sTokens[tt] );		break;
			case TIMING_BPM:		LoadBPMs( sTokens[tt] );		break;
			case TIMING_STOP:		LoadStops( sTokens[tt] );		break;
			case TIMING_DELAY:		LoadStops( sTokens[tt], true );	break;
			case TIMING_TICKCOUNT:	LoadTickcounts( sTokens[tt] );	break;
			case TIMING_SPEED:		LoadSpeeds( sTokens[tt] );		break;
			case TIMING_MULTIPLIER:	LoadMultipliers( sTokens[tt] );	break;
			case TIMING_FAKE:		LoadFakes( sTokens[tt] );		break;
			case TIMING_SPEED_AREA:	LoadSpeedAreas( sTokens[tt] );	break;
			}
		}
	}

	out = m_Timing;
}

void SMLoader::LoadStepTimingFromSMFile( const MsdFile &msd, TimingData &out, StepsType type, Difficulty diff, int meter, CString& description )
{
	LoadTimingFromSMFile( msd, out );

	CString sTokens[NUM_TIMING_TOKENS];
	bool bTokenLoaded[NUM_TIMING_TOKENS], bLoadTiming = false;

	FOREACH_TimingToken( tt )
	{
		sTokens[tt].clear();
		bTokenLoaded[tt] = false;
	}

	// Now, begin loading actual timing data
	for( unsigned i=0; i<msd.GetNumValues(); i++ )
	{
		const MsdFile::value_t &sParams = msd.GetValue(i);
		const CString sValueName = sParams[0];

		// Aldo_MX: NOTES will tell us if we are under the correct difficulty
		if( 0==stricmp(sValueName,"NOTES") )
		{
			CString sType = sParams[1];
			CString sDiff = sParams[3];
			CString sMeter = sParams[4];
			TrimLeft( sType );
			TrimLeft( sDiff );
			TrimLeft( sMeter );
			TrimRight( sType );
			TrimRight( sDiff );
			TrimRight( sMeter );

			if( type == StringToStepsType2(sType) && diff == StringToDifficulty( sDiff ) && meter == atoi(sMeter) )
			{
				if( diff != DIFFICULTY_EDIT )
					bLoadTiming = true;
				else
				{
					CString sDescription = sParams[2];
					TrimLeft( sDescription );
					TrimRight( sDescription );
					bLoadTiming = stricmp(sDescription, description) == 0;
				}

				if( bLoadTiming )
					break;
			}

			FOREACH_TimingToken( tt )
			{
				sTokens[tt].clear();
				bTokenLoaded[tt] = false;
			}
		}

		// HACK - Aldo_MX: BGCHANGES has been always the last tag for Song Timing xD
		else if( 0==stricmp(sValueName,"BGCHANGES") )
		{
			bLoadTiming = false;
			FOREACH_TimingToken( tt )
			{
				sTokens[tt].clear();
				bTokenLoaded[tt] = false;
			}
		}

		else if( 0==stricmp(sValueName.Left(6),"OFFSET") )
		{
			if( !bTokenLoaded[TIMING_OFFSET] )
			{
				sTokens[TIMING_OFFSET] = sParams[1];
				bTokenLoaded[TIMING_OFFSET] = true;
			}
		}

		else if( 0==stricmp(sValueName.Left(4),"BPMS" ) )
		{
			if( !bTokenLoaded[TIMING_BPM] )
			{
				sTokens[TIMING_BPM] = sParams[1];
				bTokenLoaded[TIMING_BPM] = true;
			}
		}

		else if( 0==stricmp(sValueName.Left(5),"STOPS" ) || 0==stricmp(sValueName.Left(7),"FREEZES" ) )
		{
			if( !bTokenLoaded[TIMING_STOP] )
			{
				sTokens[TIMING_STOP] = sParams[1];
				bTokenLoaded[TIMING_STOP] = true;
			}
		}

		else if( 0==stricmp(sValueName.Left(6),"DELAYS" ) )
		{
			if( !bTokenLoaded[TIMING_DELAY] )
			{
				sTokens[TIMING_DELAY] = sParams[1];
				bTokenLoaded[TIMING_DELAY] = true;
			}
		}

		else if( 0==stricmp(sValueName.Left(9),"TICKCOUNT" ) )
		{
			if( !bTokenLoaded[TIMING_TICKCOUNT] )
			{
				sTokens[TIMING_TICKCOUNT] = sParams[1];
				bTokenLoaded[TIMING_TICKCOUNT] = true;
			}
		}

		else if( 0==stricmp(sValueName.Left(5),"SPEED" ) )
		{
			if( !bTokenLoaded[TIMING_SPEED] )
			{
				sTokens[TIMING_SPEED] = sParams[1];
				bTokenLoaded[TIMING_SPEED] = true;
			}
		}

		else if( 0==stricmp(sValueName.Left(10),"MULTIPLIER" ) )
		{
			if( !bTokenLoaded[TIMING_MULTIPLIER] )
			{
				sTokens[TIMING_MULTIPLIER] = sParams[1];
				bTokenLoaded[TIMING_MULTIPLIER] = true;
			}
		}

		else if( 0==stricmp(sValueName,"FAKES" ) )
		{
			if( !bTokenLoaded[TIMING_FAKE] )
			{
				sTokens[TIMING_FAKE] = sParams[1];
				bTokenLoaded[TIMING_FAKE] = true;
			}
		}

		else if( 0==stricmp(sValueName,"SPDAREAS" ) )
		{
			if( !bTokenLoaded[TIMING_SPEED_AREA] )
			{
				sTokens[TIMING_SPEED_AREA] = sParams[1];
				bTokenLoaded[TIMING_SPEED_AREA] = true;
			}
		}
	}

	if( bLoadTiming )
	{
		bool bEraseStops = true, bChangeTiming = false;
		FOREACH_TimingToken( tt )
		{
			if( bTokenLoaded[tt] )
			{
				if( !bChangeTiming )
					bChangeTiming = true;

				switch( tt )
				{
				case TIMING_STOP:
				case TIMING_DELAY:
					if( bEraseStops )
					{
						m_Timing.m_StopSegments.clear();
						bEraseStops = false;
					}
				}

				switch( tt )
				{
				case TIMING_OFFSET:		LoadOffset( sTokens[tt] );		break;
				case TIMING_BPM:		LoadBPMs( sTokens[tt] );		break;
				case TIMING_STOP:		LoadStops( sTokens[tt] );		break;
				case TIMING_DELAY:		LoadStops( sTokens[tt], true );	break;
				case TIMING_TICKCOUNT:	LoadTickcounts( sTokens[tt] );	break;
				case TIMING_SPEED:		LoadSpeeds( sTokens[tt] );		break;
				case TIMING_MULTIPLIER:	LoadMultipliers( sTokens[tt] );	break;
				case TIMING_FAKE:		LoadFakes( sTokens[tt] );		break;
				case TIMING_SPEED_AREA:	LoadSpeedAreas( sTokens[tt] );	break;
				}
			}
		}

		if( bChangeTiming )
			out = m_Timing;
	}
}

bool LoadFromBGChangesString( BGChange &change, const CString &sBGChangeExpression )
{
	CStringArray aBGChangeValues;
	split( sBGChangeExpression, "=", aBGChangeValues );

	// To prevent people from causing crashes with extra BGCHANGE paramaters
	aBGChangeValues.resize( min((int)aBGChangeValues.size(),11) );

	switch( aBGChangeValues.size() )
	{
	// Cases 7 through 11 are due to additional BGCHANGES parts in SM 4.0; this is for crash prevention
	// Everything in the additional sections is going to be ignored by the code
	case 11:
	case 10:
	case 9:
	case 8:
	case 7:
	case 6:
		change.m_fRate = strtof( aBGChangeValues[2], NULL );
		change.m_bFadeLast = atoi( aBGChangeValues[3] ) != 0;
		change.m_bRewindMovie = atoi( aBGChangeValues[4] ) != 0;
		change.m_bLoop = atoi( aBGChangeValues[5] ) != 0;
		// fall through
	case 2:
		change.m_fBeat = ( 0==stricmp( aBGChangeValues[0].Right(1), "r" ) ) ? NoteRowToBeat( atoi( aBGChangeValues[0].Left(aBGChangeValues[0].length()-1) ) ) : NoteRowToBeat( BeatToNoteRow( strtof( aBGChangeValues[0], NULL ) ) );
		change.m_sBGName = aBGChangeValues[1];
		return true;
	default:
		LOG->Warn("Invalid #BGCHANGES value \"%s\" was ignored", sBGChangeExpression.c_str());
		return false;
	}
}

bool SMLoader::LoadFromSMFile( CString sPath, Song &out )
{
	LOG->Trace( "Song::LoadFromSMFile(%s)", sPath.c_str() );

	MsdFile msd;
	if( !msd.ReadFile( sPath ) )
		RageException::Throw( "Error opening file \"%s\": %s", sPath.c_str(), msd.GetError().c_str() );

	LoadTimingFromSMFile( msd, out.m_Timing );
	out.m_Timing.m_sFile = sPath;

	CString sCredit = "";
	CStringArray sAttackData;
	DisplayBPM displayBpm = BPM_REAL;
	BPMRange range;

	for( unsigned i=0; i<msd.GetNumValues(); i++ )
	{
		int iNumParams = msd.GetNumParams(i);
		const MsdFile::value_t &sParams = msd.GetValue(i);
		const CString sValueName = sParams[0];

		// handle the data
		// Don't use GetMainAndSubTitlesFromFullTitle; that's only for heuristically
		// splitting other formats that *don't* natively support #SUBTITLE.
		if( 0==stricmp(sValueName,"TITLE") )
			out.m_sMainTitle = sParams[1];

		else if( 0==stricmp(sValueName,"SUBTITLE") )
			out.m_sSubTitle = sParams[1];

		else if( 0==stricmp(sValueName,"ARTIST") )
			out.m_sArtist = sParams[1];

		else if( 0==stricmp(sValueName,"TITLETRANSLIT") )
			out.m_sMainTitleTranslit = sParams[1];

		else if( 0==stricmp(sValueName,"SUBTITLETRANSLIT") )
			out.m_sSubTitleTranslit = sParams[1];

		else if( 0==stricmp(sValueName,"ARTISTTRANSLIT") )
			out.m_sArtistTranslit = sParams[1];

		else if( sValueName=="GENRE" )
			out.m_sGenre = sParams[1];

		else if( 0==stricmp(sValueName,"CREDIT") )
			sCredit = sParams[1];

		else if( 0==stricmp(sValueName,"MENUCOLOR") )
			out.m_sMenuColor = sParams[1];

		else if( 0==stricmp(sValueName,"BANNER") )
			out.m_sBannerFile = sParams[1];

		else if( 0==stricmp(sValueName,"DISC") )
			out.m_sDiscFile = sParams[1];

		else if( 0==stricmp(sValueName,"BACKGROUND") )
			out.m_sBackgroundFile = sParams[1];

		else if( 0==stricmp(sValueName,"PREVIEW") )
			out.m_sPreviewFile = sParams[1];

		// Save "#LYRICS" for later, so we can add an internal lyrics tag.
		else if( 0==stricmp(sValueName,"LYRICSPATH") )
			out.m_sLyricsFile = sParams[1];

		else if( 0==stricmp(sValueName,"CDTITLE") )
			out.m_sCDTitleFile = sParams[1];

		else if( 0==stricmp(sValueName,"INTRO") )
			out.m_sIntroFile = sParams[1];

		else if( 0==stricmp(sValueName,"MUSIC") )
			out.m_sMusicFile = sParams[1];

		else if( 0==stricmp(sValueName,"MUSICLENGTH") )
		{
			if(!FromCache)
				continue;

			out.m_fMusicLengthSeconds = strtof( sParams[1], NULL );
		}

		else if( 0==stricmp(sValueName,"MUSICBYTES") )
			; // ignore

		/* We calculate these.  Some SMs in circulation have bogus values for
		 * these, so make sure we always calculate it ourself. */
		else if( 0==stricmp(sValueName,"FIRSTBEAT") )
		{
			if(!FromCache)
			{
				LOG->Trace("Ignored #FIRSTBEAT (cache only)");
				continue;
			}

			out.m_Timing.m_fFirstBeat = strtof( sParams[1], NULL );
		}

		else if( 0==stricmp(sValueName,"LASTBEAT") )
		{
			if(!FromCache)
			{
				LOG->Trace("Ignored #LASTBEAT (cache only)");
				continue;
			}

			out.m_Timing.m_fLastBeat = strtof( sParams[1], NULL );
		}

		else if( 0==stricmp(sValueName,"SONGFILENAME") )
		{
			if( FromCache )
				out.m_sSongFileName = sParams[1];
		}

		else if( 0==stricmp(sValueName,"HASINTRO") )
		{
			if( FromCache )
				out.m_bHasIntro = atoi( sParams[1] ) != 0;
		}

		else if( 0==stricmp(sValueName,"HASMUSIC") )
		{
			if( FromCache )
				out.m_bHasMusic = atoi( sParams[1] ) != 0;
		}

		else if( 0==stricmp(sValueName,"HASBANNER") )
		{
			if( FromCache )
				out.m_bHasBanner = atoi( sParams[1] ) != 0;
		}

		else if( 0==stricmp(sValueName,"HASDISC") )
		{
			if( FromCache )
				out.m_bHasDisc = atoi( sParams[1] ) != 0;
		}

		else if( 0==stricmp(sValueName,"HASBACKGROUND") )
		{
			if( FromCache )
				out.m_bHasBackground = atoi( sParams[1] ) != 0;
		}

		else if( 0==stricmp(sValueName,"HASPREVIEW") )
		{
			if( FromCache )
				out.m_bHasPreview = atoi( sParams[1] ) != 0;
		}

		else if( 0==stricmp(sValueName,"SAMPLESTART") )
			out.m_fMusicSampleStartSeconds = HHMMSSToSeconds( sParams[1] );

		else if( 0==stricmp(sValueName,"SAMPLELENGTH") )
			out.m_fMusicSampleLengthSeconds = HHMMSSToSeconds( sParams[1] );

		else if( 0==stricmp(sValueName,"DISPLAYBPM") )
		{
			// #DISPLAYBPM:[xxx][xxx:xxx]|[*];
			if( sParams[1].Right(1) != "*" )
			{
				range = BPMRange();
				displayBpm = sParams[1].length() > 1 ? BPM_REAL : BPM_RANDOM;
			}
			else
			{
				displayBpm = BPM_CUSTOM;
				if( iNumParams < 3 )
				{
					CString param = sParams[1];
					range.FromString(param);
				}
				else
					range = BPMRange(strtof(sParams[1], NULL), strtof(sParams[2], NULL));
			}
		}

		else if( 0==stricmp(sValueName,"SELECTABLE") )
		{
			out.m_SelectionDisplay = StringToSelectionDisplay(sParams[1]);
			if( out.m_SelectionDisplay == SELECTION_DISPLAY_INVALID )
			{
				if( atoi(sParams[1]) > 6 )
					out.m_SelectionDisplay = SHOW_ALWAYS;
				else
				{
					out.m_SelectionDisplay = SHOW_NEVER;	// failsafe
					LOG->Warn( "The song file '%s' has an unknown #SELECTABLE value, '%s'; will not display in game.", sPath.c_str(), sParams[1].c_str());
				}
			}
		}

		// Used for SORT_LIST
		else if( 0==stricmp(sValueName,"LISTSORT") )
			out.m_iListSortPosition = abs(atoi( sParams[1] ));

		// TODO: Handle BGCHANGES2, 3, etc.
		else if( 0==stricmp(sValueName,"BGCHANGES") || 0==stricmp(sValueName,"ANIMATIONS") )
		{
			CStringArray aBGChangeExpressions;
			split( sParams[1], ",", aBGChangeExpressions );

			for( unsigned b=0; b<aBGChangeExpressions.size(); b++ )
			{
				BGChange change;
				if( LoadFromBGChangesString( change, aBGChangeExpressions[b] ) )
					out.AddBGChange( change );
			}
		}

		// Attacks loaded from file
		else if( 0==stricmp(sValueName,"ATTACKS") )
		{
			sAttackData.clear();

			// Build the CStringArray here so we can write it to file again later
			// No need to worry about sParams[0], which is the #ATTACK string; this is already taken care of
			for( unsigned s = 1; s < sParams.params.size(); ++s )
				sAttackData.push_back( sParams[s] );	// We'll write the ':' and ';' in NotesWriter
		}

		else if( 0==stricmp(sValueName,"FGCHANGES") )
		{
			CStringArray aFGChangeExpressions;
			split( sParams[1], ",", aFGChangeExpressions );

			for( unsigned b=0; b<aFGChangeExpressions.size(); b++ )
			{
				BGChange change;
				if( LoadFromBGChangesString( change, aFGChangeExpressions[b] ) )
					out.AddForegroundChange( change );
			}
		}

		else if( 0==stricmp(sValueName,"NOTES") )
		{
			if( iNumParams < 7 )
			{
				LOG->Trace( "The song file '%s' has %d fields in a #NOTES tag, but should have at least %d.", sPath.c_str(), iNumParams, 7 );
				continue;
			}

			Steps* pNewNotes = new Steps;
			ASSERT( pNewNotes );

			LoadFromSMTokens( sParams[1], sParams[2], sCredit, sParams[3], sParams[4], sAttackData, sParams[5], sParams[6], *pNewNotes );

			Difficulty diff = pNewNotes->GetDifficulty();
			CString sDescription = pNewNotes->GetDescription();
			LoadStepTimingFromSMFile( msd, pNewNotes->m_Timing, pNewNotes->m_StepsType, diff, pNewNotes->GetMeter(), sDescription );
			pNewNotes->m_Timing.m_sFile = sPath;
			pNewNotes->m_DisplayBPM = displayBpm;
			if( displayBpm == BPM_REAL )
				range = BPMRange(&pNewNotes->m_Timing);
			pNewNotes->m_BPMRange = range;
			pNewNotes->TidyUpData();

			out.AddSteps( pNewNotes );
		}

		// Used in SM 4 CVS / SM4 SVN, but not in 3.9 Plus
		else if( sValueName == "INSTRUMENTTRACK" || sValueName == "LASTBEATHINT" ||
				 sValueName == "LEADTRACK" || sValueName == "MUSICBYTES" ||
				 sValueName == "TIMESIGNATURES" )
			;

		// StepMania's Cache or deprecated tags
		else if( 0==stricmp(sValueName,"MUSICLENGTH") || 0==stricmp(sValueName,"MUSICBYTES") ||
				 0==stricmp(sValueName,"FIRSTBEAT") || 0==stricmp(sValueName,"LASTBEAT") ||
				 0==stricmp(sValueName,"SONGFILENAME") || 0==stricmp(sValueName,"HASMUSIC") ||
				 0==stricmp(sValueName,"HASBANNER") || 0==stricmp(sValueName,"HASDISC") ||
				 0==stricmp(sValueName,"HASBACKGROUND") || 0==stricmp(sValueName,"HASPREVIEW") ||
				 0==stricmp( sValueName, "KEYSOUNDS" ) || 0==stricmp( sValueName, "NEWMETERSYSTEM" ) ||
				 0==stricmp(sValueName, "METERTYPE") )
			;

		// StepMania AMX's tags
		else if( 0==stricmp(sValueName.Left(6),"OFFSET") || 0==stricmp(sValueName.Left(4),"BPMS") ||
				 0==stricmp(sValueName.Left(5),"STOPS") || 0==stricmp(sValueName.Left(7),"FREEZES") ||
				 0==stricmp(sValueName.Left(9),"TICKCOUNT") || 0==stricmp(sValueName.Left(5),"SPEED") ||
				 0==stricmp(sValueName.Left(10),"MULTIPLIER") || 0==stricmp(sValueName.Left(6),"DELAYS") ||
				 0==stricmp(sValueName,"SMVERSION") || 0==stricmp(sValueName,"SMAVERSION") ||
				 0==stricmp(sValueName,"ROWSPERBEAT") || 0==stricmp(sValueName,"BEATSPERMEASURE") ||
				 0==stricmp(sValueName,"FAKES") || 0==stricmp(sValueName,"SPDAREAS" ) )
			;

		else
			LOG->Trace( "Unexpected value named '%s'", sValueName.c_str() );
	}

	return true;
}


bool SMLoader::LoadFromDir( CString sPath, Song &out )
{
	CStringArray aFileNames;
	GetApplicableFiles( sPath, aFileNames );

	if( aFileNames.size() > 1 )
		LOG->Warn( "There is more than one SM or SMA file in '%s'.  There should be only one!", sPath.c_str() );

	// We should have exactly one; if we had none, we shouldn't have been called to begin with.
	ASSERT( aFileNames.size() > 0 );

	return LoadFromSMFile( sPath + aFileNames[0], out );
}

bool SMLoader::LoadEdit( CString sEditFilePath, ProfileSlot slot )
{
	LOG->Trace( "Song::LoadEdit(%s)", sEditFilePath.c_str() );

	int iBytes = FILEMAN->GetFileSizeInBytes( sEditFilePath );
	if( iBytes > MAX_EDIT_SIZE_BYTES )
	{
		LOG->Warn( "The edit '%s' is unreasonably large.  It won't be loaded.", sEditFilePath.c_str() );
		return false;
	}

	MsdFile msd;
	if( !msd.ReadFile( sEditFilePath ) )
		RageException::Throw( "Error opening file \"%s\": %s", sEditFilePath.c_str(), msd.GetError().c_str() );

	Song* pSong = NULL;

	CString sCredit = "";
	CStringArray sAttackData;

	for( unsigned i=0; i<msd.GetNumValues(); i++ )
	{
		int iNumParams = msd.GetNumParams(i);
		const MsdFile::value_t &sParams = msd.GetValue(i);
		const CString sValueName = sParams[0];

		// handle the data
		if( 0==stricmp(sValueName,"SONG") )
		{
			if( pSong )
			{
				LOG->Warn( "The edit file '%s' has more than one #SONG tag.", sEditFilePath.c_str() );
				return false;
			}

			CString sSongFullTitle = sParams[1];
			sSongFullTitle.Replace( '\\', '/' );

			pSong = SONGMAN->FindSong( sSongFullTitle );
			if( pSong == NULL )
			{
				LOG->Warn( "The edit file '%s' required a song '%s' that isn't present.", sEditFilePath.c_str(), sSongFullTitle.c_str() );
				return false;
			}

			if( pSong->GetNumStepsLoadedFromProfile(slot) >= MAX_EDITS_PER_SONG_PER_PROFILE )
			{
				LOG->Warn( "The song '%s' already has the maximum number of edits allowed for ProfileSlotP%d.", sSongFullTitle.c_str(), slot+1 );
				return false;
			}
		}

		else if( 0==stricmp(sValueName,"CREDIT") )
			sCredit = sParams[1];

		else if( 0==stricmp(sValueName,"METERTYPE") )
			;	// ignore

		else if( 0==stricmp(sValueName,"ATTACKS") )
		{
			sAttackData.clear();

			for( unsigned s = 1; s < sParams.params.size(); ++s )
				sAttackData.push_back( sParams[s] );	// We'll write the ':' and ';' in NotesWriter
		}

		else if( 0==stricmp(sValueName,"NOTES") )
		{
			if( pSong == NULL )
			{
				LOG->Warn( "The edit file '%s' has doesn't have a #SONG tag preceeding the first #NOTES tag.", sEditFilePath.c_str() );
				return false;
			}

			if( iNumParams < 7 )
			{
				LOG->Trace( "The song file '%s' is has %d fields in a #NOTES tag, but should have at least %d.", sEditFilePath.c_str(), iNumParams, 7 );
				continue;
			}

			Steps* pNewNotes = new Steps;
			ASSERT( pNewNotes );

			LoadFromSMTokens( sParams[1], sParams[2], sCredit, sParams[3], sParams[4], sAttackData, sParams[5], sParams[6], *pNewNotes );

			pNewNotes->SetLoadedFromProfile( slot );
			pNewNotes->SetDifficulty( DIFFICULTY_EDIT );


			if( pSong->IsEditAlreadyLoaded(pNewNotes) )
			{
				LOG->Warn( "The edit file '%s' is a duplicate of another edit that was already loaded.", sEditFilePath.c_str() );
				SAFE_DELETE( pNewNotes );
				return false;
			}

			pNewNotes->TidyUpData();
			pSong->AddSteps( pNewNotes );
			return true;	// Only allow one Steps per edit file!
		}
		else
			LOG->Trace( "Unexpected value named '%s'", sValueName.c_str() );
	}

	return true;

}

void SMLoader::TidyUpData( Song &song, bool cache )
{
	/*
	 * Hack: if the song has any changes at all (so it won't use a random BGA)
	 * and doesn't end with "-nosongbg-", add a song background BGC.  Remove
	 * "-nosongbg-" if it exists.
	 *
	 * This way, songs that were created earlier, when we added the song BG
	 * at the end by default, will still behave as expected; all new songs will
	 * have to add an explicit song BG tag if they want it.  This is really a
	 * formatting hack only; nothing outside of SMLoader ever sees "-nosongbg-".
	 */
	BGChangeArray &bg = song.m_BGChanges;
	if( !bg.empty() )
	{
		/* BGChanges have been sorted.  On the odd chance that a BGChange exists
		 * with a very high beat, search the whole list. */
		bool bHasNoSongBgTag = false;

		for( unsigned i = 0; !bHasNoSongBgTag && i < bg.size(); ++i )
		{
			if( !bg[i].m_sBGName.CompareNoCase("-nosongbg-") )
			{
				bg.erase( bg.begin()+i );
				bHasNoSongBgTag = true;
			}
		}

		/* If there's no -nosongbg- tag, add the song BG. */
		if( !bHasNoSongBgTag ) do
		{
			/* If we're loading cache, -nosongbg- should always be in there.  We must
			 * not call IsAFile(song.GetBackgroundPath()) when loading cache. */
			if( cache )
				break;

			/* If BGChanges already exist after the last beat, don't add the background
			 * in the middle. */
			if( !bg.empty() && bg.back().m_fBeat-0.0001f >= song.m_Timing.m_fLastBeat )
				break;

			/* If the last BGA is already the song BGA, don't add a duplicate. */
			if( !bg.empty() && !bg.back().m_sBGName.CompareNoCase(song.m_sBackgroundFile) )
				break;

			if( !IsAFile( song.GetBackgroundPath() ) )
				break;

			bg.push_back( BGChange(song.m_Timing.m_fLastBeat, song.m_sBackgroundFile) );
		} while(0);
	}
}

bool SMLoader::LoadOffset( const CString& sTokens )
{
	if( !sTokens.empty() )
	{
		m_Timing.m_fBeat0Offset = (float)atof( sTokens );
		return true;
	}

	m_Timing.m_fBeat0Offset = 0;
	return false;
}

bool SMLoader::LoadBPMs( const CString& sTokens )
{
	m_Timing.m_BPMSegments.clear();

	if( !sTokens.empty() )
	{
		CStringArray asBPMs;
		split( sTokens, ",", asBPMs, true );

		for( unsigned i=0; i<asBPMs.size(); i++ )
		{
			CStringArray sParams;
			split( asBPMs[i], "=", sParams );

			float fBeat;
			int iRow;
			float fBPM = 60.f;

			switch( sParams.size() )
			{
			default:
				LOG->Warn( ssprintf("Invalid BPM value, must have zero to one '=': \"%s\"\n\n\nFile: \"%s\"", asBPMs[i].c_str(), m_Timing.m_sFile.c_str()) );
				if( sParams.size() < 1 )
					continue;
			case 2:
				fBPM = strtof( sParams[1], NULL );
			case 1:
				if( 0==stricmp(sParams[0].Right(1),"r") )
				{
					iRow = atoi( sParams[0].Left(sParams[0].length()-1) );
					fBeat = NoteRowToBeat( iRow );
				}
				else
				{
					fBeat = strtof( sParams[0], NULL );
					iRow = BeatToNoteRow( fBeat );
				}
			}

			m_Timing.SetBPMAtBeat( fBeat, fBPM );
		}
		if( !m_Timing.m_BPMSegments.empty() )
			return true;
	}

	m_Timing.SetBPMAtBeat( 0, 60.f );
	return false;
}

bool SMLoader::LoadStops( const CString& sTokens, bool bDelays )
{
	if( !bDelays )
		m_Timing.m_StopSegments.clear();

	if( !sTokens.empty() )
	{
		CStringArray asStops;
		split( sTokens, ",", asStops, true );

		for( unsigned i=0; i<asStops.size(); i++ )
		{
			CStringArray sParams;
			split( asStops[i], "=", sParams );

			float fBeat;
			int iRow;
			float fSeconds = 0;
			bool bDelay = false;

			switch( sParams.size() )
			{
			default:
				LOG->Warn( ssprintf("Invalid Stop value, must have one to two '=': \"%s\"\n\n\nFile: \"%s\"", asStops[i].c_str(), m_Timing.m_sFile.c_str()) );
				if( sParams.size() < 2 )
					continue;
			case 3:
				bDelay = atoi( sParams[2] ) == 0 ? false : true;
			case 2:
				fSeconds = strtof( sParams[1], NULL );

				if( 0==stricmp(sParams[0].Right(1),"r") )
				{
					iRow = atoi( sParams[0].Left(sParams[0].length()-1) );
					fBeat = NoteRowToBeat( iRow );
				}
				else
				{
					fBeat = strtof( sParams[0], NULL );
					iRow = BeatToNoteRow( fBeat );
				}
			}

			if( bDelays )
				bDelay ^= 1;

			m_Timing.SetStopAtBeat( fBeat, fSeconds, bDelay );
		}
		if( !m_Timing.m_StopSegments.empty() )
			return true;
	}

	return false;
}

bool SMLoader::LoadSpeeds( const CString& sTokens )
{
	m_Timing.m_SpeedSegments.clear();

	if( !sTokens.empty() )
	{
		CStringArray asSpeeds;
		split( sTokens, ",", asSpeeds, true );

		for( unsigned i=0; i<asSpeeds.size(); i++ )
		{
			CStringArray sParams;
			split( asSpeeds[i], "=", sParams );

			float fBeat;
			int iRow;
			float fSpeed = 1.f, fBeats = 4.f, fFactor = 1.f;

			switch( sParams.size() )
			{
			default:
				LOG->Warn( ssprintf("Invalid Speed value, must have zero to three '=': \"%s\"\n\n\nFile: \"%s\"", asSpeeds[i].c_str(), m_Timing.m_sFile.c_str()) );
				if( sParams.size() < 1 )
					continue;
			case 4:
				fFactor = strtof( sParams[3], NULL );
			case 3:
				if( 0==stricmp(sParams[2].Right(1),"s") )
					fBeats = strtof( sParams[2].Left(sParams[2].length()-1), NULL ) * -1;
				else
					fBeats = strtof( sParams[2], NULL );
			case 2:
				fSpeed = strtof( sParams[1], NULL );
			case 1:
				if( 0==stricmp(sParams[0].Right(1),"r") )
				{
					iRow = atoi( sParams[0].Left(sParams[0].length()-1) );
					fBeat = NoteRowToBeat( iRow );
				}
				else
				{
					fBeat = strtof( sParams[0], NULL );
					iRow = BeatToNoteRow( fBeat );
				}
			}

			m_Timing.SetSpeedAtBeat( fBeat, fSpeed, fBeats, fFactor );
		}
		if( !m_Timing.m_SpeedSegments.empty() )
			return true;
	}

	m_Timing.SetSpeedAtBeat( 0, 1.f, 4.f, 1.f );
	return false;
}

bool SMLoader::LoadTickcounts( const CString& sTokens )
{
	m_Timing.m_TickcountSegments.clear();

	if( !sTokens.empty() )
	{
		CStringArray asCheckpoints;
		split( sTokens, ",", asCheckpoints, true );

		for( unsigned i=0; i<asCheckpoints.size(); i++ )
		{
			CStringArray sParams;
			split( asCheckpoints[i], "=", sParams );

			float fBeat;
			int iRow;
			unsigned uNumerator = 2;
			unsigned uDenominator = 4;

			switch( sParams.size() )
			{
			default:
				LOG->Warn( ssprintf("Invalid Tickcount value, must have zero to two '=': \"%s\"\n\n\nFile: \"%s\"", asCheckpoints[i].c_str(), m_Timing.m_sFile.c_str()) );
				if( sParams.size() < 1 )
					continue;
			case 3:
				uDenominator = atoi( sParams[2] );
			case 2:
				uNumerator = atoi( sParams[1] );
			case 1:
				if( 0==stricmp(sParams[0].Right(1),"r") )
				{
					iRow = atoi( sParams[0].Left(sParams[0].length()-1) );
					fBeat = NoteRowToBeat( iRow );
				}
				else
				{
					fBeat = strtof( sParams[0], NULL );
					iRow = BeatToNoteRow( fBeat );
				}
			}

			//SetCheckpointAtBeat( fBeat, uNumerator, uDenominator );
			m_Timing.SetTickcountAtBeat( fBeat, uNumerator );
		}
		if( !m_Timing.m_TickcountSegments.empty() )
			return true;
	}

	m_Timing.SetTickcountAtBeat( 0, 2 );
	return false;
}

bool SMLoader::LoadMultipliers( const CString& sTokens )
{
	m_Timing.m_MultiplierSegments.clear();

	if( !sTokens.empty() )
	{
		CStringArray asMultipliers;
		split( sTokens, ",", asMultipliers, true );

		for( unsigned i=0; i<asMultipliers.size(); i++ )
		{
			CStringArray sParams;
			split( asMultipliers[i], "=", sParams );

			float fBeat;
			int iRow;
			unsigned uHit = 1;
			unsigned uMiss = 1;
			float fLifeHit = 1.f;
			float fScoreHit = 1.f;
			float fLifeMiss = 1.f;
			float fScoreMiss = 1.f;

			switch( sParams.size() )
			{
			default:
				LOG->Warn( ssprintf("Invalid Multiplier value, must have zero to six '=': \"%s\"\n\n\nFile: \"%s\"", asMultipliers[i].c_str(), m_Timing.m_sFile.c_str()) );
				if( sParams.size() < 1 )
					continue;
			case 7:
				fScoreMiss = strtof( sParams[6], NULL );
				fLifeMiss = strtof( sParams[5], NULL );
				fScoreHit = strtof( sParams[4], NULL );
				fLifeHit = strtof( sParams[3], NULL );
				uMiss = atoi( sParams[2] );
				uHit = atoi( sParams[1] );
				if( 0==stricmp(sParams[0].Right(1),"r") )
				{
					iRow = atoi( sParams[0].Left(sParams[0].length()-1) );
					fBeat = NoteRowToBeat( iRow );
				}
				else
				{
					fBeat = strtof( sParams[0], NULL );
					iRow = BeatToNoteRow( fBeat );
				}
				break;
			case 6:
				fScoreMiss = fLifeMiss = strtof( sParams[5], NULL );
				fScoreHit = strtof( sParams[4], NULL );
				fLifeHit = strtof( sParams[3], NULL );
				uMiss = atoi( sParams[2] );
				uHit = atoi( sParams[1] );
				if( 0==stricmp(sParams[0].Right(1),"r") )
				{
					iRow = atoi( sParams[0].Left(sParams[0].length()-1) );
					fBeat = NoteRowToBeat( iRow );
				}
				else
				{
					fBeat = strtof( sParams[0], NULL );
					iRow = BeatToNoteRow( fBeat );
				}
				break;
			case 5:
				fLifeHit = fLifeMiss = strtof( sParams[4], NULL );
				fScoreHit = fScoreMiss = strtof( sParams[3], NULL );
				uMiss = atoi( sParams[2] );
				uHit = atoi( sParams[1] );
				if( 0==stricmp(sParams[0].Right(1),"r") )
				{
					iRow = atoi( sParams[0].Left(sParams[0].length()-1) );
					fBeat = NoteRowToBeat( iRow );
				}
				else
				{
					fBeat = strtof( sParams[0], NULL );
					iRow = BeatToNoteRow( fBeat );
				}
				break;
			case 4:
				fScoreHit = fScoreMiss = fLifeHit = fLifeMiss = strtof( sParams[3], NULL );
				uMiss = atoi( sParams[2] );
				uHit = atoi( sParams[1] );
				if( 0==stricmp(sParams[0].Right(1),"r") )
				{
					iRow = atoi( sParams[0].Left(sParams[0].length()-1) );
					fBeat = NoteRowToBeat( iRow );
				}
				else
				{
					fBeat = strtof( sParams[0], NULL );
					iRow = BeatToNoteRow( fBeat );
				}
				break;
			case 3:
				fScoreHit = fScoreMiss = fLifeHit = fLifeMiss = strtof( sParams[2], NULL );
				uHit = uMiss = atoi( sParams[1] );
				if( 0==stricmp(sParams[0].Right(1),"r") )
				{
					iRow = atoi( sParams[0].Left(sParams[0].length()-1) );
					fBeat = NoteRowToBeat( iRow );
				}
				else
				{
					fBeat = strtof( sParams[0], NULL );
					iRow = BeatToNoteRow( fBeat );
				}
				break;
			case 2:
				fScoreMiss = fLifeMiss = fScoreHit = fLifeHit = strtof( sParams[1], NULL );
				uMiss = uHit = atoi( sParams[1] );
				if( 0==stricmp(sParams[0].Right(1),"r") )
				{
					iRow = atoi( sParams[0].Left(sParams[0].length()-1) );
					fBeat = NoteRowToBeat( iRow );
				}
				else
				{
					fBeat = strtof( sParams[0], NULL );
					iRow = BeatToNoteRow( fBeat );
				}
				break;
			case 1:
				if( 0==stricmp(sParams[0].Right(1),"r") )
				{
					iRow = atoi( sParams[0].Left(sParams[0].length()-1) );
					fBeat = NoteRowToBeat( iRow );
				}
				else
				{
					fBeat = strtof( sParams[0], NULL );
					iRow = BeatToNoteRow( fBeat );
				}
			}

			m_Timing.SetMultiplierAtBeat( fBeat, uHit, uMiss, fLifeHit, fScoreHit, fLifeMiss, fScoreMiss );
		}
		if( !m_Timing.m_MultiplierSegments.empty() )
			return true;
	}

	m_Timing.SetMultiplierAtBeat( 0, 1, 1, 1.f, 1.f, 1.f, 1.f );
	return false;
}

bool SMLoader::LoadFakes( const CString& sTokens )
{
	m_Timing.m_FakeAreaSegments.clear();

	if( !sTokens.empty() )
	{
		CStringArray asFakes;
		split( sTokens, ",", asFakes, true );

		for( unsigned i=0; i<asFakes.size(); i++ )
		{
			CStringArray sParams;
			split( asFakes[i], "=", sParams );

			float fBeat;
			int iRow;
			float fBeats = 4.f;

			switch( sParams.size() )
			{
			default:
				LOG->Warn( ssprintf("Invalid Fake value, must have zero to one '=': \"%s\"\n\n\nFile: \"%s\"", asFakes[i].c_str(), m_Timing.m_sFile.c_str()) );
				if( sParams.size() < 1 )
					continue;
			case 2:
				fBeats = strtof( sParams[1], NULL );
			case 1:
				if( 0==stricmp(sParams[0].Right(1),"r") )
				{
					iRow = atoi( sParams[0].Left(sParams[0].length()-1) );
					fBeat = NoteRowToBeat( iRow );
				}
				else
				{
					fBeat = strtof( sParams[0], NULL );
					iRow = BeatToNoteRow( fBeat );
				}
			}

			m_Timing.SetFakeAtBeat( fBeat, fBeats );
		}
		if( !m_Timing.m_FakeAreaSegments.empty() )
			return true;
	}

	return false;
}

bool SMLoader::LoadSpeedAreas( const CString& sTokens )
{
	m_Timing.m_SpeedAreaSegments.clear();

	if( !sTokens.empty() )
	{
		CStringArray asSpeedAreas;
		split( sTokens, ",", asSpeedAreas, true );

		for( unsigned i=0; i<asSpeedAreas.size(); i++ )
		{
			CStringArray sParams;
			split( asSpeedAreas[i], "=", sParams );

			float fBeat;
			int iRow;
			float fFromSpeed = 1.f, fToSpeed = 1.f, fBeats = 4.f, fTriggerOffset = 0.f, fFactor = 1.f;

			switch( sParams.size() )
			{
			default:
				LOG->Warn( ssprintf("Invalid Speed Area value, must have zero to five '=': \"%s\"\n\n\nFile: \"%s\"", asSpeedAreas[i].c_str(), m_Timing.m_sFile.c_str()) );
				if( sParams.size() < 1 )
					continue;

			case 6:
				fFactor = strtof( sParams[5], NULL );

			case 5:
				fTriggerOffset = strtof( sParams[4], NULL );

			case 4:
				if( 0==stricmp(sParams[3].Right(1),"s") )
					fBeats = strtof( sParams[3].Left(sParams[3].length()-1), NULL ) * -1;
				else
					fBeats = strtof( sParams[3], NULL );

			case 3:
				fToSpeed = strtof( sParams[2], NULL );

			case 2:
				fFromSpeed = strtof( sParams[1], NULL );

				if( sParams.size() == 2 )
					fToSpeed = fFromSpeed;

			case 1:
				if( 0==stricmp(sParams[0].Right(1),"r") )
				{
					iRow = atoi( sParams[0].Left(sParams[0].length()-1) );
					fBeat = NoteRowToBeat( iRow );
				}
				else
				{
					fBeat = strtof( sParams[0], NULL );
					iRow = BeatToNoteRow( fBeat );
				}
			}

			m_Timing.SetSpeedAreaAtBeat(fBeat, fFromSpeed, fToSpeed, fBeats, fTriggerOffset, fFactor);
		}
		if( !m_Timing.m_SpeedAreaSegments.empty() )
			return true;
	}

	return false;
}

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * Contains code (c) 2001-2004 Chris Danford, Glenn Maynard.
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
