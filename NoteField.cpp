#include "global.h"
#include "NoteField.h"
#include "RageUtil.h"
#include "GameConstantsAndTypes.h"
#include "PrefsManager.h"
#include "ArrowEffects.h"
#include "GameManager.h"
#include "GameState.h"
#include "RageException.h"
#include "RageTimer.h"
#include "RageLog.h"
#include "RageMath.h"
#include "ThemeManager.h"
#include "NoteFieldPositioning.h"
#include "NoteSkinManager.h"
#include "Song.h"
#include "Steps.h"

NoteField::NoteField()
{
	m_textMeasureNumber.LoadFromFont( THEME->GetPathF("Common", "normal") );
	m_textMeasureNumber.SetZoom( 1.0f );
	m_textMeasureNumber.SetShadowLength( THEME->GetMetricF("ScreenEdit", "MeasureTextShadowLength") );

	m_textTiming.LoadFromFont( THEME->GetPathF("Common", "normal") );
	m_textTiming.SetZoom( 1.0f );
	m_textTiming.SetShadowLength( THEME->GetMetricF("ScreenEdit", "TimingTextShadowLength") );

	m_rectMarkerBar.SetShadowLength( 0 );
	m_rectMarkerBar.SetEffectDiffuseShift( 2, RageColor(1,1,1,0.5f), RageColor(0.5f,0.5f,0.5f,0.5f) );

	m_sprBars.Load( THEME->GetPathToG("NoteField bars") );
	m_sprBars.StopAnimating();

	m_bRDCols = THEME->GetMetricB("NoteField","ReverseColumnDrawing");
	m_bRDRows = THEME->GetMetricB("NoteField","ReverseRowDrawing");
	m_bRDHolds = THEME->GetMetricB("NoteField","ReverseHoldDrawing");
	m_bDHoldsFirst = THEME->GetMetricB("NoteField","DrawHoldsFirst");

	m_fBeginMarker = m_fEndMarker = -1;

	{
		CString at = THEME->GetMetric("NoteField", "AnimationTimingForNotes");
		m_notesAnimationTiming = StringToAnimationTiming(at);
		if (m_notesAnimationTiming == AT_INVALID) {
			m_notesAnimationTiming = THEME->GetMetricB("NoteField", "UseBGATimingForNoteAnimation") ? AT_BGA : AT_STEP;
		}
	}

	{
		CString at = THEME->GetMetric("NoteField", "AnimationTimingForReceptor");
		m_receptorAnimationTiming = StringToAnimationTiming(at);
		if (m_receptorAnimationTiming == AT_INVALID) {
			m_receptorAnimationTiming = THEME->GetMetricB("NoteField", "UseBGATimingForReceptorAnimation") ? AT_BGA : AT_STEP;
		}
	}

	m_fPercentFadeToFail = -1;
	m_fAlphaNotes = 1.f;
	LastDisplay = NULL;

	m_ColorBPM = THEME->GetMetricC("ScreenEdit","BPMTextColor");
	m_ColorStop = THEME->GetMetricC("ScreenEdit","StopTextColor");
	m_ColorStopX = THEME->GetMetricC("ScreenEdit","StopTextColor2");
	m_ColorSpeed = THEME->GetMetricC("ScreenEdit","SpeedTextColor");
	m_ColorSpeedX = THEME->GetMetricC("ScreenEdit","SpeedTextColor2");
	m_ColorTickcount = THEME->GetMetricC("ScreenEdit","TickcountTextColor");
	m_ColorMultiplier = THEME->GetMetricC("ScreenEdit","MultiplierTextColor");
	m_ColorMultiplierX = THEME->GetMetricC("ScreenEdit","MultiplierTextColor2");
	m_ColorBGA = THEME->GetMetricC("ScreenEdit","BGATextColor");

	m_fBPMTextX = THEME->GetMetricF("ScreenEdit","BPMTextX");
	m_fStopTextX = THEME->GetMetricF("ScreenEdit","StopTextX");
	m_fSpeedTextX = THEME->GetMetricF("ScreenEdit","SpeedTextX");
	m_fTickcountTextX = THEME->GetMetricF("ScreenEdit","TickcountTextX");
	m_fMultiplierTextX = THEME->GetMetricF("ScreenEdit","MultiplierTextX");
	m_fBGATextX = THEME->GetMetricF("ScreenEdit","BGATextX");

	m_fBPMTextZoom = THEME->GetMetricF("ScreenEdit","BPMTextZoom");
	m_fStopTextZoom = THEME->GetMetricF("ScreenEdit","StopTextZoom");
	m_fSpeedTextZoom = THEME->GetMetricF("ScreenEdit","SpeedTextZoom");
	m_fTickcountTextZoom = THEME->GetMetricF("ScreenEdit","TickcountTextZoom");
	m_fMultiplierTextZoom = THEME->GetMetricF("ScreenEdit","MultiplierTextZoom");
	m_fBGATextZoom = THEME->GetMetricF("ScreenEdit","BGATextZoom");

	m_bHalfMN = THEME->GetMetricB("ScreenEdit","DrawHalfMeasureNumbers");
	m_bQuarterMN = THEME->GetMetricB("ScreenEdit","DrawQuarterMeasureNumbers");

	m_iExtraWidth = THEME->GetMetricI("ScreenEdit","MeasureBarsExtraWidth");
}

NoteField::~NoteField()
{
	Unload();
}

void NoteField::Unload()
{
	for( map<CString, NoteDisplayCols *>::iterator it = m_NoteDisplays.begin();
		it != m_NoteDisplays.end(); ++it )
		delete it->second;
	m_NoteDisplays.clear();
	LastDisplay = NULL;
}

void NoteField::Reload()
{
	Unload();

	m_fPercentFadeToFail = -1;
	m_fAlphaNotes = 1.f;
	m_LastSeenBeatToNoteSkinRev = -1;

	m_HeldHoldNotes.clear();
	m_ActiveHoldNotes.clear();

	CacheAllUsedNoteSkins();
	RefreshBeatToNoteSkin();
}

void NoteField::CacheNoteSkin( CString skin )
{
	if( m_NoteDisplays.find(skin) != m_NoteDisplays.end() )
		return;

	LOG->Trace("NoteField::CacheNoteSkin: cache %s", skin.c_str() );
	NoteDisplayCols *nd = new NoteDisplayCols( GetNumTracks() );
	bool bIsRoutine = GetNumPlayers() > 1;
	for( int c=0; c<GetNumTracks(); c++ )
		nd->display[c].Load( c, m_PlayerNumber, skin, m_fYReverseOffsetPixels, bIsRoutine, m_notesAnimationTiming );
	nd->m_ReceptorArrowRow.Load( m_PlayerNumber, skin, m_fYReverseOffsetPixels, bIsRoutine, m_receptorAnimationTiming );
	nd->m_GhostArrowRow.Load( m_PlayerNumber, skin, m_fYReverseOffsetPixels, bIsRoutine );

	m_NoteDisplays[ skin ] = nd;
}

void NoteField::CacheAllUsedNoteSkins()
{
	/* Cache note skins. */
	vector<CString> skins;
	GAMESTATE->GetAllUsedNoteSkins( skins );
	for( unsigned i=0; i < skins.size(); ++i )
		CacheNoteSkin( skins[i] );
}

void NoteField::Load( const NoteData* pNoteData, PlayerNumber pn )
{
	Unload();

	m_PlayerNumber = pn;
	m_fPercentFadeToFail = -1;
	m_fAlphaNotes = 1.f;
	m_LastSeenBeatToNoteSkinRev = -1;

	ArrowEffects::Init(pn);
	NoteDataWithScoring::Init();

	m_HeldHoldNotes.clear();
	m_ActiveHoldNotes.clear();

	this->CopyAll( pNoteData );
	ASSERT( GetNumTracks() == GAMESTATE->GetCurrentStyle()->m_iColsPerPlayer );

	CacheAllUsedNoteSkins();
	RefreshBeatToNoteSkin();
}


void NoteField::RefreshBeatToNoteSkin()
{
	if( GAMESTATE->m_BeatToNoteSkinRev[m_PlayerNumber] == m_LastSeenBeatToNoteSkinRev )
		return;
	m_LastSeenBeatToNoteSkinRev = GAMESTATE->m_BeatToNoteSkinRev[m_PlayerNumber];

	/* Set by GameState::ResetNoteSkins(): */
	ASSERT( !GAMESTATE->m_BeatToNoteSkin[m_PlayerNumber].empty() );

	m_BeatToNoteDisplays.clear();

	/* GAMESTATE->m_BeatToNoteSkin[pn] maps from song beats to note skins.  Maintain
	 * m_BeatToNoteDisplays, to map from song beats to NoteDisplay*s, so we don't
	 * have to do it while rendering. */
	map<float,CString>::iterator it;
	for( it = GAMESTATE->m_BeatToNoteSkin[m_PlayerNumber].begin();
		 it != GAMESTATE->m_BeatToNoteSkin[m_PlayerNumber].end(); ++it )
	{
		const float Beat = it->first;
		const CString &Skin = it->second;

		map<CString, NoteDisplayCols *>::iterator display = m_NoteDisplays.find( Skin );
		if( display == m_NoteDisplays.end() )
		{
			this->CacheNoteSkin( Skin );
			display = m_NoteDisplays.find( Skin );
		}

		ASSERT_M( display != m_NoteDisplays.end(), ssprintf("Couldn't find %s", Skin.c_str()) );

		NoteDisplayCols *cols = display->second;
		m_BeatToNoteDisplays[Beat] = cols;
	}
}

void NoteField::Update( float fDeltaTime )
{
	ArrowEffects::UpdatePlayer( m_PlayerNumber );
	ActorFrame::Update( fDeltaTime );

	m_rectMarkerBar.Update( fDeltaTime );

	NoteDisplayCols *cur = SearchForSongBeat();

	if( cur != LastDisplay )
	{
		/* The display has changed.  We might be in the middle of a step; copy any
		 * tweens. */
		if( LastDisplay )
		{
			cur->m_GhostArrowRow.CopyTweening( LastDisplay->m_GhostArrowRow );
			cur->m_ReceptorArrowRow.CopyTweening( LastDisplay->m_ReceptorArrowRow );
		}

		LastDisplay = cur;
	}

	cur->m_ReceptorArrowRow.Update( fDeltaTime );
	cur->m_GhostArrowRow.Update( fDeltaTime );

	if( m_fPercentFadeToFail >= 0 )
		m_fPercentFadeToFail = min( m_fPercentFadeToFail + fDeltaTime/1.5f, 1.f );	// take 1.5 seconds to totally fade

	if( m_fAlphaNotes < 1.f )
		m_fAlphaNotes = min( m_fAlphaNotes + fDeltaTime, 1.f );	// 1 second

	RefreshBeatToNoteSkin();


	//
	// update all NoteDisplays
	//

	/*
	 * Update all NoteDisplays.  Hack: We need to call this once per frame, not
	 * once per player.
	 */
	if( m_PlayerNumber == GAMESTATE->m_MasterPlayerNumber )
		NoteDisplay::Update( fDeltaTime );
}

int NoteField::GetWidth()
{
	int iCol = (int)GAMESTATE->m_fNSColSpacing[m_PlayerNumber],
		iSize = (int)GAMESTATE->m_fNSArrowSize[m_PlayerNumber],
		iCols = GAMESTATE->GetCurrentStyle()->m_iColsPerPlayer,
		iWidth = 0;

	if( iCol == iSize )
		iWidth = iCols * iSize;
	else if( iCol < iSize )
		iWidth = ( ( iCols - 1 ) * iCol ) + iSize;
	else
		iWidth = ( iCols * iCol ) - ( iCol - iSize );

	return iWidth + m_iExtraWidth;
}

void NoteField::DrawBeatBar( const float fBeat )
{
	bool bIsMeasure = fmodf( fBeat, fBEATS_PER_MEASURE ) == 0;

	const float fYOffset	= ArrowGetYOffset( m_PlayerNumber, fBeat );
	const float fYPos		= ArrowGetYPos(	m_PlayerNumber, 0, fYOffset, m_fYReverseOffsetPixels );

	float fAlpha = 1;
	int iState = 0;

	if( !bIsMeasure )
	{
		float fScrollSpeed = GAMESTATE->m_CurrentPlayerOptions[m_PlayerNumber].m_fScrollSpeed;
		NoteType nt = BeatToNoteType( fBeat );
		switch( nt )
		{
		default:	ASSERT(0);
		case NOTE_TYPE_4TH:	fAlpha = 1;										iState = 1;	break;
		case NOTE_TYPE_8TH:	fAlpha = SCALE(fScrollSpeed,1.f,2.f,0.f,1.f);	iState = 2;	break;
		case NOTE_TYPE_16TH:fAlpha = SCALE(fScrollSpeed,2.f,4.f,0.f,1.f);	iState = 3;	break;
		}
		CLAMP( fAlpha, 0, 1 );
	}

	int iWidth = GetWidth();
	float fFrameWidth = m_sprBars.GetUnzoomedWidth();

	m_sprBars.SetX( 0 );
	m_sprBars.SetY( fYPos );
	m_sprBars.SetDiffuse( RageColor(1,1,1,fAlpha) );
	m_sprBars.SetState( iState );
	m_sprBars.SetCustomTextureRect( RectF(0,SCALE(iState,0.f,4.f,0.f,1.f), iWidth/fFrameWidth, SCALE(iState+1,0.f,4.f,0.f,1.f)) );
	m_sprBars.SetZoomX( iWidth/m_sprBars.GetUnzoomedWidth() );
	m_sprBars.Draw();

	if( fmodf( fBeat, fBEATS_PER_MEASURE/4.f ) == 0 )
	{
		float fMeasureNoDisplay = fBeat / fBEATS_PER_MEASURE;
		fMeasureNoDisplay++;

		m_textMeasureNumber.SetDiffuse( RageColor(1,1,1,1) );
		m_textMeasureNumber.SetGlow( RageColor(1,1,1,0) );
		m_textMeasureNumber.SetHorizAlign( Actor::align_right );
		m_textMeasureNumber.SetXY( -iWidth/2.f, fYPos );

		bool bDraw = true;

		if( bIsMeasure )
		{
			m_textMeasureNumber.SetText( ssprintf("%.0f", fMeasureNoDisplay ) );
			m_textMeasureNumber.SetZoom( 1.f );
		}
		else if( fmodf( fBeat, fBEATS_PER_MEASURE/2.f ) == 0 )
		{
			m_textMeasureNumber.SetText( ssprintf("%.1f", fMeasureNoDisplay ) );
			m_textMeasureNumber.SetZoom( .85f );
			bDraw = m_bHalfMN;
		}
		else
		{
			m_textMeasureNumber.SetText( ssprintf("%.2f", fMeasureNoDisplay ) );
			m_textMeasureNumber.SetZoom( .7f );
			bDraw = m_bQuarterMN;
		}
		if( bDraw )
			m_textMeasureNumber.Draw();
	}
}

void NoteField::DrawMarkerBar( const float fBeat )
{
	const float fYOffset	= ArrowGetYOffset( m_PlayerNumber, fBeat );
	const float fYPos		= ArrowGetYPos(	m_PlayerNumber, 0, fYOffset, m_fYReverseOffsetPixels );
	const float fArrowSize	= GAMESTATE->m_fNSArrowSize[m_PlayerNumber];

	m_rectMarkerBar.StretchTo( RectF(-GetWidth()/2.f, fYPos-fArrowSize/2.f, GetWidth()/2.f, fYPos+fArrowSize/2.f) );
	m_rectMarkerBar.Draw();
}

void NoteField::DrawAreaHighlight( const float fStartBeat, const float fEndBeat, const RageColor rColor )
{
	float fYStartOffset	= ArrowGetYOffset( m_PlayerNumber, fStartBeat );
	float fYStartPos	= ArrowGetYPos(	m_PlayerNumber, 0, fYStartOffset, m_fYReverseOffsetPixels );
	float fYEndOffset	= ArrowGetYOffset( m_PlayerNumber, fEndBeat );
	float fYEndPos		= ArrowGetYPos(	m_PlayerNumber, 0, fYEndOffset, m_fYReverseOffsetPixels );
	float fArrowSize	= GAMESTATE->m_fNSArrowSize[m_PlayerNumber];

	// Something in OpenGL crashes if this is values are too large.  Strange.  -Chris
	fYStartPos = max( fYStartPos, -1000 );
	fYEndPos = min( fYEndPos, +5000 );

	m_rectAreaHighlight.StretchTo( RectF(-GetWidth()/2.f, fYStartPos-fArrowSize/2.f, GetWidth()/2.f, fYEndPos+fArrowSize/2.f) );
	m_rectAreaHighlight.SetDiffuse( rColor );
	m_rectAreaHighlight.Draw();
}

void NoteField::DrawTimingText( const float fBeat, const float fXOffset, const float fZoom, const CString sText, const RageColor rColor, bool bLeft )
{
	const float fYOffset	= ArrowGetYOffset(	m_PlayerNumber, fBeat );
	const float fYPos		= ArrowGetYPos(		m_PlayerNumber, 0, fYOffset, m_fYReverseOffsetPixels );

	m_textTiming.SetHorizAlign( bLeft ? Actor::align_right : Actor::align_left );
	m_textTiming.SetDiffuse( rColor );
	m_textTiming.SetGlow( RageColor(1,1,1,cosf(RageTimer::GetTimeSinceStart()*2)/2+0.5f) );
	m_textTiming.SetText( sText );
	m_textTiming.SetXY( ( bLeft ? -GetWidth()/2.f : GetWidth()/2.f ) + fXOffset, fYPos );
	m_textTiming.SetZoom( fZoom );
	m_textTiming.Draw();
}

void NoteField::DrawBPMText( const float fBeat, const float fBPM )
{
	DrawTimingText( fBeat, m_fBPMTextX, m_fBPMTextZoom, ssprintf("%.3f",fBPM), m_ColorBPM );
}

void NoteField::DrawStopText( const float fBeat, const float fStop, const bool bExtended )
{
	DrawTimingText( fBeat, m_fStopTextX, m_fStopTextZoom, ssprintf("%.3f",fStop), bExtended ? m_ColorStopX : m_ColorStop );
}

void NoteField::DrawSpeedText( const float fBeat, const float fSpeed, const bool bExtended )
{
	DrawTimingText( fBeat, m_fSpeedTextX, m_fSpeedTextZoom, ssprintf("%.3f",fSpeed), bExtended ? m_ColorSpeedX : m_ColorSpeed, false );
}

void NoteField::DrawTickcountText( const float fBeat, const unsigned uTickCount )
{
	DrawTimingText( fBeat, m_fTickcountTextX, m_fTickcountTextZoom, ssprintf("%d",uTickCount), m_ColorTickcount, false );
}

void NoteField::DrawMultiplierText( const float fBeat, const unsigned uMultiplier, const bool bExtended )
{
	DrawTimingText( fBeat, m_fMultiplierTextX, m_fMultiplierTextZoom, ssprintf("%d",uMultiplier), bExtended ? m_ColorMultiplierX : m_ColorMultiplier, false );
}

void NoteField::DrawBGChangeText( const float fBeat, const CString sNewBGName )
{
	DrawTimingText( fBeat, m_fBGATextX, m_fBGATextZoom, sNewBGName, m_ColorBGA, false );
}

/* cur is an iterator within m_NoteDisplays.  next is ++cur.  Advance or rewind cur to
 * point to the block that contains beat.  We maintain next as an optimization, as this
 * is called for every tap. */
void NoteField::SearchForBeat( NDMap::iterator &cur, NDMap::iterator &next, float Beat )
{
	/* cur is too far ahead: */
	while( cur != m_BeatToNoteDisplays.begin() && cur->first > Beat )
	{
		next = cur;
		--cur;
	}

	/* cur is too far behind: */
	while( next != m_BeatToNoteDisplays.end() && next->first < Beat )
	{
		cur = next;
		++next;
	}
}

NoteField::NoteDisplayCols *NoteField::SearchForSongBeat()
{
	return SearchForBeat( GAMESTATE->m_fPlayerBeat[m_PlayerNumber] );
}

NoteField::NoteDisplayCols *NoteField::SearchForBeat( float Beat )
{
	NDMap::iterator it = m_BeatToNoteDisplays.lower_bound( Beat );
	// Aldo_MX: WTF is this shit? :/
	/* The first entry should always be lower than any Beat we might receive. */
	// This assert is firing with Beat = -7408. -Chris
	// Again with Beat = -7254 and GAMESTATE->m_fMusicSeconds = -3043.61
	// Again with Beat = -9806 and GAMESTATE->m_fMusicSeconds = -3017.22
	// Again with Beat = -9806 and GAMESTATE->m_fMusicSeconds = -3017.22
	// Again in the middle of Remember December in Hardcore Galore:
	//    Beat = -9373.56 and GAMESTATE->m_fMusicSeconds = -2923.48
 	//ASSERT_M( it != m_BeatToNoteDisplays.begin(), ssprintf("%f",Beat) );
	--it;
	//ASSERT_M( it != m_BeatToNoteDisplays.end(), ssprintf("%f",Beat) );

	return it->second;
}

void NoteField::DrawHolds( int& iCol, const int& iFirstRowToDraw, const int& iLastRowToDraw, float& fFirstPixelToDraw, float& fLastPixelToDraw, NDMap::iterator& CurDisplay, NDMap::iterator& NextDisplay, float& fSelectedRangeGlow, bool& bReverse )
{
	//
	// Draw all HoldNotes in this column
	//
	int i;
	for( bReverse ? i=GetNumHoldsAndRolls()-1 : i=0;
		bReverse ? i>=0 : i<GetNumHoldsAndRolls();
		bReverse ? i-- : i++ )
	{
		HoldNote &hn = GetHoldNote(i);

		if( hn.iTrack != iCol )	// this HoldNote doesn't belong to this column
			continue;

		if( GAMESTATE->m_CurrentPlayerOptions[m_PlayerNumber].m_fTimeSpacing >= .5f )
		{
			if( hn.behaviorFlag & (TapNote::behavior_fake | TapNote::behavior_bonus) )
				continue;
		}

		const HoldNoteResult& HResult = GetHoldNoteResult( hn );

		if( HResult.hns == HNS_OK || HResult.hns == RNS_OK )	// if this HoldNote was completed
			continue;	// don't draw anything

		// If no part of this HoldNote is on the screen, skip it
		if( !hn.RangeOverlaps(iFirstRowToDraw, iLastRowToDraw) )
			continue;	// skip

		// TRICKY: If boomerang is on, then all notes in the range [iFirstRowToDraw,iLastRowToDraw]
		// aren't necessarily visible. Test every note to make sure it's on screen before drawing
		float fYHoldStartOffset = ArrowGetYOffset( m_PlayerNumber, NoteRowToBeat(hn.iStartRow) );
		float fYHoldEndOffset = ArrowGetYOffset( m_PlayerNumber, NoteRowToBeat(hn.iEndRow) );

		if( !( fFirstPixelToDraw <= fYHoldEndOffset && fYHoldEndOffset <= fLastPixelToDraw  ||
			fFirstPixelToDraw <= fYHoldStartOffset  && fYHoldStartOffset <= fLastPixelToDraw  ||
			fYHoldStartOffset < fFirstPixelToDraw   && fYHoldEndOffset > fLastPixelToDraw ) )
		{
			continue;	// skip
		}

		const bool bHoldIsActive = m_ActiveHoldNotes[hn];
		const bool bIsHoldingHoldNote = m_HeldHoldNotes[hn];

		if( bHoldIsActive && !PREFSMAN->m_bUsePIUHolds )
			SearchForSongBeat()->m_GhostArrowRow.SetHoldIsActive( hn );

		ASSERT_M( NoteRowToBeat(hn.iStartRow) > -2000, ssprintf("%i %i %i", hn.iStartRow, hn.iEndRow, hn.iTrack) );
		SearchForBeat( CurDisplay, NextDisplay, NoteRowToBeat(hn.iStartRow) );

		bool bHoldIsInSelectionRange = false;
		if( m_fBeginMarker!=-1 && m_fEndMarker!=-1 )
			bHoldIsInSelectionRange = hn.ContainedByRange( BeatToNoteRow( m_fBeginMarker ), BeatToNoteRow( m_fEndMarker ) );

		NoteDisplayCols *ndc = CurDisplay->second;
		float fPercentFade = bHoldIsInSelectionRange ? fSelectedRangeGlow : m_fPercentFadeToFail;
		ndc->display[iCol].DrawHold( hn, bIsHoldingHoldNote, bHoldIsActive, HResult, fPercentFade, m_fAlphaNotes, false, m_fYReverseOffsetPixels );
	}

}

void NoteField::DrawNotes( int& iCol, const int& iFirstRowToDraw, const int& iLastRowToDraw, float& fFirstPixelToDraw, float& fLastPixelToDraw, NDMap::iterator& CurDisplay, NDMap::iterator& NextDisplay, float& fSelectedRangeGlow, bool& bReverse )
{
	//
	// Draw all TapNotes in this column
	//
	CurDisplay = m_BeatToNoteDisplays.begin();
	NextDisplay = CurDisplay; ++NextDisplay;

	// draw notes from furthest to closest
	int i;
	for( bReverse ? i=iFirstRowToDraw : i=iLastRowToDraw;
		bReverse ? i<=iLastRowToDraw : i>=iFirstRowToDraw;
		bReverse ? ++i : --i )	//	 for each row
	{
		TapNote tn = GetTapNote(iCol, i);
		if( tn.drawType == TapNote::empty )	// no note here
			continue;	// skip

		if( tn.drawType == TapNote::hold_head )	// this is a HoldNote begin marker.  Grade it, but don't draw
			continue;	// skip

		if( GAMESTATE->m_CurrentPlayerOptions[m_PlayerNumber].m_fTimeSpacing >= .5f )
		{
			if( tn.type == TapNote::empty )
				continue;
		}

		// TRICKY: If boomerang is on, then all notes in the range
		// [iFirstRowToDraw,iLastRowToDraw] aren't necessarily visible.
		// Test every note to make sure it's on screen before drawing
		float fYOffset = ArrowGetYOffset( m_PlayerNumber, NoteRowToBeat(i) );
		if( fYOffset > fLastPixelToDraw )	// off screen
			continue;	// skip
		if( fYOffset < fFirstPixelToDraw )	// off screen
			continue;	// skip

		// See if there is a hold step that begins on this index.
		HoldNote* pHN = NULL;
		for( int c2=0; c2<GetNumTracks(); c2++ )
		{
			TapNote tn = GetTapNote(c2, i);
			if( tn.drawType == TapNote::hold_head )
			{
				for( int h=0; h<GetNumHoldsAndRolls(); h++ )
				{
					HoldNote& hn = GetHoldNote(h);
					if( c2 == hn.iTrack && i == hn.iStartRow )
					{
						pHN = &hn;
						break;
					}
				}
				if( pHN )
					break;
			}
		}

		bool bIsInSelectionRange = false;
		if( m_fBeginMarker!=-1 && m_fEndMarker!=-1 )
		{
			float fBeat = NoteRowToBeat(i);
			bIsInSelectionRange = m_fBeginMarker<=fBeat && fBeat<=m_fEndMarker;
		}

		ASSERT_M( NoteRowToBeat(i) > -2000, ssprintf("%i %i %i, %f %f", i, iLastRowToDraw, iFirstRowToDraw, GAMESTATE->m_fPlayerBeat[m_PlayerNumber], GAMESTATE->m_fMusicSeconds) );
		SearchForBeat( CurDisplay, NextDisplay, NoteRowToBeat(i) );
		NoteDisplayCols *nd = CurDisplay->second;
		nd->display[iCol].DrawTap( tn, iCol, NoteRowToBeat(i), bIsInSelectionRange ? fSelectedRangeGlow : m_fPercentFadeToFail, m_fAlphaNotes, 1, m_fYReverseOffsetPixels, pHN );
	}
}

void NoteField::DrawPrimitives()
{
	//LOG->Trace( "NoteField::DrawPrimitives()" );

	// This should be filled in on the first update.
	ASSERT( !m_BeatToNoteDisplays.empty() );

	NoteDisplayCols *cur = SearchForSongBeat();
	cur->m_ReceptorArrowRow.Draw();

	const PlayerOptions &current_po = GAMESTATE->m_CurrentPlayerOptions[m_PlayerNumber];

	//
	// Adjust draw range depending on some effects
	//
	float fFirstPixelToDraw = m_fStartDrawingPixel - GAMESTATE->m_fNSArrowSize[m_PlayerNumber];
	float fLastPixelToDraw = m_fEndDrawingPixel + GAMESTATE->m_fNSArrowSize[m_PlayerNumber];

	// HACK: if boomerang and centered are on, then we want to draw much
	// earlier so that the notes don't pop on screen.
	float fCenteredTimesBoomerang =
		current_po.m_fScrolls[PlayerOptions::SCROLL_CENTERED] *
		current_po.m_fAccels[PlayerOptions::ACCEL_BOOMERANG];
	fFirstPixelToDraw += SCALE( fCenteredTimesBoomerang, 0.f, 1.f, 0.f, -SCREEN_HEIGHT/2 );

	float fDrawScale = 1;
	fDrawScale *= 1 + 0.5f * fabsf( current_po.m_fPerspectiveTilt );
	// fDrawScale *= 1 + fabsf( current_po.m_fEffects[PlayerOptions::EFFECT_MINI] );
	fDrawScale *= 1 + fabsf( current_po.m_fEffects[PlayerOptions::EFFECT_TINY] );

	fFirstPixelToDraw *= g_NoteFieldMode[m_PlayerNumber].m_fFirstPixelToDrawScale * fDrawScale;
	fLastPixelToDraw *= g_NoteFieldMode[m_PlayerNumber].m_fLastPixelToDrawScale * fDrawScale;

	fFirstPixelToDraw = roundf(fFirstPixelToDraw);
	fLastPixelToDraw = roundf(fLastPixelToDraw);

	float fScrollSpeed = GetCurrentScrollSpeed(m_PlayerNumber);
	int iFirstRowToDraw, iLastRowToDraw;
	
	// Probe for first and last notes on the screen
	if( fabsf(fScrollSpeed) >= 0.001f )
	{
		int iFirstRow = ArrowEffects::m_iFirstRowToDraw[m_PlayerNumber];
		ArrowEffects::GetNearestDisplayedRow( m_PlayerNumber, fFirstPixelToDraw, iFirstRow, true );

		int iLastRow = BeatToNoteRow(GAMESTATE->m_fLastDrawnBeat[m_PlayerNumber]);
		ArrowEffects::GetNearestDisplayedRow( m_PlayerNumber, fLastPixelToDraw, iLastRow, false );

		iFirstRowToDraw = min( iFirstRow, iLastRow );
		iLastRowToDraw = max( iFirstRow, iLastRow );
	}
	// Small speed values tend to infinity, no need to iterate, but we really need to draw all the arrows... sigh...
	else
	{
		iFirstRowToDraw = ArrowEffects::m_iFirstRowToDraw[m_PlayerNumber];
		iLastRowToDraw = ArrowEffects::m_iLastRowToDraw[m_PlayerNumber];
	}

	float fFirstBeatToDraw = NoteRowToBeat(iFirstRowToDraw),
		fLastBeatToDraw = NoteRowToBeat(iLastRowToDraw);

	ArrowEffects::m_iFirstRowToDraw[m_PlayerNumber] = max(iFirstRowToDraw, ArrowEffects::m_iFirstRowToDraw[m_PlayerNumber]);
	GAMESTATE->m_fLastDrawnBeat[m_PlayerNumber] = fLastBeatToDraw;

//	LOG->Trace( "start = %f.1, end = %f.1", fFirstBeatToDraw-fSongBeat, fLastBeatToDraw-fSongBeat );
//	LOG->Trace( "Drawing elements %d through %d", iFirstRowToDraw, iLastRowToDraw );

	if( GAMESTATE->m_bEditing )
	{
		ASSERT(GAMESTATE->m_pCurSong);

		unsigned i;

		//
		// Draw beat bars
		//
		{
			float fStartDrawingMeasureBars = max( 0, froundf(fFirstBeatToDraw-0.25f,0.25f) );
			float fEndDrawingMeasureBars = max( 0, froundf(fLastBeatToDraw+0.25f,0.25f) );
			for( float f=fStartDrawingMeasureBars; f<=fEndDrawingMeasureBars; f+=0.25f )
				DrawBeatBar( f );
		}

		if( GAMESTATE->m_EditMode == GAMESTATE->MODE_BACKGROUND )
		{
			//
			// BPM text
			//
			vector<BPMSegment> &aBPMSegments = GAMESTATE->m_pCurSong->m_Timing.m_BPMSegments;
			for( i=0; i<aBPMSegments.size(); i++ )
			{
				if( aBPMSegments[i].m_fBeat < fFirstBeatToDraw )
					continue;
				else if( aBPMSegments[i].m_fBeat > fLastBeatToDraw )
					break;
				else
					DrawBPMText( aBPMSegments[i].m_fBeat, aBPMSegments[i].m_fBPM );
			}

			//
			// Stop text
			//
			vector<StopSegment> &aStopSegments = GAMESTATE->m_pCurSong->m_Timing.m_StopSegments;
			for( i=0; i<aStopSegments.size(); i++ )
			{
				if( aStopSegments[i].m_fBeat < fFirstBeatToDraw )
					continue;
				else if( aStopSegments[i].m_fBeat > fLastBeatToDraw )
					break;
				else
					DrawStopText( aStopSegments[i].m_fBeat, aStopSegments[i].m_fSeconds, aStopSegments[i].m_bDelay );
			}

			//
			// BGChange text
			//
			BGChangeArray &aBGChanges = GAMESTATE->m_pCurSong->m_BGChanges;
			for( i=0; i<aBGChanges.size(); i++ )
			{
				if(aBGChanges[i].m_fBeat >= fFirstBeatToDraw &&
				aBGChanges[i].m_fBeat <= fLastBeatToDraw)
				{
					const BGChange& change = aBGChanges[i];
					CString sChangeText = ssprintf("%s\n%.0f%%%s%s%s",
						change.m_sBGName.c_str(),
						change.m_fRate*100,
						change.m_bFadeLast ? " Fade" : "",
						change.m_bRewindMovie ? " Rewind" : "",
						change.m_bLoop ? " Loop" : "" );

					DrawBGChangeText( change.m_fBeat, sChangeText );
				}
			}
		}
		else
		{
			//
			// BPM text
			//
			vector<BPMSegment> &aBPMSegments = GAMESTATE->m_pCurSteps[m_PlayerNumber]->m_Timing.m_BPMSegments;
			for( i=0; i<aBPMSegments.size(); i++ )
			{
				if( aBPMSegments[i].m_fBeat < fFirstBeatToDraw )
					continue;
				else if( aBPMSegments[i].m_fBeat > fLastBeatToDraw )
					break;
				else
					DrawBPMText( aBPMSegments[i].m_fBeat, aBPMSegments[i].m_fBPM );
			}

			//
			// Stop text
			//
			vector<StopSegment> &aStopSegments = GAMESTATE->m_pCurSteps[m_PlayerNumber]->m_Timing.m_StopSegments;
			for( i=0; i<aStopSegments.size(); i++ )
			{
				if( aStopSegments[i].m_fBeat < fFirstBeatToDraw )
					continue;
				else if( aStopSegments[i].m_fBeat > fLastBeatToDraw )
					break;
				else
					DrawStopText( aStopSegments[i].m_fBeat, aStopSegments[i].m_fSeconds, aStopSegments[i].m_bDelay );
			}

			//
			// Speed text
			//
			vector<SpeedSegment> &aSpeedSegments = GAMESTATE->m_pCurSteps[m_PlayerNumber]->m_Timing.m_SpeedSegments;
			for( i=0; i<aSpeedSegments.size(); i++ )
			{
				if( aSpeedSegments[i].m_fBeat < fFirstBeatToDraw )
					continue;
				else if( aSpeedSegments[i].m_fBeat > fLastBeatToDraw )
					break;
				else
					DrawSpeedText( aSpeedSegments[i].m_fBeat, aSpeedSegments[i].m_fToSpeed, aSpeedSegments[i].m_fBeats < 0 );
			}

			//
			// Tickcount text
			//
			vector<TickcountSegment> &aTickcountSegments = GAMESTATE->m_pCurSteps[m_PlayerNumber]->m_Timing.m_TickcountSegments;
			for( i=0; i<aTickcountSegments.size(); i++ )
			{
				if( aTickcountSegments[i].m_fBeat < fFirstBeatToDraw )
					continue;
				else if( aTickcountSegments[i].m_fBeat > fLastBeatToDraw )
					break;
				else
					DrawTickcountText( aTickcountSegments[i].m_fBeat, aTickcountSegments[i].m_uTickcount );
			}

			//
			// Multiplier text
			//
			vector<MultiplierSegment> &aMultiplierSegments = GAMESTATE->m_pCurSteps[m_PlayerNumber]->m_Timing.m_MultiplierSegments;
			for( i=0; i<aMultiplierSegments.size(); i++ )
			{
				if( aMultiplierSegments[i].m_fBeat < fFirstBeatToDraw )
					continue;
				else if( aMultiplierSegments[i].m_fBeat > fLastBeatToDraw )
					break;
				else
					DrawMultiplierText( aMultiplierSegments[i].m_fBeat, aMultiplierSegments[i].m_uHit,
						aMultiplierSegments[i].m_uHit != aMultiplierSegments[i].m_uMiss ||
						aMultiplierSegments[i].m_uHit != aMultiplierSegments[i].m_fLifeHit ||
						aMultiplierSegments[i].m_uHit != aMultiplierSegments[i].m_fScoreHit ||
						aMultiplierSegments[i].m_uHit != aMultiplierSegments[i].m_fLifeMiss ||
						aMultiplierSegments[i].m_uHit != aMultiplierSegments[i].m_fScoreMiss
					);
			}

			//
			// Fake area
			//
			vector<AreaSegment> &aFakeSegments = GAMESTATE->m_pCurSteps[m_PlayerNumber]->m_Timing.m_FakeAreaSegments;
			for( i=0; i<aFakeSegments.size(); i++ )
			{
				float fStartBeat = aFakeSegments[i].m_fBeat;
				float fEndBeat = aFakeSegments[i].m_fBeat + aFakeSegments[i].m_fBeats;

				if( fEndBeat < fFirstBeatToDraw )
					continue;
				else if( fStartBeat > fLastBeatToDraw )
					break;
				else
				{
					if( fStartBeat >= fFirstBeatToDraw && fStartBeat <= fLastBeatToDraw )
						CLAMP( fEndBeat, fFirstBeatToDraw, fLastBeatToDraw );
					else if( fEndBeat >= fFirstBeatToDraw && fEndBeat <= fLastBeatToDraw )
						CLAMP( fStartBeat, fFirstBeatToDraw, fLastBeatToDraw );
					else if( fEndBeat >= fFirstBeatToDraw && fStartBeat <= fLastBeatToDraw )
					{
						CLAMP( fStartBeat, fFirstBeatToDraw, fLastBeatToDraw );
						CLAMP( fEndBeat, fFirstBeatToDraw, fLastBeatToDraw );
					}
					else
						continue;
				}

				DrawAreaHighlight( fStartBeat, fEndBeat, RageColor(0.5f,0.5f,0.5f,0.5f) );
			}

			//
			// Speed Areas
			//
			vector<SpeedAreaSegment> &aSpeedAreaSegments = GAMESTATE->m_pCurSteps[m_PlayerNumber]->m_Timing.m_SpeedAreaSegments;
			for( i=0; i<aSpeedAreaSegments.size(); i++ )
			{
				float fStartBeat = aSpeedAreaSegments[i].m_fBeat;
				float fEndBeat = aSpeedAreaSegments[i].m_fBeat + aSpeedAreaSegments[i].m_fBeats;

				if( fEndBeat < fFirstBeatToDraw )
					continue;
				else if( fStartBeat > fLastBeatToDraw )
					break;
				else
				{
					if( fStartBeat >= fFirstBeatToDraw && fStartBeat <= fLastBeatToDraw )
						CLAMP( fEndBeat, fFirstBeatToDraw, fLastBeatToDraw );
					else if( fEndBeat >= fFirstBeatToDraw && fEndBeat <= fLastBeatToDraw )
						CLAMP( fStartBeat, fFirstBeatToDraw, fLastBeatToDraw );
					else if( fEndBeat >= fFirstBeatToDraw && fStartBeat <= fLastBeatToDraw )
					{
						CLAMP( fStartBeat, fFirstBeatToDraw, fLastBeatToDraw );
						CLAMP( fEndBeat, fFirstBeatToDraw, fLastBeatToDraw );
					}
					else
						continue;
				}

				// TODO: Draw Text %d -> %d
				DrawAreaHighlight( fStartBeat, fEndBeat, RageColor(0.0f,0.5f,0.0f,0.5f) );
			}
		}

		//
		// Draw marker bars
		//
		if( m_fBeginMarker != -1  &&  m_fEndMarker != -1 )
			DrawAreaHighlight( m_fBeginMarker, m_fEndMarker, RageColor(1,0,0,0.3f) );
		else if( m_fBeginMarker != -1 )
			DrawMarkerBar( m_fBeginMarker );
		else if( m_fEndMarker != -1 )
			DrawMarkerBar( m_fEndMarker );

	}

	//
	// Optimization is very important here because there are so many arrows to draw.
	// Draw the arrows in order of column.  This minimize texture switches and let us
	// draw in big batches.
	//

	float fSelectedRangeGlow = SCALE( cosf(RageTimer::GetTimeSinceStart()*2), -1, 1, 0.1f, 0.3f );

	int c;
	for( m_bRDCols ? c=GetNumTracks()-1 : c=0;
		m_bRDCols ? c>=0 : c<GetNumTracks();
		m_bRDCols ? c-- : c++ )	// for each arrow column
	{
		int col = GAMESTATE->m_viNSDrawOrder[m_PlayerNumber][c];

		g_NoteFieldMode[m_PlayerNumber].BeginDrawTrack(col);

		NDMap::iterator CurDisplay = m_BeatToNoteDisplays.begin();
		ASSERT( CurDisplay != m_BeatToNoteDisplays.end() );
		NDMap::iterator NextDisplay = CurDisplay; ++NextDisplay;

		// When drawn, this is the order things stack upon each other in (1 being on top)
		if( m_bDHoldsFirst )
		{
			// 1. Hold Notes
			// 2. Any tap note or item
			DrawNotes( col, iFirstRowToDraw, iLastRowToDraw, fFirstPixelToDraw, fLastPixelToDraw, CurDisplay, NextDisplay, fSelectedRangeGlow, m_bRDRows );
			DrawHolds( col, iFirstRowToDraw, iLastRowToDraw, fFirstPixelToDraw, fLastPixelToDraw, CurDisplay, NextDisplay, fSelectedRangeGlow, m_bRDHolds );
		}
		else
		{
			// 1. Any tap note or item
			// 2. Hold Notes
			DrawHolds( col, iFirstRowToDraw, iLastRowToDraw, fFirstPixelToDraw, fLastPixelToDraw, CurDisplay, NextDisplay, fSelectedRangeGlow, m_bRDHolds );
			DrawNotes( col, iFirstRowToDraw, iLastRowToDraw, fFirstPixelToDraw, fLastPixelToDraw, CurDisplay, NextDisplay, fSelectedRangeGlow, m_bRDRows );
		}

		g_NoteFieldMode[m_PlayerNumber].EndDrawTrack(col);
	}

	cur->m_GhostArrowRow.Draw();
}

void NoteField::RemoveTapNoteRow( int iIndex )
{
	for( int c=0; c<GetNumTracks(); c++ )
		SetTapNote(c, iIndex, TAP_EMPTY);
}

void NoteField::UpdateDrawingArea( float fStartDrawingPixel, float fEndDrawingPixel, float fYReverseOffsetPixels )
{
	m_fStartDrawingPixel = fStartDrawingPixel;
	m_fEndDrawingPixel = fEndDrawingPixel;
	m_fYReverseOffsetPixels = fYReverseOffsetPixels;

	for( map<CString, NoteDisplayCols *>::iterator it = m_NoteDisplays.begin(); it != m_NoteDisplays.end(); ++it )
	{
		NoteDisplayCols& nd = *it->second;
		nd.display->m_fYReverseOffsetPixels = m_fYReverseOffsetPixels;
		nd.m_GhostArrowRow.m_fYReverseOffsetPixels = m_fYReverseOffsetPixels;
		nd.m_ReceptorArrowRow.m_fYReverseOffsetPixels = m_fYReverseOffsetPixels;
	}
}

void NoteField::FadeToFail()
{
	m_fPercentFadeToFail = max( 0.0f, m_fPercentFadeToFail );	// this will slowly increase every Update()
		// don't fade all over again if this is called twice
}

void NoteField::Step( int iCol, TapNoteScore score, bool released )
{
	SearchForSongBeat()->m_ReceptorArrowRow.Step( iCol, score, released );
}

void NoteField::SetPressed( int iCol )
{
	SearchForSongBeat()->m_ReceptorArrowRow.SetPressed( iCol );
}

void NoteField::DidTapNote( int iCol, TapNoteScore score, bool bBright )
{
	if( score == TNS_NONE ||
		GAMESTATE->m_bNSOverrideDim[m_PlayerNumber] && GAMESTATE->m_bNSOverrideBright[m_PlayerNumber] ||
		GAMESTATE->m_bNSOverrideDim[m_PlayerNumber] && bBright ||
		GAMESTATE->m_bNSOverrideBright[m_PlayerNumber] && !bBright )
		SearchForSongBeat()->m_GhostArrowRow.DidTapNote( iCol, score );

	else if( GAMESTATE->m_bNSOverrideDim[m_PlayerNumber] )
		SearchForSongBeat()->m_GhostArrowRow.DidTapNote( iCol, score, false );

	else if( GAMESTATE->m_bNSOverrideBright[m_PlayerNumber] )
		SearchForSongBeat()->m_GhostArrowRow.DidTapNote( iCol, score, true );

	else
		SearchForSongBeat()->m_GhostArrowRow.DidTapNote( iCol, score, bBright );
}

void NoteField::DidHoldNote( int iCol )
{
	//SearchForSongBeat()->m_GhostArrowRow.DidHoldNote( iCol );
}

void NoteField::DidShockArrow()
{
	m_fAlphaNotes = 0;
}

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * Contains code (c) 2001-2004 Chris Danford.
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
