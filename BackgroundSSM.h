/* BackgroundSSM - The song's background displayed in SelectSong. */

#ifndef BACKGROUND_SSM_H
#define BACKGROUND_SSM_H

#include "Sprite.h"
#include "RageTextureID.h"
class Song;
class Course;
class Character;

class BackgroundSSM : public Sprite
{
public:
	BackgroundSSM();
	virtual ~BackgroundSSM() { }

	virtual bool Load( RageTextureID ID, bool bPreview );

	virtual void Update( float fDeltaTime );

	void LoadFromSong( Song* pSong, bool bPreview );		// NULL means no song
	void LoadAllMusic( bool bPreview );
	void LoadSort( bool bPreview );
	void LoadMode( bool bPreview );
	void LoadFromGroup( CString sGroupName, bool bPreview );
	void LoadFromCourse( Course* pCourse, bool bPreview );
	void LoadCardFromCharacter( Character* pCharacter, bool bPreview );
	void LoadIconFromCharacter( Character* pCharacter, bool bPreview );
	void LoadTABreakFromCharacter( Character* pCharacter, bool bPreview );
	void LoadRoulette( bool bPreview );
	void LoadRandom( bool bPreview );
	void LoadFallback( bool bPreview );

	void SetScrolling( bool bScroll, float Percent = 0);
	bool IsScrolling() const { return m_bScrolling; }
	float ScrollingPercent() const { return m_fPercentScrolling; }

	bool	m_bBGCache;

protected:

	bool	m_bScrolling;
	float	m_fPercentScrolling;
};

#endif

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * Contains code (c) 2001-2004 Chris Danford.
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
