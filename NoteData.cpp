/*
 * NoteData is organized by:
 *  track - corresponds to different columns of notes on the screen
 *  row/index - corresponds to subdivisions of beats
 */

#include "global.h"
#include "NoteData.h"
#include "RageUtil.h"
#include "RageLog.h"
#include "GameState.h"
#include "Song.h"
#include "PrefsManager.h"

NoteData::NoteData()
{
	Init();
}

void NoteData::Init()
{
	m_iNumTracks = MAX_NOTE_TRACKS;
	ClearAll();

	m_iNumTracks = 0;
	m_uNumPlayers = 0;
}

NoteData::~NoteData()
{
}

int NoteData::GetNumTracks() const
{
	return m_iNumTracks;
}

void NoteData::SetNumTracks( int iNewNumTracks )
{
	if( m_iNumTracks == iNewNumTracks )
		return;

	m_iNumTracks = iNewNumTracks;

	// TODO - Aldo_MX: Remove m_TapNotes and m_HoldNotes
	// Make sure that all tracks are of the same length
	ASSERT( m_iNumTracks > 0 );
	int rows = m_TapNotes[0].size();

	for( int t=0; t<MAX_NOTE_TRACKS; t++ )
	{
		if( t<m_iNumTracks )
			m_TapNotes[t].resize( rows, TAP_EMPTY );
		else
			m_TapNotes[t].clear();
	}

	/* Remove all hold notes that are out of bounds. */
	for( int h = m_HoldNotes.size()-1; h >= 0; --h )
		if( m_HoldNotes[h].iTrack >= iNewNumTracks )
			m_HoldNotes.erase( m_HoldNotes.begin()+h );
}

unsigned NoteData::GetNumPlayers() const
{
	return m_uNumPlayers;
}

void NoteData::AddPlayer()
{
	m_uNumPlayers++;
}

void NoteData::SetNumPlayers( unsigned uPlayers )
{
	ASSERT( uPlayers > 0 );
	if( m_uNumPlayers > uPlayers )
	{
		this->ConvertBackTo9sAnd8s();
		for( int t=0; t<m_iNumTracks; t++ )
		{
			for( int r=0; r<GetNumRows(); r++ )
			{
				if( GetTapNoteX(t, r).playerNumber > uPlayers )
				{
					SetTapNote(t, r, TAP_EMPTY);
				}
			}
		}
		this->Convert9sAnd8s();
	}
	m_uNumPlayers = uPlayers;
}

/* Clear [iNoteIndexBegin,iNoteIndexEnd]; that is, including iNoteIndexEnd. */
void NoteData::ClearRange( int iNoteIndexBegin, int iNoteIndexEnd )
{
	this->ConvertBackTo9sAnd8s();
	for( int c=0; c<m_iNumTracks; c++ )
	{
		for( int i=iNoteIndexBegin; i <= iNoteIndexEnd; i++ )
			SetTapNote(c, i, TAP_EMPTY);
	}
	this->Convert9sAnd8s();
}

void NoteData::ClearAll()
{
	//m_Notes.clear();

	// TODO - Aldo_MX: Remove m_TapNotes, m_HoldNotes & m_AttackMap
	for( int t=0; t<m_iNumTracks; t++ )
		m_TapNotes[t].clear();
	m_HoldNotes.clear();
}

/* Copy a range from pFrom to this.  (Note that this does *not* overlay;
 * all data in the range is overwritten.) */
void NoteData::CopyRange( const NoteData* pFrom, int iFromIndexBegin, int iFromIndexEnd, int iToIndexBegin )
{
	ASSERT( pFrom->m_iNumTracks == m_iNumTracks );

	int f;
	int t;

	NoteData From, To;

	From.To9sAnd8s( *pFrom );
	To.To9sAnd8s( *this );

	// copy recorded TapNotes
	f = iFromIndexBegin, t = iToIndexBegin;

	while( f<=iFromIndexEnd )
	{
		for( int c=0; c<m_iNumTracks; c++ )
		{
			TapNote tn = From.GetTapNote( c, f );
			if( tn.playerNumber > m_uNumPlayers )
				m_uNumPlayers = tn.playerNumber;

			To.SetTapNote( c, t, tn );
		}
		f++;
		t++;
	}

	this->From9sAnd8s( To );
}

void NoteData::Config( const NoteData &From )
{
	m_iNumTracks = From.m_iNumTracks;
	m_uNumPlayers = From.m_uNumPlayers;
}

void NoteData::CopyAll( const NoteData* pFrom )
{
	Config(*pFrom);
	ClearAll();

	//m_Notes = pFrom->m_Notes;

	// TODO - Aldo_MX: Remove m_TapNotes, m_HoldNotes & m_AttackMap
	for( int c=0; c<m_iNumTracks; c++ )
		m_TapNotes[c] = pFrom->m_TapNotes[c];
	m_HoldNotes = pFrom->m_HoldNotes;
}

void NoteData::CopyAllTurned( const NoteData& from )
{
	*this = from;
}

bool NoteData::IsRowEmpty( int index ) const
{
	/* If this is out of range, we don't have any notes there, so all tracks are empty. */
	if( index < 0 || index >= GetNumRows() )
		return true;

	for( int t=0; t<m_iNumTracks; t++ )
		if( GetTapNoteX(t, index).type != TapNote::empty || GetTapNoteX(t, index).checkpoints > 0 )
			return false;
	return true;
}

bool NoteData::IsRangeEmpty( int track, int iIndexBegin, int iIndexEnd ) const
{
	ASSERT( track<m_iNumTracks );

	CLAMP( iIndexBegin, 0, GetNumRows()-1 );
	CLAMP( iIndexEnd, 0, GetNumRows()-1 );

	for( int i=iIndexBegin; i<=iIndexEnd; i++ )
		if( GetTapNoteX(track,i).type != TapNote::empty || GetTapNoteX(track,i).checkpoints > 0 )
			return false;
	return true;
}

int NoteData::GetNumTapNonEmptyTracks( int index ) const
{
	int iNum = 0;
	for( int t=0; t<m_iNumTracks; t++ )
		if( GetTapNote(t, index).type != TapNote::empty || GetTapNoteX(t, index).checkpoints > 0 )
			iNum++;
	return iNum;
}

void NoteData::GetTapNonEmptyTracks( int index, set<int>& addTo ) const
{
	for( int t=0; t<m_iNumTracks; t++ )
		if( GetTapNote(t, index).type != TapNote::empty || GetTapNoteX(t, index).checkpoints > 0 )
			addTo.insert(t);
}

int NoteData::GetFirstNonEmptyTrack( int index ) const
{
	/* If this is out of range, we don't have any notes there, so all tracks are empty. */
	if( index < 0 || index >= GetNumRows() )
		return 0;

	for( int t=0; t<m_iNumTracks; t++ )
		if( GetTapNoteX( t, index ).type != TapNote::empty || GetTapNoteX(t, index).checkpoints > 0 )
			return t;
	return -1;
}

int NoteData::GetNumTracksWithTap( int index ) const
{
	/* If this is out of range, we don't have any notes there, so all tracks are empty. */
	if( index < 0 || index >= GetNumRows() )
		return 0;

	int iNum = 0;
	for( int t=0; t<m_iNumTracks; t++ )
	{
		TapNote tn = GetTapNoteX( t, index );
		if( tn.type == TapNote::tap )
			iNum++;
	}
	return iNum;
}

int NoteData::GetNumTracksWithTapOrHoldHead( int index ) const
{
	// If this is out of range, we don't have any notes there, so all tracks are empty. //
	if( index < 0 || index >= GetNumRows() )
		return 0;

	int iNum = 0;
	for( int t=0; t<m_iNumTracks; t++ )
	{
		TapNote tn = GetTapNoteX( t, index );
		// Note that hidden notes are not counted in this as it'll mess up scoring
		if( tn.type == TapNote::tap || tn.type == TapNote::hold_head || tn.type == TapNote::lift )
			iNum++;
	}
	return iNum;
}

int NoteData::GetNumTracksWithTapOrHiddenOrHoldHead( int index ) const
{
	// If this is out of range, we don't have any notes there, so all tracks are empty. //
	if( index < 0 || index >= GetNumRows() )
		return 0;

	int iNum = 0;
	for( int t=0; t<m_iNumTracks; t++ )
	{
		TapNote tn = GetTapNoteX( t, index );
		// Note that hidden notes are not counted in this as it'll mess up scoring
		if( tn.type == TapNote::tap || tn.type == TapNote::hold_head || tn.type == TapNote::lift || tn.type == TapNote::hidden )
			iNum++;
	}
	return iNum;
}

int NoteData::GetNumTracksWithCheckpoint( int index, bool bCheckpointsOnly, bool bSkipHoldHeads ) const
{
	// If this is out of range, we don't have any notes there, so all tracks are empty.
	if( index < 0 || index >= GetNumRows() )
		return 0;

	int iNum = 0;
	for( int t=0; t<m_iNumTracks; t++ )
	{
		TapNote tn = GetTapNoteX( t, index );

		if( bCheckpointsOnly && tn.checkpoints == 0 )
			continue;
		else if( !bCheckpointsOnly && tn.checkpoints == 0 )
		{
			if( tn.type == TapNote::empty || tn.type == TapNote::hidden || tn.type == TapNote::mine || tn.type == TapNote::shock || tn.type == TapNote::potion )
				continue;
		}
		else if( bCheckpointsOnly && bSkipHoldHeads )
		{
			if( tn.type == TapNote::hold_head )
				continue;
		}

		iNum++;
	}

	return iNum;
}

int NoteData::GetFirstTrackWithTap( int index ) const
{
	/* If this is out of range, we don't have any notes there, so all tracks are empty. */
	if( index < 0 || index >= GetNumRows() )
		return -1;

	for( int t=0; t<m_iNumTracks; t++ )
	{
		TapNote tn = GetTapNoteX( t, index );
		if( tn.type == TapNote::tap )
			return t;
	}
	return -1;
}

int NoteData::GetFirstTrackWithTapOrHoldHead( int index ) const
{
	/* If this is out of range, we don't have any notes there, so all tracks are empty. */
	if( index < 0 || index >= GetNumRows() )
		return -1;

	for( int t=0; t<m_iNumTracks; t++ )
	{
		TapNote tn = GetTapNoteX( t, index );
		if( tn.type == TapNote::tap || tn.type == TapNote::hold_head || tn.type == TapNote::lift )
			return t;
	}
	return -1;
}

int NoteData::GetFirstTrackWithTapOrHiddenOrHoldHead( int index ) const
{
	/* If this is out of range, we don't have any notes there, so all tracks are empty. */
	if( index < 0 || index >= GetNumRows() )
		return -1;

	for( int t=0; t<m_iNumTracks; t++ )
	{
		TapNote tn = GetTapNoteX( t, index );
		if( tn.type == TapNote::tap || tn.type == TapNote::hold_head || tn.type == TapNote::lift || tn.type == TapNote::hidden )
			return t;
	}

	return -1;
}

int NoteData::GetFirstTrackWithCheckpoint( int index, bool bCheckpointsOnly ) const
{
	/* If this is out of range, we don't have any notes there, so all tracks are empty. */
	if( index < 0 || index >= GetNumRows() )
		return -1;

	for( int t=0; t<m_iNumTracks; t++ )
	{
		TapNote tn = GetTapNoteX( t, index );

		if( bCheckpointsOnly && tn.checkpoints == 0 )
			continue;
		else if( !bCheckpointsOnly && tn.checkpoints == 0 )
		{
			if( tn.type == TapNote::empty || tn.type == TapNote::hidden || tn.type == TapNote::mine || tn.type == TapNote::shock || tn.type == TapNote::potion )
				continue;
		}

		return t;
	}

	return -1;
}

int NoteData::GetFirstTrackWithMine( int index ) const
{
	/* If this is out of range, we don't have any notes there, so all tracks are empty. */
	if( index < 0 || index >= GetNumRows() )
		return -1;

	for( int t=0; t<m_iNumTracks; t++ )
	{
		TapNote tn = GetTapNoteX( t, index );
		if( tn.type == TapNote::mine )
			return t;
	}

	return -1;
}

int NoteData::GetFirstTrackWithShock( int index ) const
{
	/* If this is out of range, we don't have any notes there, so all tracks are empty. */
	if( index < 0 || index >= GetNumRows() )
		return -1;

	for( int t=0; t<m_iNumTracks; t++ )
	{
		TapNote tn = GetTapNoteX( t, index );
		if( tn.type == TapNote::shock )
			return t;
	}

	return -1;
}

int NoteData::GetFirstTrackWithPotion( int index ) const
{
	/* If this is out of range, we don't have any notes there, so all tracks are empty. */
	if( index < 0 || index >= GetNumRows() )
		return -1;

	for( int t=0; t<m_iNumTracks; t++ )
	{
		TapNote tn = GetTapNoteX( t, index );
		if( tn.type == TapNote::potion )
			return t;
	}
	return -1;
}

bool CompareHoldNotes( const HoldNote& hn1, const HoldNote& hn2 )
{
	if( hn1.iEndRow == hn2.iEndRow )
	{
		if( hn1.iStartRow == hn2.iStartRow )
			return hn1.iTrack < hn2.iTrack;
		else
			return hn1.iStartRow < hn2.iStartRow;
	}
	else
		return hn1.iEndRow < hn2.iEndRow;
}

void SortHoldNotes( vector<HoldNote> &arrayHoldNotes )
{
	sort( arrayHoldNotes.begin(), arrayHoldNotes.end(), CompareHoldNotes );
}

void NoteData::AddHoldNote( HoldNote hn )
{
	ASSERT( hn.iStartRow>=0 && hn.iEndRow>=0 );

	int i;

	// look for other hold notes that overlap and merge them
	// XXX: this is done implicitly with 9s and 8s, but 9s and 8s uses this function.
	// Rework this later.
	for( i=0; i<GetNumHoldsAndRolls(); i++ )	// for each HoldNote
	{
		HoldNote &other = GetHoldNote(i);
		if( hn.iTrack == other.iTrack  &&		// the tracks correspond
			hn.RangeOverlaps(other) ) // they overlap
		{
			hn.iStartRow = min(hn.iStartRow, other.iStartRow);
			hn.iEndRow = max(hn.iEndRow, other.iEndRow);

			// delete this HoldNote
			RemoveHoldNote( i );
			--i;
		}
	}

	int iAddStartIndex = hn.iStartRow;
	int iAddEndIndex = hn.iEndRow;

	// delete TapNotes under this HoldNote
	for( i=iAddStartIndex+1; i<=iAddEndIndex; i++ )
		SetTapNote( hn.iTrack, i, TAP_EMPTY );

	// hn a tap note at the start of this hold
	TapNote tnHead = TAP_ORIGINAL_HOLD_HEAD;
	hn.ToTapNote(tnHead);
	SetTapNote( hn.iTrack, iAddStartIndex, tnHead );

	m_HoldNotes.push_back(hn);
	SortHoldNotes( m_HoldNotes );
}

void NoteData::RemoveHoldNote( int iHoldIndex )
{
	ASSERT( iHoldIndex >= 0  &&  iHoldIndex < GetNumHoldsAndRolls() );

	HoldNote& hn = GetHoldNote(iHoldIndex);

	const int iHoldStartIndex = hn.iStartRow;

	// delete a tap note at the start of this hold
	SetTapNote(hn.iTrack, iHoldStartIndex, TAP_EMPTY);

	// remove from list
	m_HoldNotes.erase(m_HoldNotes.begin()+iHoldIndex, m_HoldNotes.begin()+iHoldIndex+1);
}

int NoteData::GetFirstRow() const
{
#if 0
	// TODO - Aldo_MX: TimingData // NoteRow in NoteData maybe? o.o
	return BeatToNoteRow(GetFirstBeat());
#else
	int iEarliestRowFoundSoFar = -1;

	int i;

	for( i=0; i < int(m_TapNotes[0].size()); i++ )
	{
		if( !IsRowEmpty(i) )
		{
			iEarliestRowFoundSoFar = i;
			break;
		}
	}

	for( i=0; i<GetNumHoldsAndRolls(); i++ )
	{
		if( iEarliestRowFoundSoFar == -1 ||
			GetHoldNote(i).iStartRow < iEarliestRowFoundSoFar )
			iEarliestRowFoundSoFar = GetHoldNote(i).iStartRow;
	}

	if( iEarliestRowFoundSoFar == -1 )	// there are no notes
		return 0;

	return iEarliestRowFoundSoFar;
#endif
}

float NoteData::GetFirstBeat() const
{
	// TODO - Aldo_MX: Remove this and use m_Notes
	//return m_Notes.size() > 0 ? m_Notes[0].fBeat : -1;

	return NoteRowToBeat( GetFirstRow() );
}

int NoteData::GetLastRow() const
{
#if 0
	// TODO - Aldo_MX: TimingData // NoteRow in NoteData maybe? o.o
	return BeatToNoteRow(GetLastBeat());
#else
	int iOldestRowFoundSoFar = 0;

	int i;

	for( i = int(m_TapNotes[0].size()); i>=0; i-- )		// iterate back to front
	{
		if( !IsRowEmpty(i) )
		{
			iOldestRowFoundSoFar = i;
			break;
		}
	}

	for( i=0; i<GetNumHoldsAndRolls(); i++ )
	{
		if( GetHoldNote(i).iEndRow > iOldestRowFoundSoFar )
			iOldestRowFoundSoFar = GetHoldNote(i).iEndRow;
	}

	return iOldestRowFoundSoFar;
#endif
}

float NoteData::GetLastBeat() const
{
	// TODO - Aldo_MX: Remove this and use m_Notes
	//return m_Notes.size() > 0 ? m_Notes[m_Notes.size()-1].fBeat : 0;

	return NoteRowToBeat( GetLastRow() );
}

/*int NoteData::GetNumTapNotes() const
{
	unsigned uNumNotes = 0;
	for( unsigned u=0, uu=m_Notes.size(); u<uu; u++ )
	{
		uNumNotes += m_Notes[u].GetNumSetCols();
	}
	return uNumNotes;
}*/

int NoteData::GetNumTapNotes( float fStartBeat, float fEndBeat ) const
{
	int iNumNotes = 0;

	if( fEndBeat == -1 )
		fEndBeat = GetNumBeats();

	int iStartIndex = BeatToNoteRow( fStartBeat );
	int iEndIndex = BeatToNoteRowRounded( fEndBeat );

	// Clamp to known-good ranges.
	iStartIndex = max( iStartIndex, 0 );
	iEndIndex = min( iEndIndex, GetNumRows()-1 );

	for( int t=0; t<m_iNumTracks; t++ )
	{
		for( int i=iStartIndex; i<=iEndIndex; i++ )
		{
			TapNote tn = GetTapNoteX(t, i);
			if( tn.type != TapNote::empty && tn.type != TapNote::mine && tn.type != TapNote::shock && tn.type != TapNote::potion && tn.type != TapNote::hidden )
				iNumNotes++;
		}
	}

	return iNumNotes;
}

int NoteData::GetNumRowsWithTap( float fStartBeat, float fEndBeat ) const
{
	int iNumNotes = 0;

	if(fEndBeat == -1) fEndBeat = GetNumBeats();
	int iStartIndex = BeatToNoteRow( fStartBeat );
	int iEndIndex = BeatToNoteRowRounded( fEndBeat );

	for( int i=iStartIndex; i<=iEndIndex; i++ )
		if( IsThereATapAtRow(i) )
			iNumNotes++;

	return iNumNotes;
}

int NoteData::GetNumMines( float fStartBeat, float fEndBeat ) const
{
	int iNumMines = 0;

	if( fEndBeat == -1 )
		fEndBeat = GetNumBeats();

	int iStartIndex = BeatToNoteRow( fStartBeat );
	int iEndIndex = BeatToNoteRowRounded( fEndBeat );

	// Clamp to known-good ranges.
	iStartIndex = max( iStartIndex, 0 );
	iEndIndex = min( iEndIndex, GetNumRows()-1 );

	for( int t=0; t<m_iNumTracks; t++ )
	{
		for( int i=iStartIndex; i<=iEndIndex; i++ )
			if( GetTapNoteX(t, i).type == TapNote::mine )
				iNumMines++;
	}

	return iNumMines;
}

int NoteData::GetNumShocks( float fStartBeat, float fEndBeat ) const
{
	int iNumShocks = 0;

	if( fEndBeat == -1 )
		fEndBeat = GetNumBeats();

	int iStartIndex = BeatToNoteRow( fStartBeat );
	int iEndIndex = BeatToNoteRowRounded( fEndBeat );

	// Clamp to known-good ranges.
	iStartIndex = max( iStartIndex, 0 );
	iEndIndex = min( iEndIndex, GetNumRows()-1 );

	for( int t=0; t<m_iNumTracks; t++ )
	{
		for( int i=iStartIndex; i<=iEndIndex; i++ )
			if( GetTapNoteX(t, i).type == TapNote::shock )
				iNumShocks++;
	}

	return iNumShocks;
}

int NoteData::GetNumPotions( float fStartBeat, float fEndBeat ) const
{
	int iNumPotions = 0;

	if( fEndBeat == -1 )
		fEndBeat = GetNumBeats();

	int iStartIndex = BeatToNoteRow( fStartBeat );
	int iEndIndex = BeatToNoteRowRounded( fEndBeat );

	// Clamp to known-good ranges.
	iStartIndex = max( iStartIndex, 0 );
	iEndIndex = min( iEndIndex, GetNumRows()-1 );

	for( int t=0; t<m_iNumTracks; t++ )
	{
		for( int i=iStartIndex; i<=iEndIndex; i++ )
			if( GetTapNoteX(t, i).type == TapNote::potion )
				iNumPotions++;
	}

	return iNumPotions;
}

int NoteData::GetNumLifts( float fStartBeat, float fEndBeat ) const
{
	int iNumLifts = 0;

	if( fEndBeat == -1 )
		fEndBeat = GetNumBeats();

	int iStartIndex = BeatToNoteRow( fStartBeat );
	int iEndIndex = BeatToNoteRowRounded( fEndBeat );

	// Clamp to known-good ranges.
	iStartIndex = max( iStartIndex, 0 );
	iEndIndex = min( iEndIndex, GetNumRows()-1 );

	for( int t=0; t<m_iNumTracks; t++ )
	{
		for( int i=iStartIndex; i<=iEndIndex; i++ )
			if( GetTapNoteX(t, i).type == TapNote::lift )
				iNumLifts++;
	}

	return iNumLifts;
}

int NoteData::GetNumHidden( float fStartBeat, float fEndBeat ) const
{
	int iNumHidden = 0;

	if( fEndBeat == -1 )
		fEndBeat = GetNumBeats();

	int iStartIndex = BeatToNoteRow( fStartBeat );
	int iEndIndex = BeatToNoteRowRounded( fEndBeat );

	// Clamp to known-good ranges.
	iStartIndex = max( iStartIndex, 0 );
	iEndIndex = min( iEndIndex, GetNumRows()-1 );

	for( int t=0; t<m_iNumTracks; t++ )
	{
		for( int i=iStartIndex; i<=iEndIndex; i++ )
			if( GetTapNoteX(t, i).type == TapNote::hidden )
				iNumHidden++;
	}

	return iNumHidden;
}

int NoteData::GetNumRowsWithTapOrHoldHead( float fStartBeat, float fEndBeat ) const
{
	int iNumNotes = 0;

	if(fEndBeat == -1) fEndBeat = GetNumBeats();
	int iStartIndex = BeatToNoteRow( fStartBeat );
	int iEndIndex = BeatToNoteRowRounded( fEndBeat );

	for( int i=iStartIndex; i<=iEndIndex; i++ )
	{
		if( IsThereATapOrHoldHeadAtRow(i) )
			iNumNotes++;
	}

	return iNumNotes;
}

int NoteData::GetNumRowsWithTapOrHiddenOrHoldHead( float fStartBeat, float fEndBeat ) const
{
	int iNumNotes = 0;

	if(fEndBeat == -1) fEndBeat = GetNumBeats();
	int iStartIndex = BeatToNoteRow( fStartBeat );
	int iEndIndex = BeatToNoteRowRounded( fEndBeat );

	for( int i=iStartIndex; i<=iEndIndex; i++ )
	{
		if( IsThereATapOrHiddenOrHoldHeadAtRow(i) )
			iNumNotes++;
	}

	return iNumNotes;
}

int NoteData::GetNumRowsWithCheckpoint( bool bCheckpointsOnly, float fStartBeat, float fEndBeat ) const
{
	int iNumNotes = 0;

	if(fEndBeat == -1) fEndBeat = GetNumBeats();
	int iStartIndex = BeatToNoteRow( fStartBeat );
	int iEndIndex = BeatToNoteRowRounded( fEndBeat );

	for( int i=iStartIndex; i<=iEndIndex; i++ )
	{
		if( IsThereACheckpointAtRow(i,bCheckpointsOnly) )
			iNumNotes++;
	}

	return iNumNotes;
}

int NoteData::RowNeedsHands( const int row ) const
{
	int iNumNotesThisIndex = 0;
	for( int t=0; t<m_iNumTracks; t++ )
	{
		TapNote tn = GetTapNoteX(t, row);
		switch( tn.type )
		{
		case TapNote::mine:
		case TapNote::shock:
		case TapNote::potion:
		case TapNote::hold_tail:
		case TapNote::empty:
			continue;	// skip these types - they don't count
		}
		++iNumNotesThisIndex;
	}

	// We must have at least one non-hold-body or non-roll-body at this row to count it.
	if( !iNumNotesThisIndex )
		return false;

	if( iNumNotesThisIndex < 3 )
	{
		// We have at least one, but not enough.  Count holds and rolls.
		for( int j=0; j<GetNumHoldsAndRolls(); j++ )
		{
			const HoldNote &hn = GetHoldNote(j);
			if( hn.iStartRow+1 <= row && row <= hn.iEndRow )
				++iNumNotesThisIndex;
		}
	}

	return iNumNotesThisIndex >= 3;
}

int NoteData::GetNumHands( float fStartBeat, float fEndBeat ) const
{
	/* Count the number of times you have to use your hands.  This includes
	 * three taps at the same time, a tap while two hold notes are being held,
	 * etc.  Only count rows that have at least one tap note (hold heads and roll heads count).
	 * Otherwise, every row of hold notes counts, so three simultaneous hold
	 * notes will count as hundreds of "hands". */
	if( fEndBeat == -1 )
		fEndBeat = GetNumBeats();

	int iStartIndex = BeatToNoteRow( fStartBeat );
	int iEndIndex = BeatToNoteRowRounded( fEndBeat );

	// Clamp to known-good ranges.
	iStartIndex = max( iStartIndex, 0 );
	iEndIndex = min( iEndIndex, GetNumRows()-1 );

	int iNum = 0;
	for( int i=iStartIndex; i<=iEndIndex; i++ )
	{
		if( !RowNeedsHands(i) )
			continue;

		iNum++;
	}

	return iNum;
}

int NoteData::GetNumN( int MinTaps, int MaxTaps, bool bPIU, bool bCheckpoints, float fStartBeat, float fEndBeat ) const
{
	if( fEndBeat == -1 )
		fEndBeat = GetNumBeats();

	int iStartIndex = BeatToNoteRow( fStartBeat );
	int iEndIndex = BeatToNoteRowRounded( fEndBeat );

	// Clamp to known-good ranges.
	iStartIndex = max( iStartIndex, 0 );
	iEndIndex = min( iEndIndex, GetNumRows()-1 );

	int iNum = 0;
	for( int i=iStartIndex; i<=iEndIndex; i++ )
	{
		int iNumNotesThisIndex = 0;
		for( int t=0; t<m_iNumTracks; t++ )
		{
			TapNote tn = GetTapNoteX(t, i);
			if( bPIU )
			{
				if( bCheckpoints && tn.type == TapNote::empty && tn.checkpoints > 0 )
					iNumNotesThisIndex++;
				else if( tn.type == TapNote::empty || tn.type == TapNote::hidden || tn.type == TapNote::mine || tn.type == TapNote::shock || tn.type == TapNote::potion )
					continue;
				else
					iNumNotesThisIndex++;
			}
			else
			{
				if( tn.type != TapNote::mine && tn.type != TapNote::shock && tn.type != TapNote::potion && tn.type != TapNote::empty )	// mines and attacks don't count
					iNumNotesThisIndex++;
			}
		}
		if( iNumNotesThisIndex >= MinTaps )
		{
			if( MaxTaps == -1 )
				iNum++;
			else if( iNumNotesThisIndex <= MaxTaps )
				iNum++;
		}
	}

	return iNum;
}

int NoteData::GetNumHoldsAndRolls( float fStartBeat, float fEndBeat, bool bCountHolds, bool bCountRolls ) const
{
	if( fEndBeat == -1 )
		fEndBeat = GetNumBeats();
	int iStartIndex = BeatToNoteRow( fStartBeat );
	int iEndIndex = BeatToNoteRowRounded( fEndBeat );

	int iNumCounted = 0;
	for( int i=0; i<GetNumHoldsAndRolls(); i++ )
	{
		const HoldNote &hn = GetHoldNote(i);

		if( hn.behaviorFlag & (TapNote::behavior_fake | TapNote::behavior_bonus) )
			continue;

		if( iStartIndex <= hn.iStartRow &&  hn.iEndRow <= iEndIndex )
		{
			switch( hn.subtype )
			{
			case HOLD_TYPE_DANCE:	if(bCountHolds) break; else continue;
			case HOLD_TYPE_ROLL:	if(bCountRolls) break; else continue;
			//case HOLD_TYPE_PIU:		if(bCountHolds) break; else continue;
			default:
				continue;
			}
			iNumCounted++;
		}
	}
	return iNumCounted;
}

int NoteData::GetNumHoldNotes( float fStartBeat, float fEndBeat ) const
{
	return GetNumHoldsAndRolls( fStartBeat, fEndBeat, true, false );
}

int NoteData::GetNumRollNotes( float fStartBeat, float fEndBeat ) const
{
	return GetNumHoldsAndRolls( fStartBeat, fEndBeat, false, true );
}

void NoteData::Convert2sAnd3sAnd4s()
{
	// Any note will end a hold or roll (not just a TAP_HOLD_TAIL).  This makes parsing DWIs much easier.
	// Plus, allowing tap notes in the middle of a hold doesn't make sense!
	int rows = GetLastRow();
	for( int col=0; col<m_iNumTracks; col++ )	// foreach column
	{
		for( int i=0; i<=rows; i++ )	// foreach TapNote element
		{
			if( GetTapNote(col,i).type == TapNote::hold_head )	// this is a HoldNote begin marker
			{
				SetTapNote(col, i, TAP_EMPTY);	// clear the hold head marker

				for( int j=i+1; j<=rows; j++ )	// search for end of HoldNote
				{
					// End hold on the next note we see.  This should be a hold_tail if the
					// data is in a consistent state, but doesn't have to be.
					TapNote tn = GetTapNote(col, j);
					if( tn.type == TapNote::empty )
						continue;

					HoldNote hn(col, i, j);
					hn.FromTapNote(tn);
					SetTapNote(col, j, TAP_EMPTY);
					AddHoldNote( hn );

					break;	// done searching for the end of this hold
				}
			}
			else
				continue; // Do nothing if it's not a hold or roll
		}
	}
}

// "102000301" == "109999001"
// "104000301" == "108888001"
void NoteData::ConvertBackTo2sAnd3sAnd4s()
{
	// copy HoldNotes into the new structure, but expand them into 2s and 3s
	for( int i=0; i<GetNumHoldsAndRolls(); i++ )
	{
		HoldNote &hn = GetHoldNote(i);

		// If they're the same, then they got clamped together, so just ignore it.
		if( hn.iStartRow != hn.iEndRow )
		{
			TapNote tnHold = TAP_ORIGINAL_HOLD_HEAD;

			hn.ToTapNote(tnHold);
			SetTapNote( hn.iTrack, hn.iStartRow, tnHold );

			tnHold.type = TapNote::hold_tail;
			SetTapNote( hn.iTrack, hn.iEndRow, tnHold );
		}
	}
	m_HoldNotes.clear();
}

void NoteData::To2sAnd3sAnd4s( const NoteData &out )
{
	CopyAll( &out );
	ConvertBackTo2sAnd3sAnd4s();
}

void NoteData::From2sAnd3sAnd4s( const NoteData &out )
{
	CopyAll( &out );
	Convert2sAnd3sAnd4s();
}

void NoteData::To9sAnd8s( const NoteData &out )
{
	CopyAll( &out );
	ConvertBackTo9sAnd8s();
}

void NoteData::From9sAnd8s( const NoteData &out )
{
	CopyAll( &out );
	Convert9sAnd8s();
}

/* "109999001" == "102000301"
 *
 * "9991" basically means "hold for three rows then hold for another tap";
 * since taps don't really have a length, it's equivalent to "9990".
 * So, make sure the character after a 9 is always a 0.
 *
 *
 * "108888001" == "104000301"
 *
 * "8881" basically means "hold for three rows then hold for another tap";
 * since taps don't really have a length, it's equivalent to "8880".
 * So, make sure the character after a 8 is always a 0. */
void NoteData::Convert9sAnd8s()
{
	int rows = GetLastRow();
	for( int col=0; col<m_iNumTracks; col++ )	// foreach column
	{
		for( int i=0; i<=rows; i++ )	// foreach TapNote element
		{
			TapNote tnHold = GetTapNote(col, i);
			if( tnHold.type == TapNote::hold )	// this is a HoldNote body
			{
				HoldNote hn(col, i, 0);
				hn.FromTapNote(tnHold);

				// search for end of HoldNote
				do {
					SetTapNote(col, i, TAP_EMPTY);
					i++;
				} while( (tnHold = GetTapNote(col, i)).type == TapNote::hold );
				SetTapNote(col, i, TAP_EMPTY);

				hn.iEndRow = i;
				AddHoldNote( hn );
			}
		}
	}
}

void NoteData::ConvertBackTo9sAnd8s()
{
	// copy HoldNotes into the new structure, but expand them into 9s
	for( int i=0; i<GetNumHoldsAndRolls(); i++ )
	{
		HoldNote &hn = GetHoldNote(i);
		for( int j = hn.iStartRow; j < hn.iEndRow; ++j)
		{
			TapNote tnHold = TAP_ORIGINAL_HOLD;
			hn.ToTapNote(tnHold);
			SetTapNote(hn.iTrack, j, tnHold);
		}
	}
	m_HoldNotes.clear();
}

// -1 for iOriginalTracksToTakeFrom means no track
void NoteData::LoadTransformed( const NoteData* pOriginal, int iNewNumTracks, const int iOriginalTrackToTakeFrom[] )
{
	// reset all notes
	Init();

	NoteData Original;
	Original.To9sAnd8s( *pOriginal );

	Config( Original );
	m_iNumTracks = iNewNumTracks;

	// copy tracks
	for( int t=0; t<m_iNumTracks; t++ )
	{
		const int iOriginalTrack = iOriginalTrackToTakeFrom[t];

		ASSERT_M( iOriginalTrack < Original.m_iNumTracks, ssprintf("from %i >= %i (to %i)",
			iOriginalTrack, Original.m_iNumTracks, iOriginalTrackToTakeFrom[t]));

		if( iOriginalTrack == -1 )
			continue;
		m_TapNotes[t] = Original.m_TapNotes[iOriginalTrack];
	}
	Convert9sAnd8s();
}

void NoteData::PadTapNotes(int rows)
{
	int needed = rows - m_TapNotes[0].size() + 1;
	if(needed < 0)
		return;

	needed += ROWS_PER_MEASURE; // optimization: give it a little more than it needs

	for(int track = 0; track < m_iNumTracks; ++track)
		m_TapNotes[track].insert( m_TapNotes[track].end(), needed, TAP_EMPTY );
}

void NoteData::MoveTapNoteTrack(int dest, int src)
{
	if(dest == src) return;
	m_TapNotes[dest] = m_TapNotes[src];
	m_TapNotes[src].clear();
}

void NoteData::SetTapNote(int track, int row, TapNote t, bool fake)
{
	if(row < 0) return;
	ASSERT(track < MAX_NOTE_TRACKS);

	if( !fake )
		t.drawType = t.type;

	PadTapNotes(row);
	m_TapNotes[track][row]=t;
}

void NoteData::ReserveRows( int row )
{
	for(int track = 0; track < m_iNumTracks; ++track)
		m_TapNotes[track].reserve( row );
}

void NoteData::EliminateAllButOneTap(int row)
{
	if(row < 0) return;

	PadTapNotes(row);

	int track;
	for(track = 0; track < m_iNumTracks; ++track)
	{
		if( m_TapNotes[track][row].type == TapNote::tap )
			break;
	}

	track++;

	for( ; track < m_iNumTracks; ++track)
	{
		if( m_TapNotes[track][row].type == TapNote::tap )
			m_TapNotes[track][row] = TAP_EMPTY;
	}
}

void NoteData::GetTracksHeldAtRow( int row, set<int>& addTo )
{
	for( unsigned i=0; i<m_HoldNotes.size(); i++ )
	{
		const HoldNote& hn = m_HoldNotes[i];
		if( hn.RowIsInRange(row) )
			addTo.insert( hn.iTrack );
	}
}

int NoteData::GetNumTracksHeldAtRow( int row )
{
	static set<int> viTracks;
	viTracks.clear();
	GetTracksHeldAtRow( row, viTracks );
	return viTracks.size();
}

#include "3rdparty/xxHash-0.6.2/xxhash.h"

unsigned NoteData::GetHash() const
{
	unsigned checksum = 0;

	XXH32(&m_iNumTracks, sizeof(int), checksum);
	XXH32(&m_uNumPlayers, sizeof(unsigned int), checksum);

	for (int i = 0; i < m_iNumTracks; i++) {
		if (!m_TapNotes[i].empty())
			XXH32(m_TapNotes[i].data(), sizeof(TapNote) * m_TapNotes[i].size(), checksum);
	}

	if (!m_HoldNotes.empty())
		XXH32(m_HoldNotes.data(), sizeof(HoldNote) * m_HoldNotes.size(), checksum);

	return checksum;
}

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * Contains code (c) 2001-2004 Chris Danford, Glenn Maynard.
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
