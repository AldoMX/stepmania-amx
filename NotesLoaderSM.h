/* SMLoader - reads a Song from an .SM file. */

#ifndef NOTES_LOADER_SM_H
#define NOTES_LOADER_SM_H

#include "Song.h"
#include "Steps.h"
#include "NotesLoader.h"
#include "EnumHelper.h"

class MsdFile;
class TimingData;

enum TimingToken
{
	TIMING_OFFSET = 0,
	TIMING_BPM,
	TIMING_STOP,
	TIMING_DELAY,
	TIMING_TICKCOUNT,
	TIMING_SPEED,
	TIMING_MULTIPLIER,
	TIMING_FAKE,
	TIMING_SPEED_AREA,
	NUM_TIMING_TOKENS
};
#define FOREACH_TimingToken( tt ) FOREACH_ENUM( TimingToken, NUM_TIMING_TOKENS, tt )

class SMLoader: public NotesLoader
{
	static void LoadFromSMTokens(
		CString sStepsType,
		CString sDescription,
		CString sCredit,
		CString sDifficulty,
		CString sMeter,
		CStringArray sAttackData,
		CString sRadarValues,
		CString sNoteData,
		Steps &out
	);

	bool FromCache;

public:
	SMLoader() { FromCache = false; }
	bool LoadFromSMFile( CString sPath, Song &out );
	bool LoadFromSMFile( CString sPath, Song &out, bool cache )
	{
		FromCache=cache;
		return LoadFromSMFile( sPath, out );
	}

	void GetApplicableFiles( CString sPath, CStringArray &out );
	bool LoadFromDir( CString sPath, Song &out );
	void TidyUpData( Song &song, bool cache );
	static bool LoadTimingFromFile( const CString &fn, TimingData &out );
	static void LoadTimingFromSMFile( const MsdFile &msd, TimingData &out );
	static bool LoadStepTimingFromFile( const CString &fn, TimingData &out, StepsType type, Difficulty diff, int meter, CString& description );
	static void LoadStepTimingFromSMFile( const MsdFile &msd, TimingData &out, StepsType type, Difficulty diff, int meter, CString& description );
	static bool LoadEdit( CString sEditFilePath, ProfileSlot slot );

	static bool LoadOffset( const CString& sTokens );
	static bool LoadBPMs( const CString& sTokens );
	static bool LoadStops( const CString& sTokens, bool bDelays = false );
	static bool LoadSpeeds( const CString& sTokens );
	static bool LoadTickcounts( const CString& sTokens );
	static bool LoadMultipliers( const CString& sTokens );
	static bool LoadFakes( const CString& sTokens );
	static bool LoadSpeedAreas( const CString& sTokens );
};

#endif

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * Contains code (c) 2001-2004 Chris Danford, Glenn Maynard.
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
