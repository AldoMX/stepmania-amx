#include "global.h"

#include <numeric>

#include "TimingData.h"
#include "NoteTypes.h"
#include "PrefsManager.h"
#include "GameState.h"

TimingData::TimingData()
{
	Reset();
}

void TimingData::Clear()
{
	m_BPMSegments.clear();
	m_StopSegments.clear();
	m_TickcountSegments.clear();
	m_SpeedSegments.clear();
	m_MultiplierSegments.clear();
	m_FakeAreaSegments.clear();
	m_SpeedAreaSegments.clear();
	m_fBeat0Offset = 0;
	m_fFirstBeat = 0;
	m_fLastBeat = 0;
}

void TimingData::Reset()
{
	Clear();
	SetBPMAtBeat( 0, 60.f );
	SetTickcountAtBeat( 0, 2 );
	SetSpeedAtBeat( 0, 1.f, 4.f, 1.f );
	SetMultiplierAtBeat( 0, 1, 1, 1.f, 1.f, 1.f, 1.f );
}

static int CompareBPMSegments(const BPMSegment &seg1, const BPMSegment &seg2)
{
	return seg1.m_fBeat < seg2.m_fBeat;
}

void SortBPMSegmentsArray( vector<BPMSegment> &arrayBPMSegments )
{
	sort( arrayBPMSegments.begin(), arrayBPMSegments.end(), CompareBPMSegments );
}

void TimingData::AddBPMSegment( const BPMSegment &seg )
{
	m_BPMSegments.push_back( seg );
	SortBPMSegmentsArray( m_BPMSegments );
}

void TimingData::SetBPMAtBeat( float fBeat, float fBPM )
{
	// Snap beat
	fBeat = NoteRowToBeat( BeatToNoteRow( fBeat ) );

	unsigned i;
	for( i=0; i<m_BPMSegments.size(); i++ )
		if( m_BPMSegments[i].m_fBeat == fBeat )
			break;

	if( i == m_BPMSegments.size() )	// there is no BPMSegment at the current beat
	{
		if( abs(fBPM) < 0.001f )
			return;

		// create a new BPMSegment
		AddBPMSegment( BPMSegment(fBeat, fBPM) );
	}
	else	// BPMSegment being modified is m_BPMSegments[i]
	{
		if( i > 0 )
		{
			if( abs(m_BPMSegments[i-1].m_fBPM - fBPM) < 0.001f || abs(fBPM) < 0.001f )
			{
				m_BPMSegments.erase( m_BPMSegments.begin()+i, m_BPMSegments.begin()+i+1);
				return;
			}
		}
		else
		{
			if( abs(fBPM) < 0.001f )
				fBPM = 60.f;
		}

		m_BPMSegments[i].m_fBPM = fBPM;
	}
}

float TimingData::GetBPMAtBeat( float fBeat ) const
{
	unsigned i;
	for( i=0; i<m_BPMSegments.size()-1; i++ )
		if( m_BPMSegments[i+1].m_fBeat > fBeat )
			break;
	return m_BPMSegments[i].m_fBPM;
}

BPMSegment& TimingData::GetBPMSegmentAtBeat( float fBeat )
{
	unsigned i;
	for( i=0; i<m_BPMSegments.size()-1; i++ )
		if( m_BPMSegments[i+1].m_fBeat > fBeat )
			break;
	return m_BPMSegments[i];
}

static int CompareStopSegments(const StopSegment &seg1, const StopSegment &seg2)
{
	return seg1.m_fBeat < seg2.m_fBeat;
}

void SortStopSegmentsArray( vector<StopSegment> &arrayStopSegments )
{
	sort( arrayStopSegments.begin(), arrayStopSegments.end(), CompareStopSegments );
}

void TimingData::AddStopSegment( const StopSegment &seg )
{
	m_StopSegments.push_back( seg );
	SortStopSegmentsArray( m_StopSegments );
}

float TimingData::GetStopLengthAtBeat( float fBeat ) const
{
	for( unsigned i=0; i<m_StopSegments.size(); i++ )
	{
		if( m_StopSegments[i].m_fBeat > fBeat )
			break;
		else if( m_StopSegments[i].m_fBeat == fBeat )
			return m_StopSegments[i].m_fSeconds;
	}
	return 0;
}

float TimingData::GetStopAtBeat( float fBeat, bool& bDelay ) const
{
	bDelay = false;

	for( unsigned i=0; i<m_StopSegments.size(); i++ )
	{
		if( m_StopSegments[i].m_fBeat > fBeat )
			break;
		else if( m_StopSegments[i].m_fBeat == fBeat )
		{
			bDelay = m_StopSegments[i].m_bDelay;
			return m_StopSegments[i].m_fSeconds;
		}
	}

	return 0;
}

void TimingData::SetStopAtBeat( float fBeat, float fSeconds, bool bDelay )
{
	// Snap beat
	fBeat = NoteRowToBeat( BeatToNoteRow( fBeat ) );

	unsigned i;
	for( i=0; i<m_StopSegments.size(); i++ )
		if( m_StopSegments[i].m_fBeat == fBeat )
			break;

	if( i == m_StopSegments.size() )	// there is no StopSegment at the current beat
	{
		if( abs(fSeconds) < 0.001f )
			return;

		// create a new StopSegment
		AddStopSegment( StopSegment(fBeat, fSeconds, bDelay) );
	}
	else	// StopSegment being modified is m_StopSegments[i]
	{
		if( abs( fSeconds ) < 0.001f )
		{
			m_StopSegments.erase( m_StopSegments.begin()+i, m_StopSegments.begin()+i+1);
			return;
		}

		m_StopSegments[i].m_fSeconds = fSeconds;
		m_StopSegments[i].m_bDelay = bDelay;
	}
}

StopSegment& TimingData::GetStopSegmentAtBeat( float fBeat )
{
	unsigned i;
	for( i=0; i<m_StopSegments.size(); i++ )
		if( m_StopSegments[i].m_fBeat == fBeat )
			break;
	return m_StopSegments[i];
}

// Tickcount stuff below
static int CompareTickcountSegments(const TickcountSegment &seg1, const TickcountSegment &seg2)
{
	return seg1.m_fBeat < seg2.m_fBeat;
}

void SortTickcountSegmentsArray( vector<TickcountSegment> &arrayTickcountSegments )
{
	sort( arrayTickcountSegments.begin(), arrayTickcountSegments.end(), CompareTickcountSegments );
}

void TimingData::AddTickcountSegment( const TickcountSegment &seg )
{
	m_TickcountSegments.push_back( seg );
	SortTickcountSegmentsArray( m_TickcountSegments );
}

void TimingData::SetTickcountAtBeat( float fBeat, unsigned uTickcount )
{
	// Snap beat
	fBeat = NoteRowToBeat( BeatToNoteRow( fBeat ) );

	// Force valid tickcounts only...
	if( uTickcount > ROWS_PER_BEAT )
		uTickcount = ROWS_PER_BEAT;

	else if( uTickcount > 0 )
		while( ROWS_PER_BEAT % uTickcount != 0 )
			uTickcount = (unsigned)ceil( fROWS_PER_BEAT / floor(fROWS_PER_BEAT/uTickcount) );

	unsigned i;
	for( i=0; i<m_TickcountSegments.size(); i++ )
		if( m_TickcountSegments[i].m_fBeat == fBeat )
			break;

	if( i == m_TickcountSegments.size() )	// there is no TickcountSegment at the current beat
	{
		// create a new TickcountSegment
		AddTickcountSegment( TickcountSegment(fBeat, uTickcount) );
	}
	else	// TickcountSegment being modified is m_TickcountSegments[i]
	{
		if( i > 0 )
		{
			if( m_TickcountSegments[i-1].m_uTickcount - uTickcount == 0 )
			{
				m_TickcountSegments.erase( m_TickcountSegments.begin()+i, m_TickcountSegments.begin()+i+1);
				return;
			}
		}

		m_TickcountSegments[i].m_uTickcount = uTickcount;
	}
}

unsigned TimingData::GetTickcountAtBeat( float fBeat ) const
{
	unsigned i;
	for( i=0; i<m_TickcountSegments.size()-1; i++ )
		if( m_TickcountSegments[i+1].m_fBeat > fBeat )
			break;
	return m_TickcountSegments[i].m_uTickcount;
}

float TimingData::GetTickcountStartAtBeat( float fBeat ) const
{
	unsigned i;
	for( i=0; i<m_TickcountSegments.size()-1; i++ )
		if( m_TickcountSegments[i+1].m_fBeat > fBeat )
			break;
	return m_TickcountSegments[i].m_fBeat;
}

void TimingData::DeleteTickcountAtBeat( float fBeat )
{
	if ( fBeat == 0 )
	{
		m_TickcountSegments[0].m_uTickcount = 2;
		return;
	}

	unsigned i;
	for( i=0; i<m_TickcountSegments.size(); i++ )
		if( m_TickcountSegments[i].m_fBeat == fBeat )
			break;

	if( i == m_TickcountSegments.size() )	// there is no TickcountSegment at the current beat
		return;
	else
		m_TickcountSegments.erase( m_TickcountSegments.begin()+i, m_TickcountSegments.begin()+i+1);
}

const TickcountSegment& TimingData::GetTickcountSegmentAtBeat( float fBeat ) const
{
	unsigned i;
	for( i=0; i<m_TickcountSegments.size()-1; i++ )
		if( m_TickcountSegments[i+1].m_fBeat > fBeat )
			break;
	return m_TickcountSegments[i];
}

static int CompareSpeedSegments(const SpeedSegment &seg1, const SpeedSegment &seg2)
{
	return seg1.m_fBeat < seg2.m_fBeat;
}

void SortSpeedSegmentsArray( vector<SpeedSegment> &arraySpeedSegments )
{
	sort( arraySpeedSegments.begin(), arraySpeedSegments.end(), CompareSpeedSegments );
}

void TimingData::AddSpeedSegment( const SpeedSegment &seg )
{
	m_SpeedSegments.push_back( seg );
	SortSpeedSegmentsArray( m_SpeedSegments );
}

void TimingData::SetSpeedAtBeat( float fBeat, float fToSpeed, float fBeats, float fFactor )
{
	// Snap beat
	fBeat = NoteRowToBeat( BeatToNoteRow( fBeat ) );

	unsigned i;
	for( i=0; i<m_SpeedSegments.size(); i++ )
		if( m_SpeedSegments[i].m_fBeat == fBeat )
			break;

	if( i == m_SpeedSegments.size() )	// there is no SpeedSegment at the current beat
	{
		// create a new SpeedSegment
		AddSpeedSegment( SpeedSegment(fBeat, fToSpeed, fBeats, fFactor ) );
	}
	else	// SpeedSegment being modified is m_SpeedSegments[i]
	{
		if( i > 0 )
		{
			if( m_SpeedSegments[i-1].m_fToSpeed - fToSpeed == 0 )
			{
				m_SpeedSegments.erase( m_SpeedSegments.begin()+i, m_SpeedSegments.begin()+i+1);
				return;
			}
		}

		m_SpeedSegments[i].m_fToSpeed = fToSpeed;
		m_SpeedSegments[i].m_fBeats = fBeats;
		m_SpeedSegments[i].m_fFactor = fFactor;
	}
}

void TimingData::DeleteSpeedAtBeat( float fBeat )
{
	if ( fBeat == 0 )
	{
		m_SpeedSegments[0].m_fToSpeed = 1.f;
		m_SpeedSegments[0].m_fBeats = 4.f;
		m_SpeedSegments[0].m_fFactor = 1.f;
		return;
	}

	unsigned i;
	for( i=0; i<m_SpeedSegments.size(); i++ )
		if( m_SpeedSegments[i].m_fBeat == fBeat )
			break;

	if( i == m_SpeedSegments.size() )	// there is no SpeedSegment at the current beat
		return;
	else
		m_SpeedSegments.erase( m_SpeedSegments.begin()+i, m_SpeedSegments.begin()+i+1);
}

SpeedSegment& TimingData::GetSpeedSegmentAtBeat( float fBeat )
{
	unsigned i;
	for( i=0; i<m_SpeedSegments.size()-1; i++ )
		if( m_SpeedSegments[i+1].m_fBeat > fBeat )
			break;
	return m_SpeedSegments[i];
}

unsigned TimingData::GetSpeedSegmentNumberAtBeat( float fBeat ) const
{
	unsigned i;
	for( i=0; i<m_SpeedSegments.size()-1; i++ )
		if( m_SpeedSegments[i+1].m_fBeat > fBeat )
			break;
	return i;
}

static int CompareMultiplierSegments(const MultiplierSegment &seg1, const MultiplierSegment &seg2)
{
	return seg1.m_fBeat < seg2.m_fBeat;
}

void SortMultiplierSegmentsArray( vector<MultiplierSegment> &arrayMultiplierSegments )
{
	sort( arrayMultiplierSegments.begin(), arrayMultiplierSegments.end(), CompareMultiplierSegments );
}

unsigned TimingData::GetHitMultiplierAtBeat( float fBeat, int tnsScore ) const
{
	unsigned i;
	for( i=0; i<m_MultiplierSegments.size()-1; i++ )
		if( m_MultiplierSegments[i+1].m_fBeat > fBeat )
			break;

	switch( tnsScore )
	{
	case TNS_NONE:
	case TNS_MISS_HIDDEN:
		return 0;
	case TNS_HIT_MINE:
	case TNS_HIT_SHOCK:
	case TNS_MISS_CHECKPOINT:
	case TNS_MISS:
	case TNS_BOO:
		return m_MultiplierSegments[i].m_uMiss;
	case TNS_GOOD:
		if( PREFSMAN->m_bComboBreakOnGood )
			return m_MultiplierSegments[i].m_uMiss;
		else
			return m_MultiplierSegments[i].m_uHit;
	case TNS_HIT_POTION:
	case TNS_CHECKPOINT:
	case TNS_HIDDEN:
	case TNS_GREAT:
	case TNS_PERFECT:
	case TNS_MARVELOUS:
		return m_MultiplierSegments[i].m_uHit;
	default:
		ASSERT(0);
		return 0;
	}
}

float TimingData::GetLifeMultiplierAtBeat( float fBeat, int tnsScore ) const
{
	unsigned i;
	for( i=0; i<m_MultiplierSegments.size()-1; i++ )
		if( m_MultiplierSegments[i+1].m_fBeat > fBeat )
			break;

	switch( tnsScore )
	{
	case TNS_NONE:
	case TNS_MISS_HIDDEN:
		return 0.f;
	case TNS_HIT_MINE:
	case TNS_HIT_SHOCK:
	case TNS_MISS_CHECKPOINT:
	case TNS_MISS:
	case TNS_BOO:
		return m_MultiplierSegments[i].m_fLifeMiss;
	case TNS_GOOD:
		if( PREFSMAN->m_bComboBreakOnGood )
			return m_MultiplierSegments[i].m_fLifeMiss;
		else
			return m_MultiplierSegments[i].m_fLifeHit;
	case TNS_HIT_POTION:
	case TNS_CHECKPOINT:
	case TNS_HIDDEN:
	case TNS_GREAT:
	case TNS_PERFECT:
	case TNS_MARVELOUS:
		return m_MultiplierSegments[i].m_fLifeHit;
	default:
		ASSERT(0);
		return 0;
	}
}

float TimingData::GetScoreMultiplierAtBeat( float fBeat, int tnsScore ) const
{
	unsigned i;
	for( i=0; i<m_MultiplierSegments.size()-1; i++ )
		if( m_MultiplierSegments[i+1].m_fBeat > fBeat )
			break;

	switch( tnsScore )
	{
	case TNS_NONE:
	case TNS_MISS_HIDDEN:
		return 0.f;
	case TNS_HIT_MINE:
	case TNS_HIT_SHOCK:
	case TNS_MISS_CHECKPOINT:
	case TNS_MISS:
	case TNS_BOO:
		return m_MultiplierSegments[i].m_fScoreMiss;
	case TNS_GOOD:
		if( PREFSMAN->m_bComboBreakOnGood )
			return m_MultiplierSegments[i].m_fScoreMiss;
		else
			return m_MultiplierSegments[i].m_fScoreHit;
	case TNS_HIT_POTION:
	case TNS_CHECKPOINT:
	case TNS_HIDDEN:
	case TNS_GREAT:
	case TNS_PERFECT:
	case TNS_MARVELOUS:
		return m_MultiplierSegments[i].m_fScoreHit;
	default:
		ASSERT(0);
		return 0;
	}
}

void TimingData::GetMultiplierAtBeat( float fBeat, int tnsScore, unsigned& uComboMultiplier, float& fLifeMultiplier, float& fScoreMultiplier )
{
	unsigned i;
	for( i=0; i<m_MultiplierSegments.size()-1; i++ )
		if( m_MultiplierSegments[i+1].m_fBeat > fBeat )
			break;

	switch( tnsScore )
	{
	case TNS_NONE:
	case TNS_MISS_HIDDEN:
		uComboMultiplier = 0;
		fLifeMultiplier = 0.f;
		fScoreMultiplier = 0.f;
		return;
	case TNS_HIT_MINE:
	case TNS_HIT_SHOCK:
	case TNS_MISS_CHECKPOINT:
	case TNS_MISS:
	case TNS_BOO:
		uComboMultiplier = m_MultiplierSegments[i].m_uMiss;
		fLifeMultiplier = m_MultiplierSegments[i].m_fLifeMiss;
		fScoreMultiplier = m_MultiplierSegments[i].m_fScoreMiss;
		return;
	case TNS_GOOD:
		if( PREFSMAN->m_bComboBreakOnGood )
		{
			uComboMultiplier = m_MultiplierSegments[i].m_uMiss;
			fLifeMultiplier = m_MultiplierSegments[i].m_fLifeMiss;
			fScoreMultiplier = m_MultiplierSegments[i].m_fScoreMiss;
			return;
		}
		else
		{
			uComboMultiplier = m_MultiplierSegments[i].m_uHit;
			fLifeMultiplier = m_MultiplierSegments[i].m_fLifeHit;
			fScoreMultiplier = m_MultiplierSegments[i].m_fScoreHit;
			return;
		}
	case TNS_HIT_POTION:
	case TNS_CHECKPOINT:
	case TNS_HIDDEN:
	case TNS_GREAT:
	case TNS_PERFECT:
	case TNS_MARVELOUS:
		uComboMultiplier = m_MultiplierSegments[i].m_uHit;
		fLifeMultiplier = m_MultiplierSegments[i].m_fLifeHit;
		fScoreMultiplier = m_MultiplierSegments[i].m_fScoreHit;
		return;
	default:
		ASSERT(0);
		return;
	}
}

void TimingData::SetMultiplierAtBeat( float fBeat, unsigned uHit, unsigned uMiss, float fLifeHit, float fScoreHit, float fLifeMiss, float fScoreMiss )
{
	// Snap beat
	fBeat = NoteRowToBeat( BeatToNoteRow( fBeat ) );

	unsigned i;
	for( i=0; i<m_MultiplierSegments.size(); i++ )
		if( m_MultiplierSegments[i].m_fBeat == fBeat )
			break;

	if( i == m_MultiplierSegments.size() )	// there is no MultiplierSegment at the current beat
	{
		// create a new MultiplierSegment
		AddMultiplierSegment( MultiplierSegment(fBeat, uHit, uMiss, fLifeHit, fScoreHit, fLifeMiss, fScoreMiss) );
	}
	else	// MultiplierSegment being modified is m_MultiplierSegments[i]
	{
		if( i > 0 )
		{
			if(	m_MultiplierSegments[i-1].m_uHit - uHit == 0 &&
				m_MultiplierSegments[i-1].m_uMiss - uMiss == 0 &&
				m_MultiplierSegments[i-1].m_fLifeHit - fLifeHit == 0 &&
				m_MultiplierSegments[i-1].m_fScoreHit - fScoreHit == 0 &&
				m_MultiplierSegments[i-1].m_fLifeMiss - fLifeMiss == 0 &&
				m_MultiplierSegments[i-1].m_fScoreMiss - fScoreMiss == 0
			)
			{
				m_MultiplierSegments.erase( m_MultiplierSegments.begin()+i, m_MultiplierSegments.begin()+i+1);
				return;
			}
		}

		m_MultiplierSegments[i].m_uHit = uHit;
		m_MultiplierSegments[i].m_uMiss = uMiss;
		m_MultiplierSegments[i].m_fLifeHit = fLifeHit;
		m_MultiplierSegments[i].m_fScoreHit = fScoreHit;
		m_MultiplierSegments[i].m_fLifeMiss = fLifeMiss;
		m_MultiplierSegments[i].m_fScoreMiss = fScoreMiss;
	}
}

void TimingData::DeleteMultiplierAtBeat( float fBeat )
{
	if ( fBeat == 0 )
	{
		m_MultiplierSegments[0].m_uHit = 1;
		m_MultiplierSegments[0].m_uMiss = 1;
		m_MultiplierSegments[0].m_fLifeHit = 1;
		m_MultiplierSegments[0].m_fScoreHit = 1;
		m_MultiplierSegments[0].m_fLifeMiss = 1;
		m_MultiplierSegments[0].m_fScoreMiss = 1;
		return;
	}

	unsigned i;
	for( i=0; i<m_MultiplierSegments.size(); i++ )
		if( m_MultiplierSegments[i].m_fBeat == fBeat )
			break;

	if( i == m_MultiplierSegments.size() )	// there is no MultiplierSegment at the current beat
		return;
	else
		m_MultiplierSegments.erase( m_MultiplierSegments.begin()+i, m_MultiplierSegments.begin()+i+1);
}

void TimingData::AddMultiplierSegment( const MultiplierSegment &seg )
{
	m_MultiplierSegments.push_back( seg );
	SortMultiplierSegmentsArray( m_MultiplierSegments );
}

MultiplierSegment& TimingData::GetMultiplierSegmentAtBeat( float fBeat )
{
	unsigned i;
	for( i=0; i<m_MultiplierSegments.size()-1; i++ )
		if( m_MultiplierSegments[i+1].m_fBeat > fBeat )
			break;
	return m_MultiplierSegments[i];
}

unsigned TimingData::GetMultiplierSegmentNumberAtBeat( float fBeat )
{
	unsigned i;
	for( i=0; i<m_MultiplierSegments.size()-1; i++ )
		if( m_MultiplierSegments[i+1].m_fBeat > fBeat )
			break;
	return i;
}

static int CompareFakeSegments(const AreaSegment &seg1, const AreaSegment &seg2)
{
	return seg1.m_fBeat < seg2.m_fBeat;
}

void SortFakeSegmentsArray( vector<AreaSegment> &arrayFakeSegments )
{
	sort( arrayFakeSegments.begin(), arrayFakeSegments.end(), CompareFakeSegments );
}

void TimingData::AddFakeSegment( const AreaSegment &seg )
{
	m_FakeAreaSegments.push_back( seg );
	SortFakeSegmentsArray( m_FakeAreaSegments );
}

void TimingData::SetFakeAtBeat( float fBeat, float fBeats )
{
	if( fBeats < 0 )
	{
		fBeat += fBeats;
		fBeats = abs( fBeats );
	}

	// Snap beat
	fBeat = NoteRowToBeat( BeatToNoteRow( fBeat ) );

	unsigned i;
	for( i=0; i<m_FakeAreaSegments.size(); i++ )
		if( m_FakeAreaSegments[i].m_fBeat <= fBeat && ( m_FakeAreaSegments[i].m_fBeat + m_FakeAreaSegments[i].m_fBeats ) >= fBeat )
			break;

	if( i == m_FakeAreaSegments.size() )	// there is no AreaSegment at the current beat
	{
		// create a new AreaSegment
		AddFakeSegment( AreaSegment(fBeat, fBeats) );
	}
	else	// AreaSegment being modified is m_FakeAreaSegments[i]
	{
		if( fBeats == 0.f )
		{
			m_FakeAreaSegments.erase( m_FakeAreaSegments.begin()+i, m_FakeAreaSegments.begin()+i+1);
			return;
		}

		m_FakeAreaSegments[i].m_fBeats = fBeats;
	}
}

bool TimingData::IsFakeAreaAtBeat( float fBeat ) const
{
	for( unsigned i=0; i<m_FakeAreaSegments.size(); ++i )
		if( m_FakeAreaSegments[i].m_fBeat <= fBeat && ( m_FakeAreaSegments[i].m_fBeat + m_FakeAreaSegments[i].m_fBeats ) >= fBeat )
			return true;
	return false;
}

AreaSegment& TimingData::GetFakeSegmentAtBeat( float fBeat )
{
	unsigned i;

	for( i=0; i<m_FakeAreaSegments.size()-1; ++i )
	{
		float fStartBeat = m_FakeAreaSegments[i].m_fBeat, 
			fEndBeat = m_FakeAreaSegments[i].m_fBeat + m_FakeAreaSegments[i].m_fBeats;

		if(	fStartBeat <= fBeat && fEndBeat >= fBeat )
		{
			//bIsExactMatch = true;
			break;
		}
		else if( fStartBeat > fBeat )
		{
			//bIsExactMatch = false;
			break;
		}
	}

	return m_FakeAreaSegments[i];
}

static int CompareSpeedAreaSegments(const SpeedAreaSegment &seg1, const SpeedAreaSegment &seg2)
{
	return seg1.m_fBeat < seg2.m_fBeat;
}

void SortSpeedAreaSegmentsArray( vector<SpeedAreaSegment> &arraySpeedAreaSegments )
{
	sort( arraySpeedAreaSegments.begin(), arraySpeedAreaSegments.end(), CompareSpeedAreaSegments );
}

void TimingData::AddSpeedAreaSegment( const SpeedAreaSegment &seg )
{
	m_SpeedAreaSegments.push_back( seg );
	SortSpeedAreaSegmentsArray( m_SpeedAreaSegments );
}

void TimingData::SetSpeedAreaAtBeat( float fBeat, float fFromSpeed, float fToSpeed, float fBeats, float fTriggerOffset, float fFactor )
{
	if( fBeats < 0 )
	{
		fBeat += fBeats;
		fBeats = abs( fBeats );
	}

	// Snap beat
	fBeat = NoteRowToBeat( BeatToNoteRow( fBeat ) );

	unsigned i;
	for( i=0; i<m_SpeedAreaSegments.size(); i++ )
		if( m_SpeedAreaSegments[i].m_fBeat <= fBeat && ( m_SpeedAreaSegments[i].m_fBeat + m_SpeedAreaSegments[i].m_fBeats ) >= fBeat )
			break;

	if( i == m_SpeedAreaSegments.size() )	// there is no SpeedAreaSegment at the current beat
	{
		// create a new SpeedAreaSegment
		AddSpeedAreaSegment( SpeedAreaSegment(fBeat, fFromSpeed, fToSpeed, fBeats, fTriggerOffset, fFactor) );
	}
	else	// SpeedAreaSegment being modified is m_SpeedAreaSegments[i]
	{
		if( fBeats == 0.f )
		{
			m_SpeedAreaSegments.erase( m_SpeedAreaSegments.begin()+i, m_SpeedAreaSegments.begin()+i+1);
			return;
		}

		m_SpeedAreaSegments[i].m_fBeats = fBeats;
	}
}

bool TimingData::IsSpeedAreaAtBeat( float fBeat ) const
{
	for( unsigned i=0; i<m_SpeedAreaSegments.size(); ++i )
		if( m_SpeedAreaSegments[i].m_fBeat <= fBeat && ( m_SpeedAreaSegments[i].m_fBeat + m_SpeedAreaSegments[i].m_fBeats ) >= fBeat )
			return true;
	return false;
}

SpeedAreaSegment& TimingData::GetSpeedAreaSegmentAtBeat( float fBeat )
{
	unsigned i;

	for( i=0; i<m_SpeedAreaSegments.size()-1; ++i )
	{
		float fStartBeat = m_SpeedAreaSegments[i].m_fBeat, 
			fEndBeat = m_SpeedAreaSegments[i].m_fBeat + m_SpeedAreaSegments[i].m_fBeats;

		if(	fStartBeat <= fBeat && fEndBeat >= fBeat )
		{
			//bIsExactMatch = true;
			break;
		}
		else if( fStartBeat > fBeat )
		{
			//bIsExactMatch = false;
			break;
		}
	}

	return m_SpeedAreaSegments[i];
}

// No need to check tick segments, as they won't affect the beat itself
void TimingData::GetBeatAndBPSFromElapsedTime( float fElapsedTime, float &fBeatOut, float &fBPSOut, bool &bFreezeOut, bool &bDelayOut ) const
{
//	LOG->Trace( "GetBeatAndBPSFromElapsedTime( fElapsedTime = %f )", fElapsedTime );

	fElapsedTime += PREFSMAN->m_fGlobalOffsetSeconds * GAMESTATE->m_SongOptions.m_fMusicRate;
	fElapsedTime += m_fBeat0Offset;

	for( unsigned i=0; i<m_BPMSegments.size(); i++ ) // foreach BPMSegment
	{
		const float fStartBeatThisSegment = m_BPMSegments[i].m_fBeat;
		const bool bIsFirstBPMSegment = i==0;
		const bool bIsLastBPMSegment = i==m_BPMSegments.size()-1;
		const float fStartBeatNextSegment = bIsLastBPMSegment ? 40000/*inf*/ : m_BPMSegments[i+1].m_fBeat;
		const float fBPS = m_BPMSegments[i].m_fBPM / 60.0f;

		for( unsigned j=0; j<m_StopSegments.size(); j++ )	// foreach StopSegment
		{
			const bool bDelay = m_StopSegments[j].m_bDelay;

			if( !bIsFirstBPMSegment )
			{
				if( !bDelay && fStartBeatThisSegment >= m_StopSegments[j].m_fBeat )
					continue;
				else if( bDelay && fStartBeatThisSegment > m_StopSegments[j].m_fBeat )
					continue;
			}

			if( !bIsLastBPMSegment )
			{
				if( !bDelay && m_StopSegments[j].m_fBeat > fStartBeatNextSegment )
					continue;
				else if( bDelay && m_StopSegments[j].m_fBeat >= fStartBeatNextSegment )
					continue;
			}

			// this stop lies within this BPMSegment
			const float fBeatsSinceStartOfSegment = m_StopSegments[j].m_fBeat - fStartBeatThisSegment;
			const float fFreezeStartSecond = fBeatsSinceStartOfSegment / fBPS;

			if( fFreezeStartSecond >= fElapsedTime )
				break;

			// the freeze segment is <= current time
			fElapsedTime -= m_StopSegments[j].m_fSeconds;

			if( fFreezeStartSecond >= fElapsedTime )
			{
				/* The time lies within the stop. */
				fBeatOut = m_StopSegments[j].m_fBeat;
				fBPSOut = fBPS;
				if( bDelay )
				{
					bFreezeOut = false;
					bDelayOut = true;
				}
				else
				{
					bDelayOut = false;
					bFreezeOut = true;
				}
				return;
			}
		}

		const float fBeatsInThisSegment = fStartBeatNextSegment - fStartBeatThisSegment;
		const float fSecondsInThisSegment =  fBeatsInThisSegment / fBPS;
		if( bIsLastBPMSegment || fElapsedTime <= fSecondsInThisSegment )
		{
			// this BPMSegment IS the current segment
			fBeatOut = fStartBeatThisSegment + fElapsedTime*fBPS;
			fBPSOut = fBPS;
			bDelayOut = false;
			bFreezeOut = false;
			return;
		}

		// this BPMSegment is NOT the current segment
		fElapsedTime -= fSecondsInThisSegment;
	}
}

// No need to check tick segments, as they won't affect the beat itself
float TimingData::GetElapsedTimeFromBeat( float fBeat ) const
{
	float fElapsedTime = 0;
	fElapsedTime -= PREFSMAN->m_fGlobalOffsetSeconds * GAMESTATE->m_SongOptions.m_fMusicRate;
	fElapsedTime -= m_fBeat0Offset;

	for( unsigned j=0; j<m_StopSegments.size(); j++ )	// foreach freeze
	{
		// The exact beat comes after a delay, but before a stop.
		if( ( !m_StopSegments[j].m_bDelay && m_StopSegments[j].m_fBeat >= fBeat ) ||
			( m_StopSegments[j].m_bDelay && m_StopSegments[j].m_fBeat > fBeat ) )
			break;
		fElapsedTime += m_StopSegments[j].m_fSeconds;
	}

	for( unsigned i=0; i<m_BPMSegments.size(); i++ ) // foreach BPMSegment
	{
		const bool bIsLastBPMSegment = i==m_BPMSegments.size()-1;
		const float fBPS = m_BPMSegments[i].m_fBPM / 60.0f;

		if( bIsLastBPMSegment )
		{
			fElapsedTime += fBeat / fBPS;
		}
		else
		{
			const float fStartBeatThisSegment = m_BPMSegments[i].m_fBeat;
			const float fStartBeatNextSegment = m_BPMSegments[i+1].m_fBeat;
			const float fBeatsInThisSegment = fStartBeatNextSegment - fStartBeatThisSegment;
			fElapsedTime += min(fBeat, fBeatsInThisSegment) / fBPS;
			fBeat -= fBeatsInThisSegment;
		}

		if( fBeat <= 0 )
			return fElapsedTime;
	}

	return fElapsedTime;
}

void TimingData::ScaleRegion( float fScale, float fStartBeat, float fEndBeat )
{
	ASSERT( fScale > 0 );
	ASSERT( fStartBeat >= 0 );
	ASSERT( fStartBeat < fEndBeat );

	unsigned ix = 0;

	for ( ix = 0; ix < m_BPMSegments.size(); ix++ )
	{
		const float fSegStart = m_BPMSegments[ix].m_fBeat;
		if( fSegStart < fStartBeat )
			continue;
		else if( fSegStart > fEndBeat )
			m_BPMSegments[ix].m_fBeat += (fEndBeat - fStartBeat) * (fScale - 1);
		else
			m_BPMSegments[ix].m_fBeat = (fSegStart - fStartBeat) * fScale + fStartBeat;
	}

	for( ix = 0; ix < m_StopSegments.size(); ix++ )
	{
		const float fSegStart = m_StopSegments[ix].m_fBeat;
		if( fSegStart < fStartBeat )
			continue;
		else if( fSegStart > fEndBeat )
			m_StopSegments[ix].m_fBeat += (fEndBeat - fStartBeat) * (fScale - 1);
		else
			m_StopSegments[ix].m_fBeat = (fSegStart - fStartBeat) * fScale + fStartBeat;
	}

	for ( ix = 0; ix < m_TickcountSegments.size(); ix++ )
	{
		const float fSegStart = m_TickcountSegments[ix].m_fBeat;
		if( fSegStart < fStartBeat )
			continue;
		else if( fSegStart > fEndBeat )
			m_TickcountSegments[ix].m_fBeat += (fEndBeat - fStartBeat) * (fScale - 1);
		else
			m_TickcountSegments[ix].m_fBeat = (fSegStart - fStartBeat) * fScale + fStartBeat;
	}

	for( ix = 0; ix < m_SpeedSegments.size(); ix++ )
	{
		const float fSegStart = m_SpeedSegments[ix].m_fBeat;
		if( fSegStart < fStartBeat )
			continue;
		else if( fSegStart > fEndBeat )
			m_SpeedSegments[ix].m_fBeat += (fEndBeat - fStartBeat) * (fScale - 1);
		else
			m_SpeedSegments[ix].m_fBeat = (fSegStart - fStartBeat) * fScale + fStartBeat;
	}

	for( ix = 0; ix < m_MultiplierSegments.size(); ix++ )
	{
		const float fSegStart = m_MultiplierSegments[ix].m_fBeat;
		if( fSegStart < fStartBeat )
			continue;
		else if( fSegStart > fEndBeat )
			m_MultiplierSegments[ix].m_fBeat += (fEndBeat - fStartBeat) * (fScale - 1);
		else
			m_MultiplierSegments[ix].m_fBeat = (fSegStart - fStartBeat) * fScale + fStartBeat;
	}

	for( ix = 0; ix < m_FakeAreaSegments.size(); ix++ )
	{
		const float fSegStart = m_FakeAreaSegments[ix].m_fBeat;
		if( fSegStart < fStartBeat )
		{
			if( fSegStart + m_FakeAreaSegments[ix].m_fBeats > fStartBeat )
			{
				if( fSegStart + m_FakeAreaSegments[ix].m_fBeats > fEndBeat )
					m_FakeAreaSegments[ix].m_fBeats += (fEndBeat - fStartBeat) * (fScale - 1);
				else
					m_FakeAreaSegments[ix].m_fBeats += (fSegStart + m_FakeAreaSegments[ix].m_fBeats - fStartBeat) * (fScale - 1);
			}
		}
		else if( fSegStart > fEndBeat )
			m_FakeAreaSegments[ix].m_fBeat += (fEndBeat - fStartBeat) * (fScale - 1);
		else
		{
			m_FakeAreaSegments[ix].m_fBeat = (fSegStart - fStartBeat) * fScale + fStartBeat;

			if( fSegStart + m_FakeAreaSegments[ix].m_fBeats > fEndBeat )
				m_FakeAreaSegments[ix].m_fBeats += (fEndBeat - fSegStart) * (fScale - 1);
			else
				m_FakeAreaSegments[ix].m_fBeats *= fScale;
		}
	}

	for( ix = 0; ix < m_SpeedAreaSegments.size(); ix++ )
	{
		const float fSegStart = m_SpeedAreaSegments[ix].m_fBeat;
		if( fSegStart < fStartBeat )
		{
			if( fSegStart + m_SpeedAreaSegments[ix].m_fBeats > fStartBeat )
			{
				if( fSegStart + m_SpeedAreaSegments[ix].m_fBeats > fEndBeat )
					m_SpeedAreaSegments[ix].m_fBeats += (fEndBeat - fStartBeat) * (fScale - 1);
				else
					m_SpeedAreaSegments[ix].m_fBeats += (fSegStart + m_SpeedAreaSegments[ix].m_fBeats - fStartBeat) * (fScale - 1);
			}
		}
		else if( fSegStart > fEndBeat )
			m_SpeedAreaSegments[ix].m_fBeat += (fEndBeat - fStartBeat) * (fScale - 1);
		else
		{
			m_SpeedAreaSegments[ix].m_fBeat = (fSegStart - fStartBeat) * fScale + fStartBeat;

			if( fSegStart + m_SpeedAreaSegments[ix].m_fBeats > fEndBeat )
				m_SpeedAreaSegments[ix].m_fBeats += (fEndBeat - fSegStart) * (fScale - 1);
			else
				m_SpeedAreaSegments[ix].m_fBeats *= fScale;
		}
	}
}

void TimingData::ShiftBeats( float fStartBeat, float fBeatsToShift )
{
	unsigned ix = 0;

	for( ix = 0; ix < m_BPMSegments.size(); ix++ )
	{
		float &fSegStart = m_BPMSegments[ix].m_fBeat;
		if( fSegStart < fStartBeat )
			continue;

		fSegStart += fBeatsToShift;
		fSegStart = max( fSegStart, fStartBeat );
	}

	for( ix = 0; ix < m_StopSegments.size(); ix++ )
	{
		float &fSegStart = m_StopSegments[ix].m_fBeat;
		if( fSegStart < fStartBeat )
			continue;
		fSegStart += fBeatsToShift;
		fSegStart = max( fSegStart, fStartBeat );
	}

	for( ix = 0; ix < m_TickcountSegments.size(); ix++ )
	{
		float &fSegStart = m_TickcountSegments[ix].m_fBeat;
		if( fSegStart < fStartBeat )
			continue;

		fSegStart += fBeatsToShift;
		fSegStart = max( fSegStart, fStartBeat );
	}

	for( ix = 0; ix < m_SpeedSegments.size(); ix++ )
	{
		float &fSegStart = m_SpeedSegments[ix].m_fBeat;
		if( fSegStart < fStartBeat )
			continue;

		fSegStart += fBeatsToShift;
		fSegStart = max( fSegStart, fStartBeat );
	}

	for( ix = 0; ix < m_MultiplierSegments.size(); ix++ )
	{
		float &fSegStart = m_MultiplierSegments[ix].m_fBeat;
		if( fSegStart < fStartBeat )
			continue;

		fSegStart += fBeatsToShift;
		fSegStart = max( fSegStart, fStartBeat );
	}

	for( ix = 0; ix < m_FakeAreaSegments.size(); ix++ )
	{
		float &fSegStart = m_FakeAreaSegments[ix].m_fBeat;
		if( fSegStart < fStartBeat )
			continue;

		fSegStart += fBeatsToShift;
		fSegStart = max( fSegStart, fStartBeat );
	}

	for( ix = 0; ix < m_SpeedAreaSegments.size(); ix++ )
	{
		float &fSegStart = m_SpeedAreaSegments[ix].m_fBeat;
		if( fSegStart < fStartBeat )
			continue;

		fSegStart += fBeatsToShift;
		fSegStart = max( fSegStart, fStartBeat );
	}
}

bool TimingData::HasBpmChangesOrStops() const
{
	return m_BPMSegments.size() > 1 || m_StopSegments.size() > 0;
}

void TimingData::TidyUpData()
{
	// Make sure the first BPM segment starts at beat 0.
	if( m_BPMSegments[0].m_fBeat != 0 )
		m_BPMSegments[0].m_fBeat = 0;

	// Make sure the first Speed segment starts at beat 0.
	if( m_SpeedSegments[0].m_fBeat != 0 )
		m_SpeedSegments[0].m_fBeat = 0;

	// Make sure the first Tickcount segment starts at beat 0.
	if( m_TickcountSegments[0].m_fBeat != 0 )
		m_TickcountSegments[0].m_fBeat = 0;

	// Make sure the first Multiplier segment starts at beat 0.
	if( m_MultiplierSegments[0].m_fBeat != 0 )
		m_MultiplierSegments[0].m_fBeat = 0;
}

void TimingData::ReCalculateFirstAndLastBeat( float fFirstSecond, float fLastSecond, float fMusicLengthSeconds )
{
	if( fFirstSecond == FLT_MAX )
		fFirstSecond = 0;

	if( fLastSecond == -FLT_MAX || fLastSecond > fMusicLengthSeconds )
		fLastSecond = fMusicLengthSeconds;

	m_fFirstBeat = GetBeatFromElapsedTime( fFirstSecond );
	m_fLastBeat = GetBeatFromElapsedTime( fLastSecond );

	if( m_fFirstBeat < 0 )
		m_fFirstBeat = 0;
}

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * Contains code (c) 2001-2004 Chris Danford, Glenn Maynard.
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
