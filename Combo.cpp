#include "global.h"
#include "Combo.h"
#include "ThemeManager.h"
#include "StageStats.h"
#include "GameState.h"
#include "Song.h"
#include "PrefsManager.h"

static CachedThemeMetricI	SHOW_COMBO_AT		("Combo","ShowComboAt");
static CachedThemeMetricB	SHOW_REPEATED		("Combo","ShowWhenRepeated");

static CachedThemeMetricB	USE_LABEL_COMMANDS	("Combo","UseLabelCommands");
static CachedThemeMetricF	LABEL_X				("Combo","LabelX");
static CachedThemeMetricF	LABEL_Y				("Combo","LabelY");
static CachedThemeMetricI	LABEL_HORIZ_ALIGN	("Combo","LabelHorizAlign");
static CachedThemeMetricI	LABEL_VERT_ALIGN	("Combo","LabelVertAlign");
static CachedThemeMetricF	LABEL_SHADOW_LENGTH	("Combo","LabelShadowLength");
static CachedThemeMetricF	NUMBER_X			("Combo","NumberX");
static CachedThemeMetricF	NUMBER_Y			("Combo","NumberY");
static CachedThemeMetricI	NUMBER_HORIZ_ALIGN	("Combo","NumberHorizAlign");
static CachedThemeMetricI	NUMBER_VERT_ALIGN	("Combo","NumberVertAlign");
static CachedThemeMetricF	NUMBER_SHADOW_LENGTH("Combo","NumberShadowLength");
static CachedThemeMetricF	NUMBER_MIN_ZOOM		("Combo","NumberMinZoom");
static CachedThemeMetricF	NUMBER_MAX_ZOOM		("Combo","NumberMaxZoom");
static CachedThemeMetricF	NUMBER_MAX_ZOOM_AT	("Combo","NumberMaxZoomAt");
static CachedThemeMetricF	PULSE_ZOOM			("Combo","PulseZoom");
static CachedThemeMetricF	C_TWEEN_SECONDS		("Combo","TweenSeconds");

static CachedThemeMetricI	MINIMUM_COMBO_DIGITS	("Combo","MinimumNumberShownDigits");
static CachedThemeMetricI	MAXIMUM_COMBO_DIGITS	("Combo","MaximumNumberShownDigits");

static CachedThemeMetricB	NUMBER_SET_COLOR	("Combo","NumberSetColor");
static CachedThemeMetricC	NUMBER_COLOR		("Combo","NumberColor");
static CachedThemeMetricB	NUMBER_SET_RAINBOW	("Combo","NumberSetRainbow");

static CachedThemeMetricI	SHOW_MISS_COMBO_AT		("MissCombo","ShowComboAt");
static CachedThemeMetricB	MISS_SHOW_REPEATED		("MissCombo","ShowWhenRepeated");

static CachedThemeMetric	MISS_COMMAND			("MissCombo","Command");
static CachedThemeMetricF	MISS_LABEL_X			("MissCombo","LabelX");
static CachedThemeMetricF	MISS_LABEL_Y			("MissCombo","LabelY");
static CachedThemeMetricI	MISS_LABEL_HORIZ_ALIGN	("MissCombo","LabelHorizAlign");
static CachedThemeMetricI	MISS_LABEL_VERT_ALIGN	("MissCombo","LabelVertAlign");
static CachedThemeMetricF	MISS_LABEL_SHADOW_LENGTH("MissCombo","LabelShadowLength");
static CachedThemeMetricF	MISS_NUMBER_X			("MissCombo","NumberX");
static CachedThemeMetricF	MISS_NUMBER_Y			("MissCombo","NumberY");
static CachedThemeMetricI	MISS_NUMBER_HORIZ_ALIGN	("MissCombo","NumberHorizAlign");
static CachedThemeMetricI	MISS_NUMBER_VERT_ALIGN	("MissCombo","NumberVertAlign");
static CachedThemeMetricF	MISS_NUMBER_SHADOW_LENGTH("MissCombo","NumberShadowLength");
static CachedThemeMetricF	MISS_NUMBER_MIN_ZOOM	("MissCombo","NumberMinZoom");
static CachedThemeMetricF	MISS_NUMBER_MAX_ZOOM	("MissCombo","NumberMaxZoom");
static CachedThemeMetricF	MISS_NUMBER_MAX_ZOOM_AT	("MissCombo","NumberMaxZoomAt");
static CachedThemeMetricF	MISS_PULSE_ZOOM			("MissCombo","PulseZoom");
static CachedThemeMetricF	MISS_C_TWEEN_SECONDS	("MissCombo","TweenSeconds");

static CachedThemeMetricI	MINIMUM_MISS_COMBO_DIGITS	("MissCombo","MinimumNumberShownDigits");
static CachedThemeMetricI	MAXIMUM_MISS_COMBO_DIGITS	("MissCombo","MaximumNumberShownDigits");

static CachedThemeMetricB	MISS_NUMBER_SET_COLOR	("MissCombo","NumberSetColor");
static CachedThemeMetricC	MISS_NUMBER_COLOR		("MissCombo","NumberColor");
static CachedThemeMetricB	MISS_NUMBER_SET_RAINBOW	("MissCombo","NumberSetRainbow");

static CachedThemeMetricF	PERCENT_SONG_PASSED				("Combo","PercentSongPlayedBeforeFullComboEffects");
static CachedThemeMetric	FULL_COMBO_MARVELOUSES_COMMAND	("Combo","FullComboMarvelousesCommand");
static CachedThemeMetric	FULL_COMBO_PERFECTS_COMMAND		("Combo","FullComboPerfectsCommand");
static CachedThemeMetric	FULL_COMBO_GREATS_COMMAND		("Combo","FullComboGreatsCommand");
static CachedThemeMetric	FULL_COMBO_GOODS_COMMAND		("Combo","FullComboGoodsCommand");
static CachedThemeMetric	FULL_COMBO_BROKEN_COMMAND		("Combo","FullComboBrokenCommand");
static CachedThemeMetric	FULL_COMBO_NORMAL_COMMAND		("Combo","FullComboNormalCommand");
static CachedThemeMetricB	NORMAL_USE_BROKEN_COMMAND		("Combo","UseBrokenCommandForNormalCommand");

static CString MISS_COMMAND_LABEL;
static CString FULL_COMBO_MARVELOUSES_COMMAND_LABEL;
static CString FULL_COMBO_PERFECTS_COMMAND_LABEL;
static CString FULL_COMBO_GREATS_COMMAND_LABEL;
static CString FULL_COMBO_GOODS_COMMAND_LABEL;
static CString FULL_COMBO_BROKEN_COMMAND_LABEL;
static CString FULL_COMBO_NORMAL_COMMAND_LABEL;

Combo::Combo()
{
	SHOW_COMBO_AT.Refresh();
	SHOW_REPEATED.Refresh();

	USE_LABEL_COMMANDS.Refresh();
	LABEL_X.Refresh();
	LABEL_Y.Refresh();
	LABEL_HORIZ_ALIGN.Refresh();
	LABEL_VERT_ALIGN.Refresh();
	LABEL_SHADOW_LENGTH.Refresh();
	NUMBER_X.Refresh();
	NUMBER_Y.Refresh();
	NUMBER_HORIZ_ALIGN.Refresh();
	NUMBER_VERT_ALIGN.Refresh();
	NUMBER_SHADOW_LENGTH.Refresh();
	NUMBER_MIN_ZOOM.Refresh();
	NUMBER_MAX_ZOOM.Refresh();
	NUMBER_MAX_ZOOM_AT.Refresh();
	PULSE_ZOOM.Refresh();
	C_TWEEN_SECONDS.Refresh();

	MINIMUM_COMBO_DIGITS.Refresh();
	MAXIMUM_COMBO_DIGITS.Refresh();

	NUMBER_SET_COLOR.Refresh();
	NUMBER_COLOR.Refresh();
	NUMBER_SET_RAINBOW.Refresh();

	SHOW_MISS_COMBO_AT.Refresh();
	MISS_SHOW_REPEATED.Refresh();

	MISS_COMMAND.Refresh();
	MISS_LABEL_X.Refresh();
	MISS_LABEL_Y.Refresh();
	MISS_LABEL_HORIZ_ALIGN.Refresh();
	MISS_LABEL_VERT_ALIGN.Refresh();
	MISS_LABEL_SHADOW_LENGTH.Refresh();
	MISS_NUMBER_X.Refresh();
	MISS_NUMBER_Y.Refresh();
	MISS_NUMBER_HORIZ_ALIGN.Refresh();
	MISS_NUMBER_VERT_ALIGN.Refresh();
	MISS_NUMBER_SHADOW_LENGTH.Refresh();
	MISS_NUMBER_MIN_ZOOM.Refresh();
	MISS_NUMBER_MAX_ZOOM.Refresh();
	MISS_NUMBER_MAX_ZOOM_AT.Refresh();
	MISS_PULSE_ZOOM.Refresh();
	MISS_C_TWEEN_SECONDS.Refresh();

	MINIMUM_MISS_COMBO_DIGITS.Refresh();
	MAXIMUM_MISS_COMBO_DIGITS.Refresh();

	MISS_NUMBER_SET_COLOR.Refresh();
	MISS_NUMBER_COLOR.Refresh();
	MISS_NUMBER_SET_RAINBOW.Refresh();

	PERCENT_SONG_PASSED.Refresh();
	FULL_COMBO_MARVELOUSES_COMMAND.Refresh();
	FULL_COMBO_PERFECTS_COMMAND.Refresh();
	FULL_COMBO_GREATS_COMMAND.Refresh();
	FULL_COMBO_GOODS_COMMAND.Refresh();
	FULL_COMBO_BROKEN_COMMAND.Refresh();
	FULL_COMBO_NORMAL_COMMAND.Refresh();
	NORMAL_USE_BROKEN_COMMAND.Refresh();

	if( USE_LABEL_COMMANDS )
	{
		MISS_COMMAND_LABEL =					THEME->GetMetric("MissCombo", "LabelCommand");
		FULL_COMBO_MARVELOUSES_COMMAND_LABEL =	THEME->GetMetric("Combo", "FullComboMarvelousesLabelCommand");
		FULL_COMBO_PERFECTS_COMMAND_LABEL =		THEME->GetMetric("Combo", "FullComboPerfectsLabelCommand");
		FULL_COMBO_GREATS_COMMAND_LABEL =		THEME->GetMetric("Combo", "FullComboGreatsLabelCommand");
		FULL_COMBO_GOODS_COMMAND_LABEL =		THEME->GetMetric("Combo", "FullComboGoodsLabelCommand");
		FULL_COMBO_BROKEN_COMMAND_LABEL =		THEME->GetMetric("Combo", "FullComboBrokenLabelCommand");
		FULL_COMBO_NORMAL_COMMAND_LABEL =		THEME->GetMetric("Combo", "FullComboNormalLabelCommand");
	}
	else
	{
		MISS_COMMAND_LABEL =					MISS_COMMAND;
		FULL_COMBO_MARVELOUSES_COMMAND_LABEL =	FULL_COMBO_MARVELOUSES_COMMAND;
		FULL_COMBO_PERFECTS_COMMAND_LABEL =		FULL_COMBO_PERFECTS_COMMAND;
		FULL_COMBO_GREATS_COMMAND_LABEL =		FULL_COMBO_GREATS_COMMAND;
		FULL_COMBO_GOODS_COMMAND_LABEL =		FULL_COMBO_GOODS_COMMAND;
		FULL_COMBO_BROKEN_COMMAND_LABEL =		FULL_COMBO_BROKEN_COMMAND;
		FULL_COMBO_NORMAL_COMMAND_LABEL =		FULL_COMBO_NORMAL_COMMAND;
	}

	m_sprComboLabel.Load( THEME->GetPathToG( "Combo label") );
	m_sprComboLabel.SetShadowLength( LABEL_SHADOW_LENGTH );
	m_sprComboLabel.StopAnimating();
	m_sprComboLabel.SetXY( LABEL_X, LABEL_Y );
	m_sprComboLabel.SetHorizAlign( (Actor::HorizAlign)(int)LABEL_HORIZ_ALIGN );
	m_sprComboLabel.SetVertAlign( (Actor::VertAlign)(int)LABEL_VERT_ALIGN );
	m_sprComboLabel.SetHidden( true );
	this->AddChild( &m_sprComboLabel );

	m_sprMissesLabel.Load( THEME->GetPathToG( "Combo misses") );
	m_sprMissesLabel.SetShadowLength( MISS_LABEL_SHADOW_LENGTH );
	m_sprMissesLabel.StopAnimating();
	m_sprMissesLabel.SetXY( MISS_LABEL_X, MISS_LABEL_Y );
	m_sprMissesLabel.SetHorizAlign( (Actor::HorizAlign)(int)MISS_LABEL_HORIZ_ALIGN );
	m_sprMissesLabel.SetVertAlign( (Actor::VertAlign)(int)MISS_LABEL_VERT_ALIGN );
	m_sprMissesLabel.SetHidden( true );
	this->AddChild( &m_sprMissesLabel );

	m_textNumber.LoadFromFont( THEME->GetPathToF("Combo") );
	m_textNumber.SetShadowLength( NUMBER_SHADOW_LENGTH );
	m_textNumber.SetXY( NUMBER_X, NUMBER_Y );
	m_textNumber.SetHorizAlign( (Actor::HorizAlign)(int)NUMBER_HORIZ_ALIGN );
	m_textNumber.SetVertAlign( (Actor::VertAlign)(int)NUMBER_VERT_ALIGN );
	m_textNumber.SetHidden( true );
	if( NUMBER_SET_RAINBOW )
		m_textNumber.TurnRainbowOn();
	else if( NUMBER_SET_COLOR )
		m_textNumber.SetColor( NUMBER_COLOR );
	this->AddChild( &m_textNumber );

	m_missNumber.LoadFromFont( THEME->GetPathToF("MissCombo") );
	m_missNumber.SetShadowLength( MISS_NUMBER_SHADOW_LENGTH );
	m_missNumber.SetXY( MISS_NUMBER_X, MISS_NUMBER_Y );
	m_missNumber.SetHorizAlign( (Actor::HorizAlign)(int)MISS_NUMBER_HORIZ_ALIGN );
	m_missNumber.SetVertAlign( (Actor::VertAlign)(int)MISS_NUMBER_VERT_ALIGN );
	m_missNumber.SetHidden( true );
	if( MISS_NUMBER_SET_RAINBOW )
		m_missNumber.TurnRainbowOn();
	else if( MISS_NUMBER_SET_COLOR )
		m_missNumber.SetColor( MISS_NUMBER_COLOR );
	this->AddChild( &m_missNumber );
}

void Combo::SetCombo( int iCombo, int iMisses )
{
	if( GAMESTATE->m_bEditing && !PREFSMAN->m_bEditorCombo )
	{
		m_sprComboLabel.SetHidden( true );
		m_sprMissesLabel.SetHidden( true );
		m_textNumber.SetHidden( true );
		m_missNumber.SetHidden( true );
		return;
	}

	bool bMisses = iMisses > 0;
	int iNum = bMisses ? iMisses : iCombo;

	// If we're not showing a counter
	if( bMisses && ( SHOW_MISS_COMBO_AT == -1 || iNum < (int)SHOW_MISS_COMBO_AT ) ||
		!bMisses && ( SHOW_COMBO_AT == -1 || iNum < (int)SHOW_COMBO_AT ) )
	{
		m_sprComboLabel.SetHidden( true );
		m_sprMissesLabel.SetHidden( true );
		m_textNumber.SetHidden( true );
		m_missNumber.SetHidden( true );
		return;
	}

	m_sprComboLabel.SetHidden( bMisses );
	m_textNumber.SetHidden( bMisses );

	m_sprMissesLabel.SetHidden( !bMisses );
	m_missNumber.SetHidden( !bMisses );

	int iDigits = 0;
	CString txt = "";

	int iMaxDigits = bMisses ? MAXIMUM_MISS_COMBO_DIGITS : MAXIMUM_COMBO_DIGITS;
	CLAMP( iMaxDigits,0,9 );

	int iMinDigits = bMisses ? MINIMUM_MISS_COMBO_DIGITS : MINIMUM_COMBO_DIGITS;
	CLAMP( iMinDigits,1,9 );

	// Current number of digits
	if( iNum >= 1000000000 )
		iDigits = 10;
	else if( iNum >= 100000000 )
		iDigits = 9;
	else if( iNum >= 10000000 )
		iDigits = 8;
	else if( iNum >= 1000000 )
		iDigits = 7;
	else if( iNum >= 100000 )
		iDigits = 6;
	else if( iNum >= 10000 )
		iDigits = 5;
	else if( iNum >= 1000 )
		iDigits = 4;
	else if( iNum >= 100 )
		iDigits = 3;
	else if( iNum >= 10 )
		iDigits = 2;
	else
		iDigits = 1;

	// If iMaxDigits is zero, then we're not capping the combo at all
	if( iMaxDigits != 0 && iDigits > iMaxDigits )
		iNum = 9*((10*(iMaxDigits-1))+1);

	int iNumZeros = iMinDigits - iDigits;

	if( iNumZeros <= 0 )
		iNumZeros = 0;

	// Add in the preceding zeros
	for( int i=0; i < iNumZeros; i++ )
		txt += "0";

	// And finally, add in the combo value
	txt += ssprintf("%d", iNum);


	BitmapText& sprNumber = bMisses ? m_missNumber : m_textNumber;

	// Don't do anything if it's not changing.
	if( bMisses && !MISS_SHOW_REPEATED || !bMisses && !SHOW_REPEATED )
		if( sprNumber.GetText() == txt )
			return;

	Sprite &sprLabel = bMisses ? m_sprMissesLabel : m_sprComboLabel;

	float fNumberZoom = bMisses ? SCALE(iNum,0.f,(float)MISS_NUMBER_MAX_ZOOM_AT,(float)MISS_NUMBER_MIN_ZOOM,(float)MISS_NUMBER_MAX_ZOOM) : SCALE(iNum,0.f,(float)NUMBER_MAX_ZOOM_AT,(float)NUMBER_MIN_ZOOM,(float)NUMBER_MAX_ZOOM);
	CLAMP( fNumberZoom, bMisses ? (float)MISS_NUMBER_MIN_ZOOM : (float)NUMBER_MIN_ZOOM, bMisses ? (float)MISS_NUMBER_MAX_ZOOM : (float)NUMBER_MAX_ZOOM );

	sprNumber.SetText( txt );
	sprNumber.StopTweening();
	sprNumber.SetZoom( fNumberZoom * ( bMisses ? (float)MISS_PULSE_ZOOM : (float)PULSE_ZOOM ) );
	sprNumber.BeginTweening( bMisses ? MISS_C_TWEEN_SECONDS : C_TWEEN_SECONDS );
	sprNumber.SetZoom( fNumberZoom );

	sprLabel.StopTweening();
	sprLabel.SetZoom( bMisses ? MISS_PULSE_ZOOM : PULSE_ZOOM );
	sprLabel.BeginTweening( bMisses ? MISS_C_TWEEN_SECONDS : C_TWEEN_SECONDS );
	sprLabel.SetZoom( 1 );

	bool bPastMidpoint = GAMESTATE->GetCourseSongIndex() > 0 ||
		GAMESTATE->m_fMusicSeconds >= (GAMESTATE->m_pCurSong->m_fMusicLengthSeconds * (PERCENT_SONG_PASSED/100));

	if( !bMisses && bPastMidpoint )
	{
		if( g_CurStageStats.FullComboOfScore(m_PlayerNumber,TNS_MARVELOUS) )
		{
			sprLabel.Command( FULL_COMBO_MARVELOUSES_COMMAND_LABEL );
			sprNumber.Command( FULL_COMBO_MARVELOUSES_COMMAND );
		}
		else if( g_CurStageStats.FullComboOfScore(m_PlayerNumber,TNS_PERFECT) )
		{
			sprLabel.Command( FULL_COMBO_PERFECTS_COMMAND_LABEL );
			sprNumber.Command( FULL_COMBO_PERFECTS_COMMAND );
		}
		else if( g_CurStageStats.FullComboOfScore(m_PlayerNumber,TNS_GREAT) )
		{
			sprLabel.Command( FULL_COMBO_GREATS_COMMAND_LABEL );
			sprNumber.Command( FULL_COMBO_GREATS_COMMAND );
		}
		else if( g_CurStageStats.FullComboOfScore(m_PlayerNumber,TNS_GOOD) )
		{
			sprLabel.Command( FULL_COMBO_GOODS_COMMAND_LABEL );
			sprNumber.Command( FULL_COMBO_GOODS_COMMAND );
		}
		else
		{
			sprLabel.Command( FULL_COMBO_BROKEN_COMMAND_LABEL );
			sprNumber.Command( FULL_COMBO_BROKEN_COMMAND );
		}
	}
	else if( !bMisses )
	{
		if( NORMAL_USE_BROKEN_COMMAND )
		{
			sprLabel.Command( FULL_COMBO_BROKEN_COMMAND_LABEL );
			sprNumber.Command( FULL_COMBO_BROKEN_COMMAND );
		}
		else
		{
			sprLabel.Command( FULL_COMBO_NORMAL_COMMAND_LABEL );
			sprNumber.Command( FULL_COMBO_NORMAL_COMMAND );
		}
	}
	else
	{
		sprLabel.Command( MISS_COMMAND_LABEL );
		sprNumber.Command( MISS_COMMAND );
	}
}

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * Contains code (c) 2001-2004 Chris Danford.
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
