#include "global.h"
#include "NotesWriterSM.h"
#include "Steps.h"
#include "RageUtil.h"
#include "RageLog.h"
#include "RageFile.h"
#include "RageFileManager.h"
#include "Song.h"

#include <cstring>
#include <cerrno>
#include <deque>

#include "NoteTypes.h"
#include "PrefsManager.h"
#include "ProductInfo.h"

void NotesWriterSM::WriteGlobalTags( RageFile &f, const Song &out, bool bSavingCache )
{
	f.PutLine( ssprintf( "#TITLE:%s;", out.m_sMainTitle.c_str() ) );
	f.PutLine( ssprintf( "#SUBTITLE:%s;", out.m_sSubTitle.c_str() ) );
	f.PutLine( ssprintf( "#ARTIST:%s;", out.m_sArtist.c_str() ) );
	f.PutLine( ssprintf( "#TITLETRANSLIT:%s;", out.m_sMainTitleTranslit.c_str() ) );
	f.PutLine( ssprintf( "#SUBTITLETRANSLIT:%s;", out.m_sSubTitleTranslit.c_str() ) );
	f.PutLine( ssprintf( "#ARTISTTRANSLIT:%s;", out.m_sArtistTranslit.c_str() ) );

	f.PutLine( ssprintf( "#GENRE:%s;", out.m_sGenre.c_str() ) );

	f.PutLine( ssprintf( "#MENUCOLOR:%s;", out.m_sMenuColor.c_str() ) );

	f.PutLine( ssprintf( "#BANNER:%s;", out.m_sBannerFile.c_str() ) );
	f.PutLine( ssprintf( "#DISC:%s;", out.m_sDiscFile.c_str() ) );
	f.PutLine( ssprintf( "#BACKGROUND:%s;", out.m_sBackgroundFile.c_str() ) );
	f.PutLine( ssprintf( "#PREVIEW:%s;", out.m_sPreviewFile.c_str() ) );
	f.PutLine( ssprintf( "#LYRICSPATH:%s;", out.m_sLyricsFile.c_str() ) );
	f.PutLine( ssprintf( "#CDTITLE:%s;", out.m_sCDTitleFile.c_str() ) );
	f.PutLine( ssprintf( "#INTRO:%s;", out.m_sIntroFile.c_str() ) );
	f.PutLine( ssprintf( "#MUSIC:%s;", out.m_sMusicFile.c_str() ) );
	f.PutLine( ssprintf( "#SAMPLESTART:%.3f;", out.m_fMusicSampleStartSeconds ) );
	f.PutLine( ssprintf( "#SAMPLELENGTH:%.3f;", out.m_fMusicSampleLengthSeconds ) );

	f.PutLine( ssprintf( "#SELECTABLE:%s;", SelectionDisplayToString(out.m_SelectionDisplay).c_str() ) );

	if( out.m_iListSortPosition != -1 )
		f.PutLine( ssprintf( "#LISTSORT:%d;", out.m_iListSortPosition ) );

	// SMA Version.
	f.PutLine( "#SMAVERSION:" + SMA_FILE_VERSION + ";" );

	// Rows per Beat
	f.PutLine( ssprintf( "#ROWSPERBEAT:0=%d;", ROWS_PER_BEAT ) );
	f.PutLine( ssprintf( "#BEATSPERMEASURE:0=%d;", BEATS_PER_MEASURE ) );

	f.PutLine( ssprintf( "#OFFSET:%.3f;", out.m_Timing.m_fBeat0Offset ) );

	unsigned i, j;
	f.Write( "#BPMS" );
	if( ( j = out.m_Timing.m_BPMSegments.size() ) > 1 )
		f.PutLine( ":" );
	else
		f.Write( ":" );
	for( i=0; i<j; i++ )
	{
		const BPMSegment &bs = out.m_Timing.m_BPMSegments[i];

		if( PREFSMAN->m_bTimingAsRows || bSavingCache )
			f.Write( ssprintf( "%dr=%.3f", BeatToNoteRow( bs.m_fBeat ), bs.m_fBPM ) );
		else
			f.Write( ssprintf( "%.3f=%.3f", bs.m_fBeat, bs.m_fBPM ) );

		if( i != j-1 )
			f.PutLine( "," );
	}
	f.PutLine( ";" );

	f.Write( "#STOPS" );
	if( ( j = out.m_Timing.m_StopSegments.size() ) > 1 )
		f.PutLine( ":" );
	else
		f.Write( ":" );
	for( i=0; i<j; i++ )
	{
		const StopSegment &fs = out.m_Timing.m_StopSegments[i];

		if( PREFSMAN->m_bTimingAsRows || bSavingCache )
		{
			if( fs.m_bDelay )
				f.Write( ssprintf( "%dr=%.3f=%i", BeatToNoteRow( fs.m_fBeat ), fs.m_fSeconds, fs.m_bDelay ) );
			else
				f.Write( ssprintf( "%dr=%.3f", BeatToNoteRow( fs.m_fBeat ), fs.m_fSeconds ) );
		}
		else
		{
			if( fs.m_bDelay )
				f.Write( ssprintf( "%.3f=%.3f=%i", fs.m_fBeat, fs.m_fSeconds, fs.m_bDelay ) );
			else
				f.Write( ssprintf( "%.3f=%.3f", fs.m_fBeat, fs.m_fSeconds ) );
		}

		if( i != j-1 )
			f.PutLine( "," );
	}
	f.PutLine( ";" );

	f.Write( "#BGCHANGES:" );
	for( i=0; i<out.m_BGChanges.size(); i++ )
	{
		const BGChange &bg = out.m_BGChanges[i];

		if( PREFSMAN->m_bTimingAsRows || bSavingCache )
			f.PutLine( ssprintf( "%dr=%s=%.3f=%d=%d=%d,", BeatToNoteRow( bg.m_fBeat ), bg.m_sBGName.c_str(), bg.m_fRate, bg.m_bFadeLast, bg.m_bRewindMovie, bg.m_bLoop ) );
		else
			f.PutLine( ssprintf( "%.3f=%s=%.3f=%d=%d=%d,", bg.m_fBeat, bg.m_sBGName.c_str(), bg.m_fRate, bg.m_bFadeLast, bg.m_bRewindMovie, bg.m_bLoop ) );
	}

	/* If there's an animation plan at all, add a dummy "-nosongbg-" tag to indicate that
	 * this file doesn't want a song BG entry added at the end.  See SMLoader::TidyUpData.
	 * This tag will be removed on load.  Add it at a very high beat, so it won't cause
	 * problems if loaded in older versions. */
	if( !out.m_BGChanges.empty() )
		f.PutLine( "99999=-nosongbg-=1.000=0=0=0 // don't automatically add -songbackground-" );
	f.PutLine( ";" );

	if( out.m_FGChanges.size() )
	{
		f.Write( "#FGCHANGES:" );
		for( i=0; i<out.m_FGChanges.size(); i++ )
		{
			const BGChange &fg = out.m_FGChanges[i];

			if( PREFSMAN->m_bTimingAsRows || bSavingCache )
				f.PutLine( ssprintf( "%dr=%s=%.3f=%d=%d=%d,", BeatToNoteRow( fg.m_fBeat ), fg.m_sBGName.c_str(), fg.m_fRate, fg.m_bFadeLast, fg.m_bRewindMovie, fg.m_bLoop ) );
			else
				f.PutLine( ssprintf( "%.3f=%s=%.3f=%d=%d=%d,", fg.m_fBeat, fg.m_sBGName.c_str(), fg.m_fRate, fg.m_bFadeLast, fg.m_bRewindMovie, fg.m_bLoop ) );

			if( i != out.m_FGChanges.size()-1 )
				f.Write( "," );
		}
		f.PutLine( ";" );
	}
}

static void WriteLineList( RageFile &f, vector<CString> &lines, bool SkipLeadingBlankLines, bool OmitLastNewline )
{
	for( unsigned i = 0; i < lines.size(); ++i )
	{
		TrimRight( lines[i] );
		if( SkipLeadingBlankLines )
		{
			if( lines.size() == 0 )
				continue;
			SkipLeadingBlankLines = false;
		}
		f.Write( lines[i] );

		if( !OmitLastNewline || i+1 < lines.size() )
			f.PutLine( "" ); /* newline */
	}
}

void NotesWriterSM::WriteSMNotesTag( const Steps &in, RageFile &f, bool bSavingCache )
{
	f.PutLine( "" );
	f.PutLine( ssprintf( "//---------------%s - %s----------------",
		StepsTypeToString(in.m_StepsType).c_str(), in.GetDescription().c_str() ) );

	f.PutLine( ssprintf( "#CREDIT:%s;", in.m_sCredit.c_str() ) );

	f.PutLine( ssprintf( "#ROWSPERBEAT:0=%d;", ROWS_PER_BEAT ) );
	f.PutLine( ssprintf( "#BEATSPERMEASURE:0=%d;", BEATS_PER_MEASURE ) );

	f.PutLine( ssprintf( "#OFFSET:%.3f;", in.m_Timing.m_fBeat0Offset ) );

	unsigned i, j;
	f.Write( "#BPMS" );
	if( ( j = in.m_Timing.m_BPMSegments.size() ) > 1 )
		f.PutLine( ":" );
	else
		f.Write( ":" );
	for( i=0; i<j; i++ )
	{
		const BPMSegment &bs = in.m_Timing.m_BPMSegments[i];

		if( PREFSMAN->m_bTimingAsRows || bSavingCache )
			f.Write( ssprintf( "%dr=%.3f", BeatToNoteRow( bs.m_fBeat ), bs.m_fBPM ) );
		else
			f.Write( ssprintf( "%.3f=%.3f", bs.m_fBeat, bs.m_fBPM ) );

		if( i != j-1 )
			f.PutLine( "," );
	}
	f.PutLine( ";" );

	f.Write( "#STOPS" );
	int iStops = 0, iStopsWritten = 0;
	for( i=0, j=in.m_Timing.m_StopSegments.size(); i<j; i++ )
		if( !in.m_Timing.m_StopSegments[i].m_bDelay )
			iStops++;
	if( iStops > 1 )
		f.PutLine( ":" );
	else
		f.Write( ":" );
	for( i=0; i<j; i++ )
	{
		const StopSegment &fs = in.m_Timing.m_StopSegments[i];

		if( fs.m_bDelay )
			continue;

		if( PREFSMAN->m_bTimingAsRows || bSavingCache )
			f.Write( ssprintf( "%dr=%.3f", BeatToNoteRow( fs.m_fBeat ), fs.m_fSeconds ) );
		else
			f.Write( ssprintf( "%.3f=%.3f", fs.m_fBeat, fs.m_fSeconds ) );

		iStopsWritten++;

		if( iStopsWritten < iStops )
			f.PutLine( "," );
	}
	f.PutLine( ";" );

	f.Write( "#DELAYS" );
	int iDelays = 0, iDelaysWritten = 0;
	for( i=0; i<j; i++ )
		if( in.m_Timing.m_StopSegments[i].m_bDelay )
			iDelays++;
	if( iDelays > 1 )
		f.PutLine( ":" );
	else
		f.Write( ":" );
	for( i=0; i<j; i++ )
	{
		const StopSegment &fs = in.m_Timing.m_StopSegments[i];

		if( !fs.m_bDelay )
			continue;

		if( PREFSMAN->m_bTimingAsRows || bSavingCache )
			f.Write( ssprintf( "%dr=%.3f", BeatToNoteRow( fs.m_fBeat ), fs.m_fSeconds ) );
		else
			f.Write( ssprintf( "%.3f=%.3f", fs.m_fBeat, fs.m_fSeconds ) );

		iDelaysWritten++;

		if( iDelaysWritten < iDelays )
			f.PutLine( "," );
	}
	f.PutLine( ";" );

	f.Write( "#TICKCOUNT" );
	if( ( j = in.m_Timing.m_TickcountSegments.size() ) > 1 )
		f.PutLine( ":" );
	else
		f.Write( ":" );
	for( i=0; i<j; i++ )
	{
		const TickcountSegment &ts = in.m_Timing.m_TickcountSegments[i];

		if( PREFSMAN->m_bTimingAsRows || bSavingCache )
			f.Write( ssprintf( "%dr=%u", BeatToNoteRow( ts.m_fBeat ), ts.m_uTickcount ) );
		else
			f.Write( ssprintf( "%.3f=%u", ts.m_fBeat, ts.m_uTickcount ) );

		if( i != j-1 )
			f.PutLine( "," );
	}
	f.PutLine( ";" );

	f.Write( "#SPEED" );
	if( ( j = in.m_Timing.m_SpeedSegments.size() ) > 1 )
		f.PutLine( ":" );
	else
		f.Write( ":" );
	for( i=0; i<j; i++ )
	{
		const SpeedSegment &ss = in.m_Timing.m_SpeedSegments[i];

		if( PREFSMAN->m_bTimingAsRows || bSavingCache )
		{
			if( ss.m_fBeats == 4.f && ss.m_fFactor == 1.f )
				f.Write( ssprintf( "%dr=%.3f", BeatToNoteRow( ss.m_fBeat ), ss.m_fToSpeed ) );
			else
			{
				if( ss.m_fBeats < 0 )
					f.Write( ssprintf( "%dr=%.3f=%.3fs", BeatToNoteRow( ss.m_fBeat ), ss.m_fToSpeed, ss.m_fBeats*-1 ) );
				else
					f.Write( ssprintf( "%dr=%.3f=%.3f", BeatToNoteRow( ss.m_fBeat ), ss.m_fToSpeed, ss.m_fBeats ) );

				if( ss.m_fFactor != 1.f )
					f.Write( ssprintf( "=%.3f", ss.m_fFactor ) );
			}
		}
		else
		{
			if( ss.m_fBeats == 4.f && ss.m_fFactor == 1.f )
				f.Write( ssprintf( "%.3f=%.3f", ss.m_fBeat, ss.m_fToSpeed ) );
			else
			{
				if( ss.m_fBeats < 0 )
					f.Write( ssprintf( "%.3f=%.3f=%.3fs", ss.m_fBeat, ss.m_fToSpeed, ss.m_fBeats*-1 ) );
				else
					f.Write( ssprintf( "%.3f=%.3f=%.3f", ss.m_fBeat, ss.m_fToSpeed, ss.m_fBeats ) );

				if( ss.m_fFactor != 1.f )
					f.Write( ssprintf( "=%.3f", ss.m_fFactor ) );
			}
		}

		if( i != j-1 )
			f.PutLine( "," );
	}
	f.PutLine( ";" );

	f.Write( "#MULTIPLIER" );
	if( ( j = in.m_Timing.m_MultiplierSegments.size() ) > 1 )
		f.PutLine( ":" );
	else
		f.Write( ":" );
	for( i=0; i<j; i++ )
	{
		const MultiplierSegment &ms = in.m_Timing.m_MultiplierSegments[i];

		if( PREFSMAN->m_bTimingAsRows || bSavingCache )
		{
			if( ms.m_fLifeMiss != ms.m_fScoreMiss )
			{
				if( ms.m_fLifeHit == ms.m_fLifeMiss && ms.m_fScoreHit == ms.m_fScoreMiss )
					f.Write( ssprintf( "%dr=%u=%u=%.3f=%.3f", BeatToNoteRow( ms.m_fBeat ), ms.m_uHit, ms.m_uMiss, ms.m_fLifeHit, ms.m_fScoreHit ) );
				else
					f.Write( ssprintf( "%dr=%u=%u=%.3f=%.3f=%.3f=%.3f", BeatToNoteRow( ms.m_fBeat ), ms.m_uHit, ms.m_uMiss, ms.m_fLifeHit, ms.m_fScoreHit, ms.m_fLifeMiss, ms.m_fScoreMiss ) );
			}
			else if( ms.m_fLifeHit != ms.m_fLifeMiss )
				f.Write( ssprintf( "%dr=%u=%u=%.3f=%.3f=%.3f", BeatToNoteRow( ms.m_fBeat ), ms.m_uHit, ms.m_uMiss, ms.m_fLifeHit, ms.m_fScoreHit, ms.m_fLifeMiss ) );
			else if( ms.m_fLifeHit != ms.m_fScoreHit )
				f.Write( ssprintf( "%dr=%u=%u=%.3f=%.3f", BeatToNoteRow( ms.m_fBeat ), ms.m_uHit, ms.m_uMiss, ms.m_fLifeHit, ms.m_fScoreHit ) );
			else if( (float)ms.m_uHit != ms.m_fLifeHit || ms.m_uHit != ms.m_uMiss )
			{
				if( ms.m_uHit != ms.m_uMiss )
					f.Write( ssprintf( "%dr=%u=%u=%.3f", BeatToNoteRow( ms.m_fBeat ), ms.m_uHit, ms.m_uMiss, ms.m_fLifeHit ) );
				else
					f.Write( ssprintf( "%dr=%u=%.3f", BeatToNoteRow( ms.m_fBeat ), ms.m_uHit, ms.m_fLifeHit ) );
			}
			else //if( (float)ms.m_uHit != DEFAULT_MULTIPLIER_F )
				f.Write( ssprintf( "%dr=%u", BeatToNoteRow( ms.m_fBeat ), ms.m_uHit ) );
			//else
			//	f.Write( ssprintf( "%dr", BeatToNoteRow( ms.m_fBeat ) ) );
		}
		else
		{
			if( ms.m_fLifeMiss != ms.m_fScoreMiss )
			{
				if( ms.m_fLifeHit == ms.m_fLifeMiss && ms.m_fScoreHit == ms.m_fScoreMiss )
					f.Write( ssprintf( "%.3f=%u=%u=%.3f=%.3f", ms.m_fBeat, ms.m_uHit, ms.m_uMiss, ms.m_fLifeHit, ms.m_fScoreHit ) );
				else
					f.Write( ssprintf( "%.3f=%u=%u=%.3f=%.3f=%.3f=%.3f", ms.m_fBeat, ms.m_uHit, ms.m_uMiss, ms.m_fLifeHit, ms.m_fScoreHit, ms.m_fLifeMiss, ms.m_fScoreMiss ) );
			}
			else if( ms.m_fLifeHit != ms.m_fLifeMiss )
				f.Write( ssprintf( "%.3f=%u=%u=%.3f=%.3f=%.3f", ms.m_fBeat, ms.m_uHit, ms.m_uMiss, ms.m_fLifeHit, ms.m_fScoreHit, ms.m_fLifeMiss ) );
			else if( ms.m_fLifeHit != ms.m_fScoreHit )
				f.Write( ssprintf( "%.3f=%u=%u=%.3f=%.3f", ms.m_fBeat, ms.m_uHit, ms.m_uMiss, ms.m_fLifeHit, ms.m_fScoreHit ) );
			else if( (float)ms.m_uHit != ms.m_fLifeHit || ms.m_uHit != ms.m_uMiss )
			{
				if( ms.m_uHit != ms.m_uMiss )
					f.Write( ssprintf( "%.3f=%u=%u=%.3f", ms.m_fBeat, ms.m_uHit, ms.m_uMiss, ms.m_fLifeHit ) );
				else
					f.Write( ssprintf( "%.3f=%u=%.3f", ms.m_fBeat, ms.m_uHit, ms.m_fLifeHit ) );
			}
			else //if( (float)ms.m_uHit != DEFAULT_MULTIPLIER_F )
				f.Write( ssprintf( "%.3f=%u", ms.m_fBeat, ms.m_uHit ) );
			//else
			//	f.Write( ssprintf( "%.3f", ms.m_fBeat ) );
		}

		if( i != j-1 )
			f.PutLine( "," );
	}
	f.PutLine( ";" );

	f.Write( "#FAKES" );
	if( ( j = in.m_Timing.m_FakeAreaSegments.size() ) > 1 )
		f.PutLine( ":" );
	else
		f.Write( ":" );
	for( i=0; i<j; i++ )
	{
		const AreaSegment &fs = in.m_Timing.m_FakeAreaSegments[i];

		if( PREFSMAN->m_bTimingAsRows || bSavingCache )
			f.Write( ssprintf( "%dr=%.3f", BeatToNoteRow( fs.m_fBeat ), fs.m_fBeats ) );
		else
			f.Write( ssprintf( "%.3f=%.3f", fs.m_fBeat, fs.m_fBeats ) );

		if( i != j-1 )
			f.PutLine( "," );
	}
	f.PutLine( ";" );

	f.Write( "#SPDAREAS" );
	if( ( j = in.m_Timing.m_SpeedAreaSegments.size() ) > 1 )
		f.PutLine( ":" );
	else
		f.Write( ":" );
	for( i=0; i<j; i++ )
	{
		const SpeedAreaSegment &sa = in.m_Timing.m_SpeedAreaSegments[i];

		int iSpeedAreaElements = 2;

		if( sa.m_fToSpeed != sa.m_fFromSpeed )
			iSpeedAreaElements = 3;
		else if( sa.m_fFactor != 1.f )
			iSpeedAreaElements = 6;
		else if( sa.m_fTriggerOffset != 0.f )
			iSpeedAreaElements = 5;
		else if( sa.m_fBeats != fBEATS_PER_MEASURE )
			iSpeedAreaElements = 4;
		
		deque<CString> dsSpeedArea;

		switch( iSpeedAreaElements )
		{
		default:
		case 6:
			dsSpeedArea.push_front( ssprintf( "%.3f", sa.m_fFactor ) );

		case 5:
			dsSpeedArea.push_front( ssprintf( "%.3f", sa.m_fTriggerOffset ) );

		case 4:
			dsSpeedArea.push_front( ssprintf( "%.3f", sa.m_fBeats ) );

		case 3:
			dsSpeedArea.push_front( ssprintf( "%.3f", sa.m_fToSpeed ) );

		case 2:
			dsSpeedArea.push_front( ssprintf( "%.3f", sa.m_fFromSpeed ) );

		case 1:
			if( PREFSMAN->m_bTimingAsRows || bSavingCache )
				dsSpeedArea.push_front( ssprintf( "%dr", BeatToNoteRow(sa.m_fBeat) ) );
			else
				dsSpeedArea.push_front( ssprintf( "%.3f", sa.m_fBeat ) );
			break;

		case 0:
			ASSERT(0);
		}

		for( unsigned s = 0; ; )
		{
			f.Write( dsSpeedArea[s] );

			if( ++s < dsSpeedArea.size() )
				f.Write( "=" );
			else
				break;
		}

		if( i != j-1 )
			f.PutLine( "," );
	}
	f.PutLine( ";" );

	f.PutLine( ssprintf( "#DISPLAYBPM:%s%s;", in.m_BPMRange.GetString().c_str(), in.m_DisplayBPM == BPM_REAL ? "*" : "" ) );

	f.Write( "#ATTACKS" );
	if( ( j = in.m_Attacks.size() ) > 1 )
		f.PutLine( ":" );
	else
		f.Write( ":" );
	for( i=0; i < j; i++ )
	{
		const Attack& attack = in.m_Attacks[i];
		f.Write( ssprintf("TIME=%.3f: ", attack.fStartSecond) );
		f.Write( ssprintf("LEN=%.3f: ", attack.fSecsRemaining) );
		f.Write( ssprintf("MODS=%s", attack.sModifier.c_str()) );

		if( i != j-1 )
			f.PutLine( ":" );
	}
	f.PutLine( ";" );

	f.PutLine( "#NOTES:" );
	f.PutLine( ssprintf( "     %s:", StepsTypeToString(in.m_StepsType).c_str() ) );
	f.PutLine( ssprintf( "     %s:", in.GetDescription().c_str() ) );
	f.PutLine( ssprintf( "     %s:", DifficultyToString(in.GetDifficulty()).c_str() ) );
	f.PutLine( ssprintf( "     %d%s:", in.GetMeter(), in.m_bHiddenDifficulty ? "*" : "" ) );

	int MaxRadar = bSavingCache? NUM_RADAR_CATEGORIES:5;
	CStringArray asRadarValues;
	for( int r=0; r < MaxRadar; r++ )
		asRadarValues.push_back( ssprintf("%.3f", in.GetRadarValues()[r]) );
	/* Don't append a newline here; it's added in NoteDataUtil::GetSMNoteDataString.
	 * If we add it here, then every time we write unmodified data we'll add an extra
	 * newline and they'll accumulate. */
	f.Write( ssprintf( "     %s:", join(",",asRadarValues).c_str() ) );

	CString sNoteData;
	in.GetSMNoteData( sNoteData );

	vector<CString> lines;
	split( sNoteData, "\n", lines, false );
	WriteLineList( f, lines, true, true );
	f.PutLine( ";" );
}

bool NotesWriterSM::Write(CString sPath, const Song &out, bool bSavingCache )
{
	/* Flush dir cache when writing steps, so the old size isn't cached. */
	FILEMAN->FlushDirCache( Dirname(sPath) );

	unsigned i;

	int flags = RageFile::WRITE;

	/* If we're not saving cache, we're saving real data, so enable SLOW_FLUSH
	 * to prevent data loss.  If we're saving cache, this will slow things down
	 * too much. */
	if( !bSavingCache )
		flags |= RageFile::SLOW_FLUSH;

	RageFile f;
	if( !f.Open( sPath, flags ) )
	{
		LOG->Warn( "Error opening song file '%s' for writing: %s", sPath.c_str(), f.GetError().c_str() );
		return false;
	}

	WriteGlobalTags( f, out, bSavingCache );
	if( bSavingCache )
	{
		f.PutLine( ssprintf( "// cache tags:" ) );
		f.PutLine( ssprintf( "#FIRSTBEAT:%.3f;", out.m_Timing.m_fFirstBeat ) );
		f.PutLine( ssprintf( "#LASTBEAT:%.3f;", out.m_Timing.m_fLastBeat ) );
		f.PutLine( ssprintf( "#SONGFILENAME:%s;", out.m_sSongFileName.c_str() ) );
		f.PutLine( ssprintf( "#HASINTRO:%i;", out.m_bHasIntro ) );
		f.PutLine( ssprintf( "#HASMUSIC:%i;", out.m_bHasMusic ) );
		f.PutLine( ssprintf( "#HASBANNER:%i;", out.m_bHasBanner ) );
		f.PutLine( ssprintf( "#HASDISC:%i;", out.m_bHasDisc ) );
		f.PutLine( ssprintf( "#HASBACKGROUND:%i;", out.m_bHasBackground ) );
		f.PutLine( ssprintf( "#HASPREVIEW:%i;", out.m_bHasPreview ) );
		f.PutLine( ssprintf( "#MUSICLENGTH:%.3f;", out.m_fMusicLengthSeconds ) );
		f.PutLine( ssprintf( "// end cache tags" ) );
	}

	//
	// Save all Steps for this file
	//
	const vector<Steps*>& vpSteps = out.GetAllSteps();
	for( i=0; i<vpSteps.size(); i++ )
	{
		Steps* pSteps = vpSteps[i];
		if( pSteps->IsAutogen() )
			continue; /* don't write autogen notes */

		/* Only save steps that weren't loaded from a profile. */
		if( pSteps->WasLoadedFromProfile() )
			continue;

		WriteSMNotesTag( *pSteps, f, bSavingCache );
	}

	return true;
}

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * Contains code (c) 2001-2004 Chris Danford, Glenn Maynard.
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
