#include "global.h"
#include "ScoreKeeperPump.h"
#include "RageUtil.h"
#include "GameState.h"
#include "PrefsManager.h"
#include "ThemeManager.h"
#include "StageStats.h"
#include "ScreenManager.h"
#include "ScreenGameplay.h"
#include "ProfileManager.h"
#include "NetworkSyncManager.h"
#include "Steps.h"
#include "Style.h"

static	CachedThemeMetricI	SHOW_COMBO_AT		("Combo","ShowComboAt");
static	CachedThemeMetric	TOASTY_TRIGGERS		("ScreenGameplay","ToastyTriggers");

ScoreKeeperPump::ScoreKeeperPump(PlayerNumber pn) : ScoreKeeper(pn)
{
	SHOW_COMBO_AT.Refresh();
	TOASTY_TRIGGERS.Refresh();
}

void ScoreKeeperPump::OnNextSong( int iSongInCourseIndex, const Steps* pSteps, const NoteData* pNoteData )
{
	m_iToastyCombo = 0;
	m_iComboScoring = g_CurStageStats.iCurCombo[m_PlayerNumber];

	switch( PREFSMAN->m_iScoringType )
	{
	case PrefsManager::SCORING_PIU_NX2:
	case PrefsManager::SCORING_PIU_ZERO:

		// Nightmare Bonus!
		if( GAMESTATE->GetCurrentStyle()->m_StyleType == Style::ONE_PLAYER_TWO_CREDITS &&
			pSteps->GetDifficulty() >= DIFFICULTY_HARD &&
			pSteps->GetDifficulty() < DIFFICULTY_EDIT
		)
			g_CurStageStats.iScore[m_PlayerNumber] += 500000;
	}

	// Load Toasty Triggers
	CStringArray sTriggers;
	split( TOASTY_TRIGGERS, ",", sTriggers, true );

	m_iToastyTrigger.clear();
	for( unsigned i = 0; i < sTriggers.size(); i++ )
		m_iToastyTrigger.push_back( atoi(sTriggers[i]) );

	sort( m_iToastyTrigger.begin(), m_iToastyTrigger.end() );
}

void ScoreKeeperPump::AddScore( TapNoteScore score, const float fScoreMultiplier )
{
	// No score with AutoPlay!
	if( !GAMESTATE->m_bDemonstrationOrJukebox && GAMESTATE->m_PlayerController[m_PlayerNumber] != PC_HUMAN )
		return;

	// Did fail and not using the IIDX fail mode
	if( g_CurStageStats.bFailedEarlier[m_PlayerNumber] &&
		GAMESTATE->m_SongOptions.m_FailType != SongOptions::FAIL_IIDX )
		return;

	// Multiplier 0 will return score 0...
	if( fScoreMultiplier == 0 )
	{
		//printf( "score: %i\n", g_CurStageStats.iScore[m_PlayerNumber] );
		return;
	}

	if( !GAMESTATE->ShowMarvelous(m_PlayerNumber) &&
		( score == TNS_MARVELOUS || score == TNS_HIDDEN )
	)
		score = TNS_PERFECT;

	//  Check for score!
	float fScore = fScoreMultiplier;
	float fBonusAt = (float)SHOW_COMBO_AT;

	switch( PREFSMAN->m_iScoringType )
	{
	case PrefsManager::SCORING_PIU_NX2:
	case PrefsManager::SCORING_PIU_ZERO:
	{
		fBonusAt = max( fBonusAt, 50 );
		switch( score )
		{
		case TNS_MARVELOUS:			fScore *= 1250;	break;
		case TNS_CHECKPOINT:
		case TNS_PERFECT:			fScore *= 1000;	break;
		case TNS_GREAT:				fScore *= 500;	break;
		case TNS_GOOD:				fScore *= 100;	break;
		case TNS_BOO:				fScore *= -200;	break;
		case TNS_MISS_CHECKPOINT:	fScore *= -300;	break;
		case TNS_MISS:				fScore *= -500;	break;
		default:					fScore *= 0;	break;
		}
		break;
	}
	case PrefsManager::SCORING_PIU_PREX3:
	case PrefsManager::SCORING_PIU_EXTRA:
	{
		fBonusAt = max( fBonusAt, 4 );
		switch( score )
		{
		case TNS_MARVELOUS:			fScore *= 1250;	break;
		case TNS_CHECKPOINT:
		case TNS_PERFECT:			fScore *= 1000;	break;
		case TNS_GREAT:				fScore *= +500;	break;
		default:					fScore *= 0;	break;
		}
		break;
	}
	default:
		ASSERT(0);
	}

	// Check for combo bonus!
	float fCurrentCombo = (float)m_iComboScoring;

	if( ( fCurrentCombo + fScoreMultiplier ) >= fBonusAt )
	{
		float fBonus = 0;

		switch( score )
		{
		case TNS_MARVELOUS:
		case TNS_CHECKPOINT:
		case TNS_PERFECT:
		case TNS_GREAT:
			fBonus = 1000;
			break;
		case TNS_GOOD:
			if( PREFSMAN->m_iScoringType == PrefsManager::SCORING_PIU_EXTRA )
				fBonus = 1000;
		}

		if( fBonus > 0 )
		{
			if( fScoreMultiplier > 1 )
			{
				float fMultipliedBonus = 0;

				if( PREFSMAN->m_iScoringType == PrefsManager::SCORING_PIU_NX2 )
				{
					float fScoreMultiplierRemain = fScoreMultiplier;

					float fNextBonus = fBonusAt - fmodf( fCurrentCombo, fBonusAt );
					if( fNextBonus < fScoreMultiplierRemain && fNextBonus > 0 )
					{
						fMultipliedBonus = fNextBonus * fBonus * ( fCurrentCombo / fBonusAt );
						fScoreMultiplierRemain -= fNextBonus;
						fCurrentCombo += fNextBonus;
					}

					if( fScoreMultiplierRemain >= fBonusAt )
					{
						float fBonusRemain = fScoreMultiplierRemain / fBonusAt;
						fScoreMultiplierRemain = fmodf( fScoreMultiplierRemain, fBonusAt );

						while( fBonusRemain > 0 )
						{
							fMultipliedBonus += fBonusAt * fBonus * ( fCurrentCombo / fBonusAt );
							fCurrentCombo += fBonusAt;
							--fBonusRemain;
						}
					}

					fMultipliedBonus += fScoreMultiplierRemain * fBonus * ( fCurrentCombo / fBonusAt );
				}
				else
				{
					if( fCurrentCombo < fBonusAt )
						fMultipliedBonus = ( fCurrentCombo + fScoreMultiplier - fBonusAt ) * fBonus;
					else
						fMultipliedBonus = fScoreMultiplier * fBonus;
				}

				fBonus = fMultipliedBonus;
			}

			fScore += fBonus;
		}
	}

	// Check for score multiplier!
	switch( PREFSMAN->m_iScoringType )
	{
	case PrefsManager::SCORING_PIU_NX2:
	case PrefsManager::SCORING_PIU_ZERO:
		fScore = fScore * m_fScoreMultiplier;
	}

	g_CurStageStats.iScore[m_PlayerNumber] += int( fScore + 0.5f );

	if( g_CurStageStats.iScore[m_PlayerNumber] < 0 )
		g_CurStageStats.iScore[m_PlayerNumber] = 0;

	//printf( "score: %i\n", g_CurStageStats.iScore[m_PlayerNumber] );
}

void ScoreKeeperPump::HalfScore()
{
	g_CurStageStats.iScore[m_PlayerNumber] /= 2;
	m_iToastyCombo /= 2;
	m_iComboScoring /= 2;
}

void ScoreKeeperPump::HandleTapScore( TapNoteScore score, const unsigned uComboMultiplier, const float fScoreMultiplier )
{
	// No combo with AutoPlay!
	if( !GAMESTATE->m_bDemonstrationOrJukebox && !PREFSMAN->m_bAutoPlayCombo && GAMESTATE->m_PlayerController[m_PlayerNumber] != PC_HUMAN )
	{
		g_CurStageStats.iCurCombo[m_PlayerNumber] = 0;
		m_iComboScoring = 0;
		m_iToastyCombo = 0;
		return;
	}

	// HACK: Oni percents gets messed with checkpoints due to hold heads being counted in radar values.
	if( score <= TNS_MARVELOUS && score >= TNS_MISS )
		return;

	g_CurStageStats.iActualDancePoints[m_PlayerNumber] += TapNoteScoreToDancePoints( score, false ) * uComboMultiplier;
	g_CurStageStats.iPossibleDancePoints[m_PlayerNumber] += TapNoteScoreToDancePoints( score, true ) * uComboMultiplier;

	g_CurStageStats.iTapNoteScores[m_PlayerNumber][score] += uComboMultiplier;

	if( score == TNS_HIDDEN || score == TNS_CHECKPOINT )
	{
		if( PREFSMAN->m_bComboIncreasesOnHiddens || score == TNS_CHECKPOINT )
		{
			AddScore( score, fScoreMultiplier );

			g_CurStageStats.iCurCombo[m_PlayerNumber] += uComboMultiplier;
			m_iComboScoring += uComboMultiplier;

			m_iToastyCombo++;
			CheckToasty();
		}
	}
}

void ScoreKeeperPump::HandleTapRowScore( TapNoteScore scoreOfLastTap, int iNumNotesInRow, int iNumCheckpointsInRow, const unsigned uComboMultiplier, const float fScoreMultiplier )
{
	// No combo with AutoPlay!
	if( !GAMESTATE->m_bDemonstrationOrJukebox && !PREFSMAN->m_bAutoPlayCombo && GAMESTATE->m_PlayerController[m_PlayerNumber] != PC_HUMAN )
	{
		g_CurStageStats.iCurCombo[m_PlayerNumber] = 0;
		m_iComboScoring = 0;
		m_iToastyCombo = 0;
		return;
	}

	int iNumTapsInRow = iNumNotesInRow + iNumCheckpointsInRow;
	ASSERT( iNumTapsInRow >= 1 );

	if( iNumTapsInRow < 3 )
		m_fScoreMultiplier = 1.f;
	else if( iNumTapsInRow == 3 )
		m_fScoreMultiplier = 1.5f;
	else
		m_fScoreMultiplier = 2.f;

	// Update dance points.
	g_CurStageStats.iActualDancePoints[m_PlayerNumber] += TapNoteScoreToDancePoints( scoreOfLastTap, false ) * uComboMultiplier;
	g_CurStageStats.iPossibleDancePoints[m_PlayerNumber] += TapNoteScoreToDancePoints( scoreOfLastTap, true ) * uComboMultiplier;

	// update judged row totals
	g_CurStageStats.iTapNoteScores[m_PlayerNumber][scoreOfLastTap] += uComboMultiplier;

	// only score once per row
	AddScore( scoreOfLastTap, fScoreMultiplier );

	switch( scoreOfLastTap )
	{
	case TNS_GOOD:
		if( PREFSMAN->m_iScoringType != PrefsManager::SCORING_PIU_EXTRA )
			break;
	case TNS_MISS_CHECKPOINT:
	case TNS_MISS:
	case TNS_BOO:
		m_iToastyCombo = 0;
		g_CurStageStats.iCurCombo[m_PlayerNumber] = 0;
		m_iComboScoring = 0;
	case TNS_HIDDEN:
		if( !PREFSMAN->m_bComboIncreasesOnHiddens )
		{
			m_iToastyCombo += iNumNotesInRow;
			break;
		}
	case TNS_PERFECT:
	case TNS_MARVELOUS:
		m_iToastyCombo += iNumNotesInRow;
	case TNS_GREAT:
	case TNS_CHECKPOINT:
		g_CurStageStats.iCurCombo[m_PlayerNumber] += uComboMultiplier;
		m_iComboScoring += uComboMultiplier;
	}

	CheckToasty( iNumNotesInRow );

	NSMAN->ReportScore(m_PlayerNumber, scoreOfLastTap, g_CurStageStats.iScore[m_PlayerNumber],
		g_CurStageStats.iCurCombo[m_PlayerNumber]);
}

void ScoreKeeperPump::HandleHoldScore( HoldNoteScore holdScore, const unsigned uComboMultiplier, const float fScoreMultiplier )
{
	// No combo with AutoPlay!
	if( !GAMESTATE->m_bDemonstrationOrJukebox && !PREFSMAN->m_bAutoPlayCombo && GAMESTATE->m_PlayerController[m_PlayerNumber] != PC_HUMAN )
	{
		g_CurStageStats.iCurCombo[m_PlayerNumber] = 0;
		m_iComboScoring = 0;
		m_iToastyCombo = 0;
		return;
	}

	g_CurStageStats.iHoldNoteScores[m_PlayerNumber][holdScore]++;
}

int ScoreKeeperPump::TapNoteScoreToDancePoints( TapNoteScore tns, bool bPossible )
{
	// This is used for Oni percentage displays.  Grading values are currently in StageStats::GetGrade.
	if( bPossible )
	{
		switch( tns )
		{
		// items / hidden
		case TNS_HIT_MINE:
		case TNS_HIT_SHOCK:
		case TNS_HIT_POTION:
		case TNS_MISS_HIDDEN:
		case TNS_HIDDEN:
			return max(PREFSMAN->m_iPercentScoreWeight[tns], 0);

		// checkpoints
		case TNS_MISS_CHECKPOINT:
		case TNS_CHECKPOINT:
			return PREFSMAN->m_iPercentScoreWeight[TNS_CHECKPOINT];

		// tapnotes
		case TNS_MISS:
		case TNS_BOO:
		case TNS_GOOD:
		case TNS_GREAT:
		case TNS_PERFECT:
		case TNS_MARVELOUS:
			return PREFSMAN->m_iPercentScoreWeight[GAMESTATE->ShowMarvelous(m_PlayerNumber) ? TNS_MARVELOUS : TNS_PERFECT];

		default:
			FAIL_M( ssprintf("Unexpected TNS: %s", TapNoteScoreToString(tns).c_str()) );
		}
	}
	else if( tns == TNS_MARVELOUS && !GAMESTATE->ShowMarvelous(m_PlayerNumber) )
		tns = TNS_PERFECT;

	return PREFSMAN->m_iPercentScoreWeight[tns];
}

void ScoreKeeperPump::CheckToasty( int iNumNotesInRow )
{
	if( GAMESTATE->m_bDemonstrationOrJukebox )
		return;

	// No toasty if used AutoPlay =(
	if( GAMESTATE->UsedAutoPlay( m_PlayerNumber ) )
		return;

	bool bLoadedToasty = false;

	for( int i=m_iToastyTrigger.size()-1; i >= 0; i-- )
	{
		if( m_iToastyCombo >= m_iToastyTrigger[i] && m_iToastyCombo - iNumNotesInRow < m_iToastyTrigger[i] )
		{
			m_iToastyTrigger.erase( m_iToastyTrigger.begin()+i, m_iToastyTrigger.begin()+i+1 );

			if( !bLoadedToasty )
			{
				SCREENMAN->PostMessageToTopScreen( SM_PlayToasty, 0 );
				bLoadedToasty = true;
			}

			PROFILEMAN->IncrementToastiesCount( m_PlayerNumber );
		}
	}
}

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * Contains code (c) 2001-2004 Chris Danford.
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
