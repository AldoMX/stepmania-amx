#include "global.h"
#include "ScreenSelectMusic.h"
#include "ScreenManager.h"
#include "PrefsManager.h"
#include "SongManager.h"
#include "GameManager.h"
#include "GameSoundManager.h"
#include "GameConstantsAndTypes.h"
#include "PrefsManager.h"
#include "RageLog.h"
#include "InputMapper.h"
#include "GameState.h"
#include "CodeDetector.h"
#include "ThemeManager.h"
#include "Steps.h"
#include "ActorUtil.h"
#include "RageDisplay.h"
#include "RageTextureManager.h"
#include "Course.h"
#include "ProfileManager.h"
#include "MenuTimer.h"
#include "LightsManager.h"
#include "StageStats.h"
#include "StepsUtil.h"
#include "Foreach.h"
#include "Style.h"
#include "BannerCache.h"
#include "BackgroundCache.h"
#include "LuaHelpers.h"

// This is under the [Common] group, not [ScreenSelectMusic]
#define AUTO_SET_STYLE				THEME->GetMetricB("Common","AutoSetStyle")

#define	MOVING_COMMAND( mc )		m_s##mc##MovingCommand = THEME->GetMetric(m_sName,#mc"MovingCommand")
#define	MOVING_COMMAND_P( mc, p )	m_s##mc##MovingCommand[p] = THEME->GetMetric(m_sName,ssprintf("%sP%iMovingCommand",#mc,p+1))

#define	SETTLED_COMMAND( mc )		m_s##mc##SettledCommand = THEME->GetMetric(m_sName,#mc"SettledCommand")
#define	SETTLED_COMMAND_P( mc, p )	m_s##mc##SettledCommand[p] = THEME->GetMetric(m_sName,ssprintf("%sP%iSettledCommand",#mc,p+1))

#define	SECTION_COMMAND( mc )		m_s##mc##SectionCommand = THEME->GetMetric(m_sName,#mc"SectionCommand")
#define	SECTION_COMMAND_P( mc, p )	m_s##mc##SectionCommand[p] = THEME->GetMetric(m_sName,ssprintf("%sP%iSectionCommand",#mc,p+1))

static const ScreenMessage	SM_AllowOptionsMenuRepeat	= ScreenMessage(SM_User+1);

ScreenSelectMusic::ScreenSelectMusic( CString sClassName ) : ScreenWithMenuElements( sClassName )
{
	LOG->Trace( "ScreenSelectMusic::ScreenSelectMusic()" );

	// Set this here to make sure there are no bugs!
	GAMESTATE->m_bOptionsFromAttack = false;

	LIGHTSMAN->SetLightsMode( LIGHTSMODE_MENU );

	m_DisplayMode = GAMESTATE->IsCourseMode() ? DISPLAY_COURSES : DISPLAY_SONGS;

	/* Finish any previous stage.  It's OK to call this when we havn't played a stage yet.
	 * Do this before anything that might look at GAMESTATE->m_iCurrentStageIndex. */
	GAMESTATE->FinishStage();

	// If, for whatever reason, we're here and there is no style set, auto set a style based on the number of
	// joined players
	if( AUTO_SET_STYLE )
	{
		if( GAMESTATE->GetCurrentStyle() == NULL )
		{
			const Style* pStyle = NULL;
			switch( GAMESTATE->GetNumSidesJoined() )
			{
			case 1:
				pStyle = GAMEMAN->GameAndStringToStyle( GAMESTATE->m_pCurGame, "single" );
				break;

			case 2:
				pStyle = GAMEMAN->GameAndStringToStyle( GAMESTATE->m_pCurGame, "versus" );
				break;

			default:
				LOG->Warn( "Problem with autoset style; number of players not defined." );
				break;
			}
			if (pStyle != NULL)
				GAMESTATE->SetCurrentStyle(pStyle);
			else
				RageException::Throw( "The Style has not been set. A problem occured with the autoset function." );
		}
	}

	m_LastUsedStepsType = m_LastChosenStepsType = GAMESTATE->GetCurrentStyle()->m_StepsType;
	GAMESTATE->m_vPossibleStepsTypes.clear();
	LOG->Trace( "Resetting ModeSwitcher" );

	m_bUseModeSwitcher = THEME->GetMetricB(m_sName,"UseModeSwitcher");
	m_s1PlayerModes = THEME->GetMetric(m_sName,"OnePlayerModes");
	m_s2PlayerModes = THEME->GetMetric(m_sName,"TwoPlayerModes");
	m_bModeSwitcherWraps = THEME->GetMetricB(m_sName,"SkipMissingSongStyles");
	m_bModeSwitcherLoadsAll = THEME->GetMetricB(m_sName,"LoadEveryPossibleSong");

	if( m_bUseModeSwitcher )
	{
		if( m_bModeSwitcherLoadsAll )
		{
			CString sModes = "";
			int iNumPlayers = GAMESTATE->GetNumSidesJoined();
			switch( iNumPlayers )
			{
			case 1:
				sModes = m_s1PlayerModes;
				break;
			case 2:
				sModes = m_s2PlayerModes;
				break;
			default:
				LOG->Warn( "Problem loading every possible song; number of players not defined." );
			}

			if( sModes != "" )
			{
				CStringArray sAllowedModes;
				split(sModes,",",sAllowedModes,true);	// split the string, and ignore empty values
				if( sAllowedModes.size() != 0 )
				{
					m_vPossibleStyles.clear();
					GAMEMAN->GetStylesForGame( GAMESTATE->m_pCurGame, m_vPossibleStyles );
					for( unsigned s=0; s<m_vPossibleStyles.size(); ++s )
					{
						for( unsigned m=0; m<sAllowedModes.size(); ++m )
						{
							if( stricmp( m_vPossibleStyles[s]->m_szName, sAllowedModes[m] ) == 0 )
							{
								GAMESTATE->m_vPossibleStepsTypes.push_back( m_vPossibleStyles[s]->m_StepsType );
								LOG->Trace( "Adding '%s' to ModeSwitcher", StepsTypeToString(m_vPossibleStyles[s]->m_StepsType).c_str() );
								break;
							}
						}
					}
				}
			}
			else
			{
				m_vPossibleStyles.clear();
				GAMEMAN->GetStylesForGame( GAMESTATE->m_pCurGame, m_vPossibleStyles );
				for( unsigned s=0; s<m_vPossibleStyles.size(); ++s )
				{
					switch( iNumPlayers )
					{
					case 1:
						switch( m_vPossibleStyles[s]->m_StyleType )
						{
						case Style::ONE_PLAYER_TWO_CREDITS:
							if( GAMESTATE->GetCurrentStyle()->m_StyleType == Style::ONE_PLAYER_TWO_CREDITS )
								break;
#if defined( WITH_COIN_MODE )
							if(!( PREFSMAN->GetPremium() == PrefsManager::NO_PREMIUM && PREFSMAN->GetCoinMode() == PrefsManager::COIN_PAY ))
								break;
#endif
						case Style::TWO_PLAYERS_TWO_CREDITS:
							continue;
						default:;
						}
						break;
					case 2:
						if( m_vPossibleStyles[s]->m_StyleType != Style::TWO_PLAYERS_TWO_CREDITS )
							continue;
						break;
					default:;
					}

					GAMESTATE->m_vPossibleStepsTypes.push_back( m_vPossibleStyles[s]->m_StepsType );
					LOG->Trace( "Adding '%s' to ModeSwitcher", StepsTypeToString(m_vPossibleStyles[s]->m_StepsType).c_str() );
				}
			}
		}
	}

	if( GAMESTATE->m_vPossibleStepsTypes.empty() )
	{
		GAMESTATE->m_vPossibleStepsTypes.push_back( m_LastUsedStepsType );
		LOG->Trace( "Adding '%s' to ModeSwitcher", StepsTypeToString(m_LastUsedStepsType).c_str() );
	}

	// Set up the score digits
	m_iScoreDigits = THEME->GetMetricI("ScoreDisplay","ScreenSelectMusicNumberShownDigits");

	// Prevent negative values from being entered
	if( m_iScoreDigits < 0 )
		m_iScoreDigits = 0;

	m_fFOV = THEME->GetMetricF(m_sName,"FOV");
	m_fFOVCenterX = THEME->GetMetricF(m_sName,"FOVCenterX");
	m_fFOVCenterY = THEME->GetMetricF(m_sName,"FOVCenterY");

	if( !AUTO_SET_STYLE )
	{
		if( GAMESTATE->GetCurrentStyle() == NULL )
			RageException::Throw( "The Style has not been set.  A theme must set the Style before loading ScreenSelectMusic." );
	}

	if( GAMESTATE->m_PlayMode == PLAY_MODE_INVALID )
		RageException::Throw( "The PlayMode has not been set.  A theme must set the PlayMode before loading ScreenSelectMusic." );

	// Selection State
	m_iSelectionState = SELECT_SONG;	// Initial set

	m_bTwoPartSelection = THEME->GetMetricB(m_sName,"TwoPartSelection");
	m_bSampleMusicLoops = THEME->GetMetricB(m_sName,"SampleMusicPlaysInfinitely");
	m_iPreviewMusicMode = THEME->GetMetricI(m_sName,"PreviewMusicMode");

	if( !GAMESTATE->IsCourseMode() && m_iPreviewMusicMode == 1 )
		m_iSelectionState = SELECT_PREVIEW;

	// pop'n music has this under the songwheel...
	FOREACH_PlayerNumber( p )
	{
		m_sprCharacterIcon[p].SetName( ssprintf("CharacterIconP%d",p+1) );

		Character* pChar = GAMESTATE->m_pCurCharacters[p];
		CString sPath = pChar->GetSongSelectIconPath();

		if( sPath.empty() )
			continue;

		m_sprCharacterIcon[p].Load( sPath );
		SET_XY( m_sprCharacterIcon[p] );
		this->AddChild( &m_sprCharacterIcon[p] );
	}

	m_MusicWheelUnder.Load( THEME->GetPathG(m_sName,"wheel under") );
	m_MusicWheelUnder->SetName( "WheelUnder" );
	SET_XY( m_MusicWheelUnder );
	this->AddChild( m_MusicWheelUnder );

	m_MusicWheel.SetName( m_sName + " MusicWheel", "Wheel" );
	m_MusicWheel.Load( m_sName );
	SET_XY( m_MusicWheel );
	this->AddChild( &m_MusicWheel );

	m_bBannerWheel = THEME->GetMetricB(m_sName,"UseBannerWheel");

	m_bShowBanners = THEME->GetMetricB(m_sName,"ShowBanners") && PREFSMAN->m_bShowBanners;
	m_bShowDiscs = THEME->GetMetricB(m_sName,"ShowDiscs") && PREFSMAN->m_bShowDiscs;
	m_bShowBackgrounds = THEME->GetMetricB(m_sName,"ShowBackgrounds") && PREFSMAN->m_bShowBackgrounds;
	m_bShowPreviews = THEME->GetMetricB(m_sName,"ShowPreviews") && PREFSMAN->m_bShowPreviews;
	m_bShowCDTitles = THEME->GetMetricB(m_sName,"ShowCDTitles") && PREFSMAN->m_bShowCDTitles;

	if( m_bShowPreviews )
	{
		m_bPreviewInSprite = THEME->GetMetricB(m_sName,"DrawPreviewInSprite");

		if( m_bPreviewInSprite )
		{
			m_sFallbackPreviewPath = THEME->GetPathG("Common","fallback preview");
			m_sAllMusicPreviewPath = THEME->GetPathG("Preview","All Music");
			m_sSortPreviewPath = THEME->GetPathG("Preview","sort");
			m_sModePreviewPath = THEME->GetPathG("Preview","mode");
			m_sRoulettePreviewPath = THEME->GetPathG("Preview","roulette");
			m_sRandomPreviewPath = THEME->GetPathG("Preview","random");
		}
		else
			m_bPreviewLoadsCachedBG = THEME->GetMetricB(m_sName,"ShowCachedBackgroundInPreviewWhileMoving");
	}

	if( m_bShowCDTitles )
	{
		m_bLoadFallbackCDTitle = false;
		m_sFallbackCDTitlePath = THEME->GetPathG(m_sName,"fallback cdtitle");
	}

	if( m_bShowBanners )
	{
		if( m_bBannerWheel )
			m_sprBannerMask.SetName( m_sName + " TextBanner", "TextBanner" );	// use the same metrics and animation as TextBanner
		else
			m_sprBannerMask.SetName( "Banner" );	// use the same metrics and animation as Banner

		m_sprBannerMask.Load( THEME->GetPathG(m_sName,"banner mask") );
		m_sprBannerMask.SetBlendMode( BLEND_NO_EFFECT );	// don't draw to color buffer
		m_sprBannerMask.SetZWrite( true );	// do draw to the zbuffer
		SET_XY( m_sprBannerMask );
		this->AddChild( &m_sprBannerMask );

		if( m_bBannerWheel )
		{
			m_TextBanner.SetName( m_sName + " TextBanner", "TextBanner" );
			m_TextBanner.SetHorizAlign( align_left );
			m_TextBanner.SetZTestMode( ZTEST_WRITE_ON_PASS );	// do have to pass the z test
			m_TextBanner.SetXY( THEME->GetMetricF(m_sName,"SongNameX"), THEME->GetMetricF(m_sName,"SongNameY") );
			this->AddChild( &m_TextBanner );
		}
		else
		{
			m_Banner.SetName( "Banner" );
			m_Banner.SetZTestMode( ZTEST_WRITE_ON_PASS );	// do have to pass the z test
			m_Banner.ScaleToClipped( THEME->GetMetricF(m_sName,"BannerWidth"), THEME->GetMetricF(m_sName,"BannerHeight") );
			SET_XY( m_Banner );
			this->AddChild( &m_Banner );
		}

		m_sprBannerFrame.Load( THEME->GetPathG(m_sName,"banner frame") );
		m_sprBannerFrame->SetName( "BannerFrame" );
		SET_XY( m_sprBannerFrame );
		this->AddChild( m_sprBannerFrame );
	}

	if( m_bShowDiscs )
	{
		m_sprDiscMask.SetName( "Disc" );	// use the same metrics and animation as Disc
		m_sprDiscMask.Load( THEME->GetPathG(m_sName,"disc mask") );
		m_sprDiscMask.SetBlendMode( BLEND_NO_EFFECT );	// don't draw to color buffer
		m_sprDiscMask.SetZWrite( true );	// do draw to the zbuffer
		SET_XY( m_sprDiscMask );
		this->AddChild( &m_sprDiscMask );

		m_Disc.SetName( "Disc" );
		m_Disc.SetZTestMode( ZTEST_WRITE_ON_PASS );	// do have to pass the z test
		m_Disc.ScaleToClipped( THEME->GetMetricF(m_sName,"DiscWidth"), THEME->GetMetricF(m_sName,"DiscHeight") );
		SET_XY( m_Disc );
		this->AddChild( &m_Disc );

		m_sprDiscFrame.Load( THEME->GetPathG(m_sName,"disc frame") );
		m_sprDiscFrame->SetName( "DiscFrame" );
		SET_XY( m_sprDiscFrame );
		this->AddChild( m_sprDiscFrame );
	}

	if( m_bShowBackgrounds )
	{
		m_sprBackgroundMask.SetName( "Background" );	// use the same metrics and animation as Background
		m_sprBackgroundMask.Load( THEME->GetPathG(m_sName,"background mask") );
		m_sprBackgroundMask.SetBlendMode( BLEND_NO_EFFECT );	// don't draw to color buffer
		m_sprBackgroundMask.SetZWrite( true );	// do draw to the zbuffer
		SET_XY( m_sprBackgroundMask );
		this->AddChild( &m_sprBackgroundMask );

		m_Background.SetName( "Background" );
		m_Background.SetZTestMode( ZTEST_WRITE_ON_PASS );	// do have to pass the z test
		m_Background.ScaleToClipped( THEME->GetMetricF(m_sName,"BackgroundWidth"), THEME->GetMetricF(m_sName,"BackgroundHeight") );
		SET_XY( m_Background );
		this->AddChild( &m_Background );

		m_sprBackgroundFrame.Load( THEME->GetPathG(m_sName,"background frame") );
		m_sprBackgroundFrame->SetName( "BackgroundFrame" );
		SET_XY( m_sprBackgroundFrame );
		this->AddChild( m_sprBackgroundFrame );
	}

	if( m_bShowPreviews )
	{
		m_sprPreviewMask.SetName( "Preview" );	// use the same metrics and animation as Preview
		m_sprPreviewMask.Load( THEME->GetPathG(m_sName,"preview mask") );
		m_sprPreviewMask.SetBlendMode( BLEND_NO_EFFECT );	// don't draw to color buffer
		m_sprPreviewMask.SetZWrite( true );	// do draw to the zbuffer
		SET_XY( m_sprPreviewMask );
		this->AddChild( &m_sprPreviewMask );

		if( m_bPreviewInSprite )
		{
			m_sprPreview.SetName( "Preview" );
			m_sprPreview.Load( m_sFallbackPreviewPath );
			m_sprPreview.SetZTestMode( ZTEST_WRITE_ON_PASS );	// do have to pass the z test
			m_sprPreview.ScaleToClipped( THEME->GetMetricF(m_sName,"PreviewWidth"), THEME->GetMetricF(m_sName,"PreviewHeight") );
			SET_XY( m_sprPreview );
			this->AddChild( &m_sprPreview );
		}
		else
		{
			m_Preview.SetName( "Preview" );
			m_Preview.SetZTestMode( ZTEST_WRITE_ON_PASS );	// do have to pass the z test
			m_Preview.ScaleToClipped( THEME->GetMetricF(m_sName,"PreviewWidth"), THEME->GetMetricF(m_sName,"PreviewHeight") );
			SET_XY( m_Preview );
			this->AddChild( &m_Preview );
		}

		m_sprPreviewFrame.Load( THEME->GetPathG(m_sName,"preview frame") );
		m_sprPreviewFrame->SetName( "PreviewFrame" );
		SET_XY( m_sprPreviewFrame );
		this->AddChild( m_sprPreviewFrame );
	}

	if( m_bShowCDTitles )
	{
		m_sprCDTitleFront.SetName( "CDTitle" );
		m_sprCDTitleFront.Load( m_sFallbackCDTitlePath );

		m_sprCDTitleBack.SetName( "CDTitle" );
		m_sprCDTitleBack.Load( m_sFallbackCDTitlePath );

		if( THEME->GetMetricB(m_sName,"ResizeCDTitles") )
		{
			float fWidth = THEME->GetMetricF(m_sName,"CDTitleWidth"),
				fHeight = THEME->GetMetricF(m_sName,"CDTitleHeight");

			m_sprCDTitleFront.ScaleToClipped( fWidth, fHeight );
			m_sprCDTitleBack.ScaleToClipped( fWidth, fHeight );
		}

		SET_XY( m_sprCDTitleFront );
		COMMAND( m_sprCDTitleFront, "Front" );

		SET_XY( m_sprCDTitleBack );
		COMMAND( m_sprCDTitleBack, "Back" );

		this->AddChild( &m_sprCDTitleFront );
		this->AddChild( &m_sprCDTitleBack );
	}

	m_sprExplanation.Load( THEME->GetPathG(m_sName,"explanation") );
	m_sprExplanation->SetName( "Explanation" );
	SET_XY( m_sprExplanation );
	this->AddChild( m_sprExplanation );

	m_BPMDisplay.SetName( "BPMDisplay" );
	m_BPMDisplay.Load();
	SET_XY( m_BPMDisplay );
	this->AddChild( &m_BPMDisplay );

	m_DifficultyDisplay.SetName( "DifficultyDisplay" );
	m_DifficultyDisplay.SetShadowLength( 0 );
	SET_XY( m_DifficultyDisplay );
	this->AddChild( &m_DifficultyDisplay );

	{
		CStringArray StageTexts;
		GAMESTATE->GetAllStageTexts( StageTexts );
		for( unsigned i = 0; i < StageTexts.size(); ++i )
		{
			CString path = THEME->GetPathG( m_sName, "stage "+StageTexts[i], true );
			if( path != "" )
				TEXTUREMAN->CacheTexture( path );
		}
	}

	// loaded in AfterMusicChange
	m_sprStage.SetName( "Stage" );
	SET_XY( m_sprStage );
	this->AddChild( &m_sprStage );

	m_GrooveRadar.SetName( "Radar" );
	SET_XY( m_GrooveRadar );
	if( THEME->GetMetricB(m_sName,"ShowRadar") )
		this->AddChild( &m_GrooveRadar );

	m_GrooveGraph.SetName( "Graph" );
	SET_XY( m_GrooveGraph );
	if( THEME->GetMetricB(m_sName,"ShowGraph") )
		this->AddChild( &m_GrooveGraph );

	m_textSongOptions.SetName( "SongOptions" );
	m_textSongOptions.LoadFromFont( THEME->GetPathF("Common", "normal") );
	SET_XY( m_textSongOptions );
	this->AddChild( &m_textSongOptions );

	m_textCurMode.SetName( "CurMode" );
	m_textCurMode.LoadFromFont( THEME->GetPathF(m_sName,"cur mode") );
	SET_XY( m_textCurMode );
	this->AddChild( &m_textCurMode );

	m_textNumSongs.SetName( "NumSongs" );
	m_textNumSongs.LoadFromFont( THEME->GetPathF(m_sName,"num songs") );
	SET_XY( m_textNumSongs );
	this->AddChild( &m_textNumSongs );

	m_textGroupName.SetName( "GroupName" );
	m_textGroupName.LoadFromFont( THEME->GetPathF(m_sName,"group name") );
	SET_XY( m_textGroupName );
	this->AddChild( &m_textGroupName );

	m_textTotalTime.SetName( "TotalTime" );
	m_textTotalTime.LoadFromFont( THEME->GetPathF(m_sName,"total time") );
	SET_XY( m_textTotalTime );
	this->AddChild( &m_textTotalTime );

	m_CourseContentsFrame.SetName( "CourseContents" );
	SET_XY( m_CourseContentsFrame );
	this->AddChild( &m_CourseContentsFrame );

	m_Artist.SetName( "ArtistDisplay" );
	m_Artist.Load();
	SET_XY( m_Artist );
	this->AddChild( &m_Artist );

	m_MachineRank.SetName( "MachineRank" );
	m_MachineRank.LoadFromFont( THEME->GetPathF(m_sName,"rank") );
	SET_XY( m_MachineRank );
	this->AddChild( &m_MachineRank );

	m_bShowDifficultyList = THEME->GetMetricB(m_sName,"ShowDifficultyList");
	m_bShowPanes = THEME->GetMetricB(m_sName,"ShowPanes");
	m_bStepsTypeIcons = THEME->GetMetricB(m_sName,"DifficultyIconsPerStyle");

	if( m_bShowDifficultyList )
	{
		m_DifficultyList.SetName(THEME->GetMetric(m_sName, "DifficultyListSuffix"));
		m_DifficultyList.Load(THEME->GetMetricB(m_sName, "ExtendedDifficultyList"));
		SET_XY_AND_ON_COMMAND(m_DifficultyList);
		this->AddChild(&m_DifficultyList);
	}

	FOREACH_HumanPlayer( p )
	{
		m_sprDifficultyFrame[p].SetName( ssprintf("DifficultyFrameP%d",p+1) );
		m_sprDifficultyFrame[p].Load( THEME->GetPathG(m_sName,ssprintf("difficulty frame p%d",p+1)) );
		m_sprDifficultyFrame[p].StopAnimating();
		SET_XY( m_sprDifficultyFrame[p] );
		this->AddChild( &m_sprDifficultyFrame[p] );

		m_DifficultyIcon[p].SetName( ssprintf("DifficultyIconP%d",p+1) );
		if( m_bStepsTypeIcons )
			m_DifficultyIcon[p].Load( THEME->GetPathG(m_sName,ssprintf("difficulty icons %s 1x%d",GAMESTATE->GetCurrentStyle()->m_szName, NUM_DIFFICULTIES)) );
		else
			m_DifficultyIcon[p].Load( THEME->GetPathG(m_sName,ssprintf("difficulty icons 1x%d",NUM_DIFFICULTIES)) );
		SET_XY( m_DifficultyIcon[p] );
		this->AddChild( &m_DifficultyIcon[p] );

		m_AutoGenIcon[p].SetName( ssprintf("AutogenIconP%d",p+1) );
		m_AutoGenIcon[p].Load( THEME->GetPathG(m_sName,"autogen") );
		SET_XY( m_AutoGenIcon[p] );
		this->AddChild( &m_AutoGenIcon[p] );

		m_OptionIconRow[p].SetName( ssprintf("OptionIconsP%d",p+1) );
		m_OptionIconRow[p].Load( (PlayerNumber)p );
		m_OptionIconRow[p].Refresh();
		SET_XY( m_OptionIconRow[p] );
		this->AddChild( &m_OptionIconRow[p] );

		m_sprMeterFrame[p].SetName( ssprintf("MeterFrameP%d",p+1) );
		m_sprMeterFrame[p].Load( THEME->GetPathG(m_sName,ssprintf("meter frame p%d",p+1)) );
		SET_XY( m_sprMeterFrame[p] );
		this->AddChild( &m_sprMeterFrame[p] );

		if( m_bShowPanes )
		{
			m_PaneDisplay[p].SetName( "PaneDisplay", ssprintf("PaneDisplayP%d",p+1) );
			m_PaneDisplay[p].Load( (PlayerNumber) p );
			this->AddChild( &m_PaneDisplay[p] );
		}

		m_DifficultyMeter[p].SetName( m_sName + ssprintf(" DifficultyMeterP%d",p+1), ssprintf("MeterP%d",p+1) );
		m_DifficultyMeter[p].Load();
		SET_XY_AND_ON_COMMAND( m_DifficultyMeter[p] );
		this->AddChild( &m_DifficultyMeter[p] );

		// add an icon onto the song select to show what
		// character they're using.

		m_sprHighScoreFrame[p].SetName( ssprintf("ScoreFrameP%d",p+1) );
		m_sprHighScoreFrame[p].Load( THEME->GetPathG(m_sName,ssprintf("score frame p%d",p+1)) );
		SET_XY( m_sprHighScoreFrame[p] );
		this->AddChild( &m_sprHighScoreFrame[p] );

		m_textHighScore[p].SetName( ssprintf("ScoreP%d",p+1) );
		m_textHighScore[p].LoadFromFont( THEME->GetPathF(m_sName,"score") );
		m_textHighScore[p].SetShadowLength( 0 );
		m_textHighScore[p].SetDiffuse( PlayerToColor(p) );
		SET_XY( m_textHighScore[p] );
		this->AddChild( &m_textHighScore[p] );
	}

	m_MusicSortDisplay.SetName( "SortIcon" );
	m_MusicSortDisplay.Set( GAMESTATE->m_SortOrder );
	SET_XY( m_MusicSortDisplay );
	this->AddChild( &m_MusicSortDisplay );

	for( int st=0; st<PREFSMAN->m_iStageLengths; ++st )
		m_bStageLengthBalloon.push_back( THEME->GetMetricB(m_sName,ssprintf("StageLength%dUsesBalloon",st+1)) );

	m_sprBalloon.SetName( "Balloon" );
	this->AddChild( &m_sprBalloon );

	m_sprCourseHasMods.LoadAndSetName( m_sName, "CourseHasMods" );
	SET_XY( m_sprCourseHasMods );
	this->AddChild( m_sprCourseHasMods );

	m_sprOptionsMessage.SetName( "OptionsMessage" );
	m_sprOptionsMessage.Load( THEME->GetPathG(m_sName,"options message 1x2") );
	m_sprOptionsMessage.StopAnimating();
	m_sprOptionsMessage.SetDiffuse( RageColor(1,1,1,0) );	// invisible
	//this->AddChild( &m_sprOptionsMessage );	// we have to draw this manually over the top of transitions

	FOREACH_PlayerNumber( p )
	{
		m_sprNonPresence[p].SetName( ssprintf("NonPresenceP%d",p+1) );
		m_sprNonPresence[p].Load( THEME->GetPathG(m_sName,ssprintf("nonpresence p%d",p+1)) );
		SET_XY( m_sprNonPresence[p] );
		this->AddChild( &m_sprNonPresence[p] );
	}

	m_Overlay.SetName( "Overlay" );
	m_Overlay.LoadFromAniDir( THEME->GetPathB(m_sName, "overlay"));
	this->AddChild( &m_Overlay );

	m_bgOptionsOut.Load( THEME->GetPathB(m_sName, "options out") );
//	this->AddChild( &m_bgOptionsOut ); // drawn on top
	m_bgNoOptionsOut.Load( THEME->GetPathB(m_sName, "no options out") );
//	this->AddChild( &m_bgNoOptionsOut ); // drawn on top

	m_soundDifficultyEasier.Load( THEME->GetPathS(m_sName,"difficulty easier") );
	m_soundDifficultyHarder.Load( THEME->GetPathS(m_sName,"difficulty harder") );
	m_soundOptionsChange.Load( THEME->GetPathS(m_sName,"options") );
	m_soundLocked.Load( THEME->GetPathS(m_sName,"locked") );

	SOUND->PlayOnceFromAnnouncer( "select music intro" );

	m_bMadeChoice = false;
	m_bGoToOptions = false;
	m_bAllowOptionsMenu = m_bAllowOptionsMenuRepeat = false;
	m_bThemeMusic = true;

	m_sSongOptionsExtraCommand = THEME->GetMetric(m_sName,"SongOptionsExtraCommand");
	m_fSampleMusicDelay = THEME->GetMetricF(m_sName,"SampleMusicDelay");
	m_bAlignMusicBeats = THEME->GetMetricB(m_sName,"AlignMusicBeat");
	m_bBlinkAutogenIcon = THEME->GetMetricB(m_sName,"BlinkAutogenIcon");
	m_bMenuUpDifficulty = THEME->GetMetricB(m_sName,"MenuUpChangesDifficulty");
	m_bMenuDownDifficulty = THEME->GetMetricB(m_sName,"MenuDownChangesDifficulty");
	m_bMenuUpGroup = THEME->GetMetricB(m_sName,"MenuUpChangesGroup");
	m_bMenuDownGroup = THEME->GetMetricB(m_sName,"MenuDownChangesGroup");
	m_bReverseDifficulty = THEME->GetMetricB(m_sName,"ReverseOrderDifficultyChange");
	m_bReverseGroup = THEME->GetMetricB(m_sName,"ReverseOrderSectionChange");
	m_bSectionChangeSavesPosition = THEME->GetMetricB(m_sName,"SectionChangeSavesPosition");
	m_bUseSongNumber = THEME->GetMetricB(m_sName,"UseSongNumberInStage");
	m_bNo2PInput = THEME->GetMetricB(m_sName,"Disable2PInputInDoubles");
	m_bResetKeyTimerAfterSectionChange = THEME->GetMetricB(m_sName,"ResetKeyTimerAfterSectionChange");
	m_bPortalDisplaysSong = THEME->GetMetricB(m_sName,"PortalDisplaysSong");
	m_bShowShortGroupName = THEME->GetMetricB(m_sName,"ShortenGroupName");

	m_bBannerWaiting = false;
	m_bDiscWaiting = false;
	m_bBackgroundWaiting = false;
	m_bPreviewWaiting = false;
	m_bCDTitleWaiting = false;

	m_sBannerPath = "";
	m_sDiscPath = "";
	m_sBackgroundPath = "";
	m_sPreviewPath = "";
	m_sCDTitlePath = "";

	m_fHQBanner = THEME->GetMetricF("FadingBanner","FadeSeconds");
	m_fHQBackground = THEME->GetMetricF("FadingBackground","FadeSeconds");

	m_bSampleMusicWaiting = false;
	m_tStartedLoadingAt.SetZero();
	m_sSectionMusic = THEME->GetPathS(m_sName,"section music");

	m_bTweenWhileMovingAndSettled = THEME->GetMetricB(m_sName,"TweenWhileMovingAndSettled");
	m_bTweenWhenSectionChanges = THEME->GetMetricB(m_sName,"TweenWhenSectionChanges");
	m_bMovingCommandRepeats = THEME->GetMetricB(m_sName,"MovingCommandRepeats");

	FOREACH_HumanPlayer( pn )
	{
		m_sScoreSortChangeCommand[pn] = THEME->GetMetric(m_sName,ssprintf("ScoreP%iSortChangeCommand",pn+1));
		m_sScoreFrameSortChangeCommand[pn] = THEME->GetMetric(m_sName,ssprintf("ScoreFrameP%iSortChangeCommand",pn+1));
	}

	m_bMovingTweenWaiting = false;
	m_bSettledTweenWaiting = false;
	m_bSectionTweenWaiting = false;

	if( m_bTweenWhileMovingAndSettled )
	{
		MOVING_COMMAND( WheelUnder );
		SETTLED_COMMAND( WheelUnder );
		MOVING_COMMAND( Wheel );
		SETTLED_COMMAND( Wheel );

		if( m_bShowBanners )
		{
			if( m_bBannerWheel )
			{
				MOVING_COMMAND( TextBanner );
				SETTLED_COMMAND( TextBanner );
			}
			else
			{
				MOVING_COMMAND( Banner );
				SETTLED_COMMAND( Banner );
			}

			MOVING_COMMAND( BannerFrame );
			SETTLED_COMMAND( BannerFrame );
		}

		if( m_bShowDiscs )
		{
			MOVING_COMMAND( Disc );
			SETTLED_COMMAND( Disc );
			MOVING_COMMAND( DiscFrame );
			SETTLED_COMMAND( DiscFrame );
		}

		if( m_bShowBackgrounds )
		{
			MOVING_COMMAND( Background );
			SETTLED_COMMAND( Background );
			MOVING_COMMAND( BackgroundFrame );
			SETTLED_COMMAND( BackgroundFrame );
		}

		if( m_bShowPreviews )
		{
			MOVING_COMMAND( Preview );
			SETTLED_COMMAND( Preview );
			MOVING_COMMAND( PreviewFrame );
			SETTLED_COMMAND( PreviewFrame );
		}

		if( m_bShowCDTitles )
		{
			MOVING_COMMAND( CDTitleFront );
			SETTLED_COMMAND( CDTitleFront );
			MOVING_COMMAND( CDTitleBack );
			SETTLED_COMMAND( CDTitleBack );
		}

		MOVING_COMMAND( Explanation );
		SETTLED_COMMAND( Explanation );
		MOVING_COMMAND( BPMDisplay );
		SETTLED_COMMAND( BPMDisplay );
		MOVING_COMMAND( DifficultyDisplay );
		SETTLED_COMMAND( DifficultyDisplay );
		MOVING_COMMAND( Stage );
		SETTLED_COMMAND( Stage );
		MOVING_COMMAND( Radar );
		SETTLED_COMMAND( Radar );
		MOVING_COMMAND( Graph );
		SETTLED_COMMAND( Graph );
		MOVING_COMMAND( SongOptions );
		SETTLED_COMMAND( SongOptions );
		MOVING_COMMAND( CurMode );
		SETTLED_COMMAND( CurMode );
		MOVING_COMMAND( NumSongs );
		SETTLED_COMMAND( NumSongs );
		MOVING_COMMAND( GroupName );
		SETTLED_COMMAND( GroupName );
		MOVING_COMMAND( TotalTime );
		SETTLED_COMMAND( TotalTime );

		MOVING_COMMAND( ArtistDisplay );
		SETTLED_COMMAND( ArtistDisplay );
		MOVING_COMMAND( MachineRank );
		SETTLED_COMMAND( MachineRank );

		MOVING_COMMAND( SortIcon );
		SETTLED_COMMAND( SortIcon );
		MOVING_COMMAND( Balloon );
		SETTLED_COMMAND( Balloon );
		MOVING_COMMAND( CourseHasMods );
		SETTLED_COMMAND( CourseHasMods );

		MOVING_COMMAND( Overlay );
		SETTLED_COMMAND( Overlay );

		if( m_bShowDifficultyList )
		{
			MOVING_COMMAND( DifficultyList );
			SETTLED_COMMAND( DifficultyList );
		}

		FOREACH_PlayerNumber( pn )
		{
			if( GAMESTATE->IsHumanPlayer( pn ) )
			{
				MOVING_COMMAND_P( CharacterIcon, pn );
				SETTLED_COMMAND_P( CharacterIcon, pn );
				MOVING_COMMAND_P( OptionIcons, pn );
				SETTLED_COMMAND_P( OptionIcons, pn );

				if( m_bShowPanes )
				{
					MOVING_COMMAND_P( PaneDisplay, pn );
					SETTLED_COMMAND_P( PaneDisplay, pn );
				}

				MOVING_COMMAND_P( DifficultyMeter, pn );
				SETTLED_COMMAND_P( DifficultyMeter, pn );
				MOVING_COMMAND_P( ScoreFrame, pn );
				SETTLED_COMMAND_P( ScoreFrame, pn );
				MOVING_COMMAND_P( Score, pn );
				SETTLED_COMMAND_P( Score, pn );

				MOVING_COMMAND_P( DifficultyFrame, pn );
				SETTLED_COMMAND_P( DifficultyFrame, pn );
				MOVING_COMMAND_P( MeterFrame, pn );
				SETTLED_COMMAND_P( MeterFrame, pn );
				MOVING_COMMAND_P( DifficultyIcon, pn );
				SETTLED_COMMAND_P( DifficultyIcon, pn );
				MOVING_COMMAND_P( AutoGenIcon, pn );
				SETTLED_COMMAND_P( AutoGenIcon, pn );
			}

			MOVING_COMMAND_P( NonPresence, pn );
			SETTLED_COMMAND_P( NonPresence, pn );
		}

		MOVING_COMMAND( CourseContents );
		SETTLED_COMMAND( CourseContents );
	}

	if( m_bTweenWhenSectionChanges )
	{
		SECTION_COMMAND( WheelUnder );
		SECTION_COMMAND( Wheel );

		if( m_bShowBanners )
		{
			if( m_bBannerWheel )
				SECTION_COMMAND( TextBanner );
			else
				SECTION_COMMAND( Banner );

			SECTION_COMMAND( BannerFrame );
		}

		if( m_bShowDiscs )
		{
			SECTION_COMMAND( Disc );
			SECTION_COMMAND( DiscFrame );
		}

		if( m_bShowBackgrounds )
		{
			SECTION_COMMAND( Background );
			SECTION_COMMAND( BackgroundFrame );
		}

		if( m_bShowPreviews )
		{
			SECTION_COMMAND( Preview );
			SECTION_COMMAND( PreviewFrame );
		}

		if( m_bShowCDTitles )
		{
			SECTION_COMMAND( CDTitleFront );
			SECTION_COMMAND( CDTitleBack );
		}

		SECTION_COMMAND( Explanation );
		SECTION_COMMAND( BPMDisplay );
		SECTION_COMMAND( DifficultyDisplay );
		SECTION_COMMAND( Stage );
		SECTION_COMMAND( Radar );
		SECTION_COMMAND( Graph );
		SECTION_COMMAND( SongOptions );
		SECTION_COMMAND( CurMode );
		SECTION_COMMAND( NumSongs );
		SECTION_COMMAND( GroupName );
		SECTION_COMMAND( TotalTime );

		SECTION_COMMAND( ArtistDisplay );
		SECTION_COMMAND( MachineRank );

		SECTION_COMMAND( SortIcon );
		SECTION_COMMAND( Balloon );
		SECTION_COMMAND( CourseHasMods );

		SECTION_COMMAND( Overlay );

		if( m_bShowDifficultyList )
			SECTION_COMMAND( DifficultyList );

		FOREACH_PlayerNumber( pn )
		{
			if( GAMESTATE->IsHumanPlayer( pn ) )
			{
				SECTION_COMMAND_P( CharacterIcon, pn );
				SECTION_COMMAND_P( OptionIcons, pn );

				if( m_bShowPanes )
					SECTION_COMMAND_P( PaneDisplay, pn );

				SECTION_COMMAND_P( DifficultyMeter, pn );
				SECTION_COMMAND_P( ScoreFrame, pn );
				SECTION_COMMAND_P( Score, pn );

				SECTION_COMMAND_P( DifficultyFrame, pn );
				SECTION_COMMAND_P( MeterFrame, pn );
				SECTION_COMMAND_P( DifficultyIcon, pn );
				SECTION_COMMAND_P( AutoGenIcon, pn );
			}

			SECTION_COMMAND_P( NonPresence, pn );
		}

		SECTION_COMMAND( CourseContents );
	}

	m_iLastSection = m_MusicWheel.GetCurrentSection();
	m_bResetKeyTimers = false;

	// Used by the music wheel & modeswitcher
	m_MusicWheel.ModeSwitch( false );

	UpdateOptionsDisplays();

	AfterMusicChange();
	TweenOnScreen();

	this->SortByDrawOrder();

	m_bSectionTweenWaiting = m_bTweenWhenSectionChanges;
	m_bSettledTweenWaiting = m_bTweenWhileMovingAndSettled;

	AfterSectionChange();
}


ScreenSelectMusic::~ScreenSelectMusic()
{
	LOG->Trace( "ScreenSelectMusic::~ScreenSelectMusic()" );

}

void ScreenSelectMusic::DrawPrimitives()
{
	DISPLAY->CameraPushMatrix();
	DISPLAY->LoadMenuPerspective( m_fFOV, m_fFOVCenterX, m_fFOVCenterY );

	Screen::DrawPrimitives();
	m_sprOptionsMessage.Draw();
	m_bgOptionsOut.Draw();
	m_bgNoOptionsOut.Draw();

	DISPLAY->CameraPopMatrix();
}

void ScreenSelectMusic::TweenSongPartsOnScreen( bool Initial )
{
	m_GrooveRadar.StopTweening();
	m_GrooveGraph.StopTweening();
	m_GrooveRadar.TweenOnScreen();
	m_GrooveGraph.TweenOnScreen();
	if( m_bShowDifficultyList )
	{
		if( Initial )
		{
			ON_COMMAND( m_DifficultyList );
			m_DifficultyList.TweenOnScreen();
		}
//		else // do this after SM_SortOrderChanged
//			m_DifficultyList.Show();
	}

	{
		FOREACH_HumanPlayer( p )
		{
			ON_COMMAND( m_sprDifficultyFrame[p] );
			ON_COMMAND( m_sprMeterFrame[p] );
			ON_COMMAND( m_DifficultyIcon[p] );
			ON_COMMAND( m_AutoGenIcon[p] );
		}
	}

	{
		FOREACH_PlayerNumber( p )
		{
			ON_COMMAND( m_sprNonPresence[p] );
		}
	}
}

void ScreenSelectMusic::TweenSongPartsOffScreen( bool Final )
{
	m_GrooveRadar.TweenOffScreen();
	m_GrooveGraph.TweenOffScreen();
	if( m_bShowDifficultyList )
	{
		if( Final )
		{
			OFF_COMMAND( m_DifficultyList );
			m_DifficultyList.TweenOffScreen();
		}
		else
			m_DifficultyList.Hide();
	}

	{
		FOREACH_HumanPlayer( p )
		{
			OFF_COMMAND( m_sprDifficultyFrame[p] );
			OFF_COMMAND( m_sprMeterFrame[p] );
			OFF_COMMAND( m_DifficultyIcon[p] );
			OFF_COMMAND( m_AutoGenIcon[p] );
		}
	}

	{
		FOREACH_PlayerNumber( p )
		{
			OFF_COMMAND( m_sprNonPresence[p] );
		}
	}
}

void ScreenSelectMusic::TweenCoursePartsOnScreen( bool Initial )
{
	m_CourseContentsFrame.SetZoomY( 1 );
	if( Initial )
	{
		COMMAND( m_CourseContentsFrame, "On" );
	}
	else
	{
		m_CourseContentsFrame.SetFromGameState();
		COMMAND( m_CourseContentsFrame, "Show" );
	}
}

void ScreenSelectMusic::TweenCoursePartsOffScreen( bool Final )
{
	if( Final )
	{
		OFF_COMMAND( m_CourseContentsFrame );
	}
	else
	{
		COMMAND( m_CourseContentsFrame, "Hide" );
	}
}

void ScreenSelectMusic::TweenWhileMoving()
{
	if( !m_bTweenWhileMovingAndSettled || !m_bMovingTweenWaiting )
		return;

	switch( GAMESTATE->m_SortOrder )
	{
	case SORT_ALL_COURSES:
	case SORT_NONSTOP_COURSES:
	case SORT_ONI_COURSES:
	case SORT_ENDLESS_COURSES:
		TweenCoursePartsWhileMoving();
		break;
	default:
		TweenSongPartsWhileMoving();
		break;
	}

	FOREACH_HumanPlayer( p )
		m_sprCharacterIcon[p].Command( m_sCharacterIconMovingCommand[p] );

	m_MusicWheelUnder->Command( m_sWheelUnderMovingCommand );
	m_MusicWheel.Command( m_sWheelMovingCommand );

	if( m_bShowBanners )
	{
		if( m_bBannerWheel )
		{
			m_sprBannerMask.Command( m_sTextBannerMovingCommand );
			m_TextBanner.Command( m_sTextBannerMovingCommand );
		}
		else
		{
			m_sprBannerMask.Command( m_sBannerMovingCommand );
			m_Banner.Command( m_sBannerMovingCommand );
		}

		m_sprBannerFrame->Command( m_sBannerFrameMovingCommand );
	}

	if( m_bShowDiscs )
	{
		m_sprDiscMask.Command( m_sDiscMovingCommand );
		m_Disc.Command( m_sDiscMovingCommand );
		m_sprDiscFrame->Command( m_sDiscFrameMovingCommand );
	}

	if( m_bShowBackgrounds )
	{
		m_sprBackgroundMask.Command( m_sBackgroundMovingCommand );
		m_Background.Command( m_sBackgroundMovingCommand );
		m_sprBackgroundFrame->Command( m_sBackgroundFrameMovingCommand );
	}

	if( m_bShowPreviews )
	{
		m_sprPreviewMask.Command( m_sPreviewMovingCommand );

		if( m_bPreviewInSprite )
			m_sprPreview.Command( m_sPreviewMovingCommand );
		else
			m_Preview.Command( m_sPreviewMovingCommand );

		m_sprPreviewFrame->Command( m_sPreviewFrameMovingCommand );
	}

	if( m_bShowCDTitles )
	{
		m_sprCDTitleFront.Command( m_sCDTitleFrontMovingCommand );
		m_sprCDTitleBack.Command( m_sCDTitleBackMovingCommand );
	}

	m_sprExplanation->Command( m_sExplanationMovingCommand );
	m_BPMDisplay.Command( m_sBPMDisplayMovingCommand );
	m_DifficultyDisplay.Command( m_sDifficultyDisplayMovingCommand );
	m_sprStage.Command( m_sStageMovingCommand );
	m_GrooveRadar.Command( m_sRadarMovingCommand );
	m_GrooveGraph.Command( m_sGraphMovingCommand );
	m_textSongOptions.Command( m_sSongOptionsMovingCommand );
	m_textCurMode.Command( m_sCurModeMovingCommand );
	m_textNumSongs.Command( m_sNumSongsMovingCommand );
	m_textGroupName.Command( m_sGroupNameMovingCommand );
	m_textTotalTime.Command( m_sTotalTimeMovingCommand );

	m_Artist.Command( m_sArtistDisplayMovingCommand );
	m_MachineRank.Command( m_sMachineRankMovingCommand );

	m_MusicSortDisplay.Command( m_sSortIconMovingCommand );
	m_sprBalloon.Command( m_sBalloonMovingCommand );
	m_sprCourseHasMods->Command( m_sCourseHasModsMovingCommand );

	m_Overlay.Command( m_sOverlayMovingCommand );

	FOREACH_HumanPlayer( p )
	{
		m_OptionIconRow[p].Command( m_sOptionIconsMovingCommand[p] );
		if( m_bShowPanes )
			m_PaneDisplay[p].Command( m_sPaneDisplayMovingCommand[p] );
		m_DifficultyMeter[p].Command( m_sDifficultyMeterMovingCommand[p] );
		m_sprHighScoreFrame[p].Command( m_sScoreFrameMovingCommand[p] );
		m_textHighScore[p].Command( m_sScoreMovingCommand[p] );
	}

	//if( GAMESTATE->IsExtraStage() || GAMESTATE->IsExtraStage2() )
	//	m_textSongOptions.Command( m_sSongOptionsExtraCommandMovingCommand );

	m_bMovingTweenWaiting = false;
	m_bSettledTweenWaiting = true;
}

void ScreenSelectMusic::TweenSongPartsWhileMoving()
{
	if( m_bShowDifficultyList )
		m_DifficultyList.Command( m_sDifficultyListMovingCommand );

	FOREACH_PlayerNumber( p )
	{
		if( GAMESTATE->IsHumanPlayer(p) )
		{
			m_sprDifficultyFrame[p].Command( m_sDifficultyFrameMovingCommand[p] );
			m_sprMeterFrame[p].Command( m_sMeterFrameMovingCommand[p] );
			m_DifficultyIcon[p].Command( m_sDifficultyIconMovingCommand[p] );
			m_AutoGenIcon[p].Command( m_sAutoGenIconMovingCommand[p] );
		}

		m_sprNonPresence[p].Command( m_sNonPresenceMovingCommand[p] );
	}
}

void ScreenSelectMusic::TweenCoursePartsWhileMoving()
{
	m_CourseContentsFrame.Command( m_sCourseContentsMovingCommand );
}

void ScreenSelectMusic::TweenWhileSettled()
{
	if( !m_bTweenWhileMovingAndSettled || !m_bSettledTweenWaiting )
		return;

	switch( GAMESTATE->m_SortOrder )
	{
	case SORT_ALL_COURSES:
	case SORT_NONSTOP_COURSES:
	case SORT_ONI_COURSES:
	case SORT_ENDLESS_COURSES:
		TweenCoursePartsWhileSettled();
		break;
	default:
		TweenSongPartsWhileSettled();
		break;
	}

	FOREACH_HumanPlayer( p )
		m_sprCharacterIcon[p].Command( m_sCharacterIconSettledCommand[p] );

	m_MusicWheelUnder->Command( m_sWheelUnderSettledCommand );
	m_MusicWheel.Command( m_sWheelSettledCommand );

	if( m_bShowBanners )
	{
		if( m_bBannerWheel )
		{
			m_sprBannerMask.Command( m_sTextBannerSettledCommand );
			m_TextBanner.Command( m_sTextBannerSettledCommand );
		}
		else
		{
			m_sprBannerMask.Command( m_sBannerSettledCommand );
			m_Banner.Command( m_sBannerSettledCommand );
		}

		m_sprBannerFrame->Command( m_sBannerFrameSettledCommand );
	}

	if( m_bShowDiscs )
	{
		m_sprDiscMask.Command( m_sDiscSettledCommand );
		m_Disc.Command( m_sDiscSettledCommand );
		m_sprDiscFrame->Command( m_sDiscFrameSettledCommand );
	}

	if( m_bShowBackgrounds )
	{
		m_sprBackgroundMask.Command( m_sBackgroundSettledCommand );
		m_Background.Command( m_sBackgroundSettledCommand );
		m_sprBackgroundFrame->Command( m_sBackgroundFrameSettledCommand );
	}

	if( m_bShowPreviews )
	{
		m_sprPreviewMask.Command( m_sPreviewSettledCommand );

		if( m_bPreviewInSprite )
			m_sprPreview.Command( m_sPreviewSettledCommand );
		else
			m_Preview.Command( m_sPreviewSettledCommand );

		m_sprPreviewFrame->Command( m_sPreviewFrameSettledCommand );
	}

	if( m_bShowCDTitles )
	{
		m_sprCDTitleFront.Command( m_sCDTitleFrontSettledCommand );
		m_sprCDTitleBack.Command( m_sCDTitleBackSettledCommand );
	}

	m_sprExplanation->Command( m_sExplanationSettledCommand );
	m_BPMDisplay.Command( m_sBPMDisplaySettledCommand );
	m_DifficultyDisplay.Command( m_sDifficultyDisplaySettledCommand );
	m_sprStage.Command( m_sStageSettledCommand );
	m_GrooveRadar.Command( m_sRadarSettledCommand );
	m_GrooveGraph.Command( m_sGraphSettledCommand );
	m_textSongOptions.Command( m_sSongOptionsSettledCommand );
	m_textCurMode.Command( m_sCurModeSettledCommand );
	m_textNumSongs.Command( m_sNumSongsSettledCommand );
	m_textGroupName.Command( m_sGroupNameSettledCommand );
	m_textTotalTime.Command( m_sTotalTimeSettledCommand );

	m_Artist.Command( m_sArtistDisplaySettledCommand );
	m_MachineRank.Command( m_sMachineRankSettledCommand );

	m_MusicSortDisplay.Command( m_sSortIconSettledCommand );
	m_sprBalloon.Command( m_sBalloonSettledCommand );
	m_sprCourseHasMods->Command( m_sCourseHasModsSettledCommand );

	m_Overlay.Command( m_sOverlaySettledCommand );

	FOREACH_HumanPlayer( p )
	{
		m_OptionIconRow[p].Command( m_sOptionIconsSettledCommand[p] );
		if( m_bShowPanes )
			m_PaneDisplay[p].Command( m_sPaneDisplaySettledCommand[p] );
		m_DifficultyMeter[p].Command( m_sDifficultyMeterSettledCommand[p] );
		m_sprHighScoreFrame[p].Command( m_sScoreFrameSettledCommand[p] );
		m_textHighScore[p].Command( m_sScoreSettledCommand[p] );
	}

	//if( GAMESTATE->IsExtraStage() || GAMESTATE->IsExtraStage2() )
	//	m_textSongOptions.Command( m_sSongOptionsExtraCommandSettledCommand );

	m_bSettledTweenWaiting = false;
	m_bMovingTweenWaiting = true;
}

void ScreenSelectMusic::TweenSongPartsWhileSettled()
{
	if( m_bShowDifficultyList )
		m_DifficultyList.Command( m_sDifficultyListSettledCommand );

	FOREACH_PlayerNumber( p )
	{
		if( GAMESTATE->IsHumanPlayer(p) )
		{
			m_sprDifficultyFrame[p].Command( m_sDifficultyFrameSettledCommand[p] );
			m_sprMeterFrame[p].Command( m_sMeterFrameSettledCommand[p] );
			m_DifficultyIcon[p].Command( m_sDifficultyIconSettledCommand[p] );
			m_AutoGenIcon[p].Command( m_sAutoGenIconSettledCommand[p] );
		}

		m_sprNonPresence[p].Command( m_sNonPresenceSettledCommand[p] );
	}
}

void ScreenSelectMusic::TweenCoursePartsWhileSettled()
{
	m_CourseContentsFrame.Command( m_sCourseContentsSettledCommand );
}

void ScreenSelectMusic::TweenWhenSectionChanges()
{
	if( !m_bTweenWhenSectionChanges || !m_bSectionTweenWaiting )
		return;

	switch( GAMESTATE->m_SortOrder )
	{
	case SORT_ALL_COURSES:
	case SORT_NONSTOP_COURSES:
	case SORT_ONI_COURSES:
	case SORT_ENDLESS_COURSES:
		TweenCoursePartsWhenSectionChanges();
		break;
	default:
		TweenSongPartsWhenSectionChanges();
		break;
	}

	FOREACH_HumanPlayer( p )
		m_sprCharacterIcon[p].Command( m_sCharacterIconSectionCommand[p] );

	m_MusicWheelUnder->Command( m_sWheelUnderSectionCommand );
	m_MusicWheel.Command( m_sWheelSectionCommand );

	if( m_bShowBanners )
	{
		if( m_bBannerWheel )
		{
			m_sprBannerMask.Command( m_sTextBannerSectionCommand );
			m_TextBanner.Command( m_sTextBannerSectionCommand );
		}
		else
		{
			m_sprBannerMask.Command( m_sBannerSectionCommand );
			m_Banner.Command( m_sBannerSectionCommand );
		}

		m_sprBannerFrame->Command( m_sBannerFrameSectionCommand );
	}

	if( m_bShowDiscs )
	{
		m_sprDiscMask.Command( m_sDiscSectionCommand );
		m_Disc.Command( m_sDiscSectionCommand );
		m_sprDiscFrame->Command( m_sDiscFrameSectionCommand );
	}

	if( m_bShowBackgrounds )
	{
		m_sprBackgroundMask.Command( m_sBackgroundSectionCommand );
		m_Background.Command( m_sBackgroundSectionCommand );
		m_sprBackgroundFrame->Command( m_sBackgroundFrameSectionCommand );
	}

	if( m_bShowPreviews )
	{
		m_sprPreviewMask.Command( m_sPreviewSectionCommand );

		if( m_bPreviewInSprite )
			m_sprPreview.Command( m_sPreviewSectionCommand );
		else
			m_Preview.Command( m_sPreviewSectionCommand );

		m_sprPreviewFrame->Command( m_sPreviewFrameSectionCommand );
	}

	if( m_bShowCDTitles )
	{
		m_sprCDTitleFront.Command( m_sCDTitleFrontSectionCommand );
		m_sprCDTitleBack.Command( m_sCDTitleBackSectionCommand );
	}

	m_sprExplanation->Command( m_sExplanationSectionCommand );
	m_BPMDisplay.Command( m_sBPMDisplaySectionCommand );
	m_DifficultyDisplay.Command( m_sDifficultyDisplaySectionCommand );
	m_sprStage.Command( m_sStageSectionCommand );
	m_GrooveRadar.Command( m_sRadarSectionCommand );
	m_GrooveGraph.Command( m_sGraphSectionCommand );
	m_textSongOptions.Command( m_sSongOptionsSectionCommand );
	m_textCurMode.Command( m_sCurModeSectionCommand );
	m_textNumSongs.Command( m_sNumSongsSectionCommand );
	m_textGroupName.Command( m_sGroupNameSectionCommand );
	m_textTotalTime.Command( m_sTotalTimeSectionCommand );

	m_Artist.Command( m_sArtistDisplaySectionCommand );
	m_MachineRank.Command( m_sMachineRankSectionCommand );

	m_MusicSortDisplay.Command( m_sSortIconSectionCommand );
	m_sprBalloon.Command( m_sBalloonSectionCommand );
	m_sprCourseHasMods->Command( m_sCourseHasModsSectionCommand );

	m_Overlay.Command( m_sOverlaySectionCommand );

	FOREACH_HumanPlayer( p )
	{
		m_OptionIconRow[p].Command( m_sOptionIconsSectionCommand[p] );
		if( m_bShowPanes )
			m_PaneDisplay[p].Command( m_sPaneDisplaySectionCommand[p] );
		m_DifficultyMeter[p].Command( m_sDifficultyMeterSectionCommand[p] );
		m_sprHighScoreFrame[p].Command( m_sScoreFrameSectionCommand[p] );
		m_textHighScore[p].Command( m_sScoreSectionCommand[p] );
	}

	//if( GAMESTATE->IsExtraStage() || GAMESTATE->IsExtraStage2() )
	//	m_textSongOptions.Command( m_sSongOptionsExtraCommandSectionCommand );

	m_bSectionTweenWaiting = false;
}

void ScreenSelectMusic::TweenSongPartsWhenSectionChanges()
{
	if( m_bShowDifficultyList )
		m_DifficultyList.Command( m_sDifficultyListSectionCommand );

	FOREACH_PlayerNumber( p )
	{
		if( GAMESTATE->IsHumanPlayer(p) )
		{
			m_sprDifficultyFrame[p].Command( m_sDifficultyFrameSectionCommand[p] );
			m_sprMeterFrame[p].Command( m_sMeterFrameSectionCommand[p] );
			m_DifficultyIcon[p].Command( m_sDifficultyIconSectionCommand[p] );
			m_AutoGenIcon[p].Command( m_sAutoGenIconSectionCommand[p] );
		}

		m_sprNonPresence[p].Command( m_sNonPresenceSectionCommand[p] );
	}
}

void ScreenSelectMusic::TweenCoursePartsWhenSectionChanges()
{
	m_CourseContentsFrame.Command( m_sCourseContentsSectionCommand );
}

void ScreenSelectMusic::SkipSongPartTweens()
{
	m_GrooveRadar.FinishTweening();
	m_GrooveGraph.FinishTweening();
	if( m_bShowDifficultyList )
		m_DifficultyList.FinishTweening();

	FOREACH_PlayerNumber( p )
	{
		if( !GAMESTATE->IsHumanPlayer(p) )
			continue;	// skip

		m_sprDifficultyFrame[p].FinishTweening();
		m_sprMeterFrame[p].FinishTweening();
		m_DifficultyIcon[p].FinishTweening();
		m_AutoGenIcon[p].FinishTweening();
	}
}

void ScreenSelectMusic::SkipCoursePartTweens()
{
	m_CourseContentsFrame.FinishTweening();
}

void ScreenSelectMusic::TweenOnScreen()
{
	TweenSongPartsOnScreen( true );
	TweenCoursePartsOnScreen( true );

	switch( GAMESTATE->m_SortOrder )
	{
	case SORT_ALL_COURSES:
	case SORT_NONSTOP_COURSES:
	case SORT_ONI_COURSES:
	case SORT_ENDLESS_COURSES:
		TweenSongPartsOffScreen( false );
		SkipSongPartTweens();
		break;
	default:
		TweenCoursePartsOffScreen( false );
		SkipCoursePartTweens();
		break;
	}

	FOREACH_HumanPlayer( p )
		ON_COMMAND( m_sprCharacterIcon[p] );

	ON_COMMAND( m_MusicWheelUnder );
	m_MusicWheel.TweenOnScreen();
	ON_COMMAND( m_MusicWheel );

	if( m_bShowBanners )
	{
		ON_COMMAND( m_sprBannerMask );

		if( m_bBannerWheel )
			ON_COMMAND( m_TextBanner );
		else
			ON_COMMAND( m_Banner );

		ON_COMMAND( m_sprBannerFrame );
	}

	if( m_bShowDiscs )
	{
		ON_COMMAND( m_sprDiscMask );
		ON_COMMAND( m_Disc );
		ON_COMMAND( m_sprDiscFrame );
	}

	if( m_bShowBackgrounds )
	{
		ON_COMMAND( m_sprBackgroundMask );
		ON_COMMAND( m_Background );
		ON_COMMAND( m_sprBackgroundFrame );
	}

	if( m_bShowPreviews )
	{
		ON_COMMAND( m_sprPreviewMask );

		if( m_bPreviewInSprite )
			ON_COMMAND( m_sprPreview );
		else
			ON_COMMAND( m_Preview );

		ON_COMMAND( m_sprPreviewFrame );
	}

	if( m_bShowCDTitles )
	{
		ON_COMMAND( m_sprCDTitleFront );
		ON_COMMAND( m_sprCDTitleBack );
	}

	ON_COMMAND( m_sprExplanation );
	ON_COMMAND( m_BPMDisplay );
	ON_COMMAND( m_DifficultyDisplay );
	ON_COMMAND( m_sprStage );
	ON_COMMAND( m_GrooveRadar );
	ON_COMMAND( m_GrooveGraph );
	ON_COMMAND( m_textSongOptions );
	ON_COMMAND( m_textCurMode );
	ON_COMMAND( m_textNumSongs );
	ON_COMMAND( m_textGroupName );
	ON_COMMAND( m_textTotalTime );

	ON_COMMAND( m_Artist );
	ON_COMMAND( m_MachineRank );

	ON_COMMAND( m_MusicSortDisplay );
	ON_COMMAND( m_sprBalloon );
	ON_COMMAND( m_sprCourseHasMods );

	ON_COMMAND( m_Overlay );

	FOREACH_HumanPlayer( p )
	{
		ON_COMMAND( m_OptionIconRow[p] );
		if( m_bShowPanes )
			ON_COMMAND( m_PaneDisplay[p] );
		ON_COMMAND( m_DifficultyMeter[p] );
		ON_COMMAND( m_sprHighScoreFrame[p] );
		ON_COMMAND( m_textHighScore[p] );
	}

	if( GAMESTATE->IsExtraStage() || GAMESTATE->IsExtraStage2() )
		m_textSongOptions.Command( m_sSongOptionsExtraCommand );
}

void ScreenSelectMusic::TweenOffScreen()
{
	switch( GAMESTATE->m_SortOrder )
	{
	case SORT_ALL_COURSES:
	case SORT_NONSTOP_COURSES:
	case SORT_ONI_COURSES:
	case SORT_ENDLESS_COURSES:
		TweenCoursePartsOffScreen( true );
		break;
	default:
		TweenSongPartsOffScreen( true );
		break;
	}

	FOREACH_HumanPlayer( p )
		OFF_COMMAND( m_sprCharacterIcon[p] );

	OFF_COMMAND( m_MusicWheelUnder );
	m_MusicWheel.TweenOffScreen();
	OFF_COMMAND( m_MusicWheel );

	if( m_bShowBanners )
	{
		OFF_COMMAND( m_sprBannerMask );

		if( m_bBannerWheel )
			OFF_COMMAND( m_TextBanner );
		else
			OFF_COMMAND( m_Banner );

		OFF_COMMAND( m_sprBannerFrame );
	}

	if( m_bShowDiscs )
	{
		OFF_COMMAND( m_sprDiscMask );
		OFF_COMMAND( m_Disc );
		OFF_COMMAND( m_sprDiscFrame );
	}

	if( m_bShowBackgrounds )
	{
		OFF_COMMAND( m_sprBackgroundMask );
		OFF_COMMAND( m_Background );
		OFF_COMMAND( m_sprBackgroundFrame );
	}

	if( m_bShowPreviews )
	{
		OFF_COMMAND( m_sprPreviewMask );

		if( m_bPreviewInSprite )
			OFF_COMMAND( m_sprPreview );
		else
			OFF_COMMAND( m_Preview );

		OFF_COMMAND( m_sprPreviewFrame );
	}

	if( m_bShowCDTitles )
	{
		OFF_COMMAND( m_sprCDTitleFront );
		OFF_COMMAND( m_sprCDTitleBack );
	}

	OFF_COMMAND( m_sprExplanation );
	OFF_COMMAND( m_BPMDisplay );
	OFF_COMMAND( m_DifficultyDisplay );
	OFF_COMMAND( m_sprStage );
	OFF_COMMAND( m_GrooveRadar );
	OFF_COMMAND( m_GrooveGraph );
	OFF_COMMAND( m_textSongOptions );
	OFF_COMMAND( m_textCurMode );
	OFF_COMMAND( m_textNumSongs );
	OFF_COMMAND( m_textGroupName );
	OFF_COMMAND( m_textTotalTime );

	OFF_COMMAND( m_Artist );
	OFF_COMMAND( m_MachineRank );

	OFF_COMMAND( m_MusicSortDisplay );
	OFF_COMMAND( m_sprBalloon );
	OFF_COMMAND( m_sprCourseHasMods );

	OFF_COMMAND( m_Overlay );

	FOREACH_HumanPlayer( p )
	{
		OFF_COMMAND( m_OptionIconRow[p] );
		if( m_bShowPanes )
			OFF_COMMAND( m_PaneDisplay[p] );
		OFF_COMMAND( m_DifficultyMeter[p] );
		OFF_COMMAND( m_sprHighScoreFrame[p] );
		OFF_COMMAND( m_textHighScore[p] );
	}
}


/* This hides elements that are only relevant when displaying a single song,
 * and shows elements for course display.  XXX: Allow different tween commands. */
void ScreenSelectMusic::SwitchDisplayMode( DisplayMode dm )
{
	if( m_DisplayMode == dm )
		return;

	// tween off
	switch( m_DisplayMode )
	{
	case DISPLAY_SONGS:
		TweenSongPartsOffScreen( false );
		break;
	case DISPLAY_COURSES:
		TweenCoursePartsOffScreen( false );
		break;
	case DISPLAY_MODES:
		break;
	}

	// tween on
	m_DisplayMode = dm;
	switch( m_DisplayMode )
	{
	case DISPLAY_SONGS:
		TweenSongPartsOnScreen( false );
		break;
	case DISPLAY_COURSES:
		TweenCoursePartsOnScreen( false );
		break;
	case DISPLAY_MODES:
		break;
	}
}

void ScreenSelectMusic::TweenScoreOnAndOffAfterChangeSort()
{
	FOREACH_PlayerNumber( p )
	{
		if( !GAMESTATE->IsHumanPlayer(p) )
			continue;	// skip
		m_textHighScore[p].Command( m_sScoreSortChangeCommand[p] );
		m_sprHighScoreFrame[p].Command( m_sScoreFrameSortChangeCommand[p] );
	}

	switch( GAMESTATE->m_SortOrder )
	{
	case SORT_ALL_COURSES:
	case SORT_NONSTOP_COURSES:
	case SORT_ONI_COURSES:
	case SORT_ENDLESS_COURSES:
		SwitchDisplayMode( DISPLAY_COURSES );
		break;
	case SORT_SORT_MENU:
	case SORT_MODE_MENU:
		SwitchDisplayMode( DISPLAY_MODES );
		break;
	default:
		SwitchDisplayMode( DISPLAY_SONGS );
		break;
	}

	m_bSettledTweenWaiting = true;
}

void ScreenSelectMusic::CheckBackgroundRequests()
{
	if( !m_Out.IsTransitioning() && !m_In.IsTransitioning() )
	{
		TweenWhenSectionChanges();

		if( !m_MusicWheel.IsSettled( true ) )
		{
			if( m_bShowPreviews && !m_bPreviewInSprite && m_bPreviewLoadsCachedBG )
				m_Preview.LoadFromCachedBackground( m_sBackgroundPath, true );
			else if( m_bShowPreviews )
			{
				if( m_bPreviewInSprite )
					m_sprPreview.Load( m_sFallbackPreviewPath );
				else
					m_Preview.LoadFallback( true );
			}

			TweenWhileMoving();
		}
		else if( m_MusicWheel.IsSettled( true ) )
			TweenWhileSettled();
	}

	/* Loading the rest can cause small skips, so don't do it until the wheel settles.
	 * Do load if we're transitioning out, though, so we don't miss starting the music
	 * for the options screen if a song is selected quickly. */
	if( !m_MusicWheel.IsSettled() && !m_Out.IsTransitioning() )
		return;

	if( m_bBannerWheel && m_bShowBanners )
	{
		m_pSong = m_MusicWheel.GetSelectedSong();
		m_TextBanner.LoadFromSong( m_pSong );
	}

	if( m_bBannerWaiting && m_bShowBanners )
	{
		if( m_bBannerWheel )
			m_bBannerWaiting = false;
		else if( PREFSMAN->m_bBannerUsesCacheOnly )
		{
			m_bBannerWaiting = false;
			m_Banner.LoadFromCachedBanner( m_sBannerPath, false );
		}
		else
		{
			CString sPath;
			if( m_tStartedLoadingAt.Ago() >= m_fHQBanner && m_BackgroundLoader.IsCacheFileFinished(m_sBannerPath, sPath) )
			{
				m_bBannerWaiting = false;
				RageTimer foo;

				TEXTUREMAN->DisableOddDimensionWarning();
				m_Banner.Load( sPath, false );
				TEXTUREMAN->EnableOddDimensionWarning();

				m_BackgroundLoader.FinishedWithCachedFile( m_sBannerPath );
			}
			else
				return;
		}
	}

	if( m_bDiscWaiting && m_bShowDiscs )
	{
		if( PREFSMAN->m_bDiscUsesCacheOnly )
		{
			m_bDiscWaiting = false;
			m_Disc.LoadFromCachedBanner( m_sDiscPath, true );
		}
		else
		{
			CString sPath;
			if( m_tStartedLoadingAt.Ago() >= m_fHQBanner && m_BackgroundLoader.IsCacheFileFinished(m_sDiscPath, sPath) )
			{
				m_bDiscWaiting = false;
				RageTimer foo;

				TEXTUREMAN->DisableOddDimensionWarning();
				m_Disc.Load( sPath, true );
				TEXTUREMAN->EnableOddDimensionWarning();

				m_BackgroundLoader.FinishedWithCachedFile( m_sDiscPath );
			}
			else
				return;
		}
	}

	if( m_bBackgroundWaiting && m_bShowBackgrounds )
	{
		if( PREFSMAN->m_bBackgroundUsesCacheOnly )
		{
			m_bBackgroundWaiting = false;
			m_Background.LoadFromCachedBackground( m_sBackgroundPath, false );
		}
		else
		{
			CString sPath;
			if( m_tStartedLoadingAt.Ago() >= m_fHQBackground && m_BackgroundLoader.IsCacheFileFinished(m_sBackgroundPath, sPath) )
			{
				m_bBackgroundWaiting = false;
				RageTimer foo;

				TEXTUREMAN->DisableOddDimensionWarning();
				m_Background.Load( sPath, false );
				TEXTUREMAN->EnableOddDimensionWarning();

				m_BackgroundLoader.FinishedWithCachedFile( m_sBackgroundPath );
			}
			else
				return;
		}
	}

	// Start the music, if we haven't yet.
	if( m_bSampleMusicWaiting )
	{
		// Don't start the music sample when moving fast.
		if( m_tStartedLoadingAt.Ago() >= m_fSampleMusicDelay )
		{
			m_bSampleMusicWaiting = false;

			// Always play music in course mode
			if( GAMESTATE->IsCourseMode() )
			{
				SOUND->PlayMusic( m_sSampleMusicToPlay, m_pSampleMusicTimingData, true, m_fSampleStartSeconds,
						m_fSampleLengthSeconds, 1.5f, m_bAlignMusicBeats ); // fade out for 1.5 seconds
			}
			// No music if true
			else if( m_iPreviewMusicMode != 2 )
			{
				// We only want the main theme music if we're in this preview music mode
				if( m_iPreviewMusicMode == 4 )
					m_sSampleMusicToPlay = m_sSectionMusic;

				if( m_bThemeMusic || m_iPreviewMusicMode == 4 )
				{
					SOUND->PlayMusic( m_sSampleMusicToPlay, m_pSampleMusicTimingData, true, m_fSampleStartSeconds,
						m_fSampleLengthSeconds, 1.5f, m_bAlignMusicBeats ); // fade out for 1.5 seconds
				}
				else if( m_iPreviewMusicMode != 1 || ( m_iPreviewMusicMode == 1 && m_iSelectionState >= SELECT_SONG ) )
				{
					SOUND->PlayMusic( m_sSampleMusicToPlay, m_pSampleMusicTimingData, m_bSampleMusicLoops, m_fSampleStartSeconds,
											m_fSampleLengthSeconds, 1.5f, m_bAlignMusicBeats ); // fade out for 1.5 seconds
				}
			}
		}
		else
			return;
	}

	if( m_bPreviewWaiting && m_bShowPreviews )
	{
		CString sPath;
		if( m_BackgroundLoader.IsCacheFileFinished(m_sPreviewPath, sPath) )
		{
			if( m_bPreviewInSprite )
			{
				if( m_tStartedLoadingAt.Ago() >= m_fSampleMusicDelay )
				{
					m_bPreviewWaiting = false;

					if( sPath.empty() || !IsAFile(sPath) )
						sPath = m_sFallbackPreviewPath;

					if( !sPath.empty() )
					{
						TEXTUREMAN->DisableOddDimensionWarning();
						m_sprPreview.Load( sPath );
						TEXTUREMAN->EnableOddDimensionWarning();
					}

					m_BackgroundLoader.FinishedWithCachedFile( m_sPreviewPath );
				}
			}
			else
			{
				if( PREFSMAN->m_bPreviewUsesCacheOnly )
				{
					m_bPreviewWaiting = false;
					m_Preview.LoadFromCachedBackground( m_sPreviewPath, true );
				}
				else if( m_tStartedLoadingAt.Ago() >= m_fHQBackground )
				{
					m_bPreviewWaiting = false;

					RageTimer foo;

					TEXTUREMAN->DisableOddDimensionWarning();
					m_Preview.Load( sPath, true );
					TEXTUREMAN->EnableOddDimensionWarning();

					m_BackgroundLoader.FinishedWithCachedFile( m_sPreviewPath );
				}
				else
					return;
			}
		}
		else
			return;
	}

	if( m_bCDTitleWaiting && m_bShowCDTitles )
	{
		// The CDTitle is normally very small, so we don't bother waiting to display it.
		CString sPath;
		if( m_BackgroundLoader.IsCacheFileFinished(m_sCDTitlePath, sPath) )
		{
			m_bCDTitleWaiting = false;

			if( sPath.empty() || !IsAFile(sPath) )
				sPath = m_bLoadFallbackCDTitle ? m_sFallbackCDTitlePath : "";

			if( !sPath.empty() )
			{
				TEXTUREMAN->DisableOddDimensionWarning();

				m_sprCDTitleFront.Load( sPath );
				m_sprCDTitleBack.Load( sPath );

				TEXTUREMAN->EnableOddDimensionWarning();
			}

			m_BackgroundLoader.FinishedWithCachedFile( m_sCDTitlePath );
		}
		else
			return;
	}
}

void ScreenSelectMusic::Update( float fDeltaTime )
{
	Screen::Update( fDeltaTime );
	m_bgOptionsOut.Update( fDeltaTime );
	m_bgNoOptionsOut.Update( fDeltaTime );
	m_sprOptionsMessage.Update( fDeltaTime );

	CheckBackgroundRequests();
}

void ScreenSelectMusic::Input( const DeviceInput& DeviceI, InputEventType type, const GameInput &GameI, const MenuInput &MenuI, const StyleInput &StyleI )
{
//	LOG->Trace( "ScreenSelectMusic::Input()" );

	// debugging?
	// I just like being able to see untransliterated titles occasionally.
	if( DeviceI.device == DEVICE_KEYBOARD && DeviceI.button == KEY_F9 )
	{
		if( type != IET_FIRST_PRESS ) return;
		PREFSMAN->m_bShowNative ^= 1;
		m_MusicWheel.RebuildMusicWheelItems();
		m_CourseContentsFrame.SetFromGameState();
		return;
	}

	if( !GameI.IsValid() )		return;		// don't care

	/* XXX: What's the difference between this and StyleI.player? */
	/* StyleI won't be valid if it's a menu button that's pressed.
	 * There's got to be a better way of doing this.  -Chris */
	PlayerNumber pn = GAMESTATE->GetCurrentStyle()->ControllerToPlayerNumber( GameI.controller );
	if( !GAMESTATE->IsHumanPlayer(pn) )
		return;

	if( GAMESTATE->GetCurrentStyle()->m_StyleType == Style::ONE_PLAYER_TWO_CREDITS && m_bNo2PInput )
	{
		if( pn != (PlayerNumber)GameI.controller )
			return;
	}

	if( m_bMadeChoice && MenuI.IsValid() && MenuI.button == MENU_BUTTON_START && type != IET_RELEASE &&
		type != IET_LEVEL_CHANGED && IsTransitioning() )
	{
		// Allow if we're picking mods for extra stage, or if we're not at an extra stage
		bool bExtra1 = GAMESTATE->IsExtraStage(),
			bExtra2 = GAMESTATE->IsExtraStage2();
		if( !bExtra1 && !bExtra2 ||
			bExtra1 && PREFSMAN->m_bPickModsForExtraStage ||
			bExtra2 && PREFSMAN->m_bPickModsForExtraStage2
			)
		{
			if(m_bGoToOptions) return; // got it already
			if(!m_bAllowOptionsMenu) return; // not allowed

			if( !m_bAllowOptionsMenuRepeat && (type == IET_SLOW_REPEAT || type == IET_FAST_REPEAT ))
				return; // not allowed yet

			m_bGoToOptions = true;
			m_sprOptionsMessage.SetState( 1 );
			SCREENMAN->PlayStartSound();
			return;
		}
	}

	if( IsTransitioning() )
		return;		// ignore

	if( m_bMadeChoice )		return;		// ignore

	if( MenuI.button == MENU_BUTTON_RIGHT || MenuI.button == MENU_BUTTON_LEFT )
	{
		// If we're rouletting, hands off.
		if(m_MusicWheel.IsRouletting())
			return;

		// TRICKY:  There's lots of weirdness that can happen here when tapping
		// Left and Right quickly, like when changing sort.
		bool bLeftPressed = INPUTMAPPER->IsButtonDown( MenuInput(MenuI.player, MENU_BUTTON_LEFT) );
		bool bRightPressed = INPUTMAPPER->IsButtonDown( MenuInput(MenuI.player, MENU_BUTTON_RIGHT) );
		bool bLeftAndRightPressed = bLeftPressed && bRightPressed;
		bool bLeftOrRightPressed = bLeftPressed || bRightPressed;

		switch( type )
		{
		case IET_RELEASE:
			// Normal Speed
			m_MusicWheel.SetSpeedMultiplier( 1.f );

			// when a key is released, stop moving the wheel
			if( !bLeftOrRightPressed )
				m_MusicWheel.Move( 0 );

			// Reset the repeat timer when a key is released.
			// This fixes jumping when you release Left and Right at the same
			// time (e.g. after tapping Left+Right to change sort).
			INPUTMAPPER->ResetKeyRepeat( MenuInput(MenuI.player, MENU_BUTTON_LEFT) );
			INPUTMAPPER->ResetKeyRepeat( MenuInput(MenuI.player, MENU_BUTTON_RIGHT) );
			break;
		case IET_FIRST_PRESS:
			if( m_iSelectionState == SELECT_SONG ||
				( m_iSelectionState == SELECT_PREVIEW &&
					( m_iPreviewMusicMode == 1 || m_iPreviewMusicMode == 3 && !m_bTwoPartSelection )
				)
			)
			{
				// Normal Speed
				m_MusicWheel.SetSpeedMultiplier( 1.f );

				if( !m_bMovingCommandRepeats && m_MusicWheel.IsSettled( true ) )
					m_bMovingTweenWaiting = true;

				if( MenuI.button == MENU_BUTTON_RIGHT )
					m_MusicWheel.Move( +1 );
				else if( MenuI.button == MENU_BUTTON_LEFT )
					m_MusicWheel.Move( -1 );

				// Remember to reset this if we switch the song!
				if( m_iPreviewMusicMode == 1 )
					m_iSelectionState = SELECT_PREVIEW;
				else
					m_iSelectionState = SELECT_SONG;
			}
			else if( m_iSelectionState == SELECT_STEPS || m_iSelectionState == SELECT_PREVIEW && m_iPreviewMusicMode == 3 )
			{
				if( MenuI.button == MENU_BUTTON_RIGHT  )
				{
					if( PREFSMAN->m_bLockExtraStageDiff && GAMESTATE->IsExtraStage() || PREFSMAN->m_bLockExtraStage2Diff && GAMESTATE->IsExtraStage2() )
						m_soundLocked.Play();
					else
						ChangeDifficulty( pn, m_bReverseDifficulty ? -1 : +1 );
				}
				else if( MenuI.button == MENU_BUTTON_LEFT )
				{
					if( PREFSMAN->m_bLockExtraStageDiff && GAMESTATE->IsExtraStage() || PREFSMAN->m_bLockExtraStage2Diff && GAMESTATE->IsExtraStage2() )
						m_soundLocked.Play();
					else
						ChangeDifficulty( pn, m_bReverseDifficulty ? +1 : -1 );
				}

				// Remember to reset this if we switch the steps!
				if( m_iPreviewMusicMode == 3 )
					m_iSelectionState = SELECT_STEPS;
			}

			// The wheel moves faster than one item between FIRST_PRESS
			// and SLOW_REPEAT.  Stop the wheel immediately after moving one
			// item if both Left and Right are held.  This way, we won't move
			// another item
			if( bLeftAndRightPressed )
				m_MusicWheel.Move( 0 );

			break;
		case IET_SLOW_REPEAT:
		case IET_FAST_REPEAT:
			// We need to handle the repeat events to start the wheel spinning again
			// when Left and Right are being held, then one is released.
			if( bLeftAndRightPressed )
			{
				// Don't spin if holding both buttons
				m_MusicWheel.Move( 0 );
			}
			else
			{
				if( m_bResetKeyTimers )
				{
					INPUTMAPPER->ResetKeyRepeat( MenuInput(MenuI.player, MENU_BUTTON_LEFT) );
					INPUTMAPPER->ResetKeyRepeat( MenuInput(MenuI.player, MENU_BUTTON_RIGHT) );
					m_bResetKeyTimers = false;
				}

				float fSpeed = abs(
					INPUTMAPPER->GetSecsHeld( MenuInput(MenuI.player, MENU_BUTTON_LEFT) ) -
					INPUTMAPPER->GetSecsHeld( MenuInput(MenuI.player, MENU_BUTTON_RIGHT) )
					);

				if( type == IET_SLOW_REPEAT )
					fSpeed *= .5;
				else
					fSpeed *= .75;

				// Less than 1 not allowed...
				fSpeed = max( 1.f, fSpeed );

				// Fast Wheel
				m_MusicWheel.SetSpeedMultiplier( fSpeed );

				if( m_iSelectionState == SELECT_SONG ||
					( m_iSelectionState == SELECT_PREVIEW &&
						( m_iPreviewMusicMode == 1 || m_iPreviewMusicMode == 3 && !m_bTwoPartSelection )
					)
				)
				{
					if( MenuI.button == MENU_BUTTON_RIGHT )
						m_MusicWheel.Move( +1 );
					else if( MenuI.button == MENU_BUTTON_LEFT )
						m_MusicWheel.Move( -1 );

					// Remember to reset this if we switch the song!
					if( m_iPreviewMusicMode == 1 )
						m_iSelectionState = SELECT_PREVIEW;
					else
						m_iSelectionState = SELECT_SONG;
				}
				else if( m_iSelectionState == SELECT_STEPS || m_iSelectionState == SELECT_PREVIEW && m_iPreviewMusicMode == 3 )
				{
					if( MenuI.button == MENU_BUTTON_RIGHT  )
					{
						if( PREFSMAN->m_bLockExtraStageDiff && GAMESTATE->IsExtraStage() || PREFSMAN->m_bLockExtraStage2Diff && GAMESTATE->IsExtraStage2() )
							m_soundLocked.Play();
						else
							ChangeDifficulty( pn, m_bReverseDifficulty ? -1 : +1 );
					}
					else if( MenuI.button == MENU_BUTTON_LEFT )
					{
						if( PREFSMAN->m_bLockExtraStageDiff && GAMESTATE->IsExtraStage() || PREFSMAN->m_bLockExtraStage2Diff && GAMESTATE->IsExtraStage2() )
							m_soundLocked.Play();
						else
							ChangeDifficulty( pn, m_bReverseDifficulty ? +1 : -1 );
					}

					// Remember to reset this if we switch the steps!
					if( m_iPreviewMusicMode == 3 )
						m_iSelectionState = SELECT_STEPS;
				}
			}
			break;
		}
	}

	if( MenuI.button == MENU_BUTTON_UP || MenuI.button == MENU_BUTTON_DOWN )
	{
		// If we're rouletting, hands off.
		if(m_MusicWheel.IsRouletting())
			return;

		// TRICKY:  There's lots of weirdness that can happen here when tapping
		// Up and Down quickly.
		bool bUpPressed = INPUTMAPPER->IsButtonDown( MenuInput(MenuI.player, MENU_BUTTON_UP) );
		bool bDownPressed = INPUTMAPPER->IsButtonDown( MenuInput(MenuI.player, MENU_BUTTON_DOWN) );
		bool bUpAndDownPressed = bUpPressed && bDownPressed;

		switch( type )
		{
		case IET_RELEASE:
			// Reset the repeat timer when a key is released.
			// This fixes jumping when you release Up and Down at the same time.
			INPUTMAPPER->ResetKeyRepeat( MenuInput(MenuI.player, MENU_BUTTON_UP) );
			INPUTMAPPER->ResetKeyRepeat( MenuInput(MenuI.player, MENU_BUTTON_DOWN) );
			break;
		case IET_FIRST_PRESS:
		case IET_SLOW_REPEAT:
		case IET_FAST_REPEAT:
			if( bUpAndDownPressed )
				break;

			switch( m_iSelectionState )
			{
			case SELECT_PREVIEW:
			case SELECT_SONG:
				if( bDownPressed )
				{
					if( m_bMenuDownDifficulty )
					{
						if( PREFSMAN->m_bLockExtraStageDiff && GAMESTATE->IsExtraStage() || PREFSMAN->m_bLockExtraStage2Diff && GAMESTATE->IsExtraStage2() )
							m_soundLocked.Play();
						else
							ChangeDifficulty( pn, m_bReverseDifficulty ? -1 : +1 );
					}

					if( m_bMenuDownGroup )
					{
						if( !PREFSMAN->m_bPickExtraStage && GAMESTATE->IsExtraStage() || !PREFSMAN->m_bPickExtraStage2 && GAMESTATE->IsExtraStage2() )
							m_soundLocked.Play();
						else
							ChangeGroup( m_bReverseGroup ? -1 : +1 );
					}

					// Remember to reset this if we switch the difficulty!
					if( ( m_bMenuDownDifficulty || m_bMenuDownGroup ) && m_iPreviewMusicMode == 3 )
						m_iSelectionState = SELECT_SONG;
				}
				if( bUpPressed )
				{
					if( m_bMenuUpDifficulty )
					{
						if( PREFSMAN->m_bLockExtraStageDiff && GAMESTATE->IsExtraStage() || PREFSMAN->m_bLockExtraStage2Diff && GAMESTATE->IsExtraStage2() )
							m_soundLocked.Play();
						else
							ChangeDifficulty( pn, m_bReverseDifficulty ? +1 : -1 );
					}

					if( m_bMenuUpGroup )
					{
						if( !PREFSMAN->m_bPickExtraStage && GAMESTATE->IsExtraStage() || !PREFSMAN->m_bPickExtraStage2 && GAMESTATE->IsExtraStage2() )
							m_soundLocked.Play();
						else
							ChangeGroup( m_bReverseGroup ? +1 : -1 );
					}

					// Remember to reset this if we switch the difficulty!
					if( ( m_bMenuUpDifficulty || m_bMenuUpGroup ) && m_iPreviewMusicMode == 3 )
						m_iSelectionState = SELECT_SONG;
				}
				break;
			case SELECT_STEPS:
				if( bDownPressed && !m_bReverseDifficulty || bUpPressed && m_bReverseDifficulty )
					m_soundDifficultyHarder.Play();
				else if( bDownPressed && m_bReverseDifficulty || bUpPressed && !m_bReverseDifficulty )
					m_soundDifficultyEasier.Play();

				m_iSelectionState = SELECT_SONG;
				break;
			default:
				break;
			}
			break;
		}
	}

	// TRICKY:  Do default processing of MenuLeft and MenuRight before detecting
	// codes.  Do default processing of Start AFTER detecting codes.  This gives us a
	// change to return if Start is part of a code because we don't want to process
	// Start as "move to the next screen" if it was just part of a code.
	switch( MenuI.button )
	{
	case MENU_BUTTON_UP:	this->MenuUp( MenuI.player, type );		break;
	case MENU_BUTTON_DOWN:	this->MenuDown( MenuI.player, type );	break;
	case MENU_BUTTON_LEFT:	this->MenuLeft( MenuI.player, type );	break;
	case MENU_BUTTON_RIGHT:	this->MenuRight( MenuI.player, type );	break;
	case MENU_BUTTON_BACK:
		// Don't make the user hold the back button if they're pressing escape and escape is the back button.
		if( DeviceI.device == DEVICE_KEYBOARD && DeviceI.button == KEY_ESC )
			this->MenuBack( MenuI.player );
		else
			Screen::MenuBack( MenuI.player, type );
		break;
	// Do the default handler for Start after detecting codes.
//	case MENU_BUTTON_START:	this->MenuStart( MenuI.player, type );	break;
#if defined( WITH_COIN_MODE )
	case MENU_BUTTON_COIN:	this->MenuCoin( MenuI.player, type );	break;
#endif
	}

	if( type == IET_FIRST_PRESS )
	{
		// No need for difficulty checking here if we're using two part selection
		if( !m_bTwoPartSelection )
		{
			if( !m_bMenuDownDifficulty )
			{
				if( CodeDetector::EnteredHarderDifficulty(GameI.controller) )
				{
					if( PREFSMAN->m_bLockExtraStageDiff && GAMESTATE->IsExtraStage() || PREFSMAN->m_bLockExtraStage2Diff && GAMESTATE->IsExtraStage2() )
						m_soundLocked.Play();
					else
						ChangeDifficulty( pn, m_bReverseDifficulty ? -1 : +1 );
					return;
				}
			}
			if( !m_bMenuUpDifficulty )
			{
				if( CodeDetector::EnteredEasierDifficulty(GameI.controller) )
				{
					if( PREFSMAN->m_bLockExtraStageDiff && GAMESTATE->IsExtraStage() || PREFSMAN->m_bLockExtraStage2Diff && GAMESTATE->IsExtraStage2() )
						m_soundLocked.Play();
					else
						ChangeDifficulty( pn, m_bReverseDifficulty ? +1 : -1 );
					return;
				}
			}
		}

		if( !m_bMenuDownGroup )
		{
			if( CodeDetector::EnteredNextBannerGroup(GameI.controller) )
			{
				if( !PREFSMAN->m_bPickExtraStage && GAMESTATE->IsExtraStage() || !PREFSMAN->m_bPickExtraStage2 && GAMESTATE->IsExtraStage2() )
					m_soundLocked.Play();
				else
					ChangeGroup( m_bReverseGroup ? -1 : +1 );
				return;
			}
		}
		if( !m_bMenuUpGroup )
		{
			if( CodeDetector::EnteredPreviousBannerGroup(GameI.controller) )
			{
				if( !PREFSMAN->m_bPickExtraStage && GAMESTATE->IsExtraStage() || !PREFSMAN->m_bPickExtraStage2 && GAMESTATE->IsExtraStage2() )
					m_soundLocked.Play();
				else
					ChangeGroup( m_bReverseGroup ? +1 : -1 );
				return;
			}
		}

		if( CodeDetector::EnteredSortMenu(GameI.controller) )
		{
			/* Ignore the SortMenu when in course mode.  However, still check for the code, so
			 * if people try pressing left+right+start in course mode, we don't pick the selected
			 * course on them. */
			if( GAMESTATE->IsCourseMode() )
				; /* nothing */
			else if( !PREFSMAN->m_bPickExtraStage && GAMESTATE->IsExtraStage() || !PREFSMAN->m_bPickExtraStage2 && GAMESTATE->IsExtraStage2() )
				m_soundLocked.Play();
			else
				m_MusicWheel.ChangeSort( SORT_SORT_MENU );
			return;
		}

		if( CodeDetector::EnteredModeMenu(GameI.controller) )
		{
			if( !PREFSMAN->m_bPickExtraStage && GAMESTATE->IsExtraStage() || !PREFSMAN->m_bPickExtraStage2 && GAMESTATE->IsExtraStage2() )
				m_soundLocked.Play();
			else
				m_MusicWheel.ChangeSort( SORT_MODE_MENU );
			return;
		}

		if( CodeDetector::EnteredNextSort(GameI.controller) )
		{
			if( !PREFSMAN->m_bPickExtraStage && GAMESTATE->IsExtraStage() || !PREFSMAN->m_bPickExtraStage2 && GAMESTATE->IsExtraStage2() )
				m_soundLocked.Play();
			else
				m_MusicWheel.NextSort();
			return;
		}

		if( CodeDetector::DetectAndAdjustMusicOptions(GameI.controller) )
		{
			bool bExtra1 = GAMESTATE->IsExtraStage(),
				bExtra2 = GAMESTATE->IsExtraStage2();

			if( !bExtra1 && !bExtra2 ||
				bExtra1 && PREFSMAN->m_bPickModsForExtraStage ||
				bExtra2 && PREFSMAN->m_bPickModsForExtraStage2
				)
			{
				m_soundOptionsChange.Play();
				UpdateOptionsDisplays();
				return;
			}
		}
	}

	switch( MenuI.button )
	{
	case MENU_BUTTON_START:	Screen::MenuStart( MenuI.player, type );	break;
	}
}

void ScreenSelectMusic::ChangeDifficulty( PlayerNumber pn, int dir )
{
	LOG->Trace( "ScreenSelectMusic::ChangeDifficulty( %d, %d )", pn, dir );

	ASSERT( GAMESTATE->IsHumanPlayer(pn) );

	switch( m_MusicWheel.GetSelectedType() )
	{
	case TYPE_SONG:
	case TYPE_PORTAL:
		{
			m_iSelection[pn] += dir;

			// If we're using the mode switching abilities, we apply it here
			// No modeswitcher in battle or rave, though.

			if( m_bUseModeSwitcher )
			{
				if( GAMESTATE->m_PlayMode == PLAY_MODE_BATTLE || GAMESTATE->m_PlayMode == PLAY_MODE_RAVE )
					m_bUseModeSwitcher = false;

				// Disable the modeswitcher in extra stage if we're not autogenning steps!
				// TODO - Aldo_MX: Ver como afecta
				//else if( GAMESTATE->IsExtraStage() || GAMESTATE->IsExtraStage2() )
				//{
				//	if( !PREFSMAN->m_bAutogenSteps )
				//		m_bUseModeSwitcher = false;
				//}
			}

			if( m_bUseModeSwitcher )
			{
				m_MusicWheel.ModeSwitch( true );

				// Get the song information
				Song* pSong = NULL;

				if( GAMESTATE->IsExtraStage() || GAMESTATE->IsExtraStage2() )
				{
					// Junk variables so we can call the function properly
					Steps* pSteps;
					PlayerOptions po;
					SongOptions so;

					SONGMAN->GetExtraStageInfo( GAMESTATE->IsExtraStage2(), GAMESTATE->GetCurrentStyle(), pSong, pSteps, po, so );
				}
				else
					pSong = m_MusicWheel.GetSelectedSong();

				GAMESTATE->m_pCurSong = pSong;

				if( pSong )
					GAMESTATE->m_pPreferredSong = pSong;

				if( m_bModeSwitcherLoadsAll )
					wrap( m_iSelection[pn], m_vpSteps.size() );
				else
				{
					// Make a list of all styles for the current Game.
					if(	(dir > 0 && m_iSelection[pn] > (int)(m_vpSteps.size()-1)) ||
						(dir < 0 && m_iSelection[pn] < 0) )
					{
						m_vPossibleStyles.clear();
						GAMEMAN->GetStylesForGame( GAMESTATE->m_pCurGame, m_vPossibleStyles );
						ASSERT( !m_vPossibleStyles.empty() );

						vector<const Style*>::const_iterator iter = find(m_vPossibleStyles.begin(), m_vPossibleStyles.end(), GAMESTATE->GetCurrentStyle() );
						int index = iter - m_vPossibleStyles.begin();

						ChangeDifficultyModeSwitch( pn, pSong, dir, index, GAMESTATE->GetNumPlayersEnabled() );
					}
				}

				m_MusicWheel.ModeSwitch( false );
			}
			// Not using Mode Switcher; so clamp to restrict changing
			else
			{
				if( CLAMP(m_iSelection[pn],0,m_vpSteps.size()-1) )
					return;
			}

			// the user explicity switched difficulties.  Update the preferred difficulty
			GAMESTATE->ChangePreferredDifficulty( pn, m_vpSteps[ m_iSelection[pn] ]->GetDifficulty() );

			if( dir < 0 )
				m_soundDifficultyEasier.Play();
			else
				m_soundDifficultyHarder.Play();

			FOREACH_HumanPlayer( p )
			{
				if( pn == p || GAMESTATE->DifficultiesLocked() )
				{
					m_iSelection[p] = m_iSelection[pn];
					AfterStepsChange( p );
				}
			}

			if( m_bUseModeSwitcher )
				m_LastUsedStepsType = m_vpSteps[m_iSelection[pn]]->m_StepsType;
		}
		break;

	case TYPE_COURSE:
		{
			m_iSelection[pn] += dir;
			if( CLAMP(m_iSelection[pn],0,m_vpTrails.size()-1) )
				return;

			// the user explicity switched difficulties.  Update the preferred difficulty
			GAMESTATE->ChangePreferredCourseDifficulty( pn, m_vpTrails[ m_iSelection[pn] ]->m_CourseDifficulty );

			if( dir < 0 )
				m_soundDifficultyEasier.Play();
			else
				m_soundDifficultyHarder.Play();

			FOREACH_HumanPlayer( p )
			{
				if( pn == p || GAMESTATE->DifficultiesLocked() )
				{
					m_iSelection[p] = m_iSelection[pn];
					AfterTrailChange( p );
				}
			}
		}
		break;

	case TYPE_RANDOM:
	case TYPE_ROULETTE:
		/* XXX: We could be on a music or course sort, or even one with both; we don't
		 * really know which difficulty to change.  Maybe the two difficulties should be
		 * linked ... */
		if( GAMESTATE->ChangePreferredDifficulty( pn, dir ) )
		{
			if( dir < 0 )
				m_soundDifficultyEasier.Play();
			else
				m_soundDifficultyHarder.Play();

			AfterMusicChange();

			FOREACH_HumanPlayer( p )
			{
				if( pn == p || GAMESTATE->DifficultiesLocked() )
				{
					m_iSelection[p] = m_iSelection[pn];
					AfterStepsChange( p );
				}
			}
		}
		break;
	}
}

void ScreenSelectMusic::ChangeDifficultyModeSwitch( PlayerNumber pn, Song* pSong, int dir, int index, int iPlayers )
{
	bool bStyleOK = false;
	while( !bStyleOK && !m_bModeSwitcherLoadsAll )
	{
		index += dir;		// Go to the next style

		// If we're out of the iteration range, wrap!
		wrap( index, m_vPossibleStyles.size() );

		GAMESTATE->SetCurrentStyle(m_vPossibleStyles[index]);

		if( (iPlayers == 1 && ModeSwitchOkayFor1P()) || (iPlayers > 1 && ModeSwitchOkayFor2P()) )
		{
			if( m_bModeSwitcherWraps )
			{
				m_vpSteps.clear();
				pSong->GetSteps( m_vpSteps, GAMESTATE->GetCurrentStyle()->m_StepsType );
				if( !m_vpSteps.empty() )
					bStyleOK = true;
			}
			else
				bStyleOK = true;
		}
	}

	if( bStyleOK )
	{
		GAMESTATE->m_vPossibleStepsTypes.clear();
		LOG->Trace( "Resetting ModeSwitcher" );
		GAMESTATE->m_vPossibleStepsTypes.push_back( GAMESTATE->GetCurrentStyle()->m_StepsType );
		LOG->Trace( "Adding '%s' to ModeSwitcher", StepsTypeToString(GAMESTATE->GetCurrentStyle()->m_StepsType).c_str() );
	}

	// If not using Autogen, reload to remove songs that don't have this steps style
	if( !PREFSMAN->m_bAutogenSteps && !m_bModeSwitcherLoadsAll )
	{
		m_MusicWheel.ReloadItems();

		// Update the steps
		m_vpSteps.clear();
		pSong->GetSteps( m_vpSteps, GAMESTATE->m_vPossibleStepsTypes );

		if( m_vpSteps.empty() )
		{
			Song* newSong = NULL;

			// The song doesn't have steps for this mode. Find a new song with steps!
			bool bSongOK = false;

			while( !bSongOK )
			{
				// Update to the new song and get the steps from it
				newSong = m_MusicWheel.GetPreferredSelectionForRandomOrPortal();

				// Okay, so we found a valid song
				if( newSong != NULL )
				{
					m_vpSteps.clear();
					newSong->GetSteps( m_vpSteps, GAMESTATE->m_vPossibleStepsTypes );

					if( !m_vpSteps.empty() )
					{
						pSong = newSong;
						bSongOK = true;

						// Continue here to avoid changing the style below
						continue;
					}
				}

				// No song was found, or something has gone wrong with the new steps.
				bStyleOK = false;

				while( !bStyleOK )
				{
					index += dir;		// Go to the next style

					// If we're out of the iteration range, wrap!
					wrap( index, m_vPossibleStyles.size() );

					GAMESTATE->SetCurrentStyle(m_vPossibleStyles[index]);

					if( (iPlayers == 1 && ModeSwitchOkayFor1P()) || (iPlayers > 1 && ModeSwitchOkayFor2P()) )
					{
						if( m_bModeSwitcherWraps )
						{
							m_vpSteps.clear();
							pSong->GetSteps( m_vpSteps, GAMESTATE->GetCurrentStyle()->m_StepsType );
							if( !m_vpSteps.empty() )
								bStyleOK = true;
						}
						else
							bStyleOK = true;
					}
				}

				if( bStyleOK )
				{
					GAMESTATE->m_vPossibleStepsTypes.clear();
					LOG->Trace( "Resetting ModeSwitcher" );
					GAMESTATE->m_vPossibleStepsTypes.push_back( GAMESTATE->GetCurrentStyle()->m_StepsType );
					LOG->Trace( "Adding '%s' to ModeSwitcher", StepsTypeToString(GAMESTATE->GetCurrentStyle()->m_StepsType).c_str() );
				}

				m_MusicWheel.ReloadItems();

				// Check to see if we're valid now for original song
				// Update the steps
				m_vpSteps.clear();
				pSong->GetSteps( m_vpSteps, GAMESTATE->m_vPossibleStepsTypes );

				if( !m_vpSteps.empty() )
					break;	// The song had valid steps in this mode, so we move on

				// If we still get down to here, then we need to repeat the process.
				// Loop again!
			}
		}

		GAMESTATE->m_pCurSong = pSong;
		GAMESTATE->m_pPreferredSong = pSong;

		// So, we found a valid song for this mode. Switch to it, and remove all songs that don't have this
		// style!
		AfterMusicChange();
	}
	else
	{
		// Update the steps
		m_vpSteps.clear();
		pSong->GetSteps( m_vpSteps, GAMESTATE->m_vPossibleStepsTypes );
	}

	// Reset the selection
	if( dir > 0 )
		m_iSelection[pn] = 0;
	else
		m_iSelection[pn] = m_vpSteps.size() - 1;
}

bool ScreenSelectMusic::ModeSwitchOkayFor1P()
{
	bool bNotOkayFor1P = false;

	if( stricmp(m_s1PlayerModes,"") != 0 )	// We have data in the string
	{
		CStringArray sAllowedModes;

		split(m_s1PlayerModes,",",sAllowedModes,true);	// split the string, and ignore empty values

		if( sAllowedModes.size() != 0 )
		{
			for( unsigned i=0; true; i++ )
			{
				if( GAMESTATE->GetCurrentStyle()->m_szName == sAllowedModes[i] )
					break;	// The new mode is in the allowed string. Break out of the loop.

				// We've gone through the entire string, and we don't have a match
				// This mode is not valid
				if( i == (sAllowedModes.size()-1) )
				{
					bNotOkayFor1P = true;
					break;
				}
			}
		}
	}

	// We weren't okay above, so return false.
	if( bNotOkayFor1P )
		return false;

	// We'll check to make sure that the game won't crash, even if they specified a mode. For example, they may
	// have specified versus, but that will crash the game with 1 player, so we'll force it to ignore that entry.
	if( GAMESTATE->GetCurrentStyle()->m_StyleType == Style::TWO_PLAYERS_TWO_CREDITS )
		return false;

#if defined( WITH_COIN_MODE )
	// Special case: Doubles in pay mode without the double/joint premium enabled. Disable the ability to
	// cycle to doubles unless two credits were indeed paid.
	if( PREFSMAN->GetPremium() == PrefsManager::NO_PREMIUM && PREFSMAN->GetCoinMode() == PrefsManager::COIN_PAY )
	{
		if( GAMESTATE->GetCurrentStyle()->m_StyleType == Style::ONE_PLAYER_TWO_CREDITS && GAMESTATE->GetNumSidesJoined() == 1 )
			return false;
	}
#endif

	// Phew, we got through that without failing. We're okay in this mode.
	return true;
}

bool ScreenSelectMusic::ModeSwitchOkayFor2P()
{
	bool bNotOkayFor2P = false;

	if( stricmp(m_s2PlayerModes,"") != 0 )	// We have data in the string
	{
		CStringArray sAllowedModes;

		split(m_s2PlayerModes,",",sAllowedModes,true);	// split the string, and ignore empty values

		if( sAllowedModes.size() != 0 )
		{
			for( unsigned i=0; true; i++ )
			{
				if( GAMESTATE->GetCurrentStyle()->m_szName == sAllowedModes[i] )
					break;	// The new mode is in the allowed string. Break out of the loop.

				// We've gone through the entire string, and we don't have a match
				// This mode is not valid
				if( i == (sAllowedModes.size()-1) )
				{
					bNotOkayFor2P = true;
					break;
				}
			}
		}
	}

	// We weren't okay above, so return false. And avoid next check.
	if( bNotOkayFor2P )
		return false;

	// We'll check to make sure that the game won't crash, even if they specified a mode. For example, they may
	// have specified single, but that will crash the game with 2 players, so we'll force it to ignore that entry.
	if( GAMESTATE->GetCurrentStyle()->m_StyleType != Style::TWO_PLAYERS_TWO_CREDITS )
		return false;

	return true;
}

void ScreenSelectMusic::ChangeGroup( int dir )
{
	m_MusicWheel.MoveSection( dir, m_bSectionChangeSavesPosition );
}

void ScreenSelectMusic::HandleScreenMessage( const ScreenMessage SM )
{
	switch( SM )
	{
	case SM_AllowOptionsMenuRepeat:
		m_bAllowOptionsMenuRepeat = true;
		break;
	case SM_MenuTimer:
		if( m_MusicWheel.IsRouletting() )
		{
			m_iSelectionState = SELECT_FINALIZED;
			MenuStart(PLAYER_INVALID);

			m_MenuTimer->SetSeconds( 15 );
			m_MenuTimer->Start();
		}
		else if( THEME->GetMetricB(m_sName,"DoRouletteOnMenuTimer") )
		{
			if( m_MusicWheel.GetSelectedType() != TYPE_SONG )
			{
				m_MusicWheel.StartRoulette();
				m_MenuTimer->SetSeconds( 15 );
				m_MenuTimer->Start();
			}
			else
			{
				m_iSelectionState = SELECT_FINALIZED;
				MenuStart(PLAYER_INVALID);
			}
		}
		else
		{
			if( m_MusicWheel.GetSelectedType() != TYPE_SONG && m_MusicWheel.GetSelectedType() != TYPE_COURSE )
				m_MusicWheel.StartRandom();

			m_iSelectionState = SELECT_FINALIZED;
			MenuStart(PLAYER_INVALID);
		}
		return;
	case SM_GoToPrevScreen:
		SCREENMAN->SetNewScreen( THEME->GetMetric(m_sName,"PrevScreen") );
		/* We may have stray SM_SongChanged messages from the music wheel.  We can't
		 * handle them anymore, since the title menu (and attract screens) reset
		 * the game state, so just discard them. */
		ClearMessageQueue();
		return;
	case SM_BeginFadingOut:
		/* XXX: yuck.  Later on, maybe this can be done in one BGA with lua ... */
		if( m_bGoToOptions )
			m_bgOptionsOut.StartTransitioning( SM_GoToNextScreen );
		else
			m_bgNoOptionsOut.StartTransitioning( SM_GoToNextScreen );
		break;
	case SM_GoToNextScreen:
		if( m_bGoToOptions )
			SCREENMAN->SetNewScreen( THEME->GetMetric(m_sName,"NextOptionsScreen") );
		else
		{
			GAMESTATE->AdjustFailType();
			SCREENMAN->SetNewScreen( THEME->GetMetric(m_sName,"NextScreen") );
		}
		SOUND->StopMusic();
		return;
	case SM_SongChanged:
		AfterMusicChange();
		break;
	case SM_SortOrderChanging: /* happens immediately */
//				m_MusicSortDisplay.FadeOff( 0, "fade", TWEEN_TIME );
		TweenScoreOnAndOffAfterChangeSort();
		break;
	case SM_SortOrderChanged: /* happens after the wheel is off and the new song is selected */
		SortOrderChanged();
		break;
	case SM_GainFocus:
		CodeDetector::RefreshCacheItems( THEME->GetMetric(m_sName,"Codes") );
		break;
	case SM_LoseFocus:
		CodeDetector::RefreshCacheItems(); /* reset for other screens */
		break;
	}

	Screen::HandleScreenMessage( SM );
}

void ScreenSelectMusic::MenuStart( PlayerNumber pn )
{
	// this needs to check whether valid Steps are selected!
	bool bResult = m_MusicWheel.Select();

	// If false, we don't have a selection just yet.
	if( !bResult )
		return;

	// a song was selected
	switch( m_MusicWheel.GetSelectedType() )
	{
	case TYPE_PORTAL:
	case TYPE_SONG:
		{
			bool bIsNew = PROFILEMAN->IsSongNew( m_MusicWheel.GetSelectedSong() );
			bool bIsHard = false;
			FOREACH_HumanPlayer( p )
			{
				if( GAMESTATE->m_pCurSteps[p]  &&  GAMESTATE->m_pCurSteps[p]->GetMeter() >= 10 )
					bIsHard = true;
			}

			/* See if this song is a repeat.  If we're in event mode, only check the last five songs. */
			bool bIsRepeat = false;
			int i = 0;
			if( PREFSMAN->m_bEventMode )
				i = max( 0, int(g_vPlayedStageStats.size())-5 );
			for( ; i < (int)g_vPlayedStageStats.size(); ++i )
			{
				if( g_vPlayedStageStats[i].vpSongs.back() == m_MusicWheel.GetSelectedSong() )
				{
					bIsRepeat = true;
					break;
				}
			}

			/* Don't complain about repeats if the user didn't get to pick. */
			if( !PREFSMAN->m_bPickExtraStage && GAMESTATE->IsExtraStage() || !PREFSMAN->m_bPickExtraStage2 && GAMESTATE->IsExtraStage2() )
				bIsRepeat = false;

			if( !m_bPortalDisplaysSong && m_MusicWheel.GetSelectedType() == TYPE_PORTAL )
			{
				bIsRepeat = false;
				bIsNew = false;
				bIsHard = false;
			}

			if( bIsRepeat )
				SOUND->PlayOnceFromAnnouncer( "select music comment repeat" );
			else if( bIsNew )
				SOUND->PlayOnceFromAnnouncer( "select music comment new" );
			else if( bIsHard )
				SOUND->PlayOnceFromAnnouncer( "select music comment hard" );
			else
				SOUND->PlayOnceFromAnnouncer( "select music comment general" );

			NextSelectionState();	// Update SelectionState

			if( m_iSelectionState == SELECT_FINALIZED )
				m_bMadeChoice = true;

			if( m_iPreviewMusicMode == 1 && m_iSelectionState >= SELECT_SONG )
			{
				SOUND->PlayMusic( m_sSampleMusicToPlay, m_pSampleMusicTimingData, m_bSampleMusicLoops, m_fSampleStartSeconds,
										m_fSampleLengthSeconds, 1.5f, m_bAlignMusicBeats ); // fade out for 1.5 seconds
			}

			/* If we're in event mode, we may have just played a course (putting us
			 * in course mode).  Make sure we're in a single song mode. */
			if( GAMESTATE->IsCourseMode() )
				GAMESTATE->m_PlayMode = PLAY_MODE_REGULAR;
		}
		break;

	case TYPE_COURSE:
		{
			NextSelectionState();	// Update SelectionState

			SOUND->PlayOnceFromAnnouncer( "select course comment general" );

			Course *pCourse = m_MusicWheel.GetSelectedCourse();
			ASSERT( pCourse );
			GAMESTATE->m_PlayMode = pCourse->GetPlayMode();

			// apply #LIVES
			if( pCourse->m_iLives != -1 )
			{
				GAMESTATE->m_PlayerOptions[pn].m_LifeType = PlayerOptions::LIFE_BATTERY;
				GAMESTATE->m_PlayerOptions[pn].m_iBatteryLives = pCourse->m_iLives;
			}

			if( m_iSelectionState == SELECT_FINALIZED )
				m_bMadeChoice = true;
		}
		break;
	case TYPE_SECTION:
	case TYPE_ROULETTE:
	case TYPE_RANDOM:
	case TYPE_SORT:
		break;
	default:
		ASSERT(0);
	}

	if( m_bMadeChoice )
	{
		TweenOffScreen();
		//SCREENMAN->PlayStartSound();

		bool bExtra1 = GAMESTATE->IsExtraStage(),
			bExtra2 = GAMESTATE->IsExtraStage2();

		if( !bExtra1 && !bExtra2 ||
			bExtra1 && PREFSMAN->m_bPickModsForExtraStage ||
			bExtra2 && PREFSMAN->m_bPickModsForExtraStage2
			)
		{
//			float fShowSeconds = m_Out.GetLengthSeconds();

			// show "hold START for options"
			m_sprOptionsMessage.SetDiffuse( RageColor(1,1,1,1) );	// visible
			SET_XY_AND_ON_COMMAND( m_sprOptionsMessage );

			m_bAllowOptionsMenu = true;
			/* Don't accept a held START for a little while, so it's not
			 * hit accidentally.  Accept an initial START right away, though,
			 * so we don't ignore deliberate fast presses (which would be
			 * annoying). */
			this->PostScreenMessage( SM_AllowOptionsMenuRepeat, 0.5f );
		}

		/* If we're currently waiting on song assets, abort all except the music and
		 * start the music, so if we make a choice quickly before background requests
		 * come through, the music will still start. */

		if( m_bTweenWhileMovingAndSettled )
		{
			m_bMovingTweenWaiting = false;
			m_bSettledTweenWaiting = false;
		}

		m_bBannerWaiting = m_bDiscWaiting = m_bBackgroundWaiting = m_bPreviewWaiting = m_bCDTitleWaiting = false;
		m_BackgroundLoader.Abort();
		CheckBackgroundRequests();

		StartTransitioning( SM_BeginFadingOut );
	}

	if( GAMESTATE->IsExtraStage() && PREFSMAN->m_bPickExtraStage )
	{
		// Check if user selected the real extra stage.
		Song* pSong;
		Steps* pSteps;
		PlayerOptions po;
		SongOptions so;
		SONGMAN->GetExtraStageInfo( false, GAMESTATE->GetCurrentStyle(), pSong, pSteps, po, so );
		ASSERT(pSong);

		// Enable 2nd extra stage if user chose the correct song or if it's always allowed
		if( m_MusicWheel.GetSelectedSong() == pSong || PREFSMAN->m_bAlwaysAllowExtraStage2 )
			GAMESTATE->m_bAllow2ndExtraStage = true;
		else
			GAMESTATE->m_bAllow2ndExtraStage = false;
	}
}


void ScreenSelectMusic::MenuBack( PlayerNumber pn )
{
	SOUND->StopMusic();

	Back( SM_GoToPrevScreen );
}

void ScreenSelectMusic::AfterStepsChange( PlayerNumber pn )
{
	ASSERT( GAMESTATE->IsHumanPlayer(pn) );

	CLAMP( m_iSelection[pn], 0, m_vpSteps.size()-1 );

	Song* pSong = GAMESTATE->m_pCurSong;
	Steps* pSteps = m_vpSteps.empty() ? NULL : m_vpSteps[m_iSelection[pn]];

	GAMESTATE->m_pCurSteps[pn] = pSteps;
	int iScore = 0;

	if( pSteps )
	{
		// TODO - Aldo_MX: SM crashea con doubles, y esta es la gota que derrama el vaso... u_u
		const Style* pStyle = GAMEMAN->GetGameplayStyleForStepsType( pSteps->m_StepsType, GAMESTATE->GetNumHumanPlayers() > 1 );
		GAMESTATE->SetCurrentStyle(pStyle);

		if( GAMESTATE->GetCurrentStyle()->m_StepsType != m_LastChosenStepsType )
		{
			if( m_bStepsTypeIcons )
				m_DifficultyIcon[pn].Load( THEME->GetPathG(m_sName,ssprintf("difficulty icons %s 1x%d",GAMESTATE->GetCurrentStyle()->m_szName, NUM_DIFFICULTIES)) );

			m_DifficultyIcon[pn].SetFromSteps( pn, pSteps );

			m_LastChosenStepsType = GAMESTATE->GetCurrentStyle()->m_StepsType;
		}

		// Update the current mode text!
		CString sModeName = GAMESTATE->GetCurrentStyle()->m_szName;
		sModeName.MakeUpper();
		m_textCurMode.SetText( sModeName );

		Profile* pProfile = PROFILEMAN->IsUsingProfile(pn) ? PROFILEMAN->GetProfile(pn) : PROFILEMAN->GetMachineProfile();
		iScore = pProfile->GetStepsHighScoreList(pSong,pSteps).GetTopScore().iScore;

		if( !GAMESTATE->IsExtraStage() && !GAMESTATE->IsExtraStage2() )
			m_BPMDisplay.SetBPM(GAMESTATE->m_pCurSteps);
	}

	m_textHighScore[pn].SetText( ssprintf("%*d", m_iScoreDigits, iScore) );
	m_DifficultyIcon[pn].SetFromSteps( pn, pSteps );

	if( pSteps && pSteps->IsAutogen() )
	{
		if( m_bBlinkAutogenIcon )
			m_AutoGenIcon[pn].SetEffectDiffuseShift( 1.0f, RageColor(1,1,1,1), RageColor(1,1,1,0) );

		m_AutoGenIcon[pn].SetDiffuse( RageColor(1,1,1,1) );
	}
	else
	{
		m_AutoGenIcon[pn].SetEffectNone();
		m_AutoGenIcon[pn].SetDiffuse( RageColor(1,1,1,0) );
	}

	m_DifficultyMeter[pn].SetFromGameState( pn );

	if( m_bShowDifficultyList )
		m_DifficultyList.SetFromGameState( true );

	m_GrooveRadar.SetFromSteps( pn, pSteps );
	m_MusicWheel.NotesOrTrailChanged( pn );

	if( m_bShowPanes )
		m_PaneDisplay[pn].SetFromGameState();
}

void ScreenSelectMusic::AfterTrailChange( PlayerNumber pn )
{
	ASSERT( GAMESTATE->IsHumanPlayer(pn) );

	CLAMP( m_iSelection[pn], 0, m_vpTrails.size()-1 );

	Course* pCourse = GAMESTATE->m_pCurCourse;
	Trail* pTrail = m_vpTrails.empty() ? NULL : m_vpTrails[m_iSelection[pn]];

	GAMESTATE->m_pCurTrail[pn] = pTrail;

	int iScore = 0;
	if( pTrail )
	{
		Profile* pProfile = PROFILEMAN->IsUsingProfile(pn) ? PROFILEMAN->GetProfile(pn) : PROFILEMAN->GetMachineProfile();
		iScore = pProfile->GetCourseHighScoreList(pCourse,pTrail).GetTopScore().iScore;
	}

	m_textHighScore[pn].SetText( ssprintf("%*i", m_iScoreDigits, iScore) );
	m_DifficultyIcon[pn].SetFromTrail( pn, pTrail );

	// Update the trail list, but don't actually start the tween; only do that when
	// the actual course changes (AfterMusicChange).
	m_CourseContentsFrame.SetFromGameState();

	m_DifficultyMeter[pn].SetFromGameState( pn );

	if( m_bShowDifficultyList )
		m_DifficultyList.SetFromGameState( false );

	m_GrooveRadar.SetEmpty( pn );
	m_MusicWheel.NotesOrTrailChanged( pn );

	if( m_bShowPanes )
		m_PaneDisplay[pn].SetFromGameState();
}

void ScreenSelectMusic::SwitchToPreferredDifficulty( StepsType st )
{
	if( !GAMESTATE->m_pCurCourse )
	{
		FOREACH_HumanPlayer( pn )
		{
			/* Find the closest match to the user's preferred difficulty. */
			int CurDifference = -1;
			for( unsigned i=0; i<m_vpSteps.size(); i++ )
			{
				if( st != m_vpSteps[i]->m_StepsType )
					continue;

				int Diff = abs(m_vpSteps[i]->GetDifficulty() - GAMESTATE->m_PreferredDifficulty[pn]);

				if( CurDifference == -1 || Diff < CurDifference )
				{
					m_iSelection[pn] = i;
					CurDifference = Diff;
				}
			}

			CLAMP( m_iSelection[pn],0,m_vpSteps.size()-1 );
		}
	}
	else
	{
		FOREACH_HumanPlayer( pn )
		{
			/* Find the closest match to the user's preferred difficulty. */
			int CurDifference = -1;
			for( unsigned i=0; i<m_vpTrails.size(); i++ )
			{
				int Diff = abs(m_vpTrails[i]->m_CourseDifficulty - GAMESTATE->m_PreferredCourseDifficulty[pn]);

				if( CurDifference == -1 || Diff < CurDifference )
				{
					m_iSelection[pn] = i;
					CurDifference = Diff;
				}
			}

			CLAMP( m_iSelection[pn],0,m_vpTrails.size()-1 );
		}
	}
}

template<class T>
int FindCourseIndexOfSameMode( T begin, T end, const Course *p )
{
	const PlayMode pm = p->GetPlayMode();

	int n = 0;
	for( T it = begin; it != end; ++it )
	{
		if( *it == p )
			return n;

		/* If it's not playable in this mode, don't increment.  It might result in
		 * different output in different modes, but that's better than having holes. */
		if( !(*it)->IsPlayableIn( GAMESTATE->GetCurrentStyle()->m_StepsType ) )
			continue;
		if( (*it)->GetPlayMode() != pm )
			continue;
		++n;
	}

	return -1;
}

void ScreenSelectMusic::AfterMusicChange()
{
	if( !m_MusicWheel.IsRouletting() )
		m_MenuTimer->Stall();

	// lock difficulties.  When switching from arcade to rave, we need to
	// enforce that all players are at the same difficulty.
	if( GAMESTATE->DifficultiesLocked() )
	{
		FOREACH_HumanPlayer( p )
		{
			m_iSelection[p] = m_iSelection[GAMESTATE->m_MasterPlayerNumber];
			GAMESTATE->m_PreferredDifficulty[p] = GAMESTATE->m_PreferredDifficulty[GAMESTATE->m_MasterPlayerNumber];
		}
	}

	Song* pSong = m_MusicWheel.GetSelectedSong();
	GAMESTATE->m_pCurSong = pSong;
	if( pSong )
		GAMESTATE->m_pPreferredSong = pSong;

	m_GrooveGraph.SetFromSong( pSong );

	Course* pCourse = m_MusicWheel.GetSelectedCourse();
	GAMESTATE->m_pCurCourse = pCourse;
	if( pCourse )
		GAMESTATE->m_pPreferredCourse = pCourse;

	FOREACH_PlayerNumber( p )
	{
		GAMESTATE->m_pCurSteps[p] = NULL;
		GAMESTATE->m_pCurTrail[p] = NULL;
		m_vpSteps.clear();
		m_vpTrails.clear();
	}

	if( m_bMovingCommandRepeats )
	{
		m_bMovingTweenWaiting = true;
		TweenWhileMoving();
	}

	switch( m_MusicWheel.GetSelectedType() )
	{
	case TYPE_ROULETTE:
	case TYPE_RANDOM:
	case TYPE_PORTAL:
		m_iLastSection = -1;
		break;
	default:
		if( m_iLastSection != m_MusicWheel.GetCurrentSection() )
		{
			m_iLastSection = m_MusicWheel.GetCurrentSection();
			AfterSectionChange();

			if( GAMESTATE->m_SortOrder == SORT_GROUP || GAMESTATE->m_SortOrder == SORT_FOLDER )
				SOUND->PlayOnce( SONGMAN->GetGroupSoundPath( m_MusicWheel.GetSelectedGroupName( false ) ) );
		}
	}

	if( m_bShowBanners && !m_bBannerWheel )
		m_Banner.SetMovingFast( !!m_MusicWheel.IsMoving() );

	if( m_bShowDiscs )
		m_Disc.SetMovingFast( !!m_MusicWheel.IsMoving() );

	if( m_bShowBackgrounds )
		m_Background.SetMovingFast( !!m_MusicWheel.IsMoving() );

	if( m_bShowPreviews && !m_bPreviewInSprite )
		m_Preview.SetMovingFast( !!m_MusicWheel.IsMoving() );

	//CString SampleMusicToPlay, SampleMusicTimingData;
	vector<CString> m_Artists, m_AltArtists;

	m_MachineRank.SetText( "" );

	m_sSampleMusicToPlay = "";
	m_pSampleMusicTimingData = NULL;

	m_sBannerPath = "";
	m_sDiscPath = "";
	m_sBackgroundPath = "";
	m_sPreviewPath = "";
	m_sCDTitlePath = "";

	m_bLoadFallbackCDTitle = false;

	bool bWantBanner = true;
	bool bWantDisc = true;
	bool bWantBackground = true;
	bool bWantPreview = true;

	switch( m_MusicWheel.GetSelectedType() )
	{
	case TYPE_SECTION:
	case TYPE_SORT:
		{
			// So we don't break the background music!
			m_iSelectionState = SELECT_SONG;

			CString sGroup = m_MusicWheel.GetSelectedGroupName( false );
			FOREACH_PlayerNumber( p )
				m_iSelection[p] = -1;

			m_BPMDisplay.NoBPM();
			m_DifficultyDisplay.UnsetDifficulties();

			m_fSampleStartSeconds = 0;
			m_fSampleLengthSeconds = -1;

			m_textNumSongs.SetText( "" );
			m_textGroupName.SetText( m_MusicWheel.GetSelectedGroupName( m_bShowShortGroupName ) );
			m_textTotalTime.SetText( "" );

			switch( m_MusicWheel.GetSelectedType() )
			{
			case TYPE_SECTION:
				m_sBannerPath = SONGMAN->GetGroupBannerPath( sGroup );
				m_sDiscPath = SONGMAN->GetGroupDiscPath( sGroup );
				m_sBackgroundPath = SONGMAN->GetGroupBackgroundPath( sGroup );
				m_sPreviewPath = SONGMAN->GetGroupPreviewPath( sGroup );

				switch( GAMESTATE->m_SortOrder )
				{
				case SORT_PREFERRED:
				case SORT_ROULETTE:
				case SORT_GROUP:
				case SORT_FOLDER:
					m_sSampleMusicToPlay = SONGMAN->GetGroupSamplePath( sGroup );
				}

				if( m_sSampleMusicToPlay == "" )
					m_sSampleMusicToPlay = m_sSectionMusic;
				m_bThemeMusic = true;
				break;
			case TYPE_SORT:
				bWantBanner = false;		// we load it ourselves
				bWantDisc = false;			// we load it ourselves
				bWantBackground = false;	// we load it ourselves
				bWantPreview = false;		// we load it ourselves

				switch( GAMESTATE->m_SortOrder )
				{
				case SORT_SORT_MENU:
					if( m_bShowBanners && !m_bBannerWheel )
						m_Banner.LoadSort( false );

					if( m_bShowDiscs )
						m_Disc.LoadSort( true );

					if( m_bShowBackgrounds )
						m_Background.LoadSort( false );

					if( m_bShowPreviews )
					{
						if( m_bPreviewInSprite )
							m_sprPreview.Load( m_sSortPreviewPath );
						else
							m_Preview.LoadSort( true );
					}
					break;
				case SORT_MODE_MENU:
					if( m_bShowBanners && !m_bBannerWheel )
						m_Banner.LoadMode( false );

					if( m_bShowDiscs )
						m_Disc.LoadMode( true );

					if( m_bShowBackgrounds )
						m_Background.LoadMode( false );

					if( m_bShowPreviews )
					{
						if( m_bPreviewInSprite )
							m_sprPreview.Load( m_sModePreviewPath );
						else
							m_Preview.LoadMode( true );
					}
					break;
				}

				m_sSampleMusicToPlay = THEME->GetPathS(m_sName,"sort music");
				m_bThemeMusic = true;
				break;
			default:
				ASSERT(0);
			}

			m_sprBalloon.StopTweening();
			COMMAND( m_sprBalloon, "Hide" );

			m_sprCourseHasMods->StopTweening();
			COMMAND( m_sprCourseHasMods, "Hide" );
		}
		break;
	case TYPE_PORTAL:
		if( !m_bPortalDisplaysSong )
		{
			bWantBanner = false;		// we load it ourselves
			bWantDisc = false;			// we load it ourselves
			bWantBackground = false;	// we load it ourselves
			bWantPreview = false;		// we load it ourselves

			m_fSampleStartSeconds = 0;
			m_fSampleLengthSeconds = -1;

			m_sSampleMusicToPlay = THEME->GetPathS(m_sName,"portal music");
			m_bThemeMusic = true;

			m_textGroupName.SetText( "???" );

			if( m_bShowBanners && !m_bBannerWheel )
				m_Banner.LoadAllMusic( false );

			if( m_bShowDiscs )
				m_Disc.LoadAllMusic( true );

			if( m_bShowBackgrounds )
				m_Background.LoadAllMusic( false );

			if( m_bShowPreviews )
			{
				if( m_bPreviewInSprite )
					m_sprPreview.Load( m_sAllMusicPreviewPath );
				else
					m_Preview.LoadAllMusic( true );

			}

			m_BPMDisplay.CycleRandomly();
			m_MachineRank.SetText( "" );
			m_DifficultyDisplay.UnsetDifficulties();
			m_Artists.push_back( "???" );
			m_AltArtists.push_back( "???" );
		}
	case TYPE_SONG:
		{
			if( m_MusicWheel.GetSelectedType() == TYPE_SONG || m_bPortalDisplaysSong )
			{
				m_pSampleMusicTimingData = &pSong->m_Timing;
				if( pSong->HasIntro() )
				{
					m_sSampleMusicToPlay = pSong->GetIntroPath();
					m_fSampleStartSeconds = 0.f;
					m_fSampleLengthSeconds = -1.f;
				}
				else
				{
					m_sSampleMusicToPlay = pSong->GetMusicPath();
					m_fSampleStartSeconds = pSong->m_fMusicSampleStartSeconds;
					m_fSampleLengthSeconds = pSong->m_fMusicSampleLengthSeconds;
				}
				m_bThemeMusic = false;

				m_textGroupName.SetText( m_MusicWheel.GetSelectedGroupName( m_bShowShortGroupName ) );

				m_sBannerPath = pSong->GetBannerPath();
				m_sDiscPath = pSong->GetDiscPath();
				m_sBackgroundPath = pSong->GetBackgroundPath();
				m_sPreviewPath = pSong->GetPreviewPath();
				m_sCDTitlePath = pSong->GetCDTitlePath();

				if( GAMESTATE->IsExtraStage() || GAMESTATE->IsExtraStage2() )
					m_BPMDisplay.CycleRandomly();

				const vector<Song*> best = SONGMAN->GetBestSongs( PROFILE_SLOT_MACHINE );
				const int index = FindIndex( best.begin(), best.end(), pSong );
				if( index != -1 )
					m_MachineRank.SetText( ssprintf("%i", index+1) );

				m_DifficultyDisplay.SetDifficulties( pSong, GAMESTATE->GetCurrentStyle()->m_StepsType );

				m_Artists.push_back( pSong->GetDisplayArtist() );
				m_AltArtists.push_back( pSong->GetTranslitArtist() );
			}

			m_bLoadFallbackCDTitle = true;

			m_textNumSongs.SetText( ssprintf("%d", SongManager::GetNumStagesForSong(pSong) ) );
			m_textTotalTime.SetText( SecondsToMMSSMsMs(pSong->m_fMusicLengthSeconds) );

			pSong->GetSteps( m_vpSteps, GAMESTATE->m_vPossibleStepsTypes );

			SwitchToPreferredDifficulty( m_LastUsedStepsType );

			/* Short delay before actually showing these, so they don't show
			 * up when scrolling fast.  It'll still show up in "slow" scrolling,
			 * but it doesn't look at weird as it does in "fast", and I don't
			 * like the effect with a lot of delay. */
			bool bBalloonSet = false;
			int st;
			for( st=PREFSMAN->m_iStageLengths-1; st>=0; --st )
			{
				bBalloonSet = pSong->m_fMusicLengthSeconds > PREFSMAN->m_StageLength[st].m_fMusicLengthSeconds;

				if( bBalloonSet )
					break;
			}

			if( bBalloonSet && m_bStageLengthBalloon[st] )
			{
				m_sprBalloon.StopTweening();
				const CString path = THEME->GetPathG(m_sName, ssprintf("balloon stagelength %d", st+1), true);

				if( path == "" )
				{
					// Just because I have to be retrocompatible
					switch( st )
					{
					case 1:
						m_sprBalloon.Load( THEME->GetPathG(m_sName, "balloon long") );
						SET_XY( m_sprBalloon );
						COMMAND( m_sprBalloon, "Show" );
						break;
					case 2:
						m_sprBalloon.Load( THEME->GetPathG(m_sName, "balloon marathon") );
						SET_XY( m_sprBalloon );
						COMMAND( m_sprBalloon, "Show" );
						break;
					default:
						m_sprBalloon.StopTweening();
						COMMAND( m_sprBalloon, "Hide" );
						break;
					}
				}
				else
				{
					m_sprBalloon.Load( THEME->GetPathG(m_sName, ssprintf("balloon stagelength %d", st+1)) );
					SET_XY( m_sprBalloon );
					COMMAND( m_sprBalloon, "Show" );
				}

			}
			else
			{
				m_sprBalloon.StopTweening();
				COMMAND( m_sprBalloon, "Hide" );
			}

			m_sprCourseHasMods->StopTweening();
			COMMAND( m_sprCourseHasMods, "Hide" );
		}
		break;
	case TYPE_ROULETTE:
	case TYPE_RANDOM:
		bWantBanner = false;		// we load it ourselves
		bWantDisc = false;			// we load it ourselves
		bWantBackground = false;	// we load it ourselves
		bWantPreview = false;		// we load it ourselves

		switch(m_MusicWheel.GetSelectedType())
		{
		case TYPE_ROULETTE:
			if( m_bShowBanners && !m_bBannerWheel )
				m_Banner.LoadRoulette( false );

			if( m_bShowDiscs )
				m_Disc.LoadRoulette( true );

			if( m_bShowBackgrounds )
				m_Background.LoadRoulette( false );

			if( m_bShowPreviews )
			{
				if( m_bPreviewInSprite )
					m_sprPreview.Load( m_sRoulettePreviewPath );
				else
					m_Preview.LoadRoulette( true );
			}
			break;
		case TYPE_RANDOM:
			if( m_bShowBanners && !m_bBannerWheel )
				m_Banner.LoadRandom( false );

			if( m_bShowDiscs )
				m_Disc.LoadRandom( true );

			if( m_bShowBackgrounds )
				m_Background.LoadRandom( false );

			if( m_bShowPreviews )
			{
				if( m_bPreviewInSprite )
					m_sprPreview.Load( m_sRandomPreviewPath );
				else
					m_Preview.LoadRandom( true );
			}
			break;
		default: ASSERT(0);
		}

		m_BPMDisplay.NoBPM();
		m_DifficultyDisplay.UnsetDifficulties();

		m_fSampleStartSeconds = 0;
		m_fSampleLengthSeconds = -1;

		m_textNumSongs.SetText( "" );
		m_textGroupName.SetText( "" );
		m_textTotalTime.SetText( "" );

		switch( m_MusicWheel.GetSelectedType() )
		{
		case TYPE_ROULETTE:
			m_sSampleMusicToPlay = THEME->GetPathS(m_sName,"roulette music");
			m_bThemeMusic = true;
			break;
		case TYPE_RANDOM:
			m_sSampleMusicToPlay = THEME->GetPathS(m_sName,"random music");
			m_bThemeMusic = true;
			break;
		default:
			ASSERT(0);
		}

		m_sprBalloon.StopTweening();
		COMMAND( m_sprBalloon, "Hide" );

		m_sprCourseHasMods->StopTweening();
		COMMAND( m_sprCourseHasMods, "Hide" );
		break;
	case TYPE_COURSE:
	{
		Course* pCourse = m_MusicWheel.GetSelectedCourse();
		StepsType st = GAMESTATE->GetCurrentStyle()->m_StepsType;
		Trail *pTrail = pCourse->GetTrail( st );
		ASSERT( pTrail );

		pCourse->GetTrails( m_vpTrails, GAMESTATE->GetCurrentStyle()->m_StepsType );

		m_sSampleMusicToPlay = THEME->GetPathS(m_sName,"course music");
		m_bThemeMusic = true;
		m_fSampleStartSeconds = 0;
		m_fSampleLengthSeconds = -1;

		m_textNumSongs.SetText( ssprintf("%d", pCourse->GetEstimatedNumStages()) );
		m_textGroupName.SetText( "" );
		float fTotalSeconds;
		if( pCourse->GetTotalSeconds(st,fTotalSeconds) )
			m_textTotalTime.SetText( SecondsToMMSSMsMs(fTotalSeconds) );
		else
			m_textTotalTime.SetText( "xx:xx.xx" );	// The numbers format doesn't have a '?'.  Is there a better solution?

		m_sBannerPath = pCourse->m_sBannerPath;
		m_sDiscPath = pCourse->m_sDiscPath;
		m_sBackgroundPath = pCourse->m_sBackgroundPath;
		m_sPreviewPath = pCourse->m_sPreviewPath;

		m_BPMDisplay.SetBPM(pTrail);

		m_DifficultyDisplay.UnsetDifficulties();

		SwitchToPreferredDifficulty( m_LastUsedStepsType );

		FOREACH_CONST( TrailEntry, pTrail->m_vEntries, e )
		{
			if( e->bMystery )
			{
				m_Artists.push_back( "???" );
				m_AltArtists.push_back( "???" );
			} else {
				m_Artists.push_back( e->pSong->GetDisplayArtist() );
				m_AltArtists.push_back( e->pSong->GetTranslitArtist() );
			}
		}

		const vector<Course*> best = SONGMAN->GetBestCourses( PROFILE_SLOT_MACHINE );
		const int index = FindCourseIndexOfSameMode( best.begin(), best.end(), pCourse );
		if( index != -1 )
			m_MachineRank.SetText( ssprintf("%i", index+1) );

		m_sprBalloon.StopTweening();
		COMMAND( m_sprBalloon, "Hide" );

		if( pCourse->HasMods() )
		{
			m_sprCourseHasMods->StopTweening();
			COMMAND( m_sprCourseHasMods, "Show" );
		}
		else
		{
			m_sprCourseHasMods->StopTweening();
			COMMAND( m_sprCourseHasMods, "Hide" );
		}

		break;
	}
	default:
		ASSERT(0);
	}

	m_sprCDTitleFront.UnloadTexture();
	m_sprCDTitleBack.UnloadTexture();

	/* Cancel any previous, incomplete requests for song assets, since we need new ones. */
	m_BackgroundLoader.Abort();

	m_bCDTitleWaiting = false;
	if( !m_sCDTitlePath.empty() || m_bLoadFallbackCDTitle )
	{
		LOG->Trace( "cache \"%s\"", m_sCDTitlePath.c_str());
		m_BackgroundLoader.CacheFile( m_sCDTitlePath ); // empty OK
		m_bCDTitleWaiting = true;
	}

	m_bBannerWaiting = false;
	if( bWantBanner && m_bShowBanners && !m_bBannerWheel )
	{
		if( PREFSMAN->m_bBannerUsesCacheOnly )
			m_bBannerWaiting = true;
		else
		{
			LOG->Trace( "LoadFromCachedBanner(%s)", m_sBannerPath.c_str() );
			if( m_Banner.LoadFromCachedBanner( m_sBannerPath, false ) )
			{
				m_BackgroundLoader.CacheFile( m_sBannerPath );
				m_bBannerWaiting = true;
			}
		}
	}

	m_bDiscWaiting = false;
	if( bWantDisc && m_bShowDiscs )
	{
		if( PREFSMAN->m_bDiscUsesCacheOnly )
			m_bDiscWaiting = true;
		else
		{
			LOG->Trace( "LoadFromCachedBanner(%s)", m_sDiscPath.c_str() );
			if( m_Disc.LoadFromCachedBanner( m_sDiscPath, true ) )
			{
				m_BackgroundLoader.CacheFile( m_sDiscPath );
				m_bDiscWaiting = true;
			}
		}
	}

	m_bBackgroundWaiting = false;
	if( bWantBackground && m_bShowBackgrounds )
	{
		if( PREFSMAN->m_bBackgroundUsesCacheOnly )
			m_bBackgroundWaiting = true;
		else
		{
			LOG->Trace( "LoadFromCachedBackground(%s)", m_sBackgroundPath.c_str() );
			if( m_Background.LoadFromCachedBackground( m_sBackgroundPath, false ) )
			{
				m_BackgroundLoader.CacheFile( m_sBackgroundPath );
				m_bBackgroundWaiting = true;
			}
		}
	}

	m_bPreviewWaiting = false;
	if( bWantPreview && m_bShowPreviews )
	{
		if( PREFSMAN->m_bPreviewUsesCacheOnly && !m_bPreviewInSprite )
			m_bPreviewWaiting = true;
		else
		{
			LOG->Trace( "cache \"%s\"", m_sPreviewPath.c_str());
			m_BackgroundLoader.CacheFile( m_sPreviewPath );
			m_bPreviewWaiting = true;
		}
	}

	// Don't stop music if it's already playing the right file.
	m_bSampleMusicWaiting = false;
	if( !m_MusicWheel.IsRouletting() && SOUND->GetMusicPath() != m_sSampleMusicToPlay )
	{
		if( m_iPreviewMusicMode != 4 )
			SOUND->StopMusic();
		else
			SOUND->PlayMusic( m_sSectionMusic, m_pSampleMusicTimingData, true, 0, -1, 1.5f, m_bAlignMusicBeats ); // fade out for 1.5 seconds

		if( !m_sSampleMusicToPlay.empty() )
			m_bSampleMusicWaiting = true;
	}

	m_tStartedLoadingAt.Touch();

	// update stage counter display (long versions/marathons)
	m_sprStage.Load( THEME->GetPathG(m_sName,"stage "+(m_bUseSongNumber ? GAMESTATE->GetSongText() : GAMESTATE->GetStageText())) );

	m_Artist.SetTips( m_Artists, m_AltArtists );

	FOREACH_HumanPlayer( p )
	{
		if( GAMESTATE->m_pCurCourse )
			AfterTrailChange( p );
		else
			AfterStepsChange( p );
	}

	switch( m_MusicWheel.GetSelectedType() )
	{
	case TYPE_COURSE:
		m_CourseContentsFrame.TweenInAfterChangedCourse();
		break;
	}
}

void ScreenSelectMusic::AfterSectionChange()
{
	if( m_bTweenWhenSectionChanges )
		m_bSectionTweenWaiting = true;

	if( m_bResetKeyTimerAfterSectionChange )
		m_bResetKeyTimers = true;
}

void ScreenSelectMusic::UpdateOptionsDisplays()
{
//	m_OptionIcons.Load( GAMESTATE->m_PlayerOptions, &GAMESTATE->m_SongOptions );

//	m_PlayerOptionIcons.Refresh();

	FOREACH_PlayerNumber( p )
	{
		if( GAMESTATE->IsHumanPlayer(p) )
		{
			m_OptionIconRow[p].Refresh();

			CString s = GAMESTATE->m_PlayerOptions[p].GetString();
			s.Replace( ", ", "\n" );
//			m_textPlayerOptions[p].SetText( s );
		}
	}

	CString s = GAMESTATE->m_SongOptions.GetString( true );
	s.Replace( ", ", "\n" );
	m_textSongOptions.SetText( s );
}

void ScreenSelectMusic::SortOrderChanged()
{
	m_MusicSortDisplay.Set( GAMESTATE->m_SortOrder );

	switch( GAMESTATE->m_SortOrder )
	{
	case SORT_ALL_COURSES:
	case SORT_NONSTOP_COURSES:
	case SORT_ONI_COURSES:
	case SORT_ENDLESS_COURSES:
	case SORT_SORT_MENU:
	case SORT_MODE_MENU:
		// do nothing
		break;
	default:
		if( m_bShowDifficultyList )
			m_DifficultyList.Show();
		break;
	}

	// tween music sort on screen
//	m_MusicSortDisplay.FadeOn( 0, "fade", TWEEN_TIME );
}

void ScreenSelectMusic::NextSelectionState()
{
	if( GAMESTATE->IsCourseMode() )
	{
		// We don't play the start sound if going to finalized, as that's taken care of elsewhere
		m_iSelectionState = SELECT_FINALIZED;
	}
	else
	{
		SCREENMAN->PlayStartSound();

		switch( m_iSelectionState )
		{
		case SELECT_PREVIEW:
			if( m_iPreviewMusicMode == 3 )
				m_iSelectionState = SELECT_FINALIZED;
			else
				m_iSelectionState = SELECT_SONG;
			break;
		case SELECT_SONG:
			if( m_bTwoPartSelection )
				m_iSelectionState = SELECT_STEPS;
			else if( m_iPreviewMusicMode == 3 )
				m_iSelectionState = SELECT_PREVIEW;
			else
				m_iSelectionState = SELECT_FINALIZED;
			break;
		case SELECT_STEPS:
			if( m_iPreviewMusicMode == 3 )
				m_iSelectionState = SELECT_PREVIEW;
			else
				m_iSelectionState = SELECT_FINALIZED;
			break;
		default:
			m_iSelectionState = SELECT_FINALIZED;
		}
	}
}

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * Contains code (c) 2001-2004 Chris Danford.
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
