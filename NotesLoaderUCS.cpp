#include "global.h"
#include "NotesLoaderUCS.h"
#include "Song.h"
#include "Steps.h"
#include "RageException.h"
#include "RageFile.h"
#include "RageLog.h"
#include "RageUtil.h"
#include "NoteData.h"
#include "NoteTypes.h"

bool UCSLoader::LoadFromUCS( const CString &sPath, Steps &steps )
{
	LOG->Trace( "UCSLoader::LoadFromUCS( '%s' )", sPath.c_str() );
	
	// Open UCS
	RageFile fUCS;

	if( !fUCS.Open( sPath ) )
		RageException::Throw( "Error opening file '%s'.\n\n%s.", sPath.c_str(), fUCS.GetError().c_str() );

	steps.m_bLoadedFromSM = false;
	steps.m_Timing.Reset();

	{
		CString sDir, sFile, sExt;
		splitpath( sPath, sDir, sFile, sExt );
		steps.SetDescription(sFile);
	}

	CString sUCSLine, sUCSMode;
	CStringArray saUCSAttributes;
	bool bFirstDelay = true, bFirstMode = true, bSmoothSpeed = false;
	float fCurrentTime = FLT_MAX, fCurrentBPM = 60.f, fCurrentSpeed = 1.f, fCurrentScroll = 1.f;
	unsigned int uCurrentRow = 0, uColumns = 5, uCurrentSplit = 2, uScrollStartRow = 0u, uUCSLineSize;
	unsigned char uPlayer = 0, uMaxPlayer = 1;

	TapNote tap;
	NoteData notedata;
	vector<bool> abHoldStartSet;
	vector<unsigned int> auHoldStartRow;

	while(!fUCS.AtEOF())
	{
		fUCS.GetLine(sUCSLine); // get the current line
		uUCSLineSize = sUCSLine.size();

		if( uUCSLineSize < 1 )	// empty line
			continue;

		if( sUCSLine[0] == ':' )
		{
			split( sUCSLine.Right(--uUCSLineSize), "=", saUCSAttributes );
			if( saUCSAttributes.size() != 2 )
			{
				LOG->Warn("Invalid UCS line ignored: %s", sUCSLine.c_str());
				continue;
			}

			CString& sAttribute = saUCSAttributes[0];
			CString& sValue = saUCSAttributes[1];

			if( 0==stricmp(sAttribute,"Time") )
			{
				fCurrentTime = strtof2(sValue) / 1000.f;
			}

			else if( 0==stricmp(sAttribute,"BPM") )
			{
				float fBPM = strtof2(sValue);
				if( !FEQ(fBPM, fCurrentBPM, 0.0001) )
				{
					steps.m_Timing.SetBPMAtBeat(NoteRowToBeat(uCurrentRow), fBPM);
					fCurrentBPM = fBPM;
				}
			}

			else if( 0==stricmp(sAttribute,"Delay") )
			{
				if( bFirstDelay )	// Beat 0 Offset
				{
					if( fCurrentTime == FLT_MAX )
						steps.m_Timing.m_fBeat0Offset = -strtof2(sValue) / 1000.f;
					else
						steps.m_Timing.m_fBeat0Offset = -fCurrentTime;

					bFirstDelay = false;
				}
				else
				{
					float fDelay = -strtof2(sValue) / 1000.f;

					if( fCurrentTime == FLT_MAX )
						steps.m_Timing.SetStopAtBeat(NoteRowToBeat(uCurrentRow), fDelay, true);
					else
					{
						TimingData& timing = steps.m_Timing;
						float fCurrentBeat = NoteRowToBeat(uCurrentRow),
							fElapsedTime = timing.GetElapsedTimeFromBeat(fCurrentBeat);

						if( FEQ(fCurrentTime-fDelay, fElapsedTime, 0.0001) )
							timing.SetStopAtBeat(fCurrentBeat, fDelay, true);
						else
							timing.SetStopAtBeat(fCurrentBeat, fCurrentTime-fElapsedTime, true);
					}
				}
				fCurrentTime = FLT_MAX;
			}

			else if( 0==stricmp(sAttribute,"Speed") )
			{
				// TODO - Aldo_MX: Freeze = speed < 0.f;
				float fSpeed = fabsf(strtof2(sValue));
				if( !FEQ(fSpeed, fCurrentSpeed, 0.0001) )
				{
					steps.m_Timing.SetSpeedAtBeat(NoteRowToBeat(uCurrentRow), fSpeed, 4.0, 1.f);
					fCurrentSpeed = fSpeed;
				}
			}

			else if( 0==stricmp(sAttribute,"Split") )
			{
				unsigned int uSplit = (unsigned int)abs(atoi(sValue));
				if( uCurrentSplit != uSplit )
				{
					steps.m_Timing.SetTickcountAtBeat(NoteRowToBeat(uCurrentRow), uSplit);
					uCurrentSplit = steps.m_Timing.GetTickcountAtBeat(NoteRowToBeat(uCurrentRow));

					if( uCurrentSplit != uSplit )
						LOG->Warn("Unrecognized Split, \"%u\" is not a factor of %d, \"%u\" used instead.", uSplit, ROWS_PER_BEAT, uCurrentSplit);
				}
			}

			else if( 0==stricmp(sAttribute,"Scroll") )
			{
				if( !FEQ(fCurrentScroll, 1.f, 0.0001) )
					steps.m_Timing.SetSpeedAreaAtBeat(NoteRowToBeat(uScrollStartRow), fCurrentScroll, fCurrentScroll, NoteRowToBeat(uCurrentRow) - NoteRowToBeat(uScrollStartRow), 0.f, 1.f);

				fCurrentScroll = strtof2(sValue);
				if( !FEQ(fCurrentScroll, 0.f, 0.0001) )
					fCurrentScroll = 1.f;

				uScrollStartRow = uCurrentRow;
			}

			// TODO: BeatMeasure
			else if( 0==stricmp(sAttribute,"Beat") )
			{
				;	// nop
			}

			else if( 0==stricmp(sAttribute,"Smooth") )
			{
				bSmoothSpeed = atoi(sValue) != 0;
			}

			else if( 0==stricmp(sAttribute,"Rows") )
			{
				if( bSmoothSpeed )
				{
					int iRows = abs(atoi(sValue));
					float fBeat = NoteRowToBeat(uCurrentRow + 1),
						fBeats = (float)iRows / uCurrentSplit;

					SpeedSegment& ss = steps.m_Timing.GetSpeedSegmentAtBeat(fBeat);
					ss.m_fBeats = fBeats;
				}
			}

			else if( 0==stricmp(sAttribute,"Format") )
			{
				int iFormat = atoi(sValue);
				if( iFormat != 1 )
					LOG->Warn("Unrecognized UCS format, the file might load incorrectly: %d", iFormat);
			}

			else if( 0==stricmp(sAttribute,"Mode") )
			{
				if( !bFirstMode )
				{
					LOG->Warn("Cannot accept multiple modes in a single file.");
					continue;
				}

				else if( 0==stricmp(sValue,"Single") )
				{
					uColumns = 5;
					steps.m_StepsType = STEPS_TYPE_PUMP_SINGLE;
				}
				
				else if( 0==stricmp(sValue,"Double") )
				{
					uColumns = 10;
					steps.m_StepsType = STEPS_TYPE_PUMP_DOUBLE;
				}
				
				else if( 0==stricmp(sValue,"S-Performance") )
				{
					uColumns = 5;
					steps.m_StepsType = STEPS_TYPE_PUMP_SINGLE;
				}
				
				else if( 0==stricmp(sValue,"D-Performance") )
				{
					uColumns = 10;
					steps.m_StepsType = STEPS_TYPE_PUMP_DOUBLE;
				}
				
				else
				{
					LOG->Warn("Unrecognized UCS mode, ignored: %s", sUCSLine.c_str());
					break;
				}
				
				bFirstMode = false;
				notedata.SetNumTracks(uColumns);
				auHoldStartRow.resize(uColumns, 0);
				abHoldStartSet.resize(uColumns, false);
			}

			else
				LOG->Warn("Unrecognized UCS attribute, ignored: %s", sUCSLine.c_str());
			
			saUCSAttributes.clear();
			continue;
		}

		if( bFirstMode )
		{
			LOG->Warn("Cannot read steps without a mode.");
			break;
		}

		if( uUCSLineSize > 0 && uUCSLineSize != uColumns )
		{
			LOG->Warn("Unrecognized UCS line, ignored: %s", sUCSLine.c_str());
			continue;
		}

		for( unsigned t=0; t<uColumns; t++ )
		{
			switch( sUCSLine[t] )
			{
			// Empty rows, ignored
			case '.':
			case ',':
				tap = TAP_EMPTY;
				break;

			// Hold tail, if there was no body add a tap instead
			case 'W':
			case 'V':	// Aldo_MX: Unofficial - P2
			case 'U':	// Aldo_MX: Unofficial - P3
			case 'w':	// Aldo_MX: Unofficial - P4
			case 'v':	// Aldo_MX: Unofficial - P5
			case 'u':	// Aldo_MX: Unofficial - P6
				if( abHoldStartSet[t] )
				{
					switch( sUCSLine[t] )
					{
					case 'W':	uPlayer = 1;	break;
					case 'V':	uPlayer = 2;	break;	// Aldo_MX: Unofficial - P2
					case 'U':	uPlayer = 3;	break;	// Aldo_MX: Unofficial - P3
					case 'w':	uPlayer = 4;	break;	// Aldo_MX: Unofficial - P4
					case 'v':	uPlayer = 5;	break;	// Aldo_MX: Unofficial - P5
					case 'u':	uPlayer = 6;	break;	// Aldo_MX: Unofficial - P6
					default:	uPlayer = 0;
					}
					HoldNote hn(t, auHoldStartRow[t], uCurrentRow);
					//hn.subtype = HOLD_TYPE_PIU;
					hn.subtype = HOLD_TYPE_DANCE;
					hn.playerNumber = uPlayer;
					notedata.AddHoldNote( hn );
					auHoldStartRow[t] = 0;
					abHoldStartSet[t] = false;
					uMaxPlayer = max(uMaxPlayer, uPlayer);
					continue;
				}

			// TapNotes
			case 'X':
			case 'O':	// Aldo_MX: Unofficial - P2
			case 'S':	// Aldo_MX: Unofficial - P3
			case 'x':	// Aldo_MX: Unofficial - P4
			case 'o':	// Aldo_MX: Unofficial - P5
			case 's':	// Aldo_MX: Unofficial - P6
				{
					switch( sUCSLine[t] )
					{
					case 'X':	uPlayer = 1;	break;
					case 'O':	uPlayer = 2;	break;	// Aldo_MX: Unofficial - P2
					case 'S':	uPlayer = 3;	break;	// Aldo_MX: Unofficial - P3
					case 'x':	uPlayer = 4;	break;	// Aldo_MX: Unofficial - P4
					case 'o':	uPlayer = 5;	break;	// Aldo_MX: Unofficial - P5
					case 's':	uPlayer = 6;	break;	// Aldo_MX: Unofficial - P6
					default:	uPlayer = 0;
					}
					tap = TAP_ORIGINAL_TAP;
					tap.playerNumber = uPlayer;
					uMaxPlayer = max(uMaxPlayer, uPlayer);
				}
				break;

			// Hold head/body, update Hold start row
			case 'M':
			case 'H':
			case 'P':	// Aldo_MX: Unofficial - P2
			case 'K':	// Aldo_MX: Unofficial - P2
			case 'F':	// Aldo_MX: Unofficial - P3
			case 'N':	// Aldo_MX: Unofficial - P3
			case 'm':	// Aldo_MX: Unofficial - P4
			case 'h':	// Aldo_MX: Unofficial - P4
			case 'p':	// Aldo_MX: Unofficial - P5
			case 'k':	// Aldo_MX: Unofficial - P5
			case 'f':	// Aldo_MX: Unofficial - P6
			case 'n':	// Aldo_MX: Unofficial - P6
				if( !abHoldStartSet[t] )
				{
					auHoldStartRow[t] = uCurrentRow;
					abHoldStartSet[t] = true;
				}
				tap = TAP_EMPTY;
				break;
			
			// Other, ignored
			default:
				LOG->Warn("Unrecognized UCS character, ignored: %c", sUCSLine[t]);
				tap = TAP_EMPTY;
			}
			notedata.SetTapNote(t, uCurrentRow, tap);
		}

		uCurrentRow += ROWS_PER_BEAT/uCurrentSplit;
	}

	fUCS.Close();

	// Empty UCS
	if( bFirstMode )
		return false;

	// Check if last block is a mystery block
	if( !FEQ(fCurrentScroll, 1.f, 0.0001) )
		steps.m_Timing.SetSpeedAreaAtBeat(NoteRowToBeat(uScrollStartRow), fCurrentScroll, fCurrentScroll, NoteRowToBeat(uCurrentRow) - NoteRowToBeat(uScrollStartRow), 0.f, 1.f);

	// Complete existing holds
	for( unsigned t=0; t<uColumns; t++ )
	{
		if( abHoldStartSet[t] )
		{
			HoldNote hn(t, auHoldStartRow[t], uCurrentRow);
			//hn.subtype = HOLD_TYPE_PIU;
			hn.subtype = HOLD_TYPE_DANCE;
			hn.playerNumber = uPlayer;
			notedata.AddHoldNote( hn );
			auHoldStartRow[t] = 0;
			abHoldStartSet[t] = false;
		}
	}

	notedata.SetNumPlayers(uMaxPlayer);
	steps.SetNoteData(&notedata);

	float fMeter = steps.PredictMeter();
	int iMeter = (int)(fMeter + .5f);
	steps.SetMeter( iMeter );

	if( fMeter > 25 )
	{
		steps.m_bHiddenDifficulty = true;
		steps.SetDifficulty( DIFFICULTY_EDIT );
	}

	else if( fMeter > 18 )
		steps.SetDifficulty( DIFFICULTY_CHALLENGE );

	else if( fMeter > 12 )
		steps.SetDifficulty( DIFFICULTY_HARD );

	else if( fMeter > 5 )
		steps.SetDifficulty( DIFFICULTY_MEDIUM );

	else if( fMeter > 3 )
		steps.SetDifficulty( DIFFICULTY_EASY );

	else
		steps.SetDifficulty( DIFFICULTY_BEGINNER );

	steps.TidyUpData();

	return true;
}

void UCSLoader::GetApplicableFiles( CString sPath, CStringArray &out )
{
	GetDirListing( sPath + "*.ucs", out, false, true );
}

bool UCSLoader::LoadFromDir( CString sDir, Song &song )
{
	LOG->Trace( "UCSLoader::LoadFromDir(%s)", sDir.c_str() );

	CStringArray asUCS;
	GetApplicableFiles( sDir, asUCS );

	if( asUCS.empty() )
		RageException::Throw( "Couldn't find any UCS files in '%s'", sDir.c_str() );

	// load the Steps from the rest of the UCS files
	for( unsigned i=0; i<asUCS.size(); i++ )
	{
		Steps* pNewNotes = new Steps;
		if( !LoadFromUCS( asUCS[i], *pNewNotes ) )
		{
			delete pNewNotes;
			continue;
		}

		song.AddSteps( pNewNotes );
	}

	// TODO - Aldo_MX: Load from lightmap
	// TODO - Aldo_MX: Merge with NotesLoaderKSF

	return song.DetectSongInfoFromPIUDir();
}

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
