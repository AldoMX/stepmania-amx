#include "global.h"
#include "ScreenSelectMaster.h"
#include "ScreenManager.h"
#include "PrefsManager.h"
#include "GameManager.h"
#include "ThemeManager.h"
#include "GameSoundManager.h"
#include "GameState.h"
#include "AnnouncerManager.h"
#include "ModeChoice.h"
#include "ActorUtil.h"
#include "RageLog.h"
#include <set>
#include "Foreach.h"

CachedThemeMetricI	NUM_ICON_PARTS							("ScreenSelectMaster", "NumIconParts");
CachedThemeMetricI	NUM_PREVIEW_PARTS						("ScreenSelectMaster", "NumPreviewParts");
CachedThemeMetricI	NUM_CURSOR_PARTS						("ScreenSelectMaster", "NumCursorParts");
CachedThemeMetricB	SHARED_PREVIEW_AND_CURSOR				("ScreenSelectMaster", "SharedPreviewAndCursor");
CachedThemeMetricF	PRE_SWITCH_PAGE_SECONDS					("ScreenSelectMaster", "PreSwitchPageSeconds");
CachedThemeMetricF	POST_SWITCH_PAGE_SECONDS				("ScreenSelectMaster", "PostSwitchPageSeconds");
CachedThemeMetricF	EXTRA_SLEEP_AFTER_TWEEN_OFF_SECONDS		("ScreenSelectMaster", "ExtraSleepAfterTweenOffSeconds");
CachedThemeMetricB	SHOW_SCROLLER							("ScreenSelectMaster", "ShowScroller");

#define	NUM_CHOICES_ON_PAGE( page )	THEME->GetMetricI(m_sName, ssprintf("NumChoicesOnPage%d", page+1));
#define	DEFAULT_CHOICE				THEME->GetMetric (m_sName, "DefaultChoice")
#define	CURSOR_OFFSET_X( p, part )	THEME->GetMetricF(m_sName, ssprintf("CursorPart%dP%dOffsetXFromIcon", part+1, p+1))
#define	CURSOR_OFFSET_Y( p, part )	THEME->GetMetricF(m_sName, ssprintf("CursorPart%dP%dOffsetYFromIcon", part+1, p+1))

#define	OPTION_ORDER( dir )			THEME->GetMetric (m_sName, "OptionOrder"+CString(dir))
#define	SCROLLER_SECONDS_PER_ITEM	THEME->GetMetricF(m_sName, "ScrollerSecondsPerItem")
#define	SCROLLER_SPACING_X			THEME->GetMetricF(m_sName, "ScrollerSpacingX")
#define	SCROLLER_SPACING_Y			THEME->GetMetricF(m_sName, "ScrollerSpacingY")

#define	LOCK_INPUT_SECONDS	THEME->HasMetric (m_sName, "LockInputSeconds") ? THEME->GetMetricF(m_sName, "LockInputSeconds") : this->GetTweenTimeLeft()

/* OptionOrderLeft=0:1,1:2,2:3,3:4 */
const char *ScreenSelectMaster::dirs[NUM_DIRS] =
{
	"Up", "Down", "Left", "Right", "Auto"
};

const ScreenMessage SM_PlayPostSwitchPage = (ScreenMessage)(SM_User+1);

ScreenSelectMaster::ScreenSelectMaster( CString sClassName ) : ScreenSelect( sClassName )
{
	if( m_aModeChoices.empty() )
		RageException::Throw("ScreenSelectMaster: %s\n\nNo choices were defined.", m_sName.c_str());

	if( GAMESTATE->GetNumSidesJoined() == 0 )
		RageException::Throw("ScreenSelectMaster: %s\n\nYou must join at least one player before calling this screen.\n\nPS. Maybe you forgot to set the following metric:\n\n[ScreenAttract]\nJoinPlayersOnStart=1", m_sName.c_str());

	NUM_ICON_PARTS.Refresh(m_sName);
	NUM_PREVIEW_PARTS.Refresh(m_sName);
	NUM_CURSOR_PARTS.Refresh(m_sName);
	SHARED_PREVIEW_AND_CURSOR.Refresh(m_sName);
	PRE_SWITCH_PAGE_SECONDS.Refresh(m_sName);
	POST_SWITCH_PAGE_SECONDS.Refresh(m_sName);
	EXTRA_SLEEP_AFTER_TWEEN_OFF_SECONDS.Refresh(m_sName);
	SHOW_SCROLLER.Refresh(m_sName);

	// Choices per page (set last one automatically)
	unsigned uTotalChoices = 0;
	Page lastPage = Page(NUM_PAGES - 1);
	FOREACH_ENUM(Page, lastPage, page)
	{
		int iChoices = NUM_CHOICES_ON_PAGE(page);
		m_iChoicesPerPage[page] = iChoices;
		uTotalChoices += iChoices;
	}

	if( m_aModeChoices.size() < uTotalChoices )
		RageException::Throw("ScreenSelectMaster: %s\n\nThe total choices per page (%u) are greater than the defined choices (%u).", m_sName.c_str(), uTotalChoices, m_aModeChoices.size());
	m_iChoicesPerPage[lastPage] = m_aModeChoices.size() - uTotalChoices;

	// Default choice
	m_iDefaultChoice = -1;
	for( unsigned c = 0; c < m_aModeChoices.size(); c++ )
	{
		const ModeChoice& mc = m_aModeChoices[c];
		if( mc.m_sName == DEFAULT_CHOICE )
		{
			m_iDefaultChoice = c;
			break;
		}
	}
	CLAMP(m_iDefaultChoice, 0, (int)(m_aModeChoices.size() - 1u));
	FOREACH_PlayerNumber( pn )
	{
		m_iChoice[pn] = m_iDefaultChoice;
		m_bChosen[pn] = false;
	}

	m_bDidSharedCursorCommand = false;
	if( !SHARED_PREVIEW_AND_CURSOR )
	{
		FOREACH_HumanPlayer( pn )
			LoadPlayer(pn);
	}
	else
	{
		// init cursor
		m_sprCursor[0].assign(NUM_CURSOR_PARTS, AutoActor());
		m_vecCursorOffset[0].assign(NUM_CURSOR_PARTS, RageVector2());
		for( int i = 0; i < NUM_CURSOR_PARTS; i++ )
		{
			m_sprCursor[0][i].Load(THEME->GetPathG(m_sName, ssprintf("Cursor Part%d", i + 1)));
			m_sprCursor[0][i]->SetName(ssprintf("CursorPart%d", i + 1));
			m_vecCursorOffset[0][i].x = CURSOR_OFFSET_X(0, i);
			m_vecCursorOffset[0][i].y = CURSOR_OFFSET_Y(0, i);
			this->AddChild(m_sprCursor[0][i]);
		}

		// init scroller
		if( SHOW_SCROLLER )
		{
			m_sprScroll[0].assign(m_aModeChoices.size(), AutoActor());
			m_Scroller[0].Load(
				SCROLLER_SECONDS_PER_ITEM,
				7,
				RageVector3(0, 0, 0),
				RageVector3(0, 0, 0),
				RageVector3(SCROLLER_SPACING_X, SCROLLER_SPACING_Y, 0),
				RageVector3(0, 0, 0)
			);
			m_Scroller[0].SetName("Scroller");
			this->AddChild(&m_Scroller[0]);
		}

		m_sprPreview[0].assign(m_aModeChoices.size(), vector<AutoActor>());
		for( unsigned c = 0; c < m_aModeChoices.size(); c++ )
		{
			const ModeChoice& mc = m_aModeChoices[c];

			// init scroll
			if( SHOW_SCROLLER )
			{
				m_sprScroll[0][c].Load(THEME->GetPathG(m_sName, ssprintf("Scroll Choice%s", mc.m_sName.c_str())));
				m_sprScroll[0][c]->SetName(ssprintf("Scroll"));
				m_Scroller[0].AddChild(m_sprScroll[0][c]);
			}

			// init preview
			m_sprPreview[0][c].assign(NUM_PREVIEW_PARTS, AutoActor());
			for( int i = 0; i < NUM_PREVIEW_PARTS; i++ )
			{
				m_sprPreview[0][c][i].Load(THEME->GetPathG(m_sName, ssprintf("Preview Part%d Choice%s", i + 1, mc.m_sName.c_str())));
				m_sprPreview[0][c][i]->SetName(ssprintf("PreviewPart%d", i + 1));
				this->AddChild(m_sprPreview[0][c][i]);
			}
		}
	}

	// init icons
	m_sprIcon.assign(m_aModeChoices.size(), vector<AutoActor>());
	for( unsigned c = 0; c < m_aModeChoices.size(); c++ )
	{
		const ModeChoice& mc = m_aModeChoices[c];

		m_sprIcon[c].assign(NUM_ICON_PARTS, AutoActor());
		for( int i = 0; i < NUM_ICON_PARTS; i++ )
		{
			m_sprIcon[c][i].Load(THEME->GetPathG(m_sName, ssprintf("Icon Part%d Choice%s", i + 1, mc.m_sName.c_str())));
			m_sprIcon[c][i]->SetName(ssprintf("IconPart%dChoice%s", i + 1, mc.m_sName.c_str()));
			this->AddChild(m_sprIcon[c][i]);
		}
	}

	for( int page=0; page<NUM_PAGES; page++ )
	{
		m_sprMore[page].SetName( ssprintf("MorePage%d",page+1) );
		m_sprMore[page].Load( THEME->GetPathToG( ssprintf("%s more page%d",m_sName.c_str(), page+1) ) );
		this->AddChild( &m_sprMore[page] );

		m_sprExplanation[page].Load( THEME->GetPathToG( ssprintf("%s explanation page%d",m_sName.c_str(), page+1) ) );
		m_sprExplanation[page]->SetName( ssprintf("ExplanationPage%d",page+1) );
		this->AddChild( m_sprExplanation[page] );
	}

	m_soundChange.Load( THEME->GetPathToS( ssprintf("%s change", m_sName.c_str())), true );
	m_soundDifficult.Load( ANNOUNCER->GetPathTo("select difficulty challenge") );

	// init m_Next order info
	for( int dir = 0; dir < NUM_DIRS; ++dir )
	{
		m_Next[dir].assign( m_aModeChoices.size(), 0 );

		/* Reasonable defaults: */
		for( unsigned c = 0; c < m_aModeChoices.size(); ++c )
		{
			int add;
			switch( dir )
			{
			case DIR_UP:
			case DIR_LEFT:	add = -1; break;
			default:		add = +1; break;
			}

			m_Next[dir][c] = c + add;
			/* By default, only wrap around for DIR_AUTO. */
			if( dir == DIR_AUTO )
				wrap( m_Next[dir][c], m_aModeChoices.size() );
			else
				m_Next[dir][c] = clamp( m_Next[dir][c], 0, (int)m_aModeChoices.size()-1 );
		}

		const CString dirname = dirs[dir];
		const CString order = OPTION_ORDER( dirname );
		vector<CString> parts;
		split( order, ",", parts, true );

		if( parts.size() == 0 )
			continue;

		for( unsigned part = 0; part < parts.size(); ++part )
		{
			unsigned from, to;
			if( sscanf( parts[part], "%u:%u", &from, &to ) != 2 )
			{
				LOG->Warn( "%s::OptionOrder%s parse error", m_sName.c_str(), dirname.c_str() );
				continue;
			}

			--from;
			--to;

			if( from >= m_aModeChoices.size() ||
				to >= m_aModeChoices.size() )
			{
				LOG->Warn( "%s::OptionOrder%s out of range", m_sName.c_str(), dirname.c_str() );
				continue;
			}

			m_Next[dir][from] = to;
		}
	}

	this->UpdateSelectableChoices();

	TweenOnScreen();
	m_fLockInputSecs = LOCK_INPUT_SECONDS;
}

void ScreenSelectMaster::LoadPlayer( PlayerNumber pn )
{
	if( SHARED_PREVIEW_AND_CURSOR )
		return;

	// init cursor
	m_sprCursor[pn].assign(NUM_CURSOR_PARTS, AutoActor());
	m_vecCursorOffset[pn].assign(NUM_CURSOR_PARTS, RageVector2());
	for( int i = 0; i < NUM_CURSOR_PARTS; i++ )
	{
		m_sprCursor[pn][i].Load(THEME->GetPathG(m_sName, ssprintf("Cursor Part%d P%d", i + 1, pn + 1)));
		m_sprCursor[pn][i]->SetName(ssprintf("CursorPart%dP%d", i + 1, pn + 1));
		m_vecCursorOffset[pn][i].x = CURSOR_OFFSET_X(pn, i);
		m_vecCursorOffset[pn][i].y = CURSOR_OFFSET_Y(pn, i);
		this->AddChild(m_sprCursor[pn][i]);
	}

	// init scroller
	if( SHOW_SCROLLER )
	{
		m_sprScroll[pn].assign(m_aModeChoices.size(), AutoActor());
		m_Scroller[pn].Load(
			SCROLLER_SECONDS_PER_ITEM,
			7,
			RageVector3(0, 0, 0),
			RageVector3(0, 0, 0),
			RageVector3(SCROLLER_SPACING_X, SCROLLER_SPACING_Y, 0),
			RageVector3(0, 0, 0)
		);
		m_Scroller[pn].SetName(ssprintf("ScrollerP%d", pn + 1));
		this->AddChild(&m_Scroller[pn]);
	}

	m_sprPreview[pn].assign(m_aModeChoices.size(), vector<AutoActor>());
	for( unsigned c = 0; c < m_aModeChoices.size(); c++ )
	{
		const ModeChoice& mc = m_aModeChoices[c];

		// init scroller
		if( SHOW_SCROLLER )
		{
			m_sprScroll[pn][c].Load(THEME->GetPathG(m_sName, ssprintf("Scroll Choice%s P%d", mc.m_sName.c_str(), pn + 1)));
			m_sprScroll[pn][c]->SetName(ssprintf("ScrollP%d", pn + 1));
			m_Scroller[pn].AddChild(m_sprScroll[pn][c]);
		}

		// init preview
		m_sprPreview[pn][c].assign(NUM_PREVIEW_PARTS, AutoActor());
		for( int i = 0; i < NUM_PREVIEW_PARTS; i++ )
		{
			m_sprPreview[pn][c][i].Load(THEME->GetPathG(m_sName, ssprintf("Preview Part%d Choice%s P%d", i + 1, mc.m_sName.c_str(), pn + 1)));
			m_sprPreview[pn][c][i]->SetName(ssprintf("PreviewPart%dP%d", i + 1, pn + 1));
			this->AddChild(m_sprPreview[pn][c][i]);
		}
	}
}

void ScreenSelectMaster::Update( float fDelta )
{
	ScreenSelect::Update( fDelta );
	m_fLockInputSecs = max( 0, m_fLockInputSecs-fDelta );
}

void ScreenSelectMaster::HandleScreenMessage( const ScreenMessage SM )
{
	ScreenSelect::HandleScreenMessage( SM );

	switch( SM )
	{
	case SM_PlayerJoined + PLAYER_1:
	case SM_PlayerJoined + PLAYER_2:
		LoadPlayer( PlayerNumber(SM - SM_PlayerJoined) );
		break;

	case SM_PlayPostSwitchPage:
		{
			if( SHARED_PREVIEW_AND_CURSOR )
			{
				for( int i = 0; i < NUM_CURSOR_PARTS; i++ )
				{
					m_sprCursor[0][i]->SetXY(GetCursorX((PlayerNumber)0, i), GetCursorY((PlayerNumber)0, i));
					COMMAND(m_sprCursor[0][i], "PostSwitchPage");
				}

				for( int i = 0; i < NUM_PREVIEW_PARTS; i++ )
					COMMAND(m_sprPreview[0][m_iChoice[0]][i], "PostSwitchPage");
			}
			else
			{
				FOREACH_HumanPlayer( pn )
				{
					for( int i = 0; i < NUM_CURSOR_PARTS; i++ )
					{
						m_sprCursor[pn][i]->SetXY(GetCursorX(pn, i), GetCursorY(pn, i));
						COMMAND(m_sprCursor[pn][i], "PostSwitchPage");
					}

					for( int i = 0; i < NUM_PREVIEW_PARTS; i++ )
						COMMAND(m_sprPreview[pn][m_iChoice[pn]][i], "PostSwitchPage");
				}
			}

			m_fLockInputSecs = POST_SWITCH_PAGE_SECONDS;
		}
		break;

	case SM_BeginFadingOut:
		{
			TweenOffScreen();
			float fSecs = GetTweenTimeLeft();
			/* This can be used to allow overlap between the main tween-off and the MenuElements
			 * tweenoff. */
			fSecs += EXTRA_SLEEP_AFTER_TWEEN_OFF_SECONDS;
			fSecs = max( fSecs, 0 );
			SCREENMAN->PostMessageToTopScreen( SM_AllDoneChoosing, fSecs );	// nofify parent that we're finished
			StopTimer();
		}
		break;
	}
}

int ScreenSelectMaster::GetSelectionIndex( PlayerNumber pn )
{
	return m_iChoice[pn];
}

void ScreenSelectMaster::UpdateSelectableChoices()
{
	for( unsigned c = 0; c < m_aModeChoices.size(); c++ )
		for( int i = 0; i < NUM_ICON_PARTS; i++ )
			COMMAND(m_sprIcon[c][i], m_aModeChoices[c].IsPlayable() ? "Enabled" : "Disabled");

	FOREACH_PlayerNumber( p )
	{
		if( !GAMESTATE->IsHumanPlayer(p) )
			continue;

		if( !m_aModeChoices[m_iChoice[p]].IsPlayable() )
			Move( (PlayerNumber) p, DIR_AUTO );
		ASSERT( m_aModeChoices[m_iChoice[p]].IsPlayable() );
	}
}

bool ScreenSelectMaster::Move( PlayerNumber pn, Dirs dir )
{
	int iSwitchToIndex = m_iChoice[pn];
	set<int> seen;
	do
	{
		iSwitchToIndex = m_Next[dir][iSwitchToIndex];
		if( iSwitchToIndex == -1 )
			return false; // can't go that way
		if( seen.find(iSwitchToIndex) != seen.end() )
			return false; // went full circle and none found
		seen.insert( iSwitchToIndex );
	}
	while( !m_aModeChoices[iSwitchToIndex].IsPlayable() );

	return ChangeSelection( pn, iSwitchToIndex );
}

void ScreenSelectMaster::MenuLeft( PlayerNumber pn )
{
	if( m_fLockInputSecs > 0 || m_bChosen[pn] )
		return;
	if( Move(pn, DIR_LEFT) )
		m_soundChange.Play();
}

void ScreenSelectMaster::MenuRight( PlayerNumber pn )
{
	if( m_fLockInputSecs > 0 || m_bChosen[pn] )
		return;
	if( Move(pn, DIR_RIGHT) )
		m_soundChange.Play();
}

void ScreenSelectMaster::MenuUp( PlayerNumber pn )
{
	if( m_fLockInputSecs > 0 || m_bChosen[pn] )
		return;
	if( Move(pn, DIR_UP) )
		m_soundChange.Play();
}

void ScreenSelectMaster::MenuDown( PlayerNumber pn )
{
	if( m_fLockInputSecs > 0 || m_bChosen[pn] )
		return;
	if( Move(pn, DIR_DOWN) )
		m_soundChange.Play();
}

bool ScreenSelectMaster::ChangePage( int iNewChoice )
{
	Page newPage = GetPage(iNewChoice);

	// If anyone has already chosen, don't allow changing of pages
	FOREACH_PlayerNumber( pn )
		if( GAMESTATE->IsHumanPlayer(pn) && m_bChosen[pn] )
			return false;

	const CString sIconAndExplanationCommand = ssprintf("SwitchToPage%d", newPage + 1);

	for( unsigned c = 0; c < m_aModeChoices.size(); c++ )
		for( int i = 0; i < NUM_ICON_PARTS; i++ )
			COMMAND(m_sprIcon[c][i], sIconAndExplanationCommand);

	if( SHARED_PREVIEW_AND_CURSOR )
	{
		for( int i = 0; i < NUM_CURSOR_PARTS; i++ )
			COMMAND(m_sprCursor[0][i], "PreSwitchPage");

		for( int i = 0; i < NUM_PREVIEW_PARTS; i++ )
			COMMAND(m_sprPreview[0][m_iChoice[GAMESTATE->m_MasterPlayerNumber]][i], "PreSwitchPage");
	}
	else
	{
		FOREACH_HumanPlayer( pn )
		{
			for( int i = 0; i < NUM_CURSOR_PARTS; i++ )
				COMMAND(m_sprCursor[pn][i], "PreSwitchPage");

			for( int i = 0; i < NUM_PREVIEW_PARTS; i++ )
				COMMAND(m_sprPreview[pn][m_iChoice[pn]][i], "PreSwitchPage");
		}
	}

	for( int page = 0; page < NUM_PAGES; page++ )
	{
		COMMAND(m_sprExplanation[page], sIconAndExplanationCommand);
		COMMAND(m_sprMore[page], sIconAndExplanationCommand);
	}

	if( newPage > PAGE_1 )
	{
		// XXX: only play this once (I thought we already did that?)
		// DDR plays it on every change to page 2.  -Chris
		/* That sounds ugly if you go back and forth quickly. -g */
		// DDR locks input while it's scrolling.  Should we do the same? -Chris
		m_soundDifficult.Stop();
		m_soundDifficult.PlayRandom();
	}

	// change both players
	FOREACH_PlayerNumber( pn )
		m_iChoice[pn] = iNewChoice;

	m_fLockInputSecs = PRE_SWITCH_PAGE_SECONDS;
	this->PostScreenMessage(SM_PlayPostSwitchPage, PRE_SWITCH_PAGE_SECONDS);
	return true;
}

bool ScreenSelectMaster::ChangeSelection( PlayerNumber pn, int iNewChoice )
{
	if( iNewChoice == m_iChoice[pn] )
		return false; // already there

	if( GetPage(m_iChoice[pn]) != GetPage(iNewChoice) )
		return ChangePage( iNewChoice );

	bool bMoveAll = SHARED_PREVIEW_AND_CURSOR || GetCurrentPage() != PAGE_1;

	FOREACH_PlayerNumber( p )
	{
		const int iOldChoice = m_iChoice[p];

		if( !bMoveAll && p != pn )
			continue;	// skip

		/* Set the new m_iChoice even for disabled players, since a player might
		 * join on a SHARED_PREVIEW_AND_CURSOR after the cursor has been moved. */
		m_iChoice[p] = iNewChoice;

		if( !GAMESTATE->IsHumanPlayer(p) )
			continue;	// skip

		if( SHARED_PREVIEW_AND_CURSOR )
		{
			for( int i = 0; i < NUM_CURSOR_PARTS; i++ )
			{
				COMMAND(m_sprCursor[0][i], "Change");
				m_sprCursor[0][i]->SetXY(GetCursorX((PlayerNumber)0, i), GetCursorY((PlayerNumber)0, i));
			}

			for( int i = 0; i < NUM_PREVIEW_PARTS; i++ )
			{
				COMMAND(m_sprPreview[0][iOldChoice][i], "LoseFocus");
				COMMAND(m_sprPreview[0][iNewChoice][i], "GainFocus");
			}
		}
		else
		{
			for( int i = 0; i < NUM_CURSOR_PARTS; i++ )
			{
				COMMAND(m_sprCursor[p][i], "Change");
				m_sprCursor[p][i]->SetXY(GetCursorX(p, i), GetCursorY(p, i));
			}

			for( int i = 0; i < NUM_PREVIEW_PARTS; i++ )
			{
				COMMAND(m_sprPreview[p][iOldChoice][i], "LoseFocus");
				COMMAND(m_sprPreview[p][iNewChoice][i], "GainFocus");
			}
		}

		/* XXX: If !SharedPreviewAndCursor, this is incorrect.  (Nothing uses
		 * both icon focus and !SharedPreviewAndCursor right now.) */
		for( int i = 0; i < NUM_ICON_PARTS; i++ )
		{
			COMMAND(m_sprIcon[iOldChoice][i], "LoseFocus");
			COMMAND(m_sprIcon[iNewChoice][i], "GainFocus");
		}

		if( SHOW_SCROLLER )
		{
			if( SHARED_PREVIEW_AND_CURSOR )
			{
				m_Scroller[0].SetDestinationItem(iNewChoice);

				for( unsigned c = 0; c < m_aModeChoices.size(); c++ )
					COMMAND(*m_sprScroll[0][c], int(c) == m_iChoice[0] ? "GainFocus" : "LoseFocus");
			}
			else
			{
				m_Scroller[p].SetDestinationItem(iNewChoice);

				for( unsigned c = 0; c < m_aModeChoices.size(); c++ )
					COMMAND(*m_sprScroll[p][c], int(c) == m_iChoice[p] ? "GainFocus" : "LoseFocus");
			}
		}
	}

	return true;
}

ScreenSelectMaster::Page ScreenSelectMaster::GetPage( int iChoiceIndex ) const
{
	int iMaxChoiceIndex = 0;
	FOREACH_ENUM(Page, NUM_PAGES, page)
	{
		iMaxChoiceIndex += m_iChoicesPerPage[page];
		if( iChoiceIndex < iMaxChoiceIndex )
			return page;
	}

	if( iChoiceIndex >= iMaxChoiceIndex )
		RageException::Throw("ScreenSelectMaster: %s\n\nChoice index %d is out of range.", m_sName.c_str(), iChoiceIndex);

	return (Page)(NUM_PAGES - 1);
}

ScreenSelectMaster::Page ScreenSelectMaster::GetCurrentPage() const
{
	// Both players are guaranteed to be on the same page.
	return GetPage( m_iChoice[GAMESTATE->m_MasterPlayerNumber] );
}


float ScreenSelectMaster::DoMenuStart( PlayerNumber pn )
{
	if( m_bChosen[pn] )
		return 0;
	m_bChosen[pn] = true;

	float fSecs = 0;

	for( int page=0; page<NUM_PAGES; page++ )
	{
		OFF_COMMAND( m_sprMore[page] );
		fSecs = max( fSecs, m_sprMore[page].GetTweenTimeLeft() );
	}

	if( SHARED_PREVIEW_AND_CURSOR )
	{
		for( int i = 0; i < NUM_CURSOR_PARTS; i++ )
		{
			if( !m_bDidSharedCursorCommand )
			{
				COMMAND(m_sprCursor[0][i], "Choose");
				m_bDidSharedCursorCommand = true;
			}
			fSecs = max(fSecs, m_sprCursor[0][i]->GetTweenTimeLeft());
		}
	}
	else
	{
		for( int i = 0; i < NUM_CURSOR_PARTS; i++ )
		{
			COMMAND(m_sprCursor[pn][i], "Choose");
			fSecs = max(fSecs, m_sprCursor[pn][i]->GetTweenTimeLeft());
		}
	}

	return fSecs;
}

void ScreenSelectMaster::MenuStart( PlayerNumber pn )
{
	if( m_fLockInputSecs > 0 )
		return;
	if( m_bChosen[pn] )
		return;

	ModeChoice &mc = m_aModeChoices[m_iChoice[pn]];
	SOUND->PlayOnceFromDir( ANNOUNCER->GetPathTo(ssprintf("%s comment %s",m_sName.c_str(), mc.m_sName.c_str())) );
	SCREENMAN->PlayStartSound();

	float fSecs = 0;
	bool bAllDone = true;
	if( SHARED_PREVIEW_AND_CURSOR || GetCurrentPage() == PAGE_2 )
	{
		/* Only one player has to pick.  Choose this for all the other players, too. */
		FOREACH_HumanPlayer( pn )
		{
			ASSERT(!m_bChosen[pn]);
			fSecs = max(fSecs, DoMenuStart(pn));
		}
	}
	else
	{
		fSecs = max( fSecs, DoMenuStart(pn) );
		// check to see if everyone has chosen
		FOREACH_HumanPlayer( p )
			bAllDone &= m_bChosen[p];
	}

	if( bAllDone )
		this->PostScreenMessage( SM_BeginFadingOut, fSecs );// tell our owner it's time to move on
}

/*
 * We want all items to always run OnCommand and either GainFocus or LoseFocus on
 * tween-in.  If we only run OnCommand, then it has to contain a copy of either
 * GainFocus or LoseFocus, which implies that the default setting is hard-coded in
 * the theme.  Always run both.
 *
 * However, the actual tween-in is OnCommand; we don't always want to actually run
 * through the Gain/LoseFocus tweens during initial tween-in.  So, we run the focus
 * command first, do a FinishTweening to pop it in place, and then run OnCommand.
 * This means that the focus command should be position neutral; eg. only use "addx",
 * not "x".
 */
void ScreenSelectMaster::TweenOnScreen()
{
	for( unsigned c = 0; c < m_aModeChoices.size(); c++ )
	{
		for( int i = 0; i < NUM_ICON_PARTS; i++ )
		{
			COMMAND(m_sprIcon[c][i], (int(c) == m_iChoice[0]) ? "GainFocus" : "LoseFocus");
			m_sprIcon[c][i]->FinishTweening();
			SET_XY_AND_ON_COMMAND(m_sprIcon[c][i]);
		}
	}

	// Need to SetXY of Cursor after Icons since it depends on the Icons' positions.
	if( SHARED_PREVIEW_AND_CURSOR )
	{
		for( int i = 0; i < NUM_CURSOR_PARTS; i++ )
		{
			m_sprCursor[0][i]->SetXY(GetCursorX((PlayerNumber)0, i), GetCursorY((PlayerNumber)0, i));
			ON_COMMAND(m_sprCursor[0][i]);
		}

		for( unsigned c = 0; c < m_aModeChoices.size(); c++ )
		{
			for( int i = 0; i < NUM_PREVIEW_PARTS; i++ )
			{
				COMMAND(m_sprPreview[0][c][i], (int(c) == m_iChoice[0]) ? "GainFocus" : "LoseFocus");
				m_sprPreview[0][c][i]->FinishTweening();
				SET_XY_AND_ON_COMMAND(m_sprPreview[0][c][i]);
			}
		}

		if( SHOW_SCROLLER )
		{
			m_Scroller[0].SetCurrentAndDestinationItem(m_iChoice[0]);
			SET_XY_AND_ON_COMMAND(m_Scroller[0]);
			for( unsigned c = 0; c < m_aModeChoices.size(); c++ )
				COMMAND(*m_sprScroll[0][c], int(c) == m_iChoice[0] ? "GainFocus" : "LoseFocus");
		}
	}
	else
	{
		FOREACH_HumanPlayer(pn)
		{
			for( int i = 0; i < NUM_CURSOR_PARTS; i++ )
			{
				m_sprCursor[pn][i]->SetXY(GetCursorX(pn, i), GetCursorY(pn, i));
				ON_COMMAND(m_sprCursor[pn][i]);
			}

			int iChoice = m_iChoice[pn];

			for( unsigned c = 0; c < m_aModeChoices.size(); c++ )
			{
				for( int i = 0; i < NUM_PREVIEW_PARTS; i++ )
				{
					COMMAND(m_sprPreview[pn][c][i], int(c) == iChoice ? "GainFocus" : "LoseFocus");
					m_sprPreview[pn][c][i]->FinishTweening();
					SET_XY_AND_ON_COMMAND(m_sprPreview[pn][c][i]);
				}
			}

			if( SHOW_SCROLLER )
			{
				m_Scroller[pn].SetCurrentAndDestinationItem(iChoice);
				SET_XY_AND_ON_COMMAND(m_Scroller[pn]);
				for( unsigned c = 0; c < m_aModeChoices.size(); c++ )
					COMMAND(*m_sprScroll[pn][c], int(c) == iChoice ? "GainFocus" : "LoseFocus");
			}
		}
	}

	//We have to move page two's explanation and more off the screen
	//so it doesn't just sit there on page one.  (Thanks Zhek)

	for( int page = 0; page < NUM_PAGES; page++ )
	{
		m_sprMore[page].SetXY(999, 999);
		m_sprExplanation[page]->SetXY(999, 999);
	}

	SET_XY_AND_ON_COMMAND(m_sprExplanation[GetCurrentPage()]);
	SET_XY_AND_ON_COMMAND(m_sprMore[GetCurrentPage()]);

	this->SortByDrawOrder();
}

void ScreenSelectMaster::TweenOffScreen()
{
	if( SHARED_PREVIEW_AND_CURSOR )
	{
		for( int i = 0; i < NUM_CURSOR_PARTS; i++ )
			OFF_COMMAND(m_sprCursor[0][i]);
	}
	else
	{
		for( int i = 0; i < NUM_CURSOR_PARTS; i++ )
			FOREACH_HumanPlayer( pn )
				OFF_COMMAND(m_sprCursor[pn][i]);
	}

	for( unsigned c = 0; c < m_aModeChoices.size(); c++ )
	{
		if( GetPage(c) != GetCurrentPage() )
			continue;	// skip

		bool SelectedByEitherPlayer = false;
		if( SHARED_PREVIEW_AND_CURSOR )
		{
			if( m_iChoice[0] == (int)c )
				SelectedByEitherPlayer = true;
		}
		else
			FOREACH_HumanPlayer( p )
				if( m_iChoice[p] == (int)c )
					SelectedByEitherPlayer = true;

		for( int i = 0; i < NUM_ICON_PARTS; i++ )
		{
			OFF_COMMAND(m_sprIcon[c][i]);
			COMMAND(m_sprIcon[c][i], SelectedByEitherPlayer ? "OffFocused" : "OffUnfocused");
		}

		if( SHARED_PREVIEW_AND_CURSOR )
		{
			for( int i = 0; i < NUM_PREVIEW_PARTS; i++ )
			{
				OFF_COMMAND(m_sprPreview[0][c][i]);
				COMMAND(m_sprPreview[0][c][i], SelectedByEitherPlayer ? "OffFocused" : "OffUnfocused");
			}

			if( SHOW_SCROLLER )
				OFF_COMMAND(m_Scroller[0]);
		}
		else
		{
			FOREACH_HumanPlayer( pn )
			{
				for( int i = 0; i < NUM_PREVIEW_PARTS; i++ )
				{
					OFF_COMMAND(m_sprPreview[pn][c][i]);
					COMMAND(m_sprPreview[pn][c][i], SelectedByEitherPlayer ? "OffFocused" : "OffUnfocused");
				}

				if( SHOW_SCROLLER )
					OFF_COMMAND(m_Scroller[pn]);
			}
		}
	}

	OFF_COMMAND( m_sprExplanation[GetCurrentPage()] );
	OFF_COMMAND( m_sprMore[GetCurrentPage()] );
}

float ScreenSelectMaster::GetCursorX( PlayerNumber pn, int iPartIndex )
{
	return m_sprIcon[m_iChoice[pn]][0]->GetX() + m_vecCursorOffset[pn][iPartIndex].x;
}

float ScreenSelectMaster::GetCursorY( PlayerNumber pn, int iPartIndex )
{
	return m_sprIcon[m_iChoice[pn]][0]->GetY() + m_vecCursorOffset[pn][iPartIndex].y;
}

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * Contains code (c) 2003-2004 Chris Danford.
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
