#include "global.h"

#include "StepMania.h"

//
// Rage global classes
//
#include "RageLog.h"
#include "RageTextureManager.h"
#include "RageSoundManager.h"
#include "GameSoundManager.h"
#include "RageInput.h"
#include "RageTimer.h"
#include "RageException.h"
#include "RageDisplay.h"
#include "RageThreads.h"

#include "arch/arch.h"
#include "arch/LoadingWindow/LoadingWindow.h"
#include "arch/Dialog/Dialog.h"
#include "arch/TouchHandler/TouchHandler.h"
#include <ctime>

#include "ProductInfo.h"

#if defined(HAVE_SDL)
	#include "SDL_utils.h"
#endif

#include "Screen.h"
#include "CodeDetector.h"
#include "CommonMetrics.h"
#include "Game.h"

//
// StepMania global classes
//
#include "ThemeManager.h"
#include "NoteSkinManager.h"
#include "PrefsManager.h"
#include "SongManager.h"
#include "GameState.h"
#include "AnnouncerManager.h"
#include "ProfileManager.h"
#include "MemoryCardManager.h"
#include "ScreenManager.h"
#include "GameManager.h"
#include "FontManager.h"
#include "InputFilter.h"
#include "InputMapper.h"
#include "InputQueue.h"
#include "SongCacheIndex.h"
#include "BannerCache.h"
#include "BackgroundCache.h"
#include "UnlockSystem.h"
#include "arch/ArchHooks/ArchHooks.h"
#include "RageFileManager.h"
#include "Bookkeeper.h"
#include "LightsManager.h"
#include "ModelManager.h"
#include "CryptManager.h"
#include "NetworkSyncManager.h"

#if defined(_WIN32_WINNT)
	#include "archutils/win32/AppInstance.h"
	#include "archutils/win32/GotoURL.h"
#endif

#if defined(_MSC_VER)
	#if defined(_DEBUG)
		#pragma comment(lib, "SDLmain_debug.lib")
	#else
		#pragma comment(lib, "SDLmain.lib")
	#endif
#endif

#define ZIPS_DIR "Packages/"

#ifdef _WINDOWS
// The renderer is responsible for setting this, and updating it when it changes.
HWND g_hWndMain = NULL;
#endif

int g_argc = 0;
char **g_argv = NULL;

static bool g_bHasFocus = true;
static bool g_bQuitting = false;

static RageDisplay::VideoModeParams GetCurVideoModeParams()
{
	CString sWindowTitle = PRODUCT_NAME_VER;
	sWindowTitle += " - Theme: ";
	sWindowTitle += WINDOW_TITLE;
	return RageDisplay::VideoModeParams(
		PREFSMAN->m_bWindowed,
		PREFSMAN->m_iDisplayWidth,
		PREFSMAN->m_iDisplayHeight,
		PREFSMAN->m_iDisplayColorDepth,
		PREFSMAN->m_iRefreshRate,
		PREFSMAN->m_bVsync,
		PREFSMAN->m_bInterlaced,
		PREFSMAN->m_bSmoothLines,
		PREFSMAN->m_bTrilinearFiltering,
		PREFSMAN->m_bAnisotropicFiltering,
		sWindowTitle,
		THEME->GetPathG("Common", "window icon"),
		PREFSMAN->m_bPAL,
		PREFSMAN->m_CurrentDPI,
		PREFSMAN->m_CurrentPosition
	);
}

static void StoreActualGraphicOptions(bool initial)
{
	// find out what we actually have
	PREFSMAN->m_bWindowed = DISPLAY->GetVideoModeParams().windowed;
	PREFSMAN->m_iDisplayWidth = DISPLAY->GetVideoModeParams().width;
	PREFSMAN->m_iDisplayHeight = DISPLAY->GetVideoModeParams().height;
	PREFSMAN->m_iDisplayColorDepth = DISPLAY->GetVideoModeParams().bpp;
	PREFSMAN->m_iRefreshRate = DISPLAY->GetVideoModeParams().rate;
	PREFSMAN->m_bVsync = DISPLAY->GetVideoModeParams().vsync;
	PREFSMAN->m_CurrentDPI = DISPLAY->GetVideoModeParams().dpi;
	PREFSMAN->m_CurrentPosition = DISPLAY->GetVideoModeParams().position;

	CString log = ssprintf(
		"%s %dx%d %d color %d texture %dHz %s %s dpi %.0fx%.0f position %.0f,%.0f",
		PREFSMAN->m_bWindowed ? "Windowed" : "Fullscreen",
		PREFSMAN->m_iDisplayWidth,
		PREFSMAN->m_iDisplayHeight,
		PREFSMAN->m_iDisplayColorDepth,
		PREFSMAN->m_iTextureColorDepth,
		PREFSMAN->m_iRefreshRate,
		PREFSMAN->m_bVsync ? "Vsync" : "NoVsync",
		PREFSMAN->m_bSmoothLines ? "AA" : "NoAA",
		PREFSMAN->m_CurrentDPI.x, PREFSMAN->m_CurrentDPI.y,
		PREFSMAN->m_CurrentPosition.x, PREFSMAN->m_CurrentPosition.y
	);

	if (initial)
		LOG->Info("%s", log.c_str());
	else
		SCREENMAN->SystemMessage(log);

	Dialog::SetWindowed(DISPLAY->GetVideoModeParams().windowed);
}

void ApplyGraphicOptions(bool initial)
{
	bool bNeedReload = false;

	bNeedReload |= DISPLAY->SetVideoMode(GetCurVideoModeParams());

	DISPLAY->ChangeCentering(
		PREFSMAN->m_iCenterImageTranslateX,
		PREFSMAN->m_iCenterImageTranslateY,
		PREFSMAN->m_fCenterImageScaleX,
		PREFSMAN->m_fCenterImageScaleY
	);

	bNeedReload |= TEXTUREMAN->SetPrefs(
		RageTextureManagerPrefs(
			PREFSMAN->m_iTextureColorDepth,
			PREFSMAN->m_iMovieColorDepth,
			PREFSMAN->m_bDelayedTextureDelete,
			PREFSMAN->m_iMaxTextureResolution,
			PREFSMAN->m_bForceMipMaps
		)
	);

	bNeedReload |= MODELMAN->SetPrefs(
		ModelManagerPrefs(
			PREFSMAN->m_bDelayedModelDelete
		)
	);

	if (bNeedReload)
		TEXTUREMAN->ReloadAll();

	StoreActualGraphicOptions(initial);

	// Give the input handlers a chance to re-open devices as necessary.
	INPUTMAN->WindowReset();
}

void HandleException(CString error)
{
	if (g_bAutoRestart)
		HOOKS->RestartProgram();

	Dialog::Error(error); // throw up a pretty error dialog
}

void ExitGame()
{
	g_bQuitting = true;
}

void ResetGame(bool InitialReset)
{
	if (InitialReset) {
		ReadGamePrefsFromDisk();
		INPUTMAPPER->ReadMappingsFromDisk();

		GAMESTATE->Reset();

		if (!THEME->DoesThemeExist(THEME->GetCurThemeName())) {
			CString sGameName = GAMESTATE->GetCurrentGame()->m_szName;
			if (THEME->DoesThemeExist(sGameName))
				THEME->SwitchThemeAndLanguage(sGameName, THEME->GetCurLanguage());
			else
				THEME->SwitchThemeAndLanguage("default", THEME->GetCurLanguage());
			TEXTUREMAN->DoDelayedDelete();
		}

		SaveGamePrefsToDisk();
	}

	//
	// update last seen joysticks
	//
	vector<InputDevice> vDevices;
	vector<CString> vDescriptions;
	INPUTMAN->GetDevicesAndDescriptions(vDevices, vDescriptions);
	CString sInputDevices = join(",", vDescriptions);

	if (PREFSMAN->m_sLastSeenInputDevices != sInputDevices) {
		LOG->Info("Input devices changed from '%s' to '%s'.", PREFSMAN->m_sLastSeenInputDevices.c_str(), sInputDevices.c_str());

		if (PREFSMAN->m_bAutoMapOnJoyChange) {
			LOG->Info("Remapping joysticks.");
			INPUTMAPPER->AutoMapJoysticksForCurrentGame();
		}

		PREFSMAN->m_sLastSeenInputDevices = sInputDevices;
	}

	if (InitialReset) {
		if (PREFSMAN->m_bFirstRun)
			SCREENMAN->SetNewScreen(FIRST_RUN_INITIAL_SCREEN);
		else
			SCREENMAN->SetNewScreen(INITIAL_SCREEN);

		PREFSMAN->m_bFirstRun = false;
		PREFSMAN->SaveGlobalPrefsToDisk();	// persist FirstRun setting in case we don't exit normally
	}
}

static void GameLoop();

static bool ChangeAppPri()
{
	if (PREFSMAN->m_iBoostAppPriority == 0)
		return false;

	// if using NTPAD don't boost or else input is laggy
	if (PREFSMAN->m_iBoostAppPriority == -1) {
		vector<InputDevice> vDevices;
		vector<CString> vDescriptions;
		INPUTMAN->GetDevicesAndDescriptions(vDevices, vDescriptions);
		CString sInputDevices = join(",", vDescriptions);
		if (sInputDevices.find("NTPAD") != CString::npos) {
			LOG->Trace("Using NTPAD.  Don't boost priority.");
			return false;
		}
	}

	// If -1 and this is a debug build, don't. It makes the debugger sluggish.
#ifdef DEBUG
	if (PREFSMAN->m_iBoostAppPriority == -1)
		return false;
#endif

	return true;
}

static void BoostAppPri()
{
	if (!ChangeAppPri())
		return;

#ifdef _WINDOWS
	SetPriorityClass(GetCurrentProcess(), HIGH_PRIORITY_CLASS);
#endif
}

static void LoadDefaultVideoSettings();

static bool LoadNewVersionDefaults()
{
#if defined(HAVE_VERSION_INFO)
	if (stricmp(PREFSMAN->m_sLastCommit.c_str(), build_commit) != 0) {
		if (PREFSMAN->m_sLastCommit.empty())
			LOG->Trace("StepMania AMX was successfully installed; default settings loaded.");
		else
			LOG->Trace("StepMania AMX was successfully updated to build %s; default settings loaded.", build_commit);

		PREFSMAN->ResetToFactoryDefaults(false);
		PREFSMAN->m_sLastCommit = build_commit;
		LoadDefaultVideoSettings();
		return true;
	}
#endif
	return false;
}

static bool HasEnoughMemory()
{
#if defined(WIN32)
	// Has the amount of memory changed?
	MEMORYSTATUS mem;
	GlobalMemoryStatus(&mem);
	const size_t uMemory = mem.dwTotalPhys / 1048576;
	if (uMemory < 256) {
		CString sMemoryError = ssprintf(
			"StepMania AMX requires at least 256Mb of RAM.\n"
			"You currently have %uMb, do you really want to continue?",
			uMemory
		);
		switch (Dialog::YesNo(sMemoryError, "LowMemory"))
		{
		case Dialog::yes:
			break;
		case Dialog::no:
			return false;
		default:
			ASSERT(0);
		}
	}

	if (PREFSMAN->m_uLastSeenMemory != uMemory) {
		LOG->Trace("Memory changed from %u to %u; settings changed", PREFSMAN->m_uLastSeenMemory, uMemory);
		PREFSMAN->m_uLastSeenMemory = uMemory;

		// Let's consider 256-meg systems low-memory, and 512-meg systems high-memory.
		// Cut off at 512.  This is somewhat conservative; many 256-meg systems can
		// deal with higher memory profile settings, but some can't.
		const bool HighMemory = uMemory > 768;	// more than 768Mb
		const bool LowMemory = uMemory < 256;	// lower than 256Mb

		// Two memory-consuming features that we can disable are texture caching and
		// preloaded banners.  Texture caching can use a lot of memory; disable it for
		// low-memory systems.
		PREFSMAN->m_bDelayedTextureDelete = HighMemory;
		PREFSMAN->m_iMaxTextureResolution = LowMemory ? 1024 : (HighMemory ? 4096 : 2048);

		// Preloaded banners takes about 9k per song. Although it's smaller than the
		// actual song data, it still adds up with a lot of songs. Disable it for 64-meg
		// systems.
		PREFSMAN->m_BannerCache = LowMemory ? PrefsManager::BNCACHE_OFF : PrefsManager::BNCACHE_LOW_RES;
		PREFSMAN->m_BackgroundCache = LowMemory ? PrefsManager::BGCACHE_OFF : PrefsManager::BGCACHE_LOW_RES;
	}
#endif
	return true;
}

static bool NeedsUpdate(LoadingWindow *loading_window)
{
	// Check for Updates
	if (PREFSMAN->m_uUpdateCheckInterval == 0)
		return false;

	time_t cur_time;
	time(&cur_time);

	if ((static_cast<unsigned>(cur_time) - PREFSMAN->m_uLastUpdateCheck) <= PREFSMAN->m_uUpdateCheckInterval)
		return false;

	PREFSMAN->m_uLastUpdateCheck = static_cast<unsigned>(cur_time);

	NSMAN = new NetworkSyncManager(loading_window);

#ifndef _AMX_DEBUG_
	CString sThisVersion = PRODUCT_CYCLE_ABBR + PRODUCT_VER, sNewVersion, sUpdateURL;
	if (NSMAN->GetSMAVersion(sThisVersion, sNewVersion, sUpdateURL, loading_window)) {
		if (sThisVersion != sNewVersion) {
			CString sUpdateMessage = ssprintf(
				"A new version of StepMania AMX is available."	"\n"
				"Current Version: %s"							"\n"
				"New Version: %s"								"\n\n"
				"Do you want to download it?",
				sThisVersion.c_str(),
				sNewVersion.c_str()
			);
			switch (Dialog::YesNo(sUpdateMessage, "SMAUpdate"))
			{
			case Dialog::yes:
				GotoURL(sUpdateURL);
				return true;
			case Dialog::no:
				break;
			default:
				ASSERT(0);
			}
		}
	}
#endif
	return false;
}

#if defined(SUPPORT_D3D)
#include "RageDisplay_D3D.h"
#endif

#if defined(SUPPORT_OPENGL)
#include "RageDisplay_OGL.h"
#endif

#include "RageDisplay_Null.h"

static void LoadDefaultVideoSettings()
{
#if defined(_WINDOWS)
	HDC hdc = GetDC(g_hWndMain);
	int iDisplayWidth = GetDeviceCaps(hdc, HORZRES);
	int iDisplayHeight = GetDeviceCaps(hdc, VERTRES);
	ReleaseDC(g_hWndMain, hdc);

	if (iDisplayWidth >= 1280 && iDisplayHeight >= 720) {
		PREFSMAN->m_iDisplayWidth = 1280;
		PREFSMAN->m_iDisplayHeight = 720;
		PREFSMAN->m_iDisplayColorDepth = 32;
		PREFSMAN->m_iTextureColorDepth = 32;
		PREFSMAN->m_iMovieColorDepth = 32;
		PREFSMAN->m_bSmoothLines = true;
	}
#endif
}

RageDisplay *CreateDisplay()
{
	//
	// It's fucking 2017! Even netbooks under $100 have decent support for OpenGL,
	// even the blacklist I removed was from 15 years ago, if you still have legacy
	// hardware you probably will be better with version 5.9.2 from 2015.
	//
	// The Direct3D RageDisplay engine is incomplete and I'm too lazy to research
	// and write the missing features or backport the D3D RageDisplay engine from
	// OpenITG or SM5, so let's just use the D3D engine as a fallback when OpenGL
	// fails to initialize.
	//

	RageDisplay::VideoModeParams params(GetCurVideoModeParams());

	CString error = "A compatible video card was not found.\n"
		"Support for OpenGL or Direct3D 9.0c is required to play StepMania AMX.\n\n";

#if defined(SUPPORT_OPENGL)
	if (PREFSMAN->m_bEnableOpenGLRenderer) {
		error += "Initializing OpenGL (Hardware Acceleration)...\n";
		try {
			return new RageDisplay_OGL(params, false);
		}
		catch (RageException e) {
			error += CString(e.what()) + "\n";
		};
	}
#endif

#if defined(SUPPORT_D3D)
	if (PREFSMAN->m_bEnableDirect3DRenderer) {
		error += "Initializing Direct3D (Hardware Acceleration)...\n";
		try {
			return new RageDisplay_D3D(params);
		}
		catch (const exception &e) {
			error += CString(e.what()) + "\n";
		}
	}
#endif

#if defined(SUPPORT_OPENGL)
	if (PREFSMAN->m_bEnableOpenGLRenderer) {
		error += "Initializing OpenGL (Software Acceleration)...\n";
		try {
			return new RageDisplay_OGL(params, true);
		}
		catch (RageException e) {
			error += CString(e.what()) + "\n";
		}
	}
#endif

	if (PREFSMAN->m_bEnableNullRenderer) {
		error += "Initializing Null Renderer...\n";
		try {
			return new RageDisplay_Null(params);
		}
		catch (RageException e) {
			error += CString(e.what()) + "\n";
		}
	}

	RageException::Throw(error);
	return nullptr;
}

static void RestoreAppPri()
{
	if (!ChangeAppPri())
		return;

#ifdef _WINDOWS
	SetPriorityClass(GetCurrentProcess(), PREFSMAN->m_bPersistentInput ? NORMAL_PRIORITY_CLASS : IDLE_PRIORITY_CLASS);
#endif
}

#define GAMEPREFS_INI_PATH "Data/GamePrefs.ini"
#define STATIC_INI_PATH "Data/Static.ini"

void ChangeCurrentGame(const Game* g)
{
	ASSERT(g);

	SaveGamePrefsToDisk();
	INPUTMAPPER->SaveMappingsToDisk();	// save mappings before switching the game

	GAMESTATE->m_pCurGame = g;

	ReadGamePrefsFromDisk(false);
	INPUTMAPPER->ReadMappingsFromDisk();

	// Save the newly-selected game.
	if (!GAMESTATE->m_bEditing)
		SaveGamePrefsToDisk();
}

void ReadGamePrefsFromDisk(bool bSwitchToLastPlayedGame)
{
	ASSERT(GAMESTATE);
	ASSERT(ANNOUNCER);
	ASSERT(THEME);
	ASSERT(GAMESTATE);

	IniFile ini;
	ini.ReadFile(GAMEPREFS_INI_PATH);	// it's OK if this fails
	ini.ReadFile(STATIC_INI_PATH);	// it's OK if this fails, too

	if (bSwitchToLastPlayedGame) {
		ASSERT(GAMEMAN != NULL);
		CString sGame;
		GAMESTATE->m_pCurGame = NULL;
		if (ini.GetValue("Options", "Game", sGame))
			GAMESTATE->m_pCurGame = GAMEMAN->StringToGameType(sGame);
	}

	// If the active game type isn't actually available, revert to the default.
	if (GAMESTATE->m_pCurGame == NULL || !GAMEMAN->IsGameEnabled(GAMESTATE->m_pCurGame)) {
		if (GAMESTATE->m_pCurGame != NULL) {
			LOG->Warn("Default note skin for \"%s\" missing, reverting to \"%s\"",
				GAMESTATE->m_pCurGame->m_szName, GAMEMAN->GetDefaultGame()->m_szName);
		}
		GAMESTATE->m_pCurGame = GAMEMAN->GetDefaultGame();
	}

	// If the default isn't available, our default note skin is messed up.
	if (!GAMEMAN->IsGameEnabled(GAMESTATE->m_pCurGame))
		RageException::Throw("Default note skin for \"%s\" missing", GAMESTATE->m_pCurGame->m_szName);

	CString sGameName = GAMESTATE->GetCurrentGame()->m_szName;
	CString sAnnouncer = sGameName, sTheme = sGameName;

	// if these calls fail, the three strings will keep the initial values set above.
	ini.GetValue( "Options", "LastEditedSong",			PREFSMAN->m_sLastEditedSong );
	ini.GetValue( "Options", "LastEditedDifficulty",	PREFSMAN->m_sLastEditedDifficulty );
	ini.GetValue( sGameName, "Announcer",				sAnnouncer );
	ini.GetValue( sGameName, "Theme",					sTheme );
	ini.GetValue( sGameName, "DefaultModifiers",		PREFSMAN->m_sDefaultModifiers );

	// it's OK to call these functions with names that don't exist.
	if (!GAMESTATE->m_bEditing) {
		ANNOUNCER->SwitchAnnouncer(sAnnouncer);
		THEME->SwitchThemeAndLanguage(sTheme, PREFSMAN->m_sLanguage);
	}
}

void SaveGamePrefsToDisk()
{
	if (!GAMESTATE || !GAMESTATE->m_pCurGame)
		return;

	CString sGameName = GAMESTATE->GetCurrentGame()->m_szName;
	IniFile ini;
	ini.ReadFile(GAMEPREFS_INI_PATH);	// it's OK if this fails

	CString sGame, sAnnouncer, sTheme, sModifiers;

	if (GAMESTATE->m_bEditing)
	{
		ini.GetValue( "Options", "Game",				sGame );
		ini.GetValue( sGameName, "Announcer",			sAnnouncer );
		ini.GetValue( sGameName, "Theme",				sTheme );
		ini.GetValue( sGameName, "DefaultModifiers",	sModifiers );
	}
	else
	{
		sGame = sGameName;
		sAnnouncer = ANNOUNCER->GetCurAnnouncerName();
		sTheme = THEME->GetCurThemeName();
		sModifiers = PREFSMAN->m_sDefaultModifiers;
	}

	ini.SetValue( sGameName, "Announcer",				sAnnouncer );
	ini.SetValue( sGameName, "Theme",					sTheme );
	ini.SetValue( sGameName, "DefaultModifiers",		sModifiers );
	ini.SetValue( "Options", "Game",					sGame );
	ini.SetValue( "Options", "LastEditedSong",			PREFSMAN->m_sLastEditedSong );
	ini.SetValue( "Options", "LastEditedDifficulty",	PREFSMAN->m_sLastEditedDifficulty );

	ini.WriteFile(GAMEPREFS_INI_PATH);
}

static void OnFirstRun()
{
	// TODO: generate a machine keypair here
}

static void MountTreeOfZips(const CString &dir)
{
	vector<CString> dirs;
	dirs.push_back(dir);

	while (dirs.size()) {
		CString path = dirs.back();
		dirs.pop_back();

		if (!IsADirectory(path))
			continue;

		vector<CString> zips;
		GetDirListing(path + "/*.zip", zips, false, true);
		GetDirListing(path + "/*.smzip", zips, false, true);

		for (size_t i = 0; i < zips.size(); ++i) {
			if (!IsAFile(zips[i]))
				continue;

			LOG->Trace("VFS: found %s", zips[i].c_str());
			FILEMAN->Mount("zip", zips[i], "");
		}

		GetDirListing(path + "/*", dirs, true, true);
	}
}

static void WriteLogHeader()
{
	LOG->Info(PRODUCT_NAME_VER.c_str());

#if defined(HAVE_VERSION_INFO)
	LOG->Info("Compiled: %s", build_time);
	LOG->Info("Build: %s", build_commit);
#endif

	time_t cur_time;
	time(&cur_time);
	struct tm now;
	localtime_r(&cur_time, &now);

	LOG->Info("Log starting %.4d-%.2d-%.2d %.2d:%.2d:%.2d",
		1900 + now.tm_year, now.tm_mon + 1, now.tm_mday, now.tm_hour + 1, now.tm_min, now.tm_sec);
	LOG->Trace(" ");

	if (g_argc > 1)
	{
		CString args;
		for (int i = 1; i < g_argc; ++i)
		{
			if (i>1)
				args += " ";

			// surround all params with some marker, as they might have whitespace.
			// using [[ and ]], as they are not likely to be in the params.
			args += ssprintf("[[%s]]", g_argv[i]);
		}
		LOG->Info("Command line args (count=%d): %s", (g_argc - 1), args.c_str());
	}
}

static void ApplyLogPreferences()
{
	LOG->SetShowLogOutput(PREFSMAN->m_bShowLogOutput);
	LOG->SetLogToDisk(PREFSMAN->m_bLogToDisk);
	LOG->SetInfoToDisk(true);
	LOG->SetFlushing(PREFSMAN->m_bForceLogFlush);
	Checkpoints::LogCheckpoints(PREFSMAN->m_bLogCheckpoints);
}

// Search for the commandline argument given; eg. "test" searches for the option "--test".
// All commandline arguments are getopt_long style; short arguments (-x) are not supported.
// (As commandline arguments are not intended for common, general use, having short options
// isn't needed.)
// If argument is non-NULL, accept an argument.
bool GetCommandlineArgument(const CString &option, CString *argument, int iIndex)
{
	const CString optstr = "--" + option;

	for (int arg = 1; arg < g_argc; ++arg) {
		const CString CurArgument = g_argv[arg];

		const size_t i = CurArgument.find("=");
		CString CurOption = CurArgument.substr(0, i);
		if (CurOption.CompareNoCase(optstr))
			continue; // no match

		// Found it.
		if (iIndex) {
			--iIndex;
			continue;
		}

		if (argument) {
			if (i != CString::npos)
				*argument = CurArgument.substr(i + 1);
			else
				*argument = "";
		}

		return true;
	}

	return false;
}

// Process args first, put command line options that
// have to run before the SM window loads here
static void ProcessArgsFirst()
{
	CString Argument;
	if( GetCommandlineArgument( "test", &Argument ) )
		LOG->Info ("Test: \"%s\"", Argument.c_str() );
}

static void ProcessArgsSecond()
{
	CString Argument;
	if( GetCommandlineArgument( "test2" ) )
		LOG->Info ("Test2");

	if( GetCommandlineArgument( "netip" ) )
		NSMAN->DisplayStartupStatus();	// If we're using networking show what happend
}

#if defined(_WIN32_WINNT)

// Listed are keyboard scan code constants, taken from dinput.h
#define DIK_ESCAPE          0x01
#define DIK_1               0x02
#define DIK_2               0x03
#define DIK_3               0x04
#define DIK_4               0x05
#define DIK_5               0x06
#define DIK_6               0x07
#define DIK_7               0x08
#define DIK_8               0x09
#define DIK_9               0x0A
#define DIK_0               0x0B
#define DIK_MINUS           0x0C    // `-` on main keyboard
#define DIK_EQUALS          0x0D
#define DIK_BACK            0x0E    // backspace
#define DIK_TAB             0x0F
#define DIK_Q               0x10
#define DIK_W               0x11
#define DIK_E               0x12
#define DIK_R               0x13
#define DIK_T               0x14
#define DIK_Y               0x15
#define DIK_U               0x16
#define DIK_I               0x17
#define DIK_O               0x18
#define DIK_P               0x19
#define DIK_LBRACKET        0x1A
#define DIK_RBRACKET        0x1B
#define DIK_RETURN          0x1C    // Enter on main keyboard
#define DIK_LCONTROL        0x1D
#define DIK_A               0x1E
#define DIK_S               0x1F
#define DIK_D               0x20
#define DIK_F               0x21
#define DIK_G               0x22
#define DIK_H               0x23
#define DIK_J               0x24
#define DIK_K               0x25
#define DIK_L               0x26
#define DIK_SEMICOLON       0x27
#define DIK_APOSTROPHE      0x28
#define DIK_GRAVE           0x29    // accent grave
#define DIK_LSHIFT          0x2A
#define DIK_BACKSLASH       0x2B
#define DIK_Z               0x2C
#define DIK_X               0x2D
#define DIK_C               0x2E
#define DIK_V               0x2F
#define DIK_B               0x30
#define DIK_N               0x31
#define DIK_M               0x32
#define DIK_COMMA           0x33
#define DIK_PERIOD          0x34    // `.` on main keyboard
#define DIK_SLASH           0x35    // `/` on main keyboard
#define DIK_RSHIFT          0x36
#define DIK_MULTIPLY        0x37    // `*` on numeric keypad
#define DIK_LMENU           0x38    // left Alt
#define DIK_SPACE           0x39
#define DIK_CAPITAL         0x3A
#define DIK_F1              0x3B
#define DIK_F2              0x3C
#define DIK_F3              0x3D
#define DIK_F4              0x3E
#define DIK_F5              0x3F
#define DIK_F6              0x40
#define DIK_F7              0x41
#define DIK_F8              0x42
#define DIK_F9              0x43
#define DIK_F10             0x44
#define DIK_NUMLOCK         0x45
#define DIK_SCROLL          0x46    // Scroll Lock
#define DIK_NUMPAD7         0x47
#define DIK_NUMPAD8         0x48
#define DIK_NUMPAD9         0x49
#define DIK_SUBTRACT        0x4A    // `-` on numeric keypad
#define DIK_NUMPAD4         0x4B
#define DIK_NUMPAD5         0x4C
#define DIK_NUMPAD6         0x4D
#define DIK_ADD             0x4E    // `+` on numeric keypad
#define DIK_NUMPAD1         0x4F
#define DIK_NUMPAD2         0x50
#define DIK_NUMPAD3         0x51
#define DIK_NUMPAD0         0x52
#define DIK_DECIMAL         0x53    // `.` on numeric keypad
#define DIK_OEM_102         0x56    // `<` + `>` or `\\` + `|` on RT 102-key keyboard (Non-U.S.)
#define DIK_F11             0x57
#define DIK_F12             0x58
#define DIK_F13             0x64    //                       (NEC PC98)
#define DIK_F14             0x65    //                       (NEC PC98)
#define DIK_F15             0x66    //                       (NEC PC98)
#define DIK_NUMPADEQUALS    0x8D    // `=` on numeric keypad (NEC PC98)
#define DIK_NUMPADENTER     0x9C    // Enter on numeric keypad
#define DIK_RCONTROL        0x9D
#define DIK_NUMPADCOMMA     0xB3    // `,` on numeric keypad (NEC PC98)
#define DIK_DIVIDE          0xB5    // `/` on numeric keypad
#define DIK_SYSRQ           0xB7
#define DIK_RMENU           0xB8    // right Alt
#define DIK_PAUSE           0xC5    // Pause
#define DIK_HOME            0xC7    // Home on arrow keypad
#define DIK_UP              0xC8    // UpArrow on arrow keypad
#define DIK_PRIOR           0xC9    // PgUp on arrow keypad
#define DIK_LEFT            0xCB    // LeftArrow on arrow keypad
#define DIK_RIGHT           0xCD    // RightArrow on arrow keypad
#define DIK_END             0xCF    // End on arrow keypad
#define DIK_DOWN            0xD0    // DownArrow on arrow keypad
#define DIK_NEXT            0xD1    // PgDn on arrow keypad
#define DIK_INSERT          0xD2    // Insert on arrow keypad
#define DIK_DELETE          0xD3    // Delete on arrow keypad
#define DIK_LWIN            0xDB    // Left Windows key
#define DIK_RWIN            0xDC    // Right Windows key
#define DIK_APPS            0xDD    // AppMenu key

int ScanCodeToSMKey(DWORD scanCode)
{
	switch (scanCode)
	{
	case DIK_ESCAPE:		return KEY_ESC;			break;
	case DIK_1:				return KEY_C1;			break;
	case DIK_2:				return KEY_C2;			break;
	case DIK_3:				return KEY_C3;			break;
	case DIK_4:				return KEY_C4;			break;
	case DIK_5:				return KEY_C5;			break;
	case DIK_6:				return KEY_C6;			break;
	case DIK_7:				return KEY_C7;			break;
	case DIK_8:				return KEY_C8;			break;
	case DIK_9:				return KEY_C9;			break;
	case DIK_0:				return KEY_C0;			break;
	case DIK_MINUS:			return KEY_HYPHEN;		break;
	case DIK_EQUALS:		return KEY_EQUAL;		break;
	case DIK_BACK:			return KEY_BACK;		break;
	case DIK_TAB:			return KEY_TAB;			break;
	case DIK_Q:				return KEY_Cq;			break;
	case DIK_W:				return KEY_Cw;			break;
	case DIK_E:				return KEY_Ce;			break;
	case DIK_R:				return KEY_Cr;			break;
	case DIK_T:				return KEY_Ct;			break;
	case DIK_Y:				return KEY_Cy;			break;
	case DIK_U:				return KEY_Cu;			break;
	case DIK_I:				return KEY_Ci;			break;
	case DIK_O:				return KEY_Co;			break;
	case DIK_P:				return KEY_Cp;			break;
	case DIK_LBRACKET:		return KEY_LBRACKET;	break;
	case DIK_RBRACKET:		return KEY_RBRACKET;	break;
	case DIK_RETURN:		return KEY_ENTER;		break;
	case DIK_LCONTROL:		return KEY_LCTRL;		break;
	case DIK_A:				return KEY_Ca;			break;
	case DIK_S:				return KEY_Cs;			break;
	case DIK_D:				return KEY_Cd;			break;
	case DIK_F:				return KEY_Cf;			break;
	case DIK_G:				return KEY_Cg;			break;
	case DIK_H:				return KEY_Ch;			break;
	case DIK_J:				return KEY_Cj;			break;
	case DIK_K:				return KEY_Ck;			break;
	case DIK_L:				return KEY_Cl;			break;
	case DIK_SEMICOLON:		return KEY_SEMICOLON;	break;
	case DIK_APOSTROPHE:	return KEY_SQUOTE;		break;
	case DIK_GRAVE:			return KEY_ACCENT;		break;
	case DIK_LSHIFT:		return KEY_LSHIFT;		break;
	case DIK_BACKSLASH:		return KEY_BACKSLASH;	break;
	case DIK_OEM_102:		return KEY_BACKSLASH;	break;
	case DIK_Z:				return KEY_Cz;			break;
	case DIK_X:				return KEY_Cx;			break;
	case DIK_C:				return KEY_Cc;			break;
	case DIK_V:				return KEY_Cv;			break;
	case DIK_B:				return KEY_Cb;			break;
	case DIK_N:				return KEY_Cn;			break;
	case DIK_M:				return KEY_Cm;			break;
	case DIK_COMMA:			return KEY_COMMA;		break;
	case DIK_PERIOD:		return KEY_PERIOD;		break;
	case DIK_SLASH:			return KEY_SLASH;		break;
	case DIK_RSHIFT:		return KEY_RSHIFT;		break;
	case DIK_MULTIPLY:		return KEY_KP_ASTERISK;	break;
	case DIK_LMENU:			return KEY_LALT;		break;
	case DIK_SPACE:			return KEY_SPACE;		break;
	case DIK_CAPITAL:		return KEY_CAPSLOCK;	break;
	case DIK_F1:			return KEY_F1;			break;
	case DIK_F2:			return KEY_F2;			break;
	case DIK_F3:			return KEY_F3;			break;
	case DIK_F4:			return KEY_F4;			break;
	case DIK_F5:			return KEY_F5;			break;
	case DIK_F6:			return KEY_F6;			break;
	case DIK_F7:			return KEY_F7;			break;
	case DIK_F8:			return KEY_F8;			break;
	case DIK_F9:			return KEY_F9;			break;
	case DIK_F10:			return KEY_F10;			break;
	case DIK_NUMLOCK:		return KEY_NUMLOCK;		break;
	case DIK_SCROLL:		return KEY_SCRLLOCK;	break;
	case DIK_NUMPAD7:		return KEY_KP_C7;		break;
	case DIK_NUMPAD8:		return KEY_KP_C8;		break;
	case DIK_NUMPAD9:		return KEY_KP_C9;		break;
	case DIK_SUBTRACT:		return KEY_KP_HYPHEN;	break;
	case DIK_NUMPAD4:		return KEY_KP_C4;		break;
	case DIK_NUMPAD5:		return KEY_KP_C5;		break;
	case DIK_NUMPAD6:		return KEY_KP_C6;		break;
	case DIK_ADD:			return KEY_KP_PLUS;		break;
	case DIK_NUMPAD1:		return KEY_KP_C1;		break;
	case DIK_NUMPAD2:		return KEY_KP_C2;		break;
	case DIK_NUMPAD3:		return KEY_KP_C3;		break;
	case DIK_NUMPAD0:		return KEY_KP_C0;		break;
	case DIK_DECIMAL:		return KEY_KP_PERIOD;	break;
	case DIK_F11:			return KEY_F11;			break;
	case DIK_F12:			return KEY_F12;			break;
	case DIK_F13:			return KEY_F13;			break;
	case DIK_F14:			return KEY_F14;			break;
	case DIK_F15:			return KEY_F15;			break;
	case DIK_NUMPADEQUALS:	return KEY_KP_EQUAL;	break;
	case DIK_NUMPADENTER:	return KEY_KP_ENTER;	break;
	case DIK_RCONTROL:		return KEY_RCTRL;		break;
	case DIK_DIVIDE:		return KEY_KP_SLASH;	break;
	case DIK_SYSRQ:			return KEY_PRTSC;		break;
	case DIK_RMENU:			return KEY_RALT;		break;
	case DIK_PAUSE:			return KEY_PAUSE;		break;
	case DIK_HOME:			return KEY_HOME;		break;
	case DIK_UP:			return KEY_UP;			break;
	case DIK_PRIOR:			return KEY_PGUP;		break;
	case DIK_LEFT:			return KEY_LEFT;		break;
	case DIK_RIGHT:			return KEY_RIGHT;		break;
	case DIK_END:			return KEY_END;			break;
	case DIK_DOWN:			return KEY_DOWN;		break;
	case DIK_NEXT:			return KEY_PGDN;		break;
	case DIK_INSERT:		return KEY_INSERT;		break;
	case DIK_DELETE:		return KEY_DEL;			break;
	case DIK_LWIN:			return KEY_LMETA;		break;
	case DIK_RWIN:			return KEY_RMETA;		break;
	case DIK_APPS:			return KEY_MENU;		break;
	default:				return '?';
	}
}

// Disable annoying keys while playing
LRESULT CALLBACK SpecialKeysHook(int code, WPARAM wparam, LPARAM lparam)
{
	if (g_bQuitting)
		goto hook_ret;

	bool bAnyPlaying = GAMESTATE == nullptr ? false : GAMESTATE->AnyPlaying();
	bool bDisableSpecialKeys = PREFSMAN == nullptr ? false : PREFSMAN->m_bDisableSpecialKeys;
	bool bPersistentInput = PREFSMAN == nullptr ? false : PREFSMAN->m_bPersistentInput;

	if (bPersistentInput) {
		PKBDLLHOOKSTRUCT key = (PKBDLLHOOKSTRUCT)lparam;
		int smKey = ScanCodeToSMKey(key->scanCode);
		if (smKey != '?')
		{
			DeviceInput di(DEVICE_KEYBOARD, smKey);
			di.ts.Touch();
			if (INPUTFILTER != nullptr)
				INPUTFILTER->ButtonPressed(di, !(key->flags & LLKHF_UP));

			if (bAnyPlaying)
				return 1;
		}
	}
	else if (!g_bHasFocus)
		goto hook_ret;

	if (bDisableSpecialKeys && bAnyPlaying) {
		PKBDLLHOOKSTRUCT key = (PKBDLLHOOKSTRUCT)lparam;
		switch (wparam)
		{
		case WM_KEYDOWN:
		case WM_SYSKEYDOWN:
		{
			switch (key->vkCode)
			{
			case VK_TAB:
			case VK_SHIFT:
			case VK_CONTROL:
			case VK_MENU:	// Alt
			case VK_ESCAPE:
			case VK_LWIN:
			case VK_RWIN:
			case VK_SLEEP:
			case VK_BROWSER_SEARCH:
			case VK_BROWSER_FAVORITES:
			case VK_BROWSER_HOME:
			case VK_LAUNCH_MAIL:
			case VK_LAUNCH_MEDIA_SELECT:
			case VK_LAUNCH_APP1:
			case VK_LAUNCH_APP2:
				return 1;
			}
			break;
		}
		case WM_KEYUP:
		case WM_SYSKEYUP:
		{
			switch (key->vkCode)
			{
			case VK_LWIN:
			case VK_RWIN:
			case VK_SLEEP:
			case VK_BROWSER_SEARCH:
			case VK_BROWSER_FAVORITES:
			case VK_BROWSER_HOME:
			case VK_LAUNCH_MAIL:
			case VK_LAUNCH_MEDIA_SELECT:
			case VK_LAUNCH_APP1:
			case VK_LAUNCH_APP2:
				if (SCREENMAN != nullptr) {
					SCREENMAN->SystemMessage(ssprintf("Special Key (%lu) disabled.", key->vkCode));
					SCREENMAN->PlayInvalidSound();
				}
				// fall through
			case VK_TAB:
			case VK_SHIFT:
			case VK_CONTROL:
			case VK_MENU:	// Alt
			case VK_ESCAPE:
				return 1;
			}
		}
		}
	}

hook_ret:
	return CallNextHookEx(0, code, wparam, lparam);
}
#endif

#ifdef _XBOX
void __cdecl main()
#else
int main(int argc, char* argv[])
#endif
{
#ifdef _XBOX
	int argc = 1;
	char *argv[] = {"default.xbe"};
#endif

	g_argc = argc;
	g_argv = argv;

	// Set up arch hooks first.  This may set up crash handling.
	HOOKS = MakeArchHooks();

#if defined(_WIN32_WINNT)
	HHOOK hook = nullptr;
#endif

	CString  g_sErrorString = "";
#if !defined(DEBUG)
	// Always catch RageExceptions; they should always have distinct strings.
	try { // RageException

	 // Tricky: for other exceptions, we want a backtrace.  To do this in Windows,
	 // we need to catch the exception and force a crash.  The call stack is still
	 // there, and gets picked up by the crash handler.  (If we don't catch it, we'll
	 // get a generic, useless "abnormal termination" dialog.)  In Linux, if we do this
	 // we'll only get main() on the stack, but if we don't catch the exception, it'll
	 // just work.  So, only catch generic exceptions in Windows.
#if defined(_WINDOWS)
	try { // exception
#endif

#endif

	// Almost everything uses this to read and write files. Load this early.
	FILEMAN = new RageFileManager(argv[0]);
	FILEMAN->MountInitialFilesystems();

	// Set this up next.  Do this early, since it's needed for RageException::Throw.
	LOG = new RageLog();

	// Whew--we should be able to crash safely now!

	//
	// load preferences and mount any alternative trees.
	//
	PREFSMAN = new PrefsManager();
	LoadNewVersionDefaults();

	ApplyLogPreferences();
	WriteLogHeader();

	// Set up alternative filesystem trees.
	if (PREFSMAN->m_sAdditionalFolders != "") {
		CStringArray dirs;
		split(PREFSMAN->m_sAdditionalFolders, ",", dirs, true);
		for (unsigned i = 0; i < dirs.size(); i++)
			FILEMAN->Mount("dir", dirs[i], "");
	}
	if (PREFSMAN->m_sAdditionalSongFolders != "") {
		CStringArray dirs;
		split(PREFSMAN->m_sAdditionalSongFolders, ",", dirs, true);
		for (unsigned i = 0; i < dirs.size(); i++)
			FILEMAN->Mount("dir", dirs[i], "Songs");
	}
	MountTreeOfZips(ZIPS_DIR);

	// One of the above filesystems might contain files that affect preferences, eg Data/Static.ini.
	// Re-read preferences.
	PREFSMAN->ReadGlobalPrefsFromDisk();
	ApplyLogPreferences();

#if defined(HAVE_SDL)
	SetupSDL();
#endif

	// This needs PREFSMAN.
	Dialog::Init();

	//
	// Create game objects
	//
	GAMESTATE = new GameState;

	// This requires PREFSMAN, for PREFSMAN->m_bShowLoadingWindow
	LoadingWindow *loading_window = MakeLoadingWindow();
	if (loading_window == nullptr)
		RageException::Throw("Couldn't open any loading windows.");

	loading_window->Paint();

	srand(static_cast<unsigned>(time(nullptr)));	// seed number generator

	// Do this early, so we have debugging output if anything else fails.
	// LOG and Dialog must be set up first.
	// It shouldn't take long, but it might take a little time;
	// do this after the LoadingWindow is shown, since we don't want that to appear delayed.
	HOOKS->DumpDebugInfo();

#if defined(HAVE_TLS)
	LOG->Info("TLS is %savailable", RageThread::GetSupportsTLS() ? "" : "not ");
#endif

	if (PREFSMAN->m_bFirstRun)
		OnFirstRun();

	if (HasEnoughMemory() && !NeedsUpdate(loading_window)) {
		if (!NSMAN)
			NSMAN = new NetworkSyncManager(loading_window);

		GAMEMAN = new GameManager;
		THEME = new ThemeManager;
		ANNOUNCER = new AnnouncerManager;
		NOTESKIN = new NoteSkinManager;

		// Set up the theme and announcer.
		ReadGamePrefsFromDisk();

		// Load current theme splash
		if (PREFSMAN->m_bLoadThemeSplash) {
			CString sSplash = THEME->GetPathG("Common", "splash", true);
			if (sSplash != "")
				loading_window->SetSplash(sSplash);
			loading_window->Paint();
		}

		if (PREFSMAN->m_iSoundWriteAhead)
			LOG->Info("Sound writeahead has been overridden to %i", PREFSMAN->m_iSoundWriteAhead);
		SOUNDMAN = new RageSoundManager;
		SOUNDMAN->Init(PREFSMAN->GetSoundDrivers());
		SOUNDMAN->SetPrefs(PREFSMAN->m_fSoundVolume);
		SOUND = new GameSoundManager;

#if defined(WITH_COIN_MODE)
		BOOKKEEPER = new Bookkeeper;
#endif

		LIGHTSMAN = new LightsManager(PREFSMAN->m_sLightsDriver);
		INPUTFILTER = new InputFilter;
		INPUTMAPPER = new InputMapper;
		INPUTQUEUE = new InputQueue;
		SONGINDEX = new SongCacheIndex;
		BANNERCACHE = new BannerCache;
		BACKGROUNDCACHE = new BackgroundCache;

		// depends on SONGINDEX
		SONGMAN = new SongManager;
		SONGMAN->InitAll(loading_window);	// this takes a long time
		loading_window->SetText(ssprintf("Starting %s...", PRODUCT_NAME_VER.c_str()));

		CRYPTMAN = new CryptManager;		// need to do this before ProfileMan
		MEMCARDMAN = new MemoryCardManager;
		PROFILEMAN = new ProfileManager;
		PROFILEMAN->Init();					// must load after SONGMAN
		UNLOCKMAN = new UnlockSystem;

		delete loading_window;				// destroy this before init'ing Display

		ProcessArgsFirst();

		TOUCH = CreateTouchHandler();
		DISPLAY = CreateDisplay();

#if defined(_WIN32_WINNT)
		if (PREFSMAN->m_bDisableSpecialKeys || PREFSMAN->m_bPersistentInput) {
			AppInstance inst;
			hook = SetWindowsHookEx(
				WH_KEYBOARD_LL,
				SpecialKeysHook,
				inst.Get(),
				0
			);
			if (!hook)
				LOG->Warn("SetWindowsHookEx: Could not create hook (Error %d).", GetLastError());
		}
#endif

		DISPLAY->ChangeCentering(
			PREFSMAN->m_iCenterImageTranslateX,
			PREFSMAN->m_iCenterImageTranslateY,
			PREFSMAN->m_fCenterImageScaleX,
			PREFSMAN->m_fCenterImageScaleY);

		TEXTUREMAN = new RageTextureManager();
		TEXTUREMAN->SetPrefs(
			RageTextureManagerPrefs(
				PREFSMAN->m_iTextureColorDepth,
				PREFSMAN->m_iMovieColorDepth,
				PREFSMAN->m_bDelayedTextureDelete,
				PREFSMAN->m_iMaxTextureResolution,
				PREFSMAN->m_bForceMipMaps
			)
		);

		MODELMAN = new ModelManager;
		MODELMAN->SetPrefs(
			ModelManagerPrefs(
				PREFSMAN->m_bDelayedModelDelete
			)
		);

		StoreActualGraphicOptions(true);

		SONGMAN->PreloadSongImages();

		// This initializes objects that change the SDL event mask, and has other
		// dependencies on the SDL video subsystem, so it must be initialized after DISPLAY.
		INPUTMAN = new RageInput;

		// These things depend on the TextureManager, so do them after!
		FONT = new FontManager;
		SCREENMAN = new ScreenManager;

		// People may want to do something else while songs are loading, so do
		// this after loading songs.
		BoostAppPri();

		ResetGame();

		CodeDetector::RefreshCacheItems();

		// Initialize which courses are ranking courses here.
		SONGMAN->UpdateRankingCourses();

		// Run the second argcheck, you can do your other options here
		ProcessArgsSecond();

		// Run the main loop.
		GameLoop();
	}

	PREFSMAN->SaveGlobalPrefsToDisk();
	SaveGamePrefsToDisk();

#if !defined(DEBUG)
	}
	catch (const RageException &e) {
		// Gracefully shut down.
		g_sErrorString = e.what();
	}
#endif

	SAFE_DELETE( SCREENMAN );
    SAFE_DELETE( NSMAN );
	// Delete INPUTMAN before the other INPUTFILTER handlers, or an input
	// driver may try to send a message to INPUTFILTER after we delete it.
	SAFE_DELETE( INPUTMAN );
	SAFE_DELETE( INPUTQUEUE );
	SAFE_DELETE( INPUTMAPPER );
	SAFE_DELETE( INPUTFILTER );
	SAFE_DELETE( MODELMAN );
	SAFE_DELETE( PROFILEMAN );	// PROFILEMAN needs the songs still loaded
	SAFE_DELETE( UNLOCKMAN );
	SAFE_DELETE( CRYPTMAN );
	SAFE_DELETE( MEMCARDMAN );
	SAFE_DELETE( SONGMAN );
	SAFE_DELETE( BANNERCACHE );
	SAFE_DELETE( BACKGROUNDCACHE );
	SAFE_DELETE( SONGINDEX );
	SAFE_DELETE( SOUND );		// uses GAMESTATE, PREFSMAN
	SAFE_DELETE( PREFSMAN );
	SAFE_DELETE( GAMESTATE );
	SAFE_DELETE( GAMEMAN );
	SAFE_DELETE( NOTESKIN );
	SAFE_DELETE( THEME );
	SAFE_DELETE( ANNOUNCER );

#if defined(WITH_COIN_MODE)
	SAFE_DELETE( BOOKKEEPER );
#endif

	SAFE_DELETE( LIGHTSMAN );
	SAFE_DELETE( SOUNDMAN );
	SAFE_DELETE( FONT );
	SAFE_DELETE( TEXTUREMAN );
	SAFE_DELETE( DISPLAY );
	SAFE_DELETE( TOUCH );
	Dialog::Shutdown();
	SAFE_DELETE( LOG );
	SAFE_DELETE( FILEMAN );

#if !defined(DEBUG) && defined(_WINDOWS)
	}
	catch (const exception &e) {
		// This can be things like calling std::string::reserve(-1), or out of memory.
		// This can also happen if we throw a RageException during an actor, in which case
		// we want a crash dump.
		FAIL_M(e.what());
	}
#endif

#if defined(_WIN32_WINNT)
	if (hook)
		UnhookWindowsHookEx(hook);
#endif

	if (g_sErrorString != "")
		HandleException(g_sErrorString);

	SAFE_DELETE( HOOKS );

#ifndef _XBOX
	return 0;
#endif
}

CString SaveScreenshot(CString sDir, bool bSaveCompressed, bool bMakeSignature, int iIndex)
{
	//
	// Find a file name for the screenshot
	//
	FlushDirCache();

	vector<CString> files;
	GetDirListing(sDir + "screen*", files, false, false);
	sort(files.begin(), files.end());

	// Files should be of the form "screen######.xxx".  Ignore the extension; find
	// the last file of this form, and use the next number.  This way, we don't
	// write the same screenshot number for different formats (screen00011.bmp,
	// screen00011.jpg), and we always increase from the end, so if screen00003.jpg
	// is deleted, we won't fill in the hole (which makes screenshots hard to find).
	if (iIndex == -1) {
		iIndex = 0;

		for (int i = files.size() - 1; i >= 0; --i) {
			static Regex re("^screen([0-9]{5})\\....$");
			vector<CString> matches;
			if (!re.Compare(files[i], matches))
				continue;

			ASSERT(matches.size() == 1);
			iIndex = atoi(matches[0]) + 1;
			break;
		}
	}

	//
	// Save the screenshot
	//
	// If writing lossy to a memcard, use SAVE_LOSSY_LOW_QUAL, so we don't eat up
	// lots of space with screenshots.
	RageDisplay::GraphicsFileFormat fmt;
	if (bSaveCompressed && MEMCARDMAN->PathIsMemCard(sDir))
		fmt = RageDisplay::SAVE_LOSSY_LOW_QUAL;
	else if (bSaveCompressed)
		fmt = RageDisplay::SAVE_LOSSY_HIGH_QUAL;
	else
		fmt = RageDisplay::SAVE_LOSSLESS;

	CString sFileName = ssprintf("screen%05d.%s", iIndex, bSaveCompressed ? "jpg" : "bmp");
	CString sPath = sDir + sFileName;
	bool bResult = DISPLAY->SaveScreenshot(sPath, fmt);
	if (!bResult) {
		SCREENMAN->PlayInvalidSound();
		return "";
	}

	SCREENMAN->PlayScreenshotSound();

	// We wrote a new file, and SignFile won't pick it up unless we invalidate
	// the Dir cache.  There's got to be a better way of doing this than
	// thowing out all the cache. -Chris
	FlushDirCache();

	if (PREFSMAN->m_bSignProfileData && bMakeSignature)
		CryptManager::SignFileToFile(sPath);

	return sFileName;
}

#if defined(WITH_COIN_MODE)
//
// Coin Mode
//
void InsertCoin(int iNum, bool bFree)
{
	GAMESTATE->m_iCoins += iNum;

	//LOG->Trace("%i coins inserted, %i needed to play", GAMESTATE->m_iCoins, PREFSMAN->m_iCoinsPerCredit);

	if (!bFree)
		BOOKKEEPER->CoinInserted();

	SCREENMAN->RefreshCreditsMessages();

	if (iNum >= PREFSMAN->m_iCoinsPerCredit || GAMESTATE->m_iCoins % PREFSMAN->m_iCoinsPerCredit == 0)
		SCREENMAN->PlayCreditSound();
	else
		SCREENMAN->PlayCoinSound();
}

void InsertCredit(int iNum, bool bFree)
{
	InsertCoin(iNum * PREFSMAN->m_iCoinsPerCredit, bFree);
}
#endif

// Returns true if the key has been handled and should be discarded, false if
// the key should be sent on to screens.
bool HandleGlobalInputs(DeviceInput DeviceI, InputEventType type, GameInput GameI, MenuInput MenuI, StyleInput StyleI)
{
	// None of the globals keys act on types other than FIRST_PRESS
	if (type != IET_FIRST_PRESS)
		return false;

	switch (MenuI.button)
	{
	case MENU_BUTTON_OPERATOR:
		// Global operator key, to get quick access to the options menu. Don't do this if we're on a "system menu".
		if (!GAMESTATE->m_bIsOnSystemMenu)
			SCREENMAN->SendMessageToTopScreen(SM_MenuOperator);
		return true;

#if defined(WITH_COIN_MODE)
	case MENU_BUTTON_COIN:
		// Handle a coin insertion.
		if (GAMESTATE->m_bEditing) {	// no coins while editing
			LOG->Trace("Ignored coin insertion (editing)");
			break;
		}
		InsertCoin();
		return false;	// Attract need to know because they go to TitleMenu on > 1 credit
#endif

	default:;
	}

	const int iKeyFlag = INPUTFILTER->GetModifierKeyFlag();
	if (DeviceI == DeviceInput(DEVICE_KEYBOARD, KEY_F2)) {
		if (iKeyFlag & HOLDING_SHIFT) {
			// HACK: Also save bookkeeping and profile info for debugging
			// so we don't have to play through a whole song to get new output.

#if defined(WITH_COIN_MODE)
			BOOKKEEPER->WriteToDisk();
#endif

			PROFILEMAN->SaveMachineProfile();
			FOREACH_PlayerNumber(p) {
				if (PROFILEMAN->IsUsingProfile(p))
					PROFILEMAN->SaveProfile(p);
			}
			SCREENMAN->SystemMessage("Stats saved");
		}
		else {
			THEME->ReloadMetrics();
			TEXTUREMAN->ReloadAll();
			SCREENMAN->ReloadCreditsText();
			NOTESKIN->RefreshNoteSkinData(GAMESTATE->m_pCurGame);
			CodeDetector::RefreshCacheItems();
			SCREENMAN->SystemMessage("Reloaded metrics and textures");
		}

		return true;
	}

#ifndef DARWIN
	if (DeviceI == DeviceInput(DEVICE_KEYBOARD, KEY_F4)) {
		if (iKeyFlag & HOLDING_ALT) {
			// pressed Alt+F4
			ExitGame();
			return true;
		}
	}
#else
	if (DeviceI == DeviceInput(DEVICE_KEYBOARD, KEY_Cq)) {
		if (iKeyFlag & HOLDING_META) {
			// pressed CMD-Q
			ExitGame();
			return true;
		}
	}
#endif

	// The default Windows message handler will capture the desktop window upon
	// pressing PrntScrn, or will capture the foregroud with focus upon pressing
	// Alt+PrntScrn.  Windows will do this whether or not we save a screenshot
	// ourself by dumping the frame buffer.
	//
	// Pressing F13 on an Apple keyboard sends KEY_PRINT.
	// However, notebooks don't have F13. Use cmd-F12 then
	// "if pressing PrintScreen and not pressing Alt"
	if ((DeviceI == DeviceInput(DEVICE_KEYBOARD, KEY_PRTSC) && !(iKeyFlag & HOLDING_ALT)) ||
		(DeviceI == DeviceInput(DEVICE_KEYBOARD, KEY_F12) && (iKeyFlag & HOLDING_META)))
	{
		// If holding LShift save uncompressed, else save compressed
		bool bSaveCompressed = !(iKeyFlag & HOLDING_LSHIFT);
		SaveScreenshot("Screenshots/", bSaveCompressed, false);
		return true;	// handled
	}

	if (DeviceI == DeviceInput(DEVICE_KEYBOARD, KEY_ENTER)) {
		if (iKeyFlag & HOLDING_ALT) {
			if (PREFSMAN->m_bDisableSpecialKeys && GAMESTATE->AnyPlaying()) {
				SCREENMAN->SystemMessage("Special key combination (Alt+Enter) disabled.");
				SCREENMAN->PlayInvalidSound();
			}
			else {
				// alt-enter
				PREFSMAN->m_bWindowed = !PREFSMAN->m_bWindowed;
				ApplyGraphicOptions();
				return true;
			}
		}
	}

	if (DeviceI == DeviceInput(DEVICE_KEYBOARD, KEY_PAUSE)) {
		static bool bMute = false;
		bMute = !bMute;
		SOUNDMAN->SetPrefs(bMute ? 0 : PREFSMAN->m_fSoundVolume);
		SCREENMAN->SystemMessage(bMute ? "Mute on" : "Mute off");
		return true;
	}

#if defined(_WIN32_WINNT)
	// Message for disabled Windows Key combinations
	if (PREFSMAN->m_bDisableSpecialKeys && GAMESTATE->AnyPlaying()) {
		CString sCombination;
		if (DeviceI == DeviceInput(DEVICE_KEYBOARD, KEY_TAB) && iKeyFlag & HOLDING_ALT)
			sCombination = "Alt+Tab";
		else if (DeviceI == DeviceInput(DEVICE_KEYBOARD, KEY_ESC) && iKeyFlag & HOLDING_CTRL)
			sCombination = "Ctrl+ESC";
		else
			return false;

		SCREENMAN->SystemMessage(ssprintf("Special key combination (%s) disabled.", sCombination.c_str()));
		SCREENMAN->PlayInvalidSound();
		return true;
	}
#endif

	return false;
}

static void HandleInputEvents(float fDeltaTime)
{
	INPUTFILTER->Update(fDeltaTime);

	// Hack: If the topmost screen hasn't been updated yet, don't process input, since
	// we must not send inputs to a screen that hasn't at least had one update yet. (The
	// first Update should be the very first thing a screen gets.)  We'll process it next
	// time.  Do call Update above, so the inputs are read and timestamped.
	if (SCREENMAN->GetTopScreen()->IsFirstUpdate())
		return;

	static InputEventArray ieArray;
	ieArray.clear();	// empty the array
	INPUTFILTER->GetInputEvents(ieArray);

	// If we don't have focus discard input, unless it's persistent.
	if (!g_bHasFocus && !PREFSMAN->m_bPersistentInput)
		return;

	for (size_t i = 0; i<ieArray.size(); i++) {
		DeviceInput DeviceI = static_cast<DeviceInput>(ieArray[i]);
		InputEventType type = ieArray[i].type;
		GameInput GameI;
		MenuInput MenuI;
		StyleInput StyleI;

		INPUTMAPPER->DeviceToGame(DeviceI, GameI);

		if (GameI.IsValid()) {
			if (type == IET_FIRST_PRESS)
				INPUTQUEUE->RememberInput(GameI);
			INPUTMAPPER->GameToMenu(GameI, MenuI);
			INPUTMAPPER->GameToStyle(GameI, StyleI);
		}

		if (HandleGlobalInputs(DeviceI, type, GameI, MenuI, StyleI))
			continue;	// skip

		// check back in event mode
		if (PREFSMAN->m_bEventMode &&
			CodeDetector::EnteredCode(GameI.controller, CodeDetector::CODE_BACK_IN_EVENT_MODE))
		{
			MenuI.player = PLAYER_1;
			MenuI.button = MENU_BUTTON_BACK;
		}

#if defined(WITH_COIN_MODE)
		// Aldonio's Code!
		if (GAMESTATE->m_pCurSong == NULL && GAMESTATE->m_pCurCourse == NULL) {
			if (CodeDetector::EnteredCode(GameI.controller, CodeDetector::CODE_ALDONIOS_CODE))
				GAMESTATE->AldoniosCode();
		}
#endif

		SCREENMAN->Input(DeviceI, type, GameI, MenuI, StyleI);
	}
}

void FocusChanged(bool bHasFocus)
{
	g_bHasFocus = bHasFocus;

	LOG->Trace("App %s focus", g_bHasFocus ? "has" : "doesn't have");

	// If we lose focus, we may lose input events, especially key releases.
	INPUTFILTER->Reset();

	if (g_bHasFocus)
		BoostAppPri();
	else
		RestoreAppPri();
}

static void CheckSkips(float fDeltaTime)
{
	if (!PREFSMAN->m_bLogSkips)
		return;

	static int iLastFPS = 0;
	int iThisFPS = DISPLAY->GetFPS();

	// If vsync is on, and we have a solid framerate (vsync == refresh and we've sustained this
	// for at least one frame), we expect the amount of time for the last frame to be 1/FPS.

	if (iThisFPS != DISPLAY->GetVideoModeParams().rate || iThisFPS != iLastFPS) {
		iLastFPS = iThisFPS;
		return;
	}

	const float fExpectedTime = 1.0f / iThisFPS;
	const float fDifference = fDeltaTime - fExpectedTime;
	if (fabsf(fDifference) > 0.002f && fabsf(fDifference) < 0.100f) {
		LOG->Trace("GameLoop timer skip: %i FPS, expected %.3f, got %.3f (%.3f difference)",
			iThisFPS, fExpectedTime, fDeltaTime, fDifference);
	}
}

static void GameLoop()
{
#if defined(UNIX)
	bool bFirstLoop = true;
#endif
	RageTimer timer;
	while (!g_bQuitting) {
		//
		// Update
		//
		float fDeltaTime = timer.GetDeltaTime();

		CheckSkips(fDeltaTime);

		if (INPUTFILTER->IsBeingPressed(DeviceInput(DEVICE_KEYBOARD, KEY_TAB))) {
			if (INPUTFILTER->IsBeingPressed(DeviceInput(DEVICE_KEYBOARD, KEY_ACCENT)))
				fDeltaTime = 0; // both; stop time
			else
				fDeltaTime *= 4;
		}
		else if (INPUTFILTER->IsBeingPressed(DeviceInput(DEVICE_KEYBOARD, KEY_ACCENT))) {
			fDeltaTime /= 4;
		}

		DISPLAY->Update(fDeltaTime);

		// Update SOUNDMAN early (before any RageSound::GetPosition calls), to flush position data.
		SOUNDMAN->Update(fDeltaTime);

		// Update song beat information -before- calling update on all the classes that
		// depend on it.  If you don't do this first, the classes are all acting on old
		// information and will lag.  (but no longer fatally, due to timestamping -glenn)
		SOUND->Update(fDeltaTime);
		TEXTUREMAN->Update(fDeltaTime);
		GAMESTATE->Update(fDeltaTime);
		SCREENMAN->Update(fDeltaTime);
		MEMCARDMAN->Update(fDeltaTime);
		NSMAN->Update(fDeltaTime);

		// Important:  Process input AFTER updating game logic, or input will be acting on song beat from last frame
		HandleInputEvents(fDeltaTime);

		LIGHTSMAN->Update(fDeltaTime);

		HOOKS->Update(fDeltaTime);

		//
		// Render
		//
#ifndef _AMX_DEBUG_
		if (g_bHasFocus || PREFSMAN->m_bPersistentInput) {
#endif // _AMX_DEBUG_
			SCREENMAN->Draw();

			// In Windows, we want to give up some CPU for other threads.
			// Most OS's do this more intelligently.
			if (PREFSMAN->m_bGameLoopDelay) {
				unsigned msecs = PREFSMAN->m_uGameLoopDelayMilliseconds;
				if (msecs == 0)
					msecs = PREFSMAN->m_bWindowed ? DEFAULT_GAMELOOP_DELAY : DEFAULT_GAMELOOP_DELAY_HIGH_PERFORMANCE;
				usleep(msecs * 1000u);	// give some time to other processes and threads
			}
#ifndef _AMX_DEBUG_
		}
		// If we don't have focus, give up lots of CPU.
		else
			usleep(10000);	// 10ms
#endif // _AMX_DEBUG_

#if defined(UNIX)
		// HACK - Aldo_MX: When reloading the binary, the graphics get screwed :/
		if (bFirstLoop) {
			bFirstLoop = false;
			ApplyGraphicOptions();
		}
#endif
	}

	GAMESTATE->EndGame();
}

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * Contains code (c) 2001-2004 Chris Danford, Glenn Maynard.
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
