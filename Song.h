/* Song - Holds all music metadata and steps for one song. */

#ifndef SONG_H
#define SONG_H

#include "PlayerNumber.h"
#include "GameConstantsAndTypes.h"
#include "Grade.h"
#include "TimingData.h"

class BPMRange;
class Steps;
class Style;
class NotesLoader;
class LyricsLoader;
class Profile;
class StepsID;

#define MAX_EDITS_PER_SONG_PER_PROFILE	5
#define MAX_EDITS_PER_SONG				5*NUM_PROFILE_SLOTS

extern const int FILE_CACHE_VERSION;
extern const float DEFAULT_MUSIC_SAMPLE_LENGTH;


struct BGChange
{
	BGChange() { m_fBeat=-1; m_fRate=1; m_bFadeLast=false; m_bRewindMovie=false; m_bLoop=true; };
	BGChange( float s, CString n, float r=1.f, bool f=false, bool m=false, bool l=true ) { m_fBeat=s; m_sBGName=n; m_fRate=r; m_bFadeLast=f; m_bRewindMovie=m; m_bLoop=l; };
	float		m_fBeat;
	CString		m_sBGName;
	float		m_fRate;
	bool		m_bFadeLast;
	bool		m_bRewindMovie;
	bool		m_bLoop;
};

struct BGChangeArray : public vector<BGChange>
{
	CString GetString( CString separator = "," ) const;
	void FromString( CString& bgchanges, CString separator = "," );
};

void SortBGChangesArray( BGChangeArray &arrayBGChanges );

struct Lyric
{
	float		m_fStartTime;
	CString		m_sLyric;
	RageColor	m_Color;
};

struct LyricArray : public vector<Lyric>
{
	CString GetString( CString separator = "�" ) const;
	void FromString( CString& lyrics, CString separator = "�" );
};

class Song
{
	CString m_sSongDir;

public:
	Song();
	~Song();
	void Reset();

	bool IsPlayerSong();

	NotesLoader *MakeLoader( CString sDir ) const;

	bool DetectSongInfoFromPIUDir( bool bGetArtistAndTitle = true );
	bool LoadFromSongDir( CString sDir );
	bool LoadFromPlayerSongDir( CString sDir, CString sGroupName );

	void TidyUpData();	// call after loading to clean up invalid data
	void ReCalculateRadarValuesAndLastBeat();	// called by TidyUpData, and after saving
	void TranslateTitles();	// called by TidyUpData

	void TidyUpBeforeSave();
	void SaveToSMFile( CString sFullPath, bool bSavingCache ) const;
	void Save() const;	// saves SM and DWI
	void AutoSave( CString& sGetFilename ) const;
	void SaveToCacheFile() const;
	void SaveToDWIFile() const;

	const CString &GetSongFilePath() const;
	CString GetCacheFilePath() const;
	CString GetBackupFilePath( const CString& extension ) const;

	void AddAutoGenNotes();
	void AutoGen( StepsType ntTo, StepsType ntFrom );	// create Steps of type ntTo from Steps of type ntFrom
	void RemoveAutoGenNotes();

	/* Directory this song data came from: */
	const CString &GetSongDir() const { return m_sSongDir; }

	/* Filename associated with this file.  This will always have
	 * an .SM extension.  If we loaded an .SM, this will point to
	 * it, but if we loaded any other type, this will point to a
	 * generated .SM filename. */
	CString		m_sSongFileName;
	CString		m_sSongFolderName;

	CString		m_sGroupName;
	CString		m_sGenre;
	int			m_iListSortPosition;

	ProfileSlot	m_LoadedFromProfile;	// PROFILE_SLOT_INVALID if wasn't loaded from a profile
	bool		m_bIsSymLink;

	CString		m_sMainTitle, m_sSubTitle, m_sArtist;
	CString		m_sMainTitleTranslit, m_sSubTitleTranslit, m_sArtistTranslit;

	/* If PREFSMAN->m_bShowNative is off, these are the same as GetTranslit* below.
	 * Otherwise, they return the main titles. */
	CString GetDisplayMainTitle() const;
	CString GetDisplaySubTitle() const;
	CString GetDisplayArtist() const;

	/* Returns the transliterated titles, if any; otherwise returns the main titles. */
	CString GetTranslitMainTitle() const { return m_sMainTitleTranslit.size()? m_sMainTitleTranslit: m_sMainTitle; }
	CString GetTranslitSubTitle() const { return m_sSubTitleTranslit.size()? m_sSubTitleTranslit: m_sSubTitle; }
	CString GetTranslitArtist() const { return m_sArtistTranslit.size()? m_sArtistTranslit:m_sArtist; }

	/* "title subtitle" */
	CString GetFullDisplayTitle() const;
	CString GetFullTranslitTitle() const;

	CString		m_sIntroFile;
	CString		m_sMusicFile;
	float		m_fMusicLengthSeconds;
	float		m_fMusicSampleStartSeconds;
	float		m_fMusicSampleLengthSeconds;
	SelectionDisplay	m_SelectionDisplay;

	void SetDefaultMusicSampleStart();
	void SetDefaultMusicSampleLength();

	CString		m_sBannerFile;
	CString		m_sDiscFile;
	CString		m_sBackgroundFile;
	CString		m_sPreviewFile;
	CString		m_sLyricsFile;
	CString		m_sCDTitleFile;

	CString		m_sMenuColor;

	CString GetIntroPath() const;
	CString GetMusicPath() const;
	CString GetBannerPath() const;
	CString GetDiscPath() const;
	CString	GetLyricsPath() const;
	CString GetBackgroundPath() const;
	CString GetCDTitlePath() const;
	CString GetPreviewPath() const;

	// For avoiding UNLOCKMAN bug when writing cache files
	bool m_bCreatingSongCache;

	/* For loading only: */
	bool m_bHasIntro, m_bHasMusic, m_bHasBanner, m_bHasDisc, m_bHasBackground, m_bHasPreview;

	bool HasIntro() const;
	bool HasMusic() const;
	bool HasBanner() const;
	bool HasDisc() const;
	bool HasBackground() const;
	bool HasCDTitle() const;
	bool HasPreview() const;
	bool HasMovieBackground() const;
	bool HasBGChanges() const;
	bool HasLyrics() const;

	bool Matches(CString sGroup, CString sSong) const;

	TimingData		m_Timing;
	BGChangeArray	m_BGChanges;	// this must be sorted before gameplay
	BGChangeArray	m_FGChanges;	// this must be sorted before gameplay
	LyricArray		m_Lyrics;		// this must be sorted before gameplay

	void AddBGChange( BGChange seg );
	void AddForegroundChange( BGChange seg );
	void AddLyric( Lyric seg );

	CString GetBackgroundAtBeat( float fBeat ) const;
	bool HasSignificantBpmChangesOrStops() const;

	bool SongCompleteForStyle( const Style *st ) const;
	bool HasStepsType( StepsType st ) const;
	bool HasStepsTypeAndDifficulty( StepsType st, Difficulty dc ) const;
	const vector<Steps*>& GetAllSteps( StepsType st=STEPS_TYPE_INVALID ) const { return st==STEPS_TYPE_INVALID? m_vpSteps:m_vpStepsByType[st]; }
	vector<Steps*>& GetAllSteps( StepsType st=STEPS_TYPE_INVALID ) { return st==STEPS_TYPE_INVALID? m_vpSteps:m_vpStepsByType[st]; }
	void GetSteps( vector<Steps*>& arrayAddTo, StepsType st = STEPS_TYPE_INVALID, Difficulty dc = DIFFICULTY_INVALID, int iMeterLow = -1, int iMeterHigh = -1, const CString &sDescription = "", bool bIncludeAutoGen = true, int Max = -1 ) const
	{
		vector<StepsType> arrayStepsType;
		arrayStepsType.push_back( st );
		GetSteps( arrayAddTo, arrayStepsType, dc, iMeterLow, iMeterHigh, sDescription, bIncludeAutoGen, Max );
	}
	void GetSteps( vector<Steps*>& arrayAddTo, vector<StepsType>& arrayStepsType, Difficulty dc = DIFFICULTY_INVALID, int iMeterLow = -1, int iMeterHigh = -1, const CString &sDescription = "", bool bIncludeAutoGen = true, int Max = -1 ) const;
	Steps* GetStepsByDifficulty( StepsType st, Difficulty dc, bool bIncludeAutoGen = true ) const;
	Steps* GetStepsByMeter( StepsType st, int iMeterLow, int iMeterHigh ) const;
	Steps* GetStepsByDescription( StepsType st, CString sDescription ) const;
	Steps* GetClosestNotes( StepsType st, Difficulty dc ) const;
	bool IsEasy( StepsType st ) const;
	bool IsTutorial() const;
	bool HasEdits( StepsType st ) const;
	SelectionDisplay GetDisplayed() const;
	bool NormallyDisplayed() const;
	bool NeverDisplayed() const;
	bool RouletteDisplayed() const;
	bool HideUntilXStages() const;
	bool ShowInDemonstrationAndRanking() const;

	int ShowWhenXStages() const;

	bool ShowOnExtra1() const;
	bool ShowOnExtra2() const;

	void AddSteps( Steps* pSteps );		// we are responsible for deleting the memory pointed to by pSteps!
	void RemoveSteps( const Steps* pSteps );

	void FreeAllLoadedFromProfiles();
	bool WasLoadedFromProfile() const { return m_LoadedFromProfile != PROFILE_SLOT_INVALID; }
	int GetNumStepsLoadedFromProfile( ProfileSlot slot ) const;
	bool IsEditAlreadyLoaded( Steps* pSteps ) const;

	BPMRange* GetSortByBPMRange() const;

private:
	bool m_bIsPlayerSong;
	bool m_bMusicFailPlayerSong;

	void AdjustDuplicateSteps(); // part of TidyUpData
	void DeleteDuplicateSteps( vector<Steps*> &vSteps );

	vector<Steps*> m_vpSteps;
	vector<Steps*> m_vpStepsByType[NUM_STEPS_TYPES];

	mutable BPMRange* m_SortByBPMRange;
};

#endif

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * Contains code (c) 2001-2004 Chris Danford, Glenn Maynard.
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
