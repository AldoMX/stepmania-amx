#include "global.h"

#include "ScreenJukebox.h"
#include "RageLog.h"
#include "ThemeManager.h"
#include "GameState.h"
#include "SongManager.h"
#include "StepMania.h"
#include "ScreenManager.h"
#include "GameSoundManager.h"
#include "Steps.h"
#include "ScreenAttract.h"
#include "RageUtil.h"
#include "UnlockSystem.h"

// HACK: This belongs in ScreenDemonstration
#define DIFFICULTIES_TO_SHOW		THEME->GetMetric(sName,"DifficultiesToShow")

const ScreenMessage	SM_NotesEnded	= ScreenMessage(SM_User+10);	// MUST be same as in ScreenGameplay


bool ScreenJukebox::SetSong( CString sName, bool bDemonstration )
{
	vector<Song*> vSongs;
	if( GAMESTATE->m_sPreferredGroup == GROUP_ALL_MUSIC )
		SONGMAN->GetSongs( vSongs );
	else
		SONGMAN->GetSongs( vSongs, GAMESTATE->m_sPreferredGroup );

	//
	// Calculate what difficulties to show
	//
	CStringArray asBits;
	vector<Difficulty> vDifficultiesToShow;
	if( bDemonstration )
	{
		split( DIFFICULTIES_TO_SHOW, ",", asBits );
		for( unsigned i=0; i<asBits.size(); i++ )
		{
			Difficulty dc = StringToDifficulty( asBits[i] );
			if( dc != DIFFICULTY_INVALID )
				vDifficultiesToShow.push_back( dc );
		}
	}
	else
	{
		if( GAMESTATE->m_PreferredDifficulty[PLAYER_1] != DIFFICULTY_INVALID )
		{
			vDifficultiesToShow.push_back( GAMESTATE->m_PreferredDifficulty[PLAYER_1] );
		}
		else
		{
			FOREACH_Difficulty( dc )
				vDifficultiesToShow.push_back( dc );
		}
	}

	if( vDifficultiesToShow.empty() )
		vDifficultiesToShow.push_back( DIFFICULTY_EASY );

	//
	// Search for a Song and Steps to play during the demo
	//
	for( int i=0; i<1000; i++ )
	{
		if( vSongs.size() == 0 )
			return true;

		Song* pSong = vSongs[rand()%vSongs.size()];

		if( !pSong->HasMusic() )
			continue;	// skip
		if( UNLOCKMAN->SongIsLocked(pSong) )
			continue;
		if( !pSong->ShowInDemonstrationAndRanking() )
			continue;	// skip

		Difficulty dc = vDifficultiesToShow[ rand()%vDifficultiesToShow.size() ];
		Steps* pSteps = pSong->GetStepsByDifficulty( GAMESTATE->GetCurrentStyle()->m_StepsType, dc );

		if( pSteps == NULL )
			continue;	// skip

		if( !PREFSMAN->m_bAutogenSteps && pSteps->IsAutogen())
			continue;	// skip

		// Found something we can use!
		GAMESTATE->m_pCurSong = pSong;
		FOREACH_PlayerNumber( p )
			GAMESTATE->m_pCurSteps[p] = pSteps;

		return true;	// done looking
	}

	return false;
}

bool ScreenJukebox::PrepareForJukebox( CString sName, bool bDemonstration )		// always return true.
{
	// ScreeJukeboxMenu must set this
	ASSERT( GAMESTATE->GetCurrentStyle() );
	GAMESTATE->m_PlayMode = PLAY_MODE_REGULAR;

	SetSong( sName, bDemonstration );

	ASSERT( GAMESTATE->m_pCurSong );

	GAMESTATE->m_MasterPlayerNumber = PLAYER_1;

	FOREACH_PlayerNumber( p )
	{
		if( !GAMESTATE->IsPlayerEnabled(p) )
			continue;

		if( GAMESTATE->m_bJukeboxUsesModifiers )
		{
			GAMESTATE->m_PlayerOptions[p].Init();
			GAMESTATE->m_PlayerOptions[p].FromString( PREFSMAN->m_sDefaultModifiers );
			GAMESTATE->m_PlayerOptions[p].ChooseRandomModifiers();
		}
	}

	GAMESTATE->m_SongOptions.Init();
	GAMESTATE->m_SongOptions.FromString( PREFSMAN->m_sDefaultModifiers );

	GAMESTATE->m_SongOptions.m_FailType = SongOptions::FAIL_OFF;

	GAMESTATE->m_bDemonstrationOrJukebox = true;

	return true;
}

ScreenJukebox::ScreenJukebox( CString sName, bool bDemonstration ) : ScreenGameplay( "ScreenGameplay", PrepareForJukebox( sName, bDemonstration ), !bDemonstration )	// this is a hack to get some code to execute before the ScreenGameplay constructor
{
	LOG->Trace( "ScreenJukebox::ScreenJukebox()" );
	m_sScreenName = sName;

	if( GAMESTATE->m_pCurSong == NULL )	// we didn't find a song.
	{
		this->PostScreenMessage( SM_GoToNextScreen, 0 );	// Abort demonstration.
		return;
	}

	m_In.Load( THEME->GetPathB(m_sScreenName, "in") );
	this->AddChild( &m_In );
	m_In.StartTransitioning();

	m_Out.Load( THEME->GetPathB(m_sScreenName, "out") );
	this->AddChild( &m_Out );

	m_Back.Load( THEME->GetPathB(m_sScreenName, "back") );
	this->AddChild( &m_Back );

	ClearMessageQueue();	// remove all of the messages set in ScreenGameplay that animate "ready", "here we go", etc.

	GAMESTATE->m_bPastHereWeGo = true;

	m_DancingState = STATE_DANCING;

	m_bPrevScreen = false;
}

void ScreenJukebox::Input( const DeviceInput& DeviceI, const InputEventType type, const GameInput &GameI, const MenuInput &MenuI, const StyleInput &StyleI )
{
	//LOG->Trace( "ScreenJukebox::Input()" );

	if( DeviceI.device == DEVICE_KEYBOARD )
	{
		const int iKeyFlag = INPUTFILTER->GetModifierKeyFlag();

		if( DeviceI.button == KEY_F8 && PREFSMAN->m_bJukeboxGameplay )
		{
			if( type == IET_FIRST_PRESS )
			{
				bool bAutoPlay = false;

				FOREACH_HumanPlayer( pn )
				{
					if( !(iKeyFlag & HOLDING_SHIFT) || ( pn == PLAYER_1 && iKeyFlag & HOLDING_LSHIFT ) || ( pn == PLAYER_2 && iKeyFlag & HOLDING_RSHIFT ) )
					{
						GAMESTATE->m_PlayerOptions[pn].m_bAutoPlay ^= 1;
						GAMESTATE->m_PlayerController[pn] = GAMESTATE->m_PlayerOptions[pn].m_bAutoPlay ? PC_CPU : PC_HUMAN;

						bAutoPlay |= GAMESTATE->m_PlayerOptions[pn].m_bAutoPlay;
					}
				}

				PREFSMAN->m_bAutoPlay = bAutoPlay;

				m_textDebug.SetText( ssprintf("AutoPlay turned %s", bAutoPlay ? "ON" : "OFF") );
				m_textDebug.StopTweening();
				m_textDebug.SetDiffuse( RageColor(1,1,1,1) );
				m_textDebug.BeginTweening( 1 );	// sleep
				m_textDebug.BeginTweening( .5f );	// fade out
				m_textDebug.SetDiffuse( RageColor(1,1,1,0) );
			}
		}
		else if( DeviceI.button == KEY_F9 )
		{
			CString sShow = "diffusealpha,1;sleep,4;linear,1;diffusealpha,0";

			FOREACH_EnabledPlayer( pn )
			{
				m_textStepArtist[pn].Command( sShow );
				m_textStepDescription[pn].Command( sShow );
			}

			m_textSongTitle.Command( sShow );
			m_textArtist.Command( sShow );
			m_textGroup.Command( sShow );
		}
		// Change Speed with Keyboard - Begin
		// n				= x(n)		Speed: 1 = x1.0,	2 = x2.0	3 = x3.0	etc.
		// Ctrl + n			= x(n-0.5)	Speed: 1 = x0.5,	2 = x1.5	3 = x2.5	etc.
		// Alt + n			= x(n-0.25)	Speed: 1 = x0.75	2 = x1.75	3 = x2.75	etc.
		// Ctrl + Alt + n	= x(n-0.75)	Speed: 1 = x0.25	2 = x1.25	3 = x2.25	etc.
		// With Time Spacing we get C100, C125, C200, etc. instead of x1.0, x1.25, x2.0, etc.
		else if( DeviceI.button >= KEY_C1 && DeviceI.button <= KEY_C8 )
		{
			if( type != IET_RELEASE )
			{
				if( PREFSMAN->m_bChangeSpeedWithNumKeys )
				{
					float Speed = 0;

					if( iKeyFlag & HOLDING_CTRL && iKeyFlag & HOLDING_ALT )
						Speed = (float)(DeviceI.button - 48.75);
					else if( iKeyFlag & HOLDING_CTRL )
						Speed = (float)(DeviceI.button - 48.5);
					else if( iKeyFlag & HOLDING_ALT )
						Speed = (float)(DeviceI.button - 48.25);
					else
						Speed = (float)(DeviceI.button - 48);

					CString sSpeed = "Current Speed:\n";
					FOREACH_HumanPlayer( pn )
					{
						if( !(iKeyFlag & HOLDING_SHIFT) || ( pn == PLAYER_1 && iKeyFlag & HOLDING_LSHIFT ) || ( pn == PLAYER_2 && iKeyFlag & HOLDING_RSHIFT ) )
						{
							float fScrollSpeed = 0;

							CString spSpeed = ssprintf("\nP%d = ", pn+1);

							if( GAMESTATE->m_CurrentPlayerOptions[pn].m_fTimeSpacing != 1.0f )
							{
								fScrollSpeed = Speed * (1 - GAMESTATE->m_CurrentPlayerOptions[pn].m_fTimeSpacing);

								if( PREFSMAN->m_bStoreSpeedWithNumKeys )
									GAMESTATE->m_StoredPlayerOptions[pn].m_fScrollSpeed = fScrollSpeed;

								if( GAMESTATE->m_CurrentPlayerOptions[pn].m_fTimeSpacing == 0.0f )
									spSpeed += ssprintf("%.2fx +", fScrollSpeed);
								else
									spSpeed += ssprintf("%.2fx (%.0f%%) +", fScrollSpeed, (1 - GAMESTATE->m_CurrentPlayerOptions[pn].m_fTimeSpacing)*100 );
							}

							if( GAMESTATE->m_CurrentPlayerOptions[pn].m_fTimeSpacing != 0.0f )
							{
								float fScrollBPM = Speed * 100 * GAMESTATE->m_CurrentPlayerOptions[pn].m_fTimeSpacing;

								if( PREFSMAN->m_bStoreSpeedWithNumKeys )
									GAMESTATE->m_StoredPlayerOptions[pn].m_fScrollBPM = fScrollBPM;

								if( GAMESTATE->m_CurrentPlayerOptions[pn].m_fTimeSpacing == 1.0f )
									spSpeed += ssprintf("C%.0f +", fScrollBPM);
								else
									spSpeed += ssprintf("C%.0f (%.0f%%) +", fScrollBPM, GAMESTATE->m_CurrentPlayerOptions[pn].m_fTimeSpacing*100 );

								float fCurrentScrollBPM = GAMESTATE->m_CurrentPlayerOptions[pn].m_fScrollBPM;
								fScrollSpeed = fScrollBPM / fCurrentScrollBPM;
							}

							float fSpeedScrollSpeed = abs( GAMESTATE->m_CurrentPlayerOptions[pn].m_fScrollSpeed - fScrollSpeed );

							if( fSpeedScrollSpeed > 0 )
							{
								GAMESTATE->m_PlayerOptions[pn].m_SpeedfScrollSpeed = fSpeedScrollSpeed;
								GAMESTATE->m_PlayerOptions[pn].m_fScrollSpeed = fScrollSpeed;
							}

							spSpeed.resize( spSpeed.length()-2 );
							sSpeed += spSpeed;
						}
					}
					m_textDebug.SetText( sSpeed );
					m_textDebug.StopTweening();
					m_textDebug.SetDiffuse( RageColor(1,1,1,1) );
					m_textDebug.BeginTweening( 1 );	// sleep
					m_textDebug.BeginTweening( .5f );	// fade out
					m_textDebug.SetDiffuse( RageColor(1,1,1,0) );
				}
			}
		}
		// Change Speed with Keyboard - End
	}

	/* StyleI won't be valid if it's a menu button that's pressed.
	 * There's got to be a better way of doing this.  -Chris */
	// Aldo_MX: Hehe... I feel guilty about achieving this with a bug ^^u
	if( MenuI.IsValid() && ( PREFSMAN->m_bJukeboxNonMenuButtons || !StyleI.IsValid() && !PREFSMAN->m_bJukeboxNonMenuButtons ) )
	{
		if( type != IET_FIRST_PRESS )
			return; /* ignore */

		switch( MenuI.button )
		{
		case MENU_BUTTON_BACK:
		case MENU_BUTTON_START:
			m_bPrevScreen = true;
		case MENU_BUTTON_LEFT:
		case MENU_BUTTON_RIGHT:
			SCREENMAN->PostMessageToTopScreen( SM_NotesEnded, 0 );
			return;
		}
	}
	else if( PREFSMAN->m_bJukeboxGameplay )
	{
		if( type == IET_FIRST_PRESS && StyleI.IsValid() )
		{
			if( !GAMESTATE->m_PlayerOptions[StyleI.player].m_bAutoPlay )
				m_Players.m_Player[StyleI.player].Step( StyleI.col, DeviceI.ts );
		}

		if( type == IET_RELEASE && StyleI.IsValid() )
		{
			if( !GAMESTATE->m_PlayerOptions[StyleI.player].m_bAutoPlay )
				m_Players.m_Player[StyleI.player].Release( StyleI.col, DeviceI.ts );
		}
	}
}

void ScreenJukebox::HandleScreenMessage( const ScreenMessage SM )
{
	//LOG->Trace( "ScreenJukebox::HandleScreenMessage()" );

	switch( SM )
	{
	case SM_NotesEnded:
		if( m_Out.IsTransitioning() || m_Out.IsFinished() )
			return;	// ignore - we're already fading or faded
		m_Out.StartTransitioning( m_bPrevScreen ? SM_GoToPrevScreen : SM_GoToNextScreen );
		return;
	case SM_GoToPrevScreen:
	case SM_GoToNextScreen:
		m_soundMusic.Stop();
		SCREENMAN->SetNewScreen( m_bPrevScreen ? "ScreenJukeboxMenu" : "ScreenJukebox" );
		return;
	}

	ScreenGameplay::HandleScreenMessage( SM );
}

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * Contains code (c) 2003-2004 Chris Danford.
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
