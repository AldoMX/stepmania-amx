#include "global.h"

#include "ScreenAttract.h"
#include "ScreenManager.h"
#include "GameConstantsAndTypes.h"
#include "RageUtil.h"
#include "StepMania.h"
#include "PrefsManager.h"
#include "RageLog.h"
#include "SongManager.h"
#include "AnnouncerManager.h"
#include "GameState.h"
#include "GameManager.h"
#include "InputMapper.h"
#include "ThemeManager.h"
#include "GameSoundManager.h"
#include "CommonMetrics.h"

#define NEXT_SCREEN		THEME->GetMetric(m_sName,"NextScreen")

// I hate retrocompatibility sometimes...
#define PREV_SCREEN		( THEME->HasMetric(m_sName,"PrevScreen") ?				THEME->GetMetric(m_sName,"PrevScreen") :			NEXT_SCREEN )
#define SCREEN_BACK		( THEME->HasMetric(m_sName,"ScreenMenuBackPressed") ?	THEME->GetMetric(m_sName,"ScreenMenuBackPressed") :	INITIAL_SCREEN )
#define SCREEN_START	THEME->GetMetric(m_sName,"ScreenMenuStartPressed")

#if defined( WITH_COIN_MODE )
#define SCREEN_FREE		( THEME->HasMetric(m_sName,"ScreenMenuStartPressedFree") ?	THEME->GetMetric(m_sName,"ScreenMenuStartPressedFree") : SCREEN_START )
#define SCREEN_PAY		( THEME->HasMetric(m_sName,"ScreenMenuStartPressedPay") ?	THEME->GetMetric(m_sName,"ScreenMenuStartPressedPay") : SCREEN_START )
#endif

#define USE_CREDIT_SOUND	THEME->GetMetricB(m_sName,"UseCreditSound")
#define USE_MENU_BACK		THEME->GetMetricB(m_sName,"UseMenuBack")
#define JOIN_ON_START		THEME->GetMetricB(m_sName,"JoinPlayersOnStart")

ScreenAttract::ScreenAttract( CString sName, bool bResetGameState ) : Screen( sName )
{
	LOG->Trace( "ScreenAttract::ScreenAttract(%s)", m_sName.c_str() );

	// increment times through attract count
	if( m_sName == INITIAL_SCREEN )
		GAMESTATE->m_iNumTimesThroughAttract++;

	if( bResetGameState )
		GAMESTATE->Reset();

	m_bPrevScreen = false;

	// We have to do initialization in the first update because this->GetElementName() won't
	// work until the object has been fully constructed.
	m_Background.LoadFromAniDir( THEME->GetPathB(m_sName, "background") );
	this->AddChild( &m_Background );

	m_In.Load( THEME->GetPathB(m_sName, "in") );
	m_In.StartTransitioning();
	m_In.SetDrawOrder( DRAW_ORDER_TRANSITIONS );
	this->AddChild( &m_In );

	m_Out.Load( THEME->GetPathB(m_sName, "out") );
	m_Out.SetDrawOrder( DRAW_ORDER_TRANSITIONS );
	this->AddChild( &m_Out );

	if( USE_MENU_BACK )
	{
		m_Back.Load( THEME->GetPathB(m_sName, "back") );
		this->AddChild( &m_Back );
	}

	SOUND->PlayOnceFromDir( ANNOUNCER->GetPathTo(m_sName) );

	float fTimeUntilBeginFadingOut = m_Background.GetLengthSeconds() - m_Out.GetLengthSeconds();
	if( fTimeUntilBeginFadingOut < 0 )
	{
		LOG->Warn( "Screen '%s' Out BGAnimation (%f seconds) is longer than Background BGAnimation (%f seconds); background BGA will be truncated",
			m_sName.c_str(), m_Out.GetLengthSeconds(), m_Background.GetLengthSeconds() );
		fTimeUntilBeginFadingOut = 0;
	}

	this->PostScreenMessage( SM_BeginFadingOut, fTimeUntilBeginFadingOut );
}


ScreenAttract::~ScreenAttract()
{
	LOG->Trace( "ScreenAttract::~ScreenAttract()" );
}


void ScreenAttract::Input( const DeviceInput& DeviceI, const InputEventType type, const GameInput &GameI, const MenuInput &MenuI, const StyleInput &StyleI )
{
//	LOG->Trace( "ScreenAttract::Input()" );

	if(type != IET_FIRST_PRESS)
		return; // don't care

#if defined( WITH_COIN_MODE )
	ChangeCoinModeInput( DeviceI, type, GameI, MenuI, StyleI );
#endif

	if( MenuI.IsValid() )
	{
		switch( MenuI.button )
		{
		case MENU_BUTTON_BACK:
			if( USE_MENU_BACK )
			{
				if( m_In.IsTransitioning() || m_Out.IsTransitioning() || m_Back.IsTransitioning() )
					return;

				this->ClearMessageQueue();
				m_Back.StartTransitioning( SM_Back );
				SCREENMAN->PlayBackSound();
			}
			break;

		case MENU_BUTTON_START:
		case MENU_BUTTON_COIN:
#if defined( WITH_COIN_MODE )
			switch( PREFSMAN->GetCoinMode() )
			{
			case PrefsManager::COIN_PAY:
				LOG->Trace("ScreenAttract::AttractInput: PrefsManager::COIN_PAY (%i/%i)", GAMESTATE->m_iCoins, PREFSMAN->m_iCoinsPerCredit );
				if( GAMESTATE->m_iCoins < PREFSMAN->m_iCoinsPerCredit )
					break;	// don't fall through
				// fall through
			case PrefsManager::COIN_HOME:
			case PrefsManager::COIN_FREE:
#endif
				SOUND->StopMusic();
				SCREENMAN->SendMessageToTopScreen( SM_StopMusic );

				if( USE_CREDIT_SOUND )
				{
					/* We already played the credit sound.  Don't play it again. */
					if( MenuI.button != MENU_BUTTON_COIN )
						SCREENMAN->PlayCreditSound();

					usleep( (int)(JOIN_PAUSE_SECONDS*1000000) );	// do a little pause, like the arcade does
				}

				if( JOIN_ON_START )
					GAMESTATE->JoinPlayer( MenuI.player );

#if defined( WITH_COIN_MODE )
				{
					switch( PREFSMAN->GetCoinMode() )
					{
					case PrefsManager::COIN_PAY:	SCREENMAN->SetNewScreen( SCREEN_PAY );		break;
					case PrefsManager::COIN_HOME:	SCREENMAN->SetNewScreen( SCREEN_START );	break;
					case PrefsManager::COIN_FREE:	SCREENMAN->SetNewScreen( SCREEN_FREE );		break;
					default:		ASSERT(0);
					}
				}
				break;
			default:
				ASSERT(0);
			}
#else
			//LOG->Trace("ScreenAttract::AttractInput: go to ScreenTitleMenu" );
			SCREENMAN->SetNewScreen( SCREEN_START );
#endif
			break;

		case MENU_BUTTON_LEFT:
		case MENU_BUTTON_RIGHT:
			m_bPrevScreen = MenuI.button == MENU_BUTTON_LEFT;
			SCREENMAN->PostMessageToTopScreen( SM_BeginFadingOut, 0 );
			break;
		}
	}
}

void ScreenAttract::Update( float fDelta )
{
	if( IsFirstUpdate() )
	{
		if( GAMESTATE->IsTimeToPlayAttractSounds() )
			SOUND->PlayMusic( THEME->GetPathS(m_sName, "music", false) );
		else
			SOUND->PlayMusic( "" );	// stop music
	}
	Screen::Update(fDelta);
}

void ScreenAttract::HandleScreenMessage( const ScreenMessage SM )
{
	switch( SM )
	{
	case SM_BeginFadingOut:
		if( !m_Out.IsTransitioning() )
			m_Out.StartTransitioning( m_bPrevScreen ? SM_GoToPrevScreen : SM_GoToNextScreen );
		break;

	case SM_Back:
		SOUND->PlayMusic( "" );
		SCREENMAN->SetNewScreen( SCREEN_BACK );
		break;

	case SM_GoToPrevScreen:
	case SM_GoToNextScreen:
	{
		CString sScreen;

		switch( SM )
		{
		case SM_GoToPrevScreen:	sScreen = PREV_SCREEN;	break;
		case SM_GoToNextScreen:	sScreen = NEXT_SCREEN;	break;
		}

		/* Look at the def of the screen we're going to; if it has a music theme element
		* and it's the same as the one we're playing now, don't stop.  However, if we're
		* going to interrupt it when we fade in, stop the old music before we fade out. */
		bool bGoingToPlayTheSameMusic =
			THEME->GetPathS( sScreen, "music", false ) == THEME->GetPathS( m_sName, "music", false );
		if( bGoingToPlayTheSameMusic )
			; // do nothing
		else
			SOUND->PlayMusic( "" );	// stop the music

		SCREENMAN->SetNewScreen( sScreen );
		break;
	}

	default:
		Screen::HandleScreenMessage(SM);
	}
}

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * Contains code (c) 2003-2004 Chris Danford.
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
