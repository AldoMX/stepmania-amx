#include "global.h"
#include "NoteTypes.h"
#include "RageUtil.h"
#include "EnumHelper.h"

//									// TYPE				// SUBTYPE				// PN	// KS	// BEHAVIOR				// DISPLAY			// NS	// SOURCE		// CP
TapNote TAP_EMPTY				= { TapNote::empty,		TapNote::subtype_none,	PLAYER_1,	0,	TapNote::behavior_none,	TapNote::display_all,	0,	TapNote::original,	0 };	// '0'
TapNote TAP_ORIGINAL_TAP		= { TapNote::tap,		TapNote::subtype_none,	PLAYER_1,	0,	TapNote::behavior_none,	TapNote::display_all,	0,	TapNote::original,	0 };	// '1'
TapNote TAP_ORIGINAL_LIFT		= { TapNote::lift,		TapNote::subtype_none,	PLAYER_1,	0,	TapNote::behavior_none,	TapNote::display_all,	0,	TapNote::original,	0 };	// 'L'
TapNote TAP_ORIGINAL_HIDDEN		= { TapNote::hidden,	TapNote::subtype_none,	PLAYER_1,	0,	TapNote::behavior_none,	TapNote::display_all,	0,	TapNote::original,	0 };	// 'H'
TapNote TAP_ORIGINAL_HOLD_HEAD	= { TapNote::hold_head,	HOLD_TYPE_DANCE,		PLAYER_1,	0,	TapNote::behavior_none,	TapNote::display_all,	0,	TapNote::original,	0 };	// '2'
TapNote TAP_ORIGINAL_HOLD_TAIL	= { TapNote::hold_tail,	HOLD_TYPE_DANCE,		PLAYER_1,	0,	TapNote::behavior_none,	TapNote::display_all,	0,	TapNote::original,	0 };	// '3'
TapNote TAP_ORIGINAL_ROLL_HEAD	= { TapNote::hold_head,	HOLD_TYPE_ROLL,			PLAYER_1,	0,	TapNote::behavior_none,	TapNote::display_all,	0,	TapNote::original,	0 };	// '4'
TapNote TAP_ORIGINAL_HOLD		= { TapNote::hold,		HOLD_TYPE_DANCE,		PLAYER_1,	0,	TapNote::behavior_none,	TapNote::display_all,	0,	TapNote::original,	0 };	// '9'
TapNote TAP_ORIGINAL_ROLL		= { TapNote::hold,		HOLD_TYPE_ROLL,			PLAYER_1,	0,	TapNote::behavior_none,	TapNote::display_all,	0,	TapNote::original,	0 };	// '8'
TapNote TAP_ORIGINAL_MINE		= { TapNote::mine,		TapNote::subtype_none,	PLAYER_1,	0,	TapNote::behavior_none,	TapNote::display_all,	0,	TapNote::original,	0 };	// 'M'
TapNote TAP_ORIGINAL_SHOCK		= { TapNote::shock,		TapNote::subtype_none,	PLAYER_1,	0,	TapNote::behavior_none,	TapNote::display_all,	0,	TapNote::original,	0 };	// 'S'
TapNote TAP_ORIGINAL_POTION		= { TapNote::potion,	TapNote::subtype_none,	PLAYER_1,	0,	TapNote::behavior_none,	TapNote::display_all,	0,	TapNote::original,	0 };	// 'P'

//									// TYPE				// SUBTYPE				// PN	// KS	// BEHAVIOR				// DISPLAY			// NS	// SOURCE		// CP
TapNote TAP_ADDITION_TAP		= { TapNote::tap,		TapNote::subtype_none,	PLAYER_1,	0,	TapNote::behavior_none,	TapNote::display_all,	0,	TapNote::addition,	0 };
TapNote TAP_ADDITION_LIFT		= { TapNote::lift,		TapNote::subtype_none,	PLAYER_1,	0,	TapNote::behavior_none,	TapNote::display_all,	0,	TapNote::addition,	0 };
TapNote TAP_ADDITION_MINE		= { TapNote::mine,		TapNote::subtype_none,	PLAYER_1,	0,	TapNote::behavior_none,	TapNote::display_all,	0,	TapNote::addition,	0 };
TapNote TAP_ADDITION_SHOCK		= { TapNote::shock,		TapNote::subtype_none,	PLAYER_1,	0,	TapNote::behavior_none,	TapNote::display_all,	0,	TapNote::addition,	0 };
TapNote TAP_ADDITION_POTION		= { TapNote::potion,	TapNote::subtype_none,	PLAYER_1,	0,	TapNote::behavior_none,	TapNote::display_all,	0,	TapNote::addition,	0 };

static const CString HoldTypeNames[NUM_HOLD_TYPES] = {
	"Hold",
	//"Long",	// TODO: HOLD_TYPE_PIU
	"Roll"
};
XToString( HoldType );
StringToX( HoldType );

int NoteTypeToRow( NoteType nt )
{
	switch( nt )
	{
	case NOTE_TYPE_4TH:		return ROWS_PER_BEAT;
	case NOTE_TYPE_8TH:		return ROWS_PER_BEAT/2;
	case NOTE_TYPE_12TH:	return ROWS_PER_BEAT/3;
	case NOTE_TYPE_16TH:	return ROWS_PER_BEAT/4;
	case NOTE_TYPE_24TH:	return ROWS_PER_BEAT/6;
	case NOTE_TYPE_32ND:	return ROWS_PER_BEAT/8;
	case NOTE_TYPE_48TH:	return ROWS_PER_BEAT/12;
	case NOTE_TYPE_64TH:	return ROWS_PER_BEAT/16;
	case NOTE_TYPE_96TH:	return ROWS_PER_BEAT/24;
	case NOTE_TYPE_128TH:	return ROWS_PER_BEAT/32;
	case NOTE_TYPE_192ND:	return ROWS_PER_BEAT/48;
	case NOTE_TYPE_256TH:	return ROWS_PER_BEAT/64;
	case NOTE_TYPE_384TH:	return ROWS_PER_BEAT/96;
	case NOTE_TYPE_768TH:	return ROWS_PER_BEAT/192;
	default:	ASSERT(0); // and fall through
	case NOTE_TYPE_INVALID:	return ROWS_PER_BEAT/192;
	}
}

float NoteTypeToBeat( NoteType nt )
{
	switch( nt )
	{
	case NOTE_TYPE_4TH:		return 1.f;
	case NOTE_TYPE_8TH:		return 1.f/2;
	case NOTE_TYPE_12TH:	return 1.f/3;
	case NOTE_TYPE_16TH:	return 1.f/4;
	case NOTE_TYPE_24TH:	return 1.f/6;
	case NOTE_TYPE_32ND:	return 1.f/8;
	case NOTE_TYPE_48TH:	return 1.f/12;
	case NOTE_TYPE_64TH:	return 1.f/16;
	case NOTE_TYPE_96TH:	return 1.f/24;
	case NOTE_TYPE_128TH:	return 1.f/32;
	case NOTE_TYPE_192ND:	return 1.f/48;
	case NOTE_TYPE_256TH:	return 1.f/64;
	case NOTE_TYPE_384TH:	return 1.f/96;
	case NOTE_TYPE_768TH:	return 1.f/192;
	default:	ASSERT(0); // and fall through
	case NOTE_TYPE_INVALID:	return 1.0f/192;
	}
}

NoteType GetNoteType( int iNoteIndex )
{
	if(      iNoteIndex % (ROWS_PER_MEASURE/4) == 0)	return NOTE_TYPE_4TH;
	else if( iNoteIndex % (ROWS_PER_MEASURE/8) == 0)	return NOTE_TYPE_8TH;
	else if( iNoteIndex % (ROWS_PER_MEASURE/12) == 0)	return NOTE_TYPE_12TH;
	else if( iNoteIndex % (ROWS_PER_MEASURE/16) == 0)	return NOTE_TYPE_16TH;
	else if( iNoteIndex % (ROWS_PER_MEASURE/24) == 0)	return NOTE_TYPE_24TH;
	else if( iNoteIndex % (ROWS_PER_MEASURE/32) == 0)	return NOTE_TYPE_32ND;
	else if( iNoteIndex % (ROWS_PER_MEASURE/48) == 0)	return NOTE_TYPE_48TH;
	else if( iNoteIndex % (ROWS_PER_MEASURE/64) == 0)	return NOTE_TYPE_64TH;
	else if( iNoteIndex % (ROWS_PER_MEASURE/96) == 0)	return NOTE_TYPE_96TH;
	else if( iNoteIndex % (ROWS_PER_MEASURE/128) == 0)	return NOTE_TYPE_128TH;
	else if( iNoteIndex % (ROWS_PER_MEASURE/192) == 0)	return NOTE_TYPE_192ND;
	else if( iNoteIndex % (ROWS_PER_MEASURE/256) == 0)	return NOTE_TYPE_256TH;
	else if( iNoteIndex % (ROWS_PER_MEASURE/384) == 0)	return NOTE_TYPE_384TH;
	else if( iNoteIndex % (ROWS_PER_MEASURE/768) == 0)	return NOTE_TYPE_768TH;
	else												return NOTE_TYPE_INVALID;
};

CString NoteTypeToString( NoteType nt )
{
	switch( nt )
	{
	case NOTE_TYPE_4TH:		return "4th";
	case NOTE_TYPE_8TH:		return "8th";
	case NOTE_TYPE_12TH:	return "12th";
	case NOTE_TYPE_16TH:	return "16th";
	case NOTE_TYPE_24TH:	return "24th";
	case NOTE_TYPE_32ND:	return "32nd";
	case NOTE_TYPE_48TH:	return "48th";
	case NOTE_TYPE_64TH:	return "64th";
	case NOTE_TYPE_96TH:	return "96th";
	case NOTE_TYPE_128TH:	return "128th";
	case NOTE_TYPE_192ND:	return "192nd";
	case NOTE_TYPE_256TH:	return "256th";
	case NOTE_TYPE_384TH:	return "384th";
	case NOTE_TYPE_768TH:	// fall through
	case NOTE_TYPE_INVALID:	return "768th";
	default:	ASSERT(0);	return "";
	}
}

NoteType BeatToNoteType( float fBeat )
{
	return GetNoteType( BeatToNoteRowRounded(fBeat) );
}

bool IsNoteOfType( int iNoteIndex, NoteType t )
{
	return GetNoteType(iNoteIndex) == t;
}

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * Contains code (c) 2001-2004 Chris Danford, Glenn Maynard.
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
