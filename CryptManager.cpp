#include "global.h"
#include "CryptManager.h"
#include "RageLog.h"
#include "RageFileManager.h"
#include "RageUtil.h"
#include "CryptHelpers.h"
#include "PrefsManager.h"

#if defined(_MSC_VER)
	#if defined(_DEBUG)
		#pragma comment(lib, "libtomcrypt_debug.lib")
		#pragma comment(lib, "libtommath_debug.lib")
	#else
		#pragma comment(lib, "libtomcrypt.lib")
		#pragma comment(lib, "libtommath.lib")
	#endif
#endif

CryptManager*	CRYPTMAN	= NULL;	// global and accessable from anywhere in our program

static const CString PRIVATE_KEY_PATH = "Data/private.rsa";
static const CString PUBLIC_KEY_PATH = "Data/public.rsa";
static const CString ALTERNATE_PUBLIC_KEY_DIR = "Data/keys/";

static const int KEY_LENGTH = 1024;
static const int MAX_SIGNATURE_SIZE_BYTES = 1024;	// 1 KB

static PRNGWrapper *g_pPRNG = NULL;

CryptManager::CryptManager()
{
	//
	// generate keys if none are available
	//
	if( PREFSMAN->m_bSignProfileData )
	{
		if( !DoesFileExist(PRIVATE_KEY_PATH) || !DoesFileExist(PUBLIC_KEY_PATH) )
		{
			LOG->Warn( "Public and/or Private keys missing. Profile data cannot be signed." );
			FlushDirCache();
		}
	}

	ltc_mp = ltm_desc;
	g_pPRNG = new PRNGWrapper( &yarrow_desc );
}

CryptManager::~CryptManager()
{
	SAFE_DELETE( g_pPRNG );
}

void CryptManager::GenerateGlobalKeys()
{
	//
	// generate keys if none are available
	//
	bool bGenerate = false;
	CString sKey;
	CString sError;
	RSAKeyWrapper key;

	if( !DoesFileExist(PRIVATE_KEY_PATH) ||
	    !GetFileContents(PRIVATE_KEY_PATH, sKey) ||
	    !key.Load(sKey, sError) )
		bGenerate = true;

	if( !sError.empty() )
	{
		LOG->Warn( "Error loading RSA key: %s", sError.c_str() );
		sError.clear();
	}

	if( !DoesFileExist(PUBLIC_KEY_PATH) ||
	    !GetFileContents(PUBLIC_KEY_PATH, sKey) ||
	    !key.Load(sKey, sError) )
		bGenerate = true;

	if( !sError.empty() )
		LOG->Warn( "Error loading RSA key: %s", sError.c_str() );

	if( bGenerate )
	{
		LOG->Warn( "Keys missing or failed to load.  Generating new keys" );
		GenerateRSAKeyToFile( KEY_LENGTH, PRIVATE_KEY_PATH, PUBLIC_KEY_PATH );
	}
}

static bool HashFile( RageFile &f, unsigned char* buf_hash, int iHash )
{
	hash_state hash;
	int iRet = hash_descriptor[iHash].init( &hash );
	ASSERT_M( iRet == CRYPT_OK, error_to_string(iRet) );

	CString s;
	while( !f.AtEOF() )
	{
		s.erase();
		if( f.Read(s, 1024*4) == -1 )
		{
			LOG->Warn( "Error reading %s: %s", f.GetPath().c_str(), f.GetError().c_str() );
			hash_descriptor[iHash].done( &hash, buf_hash );
			return false;
		}

		iRet = hash_descriptor[iHash].process( &hash, (const unsigned char *) s.data(), s.size() );
		ASSERT_M( iRet == CRYPT_OK, error_to_string(iRet) );
	}

	iRet = hash_descriptor[iHash].done( &hash, buf_hash );
	ASSERT_M( iRet == CRYPT_OK, error_to_string(iRet) );

	return true;
}

static bool WriteFile( CString sFile, CString sBuf )
{
	RageFile output;
	if( !output.Open(sFile, RageFile::WRITE) )
	{
		LOG->Warn( "WriteFile: opening %s failed: %s", sFile.c_str(), output.GetError().c_str() );
		return false;
	}

	if( output.Write(sBuf) == -1 || output.Flush() == -1 )
	{
		LOG->Warn( "WriteFile: writing %s failed: %s", sFile.c_str(), output.GetError().c_str() );
		output.Close();
		FILEMAN->Remove( sFile );
		return false;
	}

	return true;
}

void CryptManager::GenerateRSAKey( unsigned int keyLength, CString& sPrivKey, CString& sPubKey )
{
	int iRet;

	rsa_key key;
	iRet = rsa_make_key( &g_pPRNG->m_PRNG, g_pPRNG->m_iPRNG, keyLength / 8, 65537, &key );
	if( iRet != CRYPT_OK )
	{
		LOG->Warn( "GenerateRSAKey(%i) error: %s", keyLength, error_to_string(iRet) );
		return;
	}

	unsigned char buf[1024];
	unsigned long iSize = sizeof(buf);
	iRet = rsa_export( buf, &iSize, PK_PUBLIC, &key );
	if( iRet != CRYPT_OK )
	{
		LOG->Warn( "Export error: %s", error_to_string(iRet) );
		return;
	}

	sPubKey = CString( (const char *) buf, iSize );

	iSize = sizeof(buf);
	iRet = rsa_export( buf, &iSize, PK_PRIVATE, &key );
	if( iRet != CRYPT_OK )
	{
		LOG->Warn( "Export error: %s", error_to_string(iRet) );
		return;
	}

	sPrivKey = CString( (const char *) buf, iSize );
}

void CryptManager::GenerateRSAKeyToFile( unsigned int keyLength, CString privFilename, CString pubFilename )
{
	CString sPrivKey, sPubKey;
	GenerateRSAKey( keyLength, sPrivKey, sPubKey );

	if( !WriteFile(pubFilename, sPubKey) )
		return;

	if( !WriteFile(privFilename, sPrivKey) )
	{
		FILEMAN->Remove( privFilename );
		return;
	}
}

void CryptManager::SignFileToFile( CString sPath, CString sSignatureFile )
{
	CString sPrivFilename = PRIVATE_KEY_PATH;
	if( sSignatureFile.empty() )
		sSignatureFile = sPath + SIGNATURE_APPEND;

	CString sPrivKey;
	if( !GetFileContents(sPrivFilename, sPrivKey) )
		return;

	CString sSignature;
	if( !Sign(sPath, sSignature, sPrivKey) )
		return;

	WriteFile( sSignatureFile, sSignature );
}

bool CryptManager::Sign( CString sPath, CString& sSignatureOut, CString sPrivateKey )
{
	if( !IsAFile(sPath) )
	{
		LOG->Trace( "SignFileToFile: \"%s\" doesn't exist", sPath.c_str() );
		return false;
	}

	RageFile file;
	if( !file.Open(sPath) )
	{
		LOG->Warn( "SignFileToFile: open(%s) failed: %s", sPath.c_str(), file.GetError().c_str() );
		return false;
	}

	RSAKeyWrapper key;
	CString sError;
	if( !key.Load(sPrivateKey, sError) )
	{
		LOG->Warn( "Error loading RSA key: %s", sError.c_str() );
		return false;
	}

	int iHash = register_hash( &sha1_desc );
	ASSERT( iHash >= 0 );

	unsigned char buf_hash[20];
	if( !HashFile(file, buf_hash, iHash) )
		return false;

	unsigned char signature[256];
	unsigned long signature_len = sizeof(signature);

	int iRet = rsa_sign_hash_ex(
			buf_hash, sizeof(buf_hash),
			signature, &signature_len,
			LTC_LTC_PKCS_1_V1_5, &g_pPRNG->m_PRNG, g_pPRNG->m_iPRNG, iHash,
			0, &key.m_Key);
	if( iRet != CRYPT_OK )
	{
		LOG->Warn( "SignFileToFile error: %s", error_to_string(iRet) );
		return false;
	}

	sSignatureOut.assign( (const char *) signature, signature_len );
	return true;
}

bool CryptManager::VerifyFileWithFile( CString sPath, CString sSignatureFile )
{
	if( VerifyFileWithFile(sPath, sSignatureFile, PUBLIC_KEY_PATH) )
		return true;

	CStringArray asKeys;
	GetDirListing( ALTERNATE_PUBLIC_KEY_DIR, asKeys, false, true );
	for( unsigned i = 0; i < asKeys.size(); ++i )
	{
		const CString &sKey = asKeys[i];
		LOG->Trace( "Trying alternate key \"%s\" ...", sKey.c_str() );

		if( VerifyFileWithFile(sPath, sSignatureFile, sKey) )
			return true;
	}

	return false;
}

bool CryptManager::VerifyFileWithFile( CString sPath, CString sSignatureFile, CString sPublicKeyFile )
{
	if( sSignatureFile.empty() )
		sSignatureFile = sPath + SIGNATURE_APPEND;

	CString sPublicKey;
	if( !GetFileContents(sPublicKeyFile, sPublicKey) )
		return false;

	int iBytes = FILEMAN->GetFileSizeInBytes( sSignatureFile );
	if( iBytes > MAX_SIGNATURE_SIZE_BYTES )
		return false;

	CString sSignature;
	if( !GetFileContents(sSignatureFile, sSignature) )
		return false;

	RageFile file;
	if( !file.Open(sPath) )
	{
		LOG->Warn( "Verify: open(%s) failed: %s", sPath.c_str(), file.GetError().c_str() );
		return false;
	}

	return Verify( file, sSignature, sPublicKey );
}

bool CryptManager::Verify( RageFile& file, CString sSignature, CString sPublicKey )
{
	RSAKeyWrapper key;
	CString sError;
	if( !key.Load(sPublicKey, sError) )
	{
		LOG->Warn( "Error loading RSA key: %s", sError.c_str() );
		return false;
	}

	int iHash = register_hash( &sha1_desc );
	ASSERT( iHash >= 0 );

	unsigned char buf_hash[20];
	HashFile( file, buf_hash, iHash );

	int iMatch;
	int iRet = rsa_verify_hash_ex( (const unsigned char *) sSignature.data(), sSignature.size(),
			buf_hash, sizeof(buf_hash),
			LTC_LTC_PKCS_1_EMSA, iHash, 0, &iMatch, &key.m_Key );

	if( iRet != CRYPT_OK )
	{
		LOG->Warn( "Verify(%s) failed: %s", file.GetPath().c_str(), error_to_string(iRet) );
		return false;
	}

	if( !iMatch )
	{
		LOG->Warn( "Verify(%s) failed: signature mismatch", file.GetPath().c_str() );
		return false;
	}

	return true;
}

void CryptManager::GetRandomBytes( void *pData, int iBytes )
{
	int iRet = prng_descriptor[g_pPRNG->m_iPRNG].read( (unsigned char *) pData, iBytes, &g_pPRNG->m_PRNG );
	ASSERT( iRet == iBytes );
}

// Generate a version 4 random UUID.
CString CryptManager::GenerateRandomUUID()
{
	uint32_t buf[4];
	CryptManager::GetRandomBytes( buf, sizeof(buf) );

	buf[1] &= 0xFFFF0FFF;
	buf[1] |= 0x00004000;
	buf[2] &= 0x0FFFFFFF;
	buf[2] |= 0xA0000000;

	return ssprintf("%08x-%04x-%04x-%04x-%04x%08x",
			buf[0],
			buf[1] >> 16, buf[1] & 0xFFFF,
			buf[2] >> 16, buf[2] & 0xFFFF,
			buf[3]);
}

CString CryptManager::GetMD5( CString& in, bool binary )
{
	unsigned char digest[16];

	int iHash = register_hash( &md5_desc );

	hash_state hash;
	hash_descriptor[iHash].init( &hash );
	hash_descriptor[iHash].process( &hash, (const unsigned char *) in.data(), in.size() );
	hash_descriptor[iHash].done( &hash, digest );

	if( binary )
		return BinaryToHex( digest, sizeof(digest) );
	else
		return CString( (const char *)digest, sizeof(digest) );
}

CString CryptManager::GetMD5FromFile( CString& sPath, bool binary )
{
	RageFile file;
	if( !file.Open( sPath, RageFile::READ ) )
	{
		LOG->Warn( "GetMD5: Failed to open file '%s'", sPath.c_str() );
		return "";
	}
	int iHash = register_hash( &md5_desc );
	ASSERT( iHash >= 0 );

	unsigned char digest[16];
	HashFile( file, digest, iHash );

	if( binary )
		return BinaryToHex( digest, sizeof(digest) );
	else
		return CString( (const char *)digest, sizeof(digest) );
}

CString CryptManager::GetSHA1( CString& in, bool binary )
{
	unsigned char digest[20];

	int iHash = register_hash( &sha1_desc );

	hash_state hash;
	hash_descriptor[iHash].init( &hash );
	hash_descriptor[iHash].process( &hash, (const unsigned char *) in.data(), in.size() );
	hash_descriptor[iHash].done( &hash, digest );

	if( binary )
		return BinaryToHex( digest, sizeof(digest) );
	else
		return CString( (const char *)digest, sizeof(digest) );
}

CString CryptManager::GetSHA1FromFile( CString& sPath, bool binary )
{
	RageFile file;
	if( !file.Open( sPath, RageFile::READ ) )
	{
		LOG->Warn( "GetMD5: Failed to open file '%s'", sPath.c_str() );
		return "";
	}
	int iHash = register_hash( &sha1_desc );
	ASSERT( iHash >= 0 );

	unsigned char digest[20];
	HashFile( file, digest, iHash );

	if( binary )
		return BinaryToHex( digest, sizeof(digest) );
	else
		return CString( (const char *)digest, sizeof(digest) );
}

CString CryptManager::GetPublicKeyFileName()
{
	ASSERT( PREFSMAN->m_bSignProfileData );

	return PUBLIC_KEY_PATH;
}

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * Contains code (c) 2004 Chris Danford.
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
