#include "global.h"
#include "LifeMeterBattery.h"
#include "GameState.h"
#include "ThemeManager.h"
#include "Steps.h"
#include "StageStats.h"
#include "PrefsManager.h"


const float	BATTERY_X[NUM_PLAYERS]	=	{ -92, +92 };

const float NUM_X[NUM_PLAYERS]		=	{ BATTERY_X[0], BATTERY_X[1] };
const float NUM_Y					=	+2;

const float BATTERY_BLINK_TIME		= 1.2f;


LifeMeterBattery::LifeMeterBattery( const CString& name ) : LifeMeter("LifeMeterBattery" + name)
{
}

void LifeMeterBattery::Load( PlayerNumber pn )
{
	LifeMeter::Load(pn);

	bool bPlayerEnabled = GAMESTATE->IsPlayerEnabled(pn);

	m_sprFrame.Load( THEME->GetPathG(m_sName, "frame") );
	this->AddChild( &m_sprFrame );

	m_sprBattery.Load( THEME->GetPathG(m_sName, "lives") );
	m_sprBattery.StopAnimating();
	if( bPlayerEnabled )
		this->AddChild( &m_sprBattery );

	m_textNumLives.LoadFromFont( THEME->GetPathF(m_sName, "lives") );
	m_textNumLives.SetDiffuse( RageColor(1,1,1,1) );
	m_textNumLives.SetShadowLength( 0 );
	if( bPlayerEnabled )
		this->AddChild( &m_textNumLives );

	m_sprFrame.SetZoomX( pn==PLAYER_1 ? 1.0f : -1.0f );
	m_sprBattery.SetZoomX( pn==PLAYER_1 ? 1.0f : -1.0f );
	m_sprBattery.SetX( BATTERY_X[pn] );
	m_textNumLives.SetX( NUM_X[pn] );
	m_textNumLives.SetY( NUM_Y );

	if( bPlayerEnabled )
	{
		m_Percent.SetName( m_sName + " Percent" );
		m_Percent.Load( pn, &g_CurStageStats, true );
		this->AddChild( &m_Percent );

		m_soundGainLife.Load(THEME->GetPathS(m_sName, "gain"));
		m_soundLoseLife.Load(THEME->GetPathS(m_sName, "lose"), true);
	}

	m_iLivesLeft = GAMESTATE->m_PlayerOptions[pn].m_iBatteryLives;
	m_iTrailingLivesLeft = m_iLivesLeft;

	m_fBatteryBlinkTime = 0;

	Refresh();
}

void LifeMeterBattery::OnSongEnded()
{
	if( g_CurStageStats.bFailedEarlier[m_PlayerNumber] )
		return;

	if( m_iLivesLeft < GAMESTATE->m_PlayerOptions[m_PlayerNumber].m_iBatteryLives )
	{
		m_iTrailingLivesLeft = m_iLivesLeft;
		m_iLivesLeft += ( GAMESTATE->m_pCurSteps[m_PlayerNumber]->GetMeter()>=8 ? 2 : 1 );
		m_iLivesLeft = min( m_iLivesLeft, GAMESTATE->m_PlayerOptions[m_PlayerNumber].m_iBatteryLives );
		m_soundGainLife.Play();
	}

	Refresh();
}


void LifeMeterBattery::ChangeLife( TapNoteScore score, const float fMultiplier )
{
	if( g_CurStageStats.bFailedEarlier[m_PlayerNumber] )
		return;

	switch( score )
	{
	case TNS_MARVELOUS:
	case TNS_PERFECT:
	case TNS_GREAT:
	case TNS_MISS_HIDDEN:
	case TNS_CHECKPOINT:
	case TNS_HIT_POTION:
		return;

	case TNS_HIDDEN:	// Hidden increases a life
		m_iTrailingLivesLeft = m_iLivesLeft;
		m_iLivesLeft += int(fMultiplier+.5f);
		m_soundGainLife.Play();

		m_textNumLives.SetZoom( 1.5f );
		m_textNumLives.BeginTweening( 0.15f );
		m_textNumLives.SetZoom( 1.0f );

		Refresh();
		m_fBatteryBlinkTime = BATTERY_BLINK_TIME;
		break;

	case TNS_GOOD:
		switch( PREFSMAN->m_iScoringType )
		{
		case PrefsManager::SCORING_PIU_NX2:
		case PrefsManager::SCORING_PIU_ZERO:
		case PrefsManager::SCORING_PIU_PREX3:
			return;
		}

		if( !PREFSMAN->m_bComboBreakOnGood )
			return;

		// fall-through

	case TNS_BOO:
	case TNS_MISS:
	case TNS_MISS_CHECKPOINT:
	case TNS_HIT_SHOCK:
	case TNS_HIT_MINE:
		m_iTrailingLivesLeft = m_iLivesLeft;
		m_iLivesLeft -= int(fMultiplier+.5f);
		m_soundLoseLife.Play();

		m_textNumLives.SetZoom( 1.5f );
		m_textNumLives.BeginTweening( 0.15f );
		m_textNumLives.SetZoom( 1.0f );

		Refresh();
		m_fBatteryBlinkTime = BATTERY_BLINK_TIME;
		break;

	default:
		ASSERT(0);
	}

	if( m_iLivesLeft == 0 )
		g_CurStageStats.bFailedEarlier[m_PlayerNumber] = true;
}

void LifeMeterBattery::ChangeLife( HoldNoteScore score, const float fMultiplier )
{
	switch( score )
	{
	case HNS_OK:
	case RNS_OK:
	case SHOCK_OK:
		return;

	case SHOCK_NG:	// Life already changed by TNS_HIT_SHOCK
		return;

	case HNS_NG:
	case RNS_NG:
		ChangeLife( TNS_MISS, fMultiplier );	// NG is the same as a miss
		return;

	default:
		ASSERT(0);
	}
}

void LifeMeterBattery::ChangeLife( float fDeltaLifePercent, bool bHiddenScore )
{
}

void LifeMeterBattery::OnDancePointsChange()
{
}

bool LifeMeterBattery::IsInDanger() const
{
	return false;
}

bool LifeMeterBattery::IsHot() const
{
	return false;
}

bool LifeMeterBattery::IsFailing() const
{
	return g_CurStageStats.bFailedEarlier[m_PlayerNumber];
}

float LifeMeterBattery::GetLife() const
{
	if( !GAMESTATE->m_PlayerOptions[m_PlayerNumber].m_iBatteryLives )
		return 1;

	return float(m_iLivesLeft) / GAMESTATE->m_PlayerOptions[m_PlayerNumber].m_iBatteryLives;
}

void LifeMeterBattery::Refresh()
{
	if( m_iLivesLeft <= 4 )
	{
		m_textNumLives.SetText( "" );
		m_sprBattery.SetState( max(m_iLivesLeft-1,0) );
	}
	else
	{
		m_textNumLives.SetText( ssprintf("x%d", m_iLivesLeft-1) );
		m_sprBattery.SetState( 3 );
	}
}

void LifeMeterBattery::Update( float fDeltaTime )
{
	LifeMeter::Update( fDeltaTime );

	if( m_fBatteryBlinkTime > 0 )
	{
		m_fBatteryBlinkTime -= fDeltaTime;
		int iFrame1 = m_iLivesLeft-1;
		int iFrame2 = m_iTrailingLivesLeft-1;

		int iFrameNo = (int(m_fBatteryBlinkTime*15)%2) ? iFrame1 : iFrame2;
		CLAMP( iFrameNo, 0, 3 );
		m_sprBattery.SetState( iFrameNo );

	}
	else
	{
		m_fBatteryBlinkTime = 0;
		int iFrameNo = m_iLivesLeft-1;
		CLAMP( iFrameNo, 0, 3 );
		m_sprBattery.SetState( iFrameNo );
	}
}

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * Contains code (c) 2001-2004 Chris Danford.
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
