#ifndef PLAYER_CONTAINER_H
#define PLAYER_CONTAINER_H

#include "ActorFrame.h"
#include "Player.h"

struct NotePressed
{
	PlayerNumber	pn;
	float			time;

	NotePressed( PlayerNumber pn_, float time_ ) { pn = pn_; time = time_; }

	bool operator>( const NotePressed &other ) const { return time > other.time; }
};

class PlayerContainer : public ActorFrame
{
public:
	PlayerContainer();
	~PlayerContainer();

	void Init( vector<float>& fPNX );

	void DrawPrimitives();

	Player					m_Player[NUM_PLAYERS];
	vector<NotePressed>		m_LastNotePressed;

protected:
	void CheckValidRotation( bool& bAllUnderAttack );
};

#endif

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
