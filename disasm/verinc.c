// Ehhh....
//
// I think I'll just put this one in the public domain
// (with no warranty as usual).
//
// --Avery

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <time.h>

typedef unsigned long ulong;

#ifdef _WIN32
#define popen _popen
#define pclose _pclose
#endif

int main()
{
	FILE *f;
	char c[41], t[25];
	time_t tm;

	memset(c, 0, sizeof c);
	if( f = popen("git rev-parse HEAD","r") ) {
		if( 1 == fread(c, sizeof c, 1, f) )
			c[40] = 0;
		pclose(f);
	}

	printf("Git commit: %s\n", c);

	time(&tm);
	memcpy(t, asctime(localtime(&tm)), 24);
	t[24]=0;

	if( f = fopen("verstub.cpp","w")) {
		fprintf(f,
			"const char *build_commit = \"%s\";\n"
			"const char *build_time = \"%s\";\n"
			,c
			,t);
		fclose(f);
	}

	return 0;
}
