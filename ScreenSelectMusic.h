/* ScreenSelectMusic - Choose a Song and Steps. */

#ifndef SCREEN_SELECT_MUSIC_H
#define SCREEN_SELECT_MUSIC_H

#include "ScreenWithMenuElements.h"
#include "Sprite.h"
#include "BitmapText.h"
#include "GameConstantsAndTypes.h"
#include "MusicWheel.h"
#include "Banner.h"
#include "FadingBackground.h"
#include "FadingBanner.h"
#include "BPMDisplay.h"
#include "GrooveRadar.h"
#include "GrooveGraph.h"
#include "DifficultyIcon.h"
#include "DifficultyMeter.h"
#include "OptionIconRow.h"
#include "DifficultyDisplay.h"
#include "DifficultyList.h"
#include "CourseContentsList.h"
#include "HelpDisplay.h"
#include "PaneDisplay.h"
#include "Character.h"
#include "BGAnimation.h"
#include "RageUtil_BackgroundLoader.h"

#include "ModeSwitcher.h"

class TimingData;

class ScreenSelectMusic : public ScreenWithMenuElements
{
public:
	ScreenSelectMusic( CString sName );
	virtual ~ScreenSelectMusic();

	virtual void DrawPrimitives();

	virtual void Update( float fDeltaTime );
	virtual void Input( const DeviceInput& DeviceI, const InputEventType type, const GameInput &GameI, const MenuInput &MenuI, const StyleInput &StyleI );
	virtual void HandleScreenMessage( const ScreenMessage SM );

	virtual void MenuStart( PlayerNumber pn );
	virtual void MenuBack( PlayerNumber pn );

protected:
	void TweenOnScreen();
	void TweenOffScreen();
	void TweenScoreOnAndOffAfterChangeSort();
	enum DisplayMode { DISPLAY_SONGS, DISPLAY_COURSES, DISPLAY_MODES } m_DisplayMode;
	void SwitchDisplayMode( DisplayMode dm );
	void TweenSongPartsOnScreen( bool Initial );
	void TweenSongPartsOffScreen( bool Final );
	void TweenCoursePartsOnScreen( bool Initial );
	void TweenCoursePartsOffScreen( bool Final );

	void TweenWhileMoving();
	void TweenSongPartsWhileMoving();
	void TweenCoursePartsWhileMoving();

	void TweenWhileSettled();
	void TweenSongPartsWhileSettled();
	void TweenCoursePartsWhileSettled();

	void TweenWhenSectionChanges();
	void TweenSongPartsWhenSectionChanges();
	void TweenCoursePartsWhenSectionChanges();

	void SkipSongPartTweens();
	void SkipCoursePartTweens();

	void ChangeDifficulty( PlayerNumber pn, int dir );
	void ChangeDifficultyModeSwitch( PlayerNumber pn, Song* pSong, int dir, int index, int iPlayers );
	bool ModeSwitchOkayFor1P();
	bool ModeSwitchOkayFor2P();
	void ChangeGroup( int dir );

	void AfterStepsChange( PlayerNumber pn );
	void AfterTrailChange( PlayerNumber pn );
	void SwitchToPreferredDifficulty( StepsType st );
	void AfterMusicChange();
	void SortOrderChanged();
	void AfterSectionChange();

	void UpdateOptionsDisplays();
	void CheckBackgroundRequests();

	void NextSelectionState();

	vector<Steps*>			m_vpSteps;
	vector<Trail*>			m_vpTrails;
	vector<const Style*>	m_vPossibleStyles;

	StepsType		m_LastUsedStepsType;
	StepsType		m_LastChosenStepsType;

	bool			m_bUseModeSwitcher;
	CString			m_s1PlayerModes;
	CString			m_s2PlayerModes;
	bool			m_bModeSwitcherWraps;
	bool			m_bModeSwitcherLoadsAll;

	int				m_iScoreDigits;

	float			m_fFOV;
	float			m_fFOVCenterX;
	float			m_fFOVCenterY;

	enum SelectionState { SELECT_PREVIEW, SELECT_SONG, SELECT_STEPS, SELECT_FINALIZED, } m_iSelectionState;

	bool			m_bSampleMusicLoops;
	int				m_iPreviewMusicMode;

	Sprite			m_sprCharacterIcon[NUM_PLAYERS];
	AutoActor		m_MusicWheelUnder;
	MusicWheel		m_MusicWheel;

	bool			m_bShowBanners;
	bool			m_bShowDiscs;
	bool			m_bShowBackgrounds;
	bool			m_bShowPreviews;
	bool			m_bShowCDTitles;

	bool			m_bBannerWheel;
	bool			m_bPreviewLoadsCachedBG;
	bool			m_bPreviewInSprite;

	CString			m_sFallbackPreviewPath;
	CString			m_sAllMusicPreviewPath;
	CString			m_sSortPreviewPath;
	CString			m_sModePreviewPath;
	CString			m_sRoulettePreviewPath;
	CString			m_sRandomPreviewPath;

	bool			m_bLoadFallbackCDTitle;
	CString			m_sFallbackCDTitlePath;

	Sprite				m_sprBannerMask;
	TextBanner			m_TextBanner;
	FadingBanner		m_Banner;
	AutoActor			m_sprBannerFrame;

	Sprite				m_sprDiscMask;
	FadingBanner		m_Disc;
	AutoActor			m_sprDiscFrame;

	Sprite				m_sprBackgroundMask;
	FadingBackground	m_Background;
	AutoActor			m_sprBackgroundFrame;

	Sprite				m_sprPreviewMask;
	Sprite				m_sprPreview;
	FadingBackground	m_Preview;
	AutoActor			m_sprPreviewFrame;

	Sprite				m_sprCDTitleFront;
	Sprite				m_sprCDTitleBack;

	AutoActor			m_sprExplanation;
	BPMDisplay			m_BPMDisplay;
	DifficultyDisplay	m_DifficultyDisplay;
	Sprite				m_sprStage;
	GrooveRadar			m_GrooveRadar;
	GrooveGraph			m_GrooveGraph;
	BitmapText			m_textSongOptions;
	BitmapText			m_textCurMode;
	BitmapText			m_textNumSongs;
	BitmapText			m_textGroupName;
	BitmapText			m_textTotalTime;

	CourseContentsList	m_CourseContentsFrame;
	HelpDisplay			m_Artist;
	BitmapText			m_MachineRank;

	MusicSortDisplay	m_MusicSortDisplay;
	vector<bool>		m_bStageLengthBalloon;
	Sprite				m_sprBalloon;
	AutoActor			m_sprCourseHasMods;

	Sprite				m_sprOptionsMessage;

	bool			m_bShowDifficultyList;
	bool			m_bShowPanes;
	bool			m_bStepsTypeIcons;

	DifficultyList	m_DifficultyList;
	Sprite			m_sprDifficultyFrame[NUM_PLAYERS];
	Sprite			m_sprMeterFrame[NUM_PLAYERS];
	DifficultyIcon	m_DifficultyIcon[NUM_PLAYERS];
	Sprite			m_AutoGenIcon[NUM_PLAYERS];

	Sprite			m_sprNonPresence[NUM_PLAYERS];

	OptionIconRow	m_OptionIconRow[NUM_PLAYERS];
	PaneDisplay		m_PaneDisplay[NUM_PLAYERS];
	DifficultyMeter	m_DifficultyMeter[NUM_PLAYERS];
	Sprite			m_sprHighScoreFrame[NUM_PLAYERS];
	BitmapText		m_textHighScore[NUM_PLAYERS];

	BGAnimation		m_Overlay;
	Transition		m_bgOptionsOut;
	Transition		m_bgNoOptionsOut;

	RageSound		m_soundDifficultyEasier;
	RageSound		m_soundDifficultyHarder;
	RageSound		m_soundOptionsChange;
	RageSound		m_soundLocked;
	//RageSound		m_soundMove;

	bool		m_bMadeChoice;
	bool		m_bGoToOptions;
	bool		m_bAllowOptionsMenu, m_bAllowOptionsMenuRepeat;
	bool		m_bThemeMusic;

	CString		m_sSongOptionsExtraCommand;

	float		m_fSampleMusicDelay;
	bool		m_bAlignMusicBeats;
	bool		m_bBlinkAutogenIcon;
	bool		m_bTwoPartSelection;

	bool		m_bMenuUpDifficulty;
	bool		m_bMenuDownDifficulty;
	bool		m_bMenuUpGroup;
	bool		m_bMenuDownGroup;
	bool		m_bReverseDifficulty;
	bool		m_bReverseGroup;
	bool		m_bSectionChangeSavesPosition;

	bool		m_bUseSongNumber;
	bool		m_bNo2PInput;
	bool		m_bResetKeyTimerAfterSectionChange;
	bool		m_bPortalDisplaysSong;
	bool		m_bShowShortGroupName;

	bool		m_bBannerWaiting;
	bool		m_bDiscWaiting;
	bool		m_bBackgroundWaiting;
	bool		m_bPreviewWaiting;
	bool		m_bCDTitleWaiting;

	CString		m_sBannerPath;
	CString		m_sDiscPath;
	CString		m_sBackgroundPath;
	CString		m_sPreviewPath;
	CString		m_sCDTitlePath;

	float		m_fHQBanner;
	float		m_fHQBackground;

	bool		m_bSampleMusicWaiting;
	RageTimer	m_tStartedLoadingAt;
	CString		m_sSectionMusic;

	bool		m_bTweenWhileMovingAndSettled;
	bool		m_bTweenWhenSectionChanges;
	bool		m_bTweenWhenSelectionStateChanges;
	bool		m_bMovingCommandRepeats;

	bool		m_bMovingTweenWaiting;
	bool		m_bSettledTweenWaiting;
	bool		m_bSectionTweenWaiting;

	int			m_iLastSection;
	bool		m_bResetKeyTimers;

	CString		m_sScoreSortChangeCommand[NUM_PLAYERS];
	CString		m_sScoreFrameSortChangeCommand[NUM_PLAYERS];

	CString		m_sCharacterIconMovingCommand[NUM_PLAYERS];

	CString		m_sWheelUnderMovingCommand;
	CString		m_sWheelMovingCommand;

	CString		m_sTextBannerMovingCommand;
	CString		m_sBannerMovingCommand;
	CString		m_sBannerFrameMovingCommand;

	CString		m_sDiscMovingCommand;
	CString		m_sDiscFrameMovingCommand;

	CString		m_sBackgroundMovingCommand;
	CString		m_sBackgroundFrameMovingCommand;

	CString		m_sPreviewMovingCommand;
	CString		m_sPreviewFrameMovingCommand;

	CString		m_sCDTitleFrontMovingCommand;
	CString		m_sCDTitleBackMovingCommand;

	CString		m_sExplanationMovingCommand;
	CString		m_sBPMDisplayMovingCommand;
	CString		m_sDifficultyDisplayMovingCommand;
	CString		m_sStageMovingCommand;
	CString		m_sRadarMovingCommand;
	CString		m_sGraphMovingCommand;
	CString		m_sSongOptionsMovingCommand;
	CString		m_sCurModeMovingCommand;
	CString		m_sNumSongsMovingCommand;
	CString		m_sGroupNameMovingCommand;
	CString		m_sTotalTimeMovingCommand;

	CString		m_sArtistDisplayMovingCommand;
	CString		m_sMachineRankMovingCommand;

	CString		m_sSortIconMovingCommand;
	CString		m_sBalloonMovingCommand;
	CString		m_sCourseHasModsMovingCommand;

	CString		m_sOverlayMovingCommand;

	CString		m_sOptionIconsMovingCommand[NUM_PLAYERS];
	CString		m_sPaneDisplayMovingCommand[NUM_PLAYERS];
	CString		m_sDifficultyMeterMovingCommand[NUM_PLAYERS];
	CString		m_sScoreFrameMovingCommand[NUM_PLAYERS];
	CString		m_sScoreMovingCommand[NUM_PLAYERS];

	// Song Parts
	CString		m_sDifficultyListMovingCommand;
	CString		m_sDifficultyFrameMovingCommand[NUM_PLAYERS];
	CString		m_sMeterFrameMovingCommand[NUM_PLAYERS];
	CString		m_sDifficultyIconMovingCommand[NUM_PLAYERS];
	CString		m_sAutoGenIconMovingCommand[NUM_PLAYERS];
	CString		m_sNonPresenceMovingCommand[NUM_PLAYERS];

	// Course Parts
	CString		m_sCourseContentsMovingCommand;

	CString		m_sCharacterIconSettledCommand[NUM_PLAYERS];

	CString		m_sWheelUnderSettledCommand;
	CString		m_sWheelSettledCommand;

	CString		m_sTextBannerSettledCommand;
	CString		m_sBannerSettledCommand;
	CString		m_sBannerFrameSettledCommand;

	CString		m_sDiscSettledCommand;
	CString		m_sDiscFrameSettledCommand;

	CString		m_sBackgroundSettledCommand;
	CString		m_sBackgroundFrameSettledCommand;

	CString		m_sPreviewSettledCommand;
	CString		m_sPreviewFrameSettledCommand;

	CString		m_sCDTitleFrontSettledCommand;
	CString		m_sCDTitleBackSettledCommand;

	CString		m_sExplanationSettledCommand;
	CString		m_sBPMDisplaySettledCommand;
	CString		m_sDifficultyDisplaySettledCommand;
	CString		m_sStageSettledCommand;
	CString		m_sRadarSettledCommand;
	CString		m_sGraphSettledCommand;
	CString		m_sSongOptionsSettledCommand;
	CString		m_sCurModeSettledCommand;
	CString		m_sNumSongsSettledCommand;
	CString		m_sGroupNameSettledCommand;
	CString		m_sTotalTimeSettledCommand;

	CString		m_sArtistDisplaySettledCommand;
	CString		m_sMachineRankSettledCommand;

	CString		m_sSortIconSettledCommand;
	CString		m_sBalloonSettledCommand;
	CString		m_sCourseHasModsSettledCommand;

	CString		m_sOverlaySettledCommand;

	CString		m_sOptionIconsSettledCommand[NUM_PLAYERS];
	CString		m_sPaneDisplaySettledCommand[NUM_PLAYERS];
	CString		m_sDifficultyMeterSettledCommand[NUM_PLAYERS];
	CString		m_sScoreFrameSettledCommand[NUM_PLAYERS];
	CString		m_sScoreSettledCommand[NUM_PLAYERS];

	// Song Parts
	CString		m_sDifficultyListSettledCommand;
	CString		m_sDifficultyFrameSettledCommand[NUM_PLAYERS];
	CString		m_sMeterFrameSettledCommand[NUM_PLAYERS];
	CString		m_sDifficultyIconSettledCommand[NUM_PLAYERS];
	CString		m_sAutoGenIconSettledCommand[NUM_PLAYERS];
	CString		m_sNonPresenceSettledCommand[NUM_PLAYERS];

	// Course Parts
	CString		m_sCourseContentsSettledCommand;

	CString		m_sCharacterIconSectionCommand[NUM_PLAYERS];

	CString		m_sWheelUnderSectionCommand;
	CString		m_sWheelSectionCommand;

	CString		m_sTextBannerSectionCommand;
	CString		m_sBannerSectionCommand;
	CString		m_sBannerFrameSectionCommand;

	CString		m_sDiscSectionCommand;
	CString		m_sDiscFrameSectionCommand;

	CString		m_sBackgroundSectionCommand;
	CString		m_sBackgroundFrameSectionCommand;

	CString		m_sPreviewSectionCommand;
	CString		m_sPreviewFrameSectionCommand;

	CString		m_sCDTitleFrontSectionCommand;
	CString		m_sCDTitleBackSectionCommand;

	CString		m_sExplanationSectionCommand;
	CString		m_sBPMDisplaySectionCommand;
	CString		m_sDifficultyDisplaySectionCommand;
	CString		m_sStageSectionCommand;
	CString		m_sRadarSectionCommand;
	CString		m_sGraphSectionCommand;
	CString		m_sSongOptionsSectionCommand;
	CString		m_sCurModeSectionCommand;
	CString		m_sNumSongsSectionCommand;
	CString		m_sGroupNameSectionCommand;
	CString		m_sTotalTimeSectionCommand;

	CString		m_sArtistDisplaySectionCommand;
	CString		m_sMachineRankSectionCommand;

	CString		m_sSortIconSectionCommand;
	CString		m_sBalloonSectionCommand;
	CString		m_sCourseHasModsSectionCommand;

	CString		m_sOverlaySectionCommand;

	CString		m_sOptionIconsSectionCommand[NUM_PLAYERS];
	CString		m_sPaneDisplaySectionCommand[NUM_PLAYERS];
	CString		m_sDifficultyMeterSectionCommand[NUM_PLAYERS];
	CString		m_sScoreFrameSectionCommand[NUM_PLAYERS];
	CString		m_sScoreSectionCommand[NUM_PLAYERS];

	// Song Parts
	CString		m_sDifficultyListSectionCommand;
	CString		m_sDifficultyFrameSectionCommand[NUM_PLAYERS];
	CString		m_sMeterFrameSectionCommand[NUM_PLAYERS];
	CString		m_sDifficultyIconSectionCommand[NUM_PLAYERS];
	CString		m_sAutoGenIconSectionCommand[NUM_PLAYERS];
	CString		m_sNonPresenceSectionCommand[NUM_PLAYERS];

	// Course Parts
	CString		m_sCourseContentsSectionCommand;

	int					m_iSelection[NUM_PLAYERS];

	CString				m_sSampleMusicToPlay;
	TimingData*			m_pSampleMusicTimingData;
	float				m_fSampleStartSeconds, m_fSampleLengthSeconds;

	BackgroundLoader	m_BackgroundLoader;

	Song*				m_pSong;

};

#endif

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * Contains code (c) 2001-2004 Chris Danford.
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
