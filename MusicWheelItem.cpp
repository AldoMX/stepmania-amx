#include "global.h"
#include "MusicWheelItem.h"
#include "RageUtil.h"
#include "SongManager.h"
#include "GameManager.h"
#include "PrefsManager.h"
#include "ScreenManager.h"	// for sending SM_PlayMusicSample
#include "RageLog.h"
#include "GameConstantsAndTypes.h"
#include "GameState.h"
#include "ThemeManager.h"
#include "Steps.h"
#include "Song.h"
#include "Course.h"
#include "ProfileManager.h"
#include "ActorUtil.h"

// WheelItem stuff
#define ICON_X				THEME->GetMetricF(m_sName,"IconX")
#define SONG_NAME_X			THEME->GetMetricF(m_sName,"SongNameX")
#define SECTION_NAME_X		THEME->GetMetricF(m_sName,"SectionNameX")
#define SECTION_ZOOM		THEME->GetMetricF(m_sName,"SectionZoom")
#define ROULETTE_X			THEME->GetMetricF(m_sName,"RouletteX")
#define ROULETTE_ZOOM		THEME->GetMetricF(m_sName,"RouletteZoom")
#define GRADE_X( p )		THEME->GetMetricF(m_sName,ssprintf("GradeP%dX",p+1))

#define BANNER_WIDTH		THEME->GetMetricF(m_sName,"BannerWidth")
#define BANNER_HEIGHT		THEME->GetMetricF(m_sName,"BannerHeight")
#define BAR_UNDER			THEME->GetMetricB(m_sName,"BarUnderItem")
#define BAR_USES_COLOR		THEME->GetMetricI(m_sName,"BarUsesItemColor")

#define USE_BANNER_WHEEL		THEME->GetMetricB(sSSMName,"UseBannerWheel")
#define USE_BACKGROUND_WHEEL	THEME->GetMetricB(sSSMName,"UseBackgroundForBannerWheel")
#define	BANNER_WHEEL_MODE		THEME->GetMetricI(sSSMName,"BannerWheelMode")

WheelItemData::WheelItemData( WheelItemType wit, Song* pSong, CString sSectionName, CString sGroupName, CString sShortGroupName, Course* pCourse, RageColor color, SortOrder so )
{
	m_Type = wit;
	m_pSong = pSong;
	m_sSectionName = sSectionName;
	m_sGroupName = sGroupName;
	m_sShortGroupName = sShortGroupName;
	m_pCourse = pCourse;
	m_color = color;
	m_Flags = WheelNotifyIcon::Flags();
	m_SortOrder = so;
}

MusicWheelItem::MusicWheelItem( const CString& sName, const CString& sSSMName )
{
	data = NULL;
	SetName( sName, "MusicWheelItem" );

	m_fPercentGray = 0;
	m_WheelNotifyIcon.SetXY( ICON_X, 0 );

	m_sprSongBar.Load( THEME->GetPathG( m_sName, "song" ) );
	m_sprSongBar.SetXY( 0, 0 );
	m_All.AddChild( &m_sprSongBar );

	m_sprSectionBar.Load( THEME->GetPathG( m_sName, "section" ) );
	m_sprSectionBar.SetXY( 0, 0 );
	m_All.AddChild( &m_sprSectionBar );

	m_sprGlowBar.Load( THEME->GetPathG( m_sName, "glow" ) );
	m_sprGlowBar.SetXY( 0, 0 );
	m_All.AddChild( &m_sprGlowBar );

	m_bBannerWheel = USE_BANNER_WHEEL;

	// Load if we're using the banner wheel
	if( m_bBannerWheel )
	{
		m_sprMask.SetName( "Banner" );
		m_sprMask.Load( THEME->GetPathG(m_sName, "mask") );
		SET_XY_AND_ON_COMMAND( m_sprMask );
		m_sprMask.ScaleToClipped( BANNER_WIDTH, BANNER_HEIGHT );
		m_sprMask.SetClearZBuffer( true );
		m_sprMask.SetBlendMode( BLEND_NO_EFFECT );
		m_sprMask.SetZWrite( true );
		m_All.AddChild( &m_sprMask );

		m_bDiscOrPreview = BANNER_WHEEL_MODE != 0;
		m_bBackgroundWheel = USE_BACKGROUND_WHEEL;

		if( m_bBackgroundWheel )
		{
			m_Background.m_bBGCache = true;
			m_Background.SetName( "Banner" );
			SET_XY_AND_ON_COMMAND( m_Background );
			m_Background.ScaleToClipped( BANNER_WIDTH, BANNER_HEIGHT );
			m_Background.SetZTestMode( ZTEST_WRITE_ON_PASS );	// do have to pass the z test
			m_All.AddChild( &m_Background );
		}
		else
		{
			m_Banner.m_bBNCache = true;
			m_Banner.SetName( "Banner" );
			SET_XY_AND_ON_COMMAND( m_Banner );
			m_Banner.ScaleToClipped( BANNER_WIDTH, BANNER_HEIGHT );
			m_Banner.SetZTestMode( ZTEST_WRITE_ON_PASS );	// do have to pass the z test
			m_All.AddChild( &m_Banner );
		}
	}
	// If not, load the normal text selection!
	else
	{
		m_textSortGroup.LoadFromFont( THEME->GetPathF( m_sName, "sort group" ) );
		m_textSortGroup.SetShadowLength( 0 );
		m_textSortGroup.SetVertAlign( align_middle );
		m_textSortGroup.SetXY( SECTION_NAME_X, 0 );
		m_textSortGroup.SetZoom( SECTION_ZOOM );
		m_All.AddChild( &m_textSortGroup );

		m_textSortFolder.LoadFromFont( THEME->GetPathF( m_sName, "sort folder" ) );
		m_textSortFolder.SetShadowLength( 0 );
		m_textSortFolder.SetVertAlign( align_middle );
		m_textSortFolder.SetXY( SECTION_NAME_X, 0 );
		m_textSortFolder.SetZoom( SECTION_ZOOM );
		m_All.AddChild( &m_textSortFolder );

		m_textRoulette.LoadFromFont( THEME->GetPathF( m_sName, "roulette" ) );
		m_textRoulette.SetShadowLength( 0 );
		m_textRoulette.TurnRainbowOn();
		m_textRoulette.SetZoom( ROULETTE_ZOOM );
		m_textRoulette.SetXY( ROULETTE_X, 0 );
		m_All.AddChild( &m_textRoulette );

		m_TextBanner.SetName( m_sName + " TextBanner", "TextBanner" );
		m_TextBanner.SetHorizAlign( align_left );
		m_TextBanner.SetXY( SONG_NAME_X, 0 );
		m_All.AddChild( &m_TextBanner );

		m_textCourse.SetName( "CourseName" );
		m_textCourse.LoadFromFont( THEME->GetPathF( m_sName, "course" ) );
		SET_XY_AND_ON_COMMAND( &m_textCourse );
		m_All.AddChild( &m_textCourse );
	}

	m_textSectionName.LoadFromFont( THEME->GetPathF( m_sName, "section" ) );
	m_textSectionName.SetShadowLength( 0 );
	m_textSectionName.SetVertAlign( align_middle );
	m_textSectionName.SetXY( SECTION_NAME_X, 0 );
	m_textSectionName.SetZoom( SECTION_ZOOM );
	m_All.AddChild( &m_textSectionName );

	// Load the fonts for the various sorts
	GAMESTATE->m_bLoadingSortFonts = true;

	m_textSortTitle.LoadFromFont( THEME->GetPathF( m_sName, "sort title" ) );
	m_textSortTitle.SetShadowLength( 0 );
	m_textSortTitle.SetVertAlign( align_middle );
	m_textSortTitle.SetXY( SECTION_NAME_X, 0 );
	m_textSortTitle.SetZoom( SECTION_ZOOM );
	m_All.AddChild( &m_textSortTitle );

	m_textSortGenre.LoadFromFont( THEME->GetPathF( m_sName, "sort genre" ) );
	m_textSortGenre.SetShadowLength( 0 );
	m_textSortGenre.SetVertAlign( align_middle );
	m_textSortGenre.SetXY( SECTION_NAME_X, 0 );
	m_textSortGenre.SetZoom( SECTION_ZOOM );
	m_All.AddChild( &m_textSortGenre );

	m_textSortBPM.LoadFromFont( THEME->GetPathF( m_sName, "sort bpm" ) );
	m_textSortBPM.SetShadowLength( 0 );
	m_textSortBPM.SetVertAlign( align_middle );
	m_textSortBPM.SetXY( SECTION_NAME_X, 0 );
	m_textSortBPM.SetZoom( SECTION_ZOOM );
	m_All.AddChild( &m_textSortBPM );

	m_textSortGrade.LoadFromFont( THEME->GetPathF( m_sName, "sort grade" ) );
	m_textSortGrade.SetShadowLength( 0 );
	m_textSortGrade.SetVertAlign( align_middle );
	m_textSortGrade.SetXY( SECTION_NAME_X, 0 );
	m_textSortGrade.SetZoom( SECTION_ZOOM );
	m_All.AddChild( &m_textSortGrade );

	m_textSortArtist.LoadFromFont( THEME->GetPathF( m_sName, "sort artist" ) );
	m_textSortArtist.SetShadowLength( 0 );
	m_textSortArtist.SetVertAlign( align_middle );
	m_textSortArtist.SetXY( SECTION_NAME_X, 0 );
	m_textSortArtist.SetZoom( SECTION_ZOOM );
	m_All.AddChild( &m_textSortArtist );

	m_textSortEasy.LoadFromFont( THEME->GetPathF( m_sName, "sort easy meter" ) );
	m_textSortEasy.SetShadowLength( 0 );
	m_textSortEasy.SetVertAlign( align_middle );
	m_textSortEasy.SetXY( SECTION_NAME_X, 0 );
	m_textSortEasy.SetZoom( SECTION_ZOOM );
	m_All.AddChild( &m_textSortEasy );

	m_textSortMedium.LoadFromFont( THEME->GetPathF( m_sName, "sort medium meter" ) );
	m_textSortMedium.SetShadowLength( 0 );
	m_textSortMedium.SetVertAlign( align_middle );
	m_textSortMedium.SetXY( SECTION_NAME_X, 0 );
	m_textSortMedium.SetZoom( SECTION_ZOOM );
	m_All.AddChild( &m_textSortMedium );

	m_textSortHard.LoadFromFont( THEME->GetPathF( m_sName, "sort hard meter" ) );
	m_textSortHard.SetShadowLength( 0 );
	m_textSortHard.SetVertAlign( align_middle );
	m_textSortHard.SetXY( SECTION_NAME_X, 0 );
	m_textSortHard.SetZoom( SECTION_ZOOM );
	m_All.AddChild( &m_textSortHard );

	m_textSortChallenge.LoadFromFont( THEME->GetPathF( m_sName, "sort challenge meter" ) );
	m_textSortChallenge.SetShadowLength( 0 );
	m_textSortChallenge.SetVertAlign( align_middle );
	m_textSortChallenge.SetXY( SECTION_NAME_X, 0 );
	m_textSortChallenge.SetZoom( SECTION_ZOOM );
	m_All.AddChild( &m_textSortChallenge );

	// Finally, load for Roulette
	GAMESTATE->m_bLoadingSortFonts = false;

	m_textSort.SetName( "Sort" );
	m_textSort.LoadFromFont( THEME->GetPathF( m_sName, "sort" ) );
	SET_XY_AND_ON_COMMAND( &m_textSort );
	m_All.AddChild( &m_textSort );

	m_bBarUnderItem = BAR_UNDER;
	m_iBarUsesColor = BAR_USES_COLOR;

	FOREACH_PlayerNumber( p )
	{
		m_GradeDisplay[p].Load( THEME->GetPathG( m_sName, "grades" ) );
		m_GradeDisplay[p].SetZoom( 1.0f );
		m_GradeDisplay[p].SetXY( GRADE_X(p), 0 );
		m_All.AddChild( &m_GradeDisplay[p] );
	}
}

void MusicWheelItem::LoadFromWheelItemData( WheelItemData* pWID )
{
	ASSERT( pWID != NULL );

	data = pWID;
	/*
	// copy all data items
	this->m_Type				= pWID->m_Type;
	this->m_sSectionName		= pWID->m_sSectionName;
	this->m_sGroupName			= pWID->m_sGroupName;
	this->m_sShortGroupName		= pWID->m_sShortGroupName;
	this->m_pCourse				= pWID->m_pCourse;
	this->m_pSong				= pWID->m_pSong;
	this->m_color				= pWID->m_color;
	this->m_Type				= pWID->m_Type; */

	// init type specific stuff
	switch( pWID->m_Type )
	{
	case TYPE_SECTION:
	case TYPE_COURSE:
	case TYPE_SORT:
	{
		CString sDisplayName, sTranslitName;
		BitmapText *bt = NULL;
		bool bBitmapText = true;
		switch( pWID->m_Type )
		{
		case TYPE_SECTION:
		{
			switch( GAMESTATE->m_SortOrder )
			{
			case SORT_GROUP:
			case SORT_FOLDER:
				if( m_bBannerWheel )
				{
					if( m_bBackgroundWheel )
						m_Background.LoadFromGroup( data->m_sSectionName, m_bDiscOrPreview );
					else
						m_Banner.LoadFromGroup( data->m_sSectionName, m_bDiscOrPreview );

					bBitmapText = false;
				}
				else
				{
					sDisplayName = data->m_sShortGroupName;
					switch( GAMESTATE->m_SortOrder )
					{
					case SORT_GROUP:	bt = &m_textSortGroup;	break;
					case SORT_FOLDER:	bt = &m_textSortFolder;	break;
					default:	ASSERT(0);
					}
				}
				break;
			default:
				if( m_bBannerWheel )
				{
					if( m_bBackgroundWheel )
						m_Background.LoadSort( m_bDiscOrPreview );
					else
						m_Banner.LoadSort( m_bDiscOrPreview );
				}

				sDisplayName = data->m_sSectionName;
				{
					switch( GAMESTATE->m_SortOrder )
					{
					case SORT_TITLE:			bt = &m_textSortTitle;		break;
					case SORT_GENRE:			bt = &m_textSortGenre;		break;
					case SORT_BPM:				bt = &m_textSortBPM;		break;
					case SORT_GRADE:			bt = &m_textSortGrade;		break;
					case SORT_ARTIST:			bt = &m_textSortArtist;		break;
					case SORT_EASY_METER:		bt = &m_textSortEasy;		break;
					case SORT_MEDIUM_METER:		bt = &m_textSortMedium;		break;
					case SORT_HARD_METER:		bt = &m_textSortHard;		break;
					case SORT_CHALLENGE_METER:	bt = &m_textSortChallenge;	break;
					default:					bt = &m_textSectionName;
					}
				}
			}
			break;
		}
		case TYPE_COURSE:
			if( m_bBannerWheel )
			{
				if( m_bBackgroundWheel )
					m_Background.LoadFromCourse( data->m_pCourse, m_bDiscOrPreview );
				else
					m_Banner.LoadFromCourse( data->m_pCourse, m_bDiscOrPreview );

				bBitmapText = false;
			}
			else
			{
				sDisplayName = data->m_pCourse->GetFullDisplayTitle();
				sTranslitName = data->m_pCourse->GetFullTranslitTitle();
				bt = &m_textCourse;
			}
			break;
		case TYPE_SORT:
			if( m_bBannerWheel )
			{
				if( m_bBackgroundWheel )
					m_Background.LoadSort( m_bDiscOrPreview );
				else
					m_Banner.LoadSort( m_bDiscOrPreview );
			}

			sDisplayName = data->m_sLabel;
			bt = &m_textSort;
			break;
		default:
			ASSERT(0);
		}

		if( bBitmapText )
		{
			bt->SetZoom( 1 );
			bt->SetText( sDisplayName, sTranslitName );
			bt->SetDiffuse( data->m_color );
			bt->TurnRainbowOff();

			const float fSourcePixelWidth = (float)bt->GetUnzoomedWidth();
			const float fMaxTextWidth = 200;

			if( fSourcePixelWidth > fMaxTextWidth  )
				bt->SetZoomX( fMaxTextWidth / fSourcePixelWidth );
		}
		break;
	}
	case TYPE_SONG:
		if( m_bBannerWheel )
		{
			if( m_bBackgroundWheel )
				m_Background.LoadFromSong( data->m_pSong, m_bDiscOrPreview );
			else
				m_Banner.LoadFromSong( data->m_pSong, m_bDiscOrPreview );
		}
		else
		{
			m_TextBanner.LoadFromSong( data->m_pSong );
			m_TextBanner.SetDiffuse( data->m_color );
		}

		m_WheelNotifyIcon.SetFlags( data->m_Flags );
		RefreshGrades();
		break;
	case TYPE_ROULETTE:
		if( m_bBannerWheel )
		{
			if( m_bBackgroundWheel )
				m_Background.LoadRoulette( m_bDiscOrPreview );
			else
				m_Banner.LoadRoulette( m_bDiscOrPreview );
		}
		else
			m_textRoulette.SetText( THEME->GetMetric("MusicWheel","Roulette") );
		break;
	case TYPE_RANDOM:
		if( m_bBannerWheel )
		{
			if( m_bBackgroundWheel )
				m_Background.LoadRandom( m_bDiscOrPreview );
			else
				m_Banner.LoadRandom( m_bDiscOrPreview );
		}
		else
			m_textRoulette.SetText( THEME->GetMetric("MusicWheel","Random") );
		break;

	case TYPE_PORTAL:
		if( m_bBannerWheel )
		{
			if( m_bBackgroundWheel )
				m_Background.LoadAllMusic( m_bDiscOrPreview );
			else
				m_Banner.LoadAllMusic( m_bDiscOrPreview );
		}
		else
			m_textRoulette.SetText( THEME->GetMetric("MusicWheel","Portal") );
		break;

	default:
		ASSERT( 0 );	// invalid type
	}
}

void MusicWheelItem::RefreshGrades()
{
	// Refresh Grades
	FOREACH_PlayerNumber( p )
	{
		if( !data->m_pSong || !GAMESTATE->IsHumanPlayer(p) ) // this isn't a song display, or not a human player
		{
			m_GradeDisplay[p].SetDiffuse( RageColor(1,1,1,0) );
			continue;
		}

		Difficulty dc;
		Grade grade;

		if( GAMESTATE->m_pCurSteps[p] )
			dc = GAMESTATE->m_pCurSteps[p]->GetDifficulty();
		else
			dc = GAMESTATE->m_PreferredDifficulty[p];

		if( PROFILEMAN->IsUsingProfile((PlayerNumber)p) )
			grade = PROFILEMAN->GetHighScoreForDifficulty( data->m_pSong, GAMESTATE->GetCurrentStyle(), (ProfileSlot)p, dc ).grade;
		else
			grade = PROFILEMAN->GetHighScoreForDifficulty( data->m_pSong, GAMESTATE->GetCurrentStyle(), PROFILE_SLOT_MACHINE, dc ).grade;

		m_GradeDisplay[p].SetGrade( (PlayerNumber)p, grade );
	}

}


void MusicWheelItem::Update( float fDeltaTime )
{
	Actor::Update( fDeltaTime );

	if( m_bBannerWheel )
	{
		m_sprMask.Update( fDeltaTime );

		if( m_bBackgroundWheel )
			m_Background.Update( fDeltaTime );
		else
			m_Banner.Update( fDeltaTime );
	}
	else
	{
		switch( data->m_Type )
		{
		case TYPE_SECTION:
		{
			switch( GAMESTATE->m_SortOrder )
			{
			case SORT_GROUP:
				m_textSortGroup.Update( fDeltaTime );
				break;
			case SORT_FOLDER:
				m_textSortFolder.Update( fDeltaTime );
				break;
			}
			break;
		}
		case TYPE_ROULETTE:
		case TYPE_RANDOM:
		case TYPE_PORTAL:
			m_textRoulette.Update( fDeltaTime );
			break;
		case TYPE_SONG:
			m_TextBanner.Update( fDeltaTime );
			break;
		case TYPE_COURSE:
			m_textCourse.Update( fDeltaTime );
			break;
		}
	}

	switch( data->m_Type )
	{
	case TYPE_SONG:
	{
		m_WheelNotifyIcon.Update( fDeltaTime );

		FOREACH_PlayerNumber( p )
			m_GradeDisplay[p].Update( fDeltaTime );
	}
	case TYPE_COURSE:
		m_sprSongBar.Update( fDeltaTime );
		break;
	case TYPE_ROULETTE:
	case TYPE_RANDOM:
	case TYPE_PORTAL:
		m_sprSectionBar.Update( fDeltaTime );
		break;
	case TYPE_SECTION:
	{
		m_sprSectionBar.Update( fDeltaTime );

		switch( GAMESTATE->m_SortOrder )
		{
		case SORT_GROUP:
		case SORT_FOLDER:
			break;
		case SORT_TITLE:
			m_textSortTitle.Update( fDeltaTime );
			break;
		case SORT_GENRE:
			m_textSortGenre.Update( fDeltaTime );
			break;
		case SORT_BPM:
			m_textSortBPM.Update( fDeltaTime );
			break;
		case SORT_GRADE:
			m_textSortGrade.Update( fDeltaTime );
			break;
		case SORT_ARTIST:
			m_textSortArtist.Update( fDeltaTime );
			break;
		case SORT_EASY_METER:
			m_textSortEasy.Update( fDeltaTime );
			break;
		case SORT_MEDIUM_METER:
			m_textSortMedium.Update( fDeltaTime );
			break;
		case SORT_HARD_METER:
			m_textSortHard.Update( fDeltaTime );
			break;
		case SORT_CHALLENGE_METER:
			m_textSortChallenge.Update( fDeltaTime );
			break;
		default:
			m_textSectionName.Update( fDeltaTime );
			break;
		}
		break;
	}
	case TYPE_SORT:
		m_textSort.Update( fDeltaTime );
		m_sprSectionBar.Update( fDeltaTime );
		break;
	default:
		ASSERT(0);
	}

	m_sprGlowBar.Update( fDeltaTime );
}

void MusicWheelItem::DrawPrimitives()
{
	Sprite *bar = NULL;
	switch( data->m_Type )
	{
	case TYPE_SECTION:
	case TYPE_ROULETTE:
	case TYPE_RANDOM:
	case TYPE_PORTAL:
	case TYPE_SORT:
		bar = &m_sprSectionBar;
		break;
	case TYPE_SONG:
	case TYPE_COURSE:
		bar = &m_sprSongBar;
		break;
	default: ASSERT(0);
	}

	if( m_fPercentGray > 0 )
	{
		bar->SetGlow( RageColor(0,0,0,m_fPercentGray) );
		bar->SetDiffuse( RageColor(0,0,0,m_fPercentGray) );
	}
	else
	{
		RageColor color;
		bool bSetColor = true;
		switch( m_iBarUsesColor )
		{
		case 1:		color = data->m_color;									break;
		case 2:		color = SONGMAN->GetGroupColor( data->m_sGroupName );	break;
		default:	bSetColor = false;
		}

		if( bSetColor )
			bar->SetDiffuseColor( color );
	}

	if( m_bBarUnderItem )
	{
		bar->Draw();

		if( m_fPercentGray > 0 )
		{
			bar->SetDiffuse( RageColor(0,0,0,1) );
			bar->SetGlow( RageColor(0,0,0,0) );
		}
	}


	if( m_bBannerWheel )
	{
		m_sprMask.Draw();

		if( m_bBackgroundWheel )
			m_Background.Draw();
		else
			m_Banner.Draw();
	}
	else
	{
		switch( data->m_Type )
		{
		case TYPE_SECTION:
		{
			switch( GAMESTATE->m_SortOrder )
			{
			case SORT_GROUP:	m_textSortGroup.Draw();		break;
			case SORT_FOLDER:	m_textSortFolder.Draw();	break;
			}
			break;
		}
		case TYPE_ROULETTE:
		case TYPE_RANDOM:
		case TYPE_PORTAL:
			m_textRoulette.Draw();
			break;
		case TYPE_SONG:
			m_TextBanner.Draw();
			break;
		case TYPE_COURSE:
			m_textCourse.Draw();
			break;
		case TYPE_SORT:
			break;
		default:
			ASSERT(0);
		}
	}

	switch( data->m_Type )
	{
	case TYPE_SECTION:
	{
		switch( GAMESTATE->m_SortOrder )
		{
		case SORT_GROUP:
		case SORT_FOLDER:
			break;
		case SORT_TITLE:
			m_textSortTitle.Draw();
			break;
		case SORT_GENRE:
			m_textSortGenre.Draw();
			break;
		case SORT_BPM:
			m_textSortBPM.Draw();
			break;
		case SORT_GRADE:
			m_textSortGrade.Draw();
			break;
		case SORT_ARTIST:
			m_textSortArtist.Draw();
			break;
		case SORT_EASY_METER:
			m_textSortEasy.Draw();
			break;
		case SORT_MEDIUM_METER:
			m_textSortMedium.Draw();
			break;
		case SORT_HARD_METER:
			m_textSortHard.Draw();
			break;
		case SORT_CHALLENGE_METER:
			m_textSortChallenge.Draw();
			break;
		default:
			m_textSectionName.Draw();
			break;
		}
		break;
	}
	case TYPE_SONG:
		m_WheelNotifyIcon.Draw();
		FOREACH_PlayerNumber( p )
			m_GradeDisplay[p].Draw();
		break;
	case TYPE_SORT:
		m_textSort.Draw();
		break;
	case TYPE_ROULETTE:
	case TYPE_RANDOM:
	case TYPE_PORTAL:
	case TYPE_COURSE:
		break;
	default:
		ASSERT(0);
	}

	if( !m_bBarUnderItem )
	{
		bar->Draw();

		if( m_fPercentGray > 0 )
		{
			bar->SetDiffuse( RageColor(0,0,0,1) );
			bar->SetGlow( RageColor(0,0,0,0) );
		}
	}

	m_sprGlowBar.Draw();
}


void MusicWheelItem::SetZTestMode( ZTestMode mode )
{
	ActorFrame::SetZTestMode( mode );

	// set all sub-Actors
	m_All.SetZTestMode( mode );
}

void MusicWheelItem::SetZWrite( bool bZWrite )
{
	ActorFrame::SetZWrite( bZWrite );

	// set all sub-Actors
	m_All.SetZWrite( bZWrite );
}

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * Contains code (c) 2001-2004 Chris Danford, Chris Gomez, Glenn Maynard.
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
