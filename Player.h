/* Player - Accepts input, knocks down TapNotes that were stepped on, and keeps score for the player. */

#ifndef PLAYER_H
#define PLAYER_H

#include "PrefsManager.h"	// for GameplayStatistics
#include "Sprite.h"
#include "BitmapText.h"

#include "ActorFrame.h"
#include "RandomSample.h"
#include "Judgment.h"
#include "HoldJudgment.h"
#include "Combo.h"
#include "NoteDataWithScoring.h"
#include "ArrowBackdrop.h"
#include "RageTimer.h"
#include "ProTimingDisplay.h"
#include "RageSound.h"
#include "AttackDisplay.h"
#include "NoteField.h"

class ScoreDisplay;
class LifeMeter;
class CombinedLifeMeter;
class ScoreKeeper;
class Inventory;

struct PlayerOptions;

#define	SAMPLE_COUNT			16
#define CPU_ROLL_REHIT_LIFE		0.25
#define CPU_ROLL_LETGO_LIFE		0.7

class PlayerMinus : public NoteDataWithScoring, public ActorFrame
{
	friend class PlayerContainer;

public:
	PlayerMinus();
	~PlayerMinus();

	virtual void Update( float fDeltaTime );
	virtual void DrawPrimitives();

	void InitJudgment( PlayerNumber pn, Difficulty dc );
	void Load( PlayerNumber player_no, const NoteData* pNoteData, LifeMeter* pLM, CombinedLifeMeter* pCombinedLM, ScoreDisplay* pScoreDisplay, ScoreDisplay* pSecondaryScoreDisplay, Inventory* pInventory, ScoreKeeper* pPrimaryScoreKeeper, ScoreKeeper* pSecondaryScoreKeeper, NoteField* pNoteField );
	void CrossedRow( int iNoteRow );
	void CrossedItemRow( int iNoteRow );
	void Step( int col, RageTimer tm );
	void StepCheckpoint( int col, int row, bool hit = true );
	void MissSkippedCheckpoint( int col, int row );
	void Release( int col, RageTimer tm );
	void RandomiseNotes( int iNoteRow );
	void FadeToFail();
	int GetDancingCharacterState() const { return m_iDCState; };
	void SetCharacterState(int iDCState) { m_iDCState = iDCState; };
	void ApplyWaitingTransforms();
	void UpdateNoteFieldRotation( const PlayerOptions& po, bool bForced = false );
	bool GetReverseStatus();

	float m_fActiveRandomAttackStart;

	CString ApplyRandomAttack();

	static float GetMaxStepDistanceSeconds();

protected:
	void UpdateTapNotesMissedOlderThan( int iMissIfOlderThanThisIndex, bool bFreeze = false );
	void OnRowCompletelyJudged( int iStepIndex, bool bSteppedEarly );
	void HandleTapRowScore( unsigned row, TapNoteScore scoreOfLastTap = TNS_NONE );
	void HandleHoldScore( HoldNoteScore holdScore, const unsigned& uComboMultiplier, const float& fScoreMultiplier, const float& fLifeMultiplier );
	void HandleAutosync(float fNoteOffset);
	void DrawTapJudgments();
	void DrawHoldJudgments();
	int GetClosestNoteDirectional( int col, float fBeat, float fTimeBeat, float fMaxBeatsAhead, int iDirection, float& fTime ) const;
	int GetClosestNote( int col, float fBeat, float fMaxBeatsAhead, float fMaxBeatsBehind ) const;

	// These exist to make the Update() function tidier
	void UpdateHoldNotes( float fDeltaTime, int iSongRow );
	bool UpdateHoldCheckpoints( int iSongRow, float fStopStart = 0, float fStopSeconds = 0, bool bEarly = true, bool bLate = false );

	TapNoteScore ReverseGrade( TapNoteScore score );

	PlayerNumber		m_PlayerNumber;

	float				m_fOffset[SAMPLE_COUNT]; // for AutoSync
	int					m_iOffsetSample;

	ArrowBackdrop		m_ArrowBackdrop;
	NoteField*			m_pNoteField;

	HoldJudgment		m_HoldJudgment[MAX_NOTE_TRACKS];

	float				m_HoldPressed[MAX_NOTE_TRACKS];
	int					m_iCurrentHoldEndRow[MAX_NOTE_TRACKS];

	Judgment*			m_Judgment;
	ProTimingDisplay	m_ProTimingDisplay;

	Combo				m_Combo;

	AttackDisplay		m_AttackDisplay;

	int m_iDCState;
	LifeMeter*			m_pLifeMeter;
	CombinedLifeMeter*	m_pCombinedLifeMeter;
	ScoreDisplay*		m_pScoreDisplay;
	ScoreDisplay*		m_pSecondaryScoreDisplay;
	ScoreKeeper*		m_pPrimaryScoreKeeper;
	ScoreKeeper*		m_pSecondaryScoreKeeper;
	Inventory*			m_pInventory;

	// This is only used for RollNotes, nothing else
	map<RowTrack,bool>	m_bMustRelease;		// true if button is being held down
	map<RowTrack,bool>	m_bPressed;		// true if button is being held down

	int		m_iLastHold;

	int		m_iLastRow;
	int		m_iLastItemRow;
	int		m_iLastMissedRow;

	RageSound	m_soundMine;
	RageSound	m_soundShock;
	RageSound	m_soundPotion;
	RageSound	m_soundAttackLaunch;
	RageSound	m_soundAttackEnding;

	const TapNote& m_EmptyNote;
	TimingData*	m_Timing;

	map<int,bool> m_LateCheckpoints;

	bool	m_bCrossingHoldNote[MAX_NOTE_TRACKS];

	float m_fLRAddY;
	float m_fLRStartY;
	float m_fLRJudgmentY;
	float m_fLRComboY;
	float m_fLRNoteFieldZoom;
	float m_fCachedZoom;

	float m_fCachedRotationX;
	float m_fCachedRotationY;
	float m_fCachedRotationZ;
	float m_fCachedScrollDrop;
};

class Player : public PlayerMinus
{
public:
	void Load( PlayerNumber player_no, const NoteData* pNoteData, LifeMeter* pLM, CombinedLifeMeter* pCombinedLM, ScoreDisplay* pScoreDisplay, ScoreDisplay* pSecondaryScoreDisplay, Inventory* pInventory, ScoreKeeper* pPrimaryScoreKeeper, ScoreKeeper* pSecondaryScoreKeeper );

protected:
	NoteField		m_NoteField;

};

#endif

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * Contains code (c) 2001-2004 Chris Danford.
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
