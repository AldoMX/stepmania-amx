#include "global.h"
#include "Song.h"
#include "Steps.h"
#include "RageUtil.h"
#include "RageLog.h"
#include "IniFile.h"
#include "NoteData.h"
#include "RageSoundReader_FileReader.h"
#include "RageSurface_Load.h"
#include "RageException.h"
#include "SongCacheIndex.h"
#include "GameManager.h"
#include "PrefsManager.h"
#include "Style.h"
#include "GameState.h"
#include "FontCharAliases.h"
#include "TitleSubstitution.h"
#include "BannerCache.h"
#include "BackgroundCache.h"
#include "Sprite.h"
#include "arch/arch.h"
#include "RageFile.h"
#include "RageFileManager.h"
#include "RageSurface.h"
#include "NoteDataUtil.h"
#include "ProfileManager.h"
#include "Foreach.h"
#include "BPMRange.h"

#include "NotesLoaderSM.h"
#include "NotesLoaderDWI.h"
#include "NotesLoaderBMS.h"
#include "NotesLoaderKSF.h"
#include "NotesLoaderUCS.h"
#include "NotesWriterDWI.h"
#include "NotesWriterSM.h"

#include "LyricsLoader.h"
#include "UnlockSystem.h"
#include "SongManager.h"

#include <set>
#include <iterator>
#include <numeric>

#define CACHE_DIR "Cache/"

const int FILE_CACHE_VERSION = 161;	// increment this to invalidate cache

const float DEFAULT_MUSIC_SAMPLE_LENGTH = 10.f;

int CompareBGChanges(const BGChange &seg1, const BGChange &seg2)
{
	return seg1.m_fBeat < seg2.m_fBeat;
}

void SortBGChangesArray( BGChangeArray &arrayBGChanges )
{
	sort( arrayBGChanges.begin(), arrayBGChanges.end(), CompareBGChanges );
}

CString BGChangeArray::GetString( CString separator ) const
{
	CStringArray saBGChanges;
	saBGChanges.reserve(size());

	for( BGChangeArray::const_iterator it = begin(); it != end(); ++it )
		saBGChanges.push_back( ssprintf( "%f=%f=%d=%d=%d=%s", it->m_fBeat, it->m_fRate, it->m_bFadeLast, it->m_bRewindMovie, it->m_bLoop, it->m_sBGName.c_str() ) );

	return join(separator, saBGChanges);
}

void BGChangeArray::FromString( CString& bgchanges, CString separator )
{
	clear();

	CStringArray saBGChanges;
	split(bgchanges, separator, saBGChanges, true);

	for( CStringArray::const_iterator it = saBGChanges.begin(); it != saBGChanges.end(); ++it )
	{
		BGChange change;
		int fadelast, rewind, loop;
		sscanf(*it, "%f=%f=%d=%d=%d", &change.m_fBeat, &change.m_fRate, &fadelast, &rewind, &loop);
		change.m_bFadeLast = fadelast != 0;
		change.m_bRewindMovie = rewind != 0;
		change.m_bLoop = loop != 0;
		change.m_sBGName = it->substr(it->find_last_of('=')+1);
		push_back(change);
	}
}

CString LyricArray::GetString( CString separator ) const
{
	CStringArray saLyrics;
	saLyrics.reserve(size());

	for( LyricArray::const_iterator it = begin(); it != end(); ++it )
	{
		CString sLyric = it->m_sLyric;
		replace(sLyric.begin(), sLyric.end(), '\n', '|');
		saLyrics.push_back( ssprintf( "%f=%f=%f=%f=%f=%f=%s", it->m_fStartTime, it->m_Color.r, it->m_Color.g, it->m_Color.b, it->m_Color.a, it->m_sLyric.c_str() ) );
	}

	return join(separator, saLyrics);
}

void LyricArray::FromString( CString& lyrics, CString separator )
{
	clear();

	CStringArray saLyrics;
	split(lyrics, separator, saLyrics, true);

	for( CStringArray::const_iterator it = saLyrics.begin(); it != saLyrics.end(); ++it )
	{
		Lyric lyric;
		sscanf(*it, "%f=%f=%f=%f=%f=%f", &lyric.m_fStartTime, &lyric.m_Color.r, &lyric.m_Color.g, &lyric.m_Color.b, &lyric.m_Color.a);
		lyric.m_sLyric = it->substr(it->find_last_of('=')+1);
		push_back(lyric);
	}
}

//////////////////////////////
// Song
//////////////////////////////
Song::Song()
{
	m_LoadedFromProfile = PROFILE_SLOT_INVALID;
	m_fMusicSampleStartSeconds = -1;
	m_fMusicSampleLengthSeconds = DEFAULT_MUSIC_SAMPLE_LENGTH;
	m_fMusicLengthSeconds = 0;
	m_iListSortPosition = -1;
	m_SelectionDisplay = SHOW_ALWAYS;
	m_SortByBPMRange = NULL;
	m_bIsSymLink = false;

	m_bHasIntro = false;
	m_bHasMusic = false;
	m_bHasBanner = false;
	m_bHasDisc = false;
	m_bHasBackground = false;
	m_bHasPreview = false;

	m_bIsPlayerSong = false;
	m_bMusicFailPlayerSong = false;

	m_bCreatingSongCache = false;
}

Song::~Song()
{
	FOREACH( Steps*, m_vpSteps, s )
		SAFE_DELETE( *s );
	m_vpSteps.clear();

	SAFE_DELETE( m_SortByBPMRange );

	// It's the responsibility of the owner of this Song to make sure
	// that all pointers to this Song and its Steps are invalidated.
}

// Reset to an empty song.
void Song::Reset()
{
	FOREACH( Steps*, m_vpSteps, s )
		SAFE_DELETE( *s );
	m_vpSteps.clear();
	FOREACH_StepsType( st )
		m_vpStepsByType[st].clear();

	Song empty;
	*this = empty;

	// It's the responsibility of the owner of this Song to make sure
	// that all pointers to this Song and its Steps are invalidated.
}


void Song::AddBGChange( BGChange seg )
{
	m_BGChanges.push_back( seg );
	SortBGChangesArray( m_BGChanges );
}

void Song::AddForegroundChange( BGChange seg )
{
	m_FGChanges.push_back( seg );
	SortBGChangesArray( m_FGChanges );
}

void Song::AddLyric( Lyric seg )
{
	m_Lyrics.push_back( seg );
}

CString Song::GetBackgroundAtBeat( float fBeat ) const
{
	unsigned i;
	for( i=0; i<m_BGChanges.size()-1; i++ )
		if( m_BGChanges[i+1].m_fBeat > fBeat )
			break;
	return m_BGChanges[i].m_sBGName;
}


CString Song::GetCacheFilePath() const
{
	return ssprintf( CACHE_DIR "Songs/%u", GetHashForString(m_sSongDir) );
}

CString Song::GetBackupFilePath( const CString& extension ) const
{
	tm now = GetLocalTime();
	return ssprintf( "%.4d-%.2d-%.2d %.2d-%.2d-%.2d%s", now.tm_year + 1900, now.tm_mon + 1, now.tm_mday, now.tm_hour + 1, now.tm_min, now.tm_sec, extension.c_str() );
}

// Get a path to the SM containing data for this song.  It might be a cache file.
const CString &Song::GetSongFilePath() const
{
	ASSERT ( m_sSongFileName.length() != 0 );
	return m_sSongFileName;
}

NotesLoader *Song::MakeLoader( CString sDir ) const
{
	NotesLoader *ret;

	// Actually, none of these have any persistant data, so we could optimize this, but since they
	// don't have any data, there's no real point...
	ret = new SMLoader;
	if(ret->Loadable( sDir )) return ret;
	delete ret;

	ret = new DWILoader;
	if(ret->Loadable( sDir )) return ret;
	delete ret;

	// Player songs can only be SM or DWI files!
	if( m_bIsPlayerSong )
		return NULL;

	ret = new BMSLoader;
	if(ret->Loadable( sDir )) return ret;
	delete ret;

	ret = new KSFLoader;
	if(ret->Loadable( sDir )) return ret;
	delete ret;

	ret = new UCSLoader;
	if(ret->Loadable( sDir )) return ret;
	delete ret;

	return NULL;
}

bool Song::DetectSongInfoFromPIUDir( bool bGetArtistAndTitle )
{
	// Copy TimingData from the first step
	if( m_vpSteps.size() < 1 )
		return false;
	m_Timing = m_vpSteps[0]->m_Timing;

	// Default PIU Intro = 7 seconds.
	m_fMusicSampleLengthSeconds = 7.f;

	CStringArray asFiles, asFiles2;

	// Artist - Title
	if( bGetArtistAndTitle )
	{
		split(m_sSongFolderName, " - ", asFiles);
		switch( asFiles.size() )
		{
		default:
			m_sArtist = asFiles[0];
			m_sMainTitle = "";
			for( size_t t = 1; t < asFiles.size(); )
			{
				m_sMainTitle += asFiles[t];
				if( ++t < asFiles.size() )
					m_sMainTitle += " - ";
			}
			break;

		case 2:
			m_sArtist = asFiles[0];
			m_sMainTitle = asFiles[1];
			break;

		case 1:
			m_sMainTitle = asFiles[0];
			break;

		case 0:
			;
		}
	}

	// Intro
	asFiles2.clear();
	GetAudioDirListing( GetSongDir() + "*intro", asFiles2 );
	if( !asFiles2.empty() )
		m_sIntroFile = asFiles2[0];
	
	// Song
	asFiles.clear();
	GetAudioDirListing( GetSongDir() + "*", asFiles );
	if( asFiles2.empty() && !asFiles.empty() )
		m_sMusicFile = asFiles[0];
	else if( asFiles.size() > asFiles2.size() )
	{
		CStringArray asDifference;
		sort(asFiles.begin(), asFiles.end());
		sort(asFiles2.begin(), asFiles2.end());
		set_difference(asFiles.begin(), asFiles.end(), asFiles2.begin(), asFiles2.end(), back_inserter(asDifference));

		if( !asDifference.empty() )
			m_sMusicFile = asDifference[0];
		else
			return false;
	}
	else
		return false;
	
	// Banner
	asFiles.clear();
	GetPictureDirListing( GetSongDir() + "*bn", asFiles );
	GetPictureDirListing( GetSongDir() + "*banner", asFiles );
	if( !asFiles.empty() )
		m_sBannerFile = asFiles[0];
	
	// Disc
	asFiles.clear();
	GetPictureDirListing( GetSongDir() + "*disc", asFiles );
	if( !asFiles.empty() )
		m_sDiscFile = asFiles[0];
	
	// CDTitle
	asFiles2.clear();
	GetPictureDirListing( GetSongDir() + "*cdtitle", asFiles2 );
	if( !asFiles2.empty() )
		m_sCDTitleFile = asFiles2[0];

	// Background
	asFiles.clear();
	GetPictureDirListing( GetSongDir() + "*bg", asFiles );
	GetPictureDirListing( GetSongDir() + "*background", asFiles );
	if( !asFiles.empty() )
		m_sBackgroundFile = asFiles[0];
	else
	{
		asFiles.clear();
		GetPictureDirListing( GetSongDir() + "*title", asFiles );

		if( asFiles2.empty() && !asFiles.empty() )
			m_sBackgroundFile = asFiles[0];
		else if( asFiles.size() > asFiles2.size() )
		{
			CStringArray asDifference;
			sort(asFiles.begin(), asFiles.end());
			sort(asFiles2.begin(), asFiles2.end());
			set_difference(asFiles.begin(), asFiles.end(), asFiles2.begin(), asFiles2.end(), back_inserter(asDifference));

			if( !asDifference.empty() )
				m_sBackgroundFile = asDifference[0];
		}
	}

	// Preview
	asFiles2.clear();
	GetVideoDirListing( GetSongDir() + "*preview", asFiles2 );
	if( !asFiles2.empty() )
		m_sPreviewFile = asFiles2[0];
	else
	{
		asFiles.clear();
		GetPictureDirListing( GetSongDir() + "*preview", asFiles );
		if( !asFiles.empty() )
			m_sPreviewFile = asFiles[0];
	}

	// BGAnimation
	CString sBGAnimation;
	asFiles.clear();
	GetVideoDirListing( GetSongDir() + "*", asFiles );
	if( asFiles2.empty() && !asFiles.empty() )
		sBGAnimation = asFiles[0];
	else if( asFiles.size() > asFiles2.size() )
	{
		CStringArray asDifference;
		sort(asFiles.begin(), asFiles.end());
		sort(asFiles2.begin(), asFiles2.end());
		set_difference(asFiles.begin(), asFiles.end(), asFiles2.begin(), asFiles2.end(), back_inserter(asDifference));

		if( !asDifference.empty() )
			sBGAnimation = asDifference[0];
	}

	if( !sBGAnimation.empty() )
	{
		m_BGChanges.clear();
		m_BGChanges.push_back(
			BGChange(
				m_Timing.GetBeatFromElapsedTime(0),	// beat
				sBGAnimation,	// file
				1.f,	// rate
				false,	// fade
				false,	// rewind
				false	// loop
			)
		);
	}

	return true;
}


// Hack: This should be a parameter to TidyUpData, but I don't want to pull
// in <set> into Song.h, which is heavily used.
static set<istring> BlacklistedImages;

/*
 * If PREFSMAN->m_bFastLoad is true, always load from cache if possible. Don't read
 * the contents of sDir if we can avoid it.  That means we can't call HasMusic(),
 * HasBanner() or GetHashForDirectory().
 *
 * If true, check the directory hash and reload the song from scratch if it's changed.
 */
bool Song::LoadFromSongDir( CString sDir )
{
//	LOG->Trace( "Song::LoadFromSongDir(%s)", sDir.c_str() );
	ASSERT( sDir != "" );

	// make sure there is a trailing slash at the end of sDir
	if( sDir.Right(1) != "/" )
		sDir += "/";

	// save song dir
	m_sSongDir = sDir;

	// save group name
	CStringArray sDirectoryParts;
	split( m_sSongDir, "/", sDirectoryParts, false );
	ASSERT( sDirectoryParts.size() >= 4 ); /* Songs/Slow/Taps/ */
	m_sGroupName = sDirectoryParts[sDirectoryParts.size()-3];	// second from last item
	m_sSongFolderName = sDirectoryParts[sDirectoryParts.size()-2];	// first from last item
	ASSERT( m_sGroupName != "" );

	//
	// First look in the cache for this song (without loading NoteData)
	//
	unsigned uDirHash = SONGINDEX->GetCacheHash(m_sSongDir);
	bool bUseCache = true;

	if( !IsAFile(GetCacheFilePath()) )
	{
		// If we're only loading cache files, then skip this non-existant one
		if( PREFSMAN->m_bFastLoadExistingCacheOnly )
			return false;
		else
			bUseCache = false;
	}
	if( !PREFSMAN->m_bFastLoad && GetHashForDirectory(m_sSongDir) != uDirHash )
		bUseCache = false; // this cache is out of date

	if( bUseCache )
	{
		LOG->Trace( "Loading '%s' from cache file '%s'.", m_sSongDir.c_str(), GetCacheFilePath().c_str() );
		SMLoader ld;
		ld.LoadFromSMFile( GetCacheFilePath(), *this, true );
		ld.TidyUpData( *this, true );
	}
	else
	{
		// No cache file exists, so we're creating one
		m_bCreatingSongCache = true;

		//
		// There was no entry in the cache for this song, or it was out of date.
		// Let's load it from a file, then write a cache entry.
		//
		NotesLoader *ld = MakeLoader( sDir );
		if(!ld)
		{
			LOG->Warn( "Couldn't find any SM, DWI, BMS, KSF OR UCS files in '%s'.  This is not a valid song directory.", sDir.c_str() );
			return false;
		}

		bool success = ld->LoadFromDir( sDir, *this );
		BlacklistedImages = ld->GetBlacklistedImages();

		if(!success)
		{
			delete ld;
			return false;
		}

		TidyUpData();
		ld->TidyUpData( *this, false );

		delete ld;

		// sort steps before saving cache
		StepsUtil::SortNotesArrayByDifficulty( m_vpSteps );
		FOREACH_StepsType(st)
			StepsUtil::SortNotesArrayByDifficulty( m_vpStepsByType[st] );

		// save a cache file so we don't have to parse it all over again next time
		SaveToCacheFile();

		m_bCreatingSongCache = false;
	}

	for( unsigned i=0; i<m_vpSteps.size(); i++ )
	{
		m_vpSteps[i]->SetFile( GetCacheFilePath() );

		/* Compress all Steps.  During initial caching, this will remove cached NoteData;
		 * during cached loads, this will just remove cached SMData. */
		m_vpSteps[i]->Compress();
	}

	// Load the cached banners, if it's not loaded already.
	if( m_bHasBanner )
		BANNERCACHE->LoadBanner( GetBannerPath() );

	if( m_bHasDisc )
		BANNERCACHE->LoadBanner( GetDiscPath() );

	// Load the cached backgrounds, if it's not loaded already.
	if( m_bHasBackground )
		BACKGROUNDCACHE->LoadBackground( GetBackgroundPath() );

	if( m_bHasPreview )
		BACKGROUNDCACHE->LoadBackground( GetPreviewPath() );

	// Add AutoGen pointers.  (These aren't cached.)
	AddAutoGenNotes();

	if( !m_bHasMusic )
		return false;	// don't load this song
	else
		return true;	// do load this song
}

bool Song::LoadFromPlayerSongDir( CString sDir, CString sGroupName )
{
	ASSERT( sDir != "" );

	// Make sure there is a trailing slash at the end of sDir
	if( sDir.Right(1) != "/" )
		sDir += "/";

	// This is a song loaded by a player. Mark it as such.
	m_bIsPlayerSong = true;

	// Save song dir
	m_sSongDir = sDir;

	// Save group name
	m_sGroupName = sGroupName;
	ASSERT( m_sGroupName != "" );

	// We should not have cache files for these songs, so don't bother trying to load.
	// Just write new data.
	NotesLoader *ld = MakeLoader( sDir );

	if(!ld)
	{
		LOG->Warn( "Couldn't find any SM files in '%s'.  This is not a valid song directory.", sDir.c_str() );
		return false;
	}

	bool success = ld->LoadFromDir( sDir, *this );
	BlacklistedImages = ld->GetBlacklistedImages();

	if(	!success )
	{
		delete ld;
		return false;
	}

	TidyUpData();
	ld->TidyUpData( *this, false );

	// Check to make sure that the song itself isn't over the time limit
	// If it is, abort loading this song and warn about it
	if( m_fMusicLengthSeconds > PREFSMAN->m_fPlayerSongsLengthLimitSeconds || m_bMusicFailPlayerSong )
	{
		LOG->Warn("Song in %s has a length longer than allowed for player songs; song was not loaded.", sDir.c_str() );
		success = false;
	}

	// Also check time from FIRSTBEAT to LASTBEAT
	// This is to stop peopsle from using the OGG metadata hack to dodge the time limit
	// If the machine owner wants you to play long songs, then they would have set the limit higher. =)
	float fEndSeconds = m_Timing.GetElapsedTimeFromBeat( m_Timing.m_fLastBeat );

	if( fEndSeconds > PREFSMAN->m_fPlayerSongsLengthLimitSeconds )
	{
		LOG->Warn("Steps in %s take longer to play through than allowed for player songs; song was not loaded.", sDir.c_str() );
		success = false;
	}

	if(	!success )
	{
		delete ld;
		return false;
	}

	delete ld;

	// If we're not allowing song components, kill them here
	if( !PREFSMAN->m_bPlayerSongsAllowBanners )
		m_sBannerFile = "";

	if( !PREFSMAN->m_bPlayerSongsAllowDiscs )
		m_sDiscFile = "";

	if( !PREFSMAN->m_bPlayerSongsAllowBackgrounds )
		m_sBackgroundFile = "";

	if( !PREFSMAN->m_bPlayerSongsAllowPreviews )
		m_sPreviewFile = "";

	if( !PREFSMAN->m_bPlayerSongsAllowCDTitles )
		m_sCDTitleFile = "";

	if( !PREFSMAN->m_bPlayerSongsAllowBGChanges )
		m_BGChanges.clear();

	if( !PREFSMAN->m_bPlayerSongsAllowLyrics )
		m_sLyricsFile = "";

	// sort steps before saving cache
	StepsUtil::SortNotesArrayByDifficulty( m_vpSteps );
	FOREACH_StepsType(st)
		StepsUtil::SortNotesArrayByDifficulty( m_vpStepsByType[st] );

	// Save a cache file so we don't have to parse it all over again next time
	SaveToCacheFile();

	for( unsigned i=0; i<m_vpSteps.size(); i++ )
	{
		//m_vpSteps[i]->SetFile( GetPlayerCacheFilePath() );
		m_vpSteps[i]->SetFile( GetCacheFilePath() );

		// Compress all Steps.  During initial caching, this will remove cached NoteData;
		// during cached loads, this will just remove cached SMData.
		m_vpSteps[i]->Compress();
	}

	// Load the cached banners, if it's not loaded already.
	// If we're not allowing banners, we don't need to check here; it's taken care of above.
	if( m_bHasBanner )
		BANNERCACHE->LoadBanner( GetBannerPath() );

	// Load the cached discs, if it's not loaded already.
	// If we're not allowing discs, we don't need to check here; it's taken care of above.
	if( m_bHasDisc )
		BANNERCACHE->LoadBanner( GetDiscPath() );

	// Load the cached backgrounds, if it's not loaded already.
	// If we're not allowing backgrounds, we don't need to check here; it's taken care of above.
	if( m_bHasBackground )
		BACKGROUNDCACHE->LoadBackground( GetBackgroundPath() );

	// Load the cached previews, if it's not loaded already.
	// If we're not allowing previews, we don't need to check here; it's taken care of above.
	if( m_bHasPreview )
		BACKGROUNDCACHE->LoadBackground( GetPreviewPath() );

	// Add AutoGen pointers.  (These aren't cached)
	AddAutoGenNotes();

	if( !m_bHasMusic )
		return false;	// don't load this song
	else
		return true;	// do load this song
}

bool Song::IsPlayerSong()
{
	return m_bIsPlayerSong;
}

static void RemoveInitialWhitespace( CString& s )
{
	size_t i = s.find_first_not_of(" \t\r\n");
	if( i > 0 && i != s.npos )
		s.erase( 0, i );
}

// This is called within TidyUpData, before autogen notes are added.
void Song::DeleteDuplicateSteps( vector<Steps*> &vSteps )
{
	/* vSteps have the same StepsType and Difficulty.  Delete them if they have the
	 * same m_sDescription, m_iMeter and SMNoteData. */
	CHECKPOINT;
	for( unsigned i=0; i<vSteps.size(); i++ )
	{
		CHECKPOINT;
		const Steps *s1 = vSteps[i];

		for( unsigned j=i+1; j<vSteps.size(); j++ )
		{
			CHECKPOINT;
			const Steps *s2 = vSteps[j];

			if( s1->GetDescription() != s2->GetDescription() )
				continue;
			if( s1->GetMeter() != s2->GetMeter() )
				continue;
			if( s1->GetRadarValues()[RADAR_NUM_TAPS_AND_HOLDS_AND_ROLLS] != s2->GetRadarValues()[RADAR_NUM_TAPS_AND_HOLDS_AND_ROLLS] )\
				continue;

			/* Compare, ignoring whitespace. */
			CString sSMNoteData1;
			s1->GetSMNoteData(sSMNoteData1);
			RemoveInitialWhitespace(sSMNoteData1);

			CString sSMNoteData2;
			s2->GetSMNoteData(sSMNoteData2);
			RemoveInitialWhitespace(sSMNoteData2);

			if( sSMNoteData1 != sSMNoteData2 )
				continue;

			LOG->Trace("Removed %p duplicate steps in song \"%s\" with description \"%s\" and meter \"%d\"",
				s2, this->GetSongDir().c_str(), s1->GetDescription().c_str(), s1->GetMeter() );

			/* Don't use RemoveSteps; autogen notes havn't yet been created and it'll
			 * create them. */
			FOREACH_StepsType( st )
			{
				for( int k=this->m_vpStepsByType[st].size()-1; k>=0; k-- )
				{
					if( this->m_vpStepsByType[st][k] == s2 )
					{
						// delete this->m_vpStepsByType[k]; // delete below
						this->m_vpStepsByType[st].erase( this->m_vpStepsByType[st].begin()+k );
						break;
					}
				}
			}

			for( int k=this->m_vpSteps.size()-1; k>=0; k-- )
			{
				if( this->m_vpSteps[k] == s2 )
				{
					delete this->m_vpSteps[k];
					this->m_vpSteps.erase( this->m_vpSteps.begin()+k );
					break;
				}
			}

			vSteps.erase(vSteps.begin()+j);
			--j;
		}
	}
}

/* Make any duplicate difficulties edits.  (Note that BMS files do a first pass
 * on this; see BMSLoader::SlideDuplicateDifficulties.) */
void Song::AdjustDuplicateSteps()
{
	for( int i=0; i<NUM_STEPS_TYPES; i++ )
	{
		StepsType st = (StepsType)i;

		for( unsigned j=0; j<=DIFFICULTY_CHALLENGE; j++ ) // not DIFFICULTY_EDIT
		{
			Difficulty dc = (Difficulty)j;

			vector<Steps*> vSteps;
			this->GetSteps( vSteps, st, dc );
			
			if( vSteps.size() > 0 )
			{
				/* Delete steps that are completely identical.  This happened due to a
				 * bug in an earlier version. */
				DeleteDuplicateSteps( vSteps );

				//CHECKPOINT;
				StepsUtil::SortNotesArrayByDifficulty( vSteps );
				//CHECKPOINT;

				int iMeter, iPreviousMeter = vSteps[0]->GetMeter();
				for( unsigned k=1; k<vSteps.size(); k++ )
				{
					iMeter = vSteps[k]->GetMeter();
					if( iMeter == iPreviousMeter )
					{
						vSteps[k]->SetDifficulty( DIFFICULTY_EDIT );
						if( vSteps[k]->GetDescription() == "" )
						{
							// "Hard Edit"
							CString EditName = ssprintf("%s %d Edit", Capitalize( DifficultyToString(dc) ).c_str(), iMeter);
							vSteps[k]->SetDescription(EditName);
						}
					}
					iPreviousMeter = iMeter;
				}
			}
		}

		/* XXX: Don't allow edits to have descriptions that look like regular difficulties.
		 * These are confusing, and they're ambiguous when passed to GetStepsByID. */
	}
}

/* Fix up song paths.  If there's a leading "./", be sure to keep it: it's
 * a signal that the path is from the root directory, not the song directory.
 * Other than a leading "./", song paths must never contain "." or "..". */
void FixupPath( CString &path, const CString &sSongPath )
{
	/* Replace backslashes with slashes in all paths. */
	FixSlashesInPlace( path );

	/* Many imported files contain erroneous whitespace before or after
	 * filenames.  Paths usually don't actually start or end with spaces,
	 * so let's just remove it. */
	TrimLeft( path );
	TrimRight( path );
}

// Songs in BlacklistImages will never be autodetected as song images.
void Song::TidyUpData()
{
	// We need to do this before calling any of HasMusic, HasHasCDTitle, etc.
	if( m_sSongDir.Left(3) == "../" )	// no ".."
	{
		RageException::ThrowNonfatal("Could not resolve the following path because \"..\" is not allowed:\n\n%s", m_sSongDir.c_str());
		return;
	}

	FixupPath( m_sSongDir,			"" );
	FixupPath( m_sIntroFile,		m_sSongDir );
	FixupPath( m_sMusicFile,		m_sSongDir );
	FixupPath( m_sBannerFile,		m_sSongDir );
	FixupPath( m_sDiscFile,			m_sSongDir );
	FixupPath( m_sBackgroundFile,	m_sSongDir );
	FixupPath( m_sCDTitleFile,		m_sSongDir );
	FixupPath( m_sPreviewFile,		m_sSongDir );
	FixupPath( m_sLyricsFile,		m_sSongDir );

	if( !HasMusic() )
	{
		CStringArray arrayPossibleMusic;
		GetAudioDirListing( m_sSongDir + "*", arrayPossibleMusic );

		if( !arrayPossibleMusic.empty() )
		{
			int idx = 0;
			/* If the first song is "intro", and we have more than one available,
			 * don't use it--it's probably a KSF intro music file, which we don't support. */
			if( arrayPossibleMusic.size() > 1 &&
				!arrayPossibleMusic[0].Left(5).CompareNoCase("intro") )
				++idx;

			// we found a match
			m_sMusicFile = arrayPossibleMusic[idx];
		}
	}

	// This must be done before radar calculation.
	if( HasMusic() )
	{
		CString error;
		SoundReader *Sample = SoundReader_FileReader::OpenFile( GetMusicPath(), error );
		if( Sample == NULL )
		{
			LOG->Warn( "Error opening sound \"%s\": %s", GetMusicPath().c_str(), error.c_str() );

			/* Don't use this file. */
			m_sMusicFile = "";
			m_bMusicFailPlayerSong = true;
		}
		else
		{
			m_fMusicLengthSeconds = Sample->GetLength() / 1000.0f;
			delete Sample;

			if( m_fMusicLengthSeconds < 0 )
			{
				/* It failed; bad file or something.  It's already logged a warning. */
				m_fMusicLengthSeconds = 100; // guess
				m_bMusicFailPlayerSong = true;
			}
			else if( m_fMusicLengthSeconds == 0 )
			{
				LOG->Warn( "File %s is empty?", GetMusicPath().c_str() );
				m_bMusicFailPlayerSong = true;
			}
		}
	}
	else	// ! HasMusic()
	{
		m_fMusicLengthSeconds = 100;		// guess
		m_bMusicFailPlayerSong = true;
		LOG->Warn("Song \"%s\" has no music file; guessing at %f seconds", this->GetSongDir().c_str(), m_fMusicLengthSeconds);
	}

	if(m_fMusicLengthSeconds < 0)
	{
		LOG->Warn( "File %s has negative length? (%f)", GetMusicPath().c_str(), m_fMusicLengthSeconds );
		m_fMusicLengthSeconds = 0;
		m_bMusicFailPlayerSong = true;
	}

	/* Generate these before we autogen notes, so the new notes can inherit
	 * their source's values. */
	ReCalculateRadarValuesAndLastBeat();

	TrimLeft( m_sMainTitle );
	TrimRight( m_sMainTitle );
	TrimLeft( m_sSubTitle );
	TrimRight( m_sSubTitle );
	TrimLeft( m_sArtist );
	TrimRight( m_sArtist );

	/* Fall back on the song directory name. */
	if( m_sMainTitle == "" )
		NotesLoader::GetMainAndSubTitlesFromFullTitle( Basename(this->GetSongDir()), m_sMainTitle, m_sSubTitle );

	if( m_sArtist == "" )
		m_sArtist = "Unknown Artist";

	TranslateTitles();

	// Make sure the first BPM segment starts at beat 0.
	if( m_Timing.m_BPMSegments[0].m_fBeat != 0 )
		m_Timing.m_BPMSegments[0].m_fBeat = 0;

	/* Only automatically set the sample time if there was no sample length
	 * (m_fMusicSampleStartSeconds == -1). */
	/* We don't want to test if m_fMusicSampleStartSeconds == 0, since some
	 * people really do want the sample to start at the very beginning of the song. */

	/* Having a sample start of 0 sounds terrible for most songs because because
	 * of the silence at the beginning.  Many of my files have a 0 for the
	 * sample start that was not manually set, and I assume other people have the same.
	 * If there are complaints atou manually-set sample start at 0 is being ignored,
	 * then change this back.  -Chris */

	/* Some DWIs have lengths in ms when they meant seconds, eg. #SAMPLELENGTH:10;.
	 * If the sample length is way too short, change it. */
	if( m_fMusicSampleLengthSeconds < 3 || m_fMusicSampleLengthSeconds > 30 )
		SetDefaultMusicSampleLength();

	// Changing this back in 3.9 Plus, at the request of k//eternal. -Mike
	//	if( m_fMusicSampleStartSeconds == -1 ||
	//	m_fMusicSampleStartSeconds == 0 ||
	//	m_fMusicSampleStartSeconds+m_fMusicSampleLengthSeconds > this->m_fMusicLengthSeconds )
		if( m_fMusicSampleStartSeconds == -1 ||
		m_fMusicSampleStartSeconds+m_fMusicSampleLengthSeconds > this->m_fMusicLengthSeconds )
		SetDefaultMusicSampleStart();

	//
	// Here's the problem:  We have a directory full of images.  We want to determine which
	// image is the banner, which is the background, and which is the CDTitle.
	//

	CHECKPOINT_M( "Looking for images..." );

	//
	// First, check the file name for hints.
	//
	if( !HasBanner() )
	{
		/* If a nonexistant banner file is specified, and we can't find a replacement,
		 * don't wipe out the old value. */
		//m_sBannerFile = "";

		// find an image with "banner" in the file name
		CStringArray arrayPossibleBanners;
		GetPictureDirListing( m_sSongDir + "*banner*", arrayPossibleBanners );

		/* Some people do things differently for the sake of being different.  Don't
		 * match eg. abnormal, numbness. */
		GetPictureDirListing( m_sSongDir + "*bn*", arrayPossibleBanners );

		if( !arrayPossibleBanners.empty() )
			m_sBannerFile = arrayPossibleBanners[0];
	}

	if( !HasDisc() )
	{
		/* If a nonexistant disc file is specified, and we can't find a replacement,
		 * don't wipe out the old value. */
		//m_sDiscFile = "";

		// find an image with "disc" in the file name
		CStringArray arrayPossibleDiscs;
		GetPictureDirListing( m_sSongDir + "*disc*", arrayPossibleDiscs );

		if( !arrayPossibleDiscs.empty() )
			m_sDiscFile = arrayPossibleDiscs[0];
	}

	if( !HasBackground() )
	{
		/* If a nonexistant background file is specified, and we can't find a replacement,
		 * don't wipe out the old value. */
		//m_sBackgroundFile = "";

		// find an image with "bg" or "background" in the file name
		CStringArray arrayPossibleBGs;
		GetPictureDirListing( m_sSongDir + "*bg*", arrayPossibleBGs );
		GetPictureDirListing( m_sSongDir + "*background*", arrayPossibleBGs );
		GetPictureDirListing( m_sSongDir + "title*", arrayPossibleBGs );

		if( !arrayPossibleBGs.empty() )
			m_sBackgroundFile = arrayPossibleBGs[0];
	}

	if( !HasCDTitle() )
	{
		// find an image with "cdtitle" in the file name
		CStringArray arrayPossibleCDTitles;
		GetPictureDirListing( m_sSongDir + "*cdtitle*", arrayPossibleCDTitles );

		if( !arrayPossibleCDTitles.empty() )
			m_sCDTitleFile = arrayPossibleCDTitles[0];
	}

	// Aldo_MX: People from PIUSM started using CDTitles as Previews, then many PIU communities copied that.
	// Consider videos and big cdtitles as previews and wipe out the old value.
	if( HasCDTitle() )
	{
		if( RageTextureID::GetTypeFromFilename(m_sCDTitleFile) == RageTextureID::TYPE_VIDEO )
		{
			if( !HasPreview() )
				m_sPreviewFile = m_sCDTitleFile;

			m_sCDTitleFile = "";
		}
		else
		{
			CString error;
			RageSurface *img = RageSurfaceUtils::LoadFile( m_sCDTitleFile, error, true );
			if( !img || img->w > 128 && img->h > 128 )
			{
				if( img && !HasPreview() )
					m_sPreviewFile = m_sCDTitleFile;

				m_sCDTitleFile = "";
			}
		}
	}

	if( !HasPreview() )
	{
		/* If a nonexistant preview file is specified, and we can't find a replacement,
		 * don't wipe out the old value. */
		//m_sPreviewFile = "";

		// find an image with "preview" in the file name
		CStringArray arrayPossiblePreviews;
		GetVideoDirListing( m_sSongDir + "*preview*", arrayPossiblePreviews );
		GetVideoDirListing( m_sSongDir + "*cdtitle*", arrayPossiblePreviews );
		GetPictureDirListing( m_sSongDir + "*preview*", arrayPossiblePreviews );

		if( !arrayPossiblePreviews.empty() )
			m_sPreviewFile = arrayPossiblePreviews[0];
	}

	if( !HasLyrics() )
	{
		// Check if there is a lyric file in here
		CStringArray arrayLyricFiles;
		GetDirListing(m_sSongDir + CString("*.lrc"), arrayLyricFiles );
		if(	!arrayLyricFiles.empty() )
			m_sLyricsFile = arrayLyricFiles[0];
	}

	//
	// Now, For the images we still haven't found, look at the image dimensions of the remaining unclassified images.
	//
	CStringArray arrayImages;
	GetPictureDirListing( m_sSongDir + "*", arrayImages );

	int i;
	for( i=0; i<(int)arrayImages.size(); i++ )	// foreach image
	{
		if( HasBackground() && HasBanner() && HasDisc() && HasCDTitle() )
			break; // done

		// ignore DWI "-char" graphics
		if( BlacklistedImages.find( arrayImages[i].c_str() ) != BlacklistedImages.end() )
			continue;	// skip

		// Skip any image that we've already classified
		if( HasBackground() && stricmp(m_sBackgroundFile, arrayImages[i])==0 )
			continue;	// skip

		if( HasBanner() && stricmp(m_sBannerFile, arrayImages[i])==0 )
			continue;	// skip

		if( HasDisc() && stricmp(m_sDiscFile, arrayImages[i])==0 )
			continue;	// skip

		if( HasCDTitle() && stricmp(m_sCDTitleFile, arrayImages[i])==0 )
			continue;	// skip

		CString sPath = m_sSongDir + arrayImages[i];

		/* We only care about the dimensions. */
		CString error;
		RageSurface *img = RageSurfaceUtils::LoadFile( sPath, error, true );
		if( !img )
		{
			LOG->Trace( "Couldn't load '%s': %s", sPath.c_str(), error.c_str() );
			continue;
		}

		const int width = img->w;
		const int height = img->h;
		delete img;

		if( !HasBackground() && width >= 320 && height >= 240 )
		{
			m_sBackgroundFile = arrayImages[i];
			continue;
		}

		if( !HasBanner() && Sprite::IsDiagonalBanner(width, height) )
		{
			m_sBannerFile = arrayImages[i];
			continue;
		}

		if( !HasDisc() && 68<=width && width<=640 && 48<=height && height<=480 )
		{
			m_sDiscFile = arrayImages[i];
			continue;
		}

		if( !HasBanner() && 100<=width && width<=320 && 50<=height && height<=240 )
		{
			m_sBannerFile = arrayImages[i];
			continue;
		}

		/* Some songs have overlarge banners.  Check if the ratio is reasonable (over
		 * 2:1; usually over 3:1), and large (not a cdtitle). */
		if( !HasBanner() && width > 200 && float(width) / height > 2.0f )
		{
			m_sBannerFile = arrayImages[i];
			continue;
		}

		if( !HasCDTitle()  &&  width<=128  &&  height<=128 )
		{
			m_sCDTitleFile = arrayImages[i];
			continue;
		}
	}

	if( !HasDisc() )
	{
		if( HasBackground() )
			m_sDiscFile = m_sBackgroundFile;
		else if( HasBanner() )
			m_sDiscFile = m_sBannerFile;
	}

	if( !HasPreview() )
	{
		if( HasBackground() )
			m_sPreviewFile = m_sBackgroundFile;
		else if( HasDisc() )
			m_sPreviewFile = m_sDiscFile;
	}

	if( !HasBanner() )
	{
		if( HasDisc() )
			m_sBannerFile = m_sDiscFile;
		else if( HasBackground() )
			m_sBannerFile = m_sBackgroundFile;
	}

	/* These will be written to cache, for Song::LoadFromSongDir to use later. */
	m_bHasIntro = HasIntro();
	m_bHasMusic = HasMusic();
	m_bHasBanner = HasBanner();
	m_bHasDisc = HasDisc();
	m_bHasBackground = HasBackground();

	if( HasBanner() && (!IsPlayerSong() || (IsPlayerSong() && PREFSMAN->m_bPlayerSongsAllowBanners)) )
		BANNERCACHE->CacheBanner( GetBannerPath() );

	if( HasDisc() && (!IsPlayerSong() || (IsPlayerSong() && PREFSMAN->m_bPlayerSongsAllowDiscs)) )
		BANNERCACHE->CacheBanner( GetDiscPath() );

	if( HasBackground() && (!IsPlayerSong() || (IsPlayerSong() && PREFSMAN->m_bPlayerSongsAllowBackgrounds)) )
		BACKGROUNDCACHE->CacheBackground( GetBackgroundPath() );

	if( HasPreview() && (!IsPlayerSong() || (IsPlayerSong() && PREFSMAN->m_bPlayerSongsAllowPreviews)) )
		BACKGROUNDCACHE->CacheBackground( GetPreviewPath() );

	// If no BGChanges are specified and there are movies in the song directory, then assume
	// they are DWI style where the movie begins at beat 0.
	if( !HasBGChanges() )
	{
		CStringArray arrayPossiblePreviews;
		GetVideoDirListing( m_sSongDir + "*preview*", arrayPossiblePreviews );

		CStringArray arrayPossibleMovies;
		GetVideoDirListing( m_sSongDir + "*", arrayPossibleMovies );

		/* Use this->GetBeatFromElapsedTime(0) instead of 0 to start when the
		 * music starts. */
		if( !arrayPossibleMovies.empty() && arrayPossibleMovies.size() > arrayPossiblePreviews.size() )
		{
			int iBG = 0;
			for( unsigned bg=0; bg<arrayPossibleMovies.size(); bg++ )
			{
				bool bContinue = false;
				for( unsigned pv=0; pv<arrayPossiblePreviews.size(); pv++ )
				{
					if( arrayPossibleMovies[bg] == arrayPossiblePreviews[pv] )
					{
						bContinue = true;
						break;
					}
				}
				if( bContinue )
					continue;

				iBG = bg;
				break;
			}
			this->AddBGChange( BGChange(this->m_Timing.GetBeatFromElapsedTime(0),arrayPossibleMovies[iBG],1.f,false,false,false) );
		}
	}

	/* Don't allow multiple Steps of the same StepsType and Difficulty (except for edits).
	 * We should be able to use difficulty names as unique identifiers for steps. */
	AdjustDuplicateSteps();

	{
		/* Generated filename; this doesn't always point to a loadable file,
		 * but instead points to the file we should write changed files to,
		 * and will always be an .SM.
		 *
		 * This is a little tricky.  We can't always use the song title directly,
		 * since it might contain characters we can't store in filenames.  Two
		 * easy options: we could manually filter out invalid characters, or we
		 * could use the name of the directory, which is always a valid filename
		 * and should always be the same as the song.  The former might not catch
		 * everything--filename restrictions are platform-specific; we might even
		 * be on an 8.3 filesystem, so let's do the latter.
		 *
		 * We can't rely on searching for other data filenames; it works for DWIs,
		 * but not KSFs and BMSs.
		 *
		 * So, let's do this (by priority):
		 * 1. If there's an .SM file, use that filename.  No reason to use anything
		 *    else; it's the filename in use.
		 * 2. If there's a .DWI, use it with a changed extension.
		 * 3. Otherwise, use the name of the directory, since it's definitely a valid
		 *    filename, and should always be the title of the song (unlike KSFs).
		 */
		m_sSongFileName = m_sSongDir;

		CStringArray asFileNames;
		GetSMADirListing( m_sSongDir + "*", asFileNames );

		if( !asFileNames.empty() )
			m_sSongFileName += asFileNames[0];
		else
		{
			GetStepDirListing( m_sSongDir + "*", asFileNames, false, false );

			if( !asFileNames.empty() )
				m_sSongFileName += SetExtension( asFileNames[0], "sma" );
			else
				m_sSongFileName += "steps.sma";
		}
	}
}

void Song::TranslateTitles()
{
	static TitleSubst tsub("songs");

	TitleFields title;
	title.LoadFromStrings( m_sMainTitle, m_sSubTitle, m_sArtist, m_sMainTitleTranslit, m_sSubTitleTranslit, m_sArtistTranslit );
	tsub.Subst( title );
	title.SaveToStrings( m_sMainTitle, m_sSubTitle, m_sArtist, m_sMainTitleTranslit, m_sSubTitleTranslit, m_sArtistTranslit );
}

void Song::ReCalculateRadarValuesAndLastBeat()
{
	float fFirstSecond = FLT_MAX;
	float fLastSecond = -FLT_MAX;

	for( unsigned i=0; i<m_vpSteps.size(); i++ )
	{
		Steps* pSteps = m_vpSteps[i];

		/* If it's autogen, radar vals and first/last beat will come from the parent. */
		if( pSteps->IsAutogen() )
			continue;

		//
		// calculate radar values
		//
		NoteData tempNoteData;
		pSteps->GetNoteData( &tempNoteData );

		RadarValues v;
		NoteDataUtil::GetRadarValues( tempNoteData, m_fMusicLengthSeconds, v );
		pSteps->SetRadarValues( v );

		/* Calculate first/last beat.
		 *
		 * Many songs have stray, empty song patterns.  Ignore them, so
		 * they don't force the first beat of the whole song to 0. */
		if( tempNoteData.GetLastRow() == 0 )
			continue;

		// Don't set first/last beat based on lights.  They often start very
		// early and end very late.
		if( pSteps->m_StepsType == STEPS_TYPE_LIGHTS_CABINET )
			continue;

		float fFirstStepSecond = pSteps->m_Timing.GetElapsedTimeFromBeat( tempNoteData.GetFirstBeat() );
		float fLastStepSecond = pSteps->m_Timing.GetElapsedTimeFromBeat( tempNoteData.GetLastBeat() );

		fFirstSecond = min( fFirstSecond, fFirstStepSecond );
		fLastSecond = max( fLastSecond, fLastStepSecond );
	}

	m_Timing.ReCalculateFirstAndLastBeat( fFirstSecond, fLastSecond, m_fMusicLengthSeconds );
}

void Song::GetSteps( vector<Steps*>& arrayAddTo, vector<StepsType>& arrayStepsType, Difficulty dc, int iMeterLow, int iMeterHigh, const CString &sDescription, bool bIncludeAutoGen, int Max ) const
{
	if( !Max )
		return;

	for( unsigned st=0; st<arrayStepsType.size(); ++st )
	{
		const vector<Steps*>& vpSteps = GetAllSteps(arrayStepsType[st]);

		for( unsigned i=0; i<vpSteps.size(); i++ )	// for each of the Song's Steps
		{
			Steps* pSteps = vpSteps[i];

			if( dc != DIFFICULTY_INVALID && dc != pSteps->GetDifficulty() )
				continue;
			if( iMeterLow != -1 && iMeterLow > pSteps->GetMeter() )
				continue;
			if( iMeterHigh != -1 && iMeterHigh < pSteps->GetMeter() )
				continue;
			if( sDescription.size() && sDescription != pSteps->GetDescription() )
				continue;
			if( !bIncludeAutoGen && pSteps->IsAutogen() )
				continue;

			// If the steps are locked, skip them unless we're writing the cache file
			if( !m_bCreatingSongCache )
			{
				if( UNLOCKMAN->StepsAreLocked( pSteps ) && !PREFSMAN->m_bEventIgnoreUnlock )
					continue;
			}

			arrayAddTo.push_back( pSteps );

			if( Max != -1 )
			{
				--Max;
				if( !Max )
					break;
			}
		}
	}
}

Steps* Song::GetStepsByDifficulty( StepsType st, Difficulty dc, bool bIncludeAutoGen ) const
{
	const vector<Steps*>& vpSteps = GetAllSteps(st);
	for( unsigned i=0; i<vpSteps.size(); i++ )	// for each of the Song's Steps
	{
		Steps* pSteps = vpSteps[i];

		if( dc != DIFFICULTY_INVALID && dc != pSteps->GetDifficulty() )
			continue;
		if( !bIncludeAutoGen && pSteps->IsAutogen() )
			continue;

		return pSteps;
	}

	return NULL;
}

Steps* Song::GetStepsByMeter( StepsType st, int iMeterLow, int iMeterHigh ) const
{
	const vector<Steps*>& vpSteps = GetAllSteps(st);
	for( unsigned i=0; i<vpSteps.size(); i++ )	// for each of the Song's Steps
	{
		Steps* pSteps = vpSteps[i];

		if( iMeterLow > pSteps->GetMeter() )
			continue;
		if( iMeterHigh < pSteps->GetMeter() )
			continue;

		return pSteps;
	}

	return NULL;
}

Steps* Song::GetStepsByDescription( StepsType st, CString sDescription ) const
{
	vector<Steps*> vNotes;
	GetSteps( vNotes, st, DIFFICULTY_INVALID, -1, -1, sDescription );
	if( vNotes.size() == 0 )
		return NULL;
	else
		return vNotes[0];
}


Steps* Song::GetClosestNotes( StepsType st, Difficulty dc ) const
{
	Difficulty newDC = dc;
	Steps* pSteps;
	pSteps = GetStepsByDifficulty( st, newDC );
	if( pSteps )
		return pSteps;
	newDC = (Difficulty)(dc-1);
	ENUM_CLAMP( newDC, Difficulty(0), Difficulty(NUM_DIFFICULTIES-1) );
	pSteps = GetStepsByDifficulty( st, newDC );
	if( pSteps )
		return pSteps;
	newDC = (Difficulty)(dc+1);
	ENUM_CLAMP( newDC, Difficulty(0), Difficulty(NUM_DIFFICULTIES-1) );
	pSteps = GetStepsByDifficulty( st, newDC );
	if( pSteps )
		return pSteps;
	newDC = (Difficulty)(dc-2);
	ENUM_CLAMP( newDC, Difficulty(0), Difficulty(NUM_DIFFICULTIES-1) );
	pSteps = GetStepsByDifficulty( st, newDC );
	if( pSteps )
		return pSteps;
	newDC = (Difficulty)(dc+2);
	ENUM_CLAMP( newDC, Difficulty(0), Difficulty(NUM_DIFFICULTIES-1) );
	pSteps = GetStepsByDifficulty( st, newDC );
	return pSteps;
}

/* Return whether the song is playable in the given style. */
bool Song::SongCompleteForStyle( const Style *st ) const
{
	return HasStepsType( st->m_StepsType );
}

bool Song::HasStepsType( StepsType st ) const
{
	vector<Steps*> add;
	GetSteps( add, st, DIFFICULTY_INVALID, -1, -1, "", true, 1 );
	return !add.empty();
}

bool Song::HasStepsTypeAndDifficulty( StepsType st, Difficulty dc ) const
{
	vector<Steps*> add;
	GetSteps( add, st, dc, -1, -1, "", true, 1 );
	return !add.empty();
}

void Song::TidyUpBeforeSave()
{
	TidyUpData();

	ReCalculateRadarValuesAndLastBeat();
	TranslateTitles();

	// sort steps before saving
	StepsUtil::SortNotesArrayByDifficulty( m_vpSteps );
	FOREACH_StepsType(st)
		StepsUtil::SortNotesArrayByDifficulty( m_vpStepsByType[st] );
}

void Song::Save() const
{
	LOG->Trace( "Song::SaveToSongFile()" );

	/* Save the new files.  These calls make backups on their own. */
	FILEMAN->FlushDirCache(m_sSongDir);
	SaveToSMFile( GetSongFilePath(), false );
	//SaveToDWIFile();

	//if( IsPlayerSong() )
	//	SaveToPlayerCacheFile();
	//else
		SaveToCacheFile();

	/* We've safely written our files and created backups.  Rename non-SM and non-DWI
	 * files to avoid confusion. */
	CStringArray arrayOldFileNames;
	GetStepDirListing( m_sSongDir + "*", arrayOldFileNames, false );

	for( unsigned i=0; i<arrayOldFileNames.size(); i++ )
	{
		const CString sOldPath = m_sSongDir + arrayOldFileNames[i];
		const CString sNewPath = m_sSongDir + "Backups/" + arrayOldFileNames[i];

		if( !FileCopy( sOldPath, sNewPath ) )
		{
			LOG->Warn( "Backup of \"%s\" failed", sOldPath.c_str() );
			/* Don't remove. */
		}
		else
			FILEMAN->Remove( sOldPath );
	}
}

void Song::SaveToSMFile( CString sFullPath, bool bSavingCache ) const
{
	LOG->Trace( "Song::SaveToSMFile('%s')", sFullPath.c_str() );

	/* If the file exists, make a backup. */
	if( !bSavingCache && IsAFile(sFullPath) )
	{
		CString sNewPath = ssprintf("%sBackups/%s", m_sSongDir.c_str(), GetBackupFilePath(".sma").c_str());

		if( !FileCopy( sFullPath, sNewPath ) )
			LOG->Warn( "Backup \"%s\" to \"%s\" failed", sFullPath.c_str(), sNewPath.c_str() );
		else
			FILEMAN->Remove( sFullPath );
	}

	NotesWriterSM wr;
	wr.Write(sFullPath, *this, bSavingCache);
}

void Song::AutoSave( CString& sGetFilename ) const
{
	sGetFilename = GetBackupFilePath(".sma");
	LOG->Trace( "Song::AutoSave('%s')", sGetFilename.c_str() );

	NotesWriterSM writer;
	writer.Write( ssprintf( "%sAutoSave/%s", m_sSongDir.c_str(), sGetFilename.c_str() ), *this, false );
}

void Song::SaveToCacheFile() const
{
	SONGINDEX->AddCacheIndex(m_sSongDir, GetHashForDirectory(m_sSongDir));
	SaveToSMFile( GetCacheFilePath(), true );
}

void Song::SaveToDWIFile() const
{
	const CString sFullPath = SetExtension( GetSongFilePath(), "dwi" );
	LOG->Trace( "Song::SaveToDWIFile(%s)", sFullPath.c_str() );

	/* If the file exists, make a backup. */
	if( IsAFile(sFullPath) )
	{
		tm now = GetLocalTime();
		CString sNewPath = ssprintf("%sBackups/%.4d-%.2d-%.2d% .2d%-.2d%-.2d.dwi", m_sSongDir.c_str(), now.tm_year+1900, now.tm_mon+1, now.tm_mday, now.tm_hour+1, now.tm_min, now.tm_sec);

		if( !FileCopy( sFullPath, sNewPath ) )
			LOG->Warn( "Backup \"%s\" to \"%s\" failed", sFullPath.c_str(), sNewPath.c_str() );
		else
			FILEMAN->Remove( sFullPath );
	}

	NotesWriterDWI wr;
	wr.Write(sFullPath, *this);
}

void Song::AddAutoGenNotes()
{
	int iAutoGenNotes = 0;
	bool HasNotes[NUM_STEPS_TYPES];
	memset( HasNotes, 0, sizeof(HasNotes) );
	for( unsigned i=0; i < m_vpSteps.size(); i++ ) // foreach Steps
	{
		if( m_vpSteps[i]->IsAutogen() )
			continue;

		StepsType st = m_vpSteps[i]->m_StepsType;
		HasNotes[st] = true;
	}

	FOREACH_StepsType( stMissing )
	{
		if( HasNotes[stMissing] )
			continue;

		/* If m_bAutogenSteps is disabled, only autogen lights. */
		/* XXX: disable lights autogen for now */
		if( !PREFSMAN->m_bAutogenSteps )
			continue;

		// missing Steps of this type
		int iNumTracksOfMissing = GAMEMAN->StepsTypeToNumTracks(stMissing);

		// look for closest match
		StepsType	stBestMatch = (StepsType)-1;
		int			iBestTrackDifference = 10000;	// inf

		FOREACH_StepsType( st )
		{
			if( !HasNotes[st] )
				continue;

			/* has (non-autogen) Steps of this type */
			const int iNumTracks = GAMEMAN->StepsTypeToNumTracks(st);
			const int iTrackDifference = abs(iNumTracks-iNumTracksOfMissing);
			if( iTrackDifference < iBestTrackDifference )
			{
				stBestMatch = st;
				iBestTrackDifference = iTrackDifference;
			}
		}

		if( stBestMatch != -1 )
		{
			iAutoGenNotes++;
			AutoGen( stMissing, stBestMatch );
		}
	}

	if( iAutoGenNotes > 0 )
	{
		// Sort Steps
		StepsUtil::SortNotesArrayByDifficulty( m_vpSteps );
		FOREACH_StepsType(st)
			StepsUtil::SortNotesArrayByDifficulty( m_vpStepsByType[st] );
	}
}

void Song::AutoGen( StepsType ntTo, StepsType ntFrom )
{
//	int iNumTracksOfTo = GAMEMAN->StepsTypeToNumTracks(ntTo);

	for( unsigned int j=0; j<m_vpSteps.size(); j++ )
	{
		const Steps* pOriginalNotes = m_vpSteps[j];
		if( pOriginalNotes->m_StepsType == ntFrom )
		{
			Steps* pNewNotes = new Steps;
			pNewNotes->AutogenFrom(pOriginalNotes, ntTo);
			this->AddSteps( pNewNotes );
		}
	}
}

void Song::RemoveAutoGenNotes()
{
	FOREACH_StepsType(st)
	{
		for( int j=m_vpStepsByType[st].size()-1; j>=0; j-- )
		{
			if( m_vpStepsByType[st][j]->IsAutogen() )
			{
//				delete m_vpSteps[j]; // delete below
				m_vpStepsByType[st].erase( m_vpStepsByType[st].begin()+j );
			}
		}
	}

	for( int j=m_vpSteps.size()-1; j>=0; j-- )
	{
		if( m_vpSteps[j]->IsAutogen() )
		{
			delete m_vpSteps[j];
			m_vpSteps.erase( m_vpSteps.begin()+j );
		}
	}
}

bool Song::IsEasy( StepsType st ) const
{
	const Steps* pHardNotes = GetStepsByDifficulty( st, DIFFICULTY_HARD );

	// HACK:  Looks bizarre to see the easy mark by Legend of MAX.
	if( pHardNotes && pHardNotes->GetMeter() > 9 )
		return false;

	/* The easy marker indicates which songs a beginner, having selected "beginner",
	 * can play and actually get a very easy song: if there are actual beginner
	 * steps, or if the light steps are 1-foot. */
	const Steps* pBeginnerNotes = GetStepsByDifficulty( st, DIFFICULTY_BEGINNER );
	if( pBeginnerNotes )
		return true;

	const Steps* pEasyNotes = GetStepsByDifficulty( st, DIFFICULTY_EASY );
	if( pEasyNotes && pEasyNotes->GetMeter() == 1 )
		return true;

	return false;
}

bool Song::IsTutorial() const
{
	// A Song is a tutorial song is it has only Beginner steps.
	FOREACH_CONST( Steps*, m_vpSteps, s )
	{
		if( (*s)->m_StepsType == STEPS_TYPE_LIGHTS_CABINET )
			continue;	// ignore
		if( (*s)->GetDifficulty() != DIFFICULTY_BEGINNER )
			return false;
	}

	return true;
}

bool Song::HasEdits( StepsType st ) const
{
	for( unsigned i=0; i<m_vpSteps.size(); i++ )
	{
		Steps* pSteps = m_vpSteps[i];
		if( pSteps->m_StepsType == st &&
			pSteps->GetDifficulty() == DIFFICULTY_EDIT )
		{
			return true;
		}
	}

	return false;
}

SelectionDisplay Song::GetDisplayed() const
{
	// If we're not using hidden songs...
	if( !PREFSMAN->m_bHiddenSongs )
		return SHOW_ALWAYS;

	// If we're in event mode and not ignoring hidden songs
	if( PREFSMAN->m_bEventMode && !PREFSMAN->m_bEventIgnoreSelectable )
		return SHOW_ALWAYS;

	// Aldo_MX: SelectionDisplay is really hackish...
	if( m_SelectionDisplay > SHOW_OMES )
	{
		bool bExtra1 = GAMESTATE->IsExtraStage(),
			bExtra2 = GAMESTATE->IsExtraStage2();

		if( bExtra1 || bExtra2 )
		{
			if( m_SelectionDisplay == SHOW_NO_ES )
				return SHOW_NEVER;
			else if( bExtra2 && m_SelectionDisplay == SHOW_NO_OMES )
				return SHOW_NEVER;
			else
				return SHOW_ALWAYS;
		}
	}

	return m_SelectionDisplay;
}
bool Song::NormallyDisplayed() const { return GetDisplayed() == SHOW_ALWAYS; }
bool Song::NeverDisplayed() const { return GetDisplayed() == SHOW_NEVER; }

bool Song::RouletteDisplayed() const
{
	if( IsTutorial() )
		return false;

	if( GetDisplayed() == SHOW_NEVER )
		return false;

	if( GAMESTATE->IsExtraStage() && GetDisplayed() == SHOW_OMES )
		return false;

	if( HideUntilXStages() )
	{
		int iCurStagesLeft = GAMESTATE->GetNumStagesLeft();
		int iShowWhen = ShowWhenXStages();

		if( iCurStagesLeft > iShowWhen )
			return false;
	}

	return true;
}

// Ugly, ain't it? ~ Mike "Archer"
bool Song::HideUntilXStages() const
{
	if( GetDisplayed() == SHOW_1 )
		return true;
	else if( GetDisplayed() == SHOW_2 )
		return true;
	else if( GetDisplayed() == SHOW_3 )
		return true;
	else if( GetDisplayed() == SHOW_4 )
		return true;
	else if( GetDisplayed() == SHOW_5 )
		return true;
	else if( GetDisplayed() == SHOW_6 )
		return true;
	else if( GetDisplayed() == SHOW_ES )
		return true;
	else if( GetDisplayed() == SHOW_OMES )
		return true;
	else
		return false;
}

// Also, still very ugly.
int Song::ShowWhenXStages() const
{
	if( GetDisplayed() == SHOW_1 )
		return 1;
	else if( GetDisplayed() == SHOW_2 )
		return 2;
	else if( GetDisplayed() == SHOW_3 )
		return 3;
	else if( GetDisplayed() == SHOW_4 )
		return 4;
	else if( GetDisplayed() == SHOW_5 )
		return 5;
	else if( GetDisplayed() == SHOW_6 )
		return 6;
	// These two are not being displayed anytime soon...
	else if( GetDisplayed() == SHOW_ES )
		return -1;
	else if( GetDisplayed() == SHOW_OMES )
		return -1;
	else
		return 0;
}

bool Song::ShowOnExtra1() const
{
	// We take care of Roulette elsewhere
	if( GetDisplayed() == SHOW_OMES || GetDisplayed() == SHOW_NEVER )
		return false;
	else
		return true;
}

bool Song::ShowOnExtra2() const
{
	// We take care of Roulette elsewhere
	if( GetDisplayed() == SHOW_NEVER )
		return false;
	else
		return true;
}

bool Song::ShowInDemonstrationAndRanking() const { return !IsTutorial() && NormallyDisplayed(); }


bool Song::HasIntro() const 		{return m_sIntroFile != ""		&&	IsAFile(GetIntroPath()); }
bool Song::HasMusic() const 		{return m_sMusicFile != ""		&&	IsAFile(GetMusicPath()); }
bool Song::HasBanner() const 		{return m_sBannerFile != ""		&&	IsAFile(GetBannerPath()); }
bool Song::HasDisc() const 			{return m_sDiscFile != ""		&&	IsAFile(GetDiscPath()); }
bool Song::HasBackground() const 	{return m_sBackgroundFile != ""	&&	IsAFile(GetBackgroundPath()); }
bool Song::HasPreview() const 		{return m_sPreviewFile != ""	&&	IsAFile(GetPreviewPath()); }
bool Song::HasLyrics() const		{return m_sLyricsFile != ""		&&	IsAFile(GetLyricsPath()); }
bool Song::HasCDTitle() const 		{return m_sCDTitleFile != ""	&&	IsAFile(GetCDTitlePath()); }
bool Song::HasBGChanges() const 	{return !m_BGChanges.empty(); }

CString GetSongAssetPath( CString sPath, const CString &sSongPath )
{
	if( sPath == "" )
		return "";

	/* If there's no path in the file, the file is in the same directory
	 * as the song.  (This is the preferred configuration.) */
	if( sPath.find('/') == CString::npos )
		return sSongPath+sPath;

	/* The song contains a path; treat it as relative to the top SM directory. */
	if( sPath.Left(3) == "../" )
	{
		/* The path begins with "../".  Resolve it wrt. the song directory. */
		sPath = sSongPath + sPath;
	}

	CollapsePath( sPath );

	/* If the path still begins with "../", then there were an unreasonable number
	 * of them at the beginning of the path.  Ignore the path entirely. */
	if( sPath.Left(3) == "../" )
		return "";

	return sPath;
}

void Song::SetDefaultMusicSampleStart()
{
	m_fMusicSampleStartSeconds = this->m_Timing.GetElapsedTimeFromBeat( 100 );

	if( m_fMusicSampleStartSeconds+m_fMusicSampleLengthSeconds > this->m_fMusicLengthSeconds )
	{
		// fix for BAG and other slow songs
		int iBeat = (int)(m_Timing.m_fLastBeat/2);
		iBeat = iBeat - (iBeat % 4);
		m_fMusicSampleStartSeconds = this->m_Timing.GetElapsedTimeFromBeat( (float)iBeat );
	}
}

void Song::SetDefaultMusicSampleLength()
{
	m_fMusicSampleLengthSeconds = DEFAULT_MUSIC_SAMPLE_LENGTH;
}

/* Note that supplying a path relative to the top-level directory is only for compatibility
 * with DWI.  We prefer paths relative to the song directory. */
CString Song::GetIntroPath() const
{
	return GetSongAssetPath( m_sIntroFile, m_sSongDir );
}

CString Song::GetMusicPath() const
{
	return GetSongAssetPath( m_sMusicFile, m_sSongDir );
}

CString Song::GetBannerPath() const
{
	return GetSongAssetPath( m_sBannerFile, m_sSongDir );
}

CString Song::GetDiscPath() const
{
	return GetSongAssetPath( m_sDiscFile, m_sSongDir );
}

CString Song::GetBackgroundPath() const
{
	return GetSongAssetPath( m_sBackgroundFile, m_sSongDir );
}

CString Song::GetPreviewPath() const
{
	return GetSongAssetPath( m_sPreviewFile, m_sSongDir );
}

CString Song::GetLyricsPath() const
{
	return GetSongAssetPath( m_sLyricsFile, m_sSongDir );
}

CString Song::GetCDTitlePath() const
{
	return GetSongAssetPath( m_sCDTitleFile, m_sSongDir );
}

CString Song::GetDisplayMainTitle() const
{
	if(!PREFSMAN->m_bShowNative) return GetTranslitMainTitle();
	return m_sMainTitle;
}

CString Song::GetDisplaySubTitle() const
{
	if(!PREFSMAN->m_bShowNative) return GetTranslitSubTitle();
	return m_sSubTitle;
}

CString Song::GetDisplayArtist() const
{
	if(!PREFSMAN->m_bShowNative) return GetTranslitArtist();
	return m_sArtist;
}


CString Song::GetFullDisplayTitle() const
{
	CString Title = GetDisplayMainTitle();
	CString SubTitle = GetDisplaySubTitle();

	if(!SubTitle.empty()) Title += " " + SubTitle;
	return Title;
}

CString Song::GetFullTranslitTitle() const
{
	CString Title = GetTranslitMainTitle();
	CString SubTitle = GetTranslitSubTitle();

	if(!SubTitle.empty()) Title += " " + SubTitle;
	return Title;
}

void Song::AddSteps( Steps* pSteps )
{
	m_vpSteps.push_back( pSteps );
	ASSERT_M( pSteps->m_StepsType < NUM_STEPS_TYPES, ssprintf("%i", pSteps->m_StepsType) );
	m_vpStepsByType[pSteps->m_StepsType].push_back( pSteps );
}

void Song::RemoveSteps( const Steps* pSteps )
{
	// Avoid any stale Note::parent pointers by removing all AutoGen'd Steps,
	// then adding them again.

	RemoveAutoGenNotes();


	vector<Steps*> &vpSteps = m_vpStepsByType[pSteps->m_StepsType];
	for( int j=vpSteps.size()-1; j>=0; j-- )
	{
		if( vpSteps[j] == pSteps )
		{
//			delete vpSteps[j]; // delete below
			vpSteps.erase( vpSteps.begin()+j );
			break;
		}
	}

	for( int j=m_vpSteps.size()-1; j>=0; j-- )
	{
		if( m_vpSteps[j] == pSteps )
		{
			delete m_vpSteps[j];
			m_vpSteps.erase( m_vpSteps.begin()+j );
			break;
		}
	}

	AddAutoGenNotes();
}

bool Song::Matches(CString sGroup, CString sSong) const
{
	if( sGroup.size() && sGroup.CompareNoCase(this->m_sGroupName) != 0)
		return false;

	CString sDir = this->GetSongDir();
	sDir.Replace("\\","/");
	CStringArray bits;
	split( sDir, "/", bits );
	ASSERT(bits.size() >= 2); /* should always have at least two parts */
	const CString &sLastBit = bits[bits.size()-1];

	// match on song dir or title (ala DWI)
	if( !sSong.CompareNoCase(sLastBit) )
		return true;
	if( !sSong.CompareNoCase(this->GetFullTranslitTitle()) )
		return true;

	return false;
}

void Song::FreeAllLoadedFromProfiles()
{
	for( int s=m_vpSteps.size()-1; s>=0; s-- )
	{
		Steps* pSteps = m_vpSteps[s];
		if( pSteps->WasLoadedFromProfile() )
			this->RemoveSteps( pSteps );
	}
}

int Song::GetNumStepsLoadedFromProfile( ProfileSlot slot ) const
{
	int iCount = 0;
	for( unsigned s=0; s<m_vpSteps.size(); s++ )
	{
		Steps* pSteps = m_vpSteps[s];
		if( pSteps->GetLoadedFromProfileSlot() == slot )
			iCount++;
	}
	return iCount;
}

bool Song::IsEditAlreadyLoaded( Steps* pSteps ) const
{
	ASSERT( pSteps->GetDifficulty() == DIFFICULTY_EDIT );

	for( unsigned i=0; i<m_vpSteps.size(); i++ )
	{
		Steps* pOther = m_vpSteps[i];
		if( pOther->GetDifficulty() == DIFFICULTY_EDIT &&
			pOther->m_StepsType == pSteps->m_StepsType &&
			pOther->GetHash() == pSteps->GetHash() )
		{
			return true;
		}
	}

	return false;
}

BPMRange* Song::GetSortByBPMRange() const
{
	if( !m_SortByBPMRange )
	{
		BPMRange range;
		vector<Steps*> aNotes;
		GetSteps( aNotes, GAMESTATE->GetCurrentStyle()->m_StepsType );
		FOREACH_CONST( Steps*, aNotes, s )
			range &= (*s)->m_BPMRange;

		m_SortByBPMRange = new BPMRange(range);
	}

	return m_SortByBPMRange;
}

bool Song::HasSignificantBpmChangesOrStops() const
{
	// TODO - Aldo_MX: Don't consider BPM changes that only are only for maintaining sync as a gimmicky BpmChange.
	return m_Timing.HasBpmChangesOrStops();
}

//void Song::GetAttackArray( AttackArray &out ) const
//{
//	CString sOptions = GAMESTATE->m_PlayerOptions[p]
//
//	{
//		Attack a;
//		a.fStartSecond = 0;
//		a.fSecsRemaining = 10000; /* whole song */
//		a.level = ATTACK_LEVEL_1;
//		a.sModifier = m_Attacks;
//		a.bGlobal = true;
//
//		out.push_back( a );
//	}
//
//	out.insert( out.end(), m_Attacks.begin(), m_Attacks.end() );
//}

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * Contains code (c) 2001-2004 Chris Danford, Glenn Maynard.
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
