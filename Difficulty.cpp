#include "global.h"
#include "Difficulty.h"
#include "GameState.h"
#include "ThemeManager.h"


static const CString DifficultyNames[NUM_DIFFICULTIES] = {
	"Beginner",
	"Easy",
	"Medium",
	"Hard",
	"Challenge",
	"Edit",
};
XToString( Difficulty );
XToThemedString( Difficulty );

/* We prefer the above names; recognize a number of others, too.  (They'll
 * get normalized when written to SMs, etc.) */
Difficulty StringToDifficulty( const CString& sDC, bool bPIU )
{
	CString s2 = sDC;
	s2.MakeLower();
	if( s2.find( "beginner" ) != CString::npos )		return DIFFICULTY_BEGINNER;
	else if( s2.find( "practice" ) != CString::npos )	return DIFFICULTY_BEGINNER;
	else if( s2.find( "easy" ) != CString::npos )		return DIFFICULTY_EASY;
	else if( s2.find( "basic" ) != CString::npos )		return DIFFICULTY_EASY;
	else if( s2.find( "light" ) != CString::npos )		return DIFFICULTY_EASY;
	else if( s2.find( "normal" ) != CString::npos )		return DIFFICULTY_EASY;
	else if( s2.find( "medium" ) != CString::npos )		return DIFFICULTY_MEDIUM;
	else if( s2.find( "trick" ) != CString::npos )		return DIFFICULTY_MEDIUM;
	else if( s2.find( "standard" ) != CString::npos )	return DIFFICULTY_MEDIUM;
	else if( s2.find( "difficult") != CString::npos )	return DIFFICULTY_MEDIUM;
	else if( s2.find( "freestyle") != CString::npos )	return DIFFICULTY_MEDIUM;
	else if( s2.find( "hard" ) != CString::npos )		{ if( bPIU ) return DIFFICULTY_MEDIUM; else return DIFFICULTY_HARD; }
	else if( s2.find( "maniac" ) != CString::npos )		return DIFFICULTY_HARD;
	else if( s2.find( "heavy" ) != CString::npos )		return DIFFICULTY_HARD;
	else if( s2.find( "crazy" ) != CString::npos )		return DIFFICULTY_HARD;
	else if( s2.find( "double") != CString::npos )		return DIFFICULTY_MEDIUM;
	else if( s2.find( "nightmare" ) != CString::npos )	return DIFFICULTY_HARD;
	else if( s2.find( "wild" ) != CString::npos )		return DIFFICULTY_CHALLENGE;
	else if( s2.find( "smaniac" ) != CString::npos )	return DIFFICULTY_CHALLENGE;
	else if( s2.find( "challenge" ) != CString::npos )	return DIFFICULTY_CHALLENGE;
	else if( s2.find( "expert" ) != CString::npos )		return DIFFICULTY_CHALLENGE;
	else if( s2.find( "another" ) != CString::npos )	{ if( bPIU ) return DIFFICULTY_EDIT; else return DIFFICULTY_MEDIUM; }
	else if( s2.find( "ssr" ) != CString::npos )		return DIFFICULTY_HARD;
	else if( s2.find( "oni" ) != CString::npos )		return DIFFICULTY_CHALLENGE;
	else if( s2.find( "edit" ) != CString::npos )		return DIFFICULTY_EDIT;
	else return DIFFICULTY_INVALID;
}

static const CString CourseDifficultyNames[NUM_DIFFICULTIES] =
{
	"(not used)",
	"Easy",
	"Regular",
	"Difficult",
	"(not used)",
};
XToString( CourseDifficulty );
XToThemedString( CourseDifficulty );
StringToX( CourseDifficulty );

CourseDifficulty GetNextShownCourseDifficulty( CourseDifficulty cd )
{
	for( CourseDifficulty d=(CourseDifficulty)(cd+1); d<NUM_DIFFICULTIES; enum_add(d, 1) )
	{
		if( GAMESTATE->IsCourseDifficultyShown(d) )
			return d;
	}
	return DIFFICULTY_INVALID;
}

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * Contains code (c) 2001-2004 Chris Danford.
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
