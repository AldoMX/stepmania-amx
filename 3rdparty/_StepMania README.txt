The following libraries are only for Windows:

ffmpeg-2.1.4-win32-dev
jpeg-9a
libogg-1.3.1
libpng-1.6.10
libvorbis-1.3.4
SDL-1.2.15
zlib-1.2.5

For other OS's, install them yourself as usual.

---

The following libraries have had some files (like VC Projects and config. headers)
modified, so it's better to make a diff before attempting to upgrade them:

jpeg-9a
libogg-1.3.1
libpng-1.6.10
libtomcrypt-1.17
libtommath-0.42.0
libvorbis-1.3.4
lua-5.1.5
SDL-1.2.15
zlib-1.2.5
