#include "global.h"
#include "BPMDisplay.h"
#include "RageUtil.h"
#include "GameConstantsAndTypes.h"
#include "GameState.h"
#include "ThemeManager.h"

#include "PlayerNumber.h"
#include "Steps.h"
#include "Trail.h"

CachedThemeMetricC	BPMD_NORMAL_COLOR		("BPMDisplay", "NormalColor");
CachedThemeMetricC	BPMD_CHANGE_COLOR		("BPMDisplay", "ChangeColor");
CachedThemeMetricC	BPMD_RANDOM_COLOR		("BPMDisplay", "RandomColor"); // Color defn for randomly cycling BPMs
CachedThemeMetricC	BPMD_EXTRA_COLOR		("BPMDisplay", "ExtraColor");
CachedThemeMetricB	BPMD_CYCLE				("BPMDisplay", "Cycle");
CachedThemeMetric	BPMD_SEPARATOR			("BPMDisplay", "Separator");
CachedThemeMetric	BPMD_NO_BPM_TEXT		("BPMDisplay", "NoBPMText");
CachedThemeMetricB	BPMD_SHOW_QUESTION_MARK	("BPMDisplay", "Show???");
CachedThemeMetricB	BPMD_SHOW_NEGATIVE		("BPMDisplay", "ShowNegative");
CachedThemeMetricB	BPMD_USE_BPS			("BPMDisplay", "ShowAsBPS");
CachedThemeMetric	BPMD_BPM_RAND_CYCLE		("BPMDisplay", "RandomBPMCycleSpeed");

BPMDisplay::BPMDisplay()
{
	m_fBPMFrom = m_fBPMTo = 0;
	m_iCurrentBPM = 0;
	m_BPMS.push_back(0);
	m_fPercentInState = 0;
	m_fCycleTime = 1.0f;
}

void BPMDisplay::Load()
{
	BPMD_NORMAL_COLOR.Refresh(m_sName);
	BPMD_CHANGE_COLOR.Refresh(m_sName);
	BPMD_RANDOM_COLOR.Refresh(m_sName);
	BPMD_EXTRA_COLOR.Refresh(m_sName);
	BPMD_CYCLE.Refresh(m_sName);
	BPMD_SEPARATOR.Refresh(m_sName);
	BPMD_NO_BPM_TEXT.Refresh(m_sName);
	BPMD_SHOW_QUESTION_MARK.Refresh(m_sName);
	BPMD_SHOW_NEGATIVE.Refresh(m_sName);
	BPMD_USE_BPS.Refresh(m_sName);
	BPMD_BPM_RAND_CYCLE.Refresh(m_sName);

	m_textBPM.SetName( "Text" );
	m_textBPM.LoadFromFont( THEME->GetPathToF(m_sName) );
	SET_XY_AND_ON_COMMAND( m_textBPM );
	m_textBPM.SetDiffuse( BPMD_NORMAL_COLOR );
	this->AddChild( &m_textBPM );

	m_sprLabel.Load( THEME->GetPathToG(ssprintf("%s label", m_sName.c_str())) );
	m_sprLabel->SetName( "Label" );
	SET_XY_AND_ON_COMMAND( m_sprLabel );
	m_sprLabel->SetDiffuse( BPMD_NORMAL_COLOR );
	this->AddChild( m_sprLabel );

	this->SortByDrawOrder();
}

float BPMDisplay::GetActiveBPM() const
{
	return m_fBPMTo + (m_fBPMFrom-m_fBPMTo) * m_fPercentInState;
}

void BPMDisplay::Update( float fDeltaTime )
{
	ActorFrame::Update( fDeltaTime );

	if( !BPMD_CYCLE )
		return;
	if( m_BPMS.size() == 0 )
		return; // no bpm

	m_fPercentInState -= fDeltaTime / m_fCycleTime;
	if( m_fPercentInState < 0 )
	{
		// go to next state
		m_fPercentInState = 1;		// reset timer

		m_iCurrentBPM = (m_iCurrentBPM + 1) % m_BPMS.size();
		m_fBPMFrom = m_fBPMTo;
		m_fBPMTo = m_BPMS[m_iCurrentBPM];

		if(m_fBPMTo == -1)
		{
			m_fBPMFrom = -1;
			if( BPMD_SHOW_QUESTION_MARK )
				m_textBPM.SetText( (RandomFloat(0,1)>0.90) ? CString("xxx") : ssprintf("%03.0f",RandomFloat(0,999)) );
			else
				m_textBPM.SetText( ssprintf("%03.0f",RandomFloat(0,999)) );
		}
		else if( m_fBPMFrom == -1 )
			m_fBPMFrom = m_fBPMTo;
	}

	// update m_textBPM
	if( m_fBPMTo != -1)
	{
		if( BPMD_USE_BPS )
		{
			const float fActualBPS = GetActiveBPM()/60.0f;

			// If we have a negative BPM in display, just display '000' if we've set it in the theme to hide it
			if( fActualBPS < 0 && !BPMD_SHOW_NEGATIVE )
				m_textBPM.SetText( ssprintf("000") );
			else
				m_textBPM.SetText( ssprintf("%03.0f", fActualBPS) );
		}
		else
		{
			const float fActualBPM = GetActiveBPM();

			// If we have a negative BPM in display, just display '000' if we've set it in the theme to hide it
			if( fActualBPM < 0 && !BPMD_SHOW_NEGATIVE )
				m_textBPM.SetText( ssprintf("000") );
			else
				m_textBPM.SetText( ssprintf("%03.0f", fActualBPM) );
		}
	}
}

void BPMDisplay::SetBPMRange( const vector<float>& bpms )
{
	ASSERT( !bpms.empty() );

	unsigned i;
	bool AllIdentical = true;
	bool IsRandom = false;
	for( i = 0; i < bpms.size(); ++i )
	{
		if( i > 0 && bpms[i] != bpms[i-1] )
			AllIdentical = false;
	}

	if( !BPMD_CYCLE )
	{
		int MinBPM = INT_MAX,
			MaxBPM = INT_MIN;
		for( i = 0; i < bpms.size(); ++i )
		{
			MinBPM = min( MinBPM, (int)roundf(bpms[i]) );
			MaxBPM = max( MaxBPM, (int)roundf(bpms[i]) );
		}
		if( MinBPM == MaxBPM )
		{
			if( MinBPM == -1 )
			{
				m_textBPM.SetText( "..." ); // random
				IsRandom = true;
			}
			else
				m_textBPM.SetText( ssprintf("%i", MinBPM) );
		}
		else
			m_textBPM.SetText( ssprintf("%i%s%i", MinBPM, CString(BPMD_SEPARATOR).c_str(), MaxBPM) );
	}
	else
	{
		m_BPMS.clear();
		for( i = 0; i < bpms.size(); ++i )
		{
			m_BPMS.push_back(bpms[i]);
			if( bpms[i] != -1 )
				m_BPMS.push_back(bpms[i]); /* hold */
		}

		m_iCurrentBPM = min(1u, m_BPMS.size()); /* start on the first hold */
		m_fBPMFrom = bpms[0];
		m_fBPMTo = bpms[0];
		m_fPercentInState = 1;
	}

	m_textBPM.StopTweening();
	m_sprLabel->StopTweening();
	m_textBPM.BeginTweening(0.5f);
	m_sprLabel->BeginTweening(0.5f);

	if( GAMESTATE->IsExtraStage() || GAMESTATE->IsExtraStage2() )
	{
		m_textBPM.SetDiffuse( BPMD_EXTRA_COLOR );
		m_sprLabel->SetDiffuse( BPMD_EXTRA_COLOR );
	}
	else if( !AllIdentical )
	{
		m_textBPM.SetDiffuse( BPMD_CHANGE_COLOR );
		m_sprLabel->SetDiffuse( BPMD_CHANGE_COLOR );
	}
	else
	{
		m_textBPM.SetDiffuse( BPMD_NORMAL_COLOR );
		m_sprLabel->SetDiffuse( BPMD_NORMAL_COLOR );
	}
}

void BPMDisplay::CycleRandomly()
{
	const static vector<float> bpms(1, -1.f);
	SetBPMRange( bpms );

	m_textBPM.SetDiffuse( BPMD_RANDOM_COLOR );
	m_sprLabel->SetDiffuse( BPMD_RANDOM_COLOR );

	CString sValue = BPMD_BPM_RAND_CYCLE;
	sValue.MakeLower();

	// Set the cycle speed
	if( sValue == "" )
		m_fCycleTime = 0.2f;
	else if( stricmp(sValue, "turtle") == 0 )
		m_fCycleTime = 1.0f;	// I doubt this one will ever see any use
	else if( stricmp(sValue, "slow") == 0 )
		m_fCycleTime = 0.5f;
	else if( stricmp(sValue, "fast") == 0 )
		m_fCycleTime = 0.1f;
	else if( stricmp(sValue, "vfast") == 0 )
		m_fCycleTime = 0.05f;	// This is what DDR Extreme runs around
	else if( stricmp(sValue, "hyper") == 0 )
		m_fCycleTime = 0.01f;
	else if( stricmp(sValue, "overdrive") == 0 )
		m_fCycleTime = 0.005f;
	else if( stricmp(sValue, "insanity") == 0 )
		m_fCycleTime = 0.0001f;
	else if( stricmp(sValue, "retardation") == 0 )
		m_fCycleTime = 0.0005f;
	else if( stricmp(sValue, "omfg") == 0 )
		m_fCycleTime = 0.00001f;	// I actually added this one as a joke... - Mike
	else
		m_fCycleTime = 0.2f;
}

void BPMDisplay::NoBPM()
{
	m_BPMS.clear();
	m_textBPM.SetText( BPMD_NO_BPM_TEXT );

	m_textBPM.SetDiffuse( BPMD_NORMAL_COLOR );
	m_sprLabel->SetDiffuse( BPMD_NORMAL_COLOR );
}

void BPMDisplay::SetBPM( Steps* pStepsArray[] )
{
	BPMRange range;
	FOREACH_EnabledPlayer( pn )
	{
		const Steps* pSteps = pStepsArray[pn];
		if( pSteps )
		{
			if( pSteps->m_DisplayBPM == BPM_RANDOM )
			{
				CycleRandomly();
				return;
			}
			range &= pSteps->m_BPMRange;
		}
	}

	vector<float> bpms;
	range.AddTo(bpms);
	SetBPMRange(bpms);

	m_fCycleTime = 1.0f;
}

void BPMDisplay::SetBPM( const Trail *pTrail )
{
	ASSERT( pTrail );

	vector<float> bpms;
	pTrail->GetBPMs(bpms);
	SetBPMRange(bpms);

	m_fCycleTime = 0.2f;
}

void BPMDisplay::DrawPrimitives()
{
	ActorFrame::DrawPrimitives();
}

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * Contains code (c) 2001-2002 Chris Danford.
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
