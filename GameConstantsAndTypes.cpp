#include "global.h"
#include "GameConstantsAndTypes.h"
#include "GameState.h"
#include "RageUtil.h"
#include "ThemeManager.h"
#include "EnumHelper.h"
#include "Foreach.h"
#include "RageLog.h"

int SCREEN_WIDTH = 640;
int SCREEN_HEIGHT = 480;

static const CString RadarCategoryNames[NUM_RADAR_CATEGORIES] = {
	"Stream",
	"Voltage",
	"Air",
	"Freeze",
	"Chaos",
	"Taps",
	"Jumps",
	"Holds",
	"Rolls",
	"Mines",
	"Shocks",
	"Potions",
	"Hands",
	"Lifts",
	"Hidden"
};
XToString( RadarCategory );
XToThemedString( RadarCategory );


static const CString StepsTypeNames[NUM_STEPS_TYPES] = {
	"dance-single",
	"dance-double",
	"dance-couple",
	"dance-solo",
	"pump-single",
	"pump-double",
	"pump-halfdouble",
	"pump-couple",
	"ez2-single",
	"ez2-double",
	"ez2-real",
	"para-single",
	"ds3ddx-single",
	"bm-single",
	"bm-double",
	"iidx-single7",
	"iidx-double7",
	"iidx-single5",
	"iidx-double5",
	"maniax-single",
	"maniax-double",
	"techno-single4",
	"techno-single5",
	"techno-single8",
	"techno-double4",
	"techno-double5",
	"pnm-five",
	"pnm-nine",
	"lights-cabinet"
};
XToString( StepsType );
XToThemedString( StepsType );
StringToX( StepsType );

StepsType StringToStepsType2( CString sStepsType )
{
	sStepsType.MakeLower();

	// HACK!  We elminitated "ez2-single-hard", but we should still handle it.
	if( sStepsType == "ez2-single-hard" )
		sStepsType = "ez2-single";

	// HACK!  "para-single" used to be called just "para"
	if( sStepsType == "para" )
		sStepsType = "para-single";

	if( sStepsType.Right(7) == "routine" )
	{
		sStepsType.resize( sStepsType.size()-7 );
		sStepsType += "double";
	}

	StepsType st = StringToStepsType(sStepsType);
	if( st == STEPS_TYPE_INVALID )
	{
		// invalid StepsType
		LOG->Warn( "Invalid StepsType string '%s' encountered.  Assuming this is 'dance-single'.", sStepsType.c_str() );
		return STEPS_TYPE_DANCE_SINGLE;
	}
	return st;
}


static const CString PlayModeNames[NUM_PLAY_MODES] = {
	"Regular",
	"Nonstop",
	"Oni",
	"Endless",
	"Battle",
	"Rave",
};
XToString( PlayMode );
XToThemedString( PlayMode );
StringToX( PlayMode );


RankingCategory AverageMeterToRankingCategory( int iAverageMeter )
{
	if(      iAverageMeter <= 3 )	return RANKING_A;
	else if( iAverageMeter <= 6 )	return RANKING_B;
	else if( iAverageMeter <= 9 )	return RANKING_C;
	else							return RANKING_D;
}


static const CString RankingCategoryNames[NUM_RANKING_CATEGORIES] = {
	"a",
	"b",
	"c",
	"d",
};
XToString( RankingCategory );
StringToX( RankingCategory );


static const CString SortOrderNames[NUM_SORT_ORDERS] = {
	"PREFERRED",
	"GROUP",
	"FOLDER",
	"TITLE",
	"GENRE",
	"BPM",
	"PLAYERS BEST",
	"LIST SORT",
	"TOP GRADE",
	"ARTIST",
	"EASY METER",
	"MEDIUM METER",
	"HARD METER",
	"CHALLENGE METER",
	"SORT",
	"MODE",
	"COURSES",
	"NONSTOP",
	"ONI",
	"ENDLESS",
	"ROULETTE"
};
XToString( SortOrder );
StringToX( SortOrder );


static const CString TapNoteScoreNames[NUM_TAP_NOTE_SCORES] = {
	"None",
	"HitMine",
	"HitShock",
	"HitPotion",
	"MissCheckpoint",
	"Checkpoint",
	"MissHidden",
	"Hidden",
	"Miss",
	"Boo",
	"Good",
	"Great",
	"Perfect",
	"Marvelous",
};
XToString( TapNoteScore );
StringToX( TapNoteScore );
XToThemedString( TapNoteScore );


static const CString HoldNoteScoreNames[NUM_HOLD_NOTE_SCORES] = {
	"None",
	"HoldNG",
	"HoldOK",
	"RollNG",
	"RollOK",
	"ShockNG",
	"ShockOK",
};
XToString( HoldNoteScore );
StringToX( HoldNoteScore );
XToThemedString( HoldNoteScore );


static const CString MemoryCardStateNames[NUM_MEMORY_CARD_STATES] = {
	"ready",
	"late",
	"error",
	"none",
};
XToString( MemoryCardState );


static const CString PerDifficultyAwardNames[NUM_PER_DIFFICULTY_AWARDS] = {
	"FullComboGreats",
	"SingleDigitGreats",
	"OneGreat",
	"FullComboPerfects",
	"SingleDigitPerfects",
	"OnePerfect",
	"FullComboMarvelouses",
	"Greats80Percent",
	"Greats90Percent",
	"Greats100Percent",
};
XToString( PerDifficultyAward );
XToThemedString( PerDifficultyAward );
StringToX( PerDifficultyAward );


// Numbers are intentially not at the front of these strings so that the
// strings can be used as XML entity names.
// Numbers are intentially not at the back so that "1000" and "10000" don't
// conflict when searching for theme elements.
static const CString PeakComboAwardNames[NUM_PEAK_COMBO_AWARDS] = {
	"Peak1000Combo",
	"Peak2000Combo",
	"Peak3000Combo",
	"Peak4000Combo",
	"Peak5000Combo",
	"Peak6000Combo",
	"Peak7000Combo",
	"Peak8000Combo",
	"Peak9000Combo",
	"Peak10000Combo",
};
XToString( PeakComboAward );
XToThemedString( PeakComboAward );
StringToX( PeakComboAward );


static const CString SelectionDisplayNames[NUM_SELECTION_DISPLAY] = {
	"YES",
	"ROULETTE",
	"NO",
	"1",
	"2",
	"3",
	"4",
	"5",
	"6",
	"ES",
	"OMES",
	"NO-ES",
	"NO-OMES",
};
XToString( SelectionDisplay );
StringToX( SelectionDisplay );


static const CString DisplayBPMNames[NUM_DISPLAY_BPM] = {
	"Real",
	"Custom",
	"Random"
};
XToString( DisplayBPM );
StringToX( DisplayBPM );


static const CString TouchStateNames[NUM_TOUCH_STATES] = {
	"Normal",
	"Pressed"
};
XToString( TouchState );
StringToX( TouchState );


static const CString AudioFormatNames[NUM_AUDIO_FORMATS] = {
	"mp3",
	"wav",
	"flac",
	"ogg",
};
XToString( AudioFormat );
StringToX( AudioFormat );


static const CString PictureFormatNames[NUM_PICTURE_FORMATS] = {
	"png",
	"jpg",
	"jpeg",
	"bmp",
	"dib",
	"gif",
	"xpm",

	//"dds",
	//"tga",

	//"pnz",
	//"g"
};
XToString( PictureFormat );
StringToX( PictureFormat );


static const CString VideoFormatNames[NUM_VIDEO_FORMATS] = {
	"avi",

	"mpg",
	"mp4",
	"mpeg",
	"m2v",
	"m4v",
	"mpv",
	"mpe",

	"mkv",
	"wmv",
	"flv",
	"mov",
	"3gp",
	"asf",

	"bik",
	"ogv",
	"webm",
	"asx",
	"nsv",
	"rm",
	"ram",

	"vob",
	"mts",
	"m2ts"
};
XToString( VideoFormat );
StringToX( VideoFormat );


static const CString StepFormatNames[NUM_STEP_FORMATS] = {
	"sma",
	"ssc",
	"sm",
	"dwi",

	//"stx",
	"old",

	"bms",
	"ksf",
	"ucs",
	//"nx"
};
XToString( StepFormat );
StringToX( StepFormat );


static const CString AnimationTimingNames[NUM_ANIMATION_TIMINGS] = {
	"Step",
	"BGA",
	"Seconds",
};
XToString(AnimationTiming);
StringToX(AnimationTiming);


#include "LuaFunctions.h"
#define LuaXToString(X)	\
CString Lua##X##ToString( int n ) \
{ return X##ToString( (X) n ); } \
LuaFunction_Int( X##ToString, Lua##X##ToString(a1) ); /* register it */

#define LuaStringToX(X)	\
X LuaStringTo##X( CString s ) \
{ return (X) StringTo##X( s ); } \
LuaFunction_Str( StringTo##X, LuaStringTo##X(str) ); /* register it */

LuaXToString( Difficulty );
LuaStringToX( Difficulty );

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * Contains code (c) 2001-2004 Chris Danford.
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
