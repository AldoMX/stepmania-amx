/*
 * RageSoundReader_FFMpeg - Sound reader for any format supported by FFMpeg
 */

#ifndef RAGE_SOUND_READER_FFMPEG
#define RAGE_SOUND_READER_FFMPEG

#include "RageSoundReader_FileReader.h"

class RageSoundReader_FFMpeg : public SoundReader_FileReader
{
	static const unsigned DEFAULT_CHANNELS = 2;

	void *m_pState;

	uint8_t *m_pFrameBuffer;
	size_t m_frameBufferSize;

	uint64_t m_channelLayout;
	int m_sampleFormat;
	int m_sampleRate;

	float m_currentTime;

	static void RegisterProtocols();
	OpenResult CreateDecoder(const CString &filename);
	void DestroyDecoder();
	void SetError(const char *fmt, ...) const;

	size_t ReadFromLastFrame(char *buf, size_t len);
	bool UpdateResamplingOpts(uint64_t channelLayout, int sampleFormat, int sampleRate);
	int DecodeFrame();

public:
	RageSoundReader_FFMpeg();
	~RageSoundReader_FFMpeg();

	OpenResult Open(CString filename);
	int GetLength() const;
	int GetLength_Fast() const;
	int SetPosition_Accurate(int ms);
	int SetPosition_Fast(int ms);
	int Read(char *buf, unsigned len);
	SoundReader * Copy() const;
	int GetSampleRate() const { return m_sampleRate; }
	unsigned GetNumChannels() const { return DEFAULT_CHANNELS; }
};

#endif

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
