/* NoteDisplay - Draws TapNotes and HoldNotes. */

#ifndef NOTEDISPLAY_H
#define NOTEDISPLAY_H

#include "Sprite.h"
#include "BitmapText.h"
class Model;
#include "NoteTypes.h"
#include "PlayerNumber.h"
#include "GameConstantsAndTypes.h"

struct HoldNoteResult;
struct NoteMetricCache_t;

enum Part
{
	PART_TAP,
	PART_ADDITION,
	PART_MINE,
	PART_SHOCK,
	PART_POTION,
	PART_LIFT,
	NUM_PARTS,
	PART_INVALID
};

enum HoldPart
{
	HOLD_PART_HEAD,
	HOLD_PART_TAIL,
	HOLD_PART_TOP_CAP,
	HOLD_PART_BODY,
	HOLD_PART_BOTTOM_CAP,
	NUM_HOLD_PARTS,
	HOLD_PART_INVALID
};

class NoteDisplay
{
	friend class NoteField;

public:
	NoteDisplay();
	~NoteDisplay();

	CString GetNoteColorPath( CString sNoteSkin, CString sButton, CString sNoteActor, NoteType nt );

	void Load( int iColNum, PlayerNumber pn, CString NoteSkin, float fYReverseOffsetPixels, bool bIsRoutine, AnimationTiming animationTiming );
	static void Update( float fDeltaTime );

	void DrawActor( Actor* pActor, int iCol, float fBeat, float fPercentFadeToFail, float fNoteAlpha, float fLife, float fReverseOffsetPixels, bool bUseLighting, bool bUseRoutine, unsigned uPlayerNumber );
	void DrawTap( const TapNote& tn, int iCol, float fBeat, float fPercentFadeToFail, float fNoteAlpha, float fLife, float fReverseOffsetPixels, const HoldNote* pHN );
	void DrawHold( const HoldNote& hn, bool bIsBeingHeld, bool bIsActive, const HoldNoteResult &Result, float fPercentFadeToFail, float fNoteAlpha, bool bDrawGlowOnly, float fReverseOffsetPixels );

protected:
	void SetActiveFrame( float fNoteBeat, Actor &actorToSet, float fAnimationLengthInBeats, bool bVivid );
	Actor* GetTapNoteActor( float fNoteBeat, Part part = PART_TAP );
	Actor* GetTapHiddenActor( float fNoteBeat );
	Actor* GetHoldActor( const HoldNote& hn, HoldPart holdPart, bool bIsActive, float fNoteBeat = FLT_MAX );

	void DrawHoldBottomCap( const HoldNote& hn, const bool bIsBeingHeld, float fYHead, float fYTail, int fYStep, int iCol, float fPercentFadeToFail, float fNoteAlpha, float fColorScale, bool bGlow );
	void DrawHoldTopCap( const HoldNote& hn, const bool bIsBeingHeld, float fYHead, float fYTail, int fYStep, int iCol, float fPercentFadeToFail, float fNoteAlpha, float fColorScale, bool bGlow );
	void DrawHoldBody( const HoldNote& hn, const bool bIsBeingHeld, float fYHead, float fYTail, int fYStep, int iCol, float fPercentFadeToFail, float fNoteAlpha, float fColorScale, bool bGlow );
	void DrawHoldTail( const HoldNote& hn, const bool bIsBeingHeld, float fYTail, int iCol, float fPercentFadeToFail, float fNoteAlpha, float fColorScale, bool bGlow );
	void DrawHoldHead( const HoldNote& hn, const bool bIsBeingHeld, bool bIsFlip, float fYHead, int iCol, float fPercentFadeToFail, float fNoteAlpha, float fColorScale, bool bGlow );

	PlayerNumber m_PlayerNumber;	// to look up PlayerOptions
	AnimationTiming m_animationTiming;
	bool m_bIsRoutine;

	BitmapText	m_RoutinePlayerNumber;

	unique_ptr< NoteMetricCache_t > cache;

	Actor*	m_pTapNote[NUM_PARTS][NUM_NOTE_TYPES];
	Actor*	m_pTapHidden[NUM_NOTE_TYPES];
	Actor*	m_pHoldActorActive[NUM_HOLD_TYPES][NUM_HOLD_PARTS][NUM_NOTE_TYPES];
	Actor*	m_pHoldActorInactive[NUM_HOLD_TYPES][NUM_HOLD_PARTS][NUM_NOTE_TYPES];

	float		m_fYReverseOffsetPixels;
};

#endif

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * Contains code (c) 2001-2004 Brian Bugh, Ben Nordstrom, Chris Danford.
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
