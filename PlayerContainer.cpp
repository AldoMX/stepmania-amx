#include "global.h"

#include "ActorUtil.h"
#include "PlayerContainer.h"
#include "GameState.h"
#include "GameManager.h"
#include "ThemeManager.h"
#include "PlayerOptions.h"
#include "Steps.h"
#include "RageLog.h"
#include "RageMath.h"

#define SINGLE_ZOOM			THEME->GetMetricF(m_sName,"LRSingleZoom")
#define SINGLE_NOTEFIELD_Y	THEME->GetMetricF(m_sName,"LRSingleAddY")

#define VERSUS_ZOOM			THEME->GetMetricF(m_sName,"LRVersusZoom")
#define VS_P1_NOTEFIELD_Y	THEME->GetMetricF(m_sName,"LRVersusP1AddY")
#define VS_P2_NOTEFIELD_Y	THEME->GetMetricF(m_sName,"LRVersusP2AddY")

#define DOUBLE_ZOOM			THEME->GetMetricF(m_sName,"LRDoubleZoom")
#define DOUBLE_NOTEFIELD_Y	THEME->GetMetricF(m_sName,"LRDoubleAddY")

PlayerContainer::PlayerContainer()
{
}

PlayerContainer::~PlayerContainer()
{
}

void PlayerContainer::Init( vector<float>& fPNX )
{
	fPNX.clear();

	bool bAllUnderAttack = false;
	CheckValidRotation(bAllUnderAttack);

	FOREACH_PlayerNumber( p )
	{
		fPNX.push_back( 0.f );

		bool bEnabled = GAMESTATE->IsPlayerEnabled(p);
		Player& player = m_Player[p];

		// NoteSkin Options
		if( bEnabled )
			GAMESTATE->LoadNSOptions(p);

		// Set the flag for different judgments here, but only if we're not in course mode!
		if( bEnabled && !GAMESTATE->IsCourseMode() )
			player.InitJudgment( p, GAMESTATE->m_pCurSteps[p]->GetDifficulty() );
		else
			player.InitJudgment( p, DIFFICULTY_INVALID );

		GAMESTATE->m_fNotePressedTime[p] = 0.f;

		if( !bEnabled )
			continue;

		float fPlayerX = GAMESTATE->m_fNSCenterX[p];

		// If every player enabled Under Attack, swap places to fake a rotation of the whole notefield.
		if( bAllUnderAttack )
			fPlayerX = GAMESTATE->m_fNSCenterX[NUM_PLAYERS - p - 1];

		bool bCenteredCoords = false;

		/* Perhaps this should be handled better by defining a new
		* StyleType for ONE_PLAYER_ONE_CREDIT_AND_ONE_COMPUTER,
		* but for now just ignore SoloSingles when it's Battle or Rave
		* Mode.  This doesn't begin to address two-player solo (6 arrows) */
		if( PREFSMAN->m_bSoloSingle &&
			GAMESTATE->m_PlayMode != PLAY_MODE_BATTLE &&
			GAMESTATE->m_PlayMode != PLAY_MODE_RAVE &&
			GAMESTATE->GetCurrentStyle()->m_StyleType == Style::ONE_PLAYER_ONE_CREDIT )
		{
			bCenteredCoords = true;
			fPlayerX = SCREEN_WIDTH / 2.f;
		}

		// Left, Right & Arbitrary Rotations
		if( !FEQ(fmodf(GAMESTATE->m_PlayerOptions[p].m_fNoteFieldRotationZ, 180.f), 0.f, 0.001f) )
		{
			float fZoom = 1.0f, fAddY = 0.f;
			bCenteredCoords = true;
			fPlayerX = SCREEN_WIDTH / 2.f;

			switch( GAMESTATE->GetCurrentStyle()->m_StyleType )
			{
			case Style::ONE_PLAYER_ONE_CREDIT:
				fZoom = SINGLE_ZOOM;
				fAddY = SINGLE_NOTEFIELD_Y;
				break;

			case Style::ONE_PLAYER_TWO_CREDITS:
				fZoom = DOUBLE_ZOOM;
				fAddY = DOUBLE_NOTEFIELD_Y;
				break;

			case Style::TWO_PLAYERS_TWO_CREDITS:
				fZoom = VERSUS_ZOOM;
				fAddY = p == PLAYER_1 ? VS_P1_NOTEFIELD_Y : VS_P2_NOTEFIELD_Y;
			}

			player.m_fLRNoteFieldZoom = fZoom;
			player.m_fLRAddY = fAddY;
		}

		player.SetName(CString(bCenteredCoords ? "Centered" : "") + "Player" + (bCenteredCoords ? "" : ssprintf("%d", p + 1)));
		SET_XY(player);
		player.m_fLRStartY = player.GetY();
		player.SetXY(player.GetX() + fPlayerX, player.m_fLRStartY + player.m_fLRAddY);
		fPNX[p] = player.GetX();
		this->AddChild(&player);
	}

	GAMESTATE->m_bUpdateNotePressed = true;
}

void PlayerContainer::DrawPrimitives()
{
	//ActorFrame::DrawPrimitives();
	if( GAMESTATE->m_bUpdateNotePressed )
	{
		vector<NotePressed> vNotePressed;

		FOREACH_EnabledPlayer( pn )
			vNotePressed.push_back( NotePressed( pn, GAMESTATE->m_fNotePressedTime[pn] ) );

		stable_sort( vNotePressed.begin(), vNotePressed.end(), greater<NotePressed>( ) );
		m_LastNotePressed = vNotePressed;

		GAMESTATE->m_bUpdateNotePressed = false;
	}

	ASSERT( m_LastNotePressed.size() > 0 )

	for( vector<NotePressed>::iterator mp = m_LastNotePressed.begin(); mp != m_LastNotePressed.end(); ++mp )
		m_Player[mp->pn].Draw();
}

void PlayerContainer::CheckValidRotation( bool& bAllUnderAttack )
{
	bool bUnderAttack = false;
	bool bLeftorRightAttack = false;
	bool bArbitraryRotation = false;
	int iEnabled = 0;

	FOREACH_EnabledPlayer( p )
	{
		// Nearest Degrees
		GAMESTATE->m_PlayerOptions[p].m_fNoteFieldRotationX = NearestDegree(GAMESTATE->m_PlayerOptions[p].m_fNoteFieldRotationX);
		GAMESTATE->m_PlayerOptions[p].m_fNoteFieldRotationY = NearestDegree(GAMESTATE->m_PlayerOptions[p].m_fNoteFieldRotationY);
		GAMESTATE->m_PlayerOptions[p].m_fNoteFieldRotationZ = NearestDegree(GAMESTATE->m_PlayerOptions[p].m_fNoteFieldRotationZ);

		// Type of rotation
		if( FEQ(GAMESTATE->m_PlayerOptions[p].m_fNoteFieldRotationZ, 180.f, 0.001f) )
			bUnderAttack = true;
		else if( FEQ(fabsf(GAMESTATE->m_PlayerOptions[p].m_fNoteFieldRotationZ), 90.f, 0.001f) )
			bLeftorRightAttack = true;
		else if( FEQ(GAMESTATE->m_PlayerOptions[p].m_fNoteFieldRotationZ, 0.f, 0.001f) )
			bArbitraryRotation = true;

		iEnabled++;
	}

	if( bUnderAttack )
	{
		// Under Attack disables other rotations.
		bLeftorRightAttack = false;
		bArbitraryRotation = false;

		bAllUnderAttack = true;
		FOREACH_EnabledPlayer( p )
		{
			// Cancel rotations different than 180.f or 0.f
			if( !FEQ(fmodf(GAMESTATE->m_PlayerOptions[p].m_fNoteFieldRotationZ, 180.f), 0.f, 0.001f) )
				GAMESTATE->m_PlayerOptions[p].m_fNoteFieldRotationZ = 0.f;

			// If everyone enables UA then the whole notefield will rotate
			bAllUnderAttack = bAllUnderAttack && FEQ(GAMESTATE->m_PlayerOptions[p].m_fNoteFieldRotationZ, 180.f, 0.001f);
		}

	}

	if( bLeftorRightAttack )
	{
		// L/R invalidates arbitrary rotation
		bArbitraryRotation = false;

		// Disable L/R if at least one player didn't enable it
		FOREACH_EnabledPlayer( p )
		{
			if( !FEQ(fabsf(GAMESTATE->m_PlayerOptions[p].m_fNoteFieldRotationZ), 90.f, 0.001f) )
			{
				bLeftorRightAttack = false;
				break;
			}
		}
		if( !bLeftorRightAttack )
			FOREACH_EnabledPlayer( p )
				GAMESTATE->m_PlayerOptions[p].m_fNoteFieldRotationZ = 0.f;

	}

	if( bArbitraryRotation )
	{
		// Arbitrary rotation MUST be the same or complementary for 2 players
		if( iEnabled == 2 )
		{
			if( !FEQ(
				fabsf(GAMESTATE->m_PlayerOptions[PLAYER_1].m_fNoteFieldRotationZ),
				fabsf(GAMESTATE->m_PlayerOptions[PLAYER_2].m_fNoteFieldRotationZ),
				0.001f ) )
			{
				bArbitraryRotation = false;
				FOREACH_PlayerNumber( p )
					GAMESTATE->m_PlayerOptions[p].m_fNoteFieldRotationZ = 0.f;
			}
		}
	}
}

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
