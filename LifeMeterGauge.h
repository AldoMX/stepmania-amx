/* LifeMeterGauge - Gauge life meter used in Oni. */

#ifndef LIFEMETERGAUGE_H
#define LIFEMETERGAUGE_H

#include "LifeMeter.h"
#include "RageTypes.h"
#include "Quad.h"


class LifeMeterGauge : public LifeMeter
{
public:
	LifeMeterGauge( const CString& name = "" );
	~LifeMeterGauge() {}

	void Load( PlayerNumber pn );
	void Update( float fDeltaTime );
	void DrawPrimitives();

	void OnSongEnded();
	void ChangeLife( TapNoteScore score, const float fMultiplier );
	void ChangeLife( HoldNoteScore score, const float fMultiplier );
	void OnDancePointsChange() {};
	bool IsInDanger() const { return m_iLife <= m_iDangerThreshold; }
	bool IsHot() const { return m_iLife >= m_iHotThreshold; }
	bool IsFailing() const { return m_iLife <= 0; };

	float GetLife() const;
	void UpdateNonstopLifebar(int cleared, int total, int ProgressiveLifebarDifficulty);

private:
	int m_iLife;
	int m_iFactor;
	int m_iFactorMin;
	int m_iFactorMax;

	int m_iDangerThreshold;
	int m_iHotThreshold;

	struct
	{
		int *Total, *Gauge, *GaugeHold, *Factor, *FactorHold;
	} m_ChangeValues;

	enum
	{
		GAUGE_STATE_NORMAL,
		GAUGE_STATE_HOT,
		GAUGE_STATE_DANGER,
		NUM_GAUGE_STATES
	} m_CurrentState;

	AnimationTiming m_animationTiming;

	Sprite	m_sprFrame[NUM_GAUGE_STATES];
	Sprite	m_sprMainBar[NUM_GAUGE_STATES];
	Sprite	m_sprSecondaryBar[NUM_GAUGE_STATES];
	Sprite	m_sprIndicator[NUM_GAUGE_STATES];
	Sprite	m_sprOverlay[NUM_GAUGE_STATES];
	
	float	m_fIndicatorX;

	Quad	m_quadMainBarMask;
	RectF	m_rectMainBarMask;
	RageMatrix m_matrixMainBarMask;

	Quad	m_quadSecondaryBarMask;
	RectF	m_rectSecondaryBarMask;
	RageMatrix m_matrixSecondaryBarMask;
};


#endif

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * Contains code (c) 2001-2004 Chris Danford.
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
