#include "global.h"
#include "InputHandler_Win32_PIUIO.h"

#include "PrefsManager.h"
#include "RageLog.h"
#include "RageUtil.h"
#include "RageInputDevice.h"

const unsigned short NUM_SENSORS = 4;

InputHandler_Win32_PIUIO::InputHandler_Win32_PIUIO() : m_bLoadedIO(false), m_Instance(NULL)
{
	if( !PREFSMAN->m_bPIUIO )
		return;

	// Load usbio.dll
	m_Instance = LoadLibrary("usbio.dll");
	if( m_Instance == NULL )
		return;

	// TODO: Check if PIUIO is connected, otherwise return
	m_bLoadedIO = true;

	// I/O functions
	m_Init = (PIUIO_VOID*)GetProcAddress(m_Instance, "MK6IO_Init");
	m_InP1 = (PIUIO_IN*)GetProcAddress(m_Instance, "MK6IO_HandleInputP1");
	m_InP2 = (PIUIO_IN*)GetProcAddress(m_Instance, "MK6IO_HandleInputP2");
	m_OutP1 = (PIUIO_OUT*)GetProcAddress(m_Instance, "MK6IO_HandleOutputP1");
	m_OutP2 = (PIUIO_OUT*)GetProcAddress(m_Instance, "MK6IO_HandleOutputP2");
	m_Deinit = (PIUIO_VOID*)GetProcAddress(m_Instance, "MK6IO_Deinit");

	if( PREFSMAN->m_bThreadedInput )
	{
		InputThread.SetName("PIUIO thread");
		InputThread.Create(InputThread_Start, this);
	}
}

InputHandler_Win32_PIUIO::~InputHandler_Win32_PIUIO()
{
	if( InputThread.IsCreated() )
	{
		m_bLoadedIO = false;
		LOG->Trace("Shutting down PIUIO thread ...");
		InputThread.Wait();
		LOG->Trace("PIUIO thread shut down.");
	}

	if( m_Instance )
		FreeLibrary(m_Instance);
}

void InputHandler_Win32_PIUIO::GetDevicesAndDescriptions(vector<InputDevice>& vDevicesOut, vector<CString>& vDescriptionsOut)
{
	if( m_bLoadedIO )
	{
		vDevicesOut.push_back( InputDevice(DEVICE_PIUIO) );
		vDescriptionsOut.push_back( "PIUIO" );
	}
}

int InputHandler_Win32_PIUIO::InputThread_Start( void *p )
{
	((InputHandler_Win32_PIUIO *) p)->InputThreadMain();
	return 0;
}

void InputHandler_Win32_PIUIO::InputThreadMain()
{
	if(!SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_HIGHEST))
		LOG->Warn(werr_ssprintf(GetLastError(), "Failed to set PIUIO thread priority"));

	/* Enable priority boosting. */
	SetThreadPriorityBoost( GetCurrentThread(), FALSE );

	m_Init();

	static const unsigned int bits[NUM_PIUIO_BUTTONS] = {
		0x01,		// PIUIO_P1_UPLEFT
		0x02,		// PIUIO_P1_UPRIGHT
		0x04,		// PIUIO_P1_CENTER
		0x08,		// PIUIO_P1_DOWNLEFT
		0x10,		// PIUIO_P1_DOWNRIGHT
		0x0200,		// PIUIO_TEST
		0x0400,		// PIUIO_COIN1
		0x4000,		// PIUIO_SERVICE
		0x8000,		// PIUIO_CLEAR
		0x010000,	// PIUIO_P2_UPLEFT
		0x020000,	// PIUIO_P2_UPRIGHT
		0x040000,	// PIUIO_P2_CENTER
		0x080000,	// PIUIO_P2_DOWNLEFT
		0x100000,	// PIUIO_P2_DOWNRIGHT
		0x04000000,	// PIUIO_COIN2
	};

	while( m_bLoadedIO )
	{
		int io_in = -1;
		for( unsigned short s=0; s<NUM_SENSORS; s++ )
		{
			m_OutP1(s);
			m_OutP2(s);
			io_in &= m_InP1() | m_InP2() << 16;
		}

		InputDevice id(DEVICE_PIUIO);

		for( int btn = 0; btn < NUM_PIUIO_BUTTONS; btn++ )
		{
			DeviceInput di(id, btn);

			/* If we're in a thread, our timestamp is accurate. */
			if( InputThread.IsCreated() )
				di.ts.Touch();

			ButtonPressed(di, !(io_in & bits[btn]));
		}

		InputHandler::UpdateTimer();
	}

	m_Deinit();
}

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
