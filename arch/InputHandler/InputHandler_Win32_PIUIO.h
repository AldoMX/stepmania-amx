#ifndef INPUT_HANDLER_WIN32_PIUIO_H
#define INPUT_HANDLER_WIN32_PIUIO_H

#include "InputHandler.h"
#include "RageThreads.h"

#include "windows.h"

typedef void PIUIO_VOID();
typedef unsigned short PIUIO_IN();
typedef void PIUIO_OUT(unsigned short value);

class InputHandler_Win32_PIUIO: public InputHandler
{
	RageThread InputThread;
	bool m_bLoadedIO;

	static int InputThread_Start( void *p );
	void InputThreadMain();

	HINSTANCE	m_Instance;
	PIUIO_VOID*	m_Init;
	PIUIO_IN*	m_InP1;
	PIUIO_IN*	m_InP2;
	PIUIO_OUT*	m_OutP1;
	PIUIO_OUT*	m_OutP2;
	PIUIO_VOID*	m_Deinit;

public:
	InputHandler_Win32_PIUIO();
	~InputHandler_Win32_PIUIO();

	void GetDevicesAndDescriptions(vector<InputDevice>& vDevicesOut, vector<CString>& vDescriptionsOut);
};

#endif

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

