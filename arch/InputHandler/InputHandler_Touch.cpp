#include "global.h"
#include "InputHandler_Touch.h"

#ifdef WIN32
#include <windows.h>
#endif

#include "Actor.h"
#include "PrefsManager.h"

InputHandler_Touch::InputHandler_Touch()
{
}

InputHandler_Touch::~InputHandler_Touch()
{
}

void InputHandler_Touch::GetDevicesAndDescriptions(vector<InputDevice>& vDevicesOut, vector<CString>& vDescriptionsOut)
{
#ifndef _WIN32_WINNT_WIN8
	if (PREFSMAN->m_iTouchMode == 0)
		return;
#endif

	vDevicesOut.push_back( InputDevice(DEVICE_TOUCH1) );
	vDescriptionsOut.push_back( "Touch Left" );

	vDevicesOut.push_back( InputDevice(DEVICE_TOUCH2) );
	vDescriptionsOut.push_back( "Touch Right" );
}

void InputHandler_Touch::TouchPress( int id, TouchEvent& event )
{
	map<int, vector<TouchEvent> >::iterator it = m_Events.find(id);
	if( it == m_Events.end() )
		m_Events[id] = vector<TouchEvent>(1, event);
	else
		it->second.push_back(event);

	event.normal->SetHidden(true);
	event.pressed->SetHidden(false);
}

void InputHandler_Touch::TouchRelease( int id )
{
	map<int, vector<TouchEvent> >::iterator it = m_Events.find(id);
	if( it != m_Events.end() )
	{
		for( vector<TouchEvent>::iterator it_event = it->second.begin(); it_event != it->second.end(); it_event++ )
		{
			ButtonPressed( DeviceInput(InputDevice(DEVICE_TOUCH1 + it_event->device), it_event->button), false );
			it_event->normal->SetHidden(false);
			it_event->pressed->SetHidden(true);
		}

		m_Events.erase(id);
	}
}

void InputHandler_Touch::Update( float fDeltaTime )
{
	for( map<int, vector<TouchEvent> >::iterator it = m_Events.begin(); it != m_Events.end(); it++ )
		for( vector<TouchEvent>::iterator it_event = it->second.begin(); it_event != it->second.end(); it_event++ )
			ButtonPressed( DeviceInput(InputDevice(DEVICE_TOUCH1 + it_event->device), it_event->button), true );

	InputHandler::UpdateTimer();
}

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
