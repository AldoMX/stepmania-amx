#include "global.h"
#include "MovieTexture_FFMpeg.h"

#include "RageLog.h"
#include "RageTextureManager.h"
#include "RageUtil.h"
#include "RageTimer.h"
#include "RageFile.h"
#include "RageSurface.h"
#include "PrefsManager.h"

#include <cerrno>

#include "arch/arch_platform.h"

namespace avcodec
{
	extern "C"
	{
		#include <libavformat/avformat.h>
		#include <libavutil/pixdesc.h>
		#include <libswscale/swscale.h>
	}
};

#if defined(_MSC_VER)
	#pragma comment(lib, "avcodec.lib")
	#pragma comment(lib, "avformat.lib")
	#pragma comment(lib, "avutil.lib")
	#pragma comment(lib, "swscale.lib")
#endif

struct AVPixelFormat_t
{
	int bpp;
	unsigned masks[4];
#if (LIBAVCODEC_VERSION_MAJOR >= 54)
	avcodec::AVPixelFormat pf;
#else
	avcodec::PixelFormat pf;
#endif
	bool HighColor;
	bool ByteSwapOnLittleEndian;
} AVPixelFormats[] = {
	{
		32,
		{ 0xFF000000,
		  0x00FF0000,
		  0x0000FF00,
		  0x000000FF },
		avcodec::PIX_FMT_RGBA,
		true,
		true
	},
	{
		32,
		{ 0x0000FF00,
		  0x00FF0000,
		  0xFF000000,
		  0x000000FF },
		avcodec::PIX_FMT_BGRA,
		true,
		true
	},
	{
		/* This format is really ARGB, and is affected by endianness, unlike PIX_FMT_RGB24
		 * and PIX_FMT_BGR24. */
		32,
		{ 0x00FF0000,
		  0x0000FF00,
		  0x000000FF,
		  0xFF000000 },
		avcodec::PIX_FMT_ARGB,
 		true,
		true
	},
	{
		24,
		{ 0xFF0000,
		  0x00FF00,
		  0x0000FF,
		  0x000000 },
		avcodec::PIX_FMT_RGB24,
		false,
		true
	},
	{
		24,
		{ 0x0000FF,
		  0x00FF00,
		  0xFF0000,
		  0x000000 },
		avcodec::PIX_FMT_BGR24,
		false,
		true
	},
	{
		16,
		{ 0x7C00,
		  0x03E0,
		  0x001F,
		  0x0000 },
		avcodec::PIX_FMT_RGB555,
		false,
		false
	},
	{ 0, { 0,0,0,0 }, avcodec::PIX_FMT_NB, true, false }
};

#ifndef WITH_MOV_SUPPORT
typedef RageFile MOVFile;
#else
struct MOVFile
{
	RageFile f;
	int version, length, offset;
	unsigned char table[256];

	bool Open( const CString& filename, int mode = 1 )
	{
		if( !f.Open(filename, mode) )
		{
			LOG->Trace("Error opening \"%s\": %s", filename.c_str(), f.GetError().c_str() );
			return false;
		}

		CString header;
		f.Read(header, 4);
		if( header.Left(3) != "MOV" )
		{
			f.Seek(0);
			return true;
		}

		version = header[3] - '0';
		f.Seek(0x84);
		f.Read((void*)&length, sizeof(int));
		f.Read((void*)&offset, sizeof(int));

		switch( version )
		{
		case 2:
			offset += 0xC0;
			break;

		case 3:
			offset += 0xD0;
			break;

		default:
			version = 0;
			length = 0;
			offset = 0;
			f.Seek(0);
			return true;
		}
		
		unsigned char checksum[4];
		f.Seek(offset - 4);
		f.Read(checksum, sizeof(checksum));
		for( unsigned char c = 0; ; c++ )
		{
			unsigned char value = ReverseByte(c) ^ checksum[0];
			table[value] = c;

			if( c == 0xFF )
				break;
		}
		return true;
	}

	int Read( unsigned char *buf, int size )
	{
		int ret = f.Read( buf, size );

		if( version )
		{
			for( int i = 0; i < size; i++ )
				buf[i] = table[buf[i]];
		}

		return ret;
	}

	int Seek( int pos, int whence )
	{
		switch( whence )
		{
		case SEEK_SET:
			return f.Seek(pos + offset);

		case SEEK_CUR:
		case SEEK_END:
			return f.Seek(pos, whence);

		default:
			ASSERT(0);
			return 0;
		}
	}

	CString GetError() const
	{
		return f.GetError();
	}

	int GetFileSize() const
	{
		if( version )
			return length;

		return f.GetFileSize();
	}

	void Close()
	{
		f.Close();
	}
};
#endif

static void FixLilEndian()
{
#if defined(ENDIAN_LITTLE)
	static bool Initialized = false;
	if( Initialized )
		return;
	Initialized = true;

	for( int i = 0; i < AVPixelFormats[i].bpp; ++i )
	{
		AVPixelFormat_t &pf = AVPixelFormats[i];

		if( !pf.ByteSwapOnLittleEndian )
			continue;

		for( int mask = 0; mask < 4; ++mask)
		{
			int m = pf.masks[mask];
			switch( pf.bpp )
			{
				case 24: m = Swap24(m); break;
				case 32: m = Swap32(m); break;
				default: ASSERT(0);
			}
			pf.masks[mask] = m;
		}
	}
#endif
}

static int FindCompatibleAVFormat( RageDisplay::PixFormat &pixfmt, bool HighColor )
{
	for( int i = 0; AVPixelFormats[i].bpp; ++i )
	{
		AVPixelFormat_t &fmt = AVPixelFormats[i];
		if( fmt.HighColor != HighColor )
			continue;

		pixfmt = DISPLAY->FindPixelFormat( fmt.bpp,
				fmt.masks[0],
				fmt.masks[1],
				fmt.masks[2],
				fmt.masks[3],
				true /* realtime */
				);

		if( pixfmt == RageDisplay::NUM_PIX_FORMATS )
			continue;

		return i;
	}

	return -1;
}

class FFMpeg_Helper
{
public:
	avcodec::AVFormatContext *m_fctx;
	avcodec::AVStream *m_stream;
	avcodec::SwsContext *m_pSwsContext;
	avcodec::AVIOContext *m_avioContext;

	bool GetNextTimestamp;
	float CurrentTimestamp, Last_IP_Timestamp;
	int FrameNumber;
	float LastFrameDelay;
	float pts;

	avcodec::AVPacket pkt;
	int current_packet_offset;

	avcodec::AVFrame frame;

	FFMpeg_Helper();
	~FFMpeg_Helper();
	int GetFrame();
	void Init();
	float GetTimestamp() const;

	CString OpenCodec( MovieTexture_FFMpeg* movie );

private:
	/* 0 = no EOF
	 * 1 = EOF from ReadPacket
	 * 2 = EOF from ReadPacket and DecodePacket */
	int eof;

	int ReadPacket();
	int DecodePacket();
	float TimestampOffset;
};

FFMpeg_Helper::FFMpeg_Helper()
{
	m_fctx = NULL;
	m_stream = NULL;
	m_pSwsContext = NULL;
	m_avioContext = NULL;

	current_packet_offset = -1;
	Init();
}

FFMpeg_Helper::~FFMpeg_Helper()
{
	if( current_packet_offset != -1 )
	{
		avcodec::av_free_packet( &pkt );
		current_packet_offset = -1;
	}

	if( m_pSwsContext != NULL )
		avcodec::sws_freeContext(m_pSwsContext);

	if( m_avioContext != NULL )
	{
		static_cast<MOVFile*>(m_avioContext->opaque)->Close();
		avcodec::av_free(m_avioContext->opaque);
		avcodec::av_free(m_avioContext->buffer);
		avcodec::av_free(m_avioContext);
	}
}

void FFMpeg_Helper::Init()
{
	eof = 0;
	GetNextTimestamp = true;
	CurrentTimestamp = 0, Last_IP_Timestamp = 0;
	LastFrameDelay = 0;
	pts = -1;
	FrameNumber = -1; /* decode one frame and you're on the 0th */
	TimestampOffset = 0;

	if( current_packet_offset != -1 )
	{
		avcodec::av_free_packet( &pkt );
		current_packet_offset = -1;
	}
}

/* Read until we get a frame, EOF or error.  Return -1 on error, 0 on EOF, 1 if we have a frame. */
int FFMpeg_Helper::GetFrame()
{
	while( 1 )
	{
		int ret = DecodePacket();
		if( ret == 1 )
			break;
		if( ret == -1 )
			return -1;
		if( ret == 0 && eof > 0 )
			return 0; /* eof */

		ASSERT( ret == 0 );
		ret = ReadPacket();
		if( ret < 0 )
			return ret; /* error */
	}

	++FrameNumber;

	if( FrameNumber == 1 )
	{
		/* Some videos start with a timestamp other than 0.  I think this is used
		 * when audio starts before the video.  We don't want to honor that, since
		 * the DShow renderer doesn't and we don't want to break sync compatibility.
		 *
		 * Look at the second frame.  (If we have B-frames, the first frame will be an
		 * I-frame with the timestamp of the next P-frame, not its own timestamp, and we
		 * want to ignore that and look at the next B-frame.) */
		const float expect = LastFrameDelay;
		const float actual = CurrentTimestamp;
		if( actual - expect > 0 )
		{
			LOG->Trace("Expect %f, got %f -> %f", expect, actual, actual - expect );
			TimestampOffset = actual - expect;
		}
	}

	return 1;
}

float FFMpeg_Helper::GetTimestamp() const
{
	/* The first frame always has a timestamp of 0. */
	if( FrameNumber == 0 )
		return 0;

	return CurrentTimestamp - TimestampOffset;
}

/* Read a packet.  Return -1 on error, 0 on EOF, 1 on OK. */
int FFMpeg_Helper::ReadPacket()
{
	if( eof > 0 )
		return 0;

	while( 1 )
	{
		//CHECKPOINT;
		if( current_packet_offset != -1 )
		{
			current_packet_offset = -1;
			avcodec::av_free_packet( &pkt );
		}

		int ret = avcodec::av_read_frame( m_fctx, &pkt );
		/* XXX: why is avformat returning AVERROR_NOMEM on EOF? */
		if( ret < 0 )
		{
			/* EOF. */
			eof = 1;
			pkt.size = 0;

			return 0;
		}

		if( pkt.stream_index == m_stream->index )
		{
			current_packet_offset = 0;
			return 1;
		}

		/* It's not for the video stream; ignore it. */
		avcodec::av_free_packet( &pkt );
	}
}


/* Decode data from the current packet.  Return -1 on error, 0 if the packet is finished,
 * and 1 if we have a frame (we may have more data in the packet). */
int FFMpeg_Helper::DecodePacket()
{
	if( eof == 0 && current_packet_offset == -1 )
		return 0; /* no packet */

	while( eof == 1 || (eof == 0 && current_packet_offset < pkt.size) )
	{
		if ( GetNextTimestamp )
		{
			if (pkt.dts != int64_t(AV_NOPTS_VALUE))
				pts = (float)pkt.dts * m_stream->time_base.num / m_stream->time_base.den;
			else
				pts = -1;

			GetNextTimestamp = false;
		}

		/* If we have no data on the first frame, just return EOF; passing an empty packet
		 * to avcodec_decode_video in this case is crashing it.  However, passing an empty
		 * packet is normal with B-frames, to flush.  This may be unnecessary in newer
		 * versions of avcodec, but I'm waiting until a new stable release to upgrade. */
		if( pkt.size == 0 && FrameNumber == -1 )
			return 0; /* eof */

		int got_frame;
		//CHECKPOINT;
		/* Hack: we need to send size = 0 to flush frames at the end, but we have
		 * to give it a buffer to read from since it tries to read anyway. */
		static uint8_t dummy[FF_INPUT_BUFFER_PADDING_SIZE] = { 0 };
		int len = avcodec::avcodec_decode_video2( m_stream->codec, &frame, &got_frame, &pkt );
		//CHECKPOINT;

		if (len < 0)
		{
			LOG->Warn("avcodec_decode_video: %i", len);
			return -1; // XXX
		}

		current_packet_offset += len;

		if (!got_frame)
		{
			if( eof == 1 )
				eof = 2;
			continue;
		}

		GetNextTimestamp = true;

		if (pts != -1)
		{
			CurrentTimestamp = pts;
		}
		else
		{
			/* If the timestamp is zero, this frame is to be played at the
			 * time of the last frame plus the length of the last frame. */
			CurrentTimestamp += LastFrameDelay;
		}

		/* Length of this frame: */
		LastFrameDelay = (float)m_stream->codec->time_base.num / m_stream->codec->time_base.den;
		LastFrameDelay += frame.repeat_pict * (LastFrameDelay * 0.5f);

		return 1;
	}

	return 0; /* packet done */
}

void MovieTexture_FFMpeg::ConvertFrame()
{
	ASSERT_M( m_ImageWaiting == FRAME_DECODED, ssprintf("%i", m_ImageWaiting ) );

	avcodec::AVPicture pict;
	pict.data[0] = m_img->pixels;
	pict.linesize[0] = m_img->pitch;

	if( decoder->m_pSwsContext == NULL )
	{
		decoder->m_pSwsContext = sws_getCachedContext(
			decoder->m_pSwsContext,
			m_iSourceWidth,
			m_iSourceHeight,
			decoder->m_stream->codec->pix_fmt,
			m_iImageWidth,
			m_iImageHeight,
			AVPixelFormats[m_AVTexfmt].pf,
			SWS_BICUBIC,
			NULL,
			NULL,
			NULL
		);

		if( decoder->m_pSwsContext == NULL )
		{
			LOG->Warn("Cannot initialize sws conversion context for (%d,%d) %d->%d", m_iSourceWidth, m_iSourceHeight, decoder->m_stream->codec->pix_fmt, m_AVTexfmt);
			return;
		}
	}
	avcodec::sws_scale( decoder->m_pSwsContext, decoder->frame.data, decoder->frame.linesize, 0, m_iSourceHeight, pict.data, pict.linesize );

	m_ImageWaiting = FRAME_WAITING;
}

MovieTexture_FFMpeg::MovieTexture_FFMpeg( RageTextureID ID ):
	RageMovieTexture( ID ),
	m_BufferFinished( "BufferFinished", 0 )
{
	LOG->Trace( "MovieTexture_FFMpeg::MovieTexture_FFMpeg(%s)", ID.filename.c_str() );

	FixLilEndian();

	decoder = new FFMpeg_Helper;

	m_uTexHandle = 0;
	m_bLoop = IsMPEG(ID.filename) ? ID.filename.find("(loop)") != CString::npos : ID.filename.find("(noloop)") == CString::npos;
    m_State = DECODER_QUIT; /* it's quit until we call StartThread */
	m_img = NULL;
	m_ImageWaiting = FRAME_NONE;
	m_Rate = 1;
	m_bWantRewind = false;
	m_Clock = 0;
	m_FrameSkipMode = false;
	m_bThreaded = PREFSMAN->m_bThreadedMovieDecode;
}

CString MovieTexture_FFMpeg::Init()
{
	CString sError = CreateDecoder();
	if( sError != "" )
		return sError;

	LOG->Trace("Bitrate: %i", decoder->m_stream->codec->bit_rate );
	LOG->Trace("Codec pixel format: %s", avcodec::av_get_pix_fmt_name(decoder->m_stream->codec->pix_fmt) );

	/* Decode one frame, to guarantee that the texture is drawn when this function returns. */
	int ret = decoder->GetFrame();
	if( ret == -1 )
		return ssprintf( "%s: error getting first frame", GetID().filename.c_str() );
	if( ret == 0 )
	{
		/* There's nothing there. */
		return ssprintf( "%s: EOF getting first frame", GetID().filename.c_str() );
	}

	m_ImageWaiting = FRAME_DECODED;

	CreateTexture();
	LOG->Trace( "Resolution: %ix%i (%ix%i, %ix%i)",
			m_iSourceWidth, m_iSourceHeight,
			m_iImageWidth, m_iImageHeight, m_iTextureWidth, m_iTextureHeight );
	LOG->Trace( "Texture pixel format: %s", DISPLAY->PixFormatToString((RageDisplay::PixFormat)m_AVTexfmt).c_str() );

	CreateFrameRects();

	ConvertFrame();
	UpdateFrame();

	//CHECKPOINT;

	StartThread();

	return "";
}

MovieTexture_FFMpeg::~MovieTexture_FFMpeg()
{
	StopThread();
	DestroyDecoder();
	DestroyTexture();

	delete decoder;
}


static CString averr_ssprintf( int err, const char *fmt, ... )
{
	ASSERT( err < 0 );

	va_list     va;
	va_start(va, fmt);
	CString s = vssprintf( fmt, va );
	va_end(va);

	CString Error;
	switch( err )
	{
	case AVERROR_BSF_NOT_FOUND:			Error = "Bitstream filter not found"; break;
	case AVERROR_DECODER_NOT_FOUND:		Error = "Decoder not found"; break;
	case AVERROR_DEMUXER_NOT_FOUND:		Error = "Demuxer not found"; break;
	case AVERROR_ENCODER_NOT_FOUND:		Error = "Encoder not found"; break;
	case AVERROR_EOF:					Error = "End of file"; break;
	case AVERROR_EXIT:					Error = "Immediate exit was requested; the called function should not be restarted"; break;
	case AVERROR_FILTER_NOT_FOUND:		Error = "Filter not found"; break;
	case AVERROR_INVALIDDATA:			Error = "Invalid data found when processing input"; break;
	case AVERROR_MUXER_NOT_FOUND:		Error = "Muxer not found"; break;
	case AVERROR_OPTION_NOT_FOUND:		Error = "Option not found"; break;
	case AVERROR_PATCHWELCOME:			Error = "Not yet implemented in FFmpeg, patches welcome"; break;
	case AVERROR_PROTOCOL_NOT_FOUND:	Error = "Protocol not found"; break;
	case AVERROR_STREAM_NOT_FOUND:		Error = "Stream not found"; break;
	default: Error = ssprintf( "unknown error %i", err ); break;
	}

	return s + " (" + Error + ")";
}

static int AVIO_MOVFile_ReadPacket( void *file, uint8_t *buf, int buf_size )
{
	return static_cast<MOVFile*>(file)->Read(buf, buf_size);
}

static int64_t AVIO_MOVFile_Seek( void *file, int64_t offset, int whence )
{
	switch( whence )
	{
	case AVSEEK_SIZE:
		return static_cast<MOVFile*>(file)->GetFileSize();

	case SEEK_SET:
	case SEEK_CUR:
	case SEEK_END:
		return static_cast<MOVFile*>(file)->Seek( (int)offset, whence );

	default:
		LOG->Trace("Error: unsupported seek whence: %d", whence);
		return -1;
	}
}

CString FFMpeg_Helper::OpenCodec( MovieTexture_FFMpeg* movie )
{
	this->Init();

	ASSERT( m_stream != NULL );
	if( m_stream->codec->codec )
		avcodec::avcodec_close( m_stream->codec );

	avcodec::AVCodec *pCodec = avcodec::avcodec_find_decoder( m_stream->codec->codec_id );
	if( pCodec == NULL )
		return ssprintf( "Couldn't find decoder %i", m_stream->codec->codec_id );

	m_stream->codec->workaround_bugs   = 1;
	m_stream->codec->idct_algo         = FF_IDCT_AUTO;
	m_stream->codec->error_concealment = 3;

	if( pCodec->capabilities & CODEC_CAP_DR1 )
		m_stream->codec->flags |= CODEC_FLAG_EMU_EDGE;

	LOG->Trace("Opening codec %s", pCodec->name );

	int ret = avcodec::avcodec_open2( m_stream->codec, pCodec, NULL );
	if( ret < 0 )
		return CString( averr_ssprintf(ret, "Couldn't open codec \"%s\"", pCodec->name) );
	ASSERT( m_stream->codec->codec != NULL );

	return "";
}

const int STEPMANIA_FFMPEG_BUFFER_SIZE = 4096;

void MovieTexture_FFMpeg::RegisterProtocols()
{
	static bool Done = false;
	if( Done )
		return;
	Done = true;

	avcodec::av_register_all();
	avcodec::avcodec_register_all();
}

CString MovieTexture_FFMpeg::CreateDecoder()
{
	RegisterProtocols();

	decoder->m_fctx = avcodec::avformat_alloc_context();
	if( !decoder->m_fctx )
		return "AVCodec: Couldn't allocate context";

	const CString& sFile = GetID().filename;
	MOVFile* mov = (MOVFile*)avcodec::av_mallocz(sizeof(MOVFile));
	if( !mov->Open(sFile, RageFile::READ) )
	{
		CString error = ssprintf("MovieDecoder_FFMpeg: Error opening \"%s\": %s", sFile.c_str(), mov->GetError().c_str() );
		avcodec::av_free(mov);
		return error;
	}

	unsigned char* buffer = (unsigned char*)avcodec::av_mallocz(STEPMANIA_FFMPEG_BUFFER_SIZE);
	decoder->m_avioContext = avcodec::avio_alloc_context(buffer, STEPMANIA_FFMPEG_BUFFER_SIZE, 0, mov, AVIO_MOVFile_ReadPacket, NULL, AVIO_MOVFile_Seek);
	decoder->m_fctx->pb = decoder->m_avioContext;

	int ret = avcodec::avformat_open_input( &decoder->m_fctx, sFile.c_str(), NULL, NULL );
	if( ret < 0 )
		return CString( averr_ssprintf(ret, "AVCodec: Couldn't open \"%s\"", sFile.c_str()) );

	ret = avcodec::avformat_find_stream_info( decoder->m_fctx, NULL );
	if( ret < 0 )
		return CString( averr_ssprintf(ret, "AVCodec (%s): Couldn't find codec parameters", sFile.c_str()) );

	int stream_idx = avcodec::av_find_best_stream( decoder->m_fctx, avcodec::AVMEDIA_TYPE_VIDEO, -1, -1, NULL, 0 );
	if ( stream_idx < 0 ||
		static_cast<unsigned int>(stream_idx) >= decoder->m_fctx->nb_streams ||
		decoder->m_fctx->streams[stream_idx] == NULL )
		return "Couldn't find any video streams";
	decoder->m_stream = decoder->m_fctx->streams[stream_idx];

	if( decoder->m_stream->codec->codec_id == avcodec::CODEC_ID_NONE )
		return ssprintf( "Unsupported codec %08x", decoder->m_stream->codec->codec_tag );

	CString sError = decoder->OpenCodec(this);
	if( !sError.empty() )
		return ssprintf( "AVCodec (%s): %s", sFile.c_str(), sError.c_str() );


	return "";
}

/* Delete the decoder.  The decoding thread must be stopped. */
void MovieTexture_FFMpeg::DestroyDecoder()
{
	if( decoder->m_stream )
	{
		if( decoder->m_stream->codec )
			avcodec::avcodec_close( decoder->m_stream->codec );
		decoder->m_stream = NULL;
	}

	if( decoder->m_fctx )
		avcodec::avformat_close_input( &decoder->m_fctx );
}

/* Delete the surface and texture.  The decoding thread must be stopped, and this
 * is normally done after destroying the decoder. */
void MovieTexture_FFMpeg::DestroyTexture()
{
	if( m_img )
	{
		delete m_img;
		m_img=NULL;
	}
	if(m_uTexHandle)
	{
		DISPLAY->DeleteTexture( m_uTexHandle );
		m_uTexHandle = 0;
	}
}

void MovieTexture_FFMpeg::CreateTexture()
{
    if( m_uTexHandle )
        return;

	//CHECKPOINT;

	RageTextureID actualID = GetID();
	actualID.iAlphaBits = 0;

	/* Cap the max texture size to the hardware max. */
	actualID.iMaxSize = min( actualID.iMaxSize, DISPLAY->GetMaxTextureSize() );

	m_iSourceWidth  = decoder->m_stream->codec->width;
	m_iSourceHeight = decoder->m_stream->codec->height;

	/* image size cannot exceed max size */
	m_iImageWidth = min( m_iSourceWidth, actualID.iMaxSize );
	m_iImageHeight = min( m_iSourceHeight, actualID.iMaxSize );

	/* Texture dimensions need to be a power of two; jump to the next. */
	m_iTextureWidth = power_of_two(m_iImageWidth);
	m_iTextureHeight = power_of_two(m_iImageHeight);

	/* Bogus assignment to shut gcc up. */
    RageDisplay::PixFormat pixfmt = RageDisplay::NUM_PIX_FORMATS;
	bool PreferHighColor = (TEXTUREMAN->GetPrefs().m_iMovieColorDepth == 32);
	m_AVTexfmt = FindCompatibleAVFormat( pixfmt, PreferHighColor );

	if( m_AVTexfmt == -1 )
		m_AVTexfmt = FindCompatibleAVFormat( pixfmt, !PreferHighColor );

	if( m_AVTexfmt == -1 )
	{
		/* No dice.  Use the first avcodec format of the preferred bit depth,
		 * and let the display system convert. */
		for( m_AVTexfmt = 0; AVPixelFormats[m_AVTexfmt].bpp; ++m_AVTexfmt )
			if( AVPixelFormats[m_AVTexfmt].HighColor == PreferHighColor )
				break;
		ASSERT( AVPixelFormats[m_AVTexfmt].bpp );

		switch( TEXTUREMAN->GetPrefs().m_iMovieColorDepth )
		{
		default:
			ASSERT(0);
		case 16:
			if( DISPLAY->SupportsTextureFormat(RageDisplay::FMT_RGB5) )
				pixfmt = RageDisplay::FMT_RGB5;
			else
				pixfmt = RageDisplay::FMT_RGBA4; // everything supports RGBA4

			break;

		case 32:
			if( DISPLAY->SupportsTextureFormat(RageDisplay::FMT_RGB8) )
				pixfmt = RageDisplay::FMT_RGB8;
			else if( DISPLAY->SupportsTextureFormat(RageDisplay::FMT_RGBA8) )
				pixfmt = RageDisplay::FMT_RGBA8;
			else if( DISPLAY->SupportsTextureFormat(RageDisplay::FMT_RGB5) )
				pixfmt = RageDisplay::FMT_RGB5;
			else
				pixfmt = RageDisplay::FMT_RGBA4; // everything supports RGBA4
			break;
		}
	}

	if( !m_img )
	{
		const AVPixelFormat_t& pfd = AVPixelFormats[m_AVTexfmt];

		LOG->Trace("format %i, %08x %08x %08x %08x",
			pfd.bpp, pfd.masks[0], pfd.masks[1], pfd.masks[2], pfd.masks[3]);

		m_img = CreateSurface( m_iTextureWidth, m_iTextureHeight, pfd.bpp,
			pfd.masks[0], pfd.masks[1], pfd.masks[2], pfd.masks[3] );
	}

    m_uTexHandle = DISPLAY->CreateTexture( pixfmt, m_img, false );
}

/* Handle decoding for a frame.  Return true if a frame was decoded, false if not
 * (due to pause, EOF, etc).  If true is returned, we'll be in FRAME_DECODED. */
bool MovieTexture_FFMpeg::DecodeFrame()
{
	ASSERT_M( m_ImageWaiting == FRAME_NONE, ssprintf("%i", m_ImageWaiting) );

	if( m_State == DECODER_QUIT )
		return false;
	//CHECKPOINT;

	/* Read a frame. */
	int ret = decoder->GetFrame();
	if( ret == -1 )
		return false;

	if( m_bWantRewind && decoder->GetTimestamp() == 0 )
		m_bWantRewind = false; /* ignore */

	if( ret == 0 )
	{
		/* EOF. */
		if( !m_bLoop )
			return false;

		LOG->Trace( "File \"%s\" looping", GetID().filename.c_str() );
		m_bWantRewind = true;
	}

	if( m_bWantRewind )
	{
		m_bWantRewind = false;

		/* When resetting the clock, set it back by the length of the last frame,
		 * so it has a proper delay. */
		float fDelay = decoder->LastFrameDelay;

		/* Restart. */
		avcodec::av_seek_frame( decoder->m_fctx, -1, 0, 0 );
		decoder->Init();
		m_Clock = -fDelay;
		return false;
	}

	/* We got a frame. */
	m_ImageWaiting = FRAME_DECODED;

	return true;
}

/*
 * Call when m_ImageWaiting == FRAME_DECODED.
 * Returns:
 *  == 0 if the currently decoded frame is ready to be displayed
 *   > 0 (seconds) if it's not yet time to display;
 *  == -1 if we're behind and the frame should be skipped
 */
float MovieTexture_FFMpeg::CheckFrameTime()
{
	ASSERT_M( m_ImageWaiting == FRAME_DECODED, ssprintf("%i", m_ImageWaiting) );

	if( m_Rate == 0 )
		return 1;	// "a long time until the next frame"

	const float Offset = (decoder->GetTimestamp() - m_Clock) / m_Rate;

	/* If we're ahead, we're decoding too fast; delay. */
	if( Offset > 0.00001f )
	{
		if( m_FrameSkipMode )
		{
			/* We're caught up; stop skipping frames. */
			LOG->Trace( "stopped skipping frames" );
			m_FrameSkipMode = false;
		}
		return Offset;
	}

	/*
	 * We're behind by -Offset seconds.
	 *
	 * If we're just slightly behind, don't worry about it; we'll simply
	 * not sleep, so we'll move as fast as we can to catch up.
	 *
	 * If we're far behind, we're short on CPU.  Skip texture updates; this
	 * is a big bottleneck on many systems.
	 *
	 * If we hit a threshold, start skipping frames via #1.  If we do that,
	 * don't stop once we hit the threshold; keep doing it until we're fully
	 * caught up.
	 *
	 * We should try to notice if we simply don't have enough CPU for the video;
	 * it's better to just stay in frame skip mode than to enter and exit it
	 * constantly, but we don't want to do that due to a single timing glitch.
	 */
	const float FrameSkipThreshold = 0.5f;

	if( -Offset >= FrameSkipThreshold && !m_FrameSkipMode )
	{
		LOG->Trace( "(%s) Time is %f, and the movie is at %f.  Entering frame skip mode.",
			GetID().filename.c_str(), m_Clock, decoder->GetTimestamp());
		m_FrameSkipMode = true;
	}

	if( m_FrameSkipMode && decoder->m_stream->codec->frame_number % 2 )
		return -1; /* skip */

	return 0;
}

void MovieTexture_FFMpeg::DiscardFrame()
{
	ASSERT_M( m_ImageWaiting == FRAME_DECODED, ssprintf("%i", m_ImageWaiting) );
	m_ImageWaiting = FRAME_NONE;
}

void MovieTexture_FFMpeg::DecoderThread()
{
#if defined(_WINDOWS)
	/* Windows likes to boost priority when processes come out of a wait state.  We don't
	 * want that, since it'll result in us having a small priority boost after each movie
	 * frame, resulting in skips in the gameplay thread. */
	if( !SetThreadPriorityBoost(GetCurrentThread(), TRUE) && GetLastError() != ERROR_CALL_NOT_IMPLEMENTED )
		LOG->Warn( werr_ssprintf(GetLastError(), "SetThreadPriorityBoost failed") );
#endif

	//CHECKPOINT;

	while( m_State != DECODER_QUIT )
	{
		if( m_ImageWaiting == FRAME_NONE )
			DecodeFrame();

		/* If we still have no frame, we're at EOF and we didn't loop. */
		if( m_ImageWaiting != FRAME_DECODED )
		{
			usleep( 10000 );
			continue;
		}

		const float fTime = CheckFrameTime();
		if( fTime == -1 )	// skip frame
		{
			DiscardFrame();
		}
		else if( fTime > 0 )		// not time to decode a new frame yet
		{
			/* This needs to be relatively short so that we wake up quickly
			 * from being paused or for changes in m_Rate. */
			usleep( 10000 );
		}
		else // fTime == 0
		{
			{
				/* The only reason m_BufferFinished might be non-zero right now (before
				 * ConvertFrame()) is if we're quitting. */
				int n = m_BufferFinished.GetValue();
				ASSERT_M( n == 0 || m_State == DECODER_QUIT, ssprintf("%i, %i", n, m_State) );
			}
			ConvertFrame();

			/* We just went into FRAME_WAITING.  Don't actually check; the main thread
			 * will change us back to FRAME_NONE without locking, and poke m_BufferFinished.
			 * Don't time out on this; if a new screen has started loading, this might not
			 * return for a while. */
			m_BufferFinished.Wait( false );

			/* If the frame wasn't used, then we must be shutting down. */
			ASSERT_M( m_ImageWaiting == FRAME_NONE || m_State == DECODER_QUIT, ssprintf("%i, %i", m_ImageWaiting, m_State) );
		}
	}
	//CHECKPOINT;
}

void MovieTexture_FFMpeg::Update( float fDeltaTime )
{
	/* We might need to decode more than one frame per update.  However, there
	 * have been bugs in ffmpeg that cause it to not handle EOF properly, which
	 * could make this never return, so let's play it safe. */
	int iMax = 4;
	while( --iMax )
	{
		if( !m_bThreaded )
		{
			/* If we don't have a frame decoded, decode one. */
			if( m_ImageWaiting == FRAME_NONE )
				DecodeFrame();

			/* If we have a frame decoded, see if it's time to display it. */
			if( m_ImageWaiting == FRAME_DECODED )
			{
				float fTime = CheckFrameTime();
				if( fTime > 0 )
					return;
				else if( fTime == -1 )
					DiscardFrame();
				else
					ConvertFrame();
			}
		}

		/* Note that if there's an image waiting, we *must* signal m_BufferFinished, or
		* the decoder thread may sit around waiting for it, even though Pause and Play
		* calls, causing the clock to keep running. */
		if( m_ImageWaiting != FRAME_WAITING )
			return;
		//CHECKPOINT;

		UpdateFrame();

		if( m_bThreaded )
			m_BufferFinished.Post();
	}

	LOG->MapLog( "ffmpeg_looping", "MovieTexture_FFMpeg::Update looping" );
}

/* Call from the main thread when m_ImageWaiting == FRAME_WAITING to update the
 * texture.  Sets FRAME_NONE.  Does not signal m_BufferFinished. */
void MovieTexture_FFMpeg::UpdateFrame()
{
	ASSERT_M( m_ImageWaiting == FRAME_WAITING, ssprintf("%i", m_ImageWaiting) );

    /* Just in case we were invalidated: */
    CreateTexture();

	// TODO - Aldo_MX: 16-bit Movie color depth doesn't display videos :/
	// m_img is correct, the issue may be UpdateTexture()

	//CHECKPOINT;
	DISPLAY->UpdateTexture(
        m_uTexHandle,
        m_img,
        0, 0,
        m_iImageWidth, m_iImageHeight );
    //CHECKPOINT;

	m_ImageWaiting = FRAME_NONE;
}

void MovieTexture_FFMpeg::Reload()
{
}

void MovieTexture_FFMpeg::StartThread()
{
	ASSERT( m_State == DECODER_QUIT );
	m_State = DECODER_RUNNING;
	m_DecoderThread.SetName( ssprintf("MovieTexture_FFMpeg(%s)", GetID().filename.c_str()) );

	if( m_bThreaded )
		m_DecoderThread.Create( DecoderThread_start, this );
}

void MovieTexture_FFMpeg::StopThread()
{
	if( !m_DecoderThread.IsCreated() )
		return;

	LOG->Trace("Shutting down decoder thread ...");

	m_State = DECODER_QUIT;

	/* Make sure we don't deadlock waiting for m_BufferFinished. */
	m_BufferFinished.Post();
	//CHECKPOINT;
	m_DecoderThread.Wait();
	//CHECKPOINT;

	m_ImageWaiting = FRAME_NONE;

	/* Clear the above post, if the thread didn't. */
	m_BufferFinished.TryWait();

	LOG->Trace("Decoder thread shut down.");
}

void MovieTexture_FFMpeg::SetPosition( float fSeconds )
{
    ASSERT( m_State != DECODER_QUIT );

	/* We can reset to 0, but I don't think this API supports fast seeking
	 * yet.  I don't think we ever actually seek except to 0 right now,
	 * anyway. XXX */
	if( fSeconds != 0 )
	{
		LOG->Warn( "MovieTexture_FFMpeg::SetPosition(%f): non-0 seeking unsupported; ignored", fSeconds );
		return;
	}

	LOG->Trace( "Seek to %f", fSeconds );
	m_bWantRewind = true;
}

/* This is used to decode data. */
void MovieTexture_FFMpeg::DecodeSeconds( float fSeconds )
{
	m_Clock += fSeconds * m_Rate;

	/* If we're not threaded, we want to be sure to decode any new frames now,
	 * and not on the next frame.  Update() may have already been called for this
	 * frame; call it again to be sure. */
	Update(0);
}

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * Contains code (c) 2003-2004 Glenn Maynard.
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
