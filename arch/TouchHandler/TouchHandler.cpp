#include "global.h"
#include "TouchHandler.h"

#include "Game.h"
#include "GameState.h"
#include "RageLog.h"
#include "SDL_utils.h"
#include "ThemeManager.h"

TouchHandler::TouchHandler( int touch_mode, InputHandler_Touch* input_handler ) :
	m_pCurrentParams(NULL),
	m_TouchMode((TouchMode)touch_mode),
	handler(input_handler)
{
	FOREACH_PlayerNumber( pn )
		m_ControlContainer[pn] = NULL;
}

TouchHandler::~TouchHandler()
{
	// TEXTUREMAN already frees memory from actors (although raises a LEAK warning).
	FOREACH_PlayerNumber( pn )
		m_Controls[pn].clear();

	SAFE_DELETE(handler);
}

void TouchHandler::LoadControls()
{
	UnloadControls();

	if( m_TouchMode == TOUCH_DISABLED )
		return;

	const Game& game = *GAMESTATE->GetCurrentGame();
	const CString sGameName = Capitalize(game.m_szName);
	const CString sFolder = ssprintf("TouchControl\\%s", sGameName.c_str());
	const CString sClassName = ssprintf("TouchControl%s", sGameName.c_str());

	FOREACH_PlayerNumber( pn )
	{
		m_ControlContainer[pn] = new ActorFrame;
		m_ControlContainer[pn]->SetName( ssprintf("TouchControlP%d", pn+1) );
		m_ControlContainer[pn]->SetDrawOrder( INT_MAX );

		for( int b = 0; b < game.m_iButtonsPerController; b++ )
		{
			const CString sButtonName = game.m_szButtonNames[b];
			if( sButtonName == "Start" )
				break;

			m_Controls[pn].push_back(new TouchControl);

			FOREACH_TouchState(ts)
			{
				Sprite& sprite = m_Controls[pn][b]->sprite[ts];
				sprite.SetName( sButtonName );
				sprite.Load( THEME->GetPathToG(ssprintf("%s\\%s %s", sFolder.c_str(), sButtonName.c_str(), TouchStateToString(ts).c_str())) );
				sprite.SetX( THEME->GetMetricF(sClassName, ssprintf("%sX", sButtonName.c_str())) );
				sprite.SetY( THEME->GetMetricF(sClassName, ssprintf("%sY", sButtonName.c_str())) );
				sprite.SetWidth( THEME->GetMetricF(sClassName, ssprintf("%sWidth", sButtonName.c_str())) );
				sprite.SetHeight( THEME->GetMetricF(sClassName, ssprintf("%sHeight", sButtonName.c_str())) );
				sprite.SetHidden( ts == TOUCH_STATE_PRESSED );

				m_ControlContainer[pn]->AddChild(&sprite);
			}
		}
	}
}

void TouchHandler::UnloadControls()
{
	FOREACH_PlayerNumber( pn )
	{
		for( vector<TouchControl*>::iterator it = m_Controls[pn].begin(); it != m_Controls[pn].end(); it++ )
			SAFE_DELETE(*it);
		m_Controls[pn].clear();

		SAFE_DELETE(m_ControlContainer[pn]);
	}
}

void TouchHandler::SetControlsAsChildren( ActorFrame* frame )
{
	if( m_TouchMode == TOUCH_DISABLED )
		return;

	float adjust_x = (float)GAMESTATE->GetDisplayWidth() / SCREEN_WIDTH,
		adjust_y = (float)GAMESTATE->GetDisplayHeight() / SCREEN_HEIGHT;

	if( adjust_x != m_PixelAdjust.x || adjust_y != m_PixelAdjust.y )
	{
		m_PixelAdjust.x = adjust_x;
		m_PixelAdjust.y = adjust_y;

		FOREACH_PlayerNumber(pn)
		{
			ActorFrame& container = *m_ControlContainer[pn];
			float fZoom = THEME->GetMetricF("Common", container.GetName() + "Zoom");
			container.SetX( THEME->GetMetricF("Common", container.GetName() + "X") / adjust_x );
			container.SetY( THEME->GetMetricF("Common", container.GetName() + "Y") / adjust_y );
			container.SetZoomX( fZoom * GAMESTATE->GetDPIZoomX() / adjust_x );
			container.SetZoomY( fZoom * GAMESTATE->GetDPIZoomY() / adjust_y );

			RectF rectContainer;
			rectContainer.left = container.GetDestX() - container.GetZoomedWidth()/2;
			rectContainer.top = container.GetDestY() - container.GetZoomedHeight()/2;
			rectContainer.right = container.GetDestX() + container.GetZoomedWidth()/2;
			rectContainer.bottom = container.GetDestY() + container.GetZoomedHeight()/2;

			for( vector<TouchControl*>::iterator it = m_Controls[pn].begin(); it != m_Controls[pn].end(); it++ )
			{
				Sprite& sprite = (*it)->sprite[TOUCH_STATE_NORMAL];
				RectF& rect = (*it)->position;

				rect.left = fZoom * GAMESTATE->GetDPIZoomX() * (sprite.GetX() - sprite.GetUnzoomedWidth()/2) / adjust_x;
				rect.top = fZoom * GAMESTATE->GetDPIZoomY() * (sprite.GetY() - sprite.GetUnzoomedHeight()/2) / adjust_y;
				rect.right = fZoom * GAMESTATE->GetDPIZoomX() * (sprite.GetX() + sprite.GetUnzoomedWidth()/2) / adjust_x;
				rect.bottom = fZoom * GAMESTATE->GetDPIZoomY() * (sprite.GetY() + sprite.GetUnzoomedHeight()/2) / adjust_y;
				rect += rectContainer;
			}

			frame->AddChild(&container);
		}
	}
	else
	{
		FOREACH_PlayerNumber( pn )
			frame->AddChild(m_ControlContainer[pn]);
	}
}

void TouchHandler::TouchPress( int id, float x, float y )
{
	x /= m_PixelAdjust.x;
	y /= m_PixelAdjust.y;
	bool bMasterTouch = !GAMESTATE->m_timeGameStarted.IsZero() &&
		GAMESTATE->GetNumSidesJoined() == 1 &&
		GAMESTATE->GetCurrentStyle() != NULL;

	if( GAMESTATE->m_bPlaying )
		bMasterTouch &= !GAMESTATE->PlayerUsingBothSides();

	FOREACH_PlayerNumber(pn)
	{
		vector<TouchControl*>& controls = m_Controls[pn];
		for( unsigned int b = 0; b < controls.size(); b++ )
		{
			TouchControl& control = *controls[b];
			if( control.position.InRange(x, y) )
			{
				PlayerNumber pn1 = GAMESTATE->m_MasterPlayerNumber;
				if( !bMasterTouch || GAMESTATE->m_PlayerOptions[pn1].m_bSingleTouch )
					pn1 = pn;

				InputHandler_Touch::TouchEvent event(pn1, b);
				event.normal = &control.sprite[TOUCH_STATE_NORMAL];
				event.pressed = &control.sprite[TOUCH_STATE_PRESSED];
				handler->TouchPress(id, event);
			}
		}
	}
}

void TouchHandler::SDL_Subscribe( RageDisplay::VideoModeParams& params )
{
	m_pCurrentParams = &params;

	if( m_TouchMode == TOUCH_LEGACY )
	{
		SDL_EventState(SDL_MOUSEMOTION,		SDL_ENABLE);
		SDL_EventState(SDL_MOUSEBUTTONDOWN,	SDL_ENABLE);
		SDL_EventState(SDL_MOUSEBUTTONUP,	SDL_ENABLE);
	}
}

void TouchHandler::SDL_Unsubscribe( bool use_mySDL )
{
	typedef uint8_t StateFunction(uint8_t type, int state);
	StateFunction* EventState = use_mySDL ? &mySDL_EventState : &SDL_EventState;

	if( m_TouchMode == TOUCH_LEGACY )
	{
		EventState(SDL_MOUSEMOTION,		SDL_IGNORE);
		EventState(SDL_MOUSEBUTTONDOWN,	SDL_IGNORE);
		EventState(SDL_MOUSEBUTTONUP,	SDL_IGNORE);
	}
}

void TouchHandler::SDL_HandleEvent( SDL_Event& event )
{
	switch( event.type )
	{
	case SDL_MOUSEMOTION:
		if( !(event.motion.state & SDL_BUTTON_LMASK) )
			break;
		TouchUpdate(-1, event.motion.x, event.motion.y);
		break;

	case SDL_MOUSEBUTTONDOWN:
		if( event.button.button != SDL_BUTTON_LEFT || event.button.state != SDL_PRESSED )
			break;
		TouchPress(-1, event.motion.x, event.motion.y);
		break;

	case SDL_MOUSEBUTTONUP:
		if( event.button.button != SDL_BUTTON_LEFT )
			break;
		handler->TouchRelease(-1);
		break;
	}
}

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
