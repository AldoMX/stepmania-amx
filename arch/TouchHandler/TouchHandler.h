/* TouchHandler - Handler for Touch Events. */

#ifndef TOUCHHANDLER_H
#define TOUCHHANDLER_H

#include "ActorFrame.h"
#include "GameConstantsAndTypes.h"
#include "RageDisplay.h"
#include "Sprite.h"
#include "arch/InputHandler/InputHandler_Touch.h"

union SDL_Event;

class TouchHandler
{
protected:
	InputHandler_Touch* handler;

	RageVector2 m_PixelAdjust;
	RageDisplay::VideoModeParams *m_pCurrentParams;
	enum TouchMode { TOUCH_DISABLED, TOUCH_ENABLED, TOUCH_LEGACY } m_TouchMode;

	struct TouchControl
	{
		RectF position;
		Sprite sprite[NUM_TOUCH_STATES];
	};
	vector<TouchControl*> m_Controls[NUM_PLAYERS];
	ActorFrame* m_ControlContainer[NUM_PLAYERS];

public:
	TouchHandler( int touch_mode, InputHandler_Touch* input_handler );
	~TouchHandler();

	InputHandler_Touch* GetInputHandler() const { return handler; }

	void LoadControls();
	void UnloadControls();

	void SetControlsAsChildren( ActorFrame* frame );
	void TouchPress( int id, float x, float y );
	void TouchUpdate( int id, float x, float y ) {};

	virtual void SDL_Subscribe( RageDisplay::VideoModeParams& params );
	virtual void SDL_Unsubscribe( bool use_mySDL = false );
	virtual void SDL_HandleEvent( SDL_Event& event );
};

#define DEFAULT_TOUCH_HANDLER TouchHandler
TouchHandler* CreateTouchHandler();
extern TouchHandler* TOUCH;

#endif

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
