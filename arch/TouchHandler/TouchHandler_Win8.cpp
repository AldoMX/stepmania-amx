#include "global.h"
#include "TouchHandler_Win8.h"

#ifdef _WIN32_WINNT_WIN8

#include "RageLog.h"
#include "RageUtil.h"
#include "SDL_utils.h"
#include "StepMania.h"

TouchHandlerWin8::TouchHandlerWin8( int touch_mode, InputHandler_Touch* input_handler ) :
	TouchHandler(touch_mode, input_handler)
{
	// This is the `touch` build, force the touch controls
	if (m_TouchMode == TOUCH_DISABLED) {
		m_TouchMode = TOUCH_ENABLED;
	}
	EnableMouseInPointer(true);
}

void TouchHandlerWin8::SDL_Subscribe( RageDisplay::VideoModeParams& params )
{
	TouchHandler::SDL_Subscribe(params);

	if( m_TouchMode != TOUCH_DISABLED )
		SDL_EventState(SDL_SYSWMEVENT, SDL_ENABLE);
}

void TouchHandlerWin8::SDL_Unsubscribe( bool use_mySDL )
{
	TouchHandler::SDL_Unsubscribe(use_mySDL);

	typedef uint8_t StateFunction(uint8_t type, int state);
	StateFunction* EventState = use_mySDL ? &mySDL_EventState : &SDL_EventState;

	if( m_TouchMode != TOUCH_DISABLED )
		EventState(SDL_SYSWMEVENT, SDL_IGNORE);
}

void TouchHandlerWin8::SDL_HandleEvent( SDL_Event& event )
{
	if( m_TouchMode == TOUCH_LEGACY )
		TouchHandler::SDL_HandleEvent(event);

	if( event.type != SDL_SYSWMEVENT )
		return;

	const SDL_SysWMmsg& msg = *event.syswm.msg;
	switch( msg.msg )
	{
	case WM_MOVE:
		SDL_UpdatePosition(m_pCurrentParams);
		break;

	case WM_NCPOINTERUPDATE:
	case WM_POINTERUPDATE:
	case WM_POINTERDOWN:
		if( m_TouchMode == TOUCH_ENABLED )
		{
			bool bIsDown = msg.msg == WM_POINTERDOWN;
			DWORD pointer_id = GET_POINTERID_WPARAM(msg.wParam);
			POINTER_INFO info;
			if( !GetPointerInfo(pointer_id, &info) )
				break;

			CString sPointerType;
			switch( info.pointerType )
			{
			case PT_TOUCH:	sPointerType = "Touch";	break;
			case PT_PEN:	sPointerType = "Pen";	break;
			case PT_MOUSE:	sPointerType = "Mouse";	break;
			}
			if( sPointerType.empty() )
				break;

			POINT point = info.ptPixelLocationRaw;
			point.x -= (LONG)m_pCurrentParams->position.x;
			point.y -= (LONG)m_pCurrentParams->position.y;
			ENUM_CLAMP(point.x, 0l, -1l + m_pCurrentParams->width);
			ENUM_CLAMP(point.y, 0l, -1l + m_pCurrentParams->height);
			if( bIsDown )
				TouchPress(pointer_id, (float)point.x, (float)point.y);
			else
				TouchUpdate(pointer_id, (float)point.x, (float)point.y);
		}
		break;

	case WM_NCPOINTERUP:
	case WM_POINTERUP:
		if( m_TouchMode == TOUCH_ENABLED )
		{
			DWORD pointer_id = GET_POINTERID_WPARAM(msg.wParam);
			handler->TouchRelease(pointer_id);
		}
		break;

	//default:
	//	LOG->Trace("SDL_SYSWMEVENT(0x%04x).", msg.msg);
	}
}

#endif

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
