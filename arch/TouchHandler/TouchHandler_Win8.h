/* TouchHandler_Win8 - Handler for Touch Events for Windows 8. */

#ifndef TOUCHHANDLER_WIN8_H
#define TOUCHHANDLER_WIN8_H

#include "TouchHandler.h"
#include <windows.h>

#ifdef _WIN32_WINNT_WIN8

class TouchHandlerWin8 : public TouchHandler
{
public:
	TouchHandlerWin8( int touch_mode, InputHandler_Touch* input_handler );

	void SDL_Subscribe( RageDisplay::VideoModeParams& params );
	void SDL_Unsubscribe( bool use_mySDL );
	void SDL_HandleEvent( SDL_Event& event );
};

#undef DEFAULT_TOUCH_HANDLER
#define DEFAULT_TOUCH_HANDLER TouchHandlerWin8

#endif

#endif

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
