#include "global.h"
#include "NotesLoaderKSF.h"
#include "RageException.h"
#include "RageUtil_CharConversions.h"
#include "MsdFile.h"
#include "RageLog.h"
#include "RageUtil.h"
#include "NoteData.h"
#include "NoteTypes.h"

#include <iterator>

bool KSFLoader::LoadFromKSFFile( const CString &sPath, Steps &out, Song &song )
{
	LOG->Trace( "Steps::LoadFromKSFFile( '%s' )", sPath.c_str() );

	out.m_bLoadedFromSM = false;
	out.m_Timing.Reset();

	MsdFile msd;
	if( !msd.ReadFile( sPath ) )
		RageException::Throw( "Error opening file '%s'.", sPath.c_str() );

	unsigned uTickCount = 0;	// this is the value we read for TICKCOUNT
	CStringArray asRows;

	// Default BPM & Double Mode
	float fBPM = 60.0f;
	bool doublemode = false;

	{
		map<int,float>	vBPMs;
		map<int,int>	vBunkis;
		vBPMs.clear();
		vBunkis.clear();

		for( unsigned i=0; i<msd.GetNumValues(); i++ )
		{
			const MsdFile::value_t &sParams = msd.GetValue(i);
			CString sValueName = sParams[0];

			if( 0==stricmp(sValueName,"BPM") )
				fBPM = strtof(sParams[1], NULL);

			else if( 0==stricmp(sValueName,"STARTTIME") )
				out.m_Timing.m_fBeat0Offset = -strtof( sParams[1], NULL )/100;

			else if( 0==stricmp(sValueName,"TICKCOUNT") )
				uTickCount = abs( atoi(sParams[1]) );

			else if( 0==stricmp(sValueName,"STEP") )
			{
				CString step = sParams[1];
				TrimLeft(step);
				split( step, "\n", asRows, true );
			}

			else if( 0==stricmp(sValueName,"DIFFICULTY") )
			{
				int iMeter = atoi(sParams[1]);
				out.m_bHiddenDifficulty = iMeter == 99;	// 99 means "hidden"
				out.SetMeter(iMeter);
			}

			// Finally, legacy BPM Changes ^^
			else if( 0==stricmp(sValueName.Left(3),"BPM") )
			{
				int BPMN = atoi(sValueName.Right(sValueName.size()-3));
				vBPMs[BPMN] = strtof( sParams[1], NULL );
			}
			else if( 0==stricmp(sValueName.Left(5),"BUNKI") )
			{
				int BunkiN = sValueName.size() == 5 ? 1 : atoi(sValueName.Right(sValueName.size()-5));
				vBunkis[BunkiN] = atoi( sParams[1] );
			}

			// DirectMove, Propietary & Requested Tags
			else if( 0==stricmp(sValueName,"PLAYER") )
			{
				CString player = sParams[1];
				player.MakeLower();
				if( player.find( "double" ) != CString::npos )
					doublemode = true;
			}

			else if( 0==stricmp(sValueName,"BASESPEED") || 0==stricmp(sValueName,"SPEED") )
				out.m_Timing.SetSpeedAtBeat( 0, strtof(sParams[1], NULL), 4.f, 1.f );

			else if( 0==stricmp(sValueName,"STEPMAKER") )
				out.m_sCredit = sParams[1];
		}

		// Validate legacy BPM changes
		if( vBPMs.empty() && !vBunkis.empty() )
		{
			LOG->Warn( "KSF Error: \"%s\": There are no BPM values to apply BUNKIs.", sPath.c_str() );
		}
		else if( !vBPMs.empty() )
		{
			if( vBunkis.size() > vBPMs.size() )
			{
				LOG->Warn( "KSF Error: \"%s\": There are more BUNKI values than BPM values.", sPath.c_str() );
			}

			// Apply legacy BPM changes
			for( map<int,float>::iterator it = vBPMs.begin(); it != vBPMs.end(); ++it )
			{
				int BunkiN = it->first - 1;

				if( vBunkis.find( BunkiN ) == vBunkis.end() )
					LOG->Warn( "KSF Error: \"%s\": Unable to find BUNKI%s to apply BPM%d.", sPath.c_str(), BunkiN == 1 ? "" : ssprintf("%d",BunkiN).c_str(), it->first );
				else
				{
					out.m_Timing.SetBPMAtBeat(
						vBunkis[BunkiN] * ( out.m_Timing.GetBPMAtBeat(0) / 60.0f ),	// Bunki
						it->second	// BPM
					);
				}
			}

			// TODO - Aldo_MX: Mess with #STARTTIMEs
		}
	}

	out.m_Timing.SetBPMAtBeat(0, fBPM);

	if( uTickCount == 0 )
	{
		uTickCount = out.m_Timing.GetTickcountAtBeat(0);
		LOG->Warn( "KSF Error: \"%s\": TICKCOUNT not found; defaulting to %d", sPath.c_str(), uTickCount );
	}
	else
	{
		out.m_Timing.SetTickcountAtBeat(0, uTickCount);
		uTickCount = out.m_Timing.GetTickcountAtBeat(0);	// no invalid tickcounts!
	}

	NoteData notedata;	// read it into here

	{
		CString sDir, sFName, sExt;
		splitpath( sPath, sDir, sFName, sExt );
		out.SetDescription(sFName);

		Difficulty diff = StringToDifficulty(sFName, true);
		sFName.MakeLower();

		if( diff == DIFFICULTY_INVALID )
		{
			// Aliases for Wild / Crazy+
			if( sFName.find("wd") != CString::npos || sFName.find("dv") != CString::npos || sFName.find("division") != CString::npos )
				diff = DIFFICULTY_CHALLENGE;
			// Aliases for Practice
			else if( sFName.find("pr") != CString::npos || sFName.find("bg") != CString::npos )
				diff = DIFFICULTY_BEGINNER;
			// Aliases for Crazy/Nigthmare (used night because some "intelligent" pumpers misspell Nigthmare)
			else if( sFName.find("cz")!=CString::npos || sFName.find("nigth") != CString::npos || sFName.find("night") != CString::npos || sFName.find("nm") != CString::npos )
				diff = DIFFICULTY_HARD;
			// Aliases for Easy/Normal
			else if( sFName.find("ez") != CString::npos )
				diff = DIFFICULTY_EASY;
			else if( sFName.find("hd") != CString::npos || sFName.find("db") != CString::npos || sFName.find("free") != CString::npos || sFName.find("fs") != CString::npos )
				diff = DIFFICULTY_MEDIUM;
			else
				diff = DIFFICULTY_EDIT;
		}

		// Crazy+ / Crazy++ / Crazy+++ etc.
		if( sFName.find("+") != CString::npos )
		{
			CString sPluses = sFName;
			int iDiff = (int)diff;
			while( sPluses.find("+") != CString::npos )
			{
				iDiff++;
				sPluses.erase( sPluses.find("+"), 1 );
			}
			diff = (Difficulty)iDiff;

			if( diff > DIFFICULTY_EDIT )
				diff = DIFFICULTY_EDIT;
		}

		out.SetDifficulty( diff );

		// Double Modes
		if( doublemode )
		{
			/* Check for "halfdouble" before "double". */
			if( sFName.find("half") != CString::npos )
			{
				notedata.SetNumTracks( 6 );
				out.m_StepsType = STEPS_TYPE_PUMP_HALFDOUBLE;
			}
			else
			{
				notedata.SetNumTracks( 10 );
				out.m_StepsType = STEPS_TYPE_PUMP_DOUBLE;
			}
		}
		else
		{
			/* Check for "halfdouble" before "double". */
			if( sFName.find("half") != CString::npos )
			{
				notedata.SetNumTracks( 6 );
				out.m_StepsType = STEPS_TYPE_PUMP_HALFDOUBLE;
			}
			// Aliases for Double/Nigthmare
			else if( sFName.find("double") != CString::npos || sFName.find("db") != CString::npos || sFName.find("nigth") != CString::npos || sFName.find("night") != CString::npos || sFName.find("nm") != CString::npos || sFName.find("free") != CString::npos || sFName.find("fs") != CString::npos )
			{
				notedata.SetNumTracks( 10 );
				out.m_StepsType = STEPS_TYPE_PUMP_DOUBLE;
			}
			else if( sFName.find("_2") != CString::npos )
			{
				notedata.SetNumTracks( 10 );
				out.m_StepsType = STEPS_TYPE_PUMP_COUPLE;
			}
			else
			{
				notedata.SetNumTracks( 5 );
				out.m_StepsType = STEPS_TYPE_PUMP_SINGLE;
			}
		}
	}

	int iHoldStartRow[10], t;
	for( t=0; t<10; t++ )
		iHoldStartRow[t] = -1;

	int iCurrentRow = 0, iLastRow = -1, iSkipStart = -1, iSkipEnd = -1;
	bool bSkip = false, bLegacyHold = false;
	bool seed = false;	// TODO - Aldo_MX: HoldSeed

	for( unsigned r=0; r<asRows.size(); r++ )
	{
		CString& sRowString = asRows[r];
		StripCrnl( sRowString );

		if( sRowString == "" )
			continue;	// skip

		/* All 2s indicates the end of the song. */
		// Aldo_MX: With KSF2 it should mean "10 holds" ex. RAW
		if( sRowString == "2222222222222" )
		{
			bool bHasHolds = false;
			for( t=0; t<notedata.GetNumTracks(); t++ )
			{
				if( iHoldStartRow[t] != -1 )
				{
					bHasHolds = true;
					break;
				}
			}
			if( !bHasHolds )
				break;
		}

		// KSF DM Parsing - Begin
		if( sRowString.find("|") != CString::npos )
		{
			// KSFDM uses this format |X%d| or |X%f|, so let's strip the "|" characters
			while( sRowString.find("|") != CString::npos )
				sRowString.erase( sRowString.find("|"), 1 );
			sRowString.MakeUpper();
			// BPM Change
			if( sRowString.find("B") != CString::npos )
			{
				sRowString.erase( 0, 1 );

				float fBPMDM = strtof( sRowString, NULL );

				if( fBPMDM == 0 )
				{
					if( !bSkip )
					{
						bSkip = true;
						iSkipStart = iCurrentRow;
						continue;
					}
				}
				else
				{
					if( bSkip )
					{
						bSkip = false;
						iSkipEnd = iCurrentRow;
					}
				}

				if( iSkipStart != -1 && iSkipEnd != -1 )
				{
					out.m_Timing.SetStopAtBeat( NoteRowToBeat(iCurrentRow), out.m_Timing.GetElapsedTimeFromBeat( NoteRowToBeat(iSkipStart) ) - out.m_Timing.GetElapsedTimeFromBeat( NoteRowToBeat(iSkipEnd) ), true );
					out.m_Timing.SetFakeAtBeat( NoteRowToBeat(iSkipStart), NoteRowToBeat(iSkipEnd) - NoteRowToBeat(iSkipStart) );
					iSkipStart = -1;
					iSkipEnd = -1;
				}

				out.m_Timing.SetBPMAtBeat( NoteRowToBeat(iCurrentRow), strtof( sRowString, NULL ) );
			}
			// Tickcount Change
			else if( sRowString.find("T") != CString::npos )
			{
				sRowString.erase( 0, 1 );
				uTickCount = abs( atoi( sRowString ) );
				out.m_Timing.SetTickcountAtBeat( NoteRowToBeat(iCurrentRow), uTickCount );

				if( uTickCount != out.m_Timing.GetTickcountAtBeat( NoteRowToBeat(iCurrentRow) ) )
				{
					unsigned uTickfromDM = uTickCount;
					uTickCount = out.m_Timing.GetTickcountAtBeat( NoteRowToBeat(iCurrentRow) );
					LOG->Warn( "KSF Error: File %s has an invalid Tickcount. \"%u\" is not a factor of %d, \"%u\" used instead",
						sPath.c_str(), uTickfromDM, ROWS_PER_BEAT, uTickCount );
				}
			}
			// Delay measured in miliseconds
			else if( sRowString.find("D") != CString::npos )
			{
				sRowString.erase( 0, 1 );

				float fCurrentBeat = NoteRowToBeat(iCurrentRow);

				// Tricky: |D10|\n|D10|\n|D10| = |D30|
				float fCurrentDelay = out.m_Timing.GetStopLengthAtBeat( fCurrentBeat );
				out.m_Timing.SetStopAtBeat( fCurrentBeat, fCurrentDelay + strtof(sRowString, NULL)/1000, true );
			}
			// Delay measured in rows
			else if( sRowString.find("E") != CString::npos )
			{
				sRowString.erase( 0, 1 );

				int iBeats = atoi( sRowString ) * ROWS_PER_BEAT/uTickCount;
				float fBeats = (float)iBeats/ROWS_PER_BEAT;
				float fCurrentBeat = NoteRowToBeat(iCurrentRow);

				// Tricky: |E4|\n|E4|\n|E4| = |E12|
				float fCurrentDelay = out.m_Timing.GetStopLengthAtBeat( fCurrentBeat );
				float Start = out.m_Timing.GetElapsedTimeFromBeat( fCurrentBeat );
				float End = out.m_Timing.GetElapsedTimeFromBeat( fCurrentBeat+fBeats );
				out.m_Timing.SetStopAtBeat( fCurrentBeat, fCurrentDelay + End-Start, true );

				if( fBeats < 0 )
					out.m_Timing.SetFakeAtBeat( fCurrentBeat, abs( fBeats ) );
			}
			// Speed
			else if( sRowString.find("S") != CString::npos )
			{
				sRowString.erase( 0, 1 );

				CStringArray sSpeedParams;

				float fSpeed = 1.f,
					fBeats = 4.f,
					fFactor = 1.f;

				split( sRowString, ",", sSpeedParams, true );
				switch( sSpeedParams.size() )
				{
				default:
				case 3:
					fFactor = strtof( sSpeedParams[2], NULL );
				case 2:
					fBeats = (float)( atoi( sSpeedParams[1] ) * ROWS_PER_BEAT/uTickCount ) / ROWS_PER_BEAT;
				case 1:
					fSpeed = strtof( sSpeedParams[0], NULL );
					break;
				case 0:
					LOG->Warn( "KSF Error: File %s has an invalid Speed value: \"%s\"", sPath.c_str(), sRowString.c_str() );
					continue;
				}

				out.m_Timing.SetSpeedAtBeat( NoteRowToBeat(iCurrentRow), fSpeed, fBeats, fFactor );
			}
			// Multiplier
			else if( sRowString.find("M") != CString::npos )
			{
				sRowString.erase( 0, 1 );
				unsigned uMultiplier = (unsigned)strtoul( sRowString, NULL, 10 );
				float fMultiplier = (float)atof( sRowString );
				out.m_Timing.SetMultiplierAtBeat( NoteRowToBeat(iCurrentRow), uMultiplier, uMultiplier, fMultiplier, fMultiplier, fMultiplier, fMultiplier );
			}
			// Fake measured in rows
			else if( sRowString.find("F") != CString::npos )
			{
				sRowString.erase( 0, 1 );

				int iBeats = atoi( sRowString ) * ROWS_PER_BEAT/uTickCount;
				float fBeats = (float)iBeats/ROWS_PER_BEAT;
				float fCurrentBeat = NoteRowToBeat(iCurrentRow);

				out.m_Timing.SetFakeAtBeat( fCurrentBeat, fBeats );
			}
			// All changes are done. Lets skip this row.
			continue;
		}
		// KSF DM Parsing - End

		if( sRowString.size() != 13 && sRowString.size() != (unsigned)notedata.GetNumTracks() )
		{
			LOG->Warn("KSF Error: File %s had a RowString with an improper length (\"%s\"); corrupt notes ignored",
				sPath.c_str(), sRowString.c_str());
		}

		/* Half-doubles is offset; "0011111100000". */
		if( out.m_StepsType == STEPS_TYPE_PUMP_HALFDOUBLE )
			sRowString.erase( 0, 2 );

		int iSize = sRowString.size();

		if( iSize >= notedata.GetNumTracks() )
		{
			// the length of a note in a row depends on TICKCOUNT
			for( int t=0; t < notedata.GetNumTracks(); t++ )
			{
				switch( sRowString[t] )
				{
				case '4':
				{
					/* Remember when each hold starts; ignore the middle. */
					if( iHoldStartRow[t] == -1 )
					{
						iHoldStartRow[t] = iCurrentRow;
						seed = false;			// Long
						bLegacyHold = true;
					}
					continue;
				}
					break;

				case '5':
				case '2':
					iHoldStartRow[t] = iCurrentRow;
					bLegacyHold = false;

					switch( sRowString[t] )
					{
					case '5':	seed = true;	break;	// Roll
					case '2':	seed = false;	break;	// Long
					default: ASSERT(0);
					}

					continue;

				case '3':
				default:
					bool bIs3 = sRowString[t] == '3';

					if( iHoldStartRow[t] != -1 )
					{
						if( bIs3 || bLegacyHold )
						{
							HoldNote hn( t, iHoldStartRow[t], bIs3 ? iCurrentRow : iLastRow );
							//hn.subtype = seed ? HOLD_TYPE_ROLL : HOLD_TYPE_PIU;
							hn.subtype = seed ? HOLD_TYPE_ROLL : HOLD_TYPE_DANCE;
							// TODO - Aldo_MX: Routine KSF
							//hn.playerNumber = 0;
							notedata.AddHoldNote(hn);
							iHoldStartRow[t] = -1;
						}
						else
							continue;
					}

					if( bIs3 )
						continue;
				}

				TapNote tap;
				switch(sRowString[t])
				{
				case '0':	tap = TAP_EMPTY;			break;
				case '1':	tap = TAP_ORIGINAL_TAP;		break;
				case 'M':	tap = TAP_ORIGINAL_MINE;	break;
				case 'S':	tap = TAP_ORIGINAL_SHOCK;	break;
				case 'P':	tap = TAP_ORIGINAL_POTION;	break;
				case 'H':	tap = TAP_ORIGINAL_HIDDEN;	break;
				case 'L':	tap = TAP_ORIGINAL_LIFT;	break;
				//case 'G':	tap = TAP_ORIGINAL_GROOVE;	break;
				//case 'W':	tap = TAP_ORIGINAL_WILD;	break;
				default:	tap = TAP_EMPTY;			break;
				}

				notedata.SetTapNote(t, iCurrentRow, tap);
			}
		}

		iLastRow = iCurrentRow;
		iCurrentRow += ROWS_PER_BEAT/uTickCount;
	}

	out.SetNoteData(&notedata);

	if(!out.GetMeter())
	{
		int iMeter = (int)(out.PredictMeter() + .5f);
		out.SetMeter( iMeter );
		//switch( diff )
		//{
		//case DIFFICULTY_BEGINNER:	out.SetMeter(1);	break;
		//case DIFFICULTY_EASY:		out.SetMeter(3);	break;
		//case DIFFICULTY_MEDIUM:		out.SetMeter(8);	break;
		//case DIFFICULTY_HARD:		out.SetMeter(15);	break;
		//case DIFFICULTY_CHALLENGE:	out.SetMeter(21);	break;
		//case DIFFICULTY_EDIT:		out.SetMeter(99);	break;
		//default:	ASSERT(0);
		//}
	}

	out.TidyUpData();

	return true;
}

void KSFLoader::GetApplicableFiles( CString sPath, CStringArray &out )
{
	GetDirListing( sPath + CString("*.ksf"), out );
}

void KSFLoader::LoadTags( const CString &str, Song &out )
{
	/* str is either a #TITLE or a directory component.  Fill in missing information.
	 * str is either "title", "artist - title", or "artist - title - difficulty". */
	CStringArray asBits;
	split( str, " - ", asBits, false );
	/* Ignore the difficulty, since we get that elsewhere. */
	if( asBits.size() == 3 &&
		(!stricmp(asBits[2], "double") ||
		 !stricmp(asBits[2], "easy") ||
		 !stricmp(asBits[2], "normal") ||
		 !stricmp(asBits[2], "hard") ||
		 !stricmp(asBits[2], "crazy")) )
	{
		asBits.erase(asBits.begin()+2, asBits.begin()+3);
	}

	CString title, artist;
	if( asBits.size() == 2 )
	{
		artist = asBits[0];
		title = asBits[1];
	}
	else
	{
		title = asBits[0];
	}

	/* Convert, if possible.  Most KSFs are in Korean encodings (CP942/EUC-KR). */
	if( !ConvertString( title, "korean" ) )
		title = "";
	if( !ConvertString( artist, "korean" ) )
		artist = "";

	if( out.m_sMainTitle == "" )
		out.m_sMainTitle = title;
	if( out.m_sArtist == "" )
		out.m_sArtist = artist;
}

static void GetImageDirListing( CString sPath, CStringArray &AddTo, bool bReturnPathToo=false )
{
	GetDirListing( sPath + ".png", AddTo, false, bReturnPathToo );
	GetDirListing( sPath + ".jpg", AddTo, false, bReturnPathToo );
	GetDirListing( sPath + ".bmp", AddTo, false, bReturnPathToo );
	GetDirListing( sPath + ".gif", AddTo, false, bReturnPathToo );
}

bool KSFLoader::LoadGlobalData( const CString &sPath, Song &out )
{
	MsdFile msd;
	if( !msd.ReadFile( sPath ) )
		RageException::Throw( "Error opening file \"%s\": %s", sPath.c_str(), msd.GetError().c_str() );

	bool bHasSong = false, bHasIntro = false, bHasTitle = false, bHasDisc = false, bHasBGA = false, bHasPreview = false, bHasBanner = false;

	// KSF DM Parsing
	CStringArray asRows;
	CString sTitle, sArtist;
	unsigned uTickCount = 2;

	{
		map<int,float>	vBPMs;
		map<int,int>	vBunkis;
		vBPMs.clear();
		vBunkis.clear();

		out.m_Timing.Reset();
		sTitle.clear();
		sArtist.clear();

		// Default PIU Intro = 7 seconds.
		out.m_fMusicSampleLengthSeconds = 7.f;

		for( unsigned i=0; i < msd.GetNumValues(); i++ )
		{
			const MsdFile::value_t &sParams = msd.GetValue(i);
			CString sValueName = sParams[0];

			// handle the data
			if( 0==stricmp(sValueName,"TITLE") )
				sTitle = sParams[1];
			else if( 0==stricmp(sValueName,"BPM") )
				out.m_Timing.SetBPMAtBeat( 0, strtof(sParams[1], NULL) );
			else if( 0==stricmp(sValueName,"TICKCOUNT") )
				uTickCount = abs( atoi(sParams[1]) );
			else if( 0==stricmp(sValueName,"STARTTIME") )
				out.m_Timing.m_fBeat0Offset = -strtof( sParams[1], NULL )/100;

			// Finally, legacy BPM Changes ^^
			else if( 0==stricmp(sValueName.Left(3),"BPM") )
			{
				int BPMN = atoi(sValueName.Right(sValueName.size()-3));
				vBPMs[BPMN] = strtof( sParams[1], NULL );
			}
			else if( 0==stricmp(sValueName.Left(5),"BUNKI") )
			{
				int BunkiN = sValueName.size() == 5 ? 1 : atoi(sValueName.Right(sValueName.size()-5));
				vBunkis[BunkiN] = atoi( sParams[1] );
			}

			// DirectMove, Propietary & Requested Tags
			else if( 0==stricmp(sValueName,"ARTIST") )
				sArtist = sParams[1];
			else if( 0==stricmp(sValueName,"MUSICINTRO") || 0==stricmp(sValueName,"INTRO") )
				out.m_fMusicSampleStartSeconds = HHMMSSToSeconds( sParams[1] );
			else if( 0==stricmp(sValueName,"MUSICFILE") || 0==stricmp(sValueName,"SONGFILE") )
			{
				bHasSong = true;
				out.m_sMusicFile = sParams[1];
			}
			else if( 0==stricmp(sValueName,"INTROFILE") )
			{
				bHasIntro = true;
				out.m_sIntroFile = sParams[1];
			}
			else if( 0==stricmp(sValueName,"TITLEFILE") )
			{
				bHasTitle = true;
				out.m_sBackgroundFile = sParams[1];
			}
			else if( 0==stricmp(sValueName,"DISCFILE") )
			{
				bHasDisc = true;
				out.m_sDiscFile = sParams[1];
			}
			else if( 0==stricmp(sValueName,"BGAFILE") )
			{
				bHasBGA = true;
				out.m_BGChanges.clear();
				out.m_BGChanges.push_back( BGChange(0,sParams[1],1.f,false,false,false) );
			}
			else if( 0==stricmp(sValueName,"PREVIEWFILE") || 0==stricmp(sValueName,"BGAPREVIEW") )
			{
				bHasPreview = true;
				out.m_sPreviewFile = sParams[1];
			}
			else if( 0==stricmp(sValueName,"BANNERFILE") )
			{
				bHasBanner = true;
				out.m_sBannerFile = sParams[1];
			}

			// KSF DM Parsing - Parse Steps to find changes
			else if( 0==stricmp(sValueName,"STEP") )
			{
				CString step = sParams[1];
				TrimLeft(step);
				split( step, "\n", asRows, true );
			}

			else if( 0==stricmp(sValueName,"DIFFICULTY") ||
					0==stricmp(sValueName,"BEATMEASURE") ||
					0==stricmp(sValueName,"MEASURE") ||
					0==stricmp(sValueName,"PLAYER") ||
					0==stricmp(sValueName,"BASESPEED") ||
					0==stricmp(sValueName,"SPEED")
				)
				; /* Handled in LoadFromKSFFile; don't warn. */

			else
				LOG->Trace( "KSF Error: Unexpected value named '%s'", sValueName.c_str() );
		}

		// Validate legacy BPM changes
		if( vBPMs.empty() && !vBunkis.empty() )
		{
			LOG->Warn( "KSF Error: \"%s\": There are no BPM values to apply BUNKIs.", sPath.c_str() );
		}
		else if( !vBPMs.empty() )
		{
			if( vBunkis.size() > vBPMs.size() )
			{
				LOG->Warn( "KSF Error: \"%s\": There are more BUNKI values than BPM values.", sPath.c_str() );
			}

			// Apply legacy BPM changes
			for( map<int,float>::iterator it = vBPMs.begin(); it != vBPMs.end(); ++it )
			{
				int BunkiN = it->first - 1;

				if( vBunkis.find( BunkiN ) == vBunkis.end() )
					LOG->Warn( "KSF Error: \"%s\": Unable to find BUNKI%s to apply BPM%d.", sPath.c_str(), BunkiN == 1 ? "" : ssprintf("%d",BunkiN).c_str(), it->first );
				else
				{
					out.m_Timing.SetBPMAtBeat(
						vBunkis[BunkiN] * ( out.m_Timing.GetBPMAtBeat(0) / 60.0f ),	// Bunki
						it->second	// BPM
					);
				}
			}

			// TODO - Aldo_MX: Mess with #STARTTIMEs
		}
	}

	if( sArtist == "" )
		LoadTags(sTitle,out);
	else
	{
		out.m_sMainTitle = sTitle;
		out.m_sArtist = sArtist;
	}

	int iCurrentRow = 0, iSkipStart = -1, iSkipEnd = -1;
	bool bSkip = false;

	for( unsigned r=0; r<asRows.size(); r++ )
	{
		CString& sRowString = asRows[r];
		StripCrnl( sRowString );

		if( sRowString == "" )
			continue;	// skip

		// Aldo_MX: Disabled to support KSF2
		///* All 2s indicates the end of the song. */
		//if( sRowString == "2222222222222" )
		//	break;

		// KSF DM Parsing - Begin
		if( sRowString.find("|") != CString::npos )
		{
			// KSFDM uses this format |X%d| or |X%f|, so let's strip the "|" characters
			while( sRowString.find("|") != CString::npos )
				sRowString.erase( sRowString.find("|"), 1 );
			sRowString.MakeUpper();
			// BPM Change
			if( sRowString.find("B") != CString::npos )
			{
				sRowString.erase( 0, 1 );

				float fBPMDM = strtof( sRowString, NULL );

				if( fBPMDM == 0 )
				{
					if( !bSkip )
					{
						bSkip = true;
						iSkipStart = iCurrentRow;
						continue;
					}
				}
				else
				{
					if( bSkip )
					{
						bSkip = false;
						iSkipEnd = iCurrentRow;
					}
				}

				if( iSkipStart != -1 && iSkipEnd != -1 )
				{
					out.m_Timing.SetStopAtBeat( NoteRowToBeat(iCurrentRow), out.m_Timing.GetElapsedTimeFromBeat( NoteRowToBeat(iSkipStart) ) - out.m_Timing.GetElapsedTimeFromBeat( NoteRowToBeat(iSkipEnd) ), true );
					iSkipStart = -1;
					iSkipEnd = -1;
				}

				out.m_Timing.SetBPMAtBeat( NoteRowToBeat(iCurrentRow), strtof( sRowString, NULL ) );
			}
			// Tickcount Change
			else if( sRowString.find("T") != CString::npos )
			{
				sRowString.erase( 0, 1 );
				uTickCount = abs( atoi( sRowString ) );

				TimingData tempTiming;
				tempTiming.SetTickcountAtBeat( NoteRowToBeat(iCurrentRow), uTickCount );

				if( uTickCount != tempTiming.GetTickcountAtBeat( NoteRowToBeat(iCurrentRow) ) )
				{
					unsigned uTickfromDM = uTickCount;
					uTickCount = tempTiming.GetTickcountAtBeat( NoteRowToBeat(iCurrentRow) );
					LOG->Warn( "KSF Error: File %s has an invalid Tickcount. \"%u\" is not a factor of %d, \"%u\" used instead",
						sPath.c_str(), uTickfromDM, ROWS_PER_BEAT, uTickCount );
				}
			}
			// Delay measured in miliseconds
			else if( sRowString.find("D") != CString::npos )
			{
				sRowString.erase( 0, 1 );

				float fCurrentBeat = NoteRowToBeat(iCurrentRow);

				// Tricky: |D10|\n|D10|\n|D10| = |D30|
				float fCurrentDelay = out.m_Timing.GetStopLengthAtBeat( fCurrentBeat );
				out.m_Timing.SetStopAtBeat( fCurrentBeat, fCurrentDelay + strtof(sRowString, NULL)/1000, true );
			}
			// Delay measured in rows
			else if( sRowString.find("E") != CString::npos )
			{
				sRowString.erase( 0, 1 );

				int iBeats = atoi( sRowString ) * ROWS_PER_BEAT/uTickCount;
				float fBeats = (float)iBeats/ROWS_PER_BEAT;
				float fCurrentBeat = NoteRowToBeat(iCurrentRow);

				// Tricky: |E4|\n|E4|\n|E4| = |E12|
				float fCurrentDelay = out.m_Timing.GetStopLengthAtBeat( fCurrentBeat );
				float Start = out.m_Timing.GetElapsedTimeFromBeat( fCurrentBeat );
				float End = out.m_Timing.GetElapsedTimeFromBeat( fCurrentBeat+fBeats );
				out.m_Timing.SetStopAtBeat( fCurrentBeat, fCurrentDelay + End-Start, true );
			}
			// All changes are done. Lets skip this row.
			continue;
		}
		// KSF DM Parsing - End

		iCurrentRow += ROWS_PER_BEAT/uTickCount;
	}

	/* Try to fill in missing bits of information from the pathname. */
	{
		CStringArray asBits;
		split( sPath, "/", asBits, true);

		ASSERT(asBits.size() > 1);
		LoadTags(asBits[asBits.size()-2], out);
	}

	// search for music with song in the file name
	if( !bHasSong )
	{
		CStringArray arrayPossibleMusic;
		GetDirListing( out.GetSongDir() + CString("*song.mp3"), arrayPossibleMusic );
		GetDirListing( out.GetSongDir() + CString("*song.wav"), arrayPossibleMusic );
		GetDirListing( out.GetSongDir() + CString("*song.flac"), arrayPossibleMusic );
		GetDirListing( out.GetSongDir() + CString("*song.ogg"), arrayPossibleMusic );

		if( !arrayPossibleMusic.empty() )
			out.m_sMusicFile = arrayPossibleMusic[0];
		else
		{
			arrayPossibleMusic.clear();
			GetDirListing( out.GetSongDir() + CString("*.mp3"), arrayPossibleMusic );
			GetDirListing( out.GetSongDir() + CString("*.wav"), arrayPossibleMusic );
			GetDirListing( out.GetSongDir() + CString("*.flac"), arrayPossibleMusic );
			GetDirListing( out.GetSongDir() + CString("*.ogg"), arrayPossibleMusic );
		
			CStringArray arrayImpossibleMusic;
			GetDirListing( out.GetSongDir() + CString("*intro.mp3"), arrayImpossibleMusic );
			GetDirListing( out.GetSongDir() + CString("*intro.wav"), arrayImpossibleMusic );
			GetDirListing( out.GetSongDir() + CString("*intro.flac"), arrayImpossibleMusic );
			GetDirListing( out.GetSongDir() + CString("*intro.ogg"), arrayImpossibleMusic );

			if( arrayPossibleMusic.size() > arrayImpossibleMusic.size() )
			{
				CStringArray asDifference;
				sort(arrayPossibleMusic.begin(), arrayPossibleMusic.end());
				sort(arrayImpossibleMusic.begin(), arrayImpossibleMusic.end());
				set_difference(arrayPossibleMusic.begin(), arrayPossibleMusic.end(), arrayImpossibleMusic.begin(), arrayImpossibleMusic.end(), back_inserter(asDifference));

				if( !asDifference.empty() )
					out.m_sMusicFile = asDifference[0];
			}
		}
	}

	if( !bHasIntro )
	{
		CStringArray arrayPossibleIntro;
		GetDirListing( out.GetSongDir() + CString("*intro.mp3"), arrayPossibleIntro );
		GetDirListing( out.GetSongDir() + CString("*intro.wav"), arrayPossibleIntro );
		GetDirListing( out.GetSongDir() + CString("*intro.flac"), arrayPossibleIntro );
		GetDirListing( out.GetSongDir() + CString("*intro.ogg"), arrayPossibleIntro );

		if( !arrayPossibleIntro.empty() )		// we found a match
			out.m_sIntroFile = arrayPossibleIntro[0];
	}

	if( !bHasBanner )
	{
		CStringArray arrayPossibleBanners;
		GetImageDirListing( out.GetSongDir() + CString("*banner*"), arrayPossibleBanners );

		if( !arrayPossibleBanners.empty() )
			out.m_sBannerFile = arrayPossibleBanners[0];
	}

	if( !bHasDisc )
	{
		CStringArray arrayPossibleDiscs;
		GetImageDirListing( out.GetSongDir() + CString("*disc.*"), arrayPossibleDiscs );

		if( !arrayPossibleDiscs.empty() )
			out.m_sDiscFile = arrayPossibleDiscs[0];
	}

	if( !bHasTitle )
	{
		CStringArray arrayPossibleCDTitles;
		GetImageDirListing( out.GetSongDir() + CString("*cdtitle*"), arrayPossibleCDTitles );

		CStringArray arrayPossibleBackgrounds;
		GetImageDirListing( out.GetSongDir() + CString("*title*"), arrayPossibleBackgrounds );

		if( !arrayPossibleBackgrounds.empty() )
			out.m_sBackgroundFile = arrayPossibleBackgrounds[0];

		if( !arrayPossibleBackgrounds.empty() && arrayPossibleBackgrounds.size() > arrayPossibleCDTitles.size() )
		{
			int iTitle = 0;
			for( unsigned t=0; t<arrayPossibleBackgrounds.size(); t++ )
			{
				bool bContinue = false;
				for( unsigned cdt=0; cdt<arrayPossibleCDTitles.size(); cdt++ )
				{
					if( arrayPossibleBackgrounds[t] == arrayPossibleCDTitles[cdt] )
					{
						bContinue = true;
						break;
					}
				}
				if( bContinue )
					continue;

				iTitle = t;
				break;
			}
			out.m_sBackgroundFile = arrayPossibleBackgrounds[iTitle];
		}
	}

	if( !bHasPreview )
	{
		CStringArray arrayPossiblePreviews;
		GetDirListing( out.GetSongDir() + CString("*preview*.avi"), arrayPossiblePreviews );
		GetDirListing( out.GetSongDir() + CString("*preview*.mpg"), arrayPossiblePreviews );
		GetDirListing( out.GetSongDir() + CString("*preview*.mpeg"), arrayPossiblePreviews );

		if( !arrayPossiblePreviews.empty() )
			out.m_sPreviewFile = arrayPossiblePreviews[0];
	}

	if( !bHasBGA )
	{
		CStringArray arrayPossiblePreviews;
		GetDirListing( out.GetSongDir() + CString("*preview*.avi"), arrayPossiblePreviews );
		GetDirListing( out.GetSongDir() + CString("*preview*.mpg"), arrayPossiblePreviews );
		GetDirListing( out.GetSongDir() + CString("*preview*.mpeg"), arrayPossiblePreviews );

		CStringArray arrayPossibleMovies;
		GetDirListing( out.GetSongDir() + CString("*.avi"), arrayPossibleMovies );
		GetDirListing( out.GetSongDir() + CString("*.mpg"), arrayPossibleMovies );
		GetDirListing( out.GetSongDir() + CString("*.mpeg"), arrayPossibleMovies );

		if( !arrayPossibleMovies.empty() && arrayPossibleMovies.size() > arrayPossiblePreviews.size() )
		{
			int iBG = 0;
			for( unsigned bg=0; bg<arrayPossibleMovies.size(); bg++ )
			{
				bool bContinue = false;
				for( unsigned pv=0; pv<arrayPossiblePreviews.size(); pv++ )
				{
					if( arrayPossibleMovies[bg] == arrayPossiblePreviews[pv] )
					{
						bContinue = true;
						break;
					}
				}
				if( bContinue )
					continue;

				iBG = bg;
				break;
			}
			out.m_BGChanges.clear();
			out.m_BGChanges.push_back( BGChange(out.m_Timing.GetBeatFromElapsedTime(0),arrayPossibleMovies[iBG],1.f,false,false,false) );
		}
	}
	else
		out.m_BGChanges[0].m_fBeat = out.m_Timing.GetBeatFromElapsedTime(0);

	return true;
}

bool KSFLoader::LoadFromDir( CString sDir, Song &out )
{
	LOG->Trace( "Song::LoadFromKSFDir(%s)", sDir.c_str() );

	CStringArray arrayKSFFileNames;
	GetDirListing( sDir + CString("*lightmap.ksf"), arrayKSFFileNames );

	if( arrayKSFFileNames.empty() )
		GetDirListing( sDir + CString("*.ksf"), arrayKSFFileNames );

	if(!LoadGlobalData(out.GetSongDir() + arrayKSFFileNames[0], out))
		return false;

	arrayKSFFileNames.clear();

	GetDirListing( sDir + CString("*.ksf"), arrayKSFFileNames );

	/* We shouldn't have been called to begin with if there were no KSFs. */
	if( arrayKSFFileNames.empty() )
		RageException::Throw( "Couldn't find any KSF files in '%s'", sDir.c_str() );


	// load the Steps from the rest of the KSF files
	for( unsigned i=0; i<arrayKSFFileNames.size(); i++ )
	{
		if( 0==stricmp( arrayKSFFileNames[i].Right(12), "lightmap.ksf" ) )
			continue;

		Steps* pNewNotes = new Steps;
		if(!LoadFromKSFFile( out.GetSongDir() + arrayKSFFileNames[i], *pNewNotes, out ))
		{
			delete pNewNotes;
			continue;
		}

		out.AddSteps( pNewNotes );
	}

	return true;
}

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * Contains code (c) 2001-2004 Chris Danford, Glenn Maynard.
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
