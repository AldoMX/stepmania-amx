// CUSTOM TITLE MENU COMMANDS -	ScreenTitleMenu.cpp

#include "global.h"
#include "ScreenTitleMenu.h"
#include "ScreenAttract.h"
#include "ScreenManager.h"
#include "GameConstantsAndTypes.h"
#include "RageUtil.h"
#include "StepMania.h"
#include "PrefsManager.h"
#include "RageLog.h"
#include "SongManager.h"
#include "AnnouncerManager.h"
#include "GameState.h"
#include "GameManager.h"
#include "InputMapper.h"
#include "ThemeManager.h"
#include "GameSoundManager.h"
#include "CodeDetector.h"
#include "RageTextureManager.h"
#include "UnlockSystem.h"
#include "ProductInfo.h"
#include "LightsManager.h"
#include "CodeDetector.h"
#include "CommonMetrics.h"
#include "Game.h"
#include "ScreenOptionsMasterPrefs.h"


#define LOGO_ON_COMMAND			THEME->GetMetric("ScreenTitleMenu","LogoOnCommand")
#define LOGO_HOME_ON_COMMAND	THEME->GetMetric("ScreenTitleMenu","LogoHomeOnCommand")
#define VERSION_ON_COMMAND		THEME->GetMetric("ScreenTitleMenu","VersionOnCommand")
#define SONGS_ON_COMMAND		THEME->GetMetric("ScreenTitleMenu","SongsOnCommand")

#define SONG_IN_TEXT	THEME->GetMetric("ScreenTitleMenu","SongsInTextSingle")
#define SONGS_IN_TEXT	THEME->GetMetric("ScreenTitleMenu","SongsInText")
#define GROUP_TEXT		THEME->GetMetric("ScreenTitleMenu","GroupsTextSingle")
#define GROUPS_TEXT		THEME->GetMetric("ScreenTitleMenu","GroupsText")
#define COURSE_TEXT		THEME->GetMetric("ScreenTitleMenu","CoursesTextSingle")
#define COURSES_TEXT	THEME->GetMetric("ScreenTitleMenu","CoursesText")
#define UNLOCK_TEXT		THEME->GetMetric("ScreenTitleMenu","UnlocksTextSingle")
#define UNLOCKS_TEXT	THEME->GetMetric("ScreenTitleMenu","UnlocksText")

#define MAX_SONGS_ON_COMMAND	THEME->GetMetric("ScreenTitleMenu","MaxSongsOnCommand")
#define MAX_SONG_TEXT_1			THEME->GetMetric("ScreenTitleMenu","MaxSongsPrefixSingle")
#define MAX_SONG_TEXT_2			THEME->GetMetric("ScreenTitleMenu","MaxSongsTextSingle")
#define MAX_SONGS_TEXT_1		THEME->GetMetric("ScreenTitleMenu","MaxSongsPrefix")
#define MAX_SONGS_TEXT_2		THEME->GetMetric("ScreenTitleMenu","MaxSongsText")
#define MAX_SONGS_EVENT			THEME->GetMetric("ScreenTitleMenu","MaxSongsEvent")

#define MAX_STAGES_ON_COMMAND	THEME->GetMetric("ScreenTitleMenu","MaxStagesOnCommand")
#define MAX_STAGE_TEXT_1		THEME->GetMetric("ScreenTitleMenu","MaxStagesPrefixSingle")
#define MAX_STAGE_TEXT_2		THEME->GetMetric("ScreenTitleMenu","MaxStagesTextSingle")
#define MAX_STAGES_TEXT_1		THEME->GetMetric("ScreenTitleMenu","MaxStagesPrefix")
#define MAX_STAGES_TEXT_2		THEME->GetMetric("ScreenTitleMenu","MaxStagesText")
#define MAX_STAGES_EVENT		THEME->GetMetric("ScreenTitleMenu","MaxStagesEvent")

#define LIFE_DIFFICULTY_ON_COMMAND	THEME->GetMetric("ScreenTitleMenu","LifeDifficultyOnCommand")
#define LIFE_DIFFICULTY_TEXT		THEME->GetMetric("ScreenTitleMenu","LifeDifficultyText")

#define HELP_X				THEME->GetMetricF("ScreenTitleMenu","HelpX")
#define HELP_Y				THEME->GetMetricF("ScreenTitleMenu","HelpY")
#define CHOICES_X			THEME->GetMetricF("ScreenTitleMenu","ChoicesX")
#define CHOICES_START_Y			THEME->GetMetricF("ScreenTitleMenu","ChoicesStartY")
#define CHOICES_SPACING_Y		THEME->GetMetricF("ScreenTitleMenu","ChoicesSpacingY")
#define CHOICES_SHADOW_LENGTH		THEME->GetMetricF("ScreenTitleMenu","ChoicesShadowLength")
#define COLOR_NOT_SELECTED		THEME->GetMetricC("ScreenTitleMenu","ColorNotSelected")
#define COLOR_SELECTED			THEME->GetMetricC("ScreenTitleMenu","ColorSelected")
#define ZOOM_NOT_SELECTED		THEME->GetMetricF("ScreenTitleMenu","ZoomNotSelected")
#define ZOOM_SELECTED			THEME->GetMetricF("ScreenTitleMenu","ZoomSelected")
#define MENU_TEXT_ALIGN			THEME->GetMetricI("ScreenTitleMenu","MenuTextAlign")
#define SECONDS_BETWEEN_COMMENTS	THEME->GetMetricF("ScreenTitleMenu","SecondsBetweenComments")
#define SECONDS_BEFORE_ATTRACT		THEME->GetMetricF("ScreenTitleMenu","SecondsBeforeAttract")
#define HELP_TEXT( coin_mode )		THEME->GetMetric("ScreenTitleMenu","HelpText"+Capitalize(PREFSMAN->CoinModeToString(coin_mode)))
#define MENU_ITEM_CREATE		THEME->GetMetric("ScreenTitleMenu","MenuCommandOnCreate")
#define MENU_ITEM_SELECT_DELAY		THEME->GetMetricF("ScreenTitleMenu","MenuCommandSelectDelay")

#define NAME( sChoiceName )		THEME->GetMetric (m_sName,ssprintf("Name%s",sChoiceName.c_str()))

// Custom Title Menu Commands
#define LEGACY_CHOICE_ANIM		THEME->GetMetricB("ScreenTitleMenu","LegacyChoiceAnim")
#define CHOICE_GAIN_FOCUS_COMMAND	THEME->GetMetric("ScreenTitleMenu","ChoiceGainFocusCommand")
#define CHOICE_LOSE_FOCUS_COMMAND	THEME->GetMetric("ScreenTitleMenu","ChoiceLoseFocusCommand")

// Information on the top of the screen
#define SHOW_FILE_INFO			THEME->GetMetricB("ScreenTitleMenu","ShowNumSongsGroupsCoursesUnlocks")
#define SHOW_ENGINE_INFO		THEME->GetMetricB("ScreenTitleMenu","ShowEngineVersion")

const ScreenMessage SM_PlayComment	=	ScreenMessage(SM_User+1);
const ScreenMessage SM_GoToAttractLoop	=	ScreenMessage(SM_User+13);


ScreenTitleMenu::ScreenTitleMenu( CString sClassName ) : ScreenSelect( sClassName )
{
	LOG->Trace( "ScreenTitleMenu::ScreenTitleMenu()" );

#if defined( WITH_COIN_MODE )
	// Don't show screen title menu (says "Press Start")
	// if there are 0 credits and inserted and CoinMode is pay.
	if( PREFSMAN->GetCoinMode() == PrefsManager::COIN_PAY && GAMESTATE->m_iCoins < PREFSMAN->m_iCoinsPerCredit )
	{
		SCREENMAN->SetNewScreen( INITIAL_SCREEN );
		return;
	}
#endif

	/* XXX We really need two common calls: 1, something run when exiting from gameplay
	 * (to do this reset), and 2, something run when entering gameplay, to apply default
	 * options.  Having special cases in attract screens and the title menu to reset
	 * things stinks ... */
	GAMESTATE->Reset();

	LIGHTSMAN->SetLightsMode( LIGHTSMODE_JOINING );	// do this after Reset!


	m_sprLogo.Load( THEME->GetPathG("ScreenLogo", GAMESTATE->GetCurrentGame()->m_szName) );
#if defined( WITH_COIN_MODE )
	if( PREFSMAN->GetCoinMode() != PrefsManager::COIN_HOME )
	{
		switch( PREFSMAN->GetPremium() )
		{
		case PrefsManager::DOUBLES_PREMIUM:
			m_Premium.LoadFromAniDir( THEME->GetPathB(m_sName, "doubles premium") );
			this->AddChild( &m_Premium );
			break;
		case PrefsManager::JOINT_PREMIUM:
			m_Premium.LoadFromAniDir( THEME->GetPathB(m_sName, "joint premium") );
			this->AddChild( &m_Premium );
			break;
		}
	}

	if( PREFSMAN->GetCoinMode()==PrefsManager::COIN_HOME )
		m_sprLogo.Command( LOGO_HOME_ON_COMMAND );
	else
#else
		m_sprLogo.Command( LOGO_ON_COMMAND );
#endif
	this->AddChild( &m_sprLogo );

	// Engine version information
	m_textVersion.LoadFromFont( THEME->GetPathF("Common", "normal") );
	m_textVersion.Command( VERSION_ON_COMMAND );
	m_textVersion.SetText( PRODUCT_ABBR_VER );
	this->AddChild( &m_textVersion );

	// "### songs in ## groups, ## courses, ## unlocks" information
	m_textSongs.LoadFromFont( THEME->GetPathF("Common", "normal") );
	m_textSongs.Command( SONGS_ON_COMMAND );
	CString SongText = "";

	if( SHOW_FILE_INFO )
	{
		SongText = ssprintf("%d %s %d %s, %d %s", SONGMAN->GetNumSongs(), (SONGMAN->GetNumSongs() > 1 ? SONGS_IN_TEXT.c_str() : SONG_IN_TEXT.c_str()), SONGMAN->GetNumGroups(), (SONGMAN->GetNumGroups() > 1 ? GROUPS_TEXT.c_str() : GROUP_TEXT.c_str()), SONGMAN->GetNumCourses(), (SONGMAN->GetNumCourses() > 1 ? COURSES_TEXT.c_str() : COURSE_TEXT.c_str()) );

		if( PREFSMAN->m_bUseUnlockSystem )
			SongText += ssprintf(", %d %s", UNLOCKMAN->GetNumUnlocks(), (UNLOCKMAN->GetNumUnlocks() > 1 ? UNLOCKS_TEXT.c_str() : UNLOCK_TEXT.c_str()) );
	}

	m_textSongs.SetText( SongText );
	this->AddChild( &m_textSongs );

	// Max songs info
	{
		m_textMaxSongs.LoadFromFont( THEME->GetPathF(m_sName, "MaxSongs") );
		m_textMaxSongs.Command( MAX_SONGS_ON_COMMAND );
		CString sText = PREFSMAN->m_bEventMode ? MAX_SONGS_EVENT : ssprintf( "%s %d %s", (PREFSMAN->m_iMaxSongsToPlay > 1 ? MAX_SONGS_TEXT_1.c_str() : MAX_SONG_TEXT_1.c_str()), PREFSMAN->m_iMaxSongsToPlay, (PREFSMAN->m_iMaxSongsToPlay > 1 ? MAX_SONGS_TEXT_2.c_str() : MAX_SONG_TEXT_2.c_str()) );
		TrimLeft( sText );
		TrimRight( sText );
		m_textMaxSongs.SetText( sText );
		this->AddChild( &m_textMaxSongs );
	}

	// Max stages info
	{
		m_textMaxStages.LoadFromFont( THEME->GetPathF(m_sName, "MaxStages") );
		m_textMaxStages.Command( MAX_STAGES_ON_COMMAND );
		CString sText = PREFSMAN->m_bEventMode ? MAX_STAGES_EVENT : ssprintf( "%s %d %s", (PREFSMAN->m_iNumArcadeStages > 1 ? MAX_STAGES_TEXT_1.c_str() : MAX_STAGE_TEXT_1.c_str()), PREFSMAN->m_iNumArcadeStages, (PREFSMAN->m_iNumArcadeStages > 1 ? MAX_STAGES_TEXT_2.c_str() : MAX_STAGE_TEXT_2.c_str()) );
		TrimLeft( sText );
		TrimRight( sText );
		m_textMaxStages.SetText( sText );
		this->AddChild( &m_textMaxStages );
	}

	int iLifeDifficulty;
	const CStringArray dummy;
	m_textLifeDifficulty.LoadFromFont( THEME->GetPathF(m_sName, "LifeDifficulty") );
	m_textLifeDifficulty.Command( LIFE_DIFFICULTY_ON_COMMAND );
	LifeDifficulty( iLifeDifficulty, true, dummy );
	iLifeDifficulty++;	// LifeDifficulty returns an index
	m_textLifeDifficulty.SetText( ssprintf( "%s %d", LIFE_DIFFICULTY_TEXT.c_str(), iLifeDifficulty ) );
	this->AddChild( &m_textLifeDifficulty );

#if defined( WITH_COIN_MODE )
	CString sCoinMode = PREFSMAN->CoinModeToString(PREFSMAN->GetCoinMode());
	m_CoinMode.LoadFromAniDir( THEME->GetPathB(m_sName, sCoinMode) );
	this->AddChild( &m_CoinMode );

	m_textCoinHelp.LoadFromFont( THEME->GetPathF(m_sName, "help") );
	m_textCoinHelp.SetText( HELP_TEXT(PREFSMAN->GetCoinMode()) );
	m_textCoinHelp.SetXY( HELP_X, HELP_Y );
	m_textCoinHelp.SetZoom( 0.5f );
	m_textCoinHelp.SetEffectDiffuseBlink();
	m_textCoinHelp.SetShadowLength( 2 );
	this->AddChild( &m_textCoinHelp );

	switch( PREFSMAN->GetCoinMode() )
	{
	case PrefsManager::COIN_HOME:
#endif
		for( unsigned i=0; i<m_aModeChoices.size(); i++ )
		{
			ModeChoice &mc = m_aModeChoices[i];

			m_textChoice[i].LoadFromFont( THEME->GetPathF(m_sName, "choices") );
			m_textChoice[i].SetHorizAlign( (enum Actor::HorizAlign)MENU_TEXT_ALIGN );
			m_textChoice[i].SetText( NAME(mc.m_sName) );
			m_textChoice[i].SetXY( CHOICES_X, CHOICES_START_Y + i*CHOICES_SPACING_Y );
			m_textChoice[i].SetShadowLength( CHOICES_SHADOW_LENGTH );
			this->AddChild( &m_textChoice[i] );
		}
#if defined( WITH_COIN_MODE )
		break;
	case PrefsManager::COIN_PAY:
	case PrefsManager::COIN_FREE:
		break;
	default:
		ASSERT(0);
	}
#endif

//	m_AttractOut.Load( THEME->GetPathToB("ScreenTitleMenu out") );
//	this->AddChild( &m_AttractOut );

//	m_BeginOut.Load( THEME->GetPathToB("ScreenTitleMenu begin out") );
//	this->AddChild( &m_BeginOut );

	SOUND->PlayOnceFromAnnouncer( "title menu game name" );

	m_soundChange.Load( THEME->GetPathS(m_sName, "change"), true );

	m_Choice = 0;

	for( unsigned i=0; i<m_aModeChoices.size(); i++ )
	{
		if( i != m_Choice )
		{
			m_textChoice[i].SetZoom( ZOOM_NOT_SELECTED );
			m_textChoice[i].Command( MENU_ITEM_CREATE );
		}
		else
			GainFocus( m_Choice );
	}

	SOUND->PlayMusic( THEME->GetPathS(m_sName, "music") );

	this->PostScreenMessage( SM_PlayComment, SECONDS_BETWEEN_COMMENTS);
	this->SortByDrawOrder();
//	this->MoveToTail( &m_AttractOut );	// put it in the back so it covers up the stuff we just added
}

ScreenTitleMenu::~ScreenTitleMenu()
{
	LOG->Trace( "ScreenTitleMenu::~ScreenTitleMenu()" );
}

void ScreenTitleMenu::UpdateSelectableChoices()
{

}

void ScreenTitleMenu::MoveCursor( bool up )
{
#if defined( WITH_COIN_MODE )
	if( PREFSMAN->GetCoinMode() != PrefsManager::COIN_HOME )
		return;
#endif

//	if( m_BeginOut.IsTransitioning() )
//		return;

	TimeToDemonstration.GetDeltaTime();	/* Reset the demonstration timer when a key is pressed. */
	LoseFocus( m_Choice );
	m_Choice += (up? -1:+1);
	wrap( (int&)m_Choice, m_aModeChoices.size() );
	m_soundChange.Play();
	GainFocus( m_Choice );
}

void ScreenTitleMenu::Input( const DeviceInput& DeviceI, const InputEventType type, const GameInput &GameI, const MenuInput &MenuI, const StyleInput &StyleI )
{
	LOG->Trace( "ScreenTitleMenu::Input( %d-%d )", DeviceI.device, DeviceI.button );	// debugging gameport joystick problem

	if( type != IET_FIRST_PRESS )
		return;

	if( DeviceI.device == DEVICE_KEYBOARD )
	{
		switch( DeviceI.button )
		{
		case KEY_F4:
		case KEY_F5:
		case KEY_F6:
		{
			const int iShiftFlag = INPUTFILTER->GetKeyFlagShift();

			switch( DeviceI.button )
			{
			case KEY_F4:	NextGame( iShiftFlag != HOLDING_NONE );		return;
			case KEY_F5:	NextTheme( iShiftFlag != HOLDING_NONE );		return;
			case KEY_F6:	NextAnnouncer( iShiftFlag != HOLDING_NONE );	return;
			}
		}
#if defined( WITH_COIN_MODE )
		default:
			/* If the CoinMode was changed, we need to reload this screen
			* so that the right m_aModeChoices will show */
			if( ScreenAttract::ChangeCoinModeInput( DeviceI, type, GameI, MenuI, StyleI ) )
			{
				SCREENMAN->SetNewScreen( "ScreenTitleMenu" );
				return;
			}
#endif
		}
	}

	if( m_In.IsTransitioning() || m_Back.IsTransitioning() ) /* not m_Out */
		return;

	if( MenuI.IsValid() )
	{
		switch( MenuI.button )
		{
		case MENU_BUTTON_UP:
		case MENU_BUTTON_LEFT:
			MoveCursor( true );
			break;
		case MENU_BUTTON_DOWN:
		case MENU_BUTTON_RIGHT:
			MoveCursor( false );
			break;
		case MENU_BUTTON_BACK:
			if( m_Out.IsTransitioning() )
				break;
			Back( SM_GoToAttractLoop );
			break;
		case MENU_BUTTON_START:
			/* return if the choice is invalid */
			const ModeChoice &mc = m_aModeChoices[m_Choice];
			CString why;
			if( !mc.IsPlayable( &why ) )
			{
				SCREENMAN->PlayInvalidSound();
				if( why != "" )
					SCREENMAN->SystemMessage( why );
				return;
			}

			if( !Screen::JoinInput( DeviceI, type, GameI, MenuI, StyleI ) )
				return;

			if( !m_Out.IsTransitioning() )
				StartTransitioning( SM_GoToNextScreen );
		}
	}

	// Aldo_MX: I hate CodeDetector, it's really buggy and it doesn't always work.
	// If someone uses those codes, please tell to do not remove them.
	// detect codes
	if( CodeDetector::EnteredCode(GameI.controller,CodeDetector::CODE_NEXT_THEME) ||
		CodeDetector::EnteredCode(GameI.controller,CodeDetector::CODE_NEXT_THEME2) )
	{
		NextTheme();
		return;
	}

	if( CodeDetector::EnteredCode(GameI.controller,CodeDetector::CODE_NEXT_ANNOUNCER) ||
		CodeDetector::EnteredCode(GameI.controller,CodeDetector::CODE_NEXT_ANNOUNCER2) )
	{
		NextAnnouncer();
		return;
	}

	if( CodeDetector::EnteredCode(GameI.controller,CodeDetector::CODE_NEXT_GAME) ||
		CodeDetector::EnteredCode(GameI.controller,CodeDetector::CODE_NEXT_GAME2) )
	{
		NextGame();
		return;
	}

	ScreenSelect::Input( DeviceI, type, GameI, MenuI, StyleI );
}

void ScreenTitleMenu::Update( float fDelta )
{
	// time out on this screen and go to the attract sequence
	if( !IsTransitioning() && TimeToDemonstration.PeekDeltaTime() >= SECONDS_BEFORE_ATTRACT)
	{
#if defined( WITH_COIN_MODE )
		// don't time out on this screen is coin mode is pay.
		// If we're here, then there's a credit in the machine.
		if( PREFSMAN->GetCoinMode() == PrefsManager::COIN_PAY )
			;	// do nothing
		else
#endif
		{
			this->PostScreenMessage( SM_GoToAttractLoop, 0 );
			TimeToDemonstration.GetDeltaTime();
		}
	}

	Screen::Update(fDelta);
}

void ScreenTitleMenu::HandleScreenMessage( const ScreenMessage SM )
{
	switch( SM )
	{
	case SM_PlayComment:
		SOUND->PlayOnceFromAnnouncer( "title menu attract" );
		this->PostScreenMessage( SM_PlayComment, SECONDS_BETWEEN_COMMENTS );
		break;

	case SM_GoToNextScreen:
		/* XXX: Bad hack: we only want to set default options if we're actually
		 * going into the game.  We don't want to set it if we're going into the
		 * editor menu, since that never resets options (so it'll maintain changes
		 * when entering and exiting the editor), and the editor probably shouldn't
		 * use default options. */
		if( m_Choice == 0 )
		{
			// apply default options
			FOREACH_PlayerNumber( p )
				GAMESTATE->m_PlayerOptions[p].FromString( PREFSMAN->m_sDefaultModifiers );
			GAMESTATE->m_SongOptions.FromString( PREFSMAN->m_sDefaultModifiers );
		}
		m_aModeChoices[m_Choice].ApplyToAllPlayers();
		break;

	case SM_GoToAttractLoop:
		SCREENMAN->SetNewScreen( INITIAL_SCREEN );
		break;

	default:
		Screen::HandleScreenMessage(SM);
	}
}

void ScreenTitleMenu::LoseFocus( int iChoiceIndex )
{
   if(LEGACY_CHOICE_ANIM)
   {
      m_textChoice[iChoiceIndex].SetEffectNone();
      m_textChoice[iChoiceIndex].StopTweening();
      m_textChoice[iChoiceIndex].BeginTweening( 0.3f );
      m_textChoice[iChoiceIndex].SetZoom( ZOOM_NOT_SELECTED );
   }
   else
   {
      m_textChoice[iChoiceIndex].Command(CHOICE_LOSE_FOCUS_COMMAND);
   }
}

void ScreenTitleMenu::NextAnnouncer( bool bPrevious )
{
	ANNOUNCER->NextAnnouncer( bPrevious );
	CString sName = ANNOUNCER->GetCurAnnouncerName();
	if( sName=="" ) sName = "(none)";
	SCREENMAN->SystemMessage( "Announcer: "+sName );
	SCREENMAN->SetNewScreen( "ScreenTitleMenu" );
}

void ScreenTitleMenu::NextGame( bool bPrevious )
{
	vector<const Game*> vGames;
	GAMEMAN->GetEnabledGames( vGames );
	ASSERT( !vGames.empty() );

	vector<const Game*>::iterator iter = find(vGames.begin(),vGames.end(),GAMESTATE->GetCurrentGame());
	ASSERT( iter != vGames.end() );

	if( bPrevious )
	{
		// wrap
		if( iter == vGames.begin() )
			iter = vGames.end();

		iter--;	// move to the previous game
	}
	else
	{
		iter++;	// move to the next game

		// wrap
		if( iter == vGames.end() )
			iter = vGames.begin();
	}

	ChangeCurrentGame( *iter );

	/* Reload the theme if it's changed, but don't back to the initial screen. */
	GAMESTATE->Reset();
	ResetGame( false );

	SCREENMAN->SystemMessage( CString("Game: ") + GAMESTATE->GetCurrentGame()->m_szDescription );
	SCREENMAN->SetNewScreen( "ScreenTitleMenu" );
}

void ScreenTitleMenu::NextTheme( bool bPrevious )
{
	THEME->NextTheme( bPrevious );
	ApplyGraphicOptions();	// update window title and icon
	SCREENMAN->SystemMessage( "Theme: "+THEME->GetCurThemeName() );
	SCREENMAN->SetNewScreen( "ScreenTitleMenu" );
	TEXTUREMAN->DoDelayedDelete();
}

void ScreenTitleMenu::GainFocus( int iChoiceIndex )
{
   if(LEGACY_CHOICE_ANIM)
   {
      m_textChoice[iChoiceIndex].StopTweening();
      m_textChoice[iChoiceIndex].BeginTweening( 0.3f );
      m_textChoice[iChoiceIndex].SetZoom( ZOOM_SELECTED );
      RageColor color1, color2;
      color1 = COLOR_SELECTED;
      color2 = color1 * 0.5f;
      color2.a = 1;
      m_textChoice[iChoiceIndex].SetEffectDiffuseShift( 0.5f, color1, color2 );
   }
   else
   {
      m_textChoice[iChoiceIndex].Command(CHOICE_GAIN_FOCUS_COMMAND);
   }
}

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * Contains code (c) 2001-2004 Chris Danford.
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
