#include "global.h"
#include "Player.h"
#include "GameConstantsAndTypes.h"
#include "RageUtil.h"
#include "PrefsManager.h"
#include "GameConstantsAndTypes.h"
#include "GameManager.h"
#include "InputMapper.h"
#include "SongManager.h"
#include "GameState.h"
#include "ScoreKeeper.h"
#include "RageLog.h"
#include "RageMath.h"
#include "RageTimer.h"
#include "RageDisplay.h"
#include "ThemeManager.h"
#include "Combo.h"
#include "ScoreDisplay.h"
#include "LifeMeter.h"
#include "CombinedLifeMeter.h"
#include "PlayerAI.h"
#include "NoteFieldPositioning.h"
#include "NoteDataUtil.h"
#include "ScreenGameplay.h" /* for SM_ComboStopped */
#include "ScreenManager.h"
#include "StageStats.h"
#include "ArrowEffects.h"
#include "Game.h"
#include "NetworkSyncManager.h"	//used for sending timing offset
#include "DancingCharacters.h"
#include "Steps.h"
#include "NoteData.h"
#include "NoteTypes.h"
#include "PlayerContainer.h"

CachedThemeMetricF	GRAY_ARROWS_Y_STANDARD		("Player","ReceptorArrowsYStandard");
CachedThemeMetricF	GRAY_ARROWS_Y_REVERSE		("Player","ReceptorArrowsYReverse");
CachedThemeMetricF	LR_GRAY_ARROWS_Y_STANDARD	("Player","LRReceptorArrowsYStandard");
CachedThemeMetricF	LR_GRAY_ARROWS_Y_REVERSE	("Player","LRReceptorArrowsYReverse");

#define JUDGMENT_X( p, both_sides )			THEME->GetMetricF("Player",both_sides ? CString("JudgmentXOffsetBothSides") : ssprintf("JudgmentXOffsetOneSideP%d",p+1))
#define JUDGMENT_Y							THEME->GetMetricF("Player","JudgmentY")
#define JUDGMENT_Y_REVERSE					THEME->GetMetricF("Player","JudgmentYReverse")
#define JUDGMENT_Y_CENTERED					THEME->GetMetricF("Player","JudgmentYCentered")

#define COMBO_X( p, both_sides )			THEME->GetMetricF("Player",both_sides ? CString("ComboXOffsetBothSides") : ssprintf("ComboXOffsetOneSideP%d",p+1))
#define COMBO_Y								THEME->GetMetricF("Player","ComboY")
#define COMBO_Y_REVERSE						THEME->GetMetricF("Player","ComboYReverse")
#define COMBO_Y_CENTERED					THEME->GetMetricF("Player","ComboYCentered")

CachedThemeMetricF	LR_JUDGMENT_Y	("Player","LRJudgmentY");
CachedThemeMetricF	LR_COMBO_Y		("Player","LRComboY");

#define ATTACK_DISPLAY_X( p, both_sides )	THEME->GetMetricF("Player",both_sides ? CString("AttackDisplayXOffsetBothSides") : ssprintf("AttackDisplayXOffsetOneSideP%d",p+1))
#define ATTACK_DISPLAY_Y					THEME->GetMetricF("Player","AttackDisplayY")
#define ATTACK_DISPLAY_Y_REVERSE			THEME->GetMetricF("Player","AttackDisplayYReverse")

CachedThemeMetricF	HOLD_JUDGMENT_Y_STANDARD	("Player","HoldJudgmentYStandard");
CachedThemeMetricF	HOLD_JUDGMENT_Y_REVERSE		("Player","HoldJudgmentYReverse");
CachedThemeMetricF	HOLD_JUDGMENT_Y_CENTERED	("Player","HoldJudgmentYCentered");

CachedThemeMetricI	BRIGHT_GHOST_COMBO_THRESHOLD	("Player","BrightGhostComboThreshold");

CachedThemeMetricB	TAP_JUDGMENTS_UNDER_FIELD	("Player","TapJudgmentsUnderField");
CachedThemeMetricB	HOLD_JUDGMENTS_UNDER_FIELD	("Player","HoldJudgmentsUnderField");
CachedThemeMetricB	COMBO_UNDER_FIELD			("Player","ComboUnderField");

CachedThemeMetricF	START_DRAWING_AT_PIXELS		("Player","StartDrawingAtPixels");
CachedThemeMetricF	STOP_DRAWING_AT_PIXELS		("Player","StopDrawingAtPixels");
CachedThemeMetricF	LR_START_DRAWING_AT_PIXELS	("Player","LRStartDrawingAtPixels");
CachedThemeMetricF	LR_STOP_DRAWING_AT_PIXELS	("Player","LRStopDrawingAtPixels");
CachedThemeMetricB	LR_ZOOM_AFFECTS_JUDGMENT	("Player","LRZoomAffectsJugdment");

#define MAX_PRO_TIMING_ERROR				THEME->GetMetricI("Player","MaxProTimingError")

CachedThemeMetricB	CUSTOM_COMBO_PER_ROW		("CustomScoring","ComboIsPerRow");
CachedThemeMetricB	OVERRIDE_COMBO_INCREMENT	("ScoringOverride","OverrideComboIncrementValues");
CachedThemeMetricB	OVERRIDE_COMBO_PER_ROW		("ScoringOverride","ComboIsPerRow");

CachedThemeMetricB	MINI_RESIZES_JUDGMENT		("Judgment","MiniResizesJudgment");
CachedThemeMetricB	TINY_RESIZES_JUDGMENT		("Judgment","TinyResizesJudgment");

// The former is used only by a special condition for rolls
#define NON_ADJUSTED_WINDOW( judge ) (PREFSMAN->m_fJudgeWindowSeconds##judge)
#define ADJUSTED_WINDOW( judge ) ((PREFSMAN->m_fJudgeWindowSeconds##judge * PREFSMAN->m_fJudgeWindowScale) + PREFSMAN->m_fJudgeWindowAdd)
#define ADJUSTED_WINDOW_AFTER( judge ) ((PREFSMAN->m_fJudgeWindowSecondsAfter##judge * PREFSMAN->m_fJudgeWindowScaleAfter) + PREFSMAN->m_fJudgeWindowAddAfter)

PlayerMinus::PlayerMinus() : m_EmptyNote(TAP_EMPTY)
{
	GRAY_ARROWS_Y_STANDARD.Refresh();
	GRAY_ARROWS_Y_REVERSE.Refresh();
	LR_GRAY_ARROWS_Y_STANDARD.Refresh();
	LR_GRAY_ARROWS_Y_REVERSE.Refresh();

	LR_JUDGMENT_Y.Refresh();
	LR_COMBO_Y.Refresh();

	HOLD_JUDGMENT_Y_STANDARD.Refresh();
	HOLD_JUDGMENT_Y_CENTERED.Refresh();
	HOLD_JUDGMENT_Y_REVERSE.Refresh();

	BRIGHT_GHOST_COMBO_THRESHOLD.Refresh();

	TAP_JUDGMENTS_UNDER_FIELD.Refresh();
	HOLD_JUDGMENTS_UNDER_FIELD.Refresh();
	COMBO_UNDER_FIELD.Refresh();

	START_DRAWING_AT_PIXELS.Refresh();
	STOP_DRAWING_AT_PIXELS.Refresh();
	LR_START_DRAWING_AT_PIXELS.Refresh();
	LR_STOP_DRAWING_AT_PIXELS.Refresh();
	LR_ZOOM_AFFECTS_JUDGMENT.Refresh();

	CUSTOM_COMBO_PER_ROW.Refresh();
	OVERRIDE_COMBO_INCREMENT.Refresh();
	OVERRIDE_COMBO_PER_ROW.Refresh();

	MINI_RESIZES_JUDGMENT.Refresh();
	TINY_RESIZES_JUDGMENT.Refresh();

	m_PlayerNumber = PLAYER_INVALID;

	m_pLifeMeter = NULL;
	m_pCombinedLifeMeter = NULL;
	m_pScoreDisplay = NULL;
	m_pSecondaryScoreDisplay = NULL;
	m_pPrimaryScoreKeeper = NULL;
	m_pSecondaryScoreKeeper = NULL;
	m_pInventory = NULL;

	m_iOffsetSample = 0;

	this->AddChild( &m_ArrowBackdrop );
	this->AddChild( &m_ProTimingDisplay );
	this->AddChild( &m_Combo );
	this->AddChild( &m_AttackDisplay );

	for( int c=0; c<MAX_NOTE_TRACKS; c++ )
	{
		this->AddChild( &m_HoldJudgment[c] );
		m_HoldPressed[c] = -1;
		m_iCurrentHoldEndRow[c] = -1;
	}

	m_fLRAddY = 0.f;
	m_fLRStartY = 0.f;
	m_fLRJudgmentY = 0.f;
	m_fLRComboY = 0.f;
	m_fLRNoteFieldZoom = 1.0f;
	m_fCachedZoom = 1.0f;

	m_fCachedRotationX = FLT_MAX;
	m_fCachedRotationY = FLT_MAX;
	m_fCachedRotationZ = FLT_MAX;
	m_fCachedScrollDrop = FLT_MAX;

	PlayerAI::InitFromDisk();
}

PlayerMinus::~PlayerMinus()
{
	SAFE_DELETE(m_Judgment);
}

void PlayerMinus::InitJudgment( PlayerNumber pn, Difficulty dc )
{
	m_Judgment = new Judgment(pn);
	m_Judgment->SetName( "Judgment" );
	m_Judgment->Load( dc );
	this->AddChild( m_Judgment );
}

void PlayerMinus::Load( PlayerNumber pn, const NoteData* pNoteData, LifeMeter* pLM, CombinedLifeMeter* pCombinedLM, ScoreDisplay* pScoreDisplay, ScoreDisplay* pSecondaryScoreDisplay, Inventory* pInventory, ScoreKeeper* pPrimaryScoreKeeper, ScoreKeeper* pSecondaryScoreKeeper, NoteField* pNoteField )
{
	m_iDCState = AS2D_IDLE;
	//LOG->Trace( "PlayerMinus::Load()", );

	m_PlayerNumber = pn;
	m_pLifeMeter = pLM;
	m_pCombinedLifeMeter = pCombinedLM;
	m_pScoreDisplay = pScoreDisplay;
	m_pSecondaryScoreDisplay = pSecondaryScoreDisplay;
	m_pInventory = pInventory;
	m_pPrimaryScoreKeeper = pPrimaryScoreKeeper;
	m_pSecondaryScoreKeeper = pSecondaryScoreKeeper;
	m_pNoteField = pNoteField;
	m_Timing = &GAMESTATE->m_pCurSteps[pn]->m_Timing;

	GAMESTATE->ResetNoteSkinsForPlayer( pn );

	// Ensure that this is up-to-date.
	GAMESTATE->m_pPosition->Load(pn);

	const Style* pStyle = GAMESTATE->GetCurrentStyle();
	const PlayerOptions& po = GAMESTATE->m_PlayerOptions[pn];

	// init scoring
	NoteDataWithScoring::Init();

	// copy note data
	this->CopyAll( pNoteData );

	if( po.m_LifeType == PlayerOptions::LIFE_BATTERY && g_CurStageStats.bFailed[pn] )	// Oni dead
		this->ClearAll();

	/* The editor reuses Players ... so we really need to make sure everything
	 * is reset and not tweening.  Perhaps ActorFrame should recurse to subactors;
	 * then we could just this->StopTweening()? -glenn */
	m_Judgment->StopTweening();

	// don't reset combos between songs in a course!
	if( GAMESTATE->m_bEditing )
	{
		g_CurStageStats.iCurCombo[pn] = 0;
		g_CurStageStats.iCurMissCombo[pn] = 0;
		m_Combo.Reset();
	}

	m_Combo.Init( pn );

	if( po.m_bReverseGrade )
		m_Combo.SetCombo( g_CurStageStats.iCurMissCombo[m_PlayerNumber], g_CurStageStats.iCurCombo[m_PlayerNumber] );	// combo can persist between songs and games
	else
		m_Combo.SetCombo( g_CurStageStats.iCurCombo[m_PlayerNumber], g_CurStageStats.iCurMissCombo[m_PlayerNumber] );	// combo can persist between songs and games

	m_AttackDisplay.Init( pn );
	m_Judgment->Reset();

	// Don't re-init this; that'll reload graphics.  Add a separate Reset() call
	// if some ScoreDisplays need it.
//	if( m_pScore )
//		m_pScore->Init( pn );

	// Apply transforms.
	NoteDataUtil::TransformNoteData( *this, po, GAMESTATE->GetCurrentStyle()->m_StepsType );

	switch( GAMESTATE->m_PlayMode )
	{
		case PLAY_MODE_RAVE:
		case PLAY_MODE_BATTLE:
			// ugly, ugly, ugly.  Works only w/ dance.

			// Because of this introducing bugs in other games, limit it to dance modes only.
			// Hopefully someone can fix this later. ~ Mike
			// Aldo_MX: Fixed for Pump It Up only...
			if( PREFSMAN->m_bDanceRaveShufflesNotes )
			{
				if( GAMESTATE->m_pCurGame->m_szName == CString("dance") ||
					GAMESTATE->m_pCurGame->m_szName == CString("pump") )
				{
					NoteDataUtil::TransformNoteData( *this, po, GAMESTATE->GetCurrentStyle()->m_StepsType );

					// shuffle either p1 or p2
					static int count = 0;
					switch( count )
					{
					case 0:
					case 3:
						NoteDataUtil::Turn( *this, GAMESTATE->GetCurrentStyle()->m_StepsType, NoteDataUtil::left);
						break;
					case 1:
					case 2:
						NoteDataUtil::Turn( *this, GAMESTATE->GetCurrentStyle()->m_StepsType, NoteDataUtil::right);
						break;
					default:
						ASSERT(0);
					}
					count++;
					count %= 4;
				}
			}
			break;
	}

	for( int h=0; h<GetNumHoldsAndRolls(); ++h )
	{
		HoldNote& hn = GetHoldNote(h);
		m_bPressed[hn] = false;
	}

	m_iLastHold = 0;

	m_iLastRow = BeatToNoteRow( GAMESTATE->m_fPlayerBeat[m_PlayerNumber] ) - 1;	// why this?
	m_iLastItemRow = BeatToNoteRow( GAMESTATE->m_fPlayerBeat[m_PlayerNumber] ) - 1;	// why this?
	m_iLastMissedRow = BeatToNoteRow( GAMESTATE->m_fPlayerBeat[m_PlayerNumber] ) - 1; // why this?

	for (int c = 0; c < MAX_NOTE_TRACKS; c++)
		m_bCrossingHoldNote[c] = false;
	m_LateCheckpoints.clear();

	m_ArrowBackdrop.Unload();
	CString BackdropName = g_NoteFieldMode[pn].m_Backdrop;

	if( !BackdropName.empty() )
		m_ArrowBackdrop.LoadFromAniDir( THEME->GetPathToB( BackdropName ) );

	m_ArrowBackdrop.SetPlayer( pn );

	const bool bReverse = GetReverseStatus();
	const bool bCentered = po.m_fScrolls[PlayerOptions::SCROLL_CENTERED] == 1;
	const bool bPlayerUsingBothSides = GAMESTATE->GetCurrentStyle()->m_StyleType==Style::ONE_PLAYER_TWO_CREDITS;

	m_AttackDisplay.SetX( ATTACK_DISPLAY_X(m_PlayerNumber,bPlayerUsingBothSides) );
	m_AttackDisplay.SetY( bReverse ? ATTACK_DISPLAY_Y_REVERSE : ATTACK_DISPLAY_Y );

	m_Judgment->SetX( JUDGMENT_X(m_PlayerNumber,bPlayerUsingBothSides) );
	m_Combo.SetX( COMBO_X(m_PlayerNumber,bPlayerUsingBothSides) );

	if( bReverse )
	{
		m_Judgment->SetY( JUDGMENT_Y_REVERSE );
		m_Combo.SetY( COMBO_Y_REVERSE );
	}
	else if( bCentered )
	{
		m_Judgment->SetY( JUDGMENT_Y_CENTERED );
		m_Combo.SetY( COMBO_Y_CENTERED );
	}
	else
	{
		m_Judgment->SetY( JUDGMENT_Y );
		m_Combo.SetY( COMBO_Y);
	}

	m_fLRJudgmentY = m_Judgment->GetY();
	m_fLRComboY = m_Combo.GetY();

	m_ProTimingDisplay.SetX( JUDGMENT_X(m_PlayerNumber,bPlayerUsingBothSides) );
	m_ProTimingDisplay.SetY( bReverse ? SCREEN_BOTTOM-JUDGMENT_Y : SCREEN_TOP+JUDGMENT_Y );

	// These commands add to the above positioning, and are usually empty.
	m_Judgment->Command( g_NoteFieldMode[pn].m_JudgmentCmd );
	m_ProTimingDisplay.Command( g_NoteFieldMode[pn].m_JudgmentCmd );
	m_Combo.Command( g_NoteFieldMode[pn].m_ComboCmd );
	m_AttackDisplay.Command( g_NoteFieldMode[pn].m_AttackDisplayCmd );

	for( int c=0; c<pStyle->m_iColsPerPlayer; c++ )
	{
		NoteFieldMode &mode = g_NoteFieldMode[pn];
		m_HoldJudgment[c].Command( mode.m_HoldJudgmentCmd[c] );
	}

	m_pNoteField->Load( this, pn );
	UpdateNoteFieldRotation( po, true );

	// Need to set Y positions of all these elements in Update since
	// they change depending on PlayerOptions.
	RageSoundParams p;
	m_soundMine.Load( THEME->GetPathToS("Player mine"), true );
	m_soundShock.Load( THEME->GetPathToS("Player shock"), true );
	m_soundPotion.Load( THEME->GetPathToS("Player potion"), true );

	// Attacks can be launched in course modes and in battle modes.  They both come
	// here to play, but allow loading a different sound for different modes.
	switch( GAMESTATE->m_PlayMode )
	{
	case PLAY_MODE_RAVE:
	case PLAY_MODE_BATTLE:
		m_soundAttackLaunch.Load( THEME->GetPathToS("Player battle attack launch"), true );
		m_soundAttackEnding.Load( THEME->GetPathToS("Player battle attack ending"), true );
		break;
	default:
		m_soundAttackLaunch.Load( THEME->GetPathToS("Player course attack launch"), true );
		m_soundAttackEnding.Load( THEME->GetPathToS("Player course attack ending"), true );
		break;
	}

	if( GAMESTATE->GetNumPlayersEnabled() == 2 )
	{
		// Two players are active.  Play sounds on this player's side.
		p.m_Balance = (m_PlayerNumber == PLAYER_1) ? -1.0f : 1.0f;
	}
	m_soundMine.SetParams( p );
	m_soundShock.SetParams( p );
	m_soundPotion.SetParams( p );
	m_soundAttackLaunch.SetParams( p );
	m_soundAttackEnding.SetParams( p );

	m_fActiveRandomAttackStart = -1.0f;

	// PIU Holds
	NoteDataUtil::ParseTiming( *this, *m_Timing, PREFSMAN->m_bUsePIUHolds );
}

void PlayerMinus::Update( float fDeltaTime )
{
	//LOG->Trace( "PlayerMinus::Update(%f)", fDeltaTime );

	if( GAMESTATE->m_pCurSong==NULL )
		return;

	const PlayerOptions& po = GAMESTATE->m_CurrentPlayerOptions[m_PlayerNumber];

	if( GAMESTATE->m_bAttackBeganThisUpdate[m_PlayerNumber] && PREFSMAN->m_bPlayAttackSounds )
		m_soundAttackLaunch.Play();
	if( GAMESTATE->m_bAttackEndedThisUpdate[m_PlayerNumber] && PREFSMAN->m_bPlayAttackSounds )
		m_soundAttackEnding.Play();

	//
	// Random Attack Mod
	//
	if( po.m_bRandomAttacks )
	{
		float fCurrentGameTime = g_CurStageStats.fGameplaySeconds;
		float fAttackRunTime = PREFSMAN->m_fRandomAttackLength;

		// Don't start until 1 seconds into game, minimum
		if( fCurrentGameTime > 1)
		{
			// Update the attack if it's been a while since we ran the last attack
			if( (fCurrentGameTime - m_fActiveRandomAttackStart) > PREFSMAN->m_fTimeBetweenRandomAttacks )
			{
				m_fActiveRandomAttackStart = fCurrentGameTime;

				Attack attRandomAttack;
				attRandomAttack.sModifier = ApplyRandomAttack();
				attRandomAttack.fSecsRemaining = fAttackRunTime;
				GAMESTATE->LaunchAttack( m_PlayerNumber, attRandomAttack);
			}
		}
	}

	//
	// NoteField
	//
	UpdateNoteFieldRotation( po );
	m_pNoteField->Update( fDeltaTime );

	float fPercentReverse = po.GetReversePercentForColumn(0);
	float fGrayYPos = SCALE( fPercentReverse, 0.f, 1.f, GRAY_ARROWS_Y_STANDARD, GRAY_ARROWS_Y_REVERSE );
	m_ArrowBackdrop.SetY( fGrayYPos );

	//
	// Update Y positions
	//
	{
		for( int c=0; c<GAMESTATE->GetCurrentStyle()->m_iColsPerPlayer; c++ )
		{
			bool bReverse = GetReverseStatus();

			float fPercentCentered = po.m_fScrolls[PlayerOptions::SCROLL_CENTERED];
			bool bCentered = fPercentCentered != 0;

			float fHoldJudgeYPos;

			if( bReverse )
			{
				fHoldJudgeYPos = SCALE( fPercentReverse, 0.f, 1.f, HOLD_JUDGMENT_Y_STANDARD, HOLD_JUDGMENT_Y_REVERSE );
				// float fGrayYPos = SCALE( fPercentReverse, 0.f, 1.f, GRAY_ARROWS_Y_STANDARD, GRAY_ARROWS_Y_REVERSE );
			}
			else if( bCentered )
			{
				fHoldJudgeYPos = SCALE( fPercentCentered, 0.f, 1.f, HOLD_JUDGMENT_Y_STANDARD, HOLD_JUDGMENT_Y_CENTERED );
				// float fGrayYPos = SCALE( fPercentReverse, 0.f, 1.f, GRAY_ARROWS_Y_STANDARD, GRAY_ARROWS_Y_CENTERED );
			}
			else
			{
				fHoldJudgeYPos = HOLD_JUDGMENT_Y_STANDARD;
				// float fGrayYPos = GRAY_ARROWS_Y_STANDARD;
			}

			const float fX = ArrowGetXPos( m_PlayerNumber, c, 0 );
			const float fZ = ArrowGetZPos( m_PlayerNumber, 0 );

			m_HoldJudgment[c].SetX( fX );
			m_HoldJudgment[c].SetY( fHoldJudgeYPos );
			m_HoldJudgment[c].SetZ( fZ );
		}
	}

	//
	// Mini & Tiny Mods
	//
	const float& fMiniPercent = po.m_fEffects[PlayerOptions::EFFECT_MINI];
	const float& fTinyPercent = po.m_fEffects[PlayerOptions::EFFECT_TINY];

	float fMiniTinyZoom = min( powf(0.5f, fMiniPercent+fTinyPercent), 1.0f ),
		fLRJudgmentZoom = LR_ZOOM_AFFECTS_JUDGMENT ? m_fCachedZoom : 1.f;

	if( ( MINI_RESIZES_JUDGMENT && fMiniPercent != 0 ) || ( TINY_RESIZES_JUDGMENT && fTinyPercent != 0 ) )
		m_Judgment->SetZoom( fMiniTinyZoom * fLRJudgmentZoom );

	m_Combo.SetZoom( fLRJudgmentZoom );

	m_pNoteField->SetZoom( fMiniTinyZoom );

	//
	// update pressed flag
	//
	const int iNumCols = GAMESTATE->GetCurrentStyle()->m_iColsPerPlayer;
	ASSERT_M( iNumCols < MAX_COLS_PER_PLAYER, ssprintf("%i >= %i", iNumCols, MAX_COLS_PER_PLAYER) );
	for( int col=0; col < iNumCols; ++col )
	{
		CHECKPOINT_M( ssprintf("%i %i", col, iNumCols) );

		const StyleInput StyleI( m_PlayerNumber, col );
		const GameInput GameI = GAMESTATE->GetCurrentStyle()->StyleInputToGameInput( StyleI );
		bool bIsHoldingButton = INPUTMAPPER->IsButtonDown( GameI );

		// TODO: Make this work for non-human-controlled players
		if( bIsHoldingButton && !GAMESTATE->m_bDemonstrationOrJukebox && GAMESTATE->m_PlayerController[m_PlayerNumber]==PC_HUMAN )
			m_pNoteField->SetPressed( col );
	}

	// Why was this originally "BeatToNoteRow"?  It should be rounded.  -Chris
	/* We want to send the crossed row message exactly when we cross the row--not
		* .5 before the row.  Use a very slow song (around 2 BPM) as a test case: without
		* rounding, autoplay steps early. -glenn */
	const int iRow = BeatToNoteRowRounded( GAMESTATE->m_fPlayerBeat[m_PlayerNumber] );
	const int iRowNow = BeatToNoteRow( GAMESTATE->m_fPlayerBeat[m_PlayerNumber] );

	UpdateHoldNotes( fDeltaTime, iRow );

	if( GAMESTATE->IsPlayerEnabled(m_PlayerNumber) )
	{
		float fBPS, fBeat, fPositionSeconds;
		bool bFreeze, bDelay;
		fPositionSeconds = GAMESTATE->m_fMusicSeconds - PREFSMAN->m_fPadStickSeconds;
		m_Timing->GetBeatAndBPSFromElapsedTime( fPositionSeconds, fBeat, fBPS, bFreeze, bDelay );

		//
		// Update Steps & Checkpoints Logic
		//
		if( iRowNow >= 0 )
		{
			while( m_iLastRow <= iRowNow )  // for each index we crossed since the last update
			{
				// HACK: First step has issues with stop/delays, maybe because it has not been updated yet...
				if( iRowNow <= 0 )
				{
					if( m_Timing->m_StopSegments.size() > 0 )
					{
						GAMESTATE->m_bStop[m_PlayerNumber] = m_Timing->m_StopSegments[0].m_fBeat == 0.f;

						if( GAMESTATE->m_bStop[m_PlayerNumber] )
						{
							GAMESTATE->m_bDelay[m_PlayerNumber] = m_Timing->m_StopSegments[0].m_bDelay;
							GAMESTATE->m_bFreeze[m_PlayerNumber] = !m_Timing->m_StopSegments[0].m_bDelay;
						}
					}
				}

				if( GAMESTATE->m_bDelay[m_PlayerNumber] && m_iLastRow == iRowNow )
					break;

				CrossedRow( m_iLastRow );

				float fStopStart = m_Timing->GetElapsedTimeFromBeat( NoteRowToBeat( m_iLastRow ) ),
					fStopSeconds = m_Timing->GetStopAtBeat( NoteRowToBeat( m_iLastRow ), bDelay );

				bool bStop = !bDelay && fStopSeconds > 0;

				// HACK: Stops have issues with late checkpoints, so use the old behavior here.
				if( UpdateHoldCheckpoints( m_iLastRow, fStopStart, fStopSeconds, true, bStop ) )
					m_LateCheckpoints[m_iLastRow] = true;

				if( GAMESTATE->m_bFreeze[m_PlayerNumber] && m_iLastRow == iRowNow )
					break;

				++m_iLastRow;
			}
		}

		//
		// Update items
		//
		{
			int iRowItem = BeatToNoteRow( fBeat );

			if( iRowItem >= 0 )
				for( ; m_iLastItemRow <= iRowItem; ++m_iLastItemRow )  // for each index we crossed since the last update
					CrossedItemRow( m_iLastItemRow );
		}

		//
		// Update Late Checkpoints Logic
		//
		if( !m_LateCheckpoints.empty() )
		{
			// TRICKY:
			fPositionSeconds -= ADJUSTED_WINDOW_AFTER(Long);
			m_Timing->GetBeatAndBPSFromElapsedTime( fPositionSeconds, fBeat, fBPS, bFreeze, bDelay );

			int iRowCP = BeatToNoteRow( fBeat );

			if( bDelay )
				iRowCP--;

			if( iRowCP >= 0 )
			{
				for( map<int,bool>::iterator it = m_LateCheckpoints.begin(); it != m_LateCheckpoints.end(); ++it )
				{
repeat_iteration:
					float fStopStart = m_Timing->GetElapsedTimeFromBeat( NoteRowToBeat( it->first ) ),
						fStopSeconds = m_Timing->GetStopLengthAtBeat( NoteRowToBeat( it->first ) );

					if( it->first < iRowCP )
						it->second = UpdateHoldCheckpoints( it->first, fStopStart, fStopSeconds, false, true );
					else
						it->second = UpdateHoldCheckpoints( it->first, fStopStart, fStopSeconds, false );

					if( !it->second )
					{
						m_LateCheckpoints.erase( it );

						if( m_LateCheckpoints.empty() )
							break;
						else
						{
							it = m_LateCheckpoints.begin();
							goto repeat_iteration;
						}
					}
				}
			}
		}

		//
		// Check for TapNote & checkpoint misses
		//
		// TRICKY:
		fPositionSeconds = GAMESTATE->m_fMusicSeconds - GetMaxStepDistanceSeconds() - PREFSMAN->m_fPadStickSeconds;
		m_Timing->GetBeatAndBPSFromElapsedTime( fPositionSeconds, fBeat, fBPS, bFreeze, bDelay );
		{
			int iRowMiss = BeatToNoteRow( fBeat );

			// If there is a freeze on iMissIfOlderThanThisIndex, include this index too.
			// Otherwise we won't show misses for tap notes on freezes until the
			// freeze finishes.
			if( bDelay )
				iRowMiss--;

			if( iRowMiss >= 0 )
			{
				while( m_iLastMissedRow <= iRowMiss )  // for each index we crossed since the last update
				{
					UpdateTapNotesMissedOlderThan( m_iLastMissedRow, bFreeze );

					if( bFreeze && m_iLastMissedRow == iRowMiss )
						break;

					++m_iLastMissedRow;
				}
			}
		}
	}

	// process transforms that are waiting to be applied
	ApplyWaitingTransforms();

	/* Cache any newly-used note skins.  Normally, the only new skins cached now are
	 * when we're adding course modifiers at the start of a song.  If this is spending
	 * time loading skins in the middle of a song, something is wrong. */
	m_pNoteField->CacheAllUsedNoteSkins();

	ActorFrame::Update( fDeltaTime );
}

void PlayerMinus::UpdateNoteFieldRotation( const PlayerOptions& po, bool bForced )
{
	if( !bForced && po.m_fNoteFieldRotationX == m_fCachedRotationX &&
		po.m_fNoteFieldRotationY == m_fCachedRotationY &&
		po.m_fNoteFieldRotationZ == m_fCachedRotationZ &&
		po.m_fScrolls[PlayerOptions::SCROLL_DROP] == m_fCachedScrollDrop )
		return;

	m_fCachedRotationX = po.m_fNoteFieldRotationX;
	m_fCachedRotationY = po.m_fNoteFieldRotationY;
	m_fCachedRotationZ = po.m_fNoteFieldRotationZ;
	m_fCachedScrollDrop = po.m_fScrolls[PlayerOptions::SCROLL_DROP];

	float fStartDrawingPixel = GAMESTATE->m_bEditing ? -100.f : START_DRAWING_AT_PIXELS,
		fStopDrawingPixel = GAMESTATE->m_bEditing ? 400.f : STOP_DRAWING_AT_PIXELS,
		fReverseOffset = GRAY_ARROWS_Y_REVERSE - GRAY_ARROWS_Y_STANDARD,
		fNoteFieldY = (GRAY_ARROWS_Y_REVERSE + GRAY_ARROWS_Y_STANDARD) / 2,
		fNoteFieldOffsetY = m_fLRStartY,
		fJudgmentY = m_fLRJudgmentY,
		fComboY = m_fLRComboY;

	if( !FEQ(fmodf(m_fCachedRotationZ, 180.f), 0.f, 0.001f) )
	{
		float fPercent = abs(sin(DegreeToRadian(m_fCachedRotationZ)));
		m_fCachedZoom = LERP(fPercent, 1.f, m_fLRNoteFieldZoom);

		fStartDrawingPixel = LERP( fPercent, fStartDrawingPixel, (GAMESTATE->m_bEditing ? -100.f : LR_START_DRAWING_AT_PIXELS) / m_fCachedZoom );
		fStopDrawingPixel = LERP( fPercent, fStopDrawingPixel, (GAMESTATE->m_bEditing ? 560.f : LR_STOP_DRAWING_AT_PIXELS) / m_fCachedZoom );
		fReverseOffset = LERP( fPercent, fReverseOffset, (LR_GRAY_ARROWS_Y_REVERSE - LR_GRAY_ARROWS_Y_STANDARD) / m_fCachedZoom );
		fNoteFieldY = LERP( fPercent, fNoteFieldY, (LR_GRAY_ARROWS_Y_REVERSE + LR_GRAY_ARROWS_Y_STANDARD) / (2 * m_fCachedZoom) );
		fNoteFieldOffsetY = LERP( fPercent, fNoteFieldOffsetY, m_fLRAddY / m_fCachedZoom );
		fJudgmentY = LERP( fPercent, fJudgmentY, LR_JUDGMENT_Y / m_fCachedZoom ) + fNoteFieldOffsetY;
		fComboY = LERP( fPercent, fComboY, LR_COMBO_Y / m_fCachedZoom ) + fNoteFieldOffsetY;
	}

	// Zoom + Drop Scroll
	float fDropZoomX = 1.f - abs(sin(DegreeToRadian(m_fCachedRotationZ))) * m_fCachedScrollDrop * 2.f,
		fDropZoomY = 1.f - abs(cos(DegreeToRadian(m_fCachedRotationZ))) * m_fCachedScrollDrop * 2.f;
	m_pNoteField->SetBaseZoomX( fDropZoomX * m_fCachedZoom );
	m_pNoteField->SetBaseZoomY( fDropZoomY * m_fCachedZoom );

	// Side Attacks
	m_pNoteField->SetBaseRotationX( m_fCachedRotationX );
	m_pNoteField->SetBaseRotationY( m_fCachedRotationY );
	m_pNoteField->SetBaseRotationZ( m_fCachedRotationZ );

	// Position
	m_pNoteField->UpdateDrawingArea( fStartDrawingPixel, fStopDrawingPixel, fReverseOffset );
	m_pNoteField->SetY( fNoteFieldY + fNoteFieldOffsetY );
	m_Judgment->SetY( fJudgmentY );
	m_Combo.SetY( fComboY );
}

bool PlayerMinus::GetReverseStatus()
{
	int iReversePosibilities = 0;
	PlayerOptions& po = GAMESTATE->m_PlayerOptions[m_PlayerNumber];

	if( po.GetReversePercentForColumn(0) >= 0.5f )
		iReversePosibilities++;
	if( FEQ(po.m_fNoteFieldRotationZ, 180.f, 0.001f) )
		iReversePosibilities++;
	if( po.m_fScrolls[PlayerOptions::SCROLL_DROP] >= 0.5f )
		iReversePosibilities++;

	return iReversePosibilities % 2 > 0;
}

void PlayerMinus::ApplyWaitingTransforms()
{
	if( GAMESTATE->m_ModsToApply[m_PlayerNumber].empty() )
		return;

	GAMESTATE->m_bOptionsFromAttack = true;

	for( unsigned j=0; j<GAMESTATE->m_ModsToApply[m_PlayerNumber].size(); j++ )
	{
		const Attack &mod = GAMESTATE->m_ModsToApply[m_PlayerNumber][j];
		PlayerOptions po;
		// Should this default to "" always? need it blank so we know if mod.sModifier changes the note skin.
		po.m_sNoteSkin = "";
		po.FromString( mod.sModifier );

		float fStartBeat, fEndBeat;
		mod.GetAttackBeats( GAMESTATE->m_pCurSteps[m_PlayerNumber]->m_Timing, m_PlayerNumber, fStartBeat, fEndBeat );
		fEndBeat = min( fEndBeat, GetNumBeats() );

		//LOG->Trace( "Applying transform '%s' from %f to %f to '%s'", mod.sModifier.c_str(), fStartBeat, fEndBeat,
		//	GAMESTATE->m_pCurSong->GetTranslitMainTitle().c_str() );

		if( po.m_sNoteSkin != "" )
			GAMESTATE->SetNoteSkinForBeatRange( m_PlayerNumber, po.m_sNoteSkin, fStartBeat, fEndBeat );

		NoteDataUtil::UnparseTiming( *this, *m_Timing, PREFSMAN->m_bUsePIUHolds, fStartBeat, fEndBeat );

		NoteDataUtil::TransformNoteData( *this, po, GAMESTATE->GetCurrentStyle()->m_StepsType, fStartBeat, fEndBeat );
		m_pNoteField->CopyRange( this, BeatToNoteRowRounded(fStartBeat), BeatToNoteRowRounded(fEndBeat), BeatToNoteRowRounded(fStartBeat) );

		NoteDataUtil::ParseTiming( *this, *m_Timing, PREFSMAN->m_bUsePIUHolds, fStartBeat, fEndBeat );
	}
	GAMESTATE->m_ModsToApply[m_PlayerNumber].clear();

	GAMESTATE->m_bOptionsFromAttack = false;
}

void PlayerMinus::DrawPrimitives()
{
	// May have both players in doubles (for battle play); only draw primary player.
	if( GAMESTATE->GetCurrentStyle()->m_StyleType == Style::ONE_PLAYER_TWO_CREDITS  &&
		m_PlayerNumber != GAMESTATE->m_MasterPlayerNumber )
		return;

	// Draw these below everything else.
	m_ArrowBackdrop.Draw();
	m_AttackDisplay.Draw();

	if( TAP_JUDGMENTS_UNDER_FIELD )
		DrawTapJudgments();

	if( HOLD_JUDGMENTS_UNDER_FIELD )
		DrawHoldJudgments();

	if( COMBO_UNDER_FIELD )
		if( GAMESTATE->m_PlayerOptions[m_PlayerNumber].m_fBlind == 0 )
			m_Combo.Draw();

	float fTilt = GAMESTATE->m_CurrentPlayerOptions[m_PlayerNumber].m_fPerspectiveTilt;
	float fSkew = GAMESTATE->m_CurrentPlayerOptions[m_PlayerNumber].m_fSkew;
	bool bReverse = GAMESTATE->m_CurrentPlayerOptions[m_PlayerNumber].GetReversePercentForColumn(0)>0.5;


	DISPLAY->CameraPushMatrix();
	DISPLAY->PushMatrix();

	float fCenterY = (GRAY_ARROWS_Y_STANDARD+GRAY_ARROWS_Y_REVERSE)/2;
//	float fHeight = GRAY_ARROWS_Y_REVERSE-GRAY_ARROWS_Y_STANDARD;

	DISPLAY->LoadMenuPerspective( 45, SCALE(fSkew,0.f,1.f,this->GetX(),CENTER_X), fCenterY );

	float fOriginalY = 	m_pNoteField->GetY();
	float fTiltDegrees = SCALE(fTilt,-1.f,+1.f,+30,-30) * (bReverse?-1:1);
	float fZoom = SCALE( GAMESTATE->m_CurrentPlayerOptions[m_PlayerNumber].m_fEffects[PlayerOptions::EFFECT_TINY], 0.f, 1.f, 1.f, 0.5f );

	if( fTilt > 0 )
		fZoom *= SCALE( fTilt, 0.f, 1.f, 1.f, 0.9f );
	else
		fZoom *= SCALE( fTilt, 0.f, -1.f, 1.f, 0.9f );

	float fYOffset;

	if( fTilt > 0 )
		fYOffset = SCALE( fTilt, 0.f, 1.f, 0.f, -45.f ) * (bReverse?-1:1);
	else
		fYOffset = SCALE( fTilt, 0.f, -1.f, 0.f, -20.f ) * (bReverse?-1:1);

	m_pNoteField->SetY( fOriginalY + fYOffset );
	m_pNoteField->SetZoom( fZoom );
	m_pNoteField->SetRotationX( fTiltDegrees );
	m_pNoteField->Draw();

	m_pNoteField->SetY( fOriginalY );

	DISPLAY->CameraPopMatrix();
	DISPLAY->PopMatrix();

	if( !TAP_JUDGMENTS_UNDER_FIELD )
		DrawTapJudgments();

	if( !HOLD_JUDGMENTS_UNDER_FIELD )
		DrawHoldJudgments();

	if( !COMBO_UNDER_FIELD )
		if( GAMESTATE->m_PlayerOptions[m_PlayerNumber].m_fBlind == 0 )
			m_Combo.Draw();
}

void PlayerMinus::DrawTapJudgments()
{
	if( GAMESTATE->m_PlayerOptions[m_PlayerNumber].m_fBlind > 0 )
		return;

	if( GAMESTATE->m_PlayerOptions[m_PlayerNumber].m_bProTiming )
		m_ProTimingDisplay.Draw();
	else
		m_Judgment->Draw();
}

void PlayerMinus::DrawHoldJudgments()
{
	if( GAMESTATE->m_PlayerOptions[m_PlayerNumber].m_fBlind > 0 )
		return;

	for( int c=0; c<GetNumTracks(); c++ )
	{
		int col = GAMESTATE->m_viNSDrawOrder[m_PlayerNumber][c];
		g_NoteFieldMode[m_PlayerNumber].BeginDrawTrack(col);
		m_HoldJudgment[col].Draw();
		g_NoteFieldMode[m_PlayerNumber].EndDrawTrack(col);
	}
}

TapNoteScore PlayerMinus::ReverseGrade( TapNoteScore score )
{
	TapNoteScore rgscore = score;

	if( GAMESTATE->m_PlayerOptions[m_PlayerNumber].m_bReverseGrade )
	{
		if( GAMESTATE->ShowMarvelous(m_PlayerNumber) )
		{
			switch( score )
			{
			case TNS_HIDDEN:
			case TNS_MARVELOUS:
				rgscore = TNS_MISS;
				break;
			case TNS_PERFECT:
				rgscore = TNS_BOO;
				break;
			case TNS_GREAT:
				rgscore = TNS_GOOD;
				break;
			case TNS_GOOD:
				rgscore = TNS_GREAT;
				break;
			case TNS_BOO:
				rgscore = TNS_PERFECT;
				break;
			case TNS_MISS:
				rgscore = TNS_MARVELOUS;
				break;
			case TNS_CHECKPOINT:
				rgscore = TNS_MISS_CHECKPOINT;
				break;
			case TNS_MISS_CHECKPOINT:
				rgscore = TNS_CHECKPOINT;
				break;
			}
		}
		else
		{
			switch( score )
			{
			case TNS_MARVELOUS:
			case TNS_HIDDEN:
			case TNS_PERFECT:
				rgscore = TNS_MISS;
				break;
			case TNS_GREAT:
				rgscore = TNS_BOO;
				break;
			case TNS_GOOD:
				rgscore = TNS_GOOD;
				break;
			case TNS_BOO:
				rgscore = TNS_GREAT;
				break;
			case TNS_MISS:
				rgscore = TNS_PERFECT;
				break;
			case TNS_CHECKPOINT:
				rgscore = TNS_MISS_CHECKPOINT;
				break;
			case TNS_MISS_CHECKPOINT:
				rgscore = TNS_CHECKPOINT;
				break;
			}
		}
	}

	return rgscore;
}

// It's OK for this function to search a little more than was requested.
int PlayerMinus::GetClosestNoteDirectional( int col, float fBeat, float fTimeBeat, float fMaxBeatsDistance, int iDirection, float& fTime ) const
{
	// look for the closest matching step
	int iIndexStartLookingAt = BeatToNoteRowRounded( fBeat );

	// Number of elements to examine on either end of iIndexStartLookingAt. Make sure we always round up.
	int iNumElementsToExamine = BeatToNoteRowRounded( fMaxBeatsDistance + 1 );

	const int minus1 = iDirection==0 ? 1 : iDirection/abs(iDirection);

	// Start at iIndexStartLookingAt and search outward.
	for( int delta=0; delta < iNumElementsToExamine; delta++ )
	{
		int iCurrentIndex = iIndexStartLookingAt + (iDirection * delta);

		if( iCurrentIndex < 0 )
			continue;
		if( GetTapNote(col, iCurrentIndex).type == TapNote::empty )
			continue;	// no note here
		if( GetTapNoteScore(col, iCurrentIndex) != TNS_NONE )
			continue;	// this note has a score already

		float fCurrentBeat = NoteRowToBeat( iCurrentIndex );

		// Fake note, this means we are inside a fake area, so let's check were it begins and ends.
		if( GetTapNote(col, iCurrentIndex).drawType != TapNote::empty )
		{
			if( m_Timing->IsFakeAreaAtBeat( fCurrentBeat ) ) // double check
			{
				AreaSegment ff = m_Timing->GetFakeSegmentAtBeat( fCurrentBeat );
				int iStartIndex = BeatToNoteRow( ff.m_fBeat );
				int iEndIndex = BeatToNoteRowRounded( ff.m_fBeat + ff.m_fBeats );

				iNumElementsToExamine -= delta - 1;
				delta = -1;

				switch( minus1 )
				{
				case -1:
					iIndexStartLookingAt = iStartIndex-1;
					break;
				case 1:
					iIndexStartLookingAt = iEndIndex+1;
					break;
				default:
					ASSERT(0);
				}

				continue;
			}
		}
		else if( GetTapNote(col, iCurrentIndex).drawType == TapNote::empty )
			continue;	// no note here

		fTime = m_Timing->GetElapsedTimeFromBeat( fCurrentBeat );
		return iCurrentIndex;
	}

	return -1;
}

int PlayerMinus::GetClosestNote( int col, float fBeat, float fMaxBeatsAhead, float fMaxBeatsBehind ) const
{
	const float fTimeBeat = m_Timing->GetElapsedTimeFromBeat( fBeat );
	float fTimeFwd, fTimeBack;

	int Fwd = GetClosestNoteDirectional(col, fBeat, fTimeBeat, fMaxBeatsAhead, 1, fTimeFwd);
	int Back = GetClosestNoteDirectional(col, fBeat, fTimeBeat, fMaxBeatsBehind, -1, fTimeBack);

	if(Fwd == -1 && Back == -1) return -1;
	if(Fwd == -1) return Back;
	if(Back == -1) return Fwd;

	// Figure out which row is closer.
	const float DistToFwd = fabsf(fTimeBeat-fTimeFwd);
	const float DistToBack = fabsf(fTimeBeat-fTimeBack);

	if( DistToFwd > DistToBack )
		return Back;

	return Fwd;
}

void PlayerMinus::Step( int col, RageTimer tm )
{
	if( GAMESTATE->m_PlayerOptions[m_PlayerNumber].m_LifeType == PlayerOptions::LIFE_BATTERY  &&  g_CurStageStats.bFailed[m_PlayerNumber] )	// Oni dead
		return;	// do nothing

	//LOG->Trace( "PlayerMinus::HandlePlayerStep()" );

	ASSERT( col >= 0  &&  col <= GetNumTracks() );

	float fPositionSeconds = GAMESTATE->m_fMusicSeconds;
	fPositionSeconds -= tm.Ago();
	const float fBeat = m_Timing ? m_Timing->GetBeatFromElapsedTime( fPositionSeconds ) : GAMESTATE->m_fPlayerBeat[m_PlayerNumber];

	// Update Hold Pressed Flag
	m_HoldPressed[col] = fPositionSeconds - PREFSMAN->m_fPadStickSeconds;

	const float StepSearchDistanceBackwards = abs( fBeat - m_Timing->GetBeatFromElapsedTime( fPositionSeconds - ADJUSTED_WINDOW_AFTER(Boo) ) );
	const float StepSearchDistanceForwards = abs( fBeat + m_Timing->GetBeatFromElapsedTime( fPositionSeconds - ADJUSTED_WINDOW(Boo) ) );

	//
	// Check for step on a TapNote
	//
	int iIndexOverlappingNote = GetClosestNote( col, fBeat,
					StepSearchDistanceForwards * GAMESTATE->m_fPlayerBPS[m_PlayerNumber] * GAMESTATE->m_SongOptions.m_fMusicRate,
					StepSearchDistanceBackwards * GAMESTATE->m_fPlayerBPS[m_PlayerNumber] * GAMESTATE->m_SongOptions.m_fMusicRate );

	// Determine TapNoteType
	TapNote tn = GetTapNote(col,iIndexOverlappingNote);

	if( iIndexOverlappingNote < m_iCurrentHoldEndRow[col] )
	{
		switch( tn.type )
		{
		case TapNote::hold:
		case TapNote::hold_head:
		case TapNote::hold_tail:
			break;
		default:
			if( m_bCrossingHoldNote[col] )
				return;
		}
	}

	// Calculate TapNoteScore
	TapNoteScore score = TNS_NONE;

	if( iIndexOverlappingNote != -1 )
	{
		// compute the score for this hit
		const float fStepBeat = NoteRowToBeat( iIndexOverlappingNote );
		const float fStepSeconds = m_Timing->GetElapsedTimeFromBeat(fStepBeat);

		/* We actually stepped on the note this long ago: */
		const float fTimeSinceStep = tm.Ago();

		/* GAMESTATE->m_fMusicSeconds is the music time as of GAMESTATE->m_LastBeatUpdate. Figure
		* out what the music time is as of now. */
		const float fCurrentMusicSeconds = GAMESTATE->m_fMusicSeconds + (GAMESTATE->m_LastBeatUpdate.Ago() * GAMESTATE->m_SongOptions.m_fMusicRate);

		/* ... which means it happened at this point in the music: */
		const float fMusicSeconds = fCurrentMusicSeconds - ( fTimeSinceStep * GAMESTATE->m_SongOptions.m_fMusicRate );

		// The offset from the actual step in seconds:
		const float fNoteOffset = (fStepSeconds - fMusicSeconds) / GAMESTATE->m_SongOptions.m_fMusicRate;   // account for music rate

		bool bSteppedEarly = fNoteOffset >= 0;

		switch( GAMESTATE->m_PlayerController[m_PlayerNumber] )
		{
		case PC_HUMAN:

			switch( tn.type )
			{
			case TapNote::lift:
				// So, you've stepped directly onto a lift note...
				if( fNoteOffset >= -ADJUSTED_WINDOW_AFTER(Boo) &&
					fNoteOffset <= ADJUSTED_WINDOW(Great) )
					score = TNS_MISS;
				break;

			case TapNote::mine:
				// stepped too close to a mine or the mine is in the middle of a delay?
				if( fNoteOffset >= -ADJUSTED_WINDOW_AFTER(Mine) &&
					fNoteOffset <= ADJUSTED_WINDOW(Mine) )
				{
					score = TNS_HIT_MINE;

					if( PREFSMAN->m_bPlayMineSound )
						m_soundMine.Play();

					if( PREFSMAN->m_bComboBreakOnMines )
					{
						const unsigned uMultiplier = m_Timing->GetHitMultiplierAtBeat( fStepBeat, score );

						if( PREFSMAN->m_bMissComboIncreasesOnComboBreak && g_CurStageStats.iCurCombo[m_PlayerNumber] > 0 )
							g_CurStageStats.iCurMissCombo[m_PlayerNumber] += uMultiplier;

						g_CurStageStats.iCurCombo[m_PlayerNumber] = 0;
					}

					// Attack Mines!
					if( GAMESTATE->m_CurrentPlayerOptions[m_PlayerNumber].m_bTransforms[PlayerOptions::TRANSFORM_ATTACK_MINES] )
					{
						Attack attMineAttack;
						attMineAttack.sModifier = ApplyRandomAttack();
						attMineAttack.level = (AttackLevel)1;
						attMineAttack.fStartSecond = -1;
						attMineAttack.fSecsRemaining = PREFSMAN->m_fAttackMinesLength;
						GAMESTATE->LaunchAttack( m_PlayerNumber, attMineAttack);
					}
					else
					{
						const float fMultiplier = m_Timing->GetLifeMultiplierAtBeat( fStepBeat, score );

						if( m_pLifeMeter )
							m_pLifeMeter->ChangeLife( score, fMultiplier );
						if( m_pCombinedLifeMeter )
							m_pCombinedLifeMeter->ChangeLife( m_PlayerNumber, score, fMultiplier );
					}

					m_pNoteField->SetTapNote( col, iIndexOverlappingNote, m_EmptyNote );    // remove from NoteField
					m_pNoteField->DidTapNote( col, score, false );

					// Aldo_MX: Hardcoded by now until Seed System is added...
					GAMESTATE->m_bUpdateNotePressed = true;
					GAMESTATE->m_fNotePressedTime[m_PlayerNumber] = fMusicSeconds;
				}
				break;

			case TapNote::shock:
				// stepped too close to a shock or the shock is in the middle of a delay?
				if( fNoteOffset >= -ADJUSTED_WINDOW_AFTER(Shock) &&
					fNoteOffset <= ADJUSTED_WINDOW(Shock) )
				{
					score = TNS_HIT_SHOCK;

					if( PREFSMAN->m_bPlayShockSound )
						m_soundShock.Play();

					g_CurStageStats.iCurCombo[m_PlayerNumber] = 0;
					g_CurStageStats.iCurMissCombo[m_PlayerNumber]++;
					g_CurStageStats.iHoldNoteScores[m_PlayerNumber][SHOCK_NG]++;

					if( GAMESTATE->m_PlayerOptions[m_PlayerNumber].m_bReverseGrade )
						m_Combo.SetCombo( g_CurStageStats.iCurMissCombo[m_PlayerNumber], g_CurStageStats.iCurCombo[m_PlayerNumber] );
					else
						m_Combo.SetCombo( g_CurStageStats.iCurCombo[m_PlayerNumber], g_CurStageStats.iCurMissCombo[m_PlayerNumber] );

					const float fMultiplier = m_Timing->GetLifeMultiplierAtBeat( fStepBeat, score );
					if( m_pLifeMeter )
						m_pLifeMeter->ChangeLife( score, fMultiplier );
					if( m_pCombinedLifeMeter )
						m_pCombinedLifeMeter->ChangeLife( m_PlayerNumber, score, fMultiplier );

					m_HoldJudgment[col].SetHoldJudgment( SHOCK_NG );
					m_pNoteField->SetTapNote( col, iIndexOverlappingNote, m_EmptyNote );    // remove from NoteField
					m_pNoteField->DidTapNote( col, score, false );

					// Fade out
					m_pNoteField->DidShockArrow();

					// Aldo_MX: Hardcoded by now until Seed System is added...
					GAMESTATE->m_bUpdateNotePressed = true;
					GAMESTATE->m_fNotePressedTime[m_PlayerNumber] = fMusicSeconds;
				}
				break;

			case TapNote::potion:
				// stepped too close to a potion?
				if( fNoteOffset >= -ADJUSTED_WINDOW_AFTER(Potion) &&
					fNoteOffset <= ADJUSTED_WINDOW(Potion) )
				{
					score = TNS_HIT_POTION;

					if( PREFSMAN->m_bPlayPotionSound )
						m_soundPotion.Play();

					// TODO - Aldo_MX: GetMultiplierNumber() o algo asi
					if( PREFSMAN->m_bComboIncreasesOnPotions )
					{
						const unsigned uMultiplier = m_Timing->GetHitMultiplierAtBeat( fStepBeat, score );

						g_CurStageStats.iCurCombo[m_PlayerNumber] += uMultiplier;
						g_CurStageStats.iCurMissCombo[m_PlayerNumber] = 0;

						if( GAMESTATE->m_PlayerOptions[m_PlayerNumber].m_bReverseGrade )
							m_Combo.SetCombo( g_CurStageStats.iCurMissCombo[m_PlayerNumber], g_CurStageStats.iCurCombo[m_PlayerNumber] );
						else
							m_Combo.SetCombo( g_CurStageStats.iCurCombo[m_PlayerNumber], g_CurStageStats.iCurMissCombo[m_PlayerNumber] );
					}

					{
						const float fMultiplier = m_Timing->GetLifeMultiplierAtBeat( fStepBeat, score );

						if( m_pLifeMeter )
							m_pLifeMeter->ChangeLife( score, fMultiplier );
						if( m_pCombinedLifeMeter )
							m_pCombinedLifeMeter->ChangeLife( m_PlayerNumber, score, fMultiplier );
					}

					bool bBright;

					if( GAMESTATE->m_PlayerController[m_PlayerNumber] == PC_HUMAN )
						bBright = g_CurStageStats.iCurCombo[m_PlayerNumber] >= BRIGHT_GHOST_COMBO_THRESHOLD;
					else
						bBright = GAMESTATE->m_iNotesInAutoPlay[m_PlayerNumber] >= BRIGHT_GHOST_COMBO_THRESHOLD;

					m_pNoteField->SetTapNote( col, iIndexOverlappingNote, m_EmptyNote );    // remove from NoteField
					m_pNoteField->DidTapNote( col, score, bBright );
				}
				break;

			case TapNote::hidden:
				if( fNoteOffset >= -ADJUSTED_WINDOW_AFTER(Hidden) &&
					fNoteOffset <= ADJUSTED_WINDOW(Hidden) )
				{
					score = TNS_HIDDEN;
					bSteppedEarly = false;
				}
				break;

			case TapNote::hold_head:
				if( PREFSMAN->m_bUsePIUHolds )
				{
					if( fNoteOffset >= -ADJUSTED_WINDOW_AFTER(Long) &&
						fNoteOffset <= ADJUSTED_WINDOW(Long) )
					{
						score = TNS_CHECKPOINT;

						TapNote tn = this->GetTapNote( col, iIndexOverlappingNote );
						tn.type = TapNote::empty;

						this->SetTapNote( col, iIndexOverlappingNote, tn );
						m_pNoteField->SetTapNote( col, iIndexOverlappingNote, tn );
					}
					break;
				}

			default:
				if( fNoteOffset >= -ADJUSTED_WINDOW_AFTER(Marvelous) &&
					fNoteOffset <= ADJUSTED_WINDOW(Marvelous) )
					score = TNS_MARVELOUS;
				else if( fNoteOffset >= -ADJUSTED_WINDOW_AFTER(Perfect) &&
					fNoteOffset <= ADJUSTED_WINDOW(Perfect) )
					score = TNS_PERFECT;
				else if( fNoteOffset >= -ADJUSTED_WINDOW_AFTER(Great) &&
					fNoteOffset <= ADJUSTED_WINDOW(Great) )
					score = TNS_GREAT;
				else if( fNoteOffset >= -ADJUSTED_WINDOW_AFTER(Good) &&
					fNoteOffset <= ADJUSTED_WINDOW(Good) )
					score = TNS_GOOD;
				else if( fNoteOffset >= -ADJUSTED_WINDOW_AFTER(Boo) &&
					fNoteOffset <= ADJUSTED_WINDOW(Boo) )
					score = TNS_BOO;
				else
					score = TNS_NONE;
				break;
			}
			break;

		case PC_CPU:
		case PC_AUTOPLAY:
			switch( GAMESTATE->m_PlayerController[m_PlayerNumber] )
			{
			case PC_CPU:
				if( tn.type != TapNote::hidden )	// Computer ignores hidden notes
				{
					score = PlayerAI::GetTapNoteScore( m_PlayerNumber );

					// This generates computer early/late markers
					if( score != TNS_MARVELOUS )
					{
						float fRand = randomf(0,1);

						if( fRand < 0.5f )
							bSteppedEarly = true;
						else
							bSteppedEarly = false;
					}
				}
				break;
			case PC_AUTOPLAY:
				if( tn.type != TapNote::hidden )	// Computer ignores hidden notes
					score = TNS_MARVELOUS;
				break;
			}

			// TRICKY:  We're asking the AI to judge mines. Consider TNS_GOOD and below
			// as "mine was hit" and everything else as "mine was avoided"
			switch( tn.type )
			{
			case TapNote::mine:
			{
				// The CPU hits a lot of mines.  Only consider hitting the
				// first mine for a row.  We know we're the first mine if
				// there are are no mines to the left of us.
				for( int t=0; t<col; t++ )
				{
					if( GetTapNote(t,iIndexOverlappingNote).type == TapNote::mine ) // there's a mine to the left of us
						return; // avoid
				}

				// The CPU hits a lot of mines.  Make it less likely to hit
				// mines that don't have a tap note on the same row.
				bool bTapsOnRow = IsThereATapOrHoldHeadAtRow( iIndexOverlappingNote );
				TapNoteScore get_to_avoid = bTapsOnRow ? TNS_GREAT : TNS_GOOD;

				if( score >= get_to_avoid )
					return; // avoided
				else
				{
					score = TNS_HIT_MINE;

					if( PREFSMAN->m_bPlayMineSound )
						m_soundMine.Play();

					// Attack Mines!
					if( GAMESTATE->m_CurrentPlayerOptions[m_PlayerNumber].m_bTransforms[PlayerOptions::TRANSFORM_ATTACK_MINES] )
					{
						Attack attMineAttack;
						attMineAttack.sModifier = ApplyRandomAttack();
						attMineAttack.level = (AttackLevel)1;
						attMineAttack.fStartSecond = -1;
						attMineAttack.fSecsRemaining = PREFSMAN->m_fAttackMinesLength;
						GAMESTATE->LaunchAttack( m_PlayerNumber, attMineAttack);
					}
					else
					{
						const float fMultiplier = m_Timing->GetLifeMultiplierAtBeat( fStepBeat, score );

						if( m_pLifeMeter )
							m_pLifeMeter->ChangeLife( score, fMultiplier );
						if( m_pCombinedLifeMeter )
							m_pCombinedLifeMeter->ChangeLife( m_PlayerNumber, score, fMultiplier );

						m_pNoteField->SetTapNote( col, iIndexOverlappingNote, m_EmptyNote );    // remove from NoteField
						m_pNoteField->DidTapNote( col, score, false );
					}
				}

				// Aldo_MX: Hardcoded by now until Seed System is added...
				GAMESTATE->m_bUpdateNotePressed = true;
				GAMESTATE->m_fNotePressedTime[m_PlayerNumber] = fMusicSeconds;
			}
			break;

			// TRICKY:  We're asking the AI to judge shocks. Consider TNS_GOOD and below
			// as "shock was hit" and everything else as "shock was avoided"
			case TapNote::shock:
			{
				// The CPU hits a lot of shocks.  Only consider hitting the
				// first shock for a row.  We know we're the first shock if
				// there are are no shocks to the left of us.
				for( int t=0; t<col; t++ )
				{
					if( GetTapNote(t,iIndexOverlappingNote).type == TapNote::shock ) // there's a shock to the left of us
						return; // avoid
				}

				// The CPU hits a lot of shocks.  Make it less likely to hit
				// shocks that don't have a tap note on the same row.
				bool bTapsOnRow = IsThereATapOrHoldHeadAtRow( iIndexOverlappingNote );
				TapNoteScore get_to_avoid = bTapsOnRow ? TNS_GREAT : TNS_GOOD;

				if( score >= get_to_avoid )
					return; // avoided
				else
				{
					score = TNS_HIT_SHOCK;

					if( PREFSMAN->m_bPlayShockSound )
						m_soundShock.Play();

					const float fMultiplier = m_Timing->GetLifeMultiplierAtBeat( fStepBeat, score );
					if( m_pLifeMeter )
						m_pLifeMeter->ChangeLife( score, fMultiplier );
					if( m_pCombinedLifeMeter )
						m_pCombinedLifeMeter->ChangeLife( m_PlayerNumber, score, fMultiplier );

					m_HoldJudgment[col].SetHoldJudgment( SHOCK_NG );
					m_pNoteField->SetTapNote( col, iIndexOverlappingNote, m_EmptyNote );    // remove from NoteField
					m_pNoteField->DidTapNote( col, score, false );
				}

				// Aldo_MX: Hardcoded by now until Seed System is added...
				GAMESTATE->m_bUpdateNotePressed = true;
				GAMESTATE->m_fNotePressedTime[m_PlayerNumber] = fMusicSeconds;
			}
			break;

			// TRICKY:  We're asking the AI to judge potions. Consider TNS_GOOD and below
			// as "potion was hit" and everything else as "potion was avoided"
			case TapNote::potion:
			{
				// The CPU hits a lot of potion.  Only consider hitting the
				// first potion for a row.  We know we're the first potion if
				// there are are no potion to the left of us.
				for( int t=0; t<col; t++ )
				{
					if( GetTapNote(t,iIndexOverlappingNote).type == TapNote::potion ) // there's a potion to the left of us
						return; // avoid
				}

				// The CPU hits a lot of potion.  Make it less likely to hit
				// potion that don't have a tap note on the same row.
				bool bTapsOnRow = IsThereATapOrHoldHeadAtRow( iIndexOverlappingNote );
				TapNoteScore get_to_avoid = bTapsOnRow ? TNS_GREAT : TNS_GOOD;

				if( score >= get_to_avoid )
					return; // avoided
				else
				{
					score = TNS_HIT_POTION;

					if( PREFSMAN->m_bPlayPotionSound )
						m_soundPotion.Play();

					if( PREFSMAN->m_bComboIncreasesOnPotions )
					{
						const unsigned uMultiplier = m_Timing->GetHitMultiplierAtBeat( fStepBeat, score );

						if( GAMESTATE->m_PlayerController[m_PlayerNumber] == PC_AUTOPLAY )
						{
							GAMESTATE->m_iNotesInAutoPlay[m_PlayerNumber] += uMultiplier;

							if( PREFSMAN->m_bAutoPlayCombo )
							{
								if( GAMESTATE->m_PlayerOptions[m_PlayerNumber].m_bReverseGrade )
									m_Combo.SetCombo( 0, GAMESTATE->m_iNotesInAutoPlay[m_PlayerNumber] );
								else
									m_Combo.SetCombo( GAMESTATE->m_iNotesInAutoPlay[m_PlayerNumber], 0 );
							}
						}
						else
						{
							g_CurStageStats.iCurCombo[m_PlayerNumber] += uMultiplier;
							g_CurStageStats.iCurMissCombo[m_PlayerNumber] = 0;

							if( GAMESTATE->m_PlayerOptions[m_PlayerNumber].m_bReverseGrade )
								m_Combo.SetCombo( g_CurStageStats.iCurMissCombo[m_PlayerNumber], g_CurStageStats.iCurCombo[m_PlayerNumber] );
							else
								m_Combo.SetCombo( g_CurStageStats.iCurCombo[m_PlayerNumber], g_CurStageStats.iCurMissCombo[m_PlayerNumber] );
						}
					}

					{
						const float fMultiplier = m_Timing->GetLifeMultiplierAtBeat( fStepBeat, score );

						if( m_pLifeMeter )
							m_pLifeMeter->ChangeLife( score, fMultiplier );
						if( m_pCombinedLifeMeter )
							m_pCombinedLifeMeter->ChangeLife( m_PlayerNumber, score, fMultiplier );
					}

					bool bBright;

					if( GAMESTATE->m_PlayerController[m_PlayerNumber] == PC_HUMAN )
						bBright = g_CurStageStats.iCurCombo[m_PlayerNumber] >= BRIGHT_GHOST_COMBO_THRESHOLD;
					else
						bBright = GAMESTATE->m_iNotesInAutoPlay[m_PlayerNumber] >= BRIGHT_GHOST_COMBO_THRESHOLD;

					m_pNoteField->SetTapNote( col, iIndexOverlappingNote, m_EmptyNote );    // remove from NoteField
					m_pNoteField->DidTapNote( col, score, bBright );
				}
			}
			break;
			}

			/* AI will generate misses here.  Don't handle a miss like a regular note because
			* we want the judgment animation to appear delayed.  Instead, return early if
			* AI generated a miss, and let UpdateMissedTapNotesOlderThan() detect and handle the
			* misses. */
			if( score == TNS_MISS )
				return;

			break;

		default:
			ASSERT(0);
			score = TNS_NONE;
			break;
		}

		// Display m_GhostArrowRow (PIU)
		if( score == TNS_NONE )
		{
			m_pNoteField->DidTapNote( col, score, false );
			tn = m_EmptyNote;
			return;
		}

		// Do game-specific score mapping.
		if( !GAMESTATE->m_PlayerOptions[m_PlayerNumber].m_bSMJudgments )
		{
			const Game* pGame = GAMESTATE->GetCurrentGame();
			if( score == TNS_MARVELOUS )	score = pGame->m_mapMarvelousTo;
			else if( score == TNS_PERFECT )	score = pGame->m_mapPerfectTo;
			else if( score == TNS_GREAT )	score = pGame->m_mapGreatTo;
			else if( score == TNS_GOOD )	score = pGame->m_mapGoodTo;
			else if( score == TNS_BOO )		score = pGame->m_mapBooTo;
		}

		if( score != TNS_MISS )
		{
			int ms_error = (int) roundf( fNoteOffset * 1000 );
			ms_error = min( ms_error, MAX_PRO_TIMING_ERROR );

			g_CurStageStats.iTotalError[m_PlayerNumber] += ms_error;

			if( !GAMESTATE->m_PlayerOptions[m_PlayerNumber].m_fBlind )
				m_ProTimingDisplay.SetJudgment( ms_error, ReverseGrade( score ) );
		}

		if( score == TNS_MARVELOUS && !GAMESTATE->ShowMarvelous(m_PlayerNumber) )
			score = TNS_PERFECT;

		//LOG->Trace("XXX: %s col %i, at %f, music at %f, step was at %f, off by %f", TapNoteScoreToString(score).c_str(), col, fStepSeconds, fCurrentMusicSeconds, fMusicSeconds, fNoteOffset );

		// TapScore for individual notes
		{
			bool bIsHold = false;
			switch( score )
			{
			case TNS_CHECKPOINT:
				bIsHold = true;
				score = GAMESTATE->ShowMarvelous(m_PlayerNumber) ? TNS_MARVELOUS : TNS_PERFECT;
				// fall-through

			case TNS_HIT_POTION:
			case TNS_HIT_SHOCK:
			case TNS_HIT_MINE:
				if( m_pPrimaryScoreKeeper || m_pSecondaryScoreKeeper )
				{
					unsigned uComboMultiplier;
					float fScoreMultiplier;
					m_Timing->GetMultiplierAtBeat( fStepBeat, score, uComboMultiplier, fScoreMultiplier, fScoreMultiplier );

					if( m_pPrimaryScoreKeeper )
						m_pPrimaryScoreKeeper->HandleTapScore( score, uComboMultiplier, fScoreMultiplier );
					if( m_pSecondaryScoreKeeper )
						m_pSecondaryScoreKeeper->HandleTapScore( score, uComboMultiplier, fScoreMultiplier );
				}

				if( bIsHold )
					return;
			}
		}

		SetTapNoteScore(col, iIndexOverlappingNote, score);
		SetTapNoteOffset(col, iIndexOverlappingNote, -fNoteOffset);

		if( GAMESTATE->m_PlayerController[m_PlayerNumber] == PC_HUMAN && score >= TNS_GREAT )
			HandleAutosync(fNoteOffset);

		//Keep this here so we get the same data as Autosync
		NSMAN->ReportTiming(fNoteOffset, m_PlayerNumber);

		switch( tn.type )
		{
		case TapNote::lift:		// For CPU, judge Lift Notes here to make them work
			if( GAMESTATE->m_PlayerController[m_PlayerNumber] == PC_HUMAN )
				break;
		case TapNote::tap:
		case TapNote::hold_head:
		case TapNote::hidden:
			// don't judge the row if this note is a mine or tap attack
			if( IsRowCompletelyJudged(iIndexOverlappingNote) )
				OnRowCompletelyJudged( iIndexOverlappingNote, bSteppedEarly );
		}

		if( score == TNS_MISS || score == TNS_BOO )
			m_iDCState = AS2D_MISS;
		if( score == TNS_GOOD || score == TNS_GREAT )
			m_iDCState = AS2D_GOOD;
		if( score == TNS_PERFECT || score == TNS_MARVELOUS )
		{
			m_iDCState = AS2D_GREAT;

			if( m_pLifeMeter && m_pLifeMeter->GetLife() == 1.0f) // full life
				m_iDCState = AS2D_FEVER; // super celebrate time :)
		}

		if( score == TNS_HIDDEN )
			m_iDCState = AS2D_FEVER;	// Found the hidden notes! Super celebrate!

		m_pNoteField->Step( col, ReverseGrade( score ) );
	}
	else
	{
		m_pNoteField->Step( col, TNS_NONE, false );
		m_pNoteField->DidTapNote( col, TNS_NONE, false );
	}
}

void PlayerMinus::StepCheckpoint(int col, int row, bool hit)
{
	if (!PREFSMAN->m_bUsePIUHolds)
		return;

	// Oni dead
	if (GAMESTATE->m_PlayerOptions[m_PlayerNumber].m_LifeType == PlayerOptions::LIFE_BATTERY && g_CurStageStats.bFailed[m_PlayerNumber])
		return;

	TapNote tn = this->GetTapNote(col, row);

	if (tn.type == TapNote::hold_head) {
		TapNoteScore score = GAMESTATE->ShowMarvelous(m_PlayerNumber) ? TNS_MARVELOUS : TNS_PERFECT;

		unsigned uComboMultiplier;
		float fScoreMultiplier;
		m_Timing->GetMultiplierAtBeat(NoteRowToBeat(row), score, uComboMultiplier, fScoreMultiplier, fScoreMultiplier);

		// HACK: Oni percents gets messed with checkpoints due to hold heads being counted in radar values.
		if (m_pPrimaryScoreKeeper)
			m_pPrimaryScoreKeeper->HandleTapScore(score, uComboMultiplier, fScoreMultiplier);
		if (m_pSecondaryScoreKeeper)
			m_pSecondaryScoreKeeper->HandleTapScore(score, uComboMultiplier, fScoreMultiplier);

		tn.type = TapNote::empty;
		this->SetTapNote(col, row, tn);
	}

	// Do this row have checkpoints?
	if (tn.checkpoints == 0)
		return;

	//LOG->Trace( "StepCheckpoint( %d, %d, %i )", col, row, hit );

	// Already judged?
	if (GetTapNoteScore(col, row) != TNS_NONE)
		return;

	// Calculate TapNoteScore
	TapNoteScore score = hit ? TNS_CHECKPOINT : TNS_MISS_CHECKPOINT;

	// Set score
	SetTapNoteScore(col, row, score);
	SetTapNoteOffset(col, row, 0);

	//Keep this here so we get the same data as Autosync
	NSMAN->ReportTiming(0, m_PlayerNumber);

	if (hit && row != m_iCurrentHoldEndRow[col]) {
		// show the ghost arrow for this column
		if (GAMESTATE->m_PlayerOptions[m_PlayerNumber].m_fBlind)
			m_pNoteField->DidTapNote(col, TNS_MARVELOUS, false);
		else {
			bool bBright;

			if (GAMESTATE->m_PlayerController[m_PlayerNumber] == PC_HUMAN)
				bBright = g_CurStageStats.iCurCombo[m_PlayerNumber] >= BRIGHT_GHOST_COMBO_THRESHOLD;
			else
				bBright = GAMESTATE->m_iNotesInAutoPlay[m_PlayerNumber] >= BRIGHT_GHOST_COMBO_THRESHOLD;

			m_pNoteField->DidTapNote(col, score, bBright);
		}
	}

	if (IsRowCompletelyJudged(row)) {
		if (IsThereATapOrHiddenOrHoldHeadAtRow(row))
			OnRowCompletelyJudged(row, false);
		else {
			HandleTapRowScore(row, score);	// update score
			m_Judgment->SetJudgment(ReverseGrade(score), false);
		}
	}

	this->SetTapNote(col, row, m_EmptyNote);
	m_pNoteField->SetTapNote(col, row, m_EmptyNote);
}

void PlayerMinus::MissSkippedCheckpoint(int col, int row)
{
	if (!PREFSMAN->m_bUsePIUHolds)
		return;

	// Oni dead
	if (GAMESTATE->m_PlayerOptions[m_PlayerNumber].m_LifeType == PlayerOptions::LIFE_BATTERY && g_CurStageStats.bFailed[m_PlayerNumber])
		return;

	if (GetTapNoteScore(col, row) != TNS_NONE)
		return;

	SetTapNoteScore(col, row, TNS_MISS_CHECKPOINT);
	SetTapNoteOffset(col, row, 0);

	//Keep this here so we get the same data as Autosync
	NSMAN->ReportTiming(0, m_PlayerNumber);

	if (IsThereATapOrHiddenOrHoldHeadAtRow(row))
		OnRowCompletelyJudged(row, false);
}

/* We happen to need a separate release function, despite the fact it's nearly identical to Step(). If we don't have
 * this, then step will also trigger on releases, and cause judgment problems on non-lift notes. */
void PlayerMinus::Release( int col, RageTimer tm )
{
	if( GAMESTATE->m_PlayerOptions[m_PlayerNumber].m_LifeType == PlayerOptions::LIFE_BATTERY && g_CurStageStats.bFailed[m_PlayerNumber] )	// Oni dead
		return;	// do nothing

	ASSERT( col >= 0 && col <= GetNumTracks() );

	float fPositionSeconds = GAMESTATE->m_fMusicSeconds;
	fPositionSeconds -= tm.Ago();
	const float fBeat = m_Timing ? m_Timing->GetBeatFromElapsedTime( fPositionSeconds ) : GAMESTATE->m_fPlayerBeat[m_PlayerNumber];

	const float StepSearchDistanceBackwards = abs( fBeat - m_Timing->GetBeatFromElapsedTime( fPositionSeconds - ADJUSTED_WINDOW_AFTER(Boo) ) );
	const float StepSearchDistanceForwards = abs( fBeat + m_Timing->GetBeatFromElapsedTime( fPositionSeconds - ADJUSTED_WINDOW(Boo) ) );

	// Check for the closest note
	int iIndexOverlappingNote = GetClosestNote( col, fBeat,
						   StepSearchDistanceForwards * GAMESTATE->m_fPlayerBPS[m_PlayerNumber] * GAMESTATE->m_SongOptions.m_fMusicRate,
						   StepSearchDistanceBackwards * GAMESTATE->m_fPlayerBPS[m_PlayerNumber] * GAMESTATE->m_SongOptions.m_fMusicRate );

	// Determine TapNoteType
	TapNote tn = GetTapNote(col,iIndexOverlappingNote);

	// Calculate TapNoteScore
	TapNoteScore score = TNS_NONE;

	if( iIndexOverlappingNote != -1)
	{
		// We're only interested in this for Lift Notes
		if( tn.type == TapNote::lift )
		{
			// compute the score for this hit
			const float fReleaseBeat = NoteRowToBeat( iIndexOverlappingNote );
			const float fReleaseSeconds = m_Timing->GetElapsedTimeFromBeat(fReleaseBeat);

			/* We actually released the note this long ago: */
			const float fTimeSinceRelease = tm.Ago();

			/* GAMESTATE->m_fMusicSeconds is the music time as of GAMESTATE->m_LastBeatUpdate. Figure
			* out what the music time is as of now. */
			const float fCurrentMusicSeconds = GAMESTATE->m_fMusicSeconds + (GAMESTATE->m_LastBeatUpdate.Ago()*GAMESTATE->m_SongOptions.m_fMusicRate);

			/* ... which means it happened at this point in the music: */
			const float fMusicSeconds = fCurrentMusicSeconds - fTimeSinceRelease * GAMESTATE->m_SongOptions.m_fMusicRate;

			// The offset from the actual release in seconds:
			const float fNoteOffset = (fReleaseSeconds - fMusicSeconds) / GAMESTATE->m_SongOptions.m_fMusicRate;	// account for music rate

			bool	bSteppedEarly = fNoteOffset >= 0;

			switch( GAMESTATE->m_PlayerController[m_PlayerNumber] )
			{
			case PC_HUMAN:
				if( fNoteOffset >= -ADJUSTED_WINDOW_AFTER(Marvelous) &&
					fNoteOffset <= ADJUSTED_WINDOW(Marvelous) )
					score = TNS_MARVELOUS;
				else if( fNoteOffset >= -ADJUSTED_WINDOW_AFTER(Perfect) &&
					fNoteOffset <= ADJUSTED_WINDOW(Perfect) )
					score = TNS_PERFECT;
				else if( fNoteOffset >= -ADJUSTED_WINDOW_AFTER(Great) &&
					fNoteOffset <= ADJUSTED_WINDOW(Great) )
					score = TNS_GREAT;
				else if( fNoteOffset >= -ADJUSTED_WINDOW_AFTER(Good) &&
					fNoteOffset <= ADJUSTED_WINDOW(Good) )
					score = TNS_GOOD;
				else if( fNoteOffset >= -ADJUSTED_WINDOW_AFTER(Boo) &&
					fNoteOffset <= ADJUSTED_WINDOW(Boo) )
					score = TNS_BOO;
				else
					score = TNS_NONE;
				break;
			default:
				ASSERT(0);
				score = TNS_NONE;
				break;
			}

			// Do game-specific score mapping.
			if( !GAMESTATE->m_PlayerOptions[m_PlayerNumber].m_bSMJudgments )
			{
				const Game* pGame = GAMESTATE->GetCurrentGame();
				if( score == TNS_MARVELOUS )	score = pGame->m_mapMarvelousTo;
				if( score == TNS_PERFECT )		score = pGame->m_mapPerfectTo;
				if( score == TNS_GREAT )		score = pGame->m_mapGreatTo;
				if( score == TNS_GOOD )			score = pGame->m_mapGoodTo;
				if( score == TNS_BOO )			score = pGame->m_mapBooTo;
			}

			if( score == TNS_NONE )
			{
				tn = m_EmptyNote;
				return;
			}

			if( score != TNS_MISS )
			{
				int ms_error = (int) roundf( fNoteOffset * 1000 );
				ms_error = min( ms_error, MAX_PRO_TIMING_ERROR );

				g_CurStageStats.iTotalError[m_PlayerNumber] += ms_error;
				if( !GAMESTATE->m_PlayerOptions[m_PlayerNumber].m_fBlind )
					m_ProTimingDisplay.SetJudgment( ms_error, ReverseGrade( score ) );
			}

			if( score == TNS_MARVELOUS && !GAMESTATE->ShowMarvelous(m_PlayerNumber) )
				score = TNS_PERFECT;

			//LOG->Trace("XXX: %i col %i, at %f, music at %f, step was at %f, off by %f", score, col, fReleaseSeconds, fCurrentMusicSeconds, fMusicSeconds, fNoteOffset );

			SetTapNoteScore(col, iIndexOverlappingNote, score);
			SetTapNoteOffset(col, iIndexOverlappingNote, -fNoteOffset);

			if( GAMESTATE->m_PlayerController[m_PlayerNumber] == PC_HUMAN  && score >= TNS_GREAT )
				HandleAutosync(fNoteOffset);

			//Keep this here so we get the same data as Autosync
			NSMAN->ReportTiming(fNoteOffset,m_PlayerNumber);

			switch( tn.type )
			{
			case TapNote::lift:
				// don't judge the row if this note is a mine or tap attack
				if( IsRowCompletelyJudged(iIndexOverlappingNote) )
					OnRowCompletelyJudged( iIndexOverlappingNote, bSteppedEarly );
			}

			if( score == TNS_MISS || score == TNS_BOO )
			{
				m_iDCState = AS2D_MISS;
			}
			if( score == TNS_GOOD || score == TNS_GREAT )
			{
				m_iDCState = AS2D_GOOD;
			}
			if( score == TNS_PERFECT || score == TNS_MARVELOUS )
			{
				m_iDCState = AS2D_GREAT;

				if( m_pLifeMeter && m_pLifeMeter->GetLife() == 1.0f) // full life
					m_iDCState = AS2D_FEVER; // super celebrate time :)
			}
			if( score == TNS_HIDDEN )
				m_iDCState = AS2D_FEVER;	// Found the hidden notes! Super celebrate!

			m_pNoteField->Step( col, ReverseGrade( score ) );
		}
		else
		{
			tn = m_EmptyNote;
		}
	}
}

void PlayerMinus::HandleAutosync( float fNoteOffset )
{
	if( !GAMESTATE->m_SongOptions.m_bAutoSync )
		return;

	m_fOffset[m_iOffsetSample++] = fNoteOffset;
	if (m_iOffsetSample < SAMPLE_COUNT)
		return; /* need more */

	const float mean = calc_mean(m_fOffset, m_fOffset+SAMPLE_COUNT);
	const float stddev = calc_stddev(m_fOffset, m_fOffset+SAMPLE_COUNT);

	//If they stepped with less than .03 error
	if (stddev < .03 && stddev < fabsf(mean))
	{
		m_Timing->m_fBeat0Offset += mean;
		LOG->Trace("Offset corrected by %f. Error in steps: %f seconds.", mean, stddev);
		SCREENMAN->SystemMessage( "Autosync correction applied." );
	}
	//If we're outside the deviation -and- not synced properly - Mark
	else if ( stddev >= .03 )
	{
		LOG->Trace("Offset NOT corrected. Average offset: %f seconds. Error: %f seconds.", mean, stddev);
		SCREENMAN->SystemMessage( "Autosync correction NOT applied: too much deviation." );
	}

	m_iOffsetSample = 0;
}


void PlayerMinus::OnRowCompletelyJudged( int iIndexThatWasSteppedOn, bool bSteppedEarly )
{
//	LOG->Trace( "PlayerMinus::OnRowCompletelyJudged" );

	/* Find the minimum score of the row.  This will never be TNS_NONE, since this
	* function is only called when a row is completed. */
	/* Instead, use the last tap score (ala DDR).  Using the minimum results in
	* slightly more harsh scoring than DDR */
	/* I'm not sure this is right, either.  Can you really jump a boo and a perfect
	* and get scored for a perfect?  (That's so loose, you can gallop jumps.) -glenn */
	/* Instead of grading individual columns, DDR sets a "was pressed recently"
	* countdown every time you step on a column.  When you step on the first note of
	* the jump, it sets the first "was pressed recently" timer.  Then, when you do
	* the 2nd step of the jump, it sets another column's timer then AND's the jump
	* columns with the "was pressed recently" columns to see whether or not you hit
	* all the columns of the jump.  -Chris */

//	TapNoteScore score = MinTapNoteScore(iIndexThatWasSteppedOn);
	TapNoteScore score = LastTapNoteScore(iIndexThatWasSteppedOn);
	TapNoteScore success = PREFSMAN->m_bComboBreakOnGood ? TNS_GREAT : TNS_GOOD;

	ASSERT(score >= TNS_MISS_CHECKPOINT);

	// If the whole row was hit with perfects or greats, remove the row from the NoteField, so it disappears.
	for( int c=0; c<GetNumTracks(); c++ )	// for each column
	{
		TapNote tn = GetTapNote(c, iIndexThatWasSteppedOn);

		if( tn.type == TapNote::empty )		continue; // no note in this col
		if( tn.type == TapNote::mine )		continue; // don't flash on mines b/c they're supposed to be missed
		if( tn.type == TapNote::shock )		continue; // don't flash on shocks b/c they're supposed to be missed
		if( tn.type == TapNote::potion )	continue; // don't flash on potions b/c they're supposed to be complementary

		// If the score is great or better, remove the note from the screen to
		// indicate success.  (Or always if blind is on.)
		if( score == TNS_HIDDEN || score >= success || GAMESTATE->m_PlayerOptions[m_PlayerNumber].m_fBlind )
			m_pNoteField->SetTapNote(c, iIndexThatWasSteppedOn, m_EmptyNote);

		// show the ghost arrow for this column
		if( GAMESTATE->m_PlayerOptions[m_PlayerNumber].m_fBlind )
			m_pNoteField->DidTapNote( c, TNS_MARVELOUS, false );
		else
		{
			if( score == TNS_HIDDEN )	// Always flash the bright ghost if they hit a hidden note
				m_pNoteField->DidTapNote( c, score, true );
			else if( score >= success )
			{
				bool bBright;

				if( GAMESTATE->m_PlayerController[m_PlayerNumber] == PC_HUMAN )
					bBright = g_CurStageStats.iCurCombo[m_PlayerNumber] >= BRIGHT_GHOST_COMBO_THRESHOLD;
				else
					bBright = GAMESTATE->m_iNotesInAutoPlay[m_PlayerNumber] >= BRIGHT_GHOST_COMBO_THRESHOLD;

				m_pNoteField->DidTapNote( c, score, bBright );
			}
		}
	}

	HandleTapRowScore( iIndexThatWasSteppedOn );	// update score

	m_Judgment->SetJudgment( ReverseGrade( score ), bSteppedEarly );
}


void PlayerMinus::UpdateTapNotesMissedOlderThan( int row, bool bFreeze )
{
	// Row should not be less than 0...
	if( row < 0 || GAMESTATE->m_fPlayerBeat[m_PlayerNumber] < 0 )
		return;
	else if( row == 0 )
	{
		// HACK: Don't miss when the fist beat is a delay, but do miss if its a freeze
		if( GAMESTATE->m_fPlayerBeat[m_PlayerNumber] == 0 && !GAMESTATE->m_bFreeze[m_PlayerNumber] )
			return;
	}

	//LOG->Trace( "PlayerMinus::UpdateTapNotesMissedOlderThan( %d, $i )", iMissIfOlderThanThisIndex, bFreeze );

	bool MissedNoteOnThisRow = false;

	for( int t=0; t<GetNumTracks(); t++ )
	{
		TapNote tn = this->GetTapNote( t, row );

		switch( tn.type )
		{
			case TapNote::hold_head:	// Should not exist with PIU Holds
				if( PREFSMAN->m_bUsePIUHolds )
				{
					TapNoteScore score = TNS_MISS;

					unsigned uComboMultiplier;
					float fScoreMultiplier;
					m_Timing->GetMultiplierAtBeat( NoteRowToBeat( row ), score, uComboMultiplier, fScoreMultiplier, fScoreMultiplier );

					// HACK: Oni percents gets messed with checkpoints due to hold heads being counted in radar values.
					if( m_pPrimaryScoreKeeper )
						m_pPrimaryScoreKeeper->HandleTapScore( score, uComboMultiplier, fScoreMultiplier );
					if( m_pSecondaryScoreKeeper )
						m_pSecondaryScoreKeeper->HandleTapScore( score, uComboMultiplier, fScoreMultiplier );

					tn.type = TapNote::empty;
					this->SetTapNote( t, row, tn );
				}
				else
					break;

			case TapNote::tap:
			case TapNote::lift:
			case TapNote::hidden:
			case TapNote::shock:
				// Only consider defined note types, ignore the rest
				break;

			case TapNote::empty:	// No note existed here
				if( tn.checkpoints > 0 && !bFreeze )
					MissSkippedCheckpoint( t, row );
				// fall-through
			case TapNote::mine:		// We don't want Mines to have an impact
			case TapNote::potion:	// We don't want Potions to have an impact
				continue;

			default:
				continue;
		}

		if( GetTapNoteScore( t, row ) != TNS_NONE ) // note here is already hit
			continue;
		else
		{
			if( tn.type == TapNote::hidden )
			{
				MissedNoteOnThisRow = false;	// We don't count hidden notes as normal notes
				SetTapNoteScore( t, row, TNS_MISS_HIDDEN );
			}
			else if( tn.type == TapNote::shock )
			{
				MissedNoteOnThisRow = false;	// We don't count shock arrows as normal notes
				g_CurStageStats.iCurCombo[m_PlayerNumber]++;
				g_CurStageStats.iCurMissCombo[m_PlayerNumber] = 0;

				if( GAMESTATE->m_PlayerOptions[m_PlayerNumber].m_bReverseGrade )
					m_Combo.SetCombo( g_CurStageStats.iCurMissCombo[m_PlayerNumber], g_CurStageStats.iCurCombo[m_PlayerNumber] );
				else
					m_Combo.SetCombo( g_CurStageStats.iCurCombo[m_PlayerNumber], g_CurStageStats.iCurMissCombo[m_PlayerNumber] );

				m_HoldJudgment[t].SetHoldJudgment( SHOCK_OK );
				m_pNoteField->SetTapNote( t, row, m_EmptyNote );    // remove from NoteField
			}
			else
			{
				// A normal note.  Penalize for not stepping on it.
				MissedNoteOnThisRow = true;
				SetTapNoteScore( t, row, TNS_MISS );
				g_CurStageStats.iTotalError[m_PlayerNumber] += MAX_PRO_TIMING_ERROR;
				m_ProTimingDisplay.SetJudgment( MAX_PRO_TIMING_ERROR, ReverseGrade( TNS_MISS ) );
			}
		}
	}

	if( MissedNoteOnThisRow )
	{
		HandleTapRowScore( row );
		m_Judgment->SetJudgment( ReverseGrade( TNS_MISS ), false );	// You can't be early on a miss...
	}
}

void PlayerMinus::CrossedRow( int iNoteRow )
{
	//LOG->Trace( "PlayerMinus::CrossedRow( %d )", iNoteRow );

	// If we're doing random vanish, randomise notes on the fly.
	if(GAMESTATE->m_CurrentPlayerOptions[m_PlayerNumber].m_fAppearances[PlayerOptions::APPEARANCE_RANDOMVANISH]==1)
		RandomiseNotes( iNoteRow );

	// check to see if there's a note at the crossed row
	RageTimer now;
	if( GAMESTATE->m_PlayerController[m_PlayerNumber] != PC_HUMAN )
		for( int t=0; t<GetNumTracks(); t++ )
			if( GetTapNote(t, iNoteRow).type != TapNote::empty && GetTapNote(t, iNoteRow).type != TapNote::hidden )
				// Autoplay and CPU also use Step() to judge lift notes. Release() is only for humans.
				if( GetTapNoteScore(t, iNoteRow) == TNS_NONE )
					Step( t, now );
}

void PlayerMinus::CrossedItemRow( int iNoteRow )
{
	//LOG->Trace( "PlayerMinus::CrossedItemRow( %d )", iNoteRow );

	// Hold the panel while crossing a mine/shock will cause the item to activate
	RageTimer now;
	for( int t=0; t<GetNumTracks(); t++ )
	{
		unsigned type = GetTapNote(t,iNoteRow).type;
		if( type == TapNote::mine || type == TapNote::shock )
		{
			const StyleInput StyleI( m_PlayerNumber, t );
			const GameInput GameI = GAMESTATE->GetCurrentStyle()->StyleInputToGameInput( StyleI );

			if( PREFSMAN->m_fPadStickSeconds > 0 )
			{
				float fSecsHeld = INPUTMAPPER->GetSecsHeld( GameI );
				if( fSecsHeld >= PREFSMAN->m_fPadStickSeconds )
					Step( t, now+(-PREFSMAN->m_fPadStickSeconds) );
			}
			else
			{
				bool bIsDown = INPUTMAPPER->IsButtonDown( GameI );
				if( bIsDown )
					Step( t, now );
			}
		}
	}
}

void PlayerMinus::RandomiseNotes( int iNoteRow )
{
	// change the row to look ahead from based upon their speed mod
	/* This is incorrect: if m_fScrollSpeed is 0.5, we'll never change
	 * any odd rows, and if it's 2, we'll shuffle each row twice. */
	int iNewNoteRow = iNoteRow + ROWS_PER_BEAT*2;
	iNewNoteRow = int( iNewNoteRow / GAMESTATE->m_PlayerOptions[m_PlayerNumber].m_fScrollSpeed );

	int iNumOfTracks = GetNumTracks();
	for( int t=0; t+1 < iNumOfTracks; t++ )
	{
		const int iSwapWith = RandomInt( 0, iNumOfTracks-1 );

		/* Only swap a tap and an empty. */
		const TapNote t1 = GetTapNote(t, iNewNoteRow);
		if( t1.type != TapNote::tap )
			continue;

		const TapNote t2 = GetTapNote( iSwapWith, iNewNoteRow );
		if( t2.type != TapNote::empty )
			continue;

		/* Make sure the destination row isn't in the middle of a hold. */
		bool bHoldSkip = false;
		for( int i = 0; !bHoldSkip && i < GetNumHoldsAndRolls(); ++i )
		{
			const HoldNote &hn = GetHoldNote(i);
			if( hn.iTrack == iSwapWith && hn.RowIsInRange(iNewNoteRow) )
				bHoldSkip = true;
		}
		if( bHoldSkip )
			continue;

		SetTapNote( t, iNewNoteRow, t2 );
		SetTapNote( iSwapWith, iNewNoteRow, t1 );

		const TapNote nft1 = m_pNoteField->GetTapNote( t, iNewNoteRow );
		const TapNote nft2 = m_pNoteField->GetTapNote( iSwapWith, iNewNoteRow );
		m_pNoteField->SetTapNote( t, iNewNoteRow, nft2 );
		m_pNoteField->SetTapNote( iSwapWith, iNewNoteRow, nft1 );
	}
}

void PlayerMinus::HandleTapRowScore( unsigned row, TapNoteScore scoreOfLastTap )
{
	if( scoreOfLastTap == TNS_NONE )
	{
		scoreOfLastTap = LastTapNoteScore(row);

		if( scoreOfLastTap == TNS_NONE )
			return;
	}

	int iNumCheckpointsInRow = this->GetNumTracksWithCheckpoint(row);
	int iNumNotesInRow = this->GetNumTracksWithTapOrHiddenOrHoldHead(row);
	int iNumTapsInRow = iNumCheckpointsInRow + iNumNotesInRow;

	ASSERT( iNumTapsInRow > 0 );

	// True if a jump/hand/quad is one to combo, false if combo is purely based on tap count.
	bool ComboIsPerRow = true;

	switch( PREFSMAN->m_iScoringType )
	{
	case PrefsManager::SCORING_MAX2:
	case PrefsManager::SCORING_HYBRID:
		ComboIsPerRow = (GAMESTATE->m_PlayMode == PLAY_MODE_ONI);
		break;
	case PrefsManager::SCORING_5TH:
	case PrefsManager::SCORING_NOVA:
	case PrefsManager::SCORING_NOVA2:
	case PrefsManager::SCORING_PIU_NX2:
	case PrefsManager::SCORING_PIU_ZERO:
	case PrefsManager::SCORING_PIU_PREX3:
	case PrefsManager::SCORING_PIU_EXTRA:
		ComboIsPerRow = true;
		break;
	case PrefsManager::SCORING_CUSTOM:
		ComboIsPerRow = CUSTOM_COMBO_PER_ROW;
		break;
	default:
		ASSERT(0);
	}

	// Override the default scoring values
	if( OVERRIDE_COMBO_INCREMENT )
	{
		if( OVERRIDE_COMBO_PER_ROW )
			ComboIsPerRow = true;
		else
			ComboIsPerRow = false;
	}

	unsigned uComboMultiplier;
	float fLifeMultiplier, fScoreMultiplier;
	m_Timing->GetMultiplierAtBeat( NoteRowToBeat( row ), scoreOfLastTap, uComboMultiplier, fLifeMultiplier, fScoreMultiplier );

	int iComboCount;
	iComboCount = ComboIsPerRow ? 1 : iNumTapsInRow;
	iComboCount *= uComboMultiplier;

	// don't accumulate points if AutoPlay is on.
	if( !GAMESTATE->m_bDemonstrationOrJukebox && GAMESTATE->m_PlayerController[m_PlayerNumber] != PC_HUMAN )
	{
		// We won't throw this flag on AutoPlay until we reach the first tap note
		GAMESTATE->m_iNotesInAutoPlay[m_PlayerNumber] += iComboCount;

		if( GAMESTATE->m_PlayerController[m_PlayerNumber] == PC_AUTOPLAY )
		{
			if( PREFSMAN->m_bAutoPlayCombo )
			{
				if( GAMESTATE->m_PlayerOptions[m_PlayerNumber].m_bReverseGrade )
					m_Combo.SetCombo( 0, GAMESTATE->m_iNotesInAutoPlay[m_PlayerNumber] );
				else
					m_Combo.SetCombo( GAMESTATE->m_iNotesInAutoPlay[m_PlayerNumber], 0 );
			}
			return;
		}
	}

	// Update miss combo, and handle "combo stopped" messages.
	int &iCurCombo = g_CurStageStats.iCurCombo[m_PlayerNumber];

	switch( scoreOfLastTap )
	{
	case TNS_CHECKPOINT:
	case TNS_MARVELOUS:
	case TNS_PERFECT:
	case TNS_GREAT:
		g_CurStageStats.iCurMissCombo[m_PlayerNumber] = 0;
		SCREENMAN->PostMessageToTopScreen( SM_MissComboAborted, 0 );
		break;

	case TNS_MISS_CHECKPOINT:
	case TNS_MISS:
		iCurCombo = 0;
		g_CurStageStats.iCurMissCombo[m_PlayerNumber] += iComboCount;

		m_iDCState = AS2D_MISS; // update dancing 2d characters that may have missed a note
		// fall through

	case TNS_BOO:
		if( iCurCombo > 50 )
			SCREENMAN->PostMessageToTopScreen( SM_ComboStopped, 0 );

		if( PREFSMAN->m_bMissComboIncreasesOnComboBreak && scoreOfLastTap == TNS_BOO && iCurCombo > 0 )
			g_CurStageStats.iCurMissCombo[m_PlayerNumber] += iComboCount;
		else if( !PREFSMAN->m_bMissComboIncreasesOnComboBreak && scoreOfLastTap == TNS_BOO )
		{
			g_CurStageStats.iCurMissCombo[m_PlayerNumber] = 0;
			SCREENMAN->PostMessageToTopScreen( SM_MissComboAborted, 0 );
		}

		iCurCombo = 0;
		break;

	case TNS_GOOD:
		if( PREFSMAN->m_bComboBreakOnGood )
		{
			if( iCurCombo > 50 )
				SCREENMAN->PostMessageToTopScreen( SM_ComboStopped, 0 );

			if( PREFSMAN->m_bMissComboIncreasesOnComboBreak && iCurCombo > 0 )
				g_CurStageStats.iCurMissCombo[m_PlayerNumber] += iComboCount;
			else if( !PREFSMAN->m_bMissComboIncreasesOnComboBreak )
			{
				g_CurStageStats.iCurMissCombo[m_PlayerNumber] = 0;
				SCREENMAN->PostMessageToTopScreen( SM_MissComboAborted, 0 );
			}

			iCurCombo = 0;
		}
		else
		{
			g_CurStageStats.iCurMissCombo[m_PlayerNumber] = 0;
			SCREENMAN->PostMessageToTopScreen( SM_MissComboAborted, 0 );
		}

		break;

	case TNS_HIDDEN:
	case TNS_MISS_HIDDEN:
		break;

	default:
		ASSERT( 0 );
	}

	// The score keeper updates the hit combo.  Remember the old combo for handling announcers.
	const int iOldCombo = g_CurStageStats.iCurCombo[m_PlayerNumber];

	if( m_pPrimaryScoreKeeper )
		m_pPrimaryScoreKeeper->HandleTapRowScore( scoreOfLastTap, iNumNotesInRow, iNumCheckpointsInRow, uComboMultiplier, fScoreMultiplier );
	if( m_pSecondaryScoreKeeper )
		m_pSecondaryScoreKeeper->HandleTapRowScore( scoreOfLastTap, iNumNotesInRow, iNumCheckpointsInRow, uComboMultiplier, fScoreMultiplier );

	if( GAMESTATE->m_PlayerOptions[m_PlayerNumber].m_bReverseGrade )
		m_Combo.SetCombo( g_CurStageStats.iCurMissCombo[m_PlayerNumber], g_CurStageStats.iCurCombo[m_PlayerNumber] );
	else
		m_Combo.SetCombo( g_CurStageStats.iCurCombo[m_PlayerNumber], g_CurStageStats.iCurMissCombo[m_PlayerNumber] );

#define CROSSED( x ) (iOldCombo<x && iCurCombo>=x)
	if( (iOldCombo / 100) < (iCurCombo / 100) && iCurCombo > 1000 )
		SCREENMAN->PostMessageToTopScreen( SM_ComboContinuing, 0 );
	else if( CROSSED(1000))
		SCREENMAN->PostMessageToTopScreen( SM_1000Combo, 0 );
	else if( CROSSED(900) )
		SCREENMAN->PostMessageToTopScreen( SM_900Combo, 0 );
	else if( CROSSED(800) )
		SCREENMAN->PostMessageToTopScreen( SM_800Combo, 0 );
	else if( CROSSED(700) )
		SCREENMAN->PostMessageToTopScreen( SM_700Combo, 0 );
	else if( CROSSED(600) )
		SCREENMAN->PostMessageToTopScreen( SM_600Combo, 0 );
	else if( CROSSED(500) )
		SCREENMAN->PostMessageToTopScreen( SM_500Combo, 0 );
	else if( CROSSED(400) )
		SCREENMAN->PostMessageToTopScreen( SM_400Combo, 0 );
	else if( CROSSED(300) )
		SCREENMAN->PostMessageToTopScreen( SM_300Combo, 0 );
	else if( CROSSED(200) )
		SCREENMAN->PostMessageToTopScreen( SM_200Combo, 0 );
	else if( CROSSED(100) )
		SCREENMAN->PostMessageToTopScreen( SM_100Combo, 0 );
#undef CROSSED

	// new max combo
	g_CurStageStats.iMaxCombo[m_PlayerNumber] = max(g_CurStageStats.iMaxCombo[m_PlayerNumber], iCurCombo);

	/* Use the real current beat, not the beat we've been passed.  That's because we
	 * want to record the current life/combo to the current time; eg. if it's a MISS,
	 * the beat we're registering is in the past, but the life is changing now. */
	g_CurStageStats.UpdateComboList( m_PlayerNumber, g_CurStageStats.fAliveSeconds[m_PlayerNumber], false );

	float life = -1;

	if( m_pLifeMeter )
		life = m_pLifeMeter->GetLife();

	else if( m_pCombinedLifeMeter )
	{
		life = GAMESTATE->m_fTugLifePercentP1;
		if( m_PlayerNumber == PLAYER_2 )
			life = 1.0f - life;
	}

	if( life != -1 )
		g_CurStageStats.SetLifeRecordAt( m_PlayerNumber, life, g_CurStageStats.fAliveSeconds[m_PlayerNumber] );

	if (m_pScoreDisplay)
		m_pScoreDisplay->SetScore(g_CurStageStats.iScore[m_PlayerNumber]);

	if (m_pSecondaryScoreDisplay)
		m_pSecondaryScoreDisplay->SetScore(g_CurStageStats.iScore[m_PlayerNumber]);

	if( m_pLifeMeter )
	{
		m_pLifeMeter->ChangeLife( scoreOfLastTap, fLifeMultiplier );
		m_pLifeMeter->OnDancePointsChange();    // update oni life meter
	}
	if( m_pCombinedLifeMeter )
	{
		m_pCombinedLifeMeter->ChangeLife( m_PlayerNumber, scoreOfLastTap, fLifeMultiplier );
		m_pCombinedLifeMeter->OnDancePointsChange( m_PlayerNumber );    // update oni life meter
	}
}

void PlayerMinus::HandleHoldScore( HoldNoteScore holdScore, const unsigned& uComboMultiplier, const float& fScoreMultiplier, const float& fLifeMultiplier )
{
	// don't accumulate points if AutoPlay is on.
	if( GAMESTATE->m_PlayerController[m_PlayerNumber] == PC_AUTOPLAY )
		return;

	if( m_pPrimaryScoreKeeper )
		m_pPrimaryScoreKeeper->HandleHoldScore( holdScore, uComboMultiplier, fScoreMultiplier );
	if( m_pSecondaryScoreKeeper )
		m_pSecondaryScoreKeeper->HandleHoldScore( holdScore, uComboMultiplier, fScoreMultiplier );

	if( m_pScoreDisplay )
		m_pScoreDisplay->SetScore(g_CurStageStats.iScore[m_PlayerNumber]);
	if( m_pSecondaryScoreDisplay )
		m_pSecondaryScoreDisplay->SetScore(g_CurStageStats.iScore[m_PlayerNumber]);

	if( m_pLifeMeter )
	{
		m_pLifeMeter->ChangeLife( holdScore, fLifeMultiplier );
		m_pLifeMeter->OnDancePointsChange();
	}
	if( m_pCombinedLifeMeter )
	{
		m_pCombinedLifeMeter->ChangeLife( m_PlayerNumber, holdScore, fLifeMultiplier );
		m_pCombinedLifeMeter->OnDancePointsChange( m_PlayerNumber );
	}
}

float PlayerMinus::GetMaxStepDistanceSeconds()
{
	return GAMESTATE->m_SongOptions.m_fMusicRate * max( ADJUSTED_WINDOW_AFTER(Boo), ADJUSTED_WINDOW(Boo) );
}

void PlayerMinus::FadeToFail()
{
	m_pNoteField->FadeToFail();
}

/* XXX: Why's m_NoteField in a separate class, again?  Is that still needed? */
void Player::Load( PlayerNumber player_no, const NoteData* pNoteData, LifeMeter* pLM, CombinedLifeMeter* pCombinedLM, ScoreDisplay* pScoreDisplay, ScoreDisplay* pSecondaryScoreDisplay, Inventory* pInventory, ScoreKeeper* pPrimaryScoreKeeper, ScoreKeeper* pSecondaryScoreKeeper )
{
	PlayerMinus::Load( player_no, pNoteData, pLM, pCombinedLM, pScoreDisplay, pSecondaryScoreDisplay, pInventory, pPrimaryScoreKeeper, pSecondaryScoreKeeper, &m_NoteField );
}

CString PlayerMinus::ApplyRandomAttack()
{
	int iAttackVectorSize = GAMESTATE->m_RandomAttacks.size();

	if( iAttackVectorSize < 1 )
	{
		LOG->Warn( "Attempting to use Random Modifiers, but none specified in file. Attacks will be nullified." );
		return "";
	}

	int iAttackToUse = rand() % iAttackVectorSize;
	return GAMESTATE->m_RandomAttacks[iAttackToUse];
}

void PlayerMinus::UpdateHoldNotes( float fDeltaTime, int iRow )
{
	for( int h=m_iLastHold; h<GetNumHoldsAndRolls(); ++h )		// for each HoldNote
	{
		const HoldNote &hn = GetHoldNote(h);

		if( hn.behaviorFlag & (TapNote::behavior_fake | TapNote::behavior_bonus) )
		{
			// HACK: Oni percents gets messed with fake holds due to hold heads being counted in radar values.
			if( m_pPrimaryScoreKeeper )
				m_pPrimaryScoreKeeper->HandleHoldScore( hn.subtype == HOLD_TYPE_ROLL ? RNS_OK : HNS_OK, 0, 0 );
			if( m_pSecondaryScoreKeeper )
				m_pSecondaryScoreKeeper->HandleHoldScore( hn.subtype == HOLD_TYPE_ROLL ? RNS_OK : HNS_OK, 0, 0 );

			if( h == m_iLastHold )
				m_iLastHold++;

			continue;
		}

		HoldNoteScore hns = GetHoldNoteScore(hn);

		m_pNoteField->m_HeldHoldNotes[hn] = false;	// set hold flag so NoteField can do intelligent drawing
		m_pNoteField->m_ActiveHoldNotes[hn] = false;	// set hold flag so NoteField can do intelligent drawing

		if( iRow < hn.iStartRow )
			continue;				// hold hasn't happened yet

		if( hns != HNS_NONE )	// if this HoldNote already has a result
			continue;				// we don't need to update the logic for this one

		// If they got a bad score or haven't stepped on the corresponding tap yet
		const TapNoteScore tns = GetTapNoteScore( hn.iTrack, hn.iStartRow );

		if( tns == TNS_NONE )
			continue;

		// We need those variables in areas non affected by this function
		m_iCurrentHoldEndRow[hn.iTrack] = hn.iEndRow;
		m_bCrossingHoldNote[hn.iTrack] = hn.iStartRow <= iRow && iRow <= hn.iEndRow;

		bool bSteppedOnTapNote = tns > TNS_MISS;	// did they step on the start of this hold?

		const StyleInput StyleI( m_PlayerNumber, hn.iTrack );
		const GameInput GameI = GAMESTATE->GetCurrentStyle()->StyleInputToGameInput( StyleI );
		bool bIsHoldingButton = INPUTMAPPER->IsButtonDown( GameI );

		// Update Hold Pressed Flag
		if( !m_bPressed[hn] && ( bIsHoldingButton || GAMESTATE->m_PlayerController[m_PlayerNumber] != PC_HUMAN ) )
			m_bPressed[hn] = true;

		if( bIsHoldingButton )
			m_HoldPressed[hn.iTrack] = GAMESTATE->m_fMusicSeconds - PREFSMAN->m_fPadStickSeconds;

		float fLife = GetHoldNoteLife(hn);

		// If the song beat is in the range of this hold:
		if( m_bCrossingHoldNote[hn.iTrack] )
		{
			// MustRelease is only needed for roll notes
			if( hn.subtype != HOLD_TYPE_ROLL )
				m_bMustRelease[hn] = false;

			// TODO: Make the CPU miss sometimes.
			if( GAMESTATE->m_PlayerController[m_PlayerNumber] != PC_HUMAN )
			{
				if( hn.subtype == HOLD_TYPE_ROLL )
				{
					// Define this explicitly!
					bIsHoldingButton = false;

					// HACK: First is to make the rolls look more realistic when hit by the CPU (since a player
					// doesn't release instantly) by keeping it 'held' for a bit longer
					// The second if makes the CPU rehit the roll to keep it alive
					if( fLife > CPU_ROLL_LETGO_LIFE )
						bIsHoldingButton = true;
					else if( fLife < CPU_ROLL_REHIT_LIFE )
					{
						bIsHoldingButton = true;
						m_bMustRelease[hn] = false;
					}
				}
				else
					bIsHoldingButton = true;
			}
			// Okay, so we have a human player. Make this check for rolls.
			else if( hn.subtype == HOLD_TYPE_ROLL )
			{
				if( !bIsHoldingButton )
					m_bMustRelease[hn] = false;
			}

			// set hold flag so NoteField can do intelligent drawing
			if( PREFSMAN->m_bUsePIUHolds )
			{
				m_pNoteField->m_HeldHoldNotes[hn] = bIsHoldingButton;
				m_pNoteField->m_ActiveHoldNotes[hn] = fLife > 0 && m_bPressed[hn];
			}
			else
			{
				m_pNoteField->m_HeldHoldNotes[hn] = bIsHoldingButton && bSteppedOnTapNote;
				m_pNoteField->m_ActiveHoldNotes[hn] = bSteppedOnTapNote;
			}

			if( bIsHoldingButton && ( PREFSMAN->m_bUsePIUHolds && m_bPressed[hn] || !PREFSMAN->m_bUsePIUHolds && bSteppedOnTapNote ) )
			{
				// This hold note is not judged and we stepped on its head. Update iLastHeldRow.
				HoldNoteResult *hnr = m_pNoteField->CreateHoldNoteResult( hn );
				hnr->iLastHeldRow = min( iRow, hn.iEndRow );

				hnr = this->CreateHoldNoteResult( hn );
				hnr->iLastHeldRow = min( iRow, hn.iEndRow );
			}

			if( ( PREFSMAN->m_bUsePIUHolds || bSteppedOnTapNote && !PREFSMAN->m_bUsePIUHolds ) && bIsHoldingButton && !m_bMustRelease[hn] )
			{
				// Increase life
				fLife = 1;

				// Only for roll notes
				if( hn.subtype == HOLD_TYPE_ROLL )
				{
					m_bMustRelease[hn] = true;

					if( PREFSMAN->m_bComboIncreasesOnRollHits )
					{
						const unsigned uMultiplier = m_Timing->GetHitMultiplierAtBeat( NoteRowToBeat( hn.iEndRow ), TNS_CHECKPOINT );

						if( GAMESTATE->m_PlayerController[m_PlayerNumber] == PC_AUTOPLAY )
						{
							GAMESTATE->m_iNotesInAutoPlay[m_PlayerNumber] += uMultiplier;

							if( PREFSMAN->m_bAutoPlayCombo )
							{
								if( GAMESTATE->m_PlayerOptions[m_PlayerNumber].m_bReverseGrade )
									m_Combo.SetCombo( 0, GAMESTATE->m_iNotesInAutoPlay[m_PlayerNumber] );
								else
									m_Combo.SetCombo( GAMESTATE->m_iNotesInAutoPlay[m_PlayerNumber], 0 );
							}
						}
						else
						{
							g_CurStageStats.iCurCombo[m_PlayerNumber] += uMultiplier;
							g_CurStageStats.iCurMissCombo[m_PlayerNumber] = 0;

							if( GAMESTATE->m_PlayerOptions[m_PlayerNumber].m_bReverseGrade )
								m_Combo.SetCombo( g_CurStageStats.iCurMissCombo[m_PlayerNumber], g_CurStageStats.iCurCombo[m_PlayerNumber] );
							else
								m_Combo.SetCombo( g_CurStageStats.iCurCombo[m_PlayerNumber], g_CurStageStats.iCurMissCombo[m_PlayerNumber] );
						}
					}

					if( PREFSMAN->m_bUsePIUHolds )
					{
						bool bBright;

						if( GAMESTATE->m_PlayerController[m_PlayerNumber] == PC_HUMAN )
							bBright = g_CurStageStats.iCurCombo[m_PlayerNumber] >= BRIGHT_GHOST_COMBO_THRESHOLD;
						else
							bBright = GAMESTATE->m_iNotesInAutoPlay[m_PlayerNumber] >= BRIGHT_GHOST_COMBO_THRESHOLD;

						m_pNoteField->DidTapNote( hn.iTrack, TNS_CHECKPOINT, bBright );
					}
				}

				if( !PREFSMAN->m_bUsePIUHolds )
					m_pNoteField->DidHoldNote( hn.iTrack );		// update the "electric ghost" effect
			}
			else
			{
				// Decrease life
				if( hn.subtype == HOLD_TYPE_ROLL )
				{
					// JudgeWindowSecondsRoll takes effect here. - Mark
					if( PREFSMAN->m_bRollTapTimingIsAffectedByJudge )
						fLife -= fDeltaTime/ADJUSTED_WINDOW(Roll);
					else
						fLife -= fDeltaTime/NON_ADJUSTED_WINDOW(Roll);
				}
				else
				{
					if( PREFSMAN->m_bUsePIUHolds )
						fLife -= fDeltaTime/ADJUSTED_WINDOW_AFTER(Long);
					else
						fLife -= fDeltaTime/ADJUSTED_WINDOW(Hold);
				}

				fLife = max( fLife, 0 );	// clamp
			}
		}

		bool bHoldEnded = ( GAMESTATE->m_bFreeze[m_PlayerNumber] && iRow >= hn.iEndRow ) || iRow > hn.iEndRow;

		if( PREFSMAN->m_bUsePIUHolds && bHoldEnded || !PREFSMAN->m_bUsePIUHolds )
		{
			// check for NG.  If the head was missed completely, don't count an NG.
			if( ( PREFSMAN->m_bUsePIUHolds || bSteppedOnTapNote && !PREFSMAN->m_bUsePIUHolds ) && fLife == 0 || !m_bPressed[hn] )	// the player has not pressed the button for a long time!
			{
				hns = hn.subtype == HOLD_TYPE_ROLL ? HNS_NG : RNS_NG;

				if( PREFSMAN->m_bHoldNGBreaksCombo )
				{
					const unsigned uMultiplier = m_Timing->GetHitMultiplierAtBeat( NoteRowToBeat( hn.iEndRow ), TNS_MISS_CHECKPOINT );

					g_CurStageStats.iCurCombo[m_PlayerNumber] = 0;
					g_CurStageStats.iCurMissCombo[m_PlayerNumber] += uMultiplier;

					if( GAMESTATE->m_PlayerOptions[m_PlayerNumber].m_bReverseGrade )
						m_Combo.SetCombo( g_CurStageStats.iCurMissCombo[m_PlayerNumber], g_CurStageStats.iCurCombo[m_PlayerNumber] );
					else
						m_Combo.SetCombo( g_CurStageStats.iCurCombo[m_PlayerNumber], g_CurStageStats.iCurMissCombo[m_PlayerNumber] );
				}
			}

			// check for OK
			if( bHoldEnded && ( PREFSMAN->m_bUsePIUHolds || bSteppedOnTapNote && !PREFSMAN->m_bUsePIUHolds ) && fLife > 0 && m_bPressed[hn] )	// if this HoldNote is in the past
			{
				fLife = 1;
				hns = hn.subtype == HOLD_TYPE_ROLL ? HNS_OK : RNS_OK;

				if( PREFSMAN->m_bHoldOKIncreasesCombo )
				{
					const unsigned uMultiplier = m_Timing->GetHitMultiplierAtBeat( NoteRowToBeat( hn.iEndRow ), TNS_CHECKPOINT );

					if( GAMESTATE->m_PlayerController[m_PlayerNumber] == PC_AUTOPLAY )
					{
						GAMESTATE->m_iNotesInAutoPlay[m_PlayerNumber] += uMultiplier;

						if( PREFSMAN->m_bAutoPlayCombo )
						{
							if( GAMESTATE->m_PlayerOptions[m_PlayerNumber].m_bReverseGrade )
								m_Combo.SetCombo( 0, GAMESTATE->m_iNotesInAutoPlay[m_PlayerNumber] );
							else
								m_Combo.SetCombo( GAMESTATE->m_iNotesInAutoPlay[m_PlayerNumber], 0 );
						}
					}
					else
					{
						g_CurStageStats.iCurCombo[m_PlayerNumber] += uMultiplier;
						g_CurStageStats.iCurMissCombo[m_PlayerNumber] = 0;

						if( GAMESTATE->m_PlayerOptions[m_PlayerNumber].m_bReverseGrade )
							m_Combo.SetCombo( g_CurStageStats.iCurMissCombo[m_PlayerNumber], g_CurStageStats.iCurCombo[m_PlayerNumber] );
						else
							m_Combo.SetCombo( g_CurStageStats.iCurCombo[m_PlayerNumber], g_CurStageStats.iCurMissCombo[m_PlayerNumber] );
					}
				}

				if( PREFSMAN->m_bUsePIUHolds )
				{
					TapNoteScore score = TNS_MARVELOUS;

					if( !GAMESTATE->m_PlayerOptions[m_PlayerNumber].m_bSMJudgments )
					{
						const Game* pGame = GAMESTATE->GetCurrentGame();
						if( GAMESTATE->ShowMarvelous(m_PlayerNumber) )
							score = pGame->m_mapMarvelousTo;
						else
							score = pGame->m_mapPerfectTo;
					}

					if( score == TNS_MARVELOUS && !GAMESTATE->ShowMarvelous(m_PlayerNumber) )
						score = TNS_PERFECT;

					bool bBright;

					if( GAMESTATE->m_PlayerController[m_PlayerNumber] == PC_HUMAN )
						bBright = g_CurStageStats.iCurCombo[m_PlayerNumber] >= BRIGHT_GHOST_COMBO_THRESHOLD;
					else
						bBright = GAMESTATE->m_iNotesInAutoPlay[m_PlayerNumber] >= BRIGHT_GHOST_COMBO_THRESHOLD;

					m_pNoteField->DidTapNote( hn.iTrack, score, bBright );
				}
				else
					m_pNoteField->DidTapNote( hn.iTrack, TNS_MARVELOUS, true );	// bright ghost flash
			}
		}

		if( hns != HNS_NONE )
		{
			m_bCrossingHoldNote[hn.iTrack] = false;

			// this note has been judged
			unsigned uComboMultiplier;
			float fScoreMultiplier,
				fLifeMultiplier;

			unsigned uMultiplier = m_Timing->GetMultiplierSegmentNumberAtBeat( NoteRowToBeat( hn.iEndRow ) );

			switch( hns )
			{
			case HNS_NONE:
				uComboMultiplier = 0;
				fScoreMultiplier = 0.f;
				fLifeMultiplier = 0.f;
				break;
			case HNS_OK:
			case RNS_OK:
				uComboMultiplier = m_Timing->m_MultiplierSegments[uMultiplier].m_uHit;
				fScoreMultiplier = m_Timing->m_MultiplierSegments[uMultiplier].m_fScoreHit;
				fLifeMultiplier = m_Timing->m_MultiplierSegments[uMultiplier].m_fLifeHit;
				break;
			case HNS_NG:
			case RNS_NG:
				uComboMultiplier = m_Timing->m_MultiplierSegments[uMultiplier].m_uMiss;
				fScoreMultiplier = m_Timing->m_MultiplierSegments[uMultiplier].m_fScoreMiss;
				fLifeMultiplier = m_Timing->m_MultiplierSegments[uMultiplier].m_fLifeMiss;
				break;
			default:
				ASSERT(0);
				uComboMultiplier = 0;
				fScoreMultiplier = 0.f;
				fLifeMultiplier = 0.f;
			}

			HandleHoldScore( hns, uComboMultiplier, fScoreMultiplier, fLifeMultiplier );
			m_HoldJudgment[hn.iTrack].SetHoldJudgment( hns );

			int ms_error = (hns == HNS_OK || hns == RNS_OK) ? 0 : MAX_PRO_TIMING_ERROR;

			g_CurStageStats.iTotalError[m_PlayerNumber] += ms_error;

			if( hns == HNS_NG ) // don't show a 0 for an OK
				m_ProTimingDisplay.SetJudgment( ms_error, ReverseGrade( TNS_MISS ) );

			m_iLastHold = h;
		}

		m_pNoteField->SetHoldNoteLife(hn, fLife);	// update the NoteField display
		m_pNoteField->SetHoldNoteScore(hn, hns);	// update the NoteField display

		SetHoldNoteLife(hn, fLife);
		SetHoldNoteScore(hn, hns);
	}
}

bool PlayerMinus::UpdateHoldCheckpoints(int iRow, float fStopStart, float fStopSeconds, bool bEarly, bool bLate)
{
	if (!PREFSMAN->m_bUsePIUHolds)
		return false;

	//LOG->Trace( "PlayerMinus::UpdateHoldCheckpoints( %d, %.3f, %.3f, %i, %i )", iRow, fStopStart, fStopSeconds, bEarly, bLate );

	bool bHasLateCheckpoints = false;

	float fCurrentSecond = GAMESTATE->m_fMusicSeconds - PREFSMAN->m_fPadStickSeconds;

	for (int t = 0; t < GetNumTracks(); ++t) {		// for each HoldNote
		if (bEarly) {
			const StyleInput StyleI(m_PlayerNumber, t);
			const GameInput GameI = GAMESTATE->GetCurrentStyle()->StyleInputToGameInput(StyleI);
			if (INPUTMAPPER->IsButtonDown(GameI))
				m_HoldPressed[t] = GAMESTATE->m_fMusicSeconds - PREFSMAN->m_fPadStickSeconds;
		}

		if (this->GetTapNote(t, iRow).checkpoints == 0)
			continue;

		float fReleasedSecs = fCurrentSecond - m_HoldPressed[t];

		if (GAMESTATE->m_PlayerController[m_PlayerNumber] == PC_HUMAN) {
			if (fReleasedSecs >= -ADJUSTED_WINDOW_AFTER(Long) &&
				fReleasedSecs <= ADJUSTED_WINDOW(Long))
				StepCheckpoint(t, iRow);
			else if (bLate)
				StepCheckpoint(t, iRow, false);
			else
				bHasLateCheckpoints = true;
		}
		else
			StepCheckpoint(t, iRow);
	}

	return bHasLateCheckpoints;
}

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * Contains code (c) 2001-2004 Chris Danford.
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
