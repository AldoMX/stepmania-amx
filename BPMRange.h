#ifndef BPM_RANGE_H
#define BPM_RANGE_H

class TimingData;

class BPMRange
{
	float m_fMin, m_fMax;

public:
	BPMRange();
	BPMRange( float fMin, float fMax );
	BPMRange( TimingData* timing );

	void Reset();
	void AddTo( vector<float>& bpms );

	inline const float GetMin() const { return m_fMin; }
	inline const float GetMax() const { return m_fMax; }

	CString GetString() const;
	void FromString( CString& range );

	inline const bool IsConstant() const { return m_fMin == m_fMax; }
	inline const bool IsRandom() const { return m_fMin == FLT_MAX && m_fMax == -FLT_MAX; }
	inline void operator&( const BPMRange& other ) { m_fMin = min(m_fMin, other.m_fMin); m_fMax = max(m_fMax, other.m_fMax); }
	inline void operator&=( const BPMRange& other ) { this->operator&(other); }
};

#endif

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
