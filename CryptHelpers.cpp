#include "global.h"
#include "CryptHelpers.h"

PRNGWrapper::PRNGWrapper( const struct ltc_prng_descriptor *pPRNGDescriptor )
{
	m_iPRNG = register_prng( pPRNGDescriptor );
	ASSERT( m_iPRNG >= 0 );

	int iRet = rng_make_prng( 128, m_iPRNG, &m_PRNG, NULL );
	ASSERT_M( iRet == CRYPT_OK, error_to_string(iRet) );
}

PRNGWrapper::~PRNGWrapper()
{
	if( m_iPRNG != -1 )
		prng_descriptor[m_iPRNG].done( &m_PRNG );
}

void PRNGWrapper::AddEntropy( const void *pData, int iSize )
{
	int iRet = prng_descriptor[m_iPRNG].add_entropy( (const unsigned char *) pData, iSize, &m_PRNG );
	ASSERT_M( iRet == CRYPT_OK, error_to_string(iRet) );

	iRet = prng_descriptor[m_iPRNG].ready( &m_PRNG );
	ASSERT_M( iRet == CRYPT_OK, error_to_string(iRet) );
}

void PRNGWrapper::AddRandomEntropy()
{
	unsigned char buf[256];
	int iRet = rng_get_bytes( buf, sizeof(buf), NULL );
	ASSERT( iRet == sizeof(buf) );

	AddEntropy( buf, sizeof(buf) );
}

RSAKeyWrapper::RSAKeyWrapper()
{
	memset( &m_Key, 0, sizeof(m_Key) );
}

RSAKeyWrapper::~RSAKeyWrapper()
{
	Unload();
}

void RSAKeyWrapper::Unload()
{
	rsa_free( &m_Key );
}

void RSAKeyWrapper::Generate( PRNGWrapper &prng, int iKeyLenBits )
{
	Unload();

	int iRet = rsa_make_key( &prng.m_PRNG, prng.m_iPRNG, iKeyLenBits / 8, 65537, &m_Key );
	ASSERT( iRet == CRYPT_OK );
}

bool RSAKeyWrapper::Load( const CString &sKey, CString &sError )
{
	Unload();

	int iRet = rsa_import( (const unsigned char *) sKey.data(), sKey.size(), &m_Key );
	if( iRet != CRYPT_OK )
	{
		memset( &m_Key, 0, sizeof(m_Key) );
		sError = error_to_string(iRet);
		return false;
	}

	return true;
}

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * Contains code (c) 2001-2004 Chris Danford.
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
