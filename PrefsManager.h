/* PrefsManager - Holds user-chosen preferences that are saved between sessions. */

#ifndef PREFSMANAGER_H
#define PREFSMANAGER_H

#include "GameConstantsAndTypes.h"
#include "PlayerNumber.h"
#include "Difficulty.h"
#include "Grade.h"

#include "ProductInfo.h"

class IPreference;
class IniFile;

class PrefsManager
{
public:
	PrefsManager();
	~PrefsManager();

	void Init();

	// GameOptions (ARE saved between sessions)
#ifdef WITH_CATALOG_XML
	bool	m_bCatalogXML;
#endif

	bool	m_bWindowed;
	int		m_iDisplayWidth;
	int		m_iDisplayHeight;
	int		m_iDisplayColorDepth;
	int		m_iTextureColorDepth;
	int		m_iMovieColorDepth;
	int		m_iMaxTextureResolution;
	int		m_iRefreshRate;
	bool	m_bShowStats;

	RageVector2 m_CurrentDPI;
	RageVector2 m_CurrentPosition;

	bool	m_bShowBanners;
	bool	m_bShowDiscs;
	bool	m_bShowBackgrounds;
	bool	m_bShowPreviews;
	bool	m_bShowCDTitles;

	bool	m_bBannerUsesCacheOnly;
	bool	m_bDiscUsesCacheOnly;
	bool	m_bBackgroundUsesCacheOnly;
	bool	m_bPreviewUsesCacheOnly;

	enum BackgroundModes { BGMODE_OFF, BGMODE_ANIMATIONS, BGMODE_MOVIEVIS, BGMODE_RANDOMMOVIES } m_BackgroundMode;
	int		m_iNumBackgrounds;
	float	m_fBGBrightness;
	bool	m_bHiddenSongs;
	bool	m_bVsync;
	bool	m_bInterlaced;
	bool	m_bPAL;
	bool	m_bDelayedTextureDelete;
	bool	m_bTexturePreload;
	bool	m_bDelayedScreenLoad;
	bool	m_bDelayedModelDelete;
	enum BannerCacheMode { BNCACHE_OFF, BNCACHE_LOW_RES, BNCACHE_FULL };
	BannerCacheMode	m_BannerCache;
	bool	m_bPalettedBannerCache;
	enum BGCacheMode { BGCACHE_OFF, BGCACHE_LOW_RES, BGCACHE_FULL };
	BGCacheMode	m_BackgroundCache;
	bool	m_bPalettedBackgroundCache;
	bool	m_bFastLoad;
	bool	m_bFastLoadExistingCacheOnly;

	// Used for loading off player profiles
	bool	m_bPlayerSongs;
	bool	m_bPlayerSongsAllowBanners;
	bool	m_bPlayerSongsAllowDiscs;
	bool	m_bPlayerSongsAllowBackgrounds;
	bool	m_bPlayerSongsAllowPreviews;
	bool	m_bPlayerSongsAllowCDTitles;
	bool	m_bPlayerSongsAllowLyrics;
	bool	m_bPlayerSongsAllowBGChanges;
	float	m_fPlayerSongsLoadTimeout;
	float	m_fPlayerSongsLengthLimitSeconds;
	int		m_iPlayerSongsLoadLimit;

	// Used for announcer
	bool	m_bPositiveAnnouncerOnly;
	bool	m_bEvalExtraAnnouncer;
	bool	m_bGameExtraAnnouncer;

	// New feature for arcade machines/flaky pads
	float	m_fDebounceTime;

	bool	m_bNonstopUsesExtremeScoring;

	// Use for playing sounds
	bool	m_bPlayAttackSounds;
	bool	m_bPlayMineSound;
	bool	m_bPlayShockSound;
	bool	m_bPlayPotionSound;

	// Used for Attack Mods
	float	m_fRandomAttackLength;
	float	m_fTimeBetweenRandomAttacks;
	float	m_fAttackMinesLength;

	bool	m_bDanceRaveShufflesNotes;

	bool	m_bOnlyDedicatedMenuButtons;
	bool	m_bMenuTimer;
	bool	m_bShowDanger;

	bool	m_bRollTapTimingIsAffectedByJudge;
	float	m_fJudgeWindowScale;
	float	m_fJudgeWindowScaleAfter;
	float	m_fJudgeWindowAdd;		// this is useful for compensating for changes in sampling rate between devices
	float	m_fJudgeWindowAddAfter;		// this is useful for compensating for changes in sampling rate between devices
	float	m_fJudgeWindowSecondsMarvelous;
	float	m_fJudgeWindowSecondsPerfect;
	float	m_fJudgeWindowSecondsGreat;
	float	m_fJudgeWindowSecondsGood;
	float	m_fJudgeWindowSecondsBoo;
	float	m_fJudgeWindowSecondsHidden;
	float	m_fJudgeWindowSecondsHold;
	float	m_fJudgeWindowSecondsRoll;	// time from a hit until a roll drops. - Mark
	float	m_fJudgeWindowSecondsLong;
	float	m_fJudgeWindowSecondsMine;
	float	m_fJudgeWindowSecondsShock;
	float	m_fJudgeWindowSecondsPotion;
	float	m_fJudgeWindowSecondsAttack;
	float	m_fJudgeWindowSecondsAfterMarvelous;
	float	m_fJudgeWindowSecondsAfterPerfect;
	float	m_fJudgeWindowSecondsAfterGreat;
	float	m_fJudgeWindowSecondsAfterGood;
	float	m_fJudgeWindowSecondsAfterBoo;
	float	m_fJudgeWindowSecondsAfterHidden;
	float	m_fJudgeWindowSecondsAfterLong;
	float	m_fJudgeWindowSecondsAfterMine;
	float	m_fJudgeWindowSecondsAfterShock;
	float	m_fJudgeWindowSecondsAfterPotion;
	float	m_fJudgeWindowSecondsAfterAttack;

	int		m_iGaugeDifficultyScale;

	// Gauge Meter, Normal Play
	int		m_iGaugeTotalValue;
	int		m_iGaugeChange[NUM_TAP_NOTE_SCORES];
	int		m_iGaugeFactorChange[NUM_TAP_NOTE_SCORES];
	int		m_iGaugeChangeHold[NUM_HOLD_NOTE_SCORES];
	int		m_iGaugeFactorChangeHold[NUM_HOLD_NOTE_SCORES];

	// Gauge Meter, No Recover
	int		m_iGaugeTotalValueNR;
	int		m_iGaugeChangeNR[NUM_TAP_NOTE_SCORES];
	int		m_iGaugeChangeHoldNR[NUM_HOLD_NOTE_SCORES];
	int		m_iGaugeFactorChangeNR[NUM_TAP_NOTE_SCORES];
	int		m_iGaugeFactorChangeHoldNR[NUM_HOLD_NOTE_SCORES];

	// Gauge Meter, Sudden Death
	int		m_iGaugeTotalValueSD;
	int		m_iGaugeChangeSD[NUM_TAP_NOTE_SCORES];
	int		m_iGaugeChangeHoldSD[NUM_HOLD_NOTE_SCORES];
	int		m_iGaugeFactorChangeSD[NUM_TAP_NOTE_SCORES];
	int		m_iGaugeFactorChangeHoldSD[NUM_HOLD_NOTE_SCORES];

	float	m_fLifeDifficultyScale;

	// Life Meter, Normal Play
	float	m_fLifePercentInitialValue;
	float	m_fLifeDeltaPercentChange[NUM_TAP_NOTE_SCORES];
	float	m_fLifeDeltaPercentChangeHold[NUM_HOLD_NOTE_SCORES];

	// Life Meter, No Recover
	float	m_fLifePercentInitialValueNR;
	float	m_fLifeDeltaPercentChangeNR[NUM_TAP_NOTE_SCORES];
	float	m_fLifeDeltaPercentChangeHoldNR[NUM_HOLD_NOTE_SCORES];

	// Life Meter, Sudden Death
	float	m_fLifePercentInitialValueSD;
	float	m_fLifeDeltaPercentChangeSD[NUM_TAP_NOTE_SCORES];
	float	m_fLifeDeltaPercentChangeHoldSD[NUM_HOLD_NOTE_SCORES];

	// tug meter used in rave
	float	m_fTugMeterPercentChange[NUM_TAP_NOTE_SCORES];
	float	m_fTugMeterPercentChangeHold[NUM_HOLD_NOTE_SCORES];

	// Should we fade the backgrounds on random movies?
	bool	m_bFadeVideoBackgrounds;

	// Whoever added these: Please add a comment saying what they do. -Chris
	int		m_iRegenComboAfterFail;
	int		m_iRegenComboAfterMiss;
	int		m_iMaxRegenComboAfterFail;
	int		m_iMaxRegenComboAfterMiss;
	bool	m_bTwoPlayerRecovery;
	bool	m_bMercifulDrain;	// negative life deltas are scaled by the players life percentage
	bool	m_bMinimum1FullSongInCourses;	// FEoS for 1st song, FailImmediate thereafter

	// percent score (the number that is shown on the screen and saved to memory card)
	int		m_iPercentScoreWeight[NUM_TAP_NOTE_SCORES];
	int		m_iPercentScoreWeightHold[NUM_HOLD_NOTE_SCORES];

	int		m_iGradeWeight[NUM_TAP_NOTE_SCORES];
	int		m_iGradeWeightHold[NUM_HOLD_NOTE_SCORES];

	int		m_iNumGradeTiersUsed;
	float	m_fGradePercent[NUM_GRADE_TIERS];	// the minimum percent necessary achieve a grade
	bool	m_bGradeTier02IsAllPerfects;		// DDR special case. If true, m_fGradePercentTier[GRADE_TIER_2] is ignored
	bool	m_bGradeTier02RequiresNoMiss;		// PIU special case
	bool	m_bGradeTier02RequiresFC;
	bool	m_bGradeTier03RequiresNoMiss;		// PIU special case
	bool	m_bGradeTier03RequiresFC;		// DDR Supernova special case

	// ScoreKeeperRave
	float	m_fSuperMeterPercentChange[NUM_TAP_NOTE_SCORES];
	float	m_fSuperMeterPercentChangeHold[NUM_HOLD_NOTE_SCORES];
	bool	m_bMercifulSuperMeter;	// negative super deltas are scaled by the players life percentage

	bool	m_bAutoPlay;
	bool	m_bDelayedEscape;
	bool	m_bInstructions, m_bShowDontDie, m_bShowSelectGroup;
	bool	m_bShowNative;
	bool	m_bArcadeOptionsNavigation;
	enum MusicWheelUsesSections { NEVER, THEME, ABC_ONLY, ALWAYS } m_MusicWheelUsesSections;
	int		m_iMusicWheelSwitchSpeed;
	bool	m_bEasterEggs;
	int 	m_iMarvelousTiming;
	bool	m_bEventMode;
	bool	m_bEventIgnoreSelectable;
	bool	m_bEventIgnoreUnlock;

	// Options Screens for courses
	bool	m_bCoursePlayerOptions;
	bool	m_bCourseSongOptions;

	bool	m_bDelayedCreditsReconcile;

	bool	m_bLockExtraStageDiff;
	bool	m_bPickExtraStage;
	bool	m_bAlwaysAllowExtraStage2;
	bool	m_bPickModsForExtraStage;
	bool	m_bDarkExtraStage;
	bool	m_bOniExtraStage1;
	bool	m_bOniExtraStage2;

	bool	m_bComboContinuesBetweenSongs;

	enum Maybe { ASK = -1, NO = 0, YES = 1 };
	Maybe	m_ShowSongOptions;
	bool	m_bSoloSingle;
	bool	m_bDancePointsForOni;	//DDR-Extreme style dance points instead of max2 percent
	bool	m_bPercentageScoring;
	float	m_fMinPercentageForMachineSongHighScore;
	float	m_fMinPercentageForMachineCourseHighScore;
	bool	m_bDisqualification;
	bool	m_bShowLyrics;
	bool	m_bAutogenSteps;
	bool	m_bAutogenGroupCourses;
	bool	m_bBreakComboToGetItem;
	bool	m_bLockCourseDifficulties;
	enum CharacterOption { CO_OFF = 0, CO_RANDOM = 1, CO_SELECT = 2};
	CharacterOption	m_ShowDancingCharacters;

	bool	m_bUseUnlockSystem;
	bool	m_bUnlockUsesMachineProfileStats;
	bool	m_bUnlockUsesPlayerProfileStats;

	bool	m_bFirstRun;
	bool	m_bAutoMapOnJoyChange;
	float	m_fGlobalOffsetSeconds;
	int		m_iProgressiveLifebar;
	int		m_iProgressiveStageLifebar;
	int		m_iProgressiveNonstopLifebar;
	bool	m_bShowBeginnerHelper;
	bool	m_bEndlessBreakEnabled;
	int		m_iEndlessNumStagesUntilBreak;
	int		m_iEndlessBreakLength;
	bool	m_bDisableScreenSaver;
	CString	m_sLanguage;
	CString	m_sMemoryCardProfileSubdir;	// the directory on a memory card to look in for a profile
	int		m_iProductID;			// Saved in HighScore to track what software version a score came from.
	CString	m_sDefaultLocalProfileID[NUM_PLAYERS];
	bool	m_bMemoryCards;
	CString	m_sMemoryCardOsMountPoint[NUM_PLAYERS];	// if set, always use the device that mounts to this point
	int		m_iMemoryCardUsbBus[NUM_PLAYERS];	// look for this bus when assigning cards.  -1 = match any
	int		m_iMemoryCardUsbPort[NUM_PLAYERS];	// look for this port when assigning cards.  -1 = match any
	int		m_iMemoryCardUsbLevel[NUM_PLAYERS];	// look for this level when assigning cards.  -1 = match any
	int		m_iCenterImageTranslateX;
	int		m_iCenterImageTranslateY;
	float	m_fCenterImageScaleX;
	float	m_fCenterImageScaleY;
	int		m_iAttractSoundFrequency;	// 0 = never, 1 = every time
	bool	m_bAllowExtraStage;
	bool	m_bHideDefaultNoteSkin;
	int		m_iMaxHighScoresPerListForMachine;
	int		m_iMaxHighScoresPerListForPlayer;
	bool	m_bCelShadeModels;

	// Number of seconds it takes for a button on the controller to release
	// after pressed.
	float	m_fPadStickSeconds;

	// Useful for non 4:3 displays and resolutions < 640x480 where texels don't
	// map directly to pixels.
	bool	m_bForceMipMaps;
	bool	m_bTrilinearFiltering;		// has no effect without mipmaps on
	bool	m_bAnisotropicFiltering;	// has no effect without mipmaps on.  Not mutually exclusive with trilinear.

	// If true, then signatures created when writing profile data
	// and verified when reading profile data.  Leave this false if
	// you want to use a profile on different machines that don't
	// have the same key, or else the profile's data will be discarded.
	bool	m_bSignProfileData;

	/* Editor prefs: */
	bool	m_bEditorShowBGChangesPlay;
	bool	m_bEditShiftSelector;

	// course ranking
	enum CourseSortOrders { COURSE_SORT_SONGS, COURSE_SORT_METER, COURSE_SORT_METER_SUM, COURSE_SORT_RANK } m_iCourseSortOrder;
	bool	m_bMoveRandomToEnd;
	bool	m_bSubSortByNumSteps;
	enum GetRankingName { RANKING_OFF, RANKING_ON, RANKING_LIST } m_iGetRankingName;

	// scoring type; SCORING_MAX2 should always be first
	enum ScoringTypes { SCORING_MAX2 = 0, SCORING_5TH, SCORING_NOVA, SCORING_NOVA2, SCORING_HYBRID, SCORING_PIU_NX2, SCORING_PIU_ZERO, SCORING_PIU_PREX3, SCORING_PIU_EXTRA, SCORING_CUSTOM = -1 } m_iScoringType;

	/* 0 = no; 1 = yes; -1 = auto (do whatever is appropriate for the arch). */
	int		m_iBoostAppPriority;

	CString	m_sAdditionalSongFolders;
	CString	m_sAdditionalFolders;

	CString	m_sLastSeenInputDevices;
#if defined(WIN32)
	unsigned	m_uLastSeenMemory;
#endif
#if defined(HAVE_VERSION_INFO)
	CString m_sLastCommit;
#endif
	bool	m_bSmoothLines;
private:
	CString	m_sSoundDrivers;
public:
	int		m_iSoundWriteAhead;
	CString	m_iSoundDevice;
	float	m_fSoundVolume;
	int		m_iSoundResampleQuality;
	CString	m_sMovieDrivers;

//CHANGE: Include LightsFalloffSeconds, LightsFalloffUsesBPM and LightsIgnoreHolds - Mark
	CString	m_sLightsDriver;
	CString	m_sLightsStepsDifficulty;
	float	m_fLightsFalloffSeconds;
	bool	m_bLightsFalloffUsesBPM;
	int		m_iLightsIgnoreHolds;
	bool	m_bBlinkGameplayButtonLightsOnNote;

	bool	m_bThreadedInput;
	bool	m_bThreadedMovieDecode;
	CString	m_sMachineName;

	CString	m_sIgnoredMessageWindows;

	CString	m_sCoursesToShowRanking;

	/* Debug: */
	bool	m_bLogToDisk;
	bool	m_bForceLogFlush;
	bool	m_bShowLogOutput;
	bool	m_bTimestamping;
	bool	m_bLogSkips;
	bool	m_bLogCheckpoints;
	bool	m_bShowLoadingWindow;

	// StepMania AMX
	bool	m_bChangeSpeedWithNumKeys;
	bool	m_bStoreSpeedWithNumKeys;
	bool	m_bAutoPlayCombo;
	bool	m_bStartRandomBGAsWithMusic;
	bool	m_bEndRandomBGAsWithMusic;
	bool	m_bStageFinishesWithMusic;
	bool	m_bMusicRateAffectsBGChanges;

	// Gameplay
	bool	m_bMissComboIncreasesOnComboBreak;
	bool	m_bComboBreakOnGood;
	bool	m_bComboBreakOnMines;
	bool	m_bComboBreakOnShocks;
	bool	m_bComboIncreasesOnPotions;
	bool	m_bComboIncreasesOnRollHits;
	bool	m_bComboIncreasesOnHiddens;
	bool	m_bHoldOKIncreasesCombo;
	bool	m_bHoldNGBreaksCombo;
	bool	m_bUsePIUHolds;

	// Editor
	bool	m_bShowAllStyles;
	bool	m_bLastEditedSong;
	bool	m_bLastEditedDifficulty;
	bool	m_bEditorSmoothScrolling;
	bool	m_bGameplayKeysInEditor;
	bool	m_bEditorCombo;
	bool	m_bShowSnapBeats;
	bool	m_bSelectLastSecond;
	bool	m_bAutoSaveBeforePlay;
	float	m_fAutoSaveEverySeconds;

	bool	m_bTimingAsRows;

	// Performance
	bool		m_bGameLoopDelay;
	unsigned	m_uGameLoopDelayMilliseconds;

	// Stage Length
	int	m_iNumArcadeStages;
	int	m_iMaxSongsToPlay;
	int	m_iStageLengths;
	struct StageLength
	{
		int		m_iStagesPerSong;
		bool	m_bUseSongWithLength;
		float	m_fMusicLengthSeconds;
	};
	int		m_iDefaultStagesPerSong;
	vector<StageLength>	m_StageLength;
	bool	m_bProgressiveLifebarBySong;

	// Extra Stages
	int		m_iSkipSongsHarderThanOMES;
	bool	m_bSetPlayerOptionsES;
	bool	m_bSetPlayerOptionsOMES;
	bool	m_bSetSongOptionsES;
	bool	m_bSetSongOptionsOMES;

	bool	m_bCheckDifficultyES;
	bool	m_bCheckDifficultyOMES;
	bool	m_bCheckDifficultyAllStagesES;
	bool	m_bCheckDifficultyAllStagesOMES;
	Difficulty	m_MinDifficultyES;
	Difficulty	m_MinDifficultyOMES;
	Difficulty	m_MaxDifficultyES;
	Difficulty	m_MaxDifficultyOMES;

	bool	m_bCheckGradeES;
	bool	m_bCheckGradeOMES;
	bool	m_bCheckGradeAllStagesES;
	bool	m_bCheckGradeAllStagesOMES;
	Grade	m_MinGradeES;
	Grade	m_MinGradeOMES;

	bool	m_bCheckSongsPlayedES;
	bool	m_bCheckSongsPlayedOMES;
	int		m_iMinSongsPlayedES;
	int		m_iMinSongsPlayedOMES;
	int		m_iMaxSongsPlayedES;
	int		m_iMaxSongsPlayedOMES;

	bool	m_bAllowExtraStage2;
	bool	m_bLockExtraStage2Diff;
	bool	m_bPickExtraStage2;
	bool	m_bPickModsForExtraStage2;
	bool	m_bDarkExtraStage2;

	bool	m_bExtraStageForcePreferredGroup;
	bool	m_bExtraStage2ForcePreferredGroup;

	bool	m_bJukeboxNonMenuButtons;
	bool	m_bJukeboxGameplay;
	bool	m_bRandomModifiersReadsRandomAttacks;
	bool	m_bHighScoreSavesBonusScore;

	// ScreenGameplay
	Maybe	m_ApplyOffsetToEveryDifficulty;
	float	m_fKeyF9BPMValue;
	float	m_fKeyF10BPMValue;
	float	m_fKeyAltBPMDivisor;
	float	m_fSlowRepeatBPMMultiplier;
	float	m_fFastRepeatBPMMultiplier;
	float	m_fKeyF11OffsetValue;
	float	m_fKeyF12OffsetValue;
	float	m_fKeyAltOffsetDivisor;
	float	m_fSlowRepeatOffsetMultiplier;
	float	m_fFastRepeatOffsetMultiplier;

	bool	m_bLoadThemeSplash;
	CString	m_sLastLoadedSplash;

	// StepMania AMX's Extras
	int		m_iTouchMode;
	bool	m_bDisableSpecialKeys;
	bool	m_bPersistentInput;
	bool	m_bPIUIO;

	// Renderers
	bool	m_bEnableOpenGLRenderer;
	bool	m_bEnableDirect3DRenderer;
	bool	m_bEnableNullRenderer;

	// Updates
	unsigned	m_uUpdateCheckInterval;
	unsigned	m_uLastUpdateCheck;

	CString	m_sLastEditedSong;
	CString	m_sLastEditedDifficulty;

	// Game-specific prefs
	CString	m_sDefaultModifiers;

	// wrappers
	CString GetSoundDrivers();

	void ReadGlobalPrefsFromDisk();
	void SaveGlobalPrefsToDisk() const;

	void ResetToFactoryDefaults( bool bInit = true );

#if defined( WITH_COIN_MODE )
	//
	// Coin Mode
	//
	int m_iCoinsPerCredit;
	enum CoinMode { COIN_HOME, COIN_FREE, COIN_PAY } m_CoinMode;
	CoinMode GetCoinMode();
	enum Premium { NO_PREMIUM, DOUBLES_PREMIUM, JOINT_PREMIUM } m_Premium;
	Premium	GetPremium();
	const CString& CoinModeToString( CoinMode cm );
#endif

protected:
	void ReadPrefsFromFile( CString sIni );
};


//
// For self-registering prefs
//
void Subscribe( IPreference *p );


/* This is global, because it can be accessed by crash handlers and error handlers
 * that are run after PREFSMAN shuts down (and probably don't want to deref tht
 * pointer anyway). */
extern bool				g_bAutoRestart;
extern const unsigned	DEFAULT_GAMELOOP_DELAY;
extern const unsigned	DEFAULT_GAMELOOP_DELAY_HIGH_PERFORMANCE;

extern PrefsManager*	PREFSMAN;	// global and accessable from anywhere in our program

#endif

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * Contains code (c) 2001-2004 Chris Danford, Chris Gomez.
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
