#include "global.h"
#include "LifeMeterGauge.h"
#include "GameState.h"
#include "ActorUtil.h"
#include "ThemeManager.h"
#include "PrefsManager.h"
#include "RageDisplay.h"

static CachedThemeMetricF	BAR_START_X			("LifeMeterGauge", "BarStartX");
static CachedThemeMetricF	BAR_END_X			("LifeMeterGauge", "BarEndX");
static CachedThemeMetricF	BAR_STEP_DIST		("LifeMeterGauge", "BarStepDistance");
static CachedThemeMetricF	BAR_BOUNCE_DIST		("LifeMeterGauge", "BarBounceDistance");
static CachedThemeMetricF	BAR_BOUNCE_BEAT		("LifeMeterGauge", "BarBounceSpeedBeats");
static CachedThemeMetricF	BAR_BOUNCE_INVERSE	("LifeMeterGauge", "BarBounceInverse");
static CachedThemeMetricF	BAR_SKEW_X			("LifeMeterGauge", "BarSkewX");
static CachedThemeMetricF	BAR_SKEW_Y			("LifeMeterGauge", "BarSkewY");

static CachedThemeMetricB	USE_SECONDARY_BAR				("LifeMeterGauge", "UseSecondaryBar");
static CachedThemeMetricF	SECONDARY_BAR_START_X			("LifeMeterGauge", "SecondaryBarStartX");
static CachedThemeMetricF	SECONDARY_BAR_END_X				("LifeMeterGauge", "SecondaryBarEndX");
static CachedThemeMetricF	SECONDARY_BAR_STEP_DIST			("LifeMeterGauge", "SecondaryBarStepDistance");
static CachedThemeMetricF	SECONDARY_BAR_BOUNCE_DIST		("LifeMeterGauge", "SecondaryBarBounceDistance");
static CachedThemeMetricF	SECONDARY_BAR_BOUNCE_BEAT		("LifeMeterGauge", "SecondaryBarBounceSpeedBeats");
static CachedThemeMetricF	SECONDARY_BAR_BOUNCE_INVERSE	("LifeMeterGauge", "SecondaryBarBounceInverse");
static CachedThemeMetricF	SECONDARY_BAR_SKEW_X			("LifeMeterGauge", "SecondaryBarSkewX");
static CachedThemeMetricF	SECONDARY_BAR_SKEW_Y			("LifeMeterGauge", "SecondaryBarSkewY");

static CachedThemeMetricF	DANGER_THRESHOLD	("LifeMeterGauge", "DangerThreshold");
static CachedThemeMetricF	HOT_THRESHOLD		("LifeMeterGauge", "HotThreshold");
static CachedThemeMetricB	SHOW_INACTIVE		("LifeMeterGauge", "ShowInactivePlayers");

static CachedThemeMetric	ANIMATION_TIMING	("LifeMeterGauge", "AnimationTiming");
static CachedThemeMetricB	BGA_TIMING			("LifeMeterGauge", "UseBGATimingForAnimation");

LifeMeterGauge::LifeMeterGauge( const CString& name ) : LifeMeter("LifeMeterGauge" + name)
{
}

void LifeMeterGauge::Load( PlayerNumber pn )
{
	LifeMeter::Load(pn);

	BAR_START_X.Refresh(m_sName);
	BAR_END_X.Refresh(m_sName);
	BAR_STEP_DIST.Refresh(m_sName);
	BAR_BOUNCE_DIST.Refresh(m_sName);
	BAR_BOUNCE_BEAT.Refresh(m_sName);
	BAR_BOUNCE_INVERSE.Refresh(m_sName);
	BAR_SKEW_X.Refresh(m_sName);
	BAR_SKEW_Y.Refresh(m_sName);

	USE_SECONDARY_BAR.Refresh(m_sName);
	if (USE_SECONDARY_BAR) {
		SECONDARY_BAR_START_X.Refresh(m_sName);
		SECONDARY_BAR_END_X.Refresh(m_sName);
		SECONDARY_BAR_STEP_DIST.Refresh(m_sName);
		SECONDARY_BAR_BOUNCE_DIST.Refresh(m_sName);
		SECONDARY_BAR_BOUNCE_BEAT.Refresh(m_sName);
		SECONDARY_BAR_BOUNCE_INVERSE.Refresh(m_sName);
		SECONDARY_BAR_SKEW_X.Refresh(m_sName);
		SECONDARY_BAR_SKEW_Y.Refresh(m_sName);
	}

	DANGER_THRESHOLD.Refresh(m_sName);
	HOT_THRESHOLD.Refresh(m_sName);
	SHOW_INACTIVE.Refresh(m_sName);

	ANIMATION_TIMING.Refresh(m_sName);
	m_animationTiming = StringToAnimationTiming(ANIMATION_TIMING);
	if (m_animationTiming == AT_INVALID) {
		BGA_TIMING.Refresh(m_sName);
		m_animationTiming = BGA_TIMING ? AT_BGA : AT_STEP;
	}

	m_CurrentState = GAUGE_STATE_NORMAL;
	bool bEnabled = GAMESTATE->IsPlayerEnabled(pn);

	if( bEnabled || SHOW_INACTIVE )
	{
		// Frame (Any)
		m_sprFrame[GAUGE_STATE_NORMAL].SetName("Frame");
		m_sprFrame[GAUGE_STATE_NORMAL].Load( THEME->GetPathG(m_sName, "frame normal") );
		SET_XY_AND_ON_COMMAND( m_sprFrame[GAUGE_STATE_NORMAL] );
		this->AddChild( &m_sprFrame[GAUGE_STATE_NORMAL] );
	}

	if( !bEnabled )
		return;

	// Frame (Enabled)
	m_sprFrame[GAUGE_STATE_HOT].SetName("Frame");
	m_sprFrame[GAUGE_STATE_HOT].Load( THEME->GetPathG(m_sName, "frame hot") );
	SET_XY_AND_ON_COMMAND( m_sprFrame[GAUGE_STATE_HOT] );
	this->AddChild( &m_sprFrame[GAUGE_STATE_HOT] );

	m_sprFrame[GAUGE_STATE_DANGER].SetName("Frame");
	m_sprFrame[GAUGE_STATE_DANGER].Load( THEME->GetPathG(m_sName, "frame danger") );
	SET_XY_AND_ON_COMMAND( m_sprFrame[GAUGE_STATE_DANGER] );
	this->AddChild( &m_sprFrame[GAUGE_STATE_DANGER] );

	// Bar Mask (Quad)
	m_quadMainBarMask.SetDiffuse( RageColor(0,0,0,1) );
	m_quadMainBarMask.SetBlendMode( BLEND_NO_EFFECT );
	m_quadMainBarMask.SetZWrite( true );
	this->AddChild( &m_quadMainBarMask );

	// Bar
	m_sprMainBar[GAUGE_STATE_NORMAL].SetName("Bar");
	m_sprMainBar[GAUGE_STATE_NORMAL].Load( THEME->GetPathG(m_sName, "bar normal") );
	m_sprMainBar[GAUGE_STATE_NORMAL].SetZTestMode( ZTEST_WRITE_ON_PASS );
	SET_XY_AND_ON_COMMAND( m_sprMainBar[GAUGE_STATE_NORMAL] );
	this->AddChild( &m_sprMainBar[GAUGE_STATE_NORMAL] );

	m_sprMainBar[GAUGE_STATE_HOT].SetName("Bar");
	m_sprMainBar[GAUGE_STATE_HOT].Load( THEME->GetPathG(m_sName, "bar hot") );
	m_sprMainBar[GAUGE_STATE_HOT].SetZTestMode( ZTEST_WRITE_ON_PASS );
	SET_XY_AND_ON_COMMAND( m_sprMainBar[GAUGE_STATE_HOT] );
	this->AddChild( &m_sprMainBar[GAUGE_STATE_HOT] );

	m_sprMainBar[GAUGE_STATE_DANGER].SetName("Bar");
	m_sprMainBar[GAUGE_STATE_DANGER].Load( THEME->GetPathG(m_sName, "bar danger") );
	m_sprMainBar[GAUGE_STATE_DANGER].SetZTestMode( ZTEST_WRITE_ON_PASS );
	SET_XY_AND_ON_COMMAND( m_sprMainBar[GAUGE_STATE_DANGER] );
	this->AddChild( &m_sprMainBar[GAUGE_STATE_DANGER] );

	if (USE_SECONDARY_BAR) {
		// Secondary Bar Mask (Quad)
		m_quadSecondaryBarMask.SetDiffuse( RageColor(0,0,0,1) );
		m_quadSecondaryBarMask.SetBlendMode( BLEND_NO_EFFECT );
		m_quadSecondaryBarMask.SetZWrite( true );
		this->AddChild( &m_quadSecondaryBarMask );

		// Secondary Bar
		m_sprSecondaryBar[GAUGE_STATE_NORMAL].SetName("SecondaryBar");
		m_sprSecondaryBar[GAUGE_STATE_NORMAL].Load( THEME->GetPathG(m_sName, "secondary bar normal") );
		m_sprSecondaryBar[GAUGE_STATE_NORMAL].SetZTestMode( ZTEST_WRITE_ON_PASS );
		SET_XY_AND_ON_COMMAND( m_sprSecondaryBar[GAUGE_STATE_NORMAL] );
		this->AddChild( &m_sprSecondaryBar[GAUGE_STATE_NORMAL] );

		m_sprSecondaryBar[GAUGE_STATE_HOT].SetName("SecondaryBar");
		m_sprSecondaryBar[GAUGE_STATE_HOT].Load( THEME->GetPathG(m_sName, "secondary bar hot") );
		m_sprSecondaryBar[GAUGE_STATE_HOT].SetZTestMode( ZTEST_WRITE_ON_PASS );
		SET_XY_AND_ON_COMMAND( m_sprSecondaryBar[GAUGE_STATE_HOT] );
		this->AddChild( &m_sprSecondaryBar[GAUGE_STATE_HOT] );

		m_sprSecondaryBar[GAUGE_STATE_DANGER].SetName("SecondaryBar");
		m_sprSecondaryBar[GAUGE_STATE_DANGER].Load( THEME->GetPathG(m_sName, "secondary bar danger") );
		m_sprSecondaryBar[GAUGE_STATE_DANGER].SetZTestMode( ZTEST_WRITE_ON_PASS );
		SET_XY_AND_ON_COMMAND( m_sprSecondaryBar[GAUGE_STATE_DANGER] );
		this->AddChild( &m_sprSecondaryBar[GAUGE_STATE_DANGER] );
	}

	// Indicator
	m_sprIndicator[GAUGE_STATE_NORMAL].SetName("Indicator");
	m_sprIndicator[GAUGE_STATE_NORMAL].Load( THEME->GetPathG(m_sName, "indicator normal") );
	SET_XY_AND_ON_COMMAND( m_sprIndicator[GAUGE_STATE_NORMAL] );
	this->AddChild( &m_sprIndicator[GAUGE_STATE_NORMAL] );

	m_sprIndicator[GAUGE_STATE_HOT].SetName("Indicator");
	m_sprIndicator[GAUGE_STATE_HOT].Load( THEME->GetPathG(m_sName, "indicator hot") );
	SET_XY_AND_ON_COMMAND( m_sprIndicator[GAUGE_STATE_HOT] );
	this->AddChild( &m_sprIndicator[GAUGE_STATE_HOT] );

	m_sprIndicator[GAUGE_STATE_DANGER].SetName("Indicator");
	m_sprIndicator[GAUGE_STATE_DANGER].Load( THEME->GetPathG(m_sName, "indicator danger") );
	SET_XY_AND_ON_COMMAND( m_sprIndicator[GAUGE_STATE_DANGER] );
	this->AddChild( &m_sprIndicator[GAUGE_STATE_DANGER] );

	// Overlay
	m_sprOverlay[GAUGE_STATE_NORMAL].SetName("Overlay");
	m_sprOverlay[GAUGE_STATE_NORMAL].Load( THEME->GetPathG(m_sName, "overlay normal") );
	SET_XY_AND_ON_COMMAND( m_sprOverlay[GAUGE_STATE_NORMAL] );
	this->AddChild( &m_sprOverlay[GAUGE_STATE_NORMAL] );

	m_sprOverlay[GAUGE_STATE_HOT].SetName("Overlay");
	m_sprOverlay[GAUGE_STATE_HOT].Load( THEME->GetPathG(m_sName, "overlay hot") );
	SET_XY_AND_ON_COMMAND( m_sprOverlay[GAUGE_STATE_HOT] );
	this->AddChild( &m_sprOverlay[GAUGE_STATE_HOT] );

	m_sprOverlay[GAUGE_STATE_DANGER].SetName("Overlay");
	m_sprOverlay[GAUGE_STATE_DANGER].Load( THEME->GetPathG(m_sName, "overlay danger") );
	SET_XY_AND_ON_COMMAND( m_sprOverlay[GAUGE_STATE_DANGER] );
	this->AddChild( &m_sprOverlay[GAUGE_STATE_DANGER] );

	// Bar Mask (Rect / Matrix)
	m_rectMainBarMask.left = (BAR_START_X - m_sprIndicator[m_CurrentState].GetUnzoomedWidth() / 2.f) * m_sprMainBar[m_CurrentState].GetZoomX();
	m_rectMainBarMask.top = -m_sprMainBar[m_CurrentState].GetZoomedHeight() / 2.f;
	m_rectMainBarMask.right = (BAR_END_X + m_sprIndicator[m_CurrentState].GetUnzoomedWidth() / 2.f) * m_sprMainBar[m_CurrentState].GetZoomX();
	m_rectMainBarMask.bottom = m_sprMainBar[m_CurrentState].GetZoomedHeight() / 2.f;

	m_matrixMainBarMask = RageMatrix(
		1.f, BAR_SKEW_Y, 0.f, 0.f,
		BAR_SKEW_X, 1.f, 0.f, 0.f,
		0.f, 0.f, 1.f, 0.f,
		0.f, 0.f, 0.f, 1.f
	);

	if (USE_SECONDARY_BAR)
	{
		// Secondary Bar Mask (Rect / Matrix)
		m_rectSecondaryBarMask.left = (SECONDARY_BAR_START_X - m_sprIndicator[m_CurrentState].GetUnzoomedWidth() / 2.f) * m_sprSecondaryBar[m_CurrentState].GetZoomX();
		m_rectSecondaryBarMask.top = -m_sprSecondaryBar[m_CurrentState].GetZoomedHeight() / 2.f;
		m_rectSecondaryBarMask.right = (SECONDARY_BAR_END_X + m_sprIndicator[m_CurrentState].GetUnzoomedWidth() / 2.f) * m_sprSecondaryBar[m_CurrentState].GetZoomX();
		m_rectSecondaryBarMask.bottom = m_sprSecondaryBar[m_CurrentState].GetZoomedHeight() / 2.f;

		m_matrixSecondaryBarMask = RageMatrix(
			1.f, SECONDARY_BAR_SKEW_Y, 0.f, 0.f,
			SECONDARY_BAR_SKEW_X, 1.f, 0.f, 0.f,
			0.f, 0.f, 1.f, 0.f,
			0.f, 0.f, 0.f, 1.f
		);
	}

	// Drain Values
	switch( GAMESTATE->m_SongOptions.m_DrainType )
	{
	case SongOptions::DRAIN_NORMAL:
		m_ChangeValues.Total		= &PREFSMAN->m_iGaugeTotalValue;
		m_ChangeValues.Gauge		= &PREFSMAN->m_iGaugeChange[TNS_NONE];
		m_ChangeValues.GaugeHold	= &PREFSMAN->m_iGaugeChangeHold[HNS_NONE];
		m_ChangeValues.Factor		= &PREFSMAN->m_iGaugeFactorChange[TNS_NONE];
		m_ChangeValues.FactorHold	= &PREFSMAN->m_iGaugeFactorChangeHold[HNS_NONE];
		break;

	case SongOptions::DRAIN_NO_RECOVER:
		m_ChangeValues.Total		= &PREFSMAN->m_iGaugeTotalValueNR;
		m_ChangeValues.Gauge		= &PREFSMAN->m_iGaugeChangeNR[TNS_NONE];
		m_ChangeValues.GaugeHold	= &PREFSMAN->m_iGaugeChangeHoldNR[HNS_NONE];
		m_ChangeValues.Factor		= &PREFSMAN->m_iGaugeFactorChangeNR[TNS_NONE];
		m_ChangeValues.FactorHold	= &PREFSMAN->m_iGaugeFactorChangeHoldNR[HNS_NONE];
		break;

	case SongOptions::DRAIN_SUDDEN_DEATH:
		m_ChangeValues.Total		= &PREFSMAN->m_iGaugeTotalValueSD;
		m_ChangeValues.Gauge		= &PREFSMAN->m_iGaugeChangeSD[TNS_NONE];
		m_ChangeValues.GaugeHold	= &PREFSMAN->m_iGaugeChangeHoldSD[HNS_NONE];
		m_ChangeValues.Factor		= &PREFSMAN->m_iGaugeFactorChangeSD[TNS_NONE];
		m_ChangeValues.FactorHold	= &PREFSMAN->m_iGaugeFactorChangeHoldSD[HNS_NONE];
		break;

	default:
		ASSERT(0);
	}

	m_iLife = *m_ChangeValues.Total / 2;
	m_fIndicatorX = 0;

	m_iFactorMin = max(PREFSMAN->m_iGaugeDifficultyScale - 800, 0);
	m_iFactorMax = PREFSMAN->m_iGaugeDifficultyScale;
	m_iFactor = max(m_iFactorMin + m_iFactorMax - 700, 0);

	m_iDangerThreshold = int(*m_ChangeValues.Total * DANGER_THRESHOLD + 0.5f);
	m_iHotThreshold = int(*m_ChangeValues.Total * HOT_THRESHOLD + 0.5f);
}

void LifeMeterGauge::Update( float fDeltaTime )
{
	if( !GAMESTATE->IsPlayerEnabled(m_PlayerNumber) ) {
		if( SHOW_INACTIVE )
			m_sprFrame[m_CurrentState].Update(fDeltaTime);

		return;
	}

	m_fIndicatorX = BAR_START_X + froundf((BAR_END_X - BAR_START_X) * GetLife(), BAR_STEP_DIST);

	if( m_iLife >= m_iHotThreshold )
		m_rectMainBarMask.left = m_rectMainBarMask.right;
	else {
		float fPercentBounce = 0.f;
		switch (m_animationTiming)
		{
		case AT_STEP:		fPercentBounce = GAMESTATE->m_fPlayerBeat[m_PlayerNumber];	break;
		case AT_BGA:		fPercentBounce = GAMESTATE->m_fSongBeat;					break;
		case AT_SECONDS:	fPercentBounce = GAMESTATE->m_fMusicSeconds;				break;
		default:
			ASSERT(0);
		}
		wrap(fPercentBounce, BAR_BOUNCE_BEAT);
		fPercentBounce /= BAR_BOUNCE_BEAT;
		if( BAR_BOUNCE_INVERSE ) {
			fPercentBounce = 1.f - fPercentBounce;
		}

		m_rectMainBarMask.left = (m_fIndicatorX - froundf(BAR_BOUNCE_DIST * fPercentBounce, BAR_STEP_DIST)) * m_sprMainBar[m_CullMode].GetZoomX();
		m_rectMainBarMask.left -= BAR_STEP_DIST - m_sprIndicator[m_CurrentState].GetZoomedWidth() / 2.f;
	}

	m_quadMainBarMask.StretchTo(m_rectMainBarMask);

	if( USE_SECONDARY_BAR ) {
		if( m_iLife >= m_iHotThreshold )
			m_rectSecondaryBarMask.left = m_rectSecondaryBarMask.right;
		else
		{
			float fPercentBounce = 0.f;
			switch (m_animationTiming)
			{
			case AT_STEP:		fPercentBounce = GAMESTATE->m_fPlayerBeat[m_PlayerNumber];	break;
			case AT_BGA:		fPercentBounce = GAMESTATE->m_fSongBeat;					break;
			case AT_SECONDS:	fPercentBounce = GAMESTATE->m_fMusicSeconds;				break;
			default:
				ASSERT(0);
			}
			wrap(fPercentBounce, SECONDARY_BAR_BOUNCE_BEAT);
			fPercentBounce /= SECONDARY_BAR_BOUNCE_BEAT;
			if( SECONDARY_BAR_BOUNCE_INVERSE ) {
				fPercentBounce = 1.f - fPercentBounce;
			}

			m_rectSecondaryBarMask.left = (m_fIndicatorX - froundf(SECONDARY_BAR_BOUNCE_DIST * fPercentBounce, SECONDARY_BAR_STEP_DIST)) * m_sprSecondaryBar[m_CullMode].GetZoomX();
			m_rectSecondaryBarMask.left -= SECONDARY_BAR_STEP_DIST - m_sprIndicator[m_CurrentState].GetZoomedWidth() / 2.f;
		}

		m_quadSecondaryBarMask.StretchTo(m_rectSecondaryBarMask);
	}

	LifeMeter::Update(fDeltaTime);
}

void LifeMeterGauge::DrawPrimitives()
{
	bool bEnabled = GAMESTATE->IsPlayerEnabled(m_PlayerNumber);

	if( bEnabled || SHOW_INACTIVE )
		m_sprFrame[m_CurrentState].Draw();

	if( !bEnabled )
		return;

	if( USE_SECONDARY_BAR ) {
		// Skew Secondary Bar
		DISPLAY->PushMatrix();
		DISPLAY->PreMultMatrix(m_matrixSecondaryBarMask);
		m_quadSecondaryBarMask.Draw();
		DISPLAY->PopMatrix();

		m_sprSecondaryBar[m_CurrentState].Draw();
	}

	// Skew Bar
	DISPLAY->PushMatrix();
	DISPLAY->PreMultMatrix( m_matrixMainBarMask );
	m_quadMainBarMask.Draw();
	DISPLAY->PopMatrix();

	m_sprMainBar[m_CurrentState].Draw();

	m_sprIndicator[m_CurrentState].SetX( m_fIndicatorX * m_sprIndicator[m_CurrentState].GetZoomX() );
	m_sprIndicator[m_CurrentState].Draw();

	m_sprOverlay[m_CurrentState].Draw();

	//LifeMeter::DrawPrimitives();
}

void LifeMeterGauge::OnSongEnded()
{
	for( vector<Actor*>::iterator it=m_SubActors.begin(); it!=m_SubActors.end(); it++ )
		OFF_COMMAND( *it );
}

void LifeMeterGauge::ChangeLife( TapNoteScore score, const float fMultiplier )
{
	switch( score )
	{
	case TNS_HIT_MINE:
	case TNS_HIT_SHOCK:
	case TNS_MISS_CHECKPOINT:
	case TNS_MISS:
		m_iLife += m_ChangeValues.Gauge[score] * m_iLife / (2 * *m_ChangeValues.Total);
		m_iLife -= m_ChangeValues.Gauge[GAMESTATE->ShowMarvelous(m_PlayerNumber) ? TNS_MARVELOUS : TNS_PERFECT];
		break;

	case TNS_BOO:
		m_iLife += m_ChangeValues.Gauge[score];
		break;

	case TNS_HIT_POTION:
	case TNS_CHECKPOINT:
	case TNS_MISS_HIDDEN:
	case TNS_HIDDEN:
	case TNS_GOOD:
	case TNS_GREAT:
	case TNS_PERFECT:
	case TNS_MARVELOUS:
		m_iLife += m_ChangeValues.Gauge[score] * m_iFactor / *m_ChangeValues.Total;
	}
	m_iFactor += m_ChangeValues.Factor[score];

	CLAMP(m_iLife, 0, *m_ChangeValues.Total);
	CLAMP(m_iFactor, m_iFactorMin, m_iFactorMax);

	if( m_iLife <= m_iDangerThreshold )
		m_CurrentState = GAUGE_STATE_DANGER;
	else if( m_iLife >= m_iHotThreshold )
		m_CurrentState = GAUGE_STATE_HOT;
	else
		m_CurrentState = GAUGE_STATE_NORMAL;
}

void LifeMeterGauge::ChangeLife( HoldNoteScore score, const float fMultiplier )
{
	switch( score )
	{
	case HNS_NG:
	case RNS_NG:
		m_iLife += m_ChangeValues.GaugeHold[score] * m_iLife / (2 * *m_ChangeValues.Total);
		m_iLife -= m_ChangeValues.GaugeHold[score == HNS_OK ? HNS_OK : RNS_OK];
		break;

	case SHOCK_NG:
	case HNS_OK:
	case RNS_OK:
	case SHOCK_OK:
		m_iLife += m_ChangeValues.GaugeHold[score] * m_iFactor / *m_ChangeValues.Total;
	}
	m_iFactor += m_ChangeValues.FactorHold[score];

	CLAMP(m_iLife, 0, *m_ChangeValues.Total);
	CLAMP(m_iFactor, m_iFactorMin, m_iFactorMax);

	if( m_iLife <= m_iDangerThreshold )
		m_CurrentState = GAUGE_STATE_DANGER;
	else if( m_iLife >= m_iHotThreshold )
		m_CurrentState = GAUGE_STATE_HOT;
	else
		m_CurrentState = GAUGE_STATE_NORMAL;
}

float LifeMeterGauge::GetLife() const
{
	return (float)m_iLife / *m_ChangeValues.Total;
}

void LifeMeterGauge::UpdateNonstopLifebar( const int cleared, const int total, int ProgressiveLifebarDifficulty )
{
#if 0
	if (GAMESTATE->IsExtraStage() || GAMESTATE->IsExtraStage2())
	{
		// extra stage is its own thing, should not be progressive
		// and it should be as difficult as life 4
		// (e.g. it should not depend on life settings)

		m_iProgressiveLifebar = 0;
		m_fLifeDifficulty = 1.0f;
		return;
	}

	if (total > 1)
		m_fLifeDifficulty = m_fBaseLifeDifficulty - 0.2f * (int)(ProgressiveLifebarDifficulty * cleared / (total - 1));
	else
		m_fLifeDifficulty = m_fBaseLifeDifficulty - 0.2f * ProgressiveLifebarDifficulty;

	if (m_fLifeDifficulty >= 0.4) return;

    /* Approximate deductions for a miss
	 * Life 1 :    5   %
	 * Life 2 :    5.7 %
	 * Life 3 :    6.6 %
	 * Life 4 :    8   %
	 * Life 5 :   10   %
	 * Life 6 :   13.3 %
	 * Life 7 :   20   %
	 * Life 8 :   26.6 %
	 * Life 9 :   32   %
	 * Life 10:   40   %
	 * Life 11:   50   %
	 * Life 12:   57.1 %
	 * Life 13:   66.6 %
	 * Life 14:   80   %
	 * Life 15:  100   %
	 * Life 16+: 200   %
	 *
	 * Note there is 200%, because boos take off 1/2 as much as
	 * a miss, and a boo would suck up half of your lifebar.
	 *
	 * Everything past 7 is intended mainly for nonstop mode.
	 */


	// the lifebar is pretty harsh at 0.4 already (you lose
	// about 20% of your lifebar); at 0.2 it would be 40%, which
	// is too harsh at one difficulty level higher.  Override.

	int m_iLifeDifficulty = int((1.8f - m_fLifeDifficulty)/0.2f);

	// first eight values don't matter
	float DifficultyValues[16] = {0,0,0,0,0,0,0,0,
		0.3f, 0.25f, 0.2f, 0.16f, 0.14f, 0.12f, 0.10f, 0.08f};

	if (m_iLifeDifficulty >= 16)
	{
		// judge 16 or higher
		m_fLifeDifficulty = 0.04f;
		return;
	}

	m_fLifeDifficulty = DifficultyValues[m_iLifeDifficulty];
	return;
#endif
}

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * Contains code (c) 2001-2004 Chris Danford.
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
