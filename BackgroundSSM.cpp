#include "global.h"
#include "BackgroundSSM.h"
#include "PrefsManager.h"
#include "SongManager.h"
#include "ThemeManager.h"
#include "RageUtil.h"
#include "Song.h"
#include "RageTextureManager.h"
#include "Course.h"
#include "Character.h"
#include "BackgroundCache.h"

CachedThemeMetricB SCROLL_BG_RANDOM			("BackgroundSSM","ScrollRandom");
CachedThemeMetricB SCROLL_BG_ROULETTE		("BackgroundSSM","ScrollRoulette");

BackgroundSSM::BackgroundSSM()
{
	SCROLL_BG_RANDOM.Refresh();
	SCROLL_BG_ROULETTE.Refresh();

	m_bBGCache = false;

	m_bScrolling = false;
	m_fPercentScrolling = 0;

	//if( PREFSMAN->m_BackgroundCache != PrefsManager::BGCACHE_OFF )
	//{
	//	TEXTUREMAN->CacheTexture( SongBackgroundSSMTexture(THEME->GetPathG("Background","All Music")) );
	//	TEXTUREMAN->CacheTexture( SongBackgroundSSMTexture(THEME->GetPathG("Common","fallback background")) );
	//	TEXTUREMAN->CacheTexture( SongBackgroundSSMTexture(THEME->GetPathG("Background","roulette")) );
	//	TEXTUREMAN->CacheTexture( SongBackgroundSSMTexture(THEME->GetPathG("Background","random")) );
	//	TEXTUREMAN->CacheTexture( SongBackgroundSSMTexture(THEME->GetPathG("Background","Sort")) );
	//	TEXTUREMAN->CacheTexture( SongBackgroundSSMTexture(THEME->GetPathG("Background","Mode")) );

	//	TEXTUREMAN->CacheTexture( SongBackgroundSSMTexture(THEME->GetPathG("Preview","All Music")) );
	//	TEXTUREMAN->CacheTexture( SongBackgroundSSMTexture(THEME->GetPathG("Common","fallback preview")) );
	//	TEXTUREMAN->CacheTexture( SongBackgroundSSMTexture(THEME->GetPathG("Preview","roulette")) );
	//	TEXTUREMAN->CacheTexture( SongBackgroundSSMTexture(THEME->GetPathG("Preview","random")) );
	//	TEXTUREMAN->CacheTexture( SongBackgroundSSMTexture(THEME->GetPathG("Preview","Sort")) );
	//	TEXTUREMAN->CacheTexture( SongBackgroundSSMTexture(THEME->GetPathG("Preview","Mode")) );
	//}
}

bool BackgroundSSM::Load( RageTextureID ID, bool bPreview )
{
	if( ID.filename == "" )
	{
		if( bPreview )
			ID = THEME->GetPathToG("Common fallback preview");
		else
			ID = THEME->GetPathToG("Common fallback background");
	}

	if( PREFSMAN->m_BackgroundCache == PrefsManager::BGCACHE_OFF || !m_bBGCache || ID.Type == RageTextureID::TYPE_VIDEO )
	{
        ID = SongBackgroundSSMTexture(ID);
	}
	else
	{
		BACKGROUNDCACHE->LoadBackground( ID.filename );
		ID = BACKGROUNDCACHE->LoadCachedBackground( ID.filename );
	}

	m_fPercentScrolling = 0;
	m_bScrolling = false;

	TEXTUREMAN->DisableOddDimensionWarning();
	TEXTUREMAN->VolatileTexture( ID );
	bool ret = Sprite::Load( ID );
	TEXTUREMAN->EnableOddDimensionWarning();

	return ret;
};

void BackgroundSSM::Update( float fDeltaTime )
{
	Sprite::Update( fDeltaTime );

	if( m_bScrolling )
	{
        m_fPercentScrolling += fDeltaTime/2;
		m_fPercentScrolling -= (int)m_fPercentScrolling;

		const RectF *pTextureRect = m_pTexture->GetTextureCoordRect(0);

		float fTexCoords[8] =
		{
			0+m_fPercentScrolling, pTextureRect->top,		// top left
			0+m_fPercentScrolling, pTextureRect->bottom,	// bottom left
			1+m_fPercentScrolling, pTextureRect->bottom,	// bottom right
			1+m_fPercentScrolling, pTextureRect->top,		// top right
		};
		Sprite::SetCustomTextureCoords( fTexCoords );
	}
}

void BackgroundSSM::SetScrolling( bool bScroll, float Percent)
{
	m_bScrolling = bScroll;
	m_fPercentScrolling = Percent;

	// Set up the texture coord rects for the current state.
	Update(0);
}

void BackgroundSSM::LoadFromSong( Song* pSong, bool bPreview )		// NULL means no song
{
	if( pSong == NULL )
		LoadFallback( bPreview );
	else if( !bPreview && pSong->HasBackground() )
		Load( pSong->GetBackgroundPath(), bPreview );
	else if( bPreview && pSong->HasPreview() )
		Load( pSong->GetPreviewPath(), bPreview );
	else
		LoadFallback( bPreview );

	m_bScrolling = false;
}

void BackgroundSSM::LoadAllMusic( bool bPreview )
{
	CString sPath = bPreview ? THEME->GetPathG("Preview","All Music") : THEME->GetPathG("Background","All Music");
	Load( sPath, bPreview );

	m_bScrolling = false;
}

void BackgroundSSM::LoadSort( bool bPreview )
{
	CString sPath = bPreview ? THEME->GetPathG("Preview","Sort") : THEME->GetPathG("Background","Sort");
	Load( sPath, bPreview );

	m_bScrolling = false;
}

void BackgroundSSM::LoadMode( bool bPreview )
{
	CString sPath = bPreview ? THEME->GetPathG("Preview","Mode") : THEME->GetPathG("Background","Mode");
	Load( sPath, bPreview );

	m_bScrolling = false;
}

void BackgroundSSM::LoadFromGroup( CString sGroupName, bool bPreview )
{
	CString sPath = bPreview ? SONGMAN->GetGroupPreviewPath( sGroupName ) : SONGMAN->GetGroupBackgroundPath( sGroupName );

	if( sPath != "" )
		Load( sPath, bPreview );
	else
		LoadFallback( bPreview );

	m_bScrolling = false;
}

void BackgroundSSM::LoadFromCourse( Course* pCourse, bool bPreview )		// NULL means no course
{
	if( pCourse == NULL )
		LoadFallback( bPreview );
	else if( !bPreview && pCourse->HasBackground() )
		Load( pCourse->m_sBackgroundPath, bPreview );
	else if( bPreview && pCourse->HasPreview() )
		Load( pCourse->m_sPreviewPath, bPreview );
	else
		LoadFallback( bPreview );

	m_bScrolling = false;
}

void BackgroundSSM::LoadCardFromCharacter( Character* pCharacter, bool bPreview )
{
	ASSERT( pCharacter );

	if( pCharacter->GetCardPath() != "" )
		Load( pCharacter->GetCardPath(), bPreview );
	else
		LoadFallback( bPreview );

	m_bScrolling = false;
}

void BackgroundSSM::LoadIconFromCharacter( Character* pCharacter, bool bPreview )
{
	ASSERT( pCharacter );

	if( pCharacter->GetIconPath() != "" )
		Load( pCharacter->GetIconPath(), bPreview );
	else if( pCharacter->GetCardPath() != "" )
		Load( pCharacter->GetCardPath(), bPreview );
	else
		LoadFallback( bPreview );

	m_bScrolling = false;
}

void BackgroundSSM::LoadTABreakFromCharacter( Character* pCharacter, bool bPreview )
{
	if( pCharacter == NULL )
		Load( THEME->GetPathToG("Common fallback takingabreak"), bPreview );
	else
	{
		Load( pCharacter->GetTakingABreakPath(), bPreview );
		m_bScrolling = false;
	}
}

void BackgroundSSM::LoadFallback( bool bPreview )
{
	CString sPath = bPreview ? THEME->GetPathToG("Common fallback preview") : THEME->GetPathToG("Common fallback background");
	Load( sPath, bPreview );
}

void BackgroundSSM::LoadRoulette( bool bPreview )
{
	CString sPath = bPreview ? THEME->GetPathToG("Preview roulette") : THEME->GetPathToG("Background roulette");
	Load( sPath, bPreview );

	m_bScrolling = (bool)SCROLL_BG_ROULETTE;
}

void BackgroundSSM::LoadRandom( bool bPreview )
{
	CString sPath = bPreview ? THEME->GetPathToG("Preview random") : THEME->GetPathToG("Background random");
	Load( sPath, bPreview );

	m_bScrolling = (bool)SCROLL_BG_RANDOM;
}


/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * Contains code (c) 2001-2004 Chris Danford, 2008 Mike Hawkins.
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
