#include "global.h"
#include "PrefsManager.h"
#include "IniFile.h"
#include "GameState.h"
#include "RageDisplay.h"
#include "RageUtil.h"
#include "arch/arch.h" /* for default driver specs */
#include "RageSoundReader_Resample.h" /* for ResampleQuality */
#include "RageFile.h"
#include "ProductInfo.h"
#include "Foreach.h"
#include "Preference.h"

#define DEFAULTS_INI_PATH	"Data/Defaults.ini"		// these can be overridden
#define STEPMANIA_INI_PATH	"Data/StepMania.ini"		// overlay on Defaults.ini, contains the user's choices
#define STATIC_INI_PATH		"Data/Static.ini"		// overlay on the 2 above, can't be overridden

PrefsManager*	PREFSMAN = NULL;	// global and accessable from anywhere in our program

const float DEFAULT_SOUND_VOLUME = 1.00f;
const CString DEFAULT_LIGHTS_DRIVER = "Null";

const unsigned DEFAULT_GAMELOOP_DELAY = 4;
const unsigned DEFAULT_GAMELOOP_DELAY_HIGH_PERFORMANCE = 1;

//
// For self-registering prefs
//
vector<IPreference*> *g_pvpSubscribers = NULL;

void Subscribe( IPreference *p )
{
	// TRICKY: If we make this a global vector instead of a global pointer,
	// then we'd have to be careful that the static constructors of all
	// Preferences are called before the vector constructor.  It's
	// too tricky to enfore that, so we'll allocate the vector ourself
	// so that the compiler can't possibly call the vector constructor
	// after we've already added to the vector.
	if( g_pvpSubscribers == NULL )
		g_pvpSubscribers = new vector<IPreference*>;
	g_pvpSubscribers->push_back( p );
}

bool g_bAutoRestart = false;

PrefsManager::PrefsManager()
{
	Init();
	ReadGlobalPrefsFromDisk();
}

static int CompareStageLengths(const PrefsManager::StageLength &sl1, const PrefsManager::StageLength &sl2)
{
	if( sl1.m_iStagesPerSong == sl2.m_iStagesPerSong )
	{
		if( sl1.m_fMusicLengthSeconds == sl2.m_fMusicLengthSeconds )
			return sl1.m_bUseSongWithLength < sl2.m_bUseSongWithLength;

		return sl1.m_fMusicLengthSeconds < sl2.m_fMusicLengthSeconds;
	}
	return sl1.m_iStagesPerSong < sl2.m_iStagesPerSong;
}

void PrefsManager::Init()
{
	size_t int_size = sizeof(int);
	size_t float_size = sizeof(float);

	m_bWindowed = false;
	m_iDisplayWidth = 640;
	m_iDisplayHeight = 480;
	m_iDisplayColorDepth = 16;
	m_iTextureColorDepth = 16;		// default to 16 for better preformance on slower cards
	m_iMovieColorDepth = 16;
	m_iMaxTextureResolution = 2048;
	m_iRefreshRate = REFRESH_DEFAULT;
	m_bOnlyDedicatedMenuButtons = false;
	m_bCelShadeModels = false;		// Work-In-Progress.. disable by default.
	m_bShowStats = true;
	m_CurrentDPI = RageVector2(SCREEN_DPI_DEFAULT, SCREEN_DPI_DEFAULT);
	m_CurrentPosition = RageVector2(0.f, 0.f);

#ifdef WITH_CATALOG_XML
	m_bCatalogXML = true;
#endif

	m_bShowBanners = true;
	m_bShowDiscs = true;
	m_bShowBackgrounds = true;
	m_bShowPreviews = true;
	m_bShowCDTitles = true;

	m_bBannerUsesCacheOnly = false;
	m_bDiscUsesCacheOnly = false;
	m_bBackgroundUsesCacheOnly = false;
	m_bPreviewUsesCacheOnly = false;

	m_BackgroundMode = BGMODE_RANDOMMOVIES;
	m_iNumBackgrounds = 1;
	m_bShowDanger = false;
	m_fBGBrightness = 1.0f;
	m_bMenuTimer = false;
	m_bEventMode = true;
	m_bEventIgnoreSelectable = true;
	m_bEventIgnoreUnlock = true;
	m_bAutoPlay = false;
	m_fJudgeWindowScale = 1.0f;
	m_fJudgeWindowAdd = 0;
	m_fJudgeWindowScaleAfter = 1.0f;
	m_fJudgeWindowAddAfter = 0;
	m_fLifeDifficultyScale = 1.0f;

	m_bPositiveAnnouncerOnly = false;
	m_bEvalExtraAnnouncer = true;
	m_bGameExtraAnnouncer = true;

	m_fDebounceTime = 0.01f;

	m_bNonstopUsesExtremeScoring = false;

	m_bDanceRaveShufflesNotes = true;

	m_bRollTapTimingIsAffectedByJudge = true;
	m_fJudgeWindowSecondsMarvelous =		0.0225f;
	m_fJudgeWindowSecondsPerfect =			0.045f;
	m_fJudgeWindowSecondsGreat =			0.090f;
	m_fJudgeWindowSecondsGood =				0.135f;
	m_fJudgeWindowSecondsBoo =				0.180f;
	m_fJudgeWindowSecondsHidden =			0.045f;	// same as perfect
	m_fJudgeWindowSecondsHold =				0.250f;	// allow enough time to take foot off and put back on
	m_fJudgeWindowSecondsRoll =				0.300f;	// same judgment for HOLD_OK, but roll time is different - Mark
	m_fJudgeWindowSecondsLong =				0.090f;	// same as great
	m_fJudgeWindowSecondsMine =				0.090f;	// same as great
	m_fJudgeWindowSecondsShock =			0.090f;	// same as great
	m_fJudgeWindowSecondsPotion =			0.090f;	// same as great
	m_fJudgeWindowSecondsAttack =			0.135f;
	m_fJudgeWindowSecondsAfterMarvelous =	0.0225f;
	m_fJudgeWindowSecondsAfterPerfect =		0.045f;
	m_fJudgeWindowSecondsAfterGreat =		0.090f;
	m_fJudgeWindowSecondsAfterGood =		0.135f;
	m_fJudgeWindowSecondsAfterBoo =			0.180f;
	m_fJudgeWindowSecondsAfterHidden =		0.045f;	// same as perfect
	m_fJudgeWindowSecondsAfterLong =		0.090f;	// same as great
	m_fJudgeWindowSecondsAfterMine =		0.090f;	// same as great
	m_fJudgeWindowSecondsAfterShock =		0.090f;	// same as great
	m_fJudgeWindowSecondsAfterPotion =		0.090f;	// same as great
	m_fJudgeWindowSecondsAfterAttack =		0.135f;

	m_iGaugeDifficultyScale = 900;

	// Gauge Meter, Normal Play
	m_iGaugeTotalValue = 1000;
	{
		int iValues[NUM_TAP_NOTE_SCORES] = {
			0,	// TNS_NONE
			-1000,	// TNS_HIT_MINE
			-1000,	// TNS_HIT_SHOCK
			24,	// TNS_HIT_POTION
			-50,	// TNS_MISS_CHECKPOINT
			10,	// TNS_CHECKPOINT
			0,	// TNS_MISS_HIDDEN
			1000,	// TNS_HIDDEN
			-500,	// TNS_MISS
			-50,	// TNS_BOO
			0,	// TNS_GOOD
			10,	// TNS_GREAT
			11,	// TNS_PERFECT
			12	// TNS_MARVELOUS
		};
		memcpy(&m_iGaugeChange, &iValues, int_size * NUM_TAP_NOTE_SCORES);
	}
	{
		int iValues[NUM_TAP_NOTE_SCORES] = {
			0,	// TNS_NONE
			-1400,	// TNS_HIT_MINE
			-1400,	// TNS_HIT_SHOCK
			40,	// TNS_HIT_POTION
			-350,	// TNS_MISS_CHECKPOINT
			16,	// TNS_CHECKPOINT
			0,	// TNS_MISS_HIDDEN
			1000,	// TNS_HIDDEN
			-700,	// TNS_MISS
			-350,	// TNS_BOO
			0,	// TNS_GOOD
			16,	// TNS_GREAT
			19,	// TNS_PERFECT
			20	// TNS_MARVELOUS
		};
		memcpy(&m_iGaugeFactorChange, &iValues, int_size * NUM_TAP_NOTE_SCORES);
	}
	{
		int iValues[NUM_HOLD_NOTE_SCORES] = {
			0,	// HNS_NONE
			0,	// HNS_NG
			0,	// HNS_OK
			0,	// RNS_NG
			0,	// RNS_OK
			0,	// SHOCK_NG
			0	// SHOCK_OK
		};
		memcpy(&m_iGaugeChangeHold, &iValues, int_size * NUM_HOLD_NOTE_SCORES);
	}
	{
		int iValues[NUM_HOLD_NOTE_SCORES] = {
			0,	// HNS_NONE
			0,	// HNS_NG
			0,	// HNS_OK
			0,	// RNS_NG
			0,	// RNS_OK
			0,	// SHOCK_NG
			0	// SHOCK_OK
		};
		memcpy(&m_iGaugeFactorChangeHold, &iValues, int_size * NUM_HOLD_NOTE_SCORES);
	}

	// Gauge Meter, No Recover
	m_iGaugeTotalValueNR = 1000;
	{
		int iValues[NUM_TAP_NOTE_SCORES] = {
			0,	// TNS_NONE
			-1000,	// TNS_HIT_MINE
			-1000,	// TNS_HIT_SHOCK
			0,	// TNS_HIT_POTION
			-50,	// TNS_MISS_CHECKPOINT
			0,	// TNS_CHECKPOINT
			0,	// TNS_MISS_HIDDEN
			0,	// TNS_HIDDEN
			-500,	// TNS_MISS
			-50,	// TNS_BOO
			0,	// TNS_GOOD
			0,	// TNS_GREAT
			0,	// TNS_PERFECT
			0	// TNS_MARVELOUS
		};
		memcpy(&m_iGaugeChangeNR, &iValues, int_size * NUM_TAP_NOTE_SCORES);
	}
	{
		int iValues[NUM_TAP_NOTE_SCORES] = {
			0,	// TNS_NONE
			-1400,	// TNS_HIT_MINE
			-1400,	// TNS_HIT_SHOCK
			40,	// TNS_HIT_POTION
			-350,	// TNS_MISS_CHECKPOINT
			16,	// TNS_CHECKPOINT
			0,	// TNS_MISS_HIDDEN
			1000,	// TNS_HIDDEN
			-700,	// TNS_MISS
			-350,	// TNS_BOO
			0,	// TNS_GOOD
			16,	// TNS_GREAT
			19,	// TNS_PERFECT
			20	// TNS_MARVELOUS
		};
		memcpy(&m_iGaugeFactorChangeNR, &iValues, int_size * NUM_TAP_NOTE_SCORES);
	}
	{
		int iValues[NUM_HOLD_NOTE_SCORES] = {
			0,	// HNS_NONE
			0,	// HNS_NG
			0,	// HNS_OK
			0,	// RNS_NG
			0,	// RNS_OK
			0,	// SHOCK_NG
			0	// SHOCK_OK
		};
		memcpy(&m_iGaugeChangeHoldNR, &iValues, int_size * NUM_HOLD_NOTE_SCORES);
	}
	{
		int iValues[NUM_HOLD_NOTE_SCORES] = {
			0,	// HNS_NONE
			0,	// HNS_NG
			0,	// HNS_OK
			0,	// RNS_NG
			0,	// RNS_OK
			0,	// SHOCK_NG
			0	// SHOCK_OK
		};
		memcpy(&m_iGaugeFactorChangeHoldNR, &iValues, int_size * NUM_HOLD_NOTE_SCORES);
	}

	// Gauge Meter, Sudden Death
	m_iGaugeTotalValueSD = 1000;
	{
		int iValues[NUM_TAP_NOTE_SCORES] = {
			0,	// TNS_NONE
			INT_MIN,	// TNS_HIT_MINE
			INT_MIN,	// TNS_HIT_SHOCK
			0,	// TNS_HIT_POTION
			INT_MIN,	// TNS_MISS_CHECKPOINT
			0,	// TNS_CHECKPOINT
			0,	// TNS_MISS_HIDDEN
			0,	// TNS_HIDDEN
			INT_MIN,	// TNS_MISS
			INT_MIN,	// TNS_BOO
			0,	// TNS_GOOD
			0,	// TNS_GREAT
			0,	// TNS_PERFECT
			0	// TNS_MARVELOUS
		};
		memcpy(&m_iGaugeChangeSD, &iValues, int_size * NUM_TAP_NOTE_SCORES);
	}
	{
		int iValues[NUM_TAP_NOTE_SCORES] = {
			0,	// TNS_NONE
			INT_MIN,	// TNS_HIT_MINE
			INT_MIN,	// TNS_HIT_SHOCK
			0,	// TNS_HIT_POTION
			INT_MIN,	// TNS_MISS_CHECKPOINT
			0,	// TNS_CHECKPOINT
			0,	// TNS_MISS_HIDDEN
			0,	// TNS_HIDDEN
			INT_MIN,	// TNS_MISS
			INT_MIN,	// TNS_BOO
			0,	// TNS_GOOD
			0,	// TNS_GREAT
			0,	// TNS_PERFECT
			0	// TNS_MARVELOUS
		};
		memcpy(&m_iGaugeFactorChangeSD, &iValues, int_size * NUM_TAP_NOTE_SCORES);
	}
	{
		int iValues[NUM_HOLD_NOTE_SCORES] = {
			0,	// HNS_NONE
			0,	// HNS_NG
			0,	// HNS_OK
			0,	// RNS_NG
			0,	// RNS_OK
			0,	// SHOCK_NG
			0	// SHOCK_OK
		};
		memcpy(&m_iGaugeChangeHoldSD, &iValues, int_size * NUM_HOLD_NOTE_SCORES);
	}
	{
		int iValues[NUM_HOLD_NOTE_SCORES] = {
			0,	// HNS_NONE
			0,	// HNS_NG
			0,	// HNS_OK
			0,	// RNS_NG
			0,	// RNS_OK
			0,	// SHOCK_NG
			0	// SHOCK_OK
		};
		memcpy(&m_iGaugeFactorChangeHoldSD, &iValues, int_size * NUM_HOLD_NOTE_SCORES);
	}

	// Life Meter, Normal Play
	m_fLifePercentInitialValue = 0.5f;
	{
		float fValues[NUM_TAP_NOTE_SCORES] = {
			0.0f,	// TNS_NONE
			-0.16f,	// TNS_HIT_MINE
			-0.16f,	// TNS_HIT_SHOCK
			0.016f,	// TNS_HIT_POTION
			-0.04f,	// TNS_MISS_CHECKPOINT
			0.004f,	// TNS_CHECKPOINT
			0.0f,	// TNS_MISS_HIDDEN
			0.25f,	// TNS_HIDDEN
			-0.08f,	// TNS_MISS
			-0.04f,	// TNS_BOO
			0.0f,	// TNS_GOOD
			0.004f,	// TNS_GREAT
			0.008f,	// TNS_PERFECT
			0.008f	// TNS_MARVELOUS
		};
		memcpy(&m_fLifeDeltaPercentChange, &fValues, float_size * NUM_TAP_NOTE_SCORES);
	}
	{
		float fValues[NUM_HOLD_NOTE_SCORES] = {
			0.0f,	// HNS_NONE
			-0.08f,	// HNS_NG
			0.008f,	// HNS_OK
			-0.08f,	// RNS_NG
			0.008f,	// RNS_OK
			0.0f,	// SHOCK_NG
			0.008f	// SHOCK_OK
		};
		memcpy(&m_fLifeDeltaPercentChangeHold, &fValues, float_size * NUM_HOLD_NOTE_SCORES);
	}

	// Life Meter, No Recover
	m_fLifePercentInitialValueNR = 1.f;
	{
		float fValues[NUM_TAP_NOTE_SCORES] = {
			0.0f,	// TNS_NONE
			-0.16f,	// TNS_HIT_MINE
			-0.16f,	// TNS_HIT_SHOCK
			0.0f,	// TNS_HIT_POTION
			-0.04f,	// TNS_MISS_CHECKPOINT
			0.0f,	// TNS_CHECKPOINT
			0.0f,	// TNS_MISS_HIDDEN
			0.0f,	// TNS_HIDDEN
			-0.08f,	// TNS_MISS
			-0.04f,	// TNS_BOO
			0.0f,	// TNS_GOOD
			0.0f,	// TNS_GREAT
			0.0f,	// TNS_PERFECT
			0.0f	// TNS_MARVELOUS
		};
		memcpy(&m_fLifeDeltaPercentChangeNR, &fValues, float_size * NUM_TAP_NOTE_SCORES);
	}
	{
		float fValues[NUM_HOLD_NOTE_SCORES] = {
			0.0f,	// HNS_NONE
			-0.08f,	// HNS_NG
			0.0f,	// HNS_OK
			-0.08f,	// RNS_NG
			0.0f,	// RNS_OK
			0.0f,	// SHOCK_NG
			0.0f	// SHOCK_OK
		};
		memcpy(&m_fLifeDeltaPercentChangeHoldNR, &fValues, float_size * NUM_HOLD_NOTE_SCORES);
	}

	// Life Meter, Sudden Death
	m_fLifePercentInitialValueSD = 1.f;
	{
		float fValues[NUM_TAP_NOTE_SCORES] = {
			0.0f,	// TNS_NONE
			-1.f,	// TNS_HIT_MINE
			-1.f,	// TNS_HIT_SHOCK
			0.0f,	// TNS_HIT_POTION
			-1.f,	// TNS_MISS_CHECKPOINT
			0.0f,	// TNS_CHECKPOINT
			0.0f,	// TNS_MISS_HIDDEN
			0.0f,	// TNS_HIDDEN
			-1.f,	// TNS_MISS
			-1.f,	// TNS_BOO
			0.0f,	// TNS_GOOD
			0.0f,	// TNS_GREAT
			0.0f,	// TNS_PERFECT
			0.0f	// TNS_MARVELOUS
		};
		memcpy(&m_fLifeDeltaPercentChangeSD, &fValues, float_size * NUM_TAP_NOTE_SCORES);
	}
	{
		float fValues[NUM_HOLD_NOTE_SCORES] = {
			0.0f,	// HNS_NONE
			-1.f,	// HNS_NG
			0.0f,	// HNS_OK
			-1.f,	// RNS_NG
			0.0f,	// RNS_OK
			0.0f,	// SHOCK_NG
			0.0f	// SHOCK_OK
		};
		memcpy(&m_fLifeDeltaPercentChangeHoldSD, &fValues, float_size * NUM_HOLD_NOTE_SCORES);
	}

	// Tug Meter, used in Rave
	{
		float fValues[NUM_TAP_NOTE_SCORES] = {
			0.0f,	// TNS_NONE
			-0.04f,	// TNS_HIT_MINE
			-0.04f,	// TNS_HIT_SHOCK
			0.02f,	// TNS_HIT_POTION
			-0.01f,	// TNS_MISS_CHECKPOINT
			0.004f,	// TNS_CHECKPOINT
			0.0f,	// TNS_MISS_HIDDEN
			0.25f,	// TNS_HIDDEN
			-0.02f,	// TNS_MISS
			-0.01f,	// TNS_BOO
			0.0f,	// TNS_GOOD
			0.004f,	// TNS_GREAT
			0.008f,	// TNS_PERFECT
			0.01f	// TNS_MARVELOUS
		};
		memcpy(&m_fTugMeterPercentChange, &fValues, float_size * NUM_TAP_NOTE_SCORES);
	}
	{
		float fValues[NUM_HOLD_NOTE_SCORES] = {
			0.0f,	// HNS_NONE
			-0.02f,	// HNS_NG
			0.008f,	// HNS_OK
			-0.02f,	// RNS_NG
			0.008f,	// RNS_OK
			0.0f,	// SHOCK_NG
			0.008f	// SHOCK_OK
		};
		memcpy(&m_fTugMeterPercentChangeHold, &fValues, float_size * NUM_HOLD_NOTE_SCORES);
	}

	// ScoreKeeperRave
	{
		float fValues[NUM_TAP_NOTE_SCORES] = {
			0.0f,	// TNS_NONE
			-0.4f,	// TNS_HIT_MINE
			-0.4f,	// TNS_HIT_SHOCK
			0.1f,	// TNS_HIT_POTION
			-0.1f,	// TNS_MISS_CHECKPOINT
			0.02f,	// TNS_CHECKPOINT
			0.0f,	// TNS_MISS_HIDDEN
			0.2f,	// TNS_HIDDEN
			-0.2f,	// TNS_MISS
			0.0f,	// TNS_BOO
			0.0f,	// TNS_GOOD
			0.02f,	// TNS_GREAT
			0.04f,	// TNS_PERFECT
			0.05f	// TNS_MARVELOUS
		};
		memcpy(&m_fSuperMeterPercentChange, &fValues, float_size * NUM_TAP_NOTE_SCORES);
	}
	{
		float fValues[NUM_HOLD_NOTE_SCORES] = {
			0.0f,	// HNS_NONE
			-0.2f,	// HNS_NG
			0.04f,	// HNS_OK
			-0.2f,	// RNS_NG
			0.04f,	// RNS_OK
			0.0f,	// SHOCK_NG
			0.04f	// SHOCK_OK
		};
		memcpy(&m_fSuperMeterPercentChangeHold, &fValues, float_size * NUM_HOLD_NOTE_SCORES);
	}
	m_bMercifulSuperMeter = true;

	m_iRegenComboAfterFail = 10; // cumulative
	m_iRegenComboAfterMiss = 5; // cumulative
	m_iMaxRegenComboAfterFail = 10;
	m_iMaxRegenComboAfterMiss = 10;
	m_bTwoPlayerRecovery = true;
	m_bMercifulDrain = true;
	m_bMinimum1FullSongInCourses = false;

	m_bFadeVideoBackgrounds = false;

	m_bPlayAttackSounds = true;
	m_bPlayMineSound = true;
	m_bPlayShockSound = true;
	m_bPlayPotionSound = true;

	m_fRandomAttackLength = 5.0f;
	m_fTimeBetweenRandomAttacks = 2.0f;
	m_fAttackMinesLength = 7.0f;

	// Percent Score
	{
		int iValues[NUM_TAP_NOTE_SCORES] = {
			0,	// TNS_NONE
			-2,	// TNS_HIT_MINE
			-2,	// TNS_HIT_SHOCK
			1,	// TNS_HIT_POTION
			0,	// TNS_MISS_CHECKPOINT
			1,	// TNS_CHECKPOINT
			0,	// TNS_MISS_HIDDEN
			2,	// TNS_HIDDEN
			0,	// TNS_MISS
			0,	// TNS_BOO
			0,	// TNS_GOOD
			1,	// TNS_GREAT
			2,	// TNS_PERFECT
			3	// TNS_MARVELOUS
		};
		memcpy(&m_iPercentScoreWeight, &iValues, int_size * NUM_TAP_NOTE_SCORES);
	}
	{
		int iValues[NUM_HOLD_NOTE_SCORES] = {
			0,	// HNS_NONE
			0,	// HNS_NG
			3,	// HNS_OK
			0,	// RNS_NG
			3,	// RNS_OK
			0,	// SHOCK_NG
			3	// SHOCK_OK
		};
		memcpy(&m_iPercentScoreWeightHold, &iValues, int_size * NUM_HOLD_NOTE_SCORES);
	}

	// Grade Weight
	{
		int iValues[NUM_TAP_NOTE_SCORES] = {
			0,	// TNS_NONE
			-20,	// TNS_HIT_MINE
			-20,	// TNS_HIT_SHOCK
			11,	// TNS_HIT_POTION
			-10,	// TNS_MISS_CHECKPOINT
			15,	// TNS_CHECKPOINT
			0,	// TNS_MISS_HIDDEN
			20,	// TNS_HIDDEN
			-20,	// TNS_MISS
			-5,	// TNS_BOO
			11,	// TNS_GOOD
			15,	// TNS_GREAT
			19,	// TNS_PERFECT
			20	// TNS_MARVELOUS
		};
		memcpy(&m_iGradeWeight, &iValues, int_size * NUM_TAP_NOTE_SCORES);
	}
	{
		int iValues[NUM_HOLD_NOTE_SCORES] = {
			0,	// HNS_NONE
			-20,	// HNS_NG
			20,	// HNS_OK
			-20,	// RNS_NG
			20,	// RNS_OK
			0,	// SHOCK_NG
			20	// SHOCK_OK
		};
		memcpy(&m_iGradeWeightHold, &iValues, int_size * NUM_HOLD_NOTE_SCORES);
	}
	m_iNumGradeTiersUsed = 8;

	for( int i=0; i<NUM_GRADE_TIERS; i++ )
		m_fGradePercent[i] = 0;

	// Because the default mode is GM_DDR, we'll use it's values for the default
	m_fGradePercent[GRADE_TIER_1] = 1.00f;	// AAAA
	m_fGradePercent[GRADE_TIER_2] = 0.95f;	// AAA
	m_fGradePercent[GRADE_TIER_3] = 0.90f;	// AA
	m_fGradePercent[GRADE_TIER_4] = 0.80f;	// A
	m_fGradePercent[GRADE_TIER_5] = 0.70f;	// B
	m_fGradePercent[GRADE_TIER_6] = 0.60f;	// C
	m_fGradePercent[GRADE_TIER_7] = 0.50f;	// D
	m_fGradePercent[GRADE_TIER_8] = -99999;	// F
	m_bGradeTier02IsAllPerfects = true;
	m_bGradeTier02RequiresNoMiss = true;
	m_bGradeTier02RequiresFC = true;
	m_bGradeTier03RequiresNoMiss = true;
	m_bGradeTier03RequiresFC = false;

	m_bDelayedEscape = true;
	m_bInstructions = true;
	m_bShowDontDie = true;
	m_bShowSelectGroup = true;
	m_bShowNative = true;
	m_bArcadeOptionsNavigation = false;
	m_bSoloSingle = false;
	m_bDelayedTextureDelete = true;
	m_bTexturePreload = false;
	m_bDelayedScreenLoad = false;
	m_bDelayedModelDelete = false;
	m_BannerCache = BNCACHE_LOW_RES;
	m_bPalettedBannerCache = false;
	m_BackgroundCache = BGCACHE_LOW_RES;
	m_bPalettedBackgroundCache = false;
	m_bFastLoad = true;
	m_bFastLoadExistingCacheOnly = false;
	m_MusicWheelUsesSections = THEME;
	m_iMusicWheelSwitchSpeed = 10;
	m_bEasterEggs = true;
	m_iMarvelousTiming = 2;
	m_bDelayedCreditsReconcile = false;
	m_iBoostAppPriority = -1;
	m_bSmoothLines = false;
	m_ShowSongOptions = ASK;
	m_bDancePointsForOni = false;
	m_bPercentageScoring = false;
	m_fMinPercentageForMachineSongHighScore = 0.5f;
	m_fMinPercentageForMachineCourseHighScore = 0.001f;	// don't save course scores with 0 percentage
	m_bDisqualification = false;
	m_bShowLyrics = true;
	m_bAutogenSteps = true;
	m_bAutogenGroupCourses = true;
	m_bBreakComboToGetItem = false;
	m_bLockCourseDifficulties = true;
	m_ShowDancingCharacters = CO_RANDOM;

	m_bUseUnlockSystem = false;
	m_bUnlockUsesMachineProfileStats = true;
	m_bUnlockUsesPlayerProfileStats = false;

	m_bFirstRun = true;
	m_bAutoMapOnJoyChange = true;
	m_fGlobalOffsetSeconds = 0;
	m_bShowBeginnerHelper = false;
	m_bEndlessBreakEnabled = true;
	m_iEndlessNumStagesUntilBreak = 5;
	m_iEndlessBreakLength = 5;
	m_bDisableScreenSaver = true;

	// set to 0 so people aren't shocked at first
	m_iProgressiveLifebar = 0;
	m_iProgressiveNonstopLifebar = 0;
	m_iProgressiveStageLifebar = 0;

	// DDR Extreme style extra stage support.
	// Default off so people used to the current behavior (or those with extra stage CRS files)
	// don't get it changed around on them.
	m_bLockExtraStageDiff = false;
	m_bPickExtraStage = true;
	m_bAlwaysAllowExtraStage2 = true;
	m_bPickModsForExtraStage = true;
	m_bDarkExtraStage = false;
	m_bOniExtraStage1 = false;
	m_bOniExtraStage2 = false;

	m_bComboContinuesBetweenSongs = false;

	// default to old sort order
	m_iCourseSortOrder = COURSE_SORT_SONGS;
	m_bMoveRandomToEnd = false;
	m_bSubSortByNumSteps = false;
	m_iScoringType = SCORING_PIU_NX2;

	m_iGetRankingName = RANKING_ON;

	/* I'd rather get occasional people asking for support for this even though it's
	 * already here than lots of people asking why songs aren't being displayed. */
	m_bHiddenSongs = false;
	m_bVsync = true;
	m_sLanguage = "";	// ThemeManager will deal with this invalid language

	m_iCenterImageTranslateX = 0;
	m_iCenterImageTranslateY = 0;
	m_fCenterImageScaleX = 1;
	m_fCenterImageScaleY = 1;

	m_iAttractSoundFrequency = 1;
	m_bAllowExtraStage = true;
	m_bHideDefaultNoteSkin = false;
	m_iMaxHighScoresPerListForMachine = 10;
	m_iMaxHighScoresPerListForPlayer = 3;
	m_fPadStickSeconds = 0;
	m_bForceMipMaps = false;
	m_bTrilinearFiltering = false;
	m_bAnisotropicFiltering = false;
	g_bAutoRestart = false;
	m_bSignProfileData = false;

	m_bEditorShowBGChangesPlay = true;
	m_bEditShiftSelector = true;

	/* XXX: Set these defaults for individual consoles using VideoCardDefaults.ini. */
	m_bPAL = false;
#ifndef _XBOX
	m_bInterlaced = false;
#endif

	m_sSoundDrivers = "";	// default
	/* Number of frames to write ahead; usually 44100 frames per second.
	 * (Number of millisec would be more flexible, but it's more useful to
	 * specify numbers directly.) This is purely a troubleshooting option
	 * and is not honored by all sound drivers. */
	m_iSoundWriteAhead = 0;
	m_iSoundDevice = "";
	m_fSoundVolume = DEFAULT_SOUND_VOLUME;
	m_iSoundResampleQuality = RageSoundReader_Resample::RESAMP_NORMAL;

	m_sMovieDrivers = DEFAULT_MOVIE_DRIVER_LIST;

	// StepMania.cpp sets these on first run:
#if defined(WIN32)
	m_uLastSeenMemory = 0;
#endif
#if defined(HAVE_VERSION_INFO)
	m_sLastCommit = "";
#endif

	m_sLightsDriver = DEFAULT_LIGHTS_DRIVER;
	m_sLightsStepsDifficulty = "medium";
//CHANGE: Several lights options - Mark
	m_fLightsFalloffSeconds = 0.08f;
	m_bLightsFalloffUsesBPM = false;
	m_iLightsIgnoreHolds = 0;
	m_bBlinkGameplayButtonLightsOnNote = false;
	m_bThreadedInput = true;
	m_bThreadedMovieDecode = true;
	m_sMachineName = "NoName";
	m_sIgnoredMessageWindows = "";

	m_sCoursesToShowRanking = "";

	m_bLogToDisk = true;
	m_bForceLogFlush = false;
	m_bShowLogOutput = false;
	m_bTimestamping = false;
	m_bLogSkips = false;
	m_bLogCheckpoints = false;
	m_bShowLoadingWindow = true;

	m_bMemoryCards = false;

	// player song stuff
	m_bPlayerSongs = true;
	m_bPlayerSongsAllowBanners = true;
	m_bPlayerSongsAllowDiscs = true;
	m_bPlayerSongsAllowBackgrounds = true;
	m_bPlayerSongsAllowPreviews = true;
	m_bPlayerSongsAllowCDTitles = true;
	m_bPlayerSongsAllowLyrics = true;
	m_bPlayerSongsAllowBGChanges = true;
	m_fPlayerSongsLoadTimeout = 5.0f;
	m_fPlayerSongsLengthLimitSeconds = 300.0f;
	m_iPlayerSongsLoadLimit = 25;

	FOREACH_PlayerNumber( p )
	{
		m_iMemoryCardUsbBus[p] = -1;
		m_iMemoryCardUsbPort[p] = -1;
		m_iMemoryCardUsbLevel[p] = -1;
	}

	m_sMemoryCardProfileSubdir = PRODUCT_NAME;
	m_iProductID = 1;

	// StepMania AMX
	m_bChangeSpeedWithNumKeys = true;
	m_bStoreSpeedWithNumKeys = true;
	m_bAutoPlayCombo = true;
	m_bStartRandomBGAsWithMusic = true;
	m_bEndRandomBGAsWithMusic = true;
	m_bStageFinishesWithMusic = true;
	m_bMusicRateAffectsBGChanges = true;

	m_bMissComboIncreasesOnComboBreak = false;
	m_bComboBreakOnGood = false;
	m_bComboBreakOnMines = false;
	m_bComboBreakOnShocks = true;
	m_bComboIncreasesOnPotions = false;
	m_bComboIncreasesOnRollHits = true;
	m_bComboIncreasesOnHiddens = true;
	m_bHoldOKIncreasesCombo = false;
	m_bHoldNGBreaksCombo = false;
	m_bUsePIUHolds = true;

	m_bShowAllStyles = true;
	m_bLastEditedSong = true;
	m_bLastEditedDifficulty = true;
	m_bEditorSmoothScrolling = true;
	m_bGameplayKeysInEditor = false;
	m_bEditorCombo = true;
	m_bShowSnapBeats = true;
	m_bSelectLastSecond = false;
	m_bAutoSaveBeforePlay = true;
	m_fAutoSaveEverySeconds = 150.f;

	m_bTimingAsRows = true;

#if defined(_WINDOWS)
	m_bGameLoopDelay = true;
#else
	m_bGameLoopDelay = false;
#endif
	m_uGameLoopDelayMilliseconds = 0;

	m_iNumArcadeStages = 8;
	m_iMaxSongsToPlay = 3;

	m_iStageLengths = 6;
	StageLength sl;

	// Short Cut
	sl.m_iStagesPerSong = 1;
	sl.m_bUseSongWithLength = true;
	sl.m_fMusicLengthSeconds = -1.f;
	m_StageLength.push_back( sl );

	// Arcade
	sl.m_iStagesPerSong = 2;
	sl.m_bUseSongWithLength = true;
	sl.m_fMusicLengthSeconds = 70.f;
	m_StageLength.push_back( sl );

	// Remix
	sl.m_iStagesPerSong = 3;
	sl.m_bUseSongWithLength = true;
	sl.m_fMusicLengthSeconds = 130.f;
	m_StageLength.push_back( sl );

	// Full Song
	sl.m_iStagesPerSong = 4;
	sl.m_bUseSongWithLength = true;
	sl.m_fMusicLengthSeconds = 190.f;
	m_StageLength.push_back( sl );

	// Marathon
	sl.m_iStagesPerSong = 6;
	sl.m_bUseSongWithLength = true;
	sl.m_fMusicLengthSeconds = 250.f;
	m_StageLength.push_back( sl );

	// Megamix
	sl.m_iStagesPerSong = 8;
	sl.m_bUseSongWithLength = true;
	sl.m_fMusicLengthSeconds = 310.f;
	m_StageLength.push_back( sl );

	sort( m_StageLength.begin(), m_StageLength.end(), CompareStageLengths );
	m_iDefaultStagesPerSong = 2;
	m_bProgressiveLifebarBySong = true;

	m_iSkipSongsHarderThanOMES = 25;
	m_bSetPlayerOptionsES = true;
	m_bSetPlayerOptionsOMES = true;
	m_bSetSongOptionsES = true;
	m_bSetSongOptionsOMES = true;

	m_bCheckDifficultyES = false;
	m_bCheckDifficultyOMES = true;
	m_bCheckDifficultyAllStagesES = false;
	m_bCheckDifficultyAllStagesOMES = false;
	m_MinDifficultyES = DIFFICULTY_HARD;
	m_MinDifficultyOMES = DIFFICULTY_HARD;
	m_MaxDifficultyES = DIFFICULTY_CHALLENGE;
	m_MaxDifficultyOMES = DIFFICULTY_CHALLENGE;

	m_bCheckGradeES = true;
	m_bCheckGradeOMES = true;
	m_bCheckGradeAllStagesES = false;
	m_bCheckGradeAllStagesOMES = false;
	m_MinGradeES = GRADE_TIER_4;
	m_MinGradeOMES = GRADE_TIER_3;

	m_bCheckSongsPlayedES = true;
	m_bCheckSongsPlayedOMES = true;
	m_iMinSongsPlayedES = 2;
	m_iMinSongsPlayedOMES = 3;
	m_iMaxSongsPlayedES = 3;
	m_iMaxSongsPlayedOMES = 4;

	m_bAllowExtraStage2 = true;
	m_bLockExtraStage2Diff = false;
	m_bPickExtraStage2 = true;
	m_bPickModsForExtraStage2 = true;
	m_bDarkExtraStage2 = false;

	m_bExtraStageForcePreferredGroup = false;
	m_bExtraStage2ForcePreferredGroup = false;

	m_bJukeboxNonMenuButtons = false;
	m_bJukeboxGameplay = true;
	m_bRandomModifiersReadsRandomAttacks = true;
	m_bHighScoreSavesBonusScore = true;

	// BPM and Offset
	m_ApplyOffsetToEveryDifficulty	= ASK;

	m_fKeyF9BPMValue				= -.02f;
	m_fKeyF10BPMValue				= +.02f;
	m_fKeyAltBPMDivisor				= 2.f;
	m_fSlowRepeatBPMMultiplier		= 1.f;
	m_fFastRepeatBPMMultiplier		= 10.f;

	m_fKeyF11OffsetValue			= -.01f;
	m_fKeyF12OffsetValue			= +.01f;
	m_fKeyAltOffsetDivisor			= 10.f;
	m_fSlowRepeatOffsetMultiplier	= 10.f;
	m_fFastRepeatOffsetMultiplier	= 50.f;

	m_bLoadThemeSplash = true;
	m_sLastLoadedSplash = "Data/splash.png";

	// StepMania AMX's Extras
	m_iTouchMode = 0;	// TOUCH_DISABLED
	m_bDisableSpecialKeys = true;
	m_bPersistentInput = false;
	m_bPIUIO = false;

	// Renderers
	m_bEnableOpenGLRenderer = true;
	m_bEnableDirect3DRenderer = true;
	m_bEnableNullRenderer = false;

	// Updates
	m_uUpdateCheckInterval = 259200;	// 3 days
	m_uLastUpdateCheck = 0;

#if defined( WITH_COIN_MODE )
	//
	// Coin Mode
	//
	m_iCoinsPerCredit = 3;
	m_CoinMode = COIN_PAY;
	m_Premium = DOUBLES_PREMIUM;
#endif

	FOREACH_CONST( IPreference*, *g_pvpSubscribers, p ) (*p)->LoadDefault();
}

PrefsManager::~PrefsManager()
{
}

void PrefsManager::ReadGlobalPrefsFromDisk()
{
	ReadPrefsFromFile( DEFAULTS_INI_PATH );
	ReadPrefsFromFile( STEPMANIA_INI_PATH );
	ReadPrefsFromFile( STATIC_INI_PATH );
}

void PrefsManager::ResetToFactoryDefaults(bool bInit)
{
	// clobber the users prefs by initing then applying defaults
	if (bInit)
		Init();
	ReadPrefsFromFile(DEFAULTS_INI_PATH);
	ReadPrefsFromFile(STATIC_INI_PATH);
}

void PrefsManager::ReadPrefsFromFile( CString sIni )
{
	IniFile ini;
	if( !ini.ReadFile(sIni) )
		return;

	ini.GetValue( "Options", "Windowed",					m_bWindowed );
	ini.GetValue( "Options", "Interlaced",					m_bInterlaced );
	ini.GetValue( "Options", "PAL",						m_bPAL );
	ini.GetValue( "Options", "CelShadeModels",				m_bCelShadeModels );
	ini.GetValue( "Options", "DisplayWidth",				m_iDisplayWidth );
	ini.GetValue( "Options", "DisplayHeight",				m_iDisplayHeight );
	ini.GetValue( "Options", "DisplayColorDepth",				m_iDisplayColorDepth );
	ini.GetValue( "Options", "TextureColorDepth",				m_iTextureColorDepth );
	ini.GetValue( "Options", "MovieColorDepth",				m_iMovieColorDepth );
	ini.GetValue( "Options", "MaxTextureResolution",			m_iMaxTextureResolution );
	ini.GetValue( "Options", "RefreshRate",					m_iRefreshRate );
	ini.GetValue( "Options", "UseDedicatedMenuButtons",			m_bOnlyDedicatedMenuButtons );
	ini.GetValue( "Options", "ShowStats",					m_bShowStats );

#ifdef WITH_CATALOG_XML
	ini.GetValue( "Options", "UseCatalogXML",				m_bCatalogXML );
#endif

	ini.GetValue( "Options", "ShowSSMBanners",				m_bShowBanners );
	ini.GetValue( "Options", "ShowSSMDiscs",				m_bShowDiscs );
	ini.GetValue( "Options", "ShowSSMBackgrounds",			m_bShowBackgrounds);
	ini.GetValue( "Options", "ShowSSMPreviews",				m_bShowPreviews );
	ini.GetValue( "Options", "ShowSSMCDTitles",				m_bShowCDTitles );

	ini.GetValue( "Options", "UseSSMCachedBannersOnly",		m_bBannerUsesCacheOnly );
	ini.GetValue( "Options", "UseSSMCachedDiscsOnly",		m_bDiscUsesCacheOnly );
	ini.GetValue( "Options", "UseSSMCachedBackgroundsOnly",	m_bBackgroundUsesCacheOnly);
	ini.GetValue( "Options", "UseSSMCachedPreviewsOnly",	m_bPreviewUsesCacheOnly );

	ini.GetValue( "Options", "BackgroundMode",				(int&)m_BackgroundMode );
	ini.GetValue( "Options", "NumBackgrounds",				m_iNumBackgrounds);
	ini.GetValue( "Options", "ShowDanger",					m_bShowDanger );
	ini.GetValue( "Options", "BGBrightness",				m_fBGBrightness );
	ini.GetValue( "Options", "MenuTimer",					m_bMenuTimer );
	ini.GetValue( "Options", "EventMode",					m_bEventMode );
	ini.GetValue( "Options", "EventModeIgnoresSelectable",			m_bEventIgnoreSelectable );
	ini.GetValue( "Options", "EventModeIgnoresUnlock",			m_bEventIgnoreUnlock );
	ini.GetValue( "Options", "AutoPlay",					m_bAutoPlay );
	ini.GetValue( "Options", "RollTapTimingIsAffectedByJudge",		m_bRollTapTimingIsAffectedByJudge );
	ini.GetValue( "Options", "JudgeWindowScale",					m_fJudgeWindowScale );
	ini.GetValue( "Options", "JudgeWindowAdd",						m_fJudgeWindowAdd );
	ini.GetValue( "Options", "JudgeWindowScaleAfter",				m_fJudgeWindowScaleAfter );
	ini.GetValue( "Options", "JudgeWindowAddAfter",					m_fJudgeWindowAddAfter );
	ini.GetValue( "Options", "JudgeWindowSecondsMarvelous",			m_fJudgeWindowSecondsMarvelous );
	ini.GetValue( "Options", "JudgeWindowSecondsPerfect",			m_fJudgeWindowSecondsPerfect );
	ini.GetValue( "Options", "JudgeWindowSecondsGreat",				m_fJudgeWindowSecondsGreat );
	ini.GetValue( "Options", "JudgeWindowSecondsGood",				m_fJudgeWindowSecondsGood );
	ini.GetValue( "Options", "JudgeWindowSecondsBoo",				m_fJudgeWindowSecondsBoo );
	ini.GetValue( "Options", "JudgeWindowSecondsHidden",			m_fJudgeWindowSecondsHidden );
	ini.GetValue( "Options", "JudgeWindowSecondsHold",				m_fJudgeWindowSecondsHold );
	ini.GetValue( "Options", "JudgeWindowSecondsRoll",				m_fJudgeWindowSecondsRoll );
	ini.GetValue( "Options", "JudgeWindowSecondsLong",				m_fJudgeWindowSecondsLong );
	ini.GetValue( "Options", "JudgeWindowSecondsMine",				m_fJudgeWindowSecondsMine );
	ini.GetValue( "Options", "JudgeWindowSecondsShock",				m_fJudgeWindowSecondsShock );
	ini.GetValue( "Options", "JudgeWindowSecondsPotion",			m_fJudgeWindowSecondsPotion );
	ini.GetValue( "Options", "JudgeWindowSecondsAttack",			m_fJudgeWindowSecondsAttack );
	ini.GetValue( "Options", "JudgeWindowSecondsMarvelousAfter",	m_fJudgeWindowSecondsAfterMarvelous );
	ini.GetValue( "Options", "JudgeWindowSecondsPerfectAfter",		m_fJudgeWindowSecondsAfterPerfect );
	ini.GetValue( "Options", "JudgeWindowSecondsGreatAfter",		m_fJudgeWindowSecondsAfterGreat );
	ini.GetValue( "Options", "JudgeWindowSecondsGoodAfter",			m_fJudgeWindowSecondsAfterGood );
	ini.GetValue( "Options", "JudgeWindowSecondsBooAfter",			m_fJudgeWindowSecondsAfterBoo );
	ini.GetValue( "Options", "JudgeWindowSecondsHiddenAfter",		m_fJudgeWindowSecondsAfterHidden );
	ini.GetValue( "Options", "JudgeWindowSecondsLongAfter",			m_fJudgeWindowSecondsAfterLong );
	ini.GetValue( "Options", "JudgeWindowSecondsMineAfter",			m_fJudgeWindowSecondsAfterMine );
	ini.GetValue( "Options", "JudgeWindowSecondsShockAfter",		m_fJudgeWindowSecondsAfterShock );
	ini.GetValue( "Options", "JudgeWindowSecondsPotionAfter",		m_fJudgeWindowSecondsAfterPotion );
	ini.GetValue( "Options", "JudgeWindowSecondsAttackAfter",		m_fJudgeWindowSecondsAfterAttack );
	ini.GetValue( "Options", "LifeDifficultyScale",					m_fLifeDifficultyScale );

	ini.GetValue( "Options", "PositiveAnnouncerOnly",			m_bPositiveAnnouncerOnly );
	ini.GetValue( "Options", "AnnouncerInExtraStageGameplay",		m_bGameExtraAnnouncer );
	ini.GetValue( "Options", "AnnouncerInExtraStageEvaluation",		m_bEvalExtraAnnouncer );

	ini.GetValue( "Options", "DebounceTime",				m_fDebounceTime );

	ini.GetValue( "Options", "NonstopUsesExtremeScoring",			m_bNonstopUsesExtremeScoring );

	ini.GetValue( "GaugeMeter", "DifficultyScale", m_iGaugeDifficultyScale );

	// Gauge Meter, Normal Play
	ini.GetValue( "GaugeMeter", "TotalValue", m_iGaugeTotalValue );
	FOREACH_TapNoteScoreNoNone( tns ) {
		ini.GetValue( "GaugeMeter", "Change" + TapNoteScoreToString(tns), m_iGaugeChange[tns] );
		ini.GetValue( "GaugeMeter", "FactorChange" + TapNoteScoreToString(tns), m_iGaugeFactorChange[tns] );
	}
	FOREACH_HoldNoteScoreNoNone( hns ) {
		ini.GetValue( "GaugeMeter", "ChangeHold" + HoldNoteScoreToString(hns), m_iGaugeChangeHold[hns] );
		ini.GetValue( "GaugeMeter", "FactorChangeHold" + HoldNoteScoreToString(hns), m_iGaugeFactorChangeHold[hns] );
	}

	// Gauge Meter, No Recover
	ini.GetValue( "GaugeMeter", "NoRecoverTotalValue", m_iGaugeTotalValueNR );
	FOREACH_TapNoteScoreNoNone( tns ) {
		ini.GetValue( "GaugeMeter", "NoRecoverChange" + TapNoteScoreToString(tns), m_iGaugeChangeNR[tns] );
		ini.GetValue( "GaugeMeter", "NoRecoverFactorChange" + TapNoteScoreToString(tns), m_iGaugeFactorChangeNR[tns] );
	}
	FOREACH_HoldNoteScoreNoNone( hns ) {
		ini.GetValue( "GaugeMeter", "NoRecoverChangeHold" + HoldNoteScoreToString(hns), m_iGaugeChangeHoldNR[hns] );
		ini.GetValue( "GaugeMeter", "NoRecoverFactorChangeHold" + HoldNoteScoreToString(hns), m_iGaugeFactorChangeHoldNR[hns] );
	}

	// Gauge Meter, Sudden Death
	ini.GetValue( "GaugeMeter", "SuddenDeathTotalValue", m_iGaugeTotalValueSD );
	FOREACH_TapNoteScoreNoNone( tns ) {
		ini.GetValue( "GaugeMeter", "SuddenDeathChange" + TapNoteScoreToString(tns), m_iGaugeChangeSD[tns] );
		ini.GetValue( "GaugeMeter", "SuddenDeathFactorChange" + TapNoteScoreToString(tns), m_iGaugeFactorChangeSD[tns] );
	}
	FOREACH_HoldNoteScoreNoNone( hns ) {
		ini.GetValue( "GaugeMeter", "SuddenDeathChangeHold" + HoldNoteScoreToString(hns), m_iGaugeChangeHoldSD[hns] );
		ini.GetValue( "GaugeMeter", "SuddenDeathFactorChangeHold" + HoldNoteScoreToString(hns), m_iGaugeFactorChangeHoldSD[hns] );
	}

	// Life Meter, Normal Play
	ini.GetValue( "Options", "LifePercentInitialValue", m_fLifePercentInitialValue );
	FOREACH_TapNoteScoreNoNone( tns )
		ini.GetValue( "Options", "LifeDeltaPercentChange" + TapNoteScoreToString(tns), m_fLifeDeltaPercentChange[tns] );
	FOREACH_HoldNoteScoreNoNone( hns )
		ini.GetValue( "Options", "LifeDeltaPercentChange" + HoldNoteScoreToString(hns), m_fLifeDeltaPercentChangeHold[hns] );

	// Life Meter, No Recover
	ini.GetValue( "Options", "LifePercentNoRecoverInitialValue", m_fLifePercentInitialValueNR );
	FOREACH_TapNoteScoreNoNone( tns )
		ini.GetValue( "Options", "LifeDeltaPercentNoRecoverChange" + TapNoteScoreToString(tns), m_fLifeDeltaPercentChangeNR[tns] );
	FOREACH_HoldNoteScoreNoNone( hns )
		ini.GetValue( "Options", "LifeDeltaPercentNoRecoverChange" + HoldNoteScoreToString(hns), m_fLifeDeltaPercentChangeHoldNR[hns] );

	// Life Meter, Sudden Death
	ini.GetValue( "Options", "LifePercentSuddenDeathInitialValue", m_fLifePercentInitialValueSD );
	FOREACH_TapNoteScoreNoNone( tns )
		ini.GetValue( "Options", "LifeDeltaPercentSuddenDeathChange" + TapNoteScoreToString(tns), m_fLifeDeltaPercentChangeSD[tns] );
	FOREACH_HoldNoteScoreNoNone( hns )
		ini.GetValue( "Options", "LifeDeltaPercentSuddenDeathChange" + HoldNoteScoreToString(hns), m_fLifeDeltaPercentChangeHoldSD[hns] );

	// Tug Meter, used in Rave
	FOREACH_TapNoteScoreNoNone( tns )
		ini.GetValue( "Options", "TugMeterPercentChange" + TapNoteScoreToString(tns), m_fTugMeterPercentChange[tns] );
	FOREACH_HoldNoteScoreNoNone( hns )
		ini.GetValue( "Options", "TugMeterPercentChange" + HoldNoteScoreToString(hns), m_fTugMeterPercentChangeHold[hns] );

	// ScoreKeeperRave
	FOREACH_TapNoteScoreNoNone( tns )
		ini.GetValue( "Options", "SuperMeterPercentChange" + TapNoteScoreToString(tns), m_fSuperMeterPercentChange[tns] );
	FOREACH_HoldNoteScoreNoNone( hns )
		ini.GetValue( "Options", "SuperMeterPercentChange" + HoldNoteScoreToString(hns), m_fSuperMeterPercentChangeHold[hns] );
	ini.GetValue( "Options", "MercifulSuperMeter", m_bMercifulSuperMeter );

	ini.GetValue( "Options", "RegenComboAfterFail",				m_iRegenComboAfterFail );
	ini.GetValue( "Options", "RegenComboAfterMiss",				m_iRegenComboAfterMiss );
	ini.GetValue( "Options", "MaxRegenComboAfterFail",			m_iMaxRegenComboAfterFail );
	ini.GetValue( "Options", "MaxRegenComboAfterMiss",			m_iMaxRegenComboAfterMiss );
	ini.GetValue( "Options", "TwoPlayerRecovery",				m_bTwoPlayerRecovery );
	ini.GetValue( "Options", "MercifulDrain",				m_bMercifulDrain );
	ini.GetValue( "Options", "Minimum1FullSongInCourses",			m_bMinimum1FullSongInCourses );

	ini.GetValue( "Options", "FadeBackgroundsOnTransition",			m_bFadeVideoBackgrounds );

	ini.GetValue( "Options", "PlayAttackSounds",			m_bPlayAttackSounds );
	ini.GetValue( "Options", "PlayMineSound",				m_bPlayMineSound );
	ini.GetValue( "Options", "PlayShockSound",				m_bPlayShockSound );
	ini.GetValue( "Options", "PlayPotionSound",				m_bPlayPotionSound );
	ini.GetValue( "Options", "RandomAttacksLength",			m_fRandomAttackLength );
	ini.GetValue( "Options", "TimeBetweenRandomAttacks",	m_fTimeBetweenRandomAttacks );
	ini.GetValue( "Options", "AttackMinesLength",			m_fAttackMinesLength );

	ini.GetValue( "Options", "ShuffleNotesInDanceRave",			m_bDanceRaveShufflesNotes );

	// Percent Score
	FOREACH_TapNoteScoreNoNone( tns )
		ini.GetValue( "Options", "PercentScoreWeight" + TapNoteScoreToString(tns), m_iPercentScoreWeight[tns] );
	FOREACH_HoldNoteScoreNoNone( hns )
		ini.GetValue( "Options", "PercentScoreWeight" + HoldNoteScoreToString(hns), m_iPercentScoreWeightHold[hns] );

	// Grade Weight
	//ini.GetValue( "Options", "GradeMode", m_GradeMode );
	FOREACH_TapNoteScoreNoNone( tns )
		ini.GetValue( "Options", "GradeWeight" + TapNoteScoreToString(tns), m_iGradeWeight[tns] );
	FOREACH_HoldNoteScoreNoNone( hns )
		ini.GetValue( "Options", "GradeWeight" + HoldNoteScoreToString(hns), m_iGradeWeightHold[hns] );

	ini.GetValue( "Options", "NumGradeTiersUsed",	m_iNumGradeTiersUsed );
	for( int g=0; g<NUM_GRADE_TIERS; g++ )
	{
		Grade grade = (Grade)g;
		CString s = GradeToString( grade );
		ini.GetValue( "Options", "GradePercent"+s,	m_fGradePercent[g] );
	}

	ini.GetValue( "Options", "GradeTier02IsAllPerfects",			m_bGradeTier02IsAllPerfects );
	ini.GetValue( "Options", "GradeTier02RequiresNoMiss",			m_bGradeTier02RequiresNoMiss );
	ini.GetValue( "Options", "GradeTier02RequiresFC",			m_bGradeTier02RequiresFC );
	ini.GetValue( "Options", "GradeTier03RequiresNoMiss",			m_bGradeTier03RequiresNoMiss );
	ini.GetValue( "Options", "GradeTier03RequiresFC",			m_bGradeTier03RequiresFC );

	ini.GetValue( "Options", "DelayedEscape",				m_bDelayedEscape );
	ini.GetValue( "Options", "HiddenSongs",					m_bHiddenSongs );
	ini.GetValue( "Options", "Vsync",					m_bVsync );
	ini.GetValue( "Options", "HowToPlay",					m_bInstructions );
	ini.GetValue( "Options", "Caution",					m_bShowDontDie );
	ini.GetValue( "Options", "ShowSelectGroup",				m_bShowSelectGroup );
	ini.GetValue( "Options", "ShowNative",					m_bShowNative );
	ini.GetValue( "Options", "ArcadeOptionsNavigation",			m_bArcadeOptionsNavigation );
	ini.GetValue( "Options", "DelayedTextureDelete",			m_bDelayedTextureDelete );
	ini.GetValue( "Options", "TexturePreload",				m_bTexturePreload );
	ini.GetValue( "Options", "DelayedScreenLoad",				m_bDelayedScreenLoad );
	ini.GetValue( "Options", "DelayedModelDelete",				m_bDelayedModelDelete );
	ini.GetValue( "Options", "BannerCache",					(int&)m_BannerCache );
	ini.GetValue( "Options", "PalettedBannerCache",				m_bPalettedBannerCache );
	ini.GetValue( "Options", "BackgroundCache",				(int&)m_BackgroundCache );
	ini.GetValue( "Options", "PalettedBackgroundCache",			m_bPalettedBackgroundCache );
	ini.GetValue( "Options", "FastLoad",					m_bFastLoad );
	ini.GetValue( "Options", "FastLoadExisitingCacheOnly",			m_bFastLoadExistingCacheOnly );
	ini.GetValue( "Options", "MusicWheelUsesSections",			(int&)m_MusicWheelUsesSections );
	ini.GetValue( "Options", "MusicWheelSwitchSpeed",			m_iMusicWheelSwitchSpeed );
	ini.GetValue( "Options", "SoundDrivers",				m_sSoundDrivers );
	ini.GetValue( "Options", "SoundWriteAhead",				m_iSoundWriteAhead );
	ini.GetValue( "Options", "SoundDevice",					m_iSoundDevice );
	ini.GetValue( "Options", "MovieDrivers",				m_sMovieDrivers );
	ini.GetValue( "Options", "EasterEggs",					m_bEasterEggs );
	ini.GetValue( "Options", "MarvelousTiming",				(int&)m_iMarvelousTiming );
	ini.GetValue( "Options", "SoundVolume",					m_fSoundVolume );
	ini.GetValue( "Options", "LightsDriver",				m_sLightsDriver );
	ini.GetValue( "Options", "SoundResampleQuality",			m_iSoundResampleQuality );
	ini.GetValue( "Options", "DelayedCreditsReconcile",			m_bDelayedCreditsReconcile );
	ini.GetValue( "Options", "BoostAppPriority",				m_iBoostAppPriority );

	ini.GetValue( "Options", "LockExtraStageDifficulty",			m_bLockExtraStageDiff );
	ini.GetValue( "Options", "PickExtraStage",				m_bPickExtraStage );
	ini.GetValue( "Options", "AlwaysAllowExtraStage2",			m_bAlwaysAllowExtraStage2 );
	ini.GetValue( "Options", "PickModsForExtraStage",			m_bPickModsForExtraStage );
	ini.GetValue( "Options", "DarkExtraStage",				m_bDarkExtraStage );
	ini.GetValue( "Options", "OniExtraStage1",				m_bOniExtraStage1 );
	ini.GetValue( "Options", "OniExtraStage2",				m_bOniExtraStage2 );

	ini.GetValue( "Options", "ComboContinuesBetweenSongs",		m_bComboContinuesBetweenSongs );
	ini.GetValue( "Options", "ShowSongOptions",					(int&)m_ShowSongOptions );
	ini.GetValue( "Options", "LightsStepsDifficulty",			m_sLightsStepsDifficulty );
	ini.GetValue( "Options", "BlinkGameplayButtonLightsOnNote",	m_bBlinkGameplayButtonLightsOnNote );

	//CHANGE: Add lights stuff - Mark
	ini.GetValue( "Options", "LightsFalloffSeconds",			m_fLightsFalloffSeconds );
	ini.GetValue( "Options", "LightsFalloffUsesBPM",			m_bLightsFalloffUsesBPM );
	ini.GetValue( "Options", "LightsIgnoreHolds",				m_iLightsIgnoreHolds );

	ini.GetValue( "Options", "ThreadedInput",				m_bThreadedInput );
	ini.GetValue( "Options", "ThreadedMovieDecode",				m_bThreadedMovieDecode );
	ini.GetValue( "Options", "MachineName",					m_sMachineName );
	ini.GetValue( "Options", "IgnoredMessageWindows",			m_sIgnoredMessageWindows );
	ini.GetValue( "Options", "SoloSingle",					m_bSoloSingle );
	ini.GetValue( "Options", "DancePointsForOni",				m_bDancePointsForOni );
	ini.GetValue( "Options", "PercentageScoring",				m_bPercentageScoring );
	ini.GetValue( "Options", "MinPercentageForMachineSongHighScore",	m_fMinPercentageForMachineSongHighScore );
	ini.GetValue( "Options", "MinPercentageForMachineCourseHighScore",	m_fMinPercentageForMachineCourseHighScore );
	ini.GetValue( "Options", "Disqualification",				m_bDisqualification );
	ini.GetValue( "Options", "ShowLyrics",					m_bShowLyrics );
	ini.GetValue( "Options", "AutogenSteps",				m_bAutogenSteps );
	ini.GetValue( "Options", "AutogenGroupCourses",				m_bAutogenGroupCourses );
	ini.GetValue( "Options", "BreakComboToGetItem",				m_bBreakComboToGetItem );
	ini.GetValue( "Options", "LockCourseDifficulties",			m_bLockCourseDifficulties );
	ini.GetValue( "Options", "ShowDancingCharacters",			(int&)m_ShowDancingCharacters );

	ini.GetValue( "Options", "CourseSortOrder",				(int&)m_iCourseSortOrder );
	ini.GetValue( "Options", "MoveRandomToEnd",				m_bMoveRandomToEnd );
	ini.GetValue( "Options", "SubSortByNumSteps",				m_bSubSortByNumSteps );

	ini.GetValue( "Options", "ScoringType",					(int&)m_iScoringType );

	ini.GetValue( "Options", "ProgressiveLifebar",				m_iProgressiveLifebar );
	ini.GetValue( "Options", "ProgressiveNonstopLifebar", 			m_iProgressiveNonstopLifebar );
	ini.GetValue( "Options", "ProgressiveStageLifebar",			m_iProgressiveStageLifebar );

	ini.GetValue( "Options", "UseUnlockSystem",				m_bUseUnlockSystem );
	ini.GetValue( "Options", "UnlockUsesMachineProfileStats",		m_bUnlockUsesMachineProfileStats );
	ini.GetValue( "Options", "UnlockUsesPlayerProfileStats",		m_bUnlockUsesPlayerProfileStats );

	ini.GetValue( "Options", "FirstRun",					m_bFirstRun );
	ini.GetValue( "Options", "AutoMapJoysticks",				m_bAutoMapOnJoyChange );
	ini.GetValue( "Options", "LastSeenInputDevices",			m_sLastSeenInputDevices );

#if defined(WIN32)
	ini.GetValue( "Options", "LastSeenMemory",	m_uLastSeenMemory );
#endif
#if defined(HAVE_VERSION_INFO)
	ini.GetValue( "Update", "LastCommit",	m_sLastCommit );
#endif

	ini.GetValue( "Options", "CoursesToShowRanking",			m_sCoursesToShowRanking );
	ini.GetValue( "Options", "GetRankingName",				(int&)m_iGetRankingName);
	ini.GetValue( "Options", "SmoothLines",					m_bSmoothLines );
	ini.GetValue( "Options", "GlobalOffsetSeconds",				m_fGlobalOffsetSeconds );
	ini.GetValue( "Options", "ShowBeginnerHelper",				m_bShowBeginnerHelper );
	ini.GetValue( "Options", "Language",					m_sLanguage );
	ini.GetValue( "Options", "EndlessBreakEnabled",				m_bEndlessBreakEnabled );
	ini.GetValue( "Options", "EndlessStagesUntilBreak",			m_iEndlessNumStagesUntilBreak );
	ini.GetValue( "Options", "EndlessBreakLength",				m_iEndlessBreakLength );
	ini.GetValue( "Options", "DisableScreenSaver",				m_bDisableScreenSaver );

	ini.GetValue( "Options", "MemoryCardProfileSubdir",			m_sMemoryCardProfileSubdir );
	ini.GetValue( "Options", "ProductID",					m_iProductID );
	ini.GetValue( "Options", "MemoryCards",					m_bMemoryCards );
	FOREACH_PlayerNumber( p )
	{
		ini.GetValue( "Options", ssprintf("DefaultLocalProfileIDP%d",p+1),	m_sDefaultLocalProfileID[p] );
		ini.GetValue( "Options", ssprintf("MemoryCardOsMountPointP%d",p+1),	m_sMemoryCardOsMountPoint[p] );
		FixSlashesInPlace( m_sMemoryCardOsMountPoint[p] );
		ini.GetValue( "Options", ssprintf("MemoryCardUsbBusP%d",p+1),		m_iMemoryCardUsbBus[p] );
		ini.GetValue( "Options", ssprintf("MemoryCardUsbPortP%d",p+1),		m_iMemoryCardUsbPort[p] );
		ini.GetValue( "Options", ssprintf("MemoryCardUsbLevelP%d",p+1),		m_iMemoryCardUsbLevel[p] );
	}

	// Player Songs!
	ini.GetValue( "Options", "PlayerSongs",						m_bPlayerSongs );
	ini.GetValue( "Options", "PlayerSongsUseBanners",			m_bPlayerSongsAllowBanners );
	ini.GetValue( "Options", "PlayerSongsUseDiscs",				m_bPlayerSongsAllowDiscs );
	ini.GetValue( "Options", "PlayerSongsUseBackgrounds",		m_bPlayerSongsAllowBackgrounds );
	ini.GetValue( "Options", "PlayerSongsUsePreviews",			m_bPlayerSongsAllowPreviews );
	ini.GetValue( "Options", "PlayerSongsUseCDTitles",			m_bPlayerSongsAllowCDTitles );
	ini.GetValue( "Options", "PlayerSongsUseLyrics",			m_bPlayerSongsAllowLyrics );
	ini.GetValue( "Options", "PlayerSongsUseBGChanges",			m_bPlayerSongsAllowBGChanges );
	ini.GetValue( "Options", "PlayerSongsLoadTimeout",			m_fPlayerSongsLoadTimeout );
	ini.GetValue( "Options", "PlayerSongsLengthLimitInSeconds",	m_fPlayerSongsLengthLimitSeconds );
	ini.GetValue( "Options", "PlayerSongsLoadLimit",			m_iPlayerSongsLoadLimit );

	ini.GetValue( "Options", "CenterImageTranslateX",			m_iCenterImageTranslateX );
	ini.GetValue( "Options", "CenterImageTranslateY",			m_iCenterImageTranslateY );
	ini.GetValue( "Options", "CenterImageScaleX",				m_fCenterImageScaleX );
	ini.GetValue( "Options", "CenterImageScaleY",				m_fCenterImageScaleY );
	ini.GetValue( "Options", "AttractSoundFrequency",			m_iAttractSoundFrequency );
	ini.GetValue( "Options", "AllowExtraStage",				m_bAllowExtraStage );
	ini.GetValue( "Options", "HideDefaultNoteSkin",				m_bHideDefaultNoteSkin );
	ini.GetValue( "Options", "MaxHighScoresPerListForMachine",		m_iMaxHighScoresPerListForMachine );
	ini.GetValue( "Options", "MaxHighScoresPerListForPlayer",		m_iMaxHighScoresPerListForPlayer );
	ini.GetValue( "Options", "PadStickSeconds",				m_fPadStickSeconds );
	ini.GetValue( "Options", "ForceMipMaps",				m_bForceMipMaps );
	ini.GetValue( "Options", "TrilinearFiltering",				m_bTrilinearFiltering );
	ini.GetValue( "Options", "AnisotropicFiltering",			m_bAnisotropicFiltering );
	ini.GetValue( "Options", "AutoRestart",					g_bAutoRestart );
	ini.GetValue( "Options", "SignProfileData",				m_bSignProfileData );

	ini.GetValue( "Editor", "ShowBGChangesPlay",				m_bEditorShowBGChangesPlay );
	ini.GetValue( "Editor",	"ShiftIsUsedAsAreaSelector",			m_bEditShiftSelector );

	ini.GetValue( "Options", "AdditionalSongFolders",			m_sAdditionalSongFolders );
	ini.GetValue( "Options", "AdditionalFolders",				m_sAdditionalFolders );
	FixSlashesInPlace(m_sAdditionalSongFolders);
	FixSlashesInPlace(m_sAdditionalFolders);

	ini.GetValue( "Debug",	"LogToDisk",			m_bLogToDisk );
	ini.GetValue( "Debug",	"ForceLogFlush",		m_bForceLogFlush );
	ini.GetValue( "Debug",	"ShowLogOutput",		m_bShowLogOutput );
	ini.GetValue( "Debug",	"Timestamping",			m_bTimestamping );
	ini.GetValue( "Debug",	"LogSkips",				m_bLogSkips );
	ini.GetValue( "Debug",	"LogCheckpoints",		m_bLogCheckpoints );
	ini.GetValue( "Debug",	"ShowLoadingWindow",	m_bShowLoadingWindow );

	// StepMania AMX
	ini.GetValue( "Options",	"ChangeSpeedWithNumberKeys",	m_bChangeSpeedWithNumKeys );
	ini.GetValue( "Options",	"StoreSpeedWithNumberKeys",		m_bStoreSpeedWithNumKeys );
	ini.GetValue( "Options",	"AutoPlayCombo",				m_bAutoPlayCombo );
	ini.GetValue( "Options",	"StartRandomBGAsWithMusic",		m_bStartRandomBGAsWithMusic );
	ini.GetValue( "Options",	"EndRandomBGAsWithMusic",		m_bEndRandomBGAsWithMusic );
	ini.GetValue( "Options",	"StageFinishesWithMusic",		m_bStageFinishesWithMusic );
	ini.GetValue( "Options",	"MusicRateAffectsBGChanges",	m_bMusicRateAffectsBGChanges );

	ini.GetValue( "Gameplay",	"MissComboIncreasesOnComboBreak",	m_bMissComboIncreasesOnComboBreak );
	ini.GetValue( "Gameplay",	"ComboBreakOnGood",					m_bComboBreakOnGood );
	ini.GetValue( "Gameplay",	"ComboBreakOnMines",				m_bComboBreakOnMines );
	ini.GetValue( "Gameplay",	"ComboBreakOnShocks",				m_bComboBreakOnShocks );
	ini.GetValue( "Gameplay",	"ComboIncreasesOnPotions",			m_bComboIncreasesOnPotions );
	ini.GetValue( "Gameplay",	"ComboIncreasesOnRollHits",			m_bComboIncreasesOnRollHits );
	ini.GetValue( "Gameplay",	"ComboIncreasesOnHiddens",			m_bComboIncreasesOnHiddens );
	ini.GetValue( "Gameplay",	"HoldOKIncreasesCombo",				m_bHoldOKIncreasesCombo );
	ini.GetValue( "Gameplay",	"HoldNGBreaksCombo",				m_bHoldNGBreaksCombo );
	ini.GetValue( "Gameplay",	"UsePIUHolds",						m_bUsePIUHolds );

	ini.GetValue( "Editor",		"ShowAllStyles",				m_bShowAllStyles );
	ini.GetValue( "Editor",		"SelectLastEditedSong",			m_bLastEditedSong );
	ini.GetValue( "Editor",		"SelectLastEditedDifficulty",	m_bLastEditedDifficulty );
	ini.GetValue( "Editor",		"SmoothScrolling",				m_bEditorSmoothScrolling );
	ini.GetValue( "Editor",		"UseGameplayKeysToAddNotes",	m_bGameplayKeysInEditor );
	ini.GetValue( "Editor",		"ShowCombo",					m_bEditorCombo );
	ini.GetValue( "Editor",		"ShowSnapBeats",				m_bShowSnapBeats );
	ini.GetValue( "Editor",		"HighlightEndsAtLastSecond",	m_bSelectLastSecond );
	ini.GetValue( "Editor",		"AutoSaveBeforePlay",		m_bAutoSaveBeforePlay );
	ini.GetValue( "Editor",		"AutoSaveEverySeconds",			m_fAutoSaveEverySeconds );

	ini.GetValue( "NotesWriter",	"TimingAsRows",	m_bTimingAsRows );

	ini.GetValue( "Performance", "GameLoopDelayEnable",			m_bGameLoopDelay );
	ini.GetValue( "Performance", "GameLoopDelayMilliseconds",	m_uGameLoopDelayMilliseconds );

	ini.GetValue( "Options",	"NumArcadeStages",		m_iNumArcadeStages );
	ini.GetValue( "Options",	"MaxSongsToPlay",		m_iMaxSongsToPlay );

	ini.GetValue( "Update",		"CheckInterval",		m_uUpdateCheckInterval );
	ini.GetValue( "Update",		"LastUpdateCheck",		m_uLastUpdateCheck );

	int iStageLengths = 0;
	if( ini.GetValue( "Options",	"NumStageLengths",	iStageLengths ) )
	{
		if( iStageLengths > 0 )
		{
			vector<StageLength> vStageLengths;

			vStageLengths.clear();

			for( int i=0; i<iStageLengths; ++i )
			{
				StageLength sl;
				bool bAllLoaded = true;

				bAllLoaded &= ini.GetValue( "Options",	ssprintf("StageLength%dStagesPerSong", i+1),	sl.m_iStagesPerSong );
				bAllLoaded &= ini.GetValue( "Options",	ssprintf("StageLength%dEnabled", i+1),			sl.m_bUseSongWithLength );
				bAllLoaded &= ini.GetValue( "Options",	ssprintf("StageLength%dLengthSeconds", i+1),	sl.m_fMusicLengthSeconds );

				if( bAllLoaded )
				{
					vStageLengths.push_back( sl );
					sort( vStageLengths.begin(), vStageLengths.end(), CompareStageLengths );
				}
			}

			iStageLengths = vStageLengths.size();

			if( iStageLengths > 0 )
			{
				m_iStageLengths = iStageLengths;
				m_StageLength = vStageLengths;
			}
		}
	}

	ini.GetValue( "Options",	"DefaultStagesPerSong",		m_iDefaultStagesPerSong );
	ini.GetValue( "Options",	"ProgressiveLifebarBySong",	m_bProgressiveLifebarBySong );

	ini.GetValue( "Options",	"SkipSongsHarderThanInExtraStage2",	m_iSkipSongsHarderThanOMES );
	ini.GetValue( "Options",	"SetPlayerOptionsInExtraStage",		m_bSetPlayerOptionsES );
	ini.GetValue( "Options",	"SetPlayerOptionsInExtraStage2",	m_bSetPlayerOptionsOMES );
	ini.GetValue( "Options",	"SetSongOptionsInExtraStage",		m_bSetSongOptionsES );
	ini.GetValue( "Options",	"SetSongOptionsInExtraStage2",		m_bSetSongOptionsOMES );

	ini.GetValue( "Options",	"ExtraStageChecksDifficulty",			m_bCheckDifficultyES );
	ini.GetValue( "Options",	"ExtraStage2ChecksDifficulty",			m_bCheckDifficultyOMES );
	ini.GetValue( "Options",	"ExtraStageChecksDifficultyAllStages",	m_bCheckDifficultyAllStagesES );
	ini.GetValue( "Options",	"ExtraStage2ChecksDifficultyAllStages",	m_bCheckDifficultyAllStagesOMES );

	{
		CString diff;
		if( ini.GetValue( "Options",	"ExtraStageMinDifficulty",	diff ) )
			m_MinDifficultyES = StringToDifficulty( diff );

		if( ini.GetValue( "Options",	"ExtraStage2MinDifficulty",	diff ) )
			m_MinDifficultyOMES = StringToDifficulty( diff );

		if( ini.GetValue( "Options",	"ExtraStageMaxDifficulty",	diff ) )
			m_MaxDifficultyES = StringToDifficulty( diff );

		if( ini.GetValue( "Options",	"ExtraStage2MaxDifficulty",	diff ) )
			m_MaxDifficultyOMES = StringToDifficulty( diff );
	}

	ini.GetValue( "Options",	"ExtraStageChecksGrade",			m_bCheckGradeES );
	ini.GetValue( "Options",	"ExtraStage2ChecksGrade",			m_bCheckGradeOMES );
	ini.GetValue( "Options",	"ExtraStageChecksGradeAllStages",	m_bCheckGradeAllStagesES );
	ini.GetValue( "Options",	"ExtraStage2ChecksGradeAllStages",	m_bCheckGradeAllStagesOMES );

	{
		CString grade;
		if( ini.GetValue( "Options",	"ExtraStageMinGrade",	grade ) )
			m_MinGradeES = StringToGrade( grade );
		if( ini.GetValue( "Options",	"ExtraStage2MinGrade",	grade ) )
			m_MinGradeOMES = StringToGrade( grade );
	}

	ini.GetValue( "Options",	"ExtraStageChecksNumberOfPlayedSongs",	m_bCheckSongsPlayedES );
	ini.GetValue( "Options",	"ExtraStage2ChecksNumberOfPlayedSongs",	m_bCheckSongsPlayedOMES );
	ini.GetValue( "Options",	"ExtraStageMinPlayedSongs",				m_iMinSongsPlayedES );
	ini.GetValue( "Options",	"ExtraStage2MinPlayedSongs",			m_iMinSongsPlayedOMES );
	ini.GetValue( "Options",	"ExtraStageMaxPlayedSongs",				m_iMaxSongsPlayedES );
	ini.GetValue( "Options",	"ExtraStage2MaxPlayedSongs",			m_iMaxSongsPlayedOMES );

	ini.GetValue( "Options",	"AllowExtraStage2",				m_bAllowExtraStage2 );
	ini.GetValue( "Options",	"LockExtraStage2Difficulty",	m_bLockExtraStage2Diff );
	ini.GetValue( "Options",	"PickExtraStage2",				m_bPickExtraStage2 );
	ini.GetValue( "Options",	"PickModsForExtraStage2",		m_bPickModsForExtraStage2 );
	ini.GetValue( "Options",	"DarkExtraStage2",				m_bDarkExtraStage2 );

	ini.GetValue( "Options",	"ExtraStageForcePreferredGroup",	m_bExtraStageForcePreferredGroup );
	ini.GetValue( "Options",	"ExtraStage2ForcePreferredGroup",	m_bExtraStage2ForcePreferredGroup );

	ini.GetValue( "Options",	"JukeboxNonMenuButtons",				m_bJukeboxNonMenuButtons );
	ini.GetValue( "Options",	"JukeboxGameplay",						m_bJukeboxGameplay );
	ini.GetValue( "Options",	"RandomModifiersReadsRandomAttacks",	m_bRandomModifiersReadsRandomAttacks );
	ini.GetValue( "Options",	"HighScoreSavesBonusScore",				m_bHighScoreSavesBonusScore );

	ini.GetValue( "ScreenGameplay",	"ApplyOffsetToEveryDifficulty",	(int&)m_ApplyOffsetToEveryDifficulty );

	ini.GetValue( "ScreenGameplay",	"KeyF9BPMValue",				m_fKeyF9BPMValue );
	ini.GetValue( "ScreenGameplay",	"KeyF10BPMValue",				m_fKeyF10BPMValue );
	ini.GetValue( "ScreenGameplay",	"KeyAltBPMDivisor",				m_fKeyAltBPMDivisor );
	ini.GetValue( "ScreenGameplay",	"SlowRepeatBPMMultiplier",		m_fSlowRepeatBPMMultiplier );
	ini.GetValue( "ScreenGameplay",	"FastRepeatBPMMultiplier",		m_fFastRepeatBPMMultiplier );

	ini.GetValue( "ScreenGameplay",	"KeyF11OffsetValue",			m_fKeyF11OffsetValue );
	ini.GetValue( "ScreenGameplay",	"KeyF12OffsetValue",			m_fKeyF12OffsetValue );
	ini.GetValue( "ScreenGameplay",	"KeyAltOffsetDivisor",			m_fKeyAltOffsetDivisor );
	ini.GetValue( "ScreenGameplay",	"SlowRepeatOffsetMultiplier",	m_fSlowRepeatOffsetMultiplier );
	ini.GetValue( "ScreenGameplay",	"FastRepeatOffsetMultiplier",	m_fFastRepeatOffsetMultiplier );

	ini.GetValue( "LoadingWindow",	"LoadThemeSplash",	m_bLoadThemeSplash );
	ini.GetValue( "LoadingWindow",	"LastLoadedSplash",	m_sLastLoadedSplash );

	ini.GetValue( "EasterEggs", "TouchMode",			m_iTouchMode );
	ini.GetValue( "EasterEggs", "DisableSpecialKeys",	m_bDisableSpecialKeys );
	ini.GetValue( "EasterEggs", "PersistentInput",		m_bPersistentInput );

	ini.GetValue( "Renderer", "EnableOpenGLRenderer",	m_bEnableOpenGLRenderer );
	ini.GetValue( "Renderer", "EnableDirect3DRenderer",	m_bEnableDirect3DRenderer );
	ini.GetValue( "Renderer", "EnableNullRenderer",		m_bEnableNullRenderer );

#if defined( WITH_COIN_MODE )
	//
	// Coin Mode
	//
	ini.GetValue( "Options", "CoinsPerCredit",	m_iCoinsPerCredit );
	m_iCoinsPerCredit = max(m_iCoinsPerCredit, 1);
	ini.GetValue( "Options", "CoinMode",		(int&)m_CoinMode );
	ini.GetValue( "Options", "Premium",			(int&)m_Premium );
#endif
	ini.GetValue( "Options", "PIUIO", m_bPIUIO );

	FOREACH( IPreference*, *g_pvpSubscribers, p ) (*p)->ReadFrom( ini );
}

void PrefsManager::SaveGlobalPrefsToDisk() const
{
	IniFile ini;

	ini.SetValue( "Options", "Windowed",					m_bWindowed );
	ini.SetValue( "Options", "CelShadeModels",				m_bCelShadeModels );
	ini.SetValue( "Options", "DisplayWidth",				m_iDisplayWidth );
	ini.SetValue( "Options", "DisplayHeight",				m_iDisplayHeight );
	ini.SetValue( "Options", "DisplayColorDepth",				m_iDisplayColorDepth );
	ini.SetValue( "Options", "TextureColorDepth",				m_iTextureColorDepth );
	ini.SetValue( "Options", "MovieColorDepth",				m_iMovieColorDepth );
	ini.SetValue( "Options", "MaxTextureResolution",			m_iMaxTextureResolution );
	ini.SetValue( "Options", "RefreshRate",					m_iRefreshRate );
	ini.SetValue( "Options", "UseDedicatedMenuButtons",			m_bOnlyDedicatedMenuButtons );
	ini.SetValue( "Options", "ShowStats",					m_bShowStats );

#ifdef WITH_CATALOG_XML
	ini.SetValue( "Options", "UseCatalogXML",				m_bCatalogXML );
#endif

	ini.SetValue( "Options", "ShowSSMBanners",				m_bShowBanners );
	ini.SetValue( "Options", "ShowSSMDiscs",				m_bShowDiscs );
	ini.SetValue( "Options", "ShowSSMBackgrounds",			m_bShowBackgrounds);
	ini.SetValue( "Options", "ShowSSMPreviews",				m_bShowPreviews );
	ini.SetValue( "Options", "ShowSSMCDTitles",				m_bShowCDTitles );

	ini.SetValue( "Options", "UseSSMCachedBannersOnly",		m_bBannerUsesCacheOnly );
	ini.SetValue( "Options", "UseSSMCachedDiscsOnly",		m_bDiscUsesCacheOnly );
	ini.SetValue( "Options", "UseSSMCachedBackgroundsOnly",	m_bBackgroundUsesCacheOnly);
	ini.SetValue( "Options", "UseSSMCachedPreviewsOnly",	m_bPreviewUsesCacheOnly );

	ini.SetValue( "Options", "BackgroundMode",				m_BackgroundMode);
	ini.SetValue( "Options", "NumBackgrounds",				m_iNumBackgrounds);
	ini.SetValue( "Options", "ShowDanger",					m_bShowDanger );
	ini.SetValue( "Options", "BGBrightness",				m_fBGBrightness );
	ini.SetValue( "Options", "MenuTimer",					m_bMenuTimer );
	ini.SetValue( "Options", "EventMode",					m_bEventMode );
	ini.SetValue( "Options", "EventModeIgnoresSelectable",			m_bEventIgnoreSelectable );
	ini.SetValue( "Options", "EventModeIgnoresUnlock",			m_bEventIgnoreUnlock );
	ini.SetValue( "Options", "AutoPlay",					m_bAutoPlay );
	ini.SetValue( "Options", "RollTapTimingIsAffectedByJudge",		m_bRollTapTimingIsAffectedByJudge );
	ini.SetValue( "Options", "JudgeWindowScale",					m_fJudgeWindowScale );
	ini.SetValue( "Options", "JudgeWindowAdd",						m_fJudgeWindowAdd );
	ini.SetValue( "Options", "JudgeWindowScaleAfter",				m_fJudgeWindowScaleAfter );
	ini.SetValue( "Options", "JudgeWindowAddAfter",					m_fJudgeWindowAddAfter );
	ini.SetValue( "Options", "JudgeWindowSecondsMarvelous",			m_fJudgeWindowSecondsMarvelous );
	ini.SetValue( "Options", "JudgeWindowSecondsPerfect",			m_fJudgeWindowSecondsPerfect );
	ini.SetValue( "Options", "JudgeWindowSecondsGreat",				m_fJudgeWindowSecondsGreat );
	ini.SetValue( "Options", "JudgeWindowSecondsGood",				m_fJudgeWindowSecondsGood );
	ini.SetValue( "Options", "JudgeWindowSecondsBoo",				m_fJudgeWindowSecondsBoo );
	ini.SetValue( "Options", "JudgeWindowSecondsHidden",			m_fJudgeWindowSecondsHidden );
	ini.SetValue( "Options", "JudgeWindowSecondsHold",				m_fJudgeWindowSecondsHold );
	ini.SetValue( "Options", "JudgeWindowSecondsRoll",				m_fJudgeWindowSecondsRoll );
	ini.SetValue( "Options", "JudgeWindowSecondsLong",				m_fJudgeWindowSecondsLong );
	ini.SetValue( "Options", "JudgeWindowSecondsMine",				m_fJudgeWindowSecondsMine );
	ini.SetValue( "Options", "JudgeWindowSecondsShock",				m_fJudgeWindowSecondsShock );
	ini.SetValue( "Options", "JudgeWindowSecondsPotion",			m_fJudgeWindowSecondsPotion );
	ini.SetValue( "Options", "JudgeWindowSecondsAttack",			m_fJudgeWindowSecondsAttack );
	ini.SetValue( "Options", "JudgeWindowSecondsMarvelousAfter",	m_fJudgeWindowSecondsAfterMarvelous );
	ini.SetValue( "Options", "JudgeWindowSecondsPerfectAfter",		m_fJudgeWindowSecondsAfterPerfect );
	ini.SetValue( "Options", "JudgeWindowSecondsGreatAfter",		m_fJudgeWindowSecondsAfterGreat );
	ini.SetValue( "Options", "JudgeWindowSecondsGoodAfter",			m_fJudgeWindowSecondsAfterGood );
	ini.SetValue( "Options", "JudgeWindowSecondsBooAfter",			m_fJudgeWindowSecondsAfterBoo );
	ini.SetValue( "Options", "JudgeWindowSecondsHiddenAfter",		m_fJudgeWindowSecondsAfterHidden );
	ini.SetValue( "Options", "JudgeWindowSecondsLongAfter",			m_fJudgeWindowSecondsAfterLong );
	ini.SetValue( "Options", "JudgeWindowSecondsMineAfter",			m_fJudgeWindowSecondsAfterMine );
	ini.SetValue( "Options", "JudgeWindowSecondsShockAfter",		m_fJudgeWindowSecondsAfterShock );
	ini.SetValue( "Options", "JudgeWindowSecondsPotionAfter",		m_fJudgeWindowSecondsAfterPotion );
	ini.SetValue( "Options", "JudgeWindowSecondsAttackAfter",		m_fJudgeWindowSecondsAfterAttack );
	ini.SetValue( "Options", "LifeDifficultyScale",					m_fLifeDifficultyScale );

	ini.SetValue( "Options", "PositiveAnnouncerOnly",			m_bPositiveAnnouncerOnly );
	ini.SetValue( "Options", "AnnouncerInExtraStageGameplay",		m_bGameExtraAnnouncer );
	ini.SetValue( "Options", "AnnouncerInExtraStageEvaluation",		m_bEvalExtraAnnouncer );

	ini.SetValue( "Options", "DebounceTime",				m_fDebounceTime );

	ini.SetValue( "Options", "NonstopUsesExtremeScoring",			m_bNonstopUsesExtremeScoring );

	ini.SetValue( "GaugeMeter", "DifficultyScale", m_iGaugeDifficultyScale );

	// Gauge Meter, Normal Play
	ini.SetValue( "GaugeMeter", "TotalValue", m_iGaugeTotalValue );
	FOREACH_TapNoteScoreNoNone( tns ) {
		ini.SetValue( "GaugeMeter", "Change" + TapNoteScoreToString(tns), m_iGaugeChange[tns] );
		ini.SetValue( "GaugeMeter", "FactorChange" + TapNoteScoreToString(tns), m_iGaugeFactorChange[tns] );
	}
	FOREACH_HoldNoteScoreNoNone( hns ) {
		ini.SetValue( "GaugeMeter", "ChangeHold" + HoldNoteScoreToString(hns), m_iGaugeChangeHold[hns] );
		ini.SetValue( "GaugeMeter", "FactorChangeHold" + HoldNoteScoreToString(hns), m_iGaugeFactorChangeHold[hns] );
	}

	// Gauge Meter, No Recover
	ini.SetValue( "GaugeMeter", "NoRecoverTotalValue", m_iGaugeTotalValueNR );
	FOREACH_TapNoteScoreNoNone( tns ) {
		ini.SetValue( "GaugeMeter", "NoRecoverChange" + TapNoteScoreToString(tns), m_iGaugeChangeNR[tns] );
		ini.SetValue( "GaugeMeter", "NoRecoverFactorChange" + TapNoteScoreToString(tns), m_iGaugeFactorChangeNR[tns] );
	}
	FOREACH_HoldNoteScoreNoNone( hns ) {
		ini.SetValue( "GaugeMeter", "NoRecoverChangeHold" + HoldNoteScoreToString(hns), m_iGaugeChangeHoldNR[hns] );
		ini.SetValue( "GaugeMeter", "NoRecoverFactorChangeHold" + HoldNoteScoreToString(hns), m_iGaugeFactorChangeHoldNR[hns] );
	}

	// Gauge Meter, Sudden Death
	ini.SetValue( "GaugeMeter", "SuddenDeathTotalValue", m_iGaugeTotalValueSD );
	FOREACH_TapNoteScoreNoNone( tns ) {
		ini.SetValue( "GaugeMeter", "SuddenDeathChange" + TapNoteScoreToString(tns), m_iGaugeChangeSD[tns] );
		ini.SetValue( "GaugeMeter", "SuddenDeathFactorChange" + TapNoteScoreToString(tns), m_iGaugeFactorChangeSD[tns] );
	}
	FOREACH_HoldNoteScoreNoNone( hns ) {
		ini.SetValue( "GaugeMeter", "SuddenDeathChangeHold" + HoldNoteScoreToString(hns), m_iGaugeChangeHoldSD[hns] );
		ini.SetValue( "GaugeMeter", "SuddenDeathFactorChangeHold" + HoldNoteScoreToString(hns), m_iGaugeFactorChangeHoldSD[hns] );
	}

	// Life Meter, Normal Play
	ini.SetValue( "Options", "LifePercentInitialValue", m_fLifePercentInitialValue );
	FOREACH_TapNoteScoreNoNone( tns )
		ini.SetValue( "Options", "LifeDeltaPercentChange" + TapNoteScoreToString(tns), m_fLifeDeltaPercentChange[tns] );
	FOREACH_HoldNoteScoreNoNone( hns )
		ini.SetValue( "Options", "LifeDeltaPercentChange" + HoldNoteScoreToString(hns), m_fLifeDeltaPercentChangeHold[hns] );

	// Life Meter, No Recover
	ini.SetValue( "Options", "LifePercentNoRecoverInitialValue", m_fLifePercentInitialValueNR );
	FOREACH_TapNoteScoreNoNone( tns )
		ini.SetValue( "Options", "LifeDeltaPercentNoRecoverChange" + TapNoteScoreToString(tns), m_fLifeDeltaPercentChangeNR[tns] );
	FOREACH_HoldNoteScoreNoNone( hns )
		ini.SetValue( "Options", "LifeDeltaPercentNoRecoverChange" + HoldNoteScoreToString(hns), m_fLifeDeltaPercentChangeHoldNR[hns] );

	// Life Meter, Sudden Death
	ini.SetValue( "Options", "LifePercentSuddenDeathInitialValue", m_fLifePercentInitialValueSD );
	FOREACH_TapNoteScoreNoNone( tns )
		ini.SetValue( "Options", "LifeDeltaPercentSuddenDeathChange" + TapNoteScoreToString(tns), m_fLifeDeltaPercentChangeSD[tns] );
	FOREACH_HoldNoteScoreNoNone( hns )
		ini.SetValue( "Options", "LifeDeltaPercentSuddenDeathChange" + HoldNoteScoreToString(hns), m_fLifeDeltaPercentChangeHoldSD[hns] );

	// Tug Meter, used in Rave
	FOREACH_TapNoteScoreNoNone( tns )
		ini.SetValue( "Options", "TugMeterPercentChange" + TapNoteScoreToString(tns), m_fTugMeterPercentChange[tns] );
	FOREACH_HoldNoteScoreNoNone( hns )
		ini.SetValue( "Options", "TugMeterPercentChange" + HoldNoteScoreToString(hns), m_fTugMeterPercentChangeHold[hns] );

	// ScoreKeeperRave
	FOREACH_TapNoteScoreNoNone( tns )
		ini.SetValue( "Options", "SuperMeterPercentChange" + TapNoteScoreToString(tns), m_fSuperMeterPercentChange[tns] );
	FOREACH_HoldNoteScoreNoNone( hns )
		ini.SetValue( "Options", "SuperMeterPercentChange" + HoldNoteScoreToString(hns), m_fSuperMeterPercentChangeHold[hns] );
	ini.SetValue( "Options", "MercifulSuperMeter", m_bMercifulSuperMeter );

	ini.SetValue( "Options", "RegenComboAfterFail",				m_iRegenComboAfterFail );
	ini.SetValue( "Options", "RegenComboAfterMiss",				m_iRegenComboAfterMiss );
	ini.SetValue( "Options", "MaxRegenComboAfterFail",			m_iMaxRegenComboAfterFail );
	ini.SetValue( "Options", "MaxRegenComboAfterMiss",			m_iMaxRegenComboAfterMiss );
	ini.SetValue( "Options", "TwoPlayerRecovery",				m_bTwoPlayerRecovery );
	ini.SetValue( "Options", "MercifulDrain",				m_bMercifulDrain );
	ini.SetValue( "Options", "Minimum1FullSongInCourses",			m_bMinimum1FullSongInCourses );

	ini.SetValue( "Options", "FadeBackgroundsOnTransition",	m_bFadeVideoBackgrounds );

	ini.SetValue( "Options", "PlayAttackSounds",			m_bPlayAttackSounds );
	ini.SetValue( "Options", "PlayMineSound",				m_bPlayMineSound );
	ini.SetValue( "Options", "PlayShockSound",				m_bPlayShockSound );
	ini.SetValue( "Options", "PlayPotionSound",				m_bPlayPotionSound );
	ini.SetValue( "Options", "RandomAttacksLength",			m_fRandomAttackLength );
	ini.SetValue( "Options", "TimeBetweenRandomAttacks",	m_fTimeBetweenRandomAttacks );
	ini.SetValue( "Options", "AttackMinesLength",			m_fAttackMinesLength );

	ini.SetValue( "Options", "ShuffleNotesInDanceRave",	m_bDanceRaveShufflesNotes );

	// Percent Score
	FOREACH_TapNoteScoreNoNone( tns )
		ini.SetValue( "Options", "PercentScoreWeight" + TapNoteScoreToString(tns), m_iPercentScoreWeight[tns] );
	FOREACH_HoldNoteScoreNoNone( hns )
		ini.SetValue( "Options", "PercentScoreWeight" + HoldNoteScoreToString(hns), m_iPercentScoreWeightHold[hns] );

	// Grade Weight
	//ini.SetValue( "Options", "GradeMode", m_GradeMode );
	FOREACH_TapNoteScoreNoNone( tns )
		ini.SetValue( "Options", "GradeWeight" + TapNoteScoreToString(tns), m_iGradeWeight[tns] );
	FOREACH_HoldNoteScoreNoNone( hns )
		ini.SetValue( "Options", "GradeWeight" + HoldNoteScoreToString(hns), m_iGradeWeightHold[hns] );

	ini.SetValue( "Options", "NumGradeTiersUsed",	m_iNumGradeTiersUsed );
	for( int g=0; g<NUM_GRADE_TIERS; g++ )
	{
		Grade grade = (Grade)g;
		CString s = GradeToString( grade );
		ini.SetValue( "Options", "GradePercent"+s,	m_fGradePercent[g] );
	}
	ini.SetValue( "Options", "GradeTier02IsAllPerfects",			m_bGradeTier02IsAllPerfects );
	ini.SetValue( "Options", "GradeTier02RequiresNoMiss",			m_bGradeTier02RequiresNoMiss );
	ini.SetValue( "Options", "GradeTier02RequiresFC",			m_bGradeTier02RequiresFC );
	ini.SetValue( "Options", "GradeTier03RequiresNoMiss",			m_bGradeTier03RequiresNoMiss );
	ini.SetValue( "Options", "GradeTier03RequiresFC",			m_bGradeTier03RequiresFC );

	ini.SetValue( "Options", "DelayedEscape",				m_bDelayedEscape );
	ini.SetValue( "Options", "HiddenSongs",					m_bHiddenSongs );
	ini.SetValue( "Options", "Vsync",					m_bVsync );
	ini.SetValue( "Options", "Interlaced",					m_bInterlaced );
	ini.SetValue( "Options", "PAL",						m_bPAL );
	ini.SetValue( "Options", "HowToPlay",					m_bInstructions );
	ini.SetValue( "Options", "Caution",					m_bShowDontDie );
	ini.SetValue( "Options", "ShowSelectGroup",				m_bShowSelectGroup );
	ini.SetValue( "Options", "ShowNative",					m_bShowNative );
	ini.SetValue( "Options", "ArcadeOptionsNavigation",			m_bArcadeOptionsNavigation );
	ini.SetValue( "Options", "DelayedTextureDelete",			m_bDelayedTextureDelete );
	ini.SetValue( "Options", "TexturePreload",				m_bTexturePreload );
	ini.SetValue( "Options", "DelayedScreenLoad",				m_bDelayedScreenLoad );
	ini.SetValue( "Options", "DelayedModelDelete",				m_bDelayedModelDelete );
	ini.SetValue( "Options", "BannerCache",					m_BannerCache );
	ini.SetValue( "Options", "PalettedBannerCache",				m_bPalettedBannerCache );
	ini.SetValue( "Options", "BackgroundCache",				m_BackgroundCache );
	ini.SetValue( "Options", "PalettedBackgroundCache",			m_bPalettedBackgroundCache );
	ini.SetValue( "Options", "FastLoad",					m_bFastLoad );
	ini.SetValue( "Options", "FastLoadExisitingCacheOnly",			m_bFastLoadExistingCacheOnly );
	ini.SetValue( "Options", "MusicWheelUsesSections",			m_MusicWheelUsesSections );
	ini.SetValue( "Options", "MusicWheelSwitchSpeed",			m_iMusicWheelSwitchSpeed );
	ini.SetValue( "Options", "EasterEggs",					m_bEasterEggs );
	ini.SetValue( "Options", "MarvelousTiming",				m_iMarvelousTiming );
	ini.SetValue( "Options", "SoundResampleQuality",			m_iSoundResampleQuality );
	ini.SetValue( "Options", "DelayedCreditsReconcile",			m_bDelayedCreditsReconcile );
	ini.SetValue( "Options", "BoostAppPriority",				m_iBoostAppPriority );

	ini.SetValue( "Options", "LockExtraStageDifficulty",			m_bLockExtraStageDiff );
	ini.SetValue( "Options", "PickExtraStage",				m_bPickExtraStage );
	ini.SetValue( "Options", "AlwaysAllowExtraStage2",			m_bAlwaysAllowExtraStage2 );
	ini.SetValue( "Options", "PickModsForExtraStage",			m_bPickModsForExtraStage );
	ini.SetValue( "Options", "DarkExtraStage",				m_bDarkExtraStage );
	ini.SetValue( "Options", "OniExtraStage1",				m_bOniExtraStage1 );
	ini.SetValue( "Options", "OniExtraStage2",				m_bOniExtraStage2 );

	ini.SetValue( "Options", "ComboContinuesBetweenSongs",			m_bComboContinuesBetweenSongs );
	ini.SetValue( "Options", "ShowSongOptions",				m_ShowSongOptions );
	ini.SetValue( "Options", "LightsStepsDifficulty",			m_sLightsStepsDifficulty );
//CHANGE: Add lights stuff. - Mark
	ini.SetValue( "Options", "LightsFalloffSeconds",			m_fLightsFalloffSeconds );
	ini.SetValue( "Options", "LightsFalloffUsesBPM",			m_bLightsFalloffUsesBPM );
	ini.SetValue( "Options", "LightsIgnoreHolds",				m_iLightsIgnoreHolds );

	ini.SetValue( "Options", "BlinkGameplayButtonLightsOnNote",		m_bBlinkGameplayButtonLightsOnNote );
	ini.SetValue( "Options", "ThreadedInput",				m_bThreadedInput );
	ini.SetValue( "Options", "ThreadedMovieDecode",				m_bThreadedMovieDecode );
	ini.SetValue( "Options", "MachineName",					m_sMachineName );
	ini.SetValue( "Options", "IgnoredMessageWindows",			m_sIgnoredMessageWindows );
	ini.SetValue( "Options", "SoloSingle",					m_bSoloSingle );
	ini.SetValue( "Options", "DancePointsForOni",				m_bDancePointsForOni );
	ini.SetValue( "Options", "PercentageScoring",				m_bPercentageScoring );
	ini.SetValue( "Options", "MinPercentageForMachineSongHighScore",	m_fMinPercentageForMachineSongHighScore );
	ini.SetValue( "Options", "MinPercentageForMachineCourseHighScore",	m_fMinPercentageForMachineCourseHighScore );
	ini.SetValue( "Options", "Disqualification",				m_bDisqualification );
	ini.SetValue( "Options", "ShowLyrics",					m_bShowLyrics );
	ini.SetValue( "Options", "AutogenSteps",				m_bAutogenSteps );
	ini.SetValue( "Options", "AutogenGroupCourses",				m_bAutogenGroupCourses );
	ini.SetValue( "Options", "BreakComboToGetItem",				m_bBreakComboToGetItem );
	ini.SetValue( "Options", "LockCourseDifficulties",			m_bLockCourseDifficulties );
	ini.SetValue( "Options", "ShowDancingCharacters",			m_ShowDancingCharacters );

	ini.SetValue( "Options", "UseUnlockSystem",				m_bUseUnlockSystem );
	ini.SetValue( "Options", "UnlockUsesMachineProfileStats",		m_bUnlockUsesMachineProfileStats );
	ini.SetValue( "Options", "UnlockUsesPlayerProfileStats",		m_bUnlockUsesPlayerProfileStats );

	ini.SetValue( "Options", "FirstRun",					m_bFirstRun );
	ini.SetValue( "Options", "AutoMapJoysticks",				m_bAutoMapOnJoyChange );
	ini.SetValue( "Options", "LastSeenInputDevices",			m_sLastSeenInputDevices );

#if defined(WIN32)
	ini.SetValue( "Options", "LastSeenMemory",	m_uLastSeenMemory );
#endif
#if defined(HAVE_VERSION_INFO)
	ini.SetValue( "Update", "LastCommit",	m_sLastCommit );
#endif

	ini.SetValue( "Options", "CoursesToShowRanking",			m_sCoursesToShowRanking );
	ini.SetValue( "Options", "GetRankingName",				m_iGetRankingName);
	ini.SetValue( "Options", "SmoothLines",					m_bSmoothLines );
	ini.SetValue( "Options", "GlobalOffsetSeconds",				m_fGlobalOffsetSeconds );

	ini.SetValue( "Options", "CourseSortOrder",				m_iCourseSortOrder );
	ini.SetValue( "Options", "MoveRandomToEnd",				m_bMoveRandomToEnd );
	ini.SetValue( "Options", "SubSortByNumSteps",				m_bSubSortByNumSteps );

	ini.SetValue( "Options", "ScoringType",					m_iScoringType );

	ini.SetValue( "Options", "ProgressiveLifebar",				m_iProgressiveLifebar );
	ini.SetValue( "Options", "ProgressiveStageLifebar",			m_iProgressiveStageLifebar );
	ini.SetValue( "Options", "ProgressiveNonstopLifebar",			m_iProgressiveNonstopLifebar );
	ini.SetValue( "Options", "ShowBeginnerHelper",				m_bShowBeginnerHelper );
	ini.SetValue( "Options", "Language",					m_sLanguage );
	ini.SetValue( "Options", "EndlessBreakEnabled",				m_bEndlessBreakEnabled );
	ini.SetValue( "Options", "EndlessStagesUntilBreak",			m_iEndlessNumStagesUntilBreak );
	ini.SetValue( "Options", "EndlessBreakLength",				m_iEndlessBreakLength );
	ini.SetValue( "Options", "DisableScreenSaver",				m_bDisableScreenSaver );

	ini.SetValue( "Options", "MemoryCardProfileSubdir",			m_sMemoryCardProfileSubdir );
	ini.SetValue( "Options", "ProductID",					m_iProductID );
	ini.SetValue( "Options", "MemoryCards",					m_bMemoryCards );
	FOREACH_PlayerNumber( p )
	{
		ini.SetValue( "Options", ssprintf("DefaultLocalProfileIDP%d",p+1),	m_sDefaultLocalProfileID[p] );

		// For some reason, the mount path in windows gets mucked up. If you specify C:\ in the file, when
		// it saves, it'll save as C:/. Very annoying and can break mount path data! This fixes that.
		if( m_sMemoryCardOsMountPoint[p].Right(1) == "/" )
		{
			CStringArray sHolder;
			CString sTemp;

			split( m_sMemoryCardOsMountPoint[p], "/", sHolder, false );

			for( unsigned j = 0; j < (sHolder.size() - 1); j++ )
			{
				sTemp += sHolder[j];
			}

			sTemp += "\\"; // Remember to escape characters!

			ini.SetValue( "Options", ssprintf("MemoryCardOsMountPointP%d",p+1),	sTemp );
		}
		else
		{
			ini.SetValue( "Options", ssprintf("MemoryCardOsMountPointP%d",p+1),	m_sMemoryCardOsMountPoint[p] );
		}

		ini.SetValue( "Options", ssprintf("MemoryCardUsbBusP%d",p+1),		m_iMemoryCardUsbBus[p] );
		ini.SetValue( "Options", ssprintf("MemoryCardUsbPortP%d",p+1),		m_iMemoryCardUsbPort[p] );
		ini.SetValue( "Options", ssprintf("MemoryCardUsbLevelP%d",p+1),		m_iMemoryCardUsbLevel[p] );
	}

	// Player Songs!
	ini.SetValue( "Options", "PlayerSongs",						m_bPlayerSongs );
	ini.SetValue( "Options", "PlayerSongsUseBanners",			m_bPlayerSongsAllowBanners );
	ini.SetValue( "Options", "PlayerSongsUseDiscs",				m_bPlayerSongsAllowDiscs );
	ini.SetValue( "Options", "PlayerSongsUseBackgrounds",		m_bPlayerSongsAllowBackgrounds );
	ini.SetValue( "Options", "PlayerSongsUsePreviews",			m_bPlayerSongsAllowPreviews );
	ini.SetValue( "Options", "PlayerSongsUseCDTitles",			m_bPlayerSongsAllowCDTitles );
	ini.SetValue( "Options", "PlayerSongsUseLyrics",			m_bPlayerSongsAllowLyrics );
	ini.SetValue( "Options", "PlayerSongsUseBGChanges",			m_bPlayerSongsAllowBGChanges );
	ini.SetValue( "Options", "PlayerSongsLoadTimeout",			m_fPlayerSongsLoadTimeout );
	ini.SetValue( "Options", "PlayerSongsLengthLimitInSeconds",	m_fPlayerSongsLengthLimitSeconds );
	ini.SetValue( "Options", "PlayerSongsLoadLimit",			m_iPlayerSongsLoadLimit );

	ini.SetValue( "Options", "CenterImageTranslateX",			m_iCenterImageTranslateX );
	ini.SetValue( "Options", "CenterImageTranslateY",			m_iCenterImageTranslateY );
	ini.SetValue( "Options", "CenterImageScaleX",				m_fCenterImageScaleX );
	ini.SetValue( "Options", "CenterImageScaleY",				m_fCenterImageScaleY );
	ini.SetValue( "Options", "AttractSoundFrequency",			m_iAttractSoundFrequency );
	ini.SetValue( "Options", "AllowExtraStage",				m_bAllowExtraStage );
	ini.SetValue( "Options", "HideDefaultNoteSkin",				m_bHideDefaultNoteSkin );
	ini.SetValue( "Options", "MaxHighScoresPerListForMachine",		m_iMaxHighScoresPerListForMachine );
	ini.SetValue( "Options", "MaxHighScoresPerListForPlayer",		m_iMaxHighScoresPerListForPlayer );
	ini.SetValue( "Options", "PadStickSeconds",				m_fPadStickSeconds );
	ini.SetValue( "Options", "ForceMipMaps",				m_bForceMipMaps );
	ini.SetValue( "Options", "TrilinearFiltering",				m_bTrilinearFiltering );
	ini.SetValue( "Options", "AnisotropicFiltering",			m_bAnisotropicFiltering );
	ini.SetValue( "Options", "AutoRestart",					g_bAutoRestart );
	ini.SetValue( "Options", "SignProfileData",				m_bSignProfileData );

	ini.SetValue( "Options", "SoundWriteAhead",				m_iSoundWriteAhead );
	ini.SetValue( "Options", "SoundDevice",					m_iSoundDevice );

	ini.SetValue( "Editor", "ShowBGChangesPlay",				m_bEditorShowBGChangesPlay );
	ini.SetValue( "Editor",	"ShiftIsUsedAsAreaSelector",			m_bEditShiftSelector );

	/* Only write these if they aren't the default.  This ensures that we can change
	 * the default and have it take effect for everyone (except people who
	 * tweaked this value). */
	if(m_sSoundDrivers != DEFAULT_SOUND_DRIVER_LIST)
		ini.SetValue ( "Options", "SoundDrivers",			m_sSoundDrivers );
	if(m_fSoundVolume != DEFAULT_SOUND_VOLUME)
		ini.SetValue( "Options", "SoundVolume",				m_fSoundVolume );
	if(m_sLightsDriver != DEFAULT_LIGHTS_DRIVER)
		ini.SetValue( "Options", "LightsDriver",			m_sLightsDriver );
	if(m_sMovieDrivers != DEFAULT_MOVIE_DRIVER_LIST)
		ini.SetValue ( "Options", "MovieDrivers",			m_sMovieDrivers );

	ini.SetValue( "Options", "AdditionalSongFolders", 			m_sAdditionalSongFolders);
	ini.SetValue( "Options", "AdditionalFolders", 				m_sAdditionalFolders);

	ini.SetValue( "Debug",	"LogToDisk",			m_bLogToDisk );
	ini.SetValue( "Debug",	"ForceLogFlush",		m_bForceLogFlush );
	ini.SetValue( "Debug",	"ShowLogOutput",		m_bShowLogOutput );
	ini.SetValue( "Debug",	"Timestamping",			m_bTimestamping );
	ini.SetValue( "Debug",	"LogSkips",				m_bLogSkips );
	ini.SetValue( "Debug",	"LogCheckpoints",		m_bLogCheckpoints );
	ini.SetValue( "Debug",	"ShowLoadingWindow",	m_bShowLoadingWindow );

	// StepMania AMX
	ini.SetValue( "Options",	"ChangeSpeedWithNumberKeys",	m_bChangeSpeedWithNumKeys );
	ini.SetValue( "Options",	"StoreSpeedWithNumberKeys",		m_bStoreSpeedWithNumKeys );
	ini.SetValue( "Options",	"AutoPlayCombo",				m_bAutoPlayCombo );
	ini.SetValue( "Options",	"StartRandomBGAsWithMusic",		m_bStartRandomBGAsWithMusic );
	ini.SetValue( "Options",	"EndRandomBGAsWithMusic",		m_bEndRandomBGAsWithMusic );
	ini.SetValue( "Options",	"StageFinishesWithMusic",		m_bStageFinishesWithMusic );
	ini.SetValue( "Options",	"MusicRateAffectsBGChanges",	m_bMusicRateAffectsBGChanges );

	ini.SetValue( "Gameplay",	"MissComboIncreasesOnComboBreak",	m_bMissComboIncreasesOnComboBreak );
	ini.SetValue( "Gameplay",	"ComboBreakOnGood",					m_bComboBreakOnGood );
	ini.SetValue( "Gameplay",	"ComboBreakOnMines",				m_bComboBreakOnMines );
	ini.SetValue( "Gameplay",	"ComboBreakOnShocks",				m_bComboBreakOnShocks );
	ini.SetValue( "Gameplay",	"ComboIncreasesOnPotions",			m_bComboIncreasesOnPotions );
	ini.SetValue( "Gameplay",	"ComboIncreasesOnRollHits",			m_bComboIncreasesOnRollHits );
	ini.SetValue( "Gameplay",	"ComboIncreasesOnHiddens",			m_bComboIncreasesOnHiddens );
	ini.SetValue( "Gameplay",	"HoldOKIncreasesCombo",				m_bHoldOKIncreasesCombo );
	ini.SetValue( "Gameplay",	"HoldNGBreaksCombo",				m_bHoldNGBreaksCombo );
	ini.SetValue( "Gameplay",	"UsePIUHolds",						m_bUsePIUHolds );

	ini.SetValue( "Editor",		"ShowAllStyles",				m_bShowAllStyles );
	ini.SetValue( "Editor",		"SelectLastEditedSong",			m_bLastEditedSong );
	ini.SetValue( "Editor",		"SelectLastEditedDifficulty",	m_bLastEditedDifficulty );
	ini.SetValue( "Editor",		"SmoothScrolling",				m_bEditorSmoothScrolling );
	ini.SetValue( "Editor",		"UseGameplayKeysToAddNotes",	m_bGameplayKeysInEditor );
	ini.SetValue( "Editor",		"ShowCombo",					m_bEditorCombo );
	ini.SetValue( "Editor",		"ShowSnapBeats",				m_bShowSnapBeats );
	ini.SetValue( "Editor",		"HighlightEndsAtLastSecond",	m_bSelectLastSecond );
	ini.SetValue( "Editor",		"AutoSaveBeforePlay",		m_bAutoSaveBeforePlay );
	ini.SetValue( "Editor",		"AutoSaveEverySeconds",			m_fAutoSaveEverySeconds );

	ini.SetValue( "NotesWriter",	"TimingAsRows",	m_bTimingAsRows );

	ini.SetValue( "Performance", "GameLoopDelayEnable",			m_bGameLoopDelay );
	ini.SetValue( "Performance", "GameLoopDelayMilliseconds",	m_uGameLoopDelayMilliseconds );

	ini.SetValue( "Options",	"NumArcadeStages",		m_iNumArcadeStages );
	ini.SetValue( "Options",	"MaxSongsToPlay",		m_iMaxSongsToPlay );

	ini.SetValue( "Update",		"CheckInterval",		m_uUpdateCheckInterval );
	ini.SetValue( "Update",		"LastUpdateCheck",		m_uLastUpdateCheck );

	ini.SetValue( "Options",	"NumStageLengths",		m_iStageLengths );
	for( int i=0; i<m_iStageLengths; ++i )
	{
		ini.SetValue( "Options", ssprintf("StageLength%dStagesPerSong", i+1),	m_StageLength[i].m_iStagesPerSong );
		ini.SetValue( "Options", ssprintf("StageLength%dEnabled", i+1),			m_StageLength[i].m_bUseSongWithLength );
		ini.SetValue( "Options", ssprintf("StageLength%dLengthSeconds", i+1),	m_StageLength[i].m_fMusicLengthSeconds );
	}

	ini.SetValue( "Options",	"DefaultStagesPerSong",		m_iDefaultStagesPerSong );
	ini.SetValue( "Options",	"ProgressiveLifebarBySong",	m_bProgressiveLifebarBySong );

	ini.SetValue( "Options",	"SkipSongsHarderThanInExtraStage2",	m_iSkipSongsHarderThanOMES );
	ini.SetValue( "Options",	"SetPlayerOptionsInExtraStage",		m_bSetPlayerOptionsES );
	ini.SetValue( "Options",	"SetPlayerOptionsInExtraStage2",	m_bSetPlayerOptionsOMES );
	ini.SetValue( "Options",	"SetSongOptionsInExtraStage",		m_bSetSongOptionsES );
	ini.SetValue( "Options",	"SetSongOptionsInExtraStage2",		m_bSetSongOptionsOMES );

	ini.SetValue( "Options",	"ExtraStageChecksDifficulty",			m_bCheckDifficultyES );
	ini.SetValue( "Options",	"ExtraStage2ChecksDifficulty",			m_bCheckDifficultyOMES );
	ini.SetValue( "Options",	"ExtraStageChecksDifficultyAllStages",	m_bCheckDifficultyAllStagesES );
	ini.SetValue( "Options",	"ExtraStage2ChecksDifficultyAllStages",	m_bCheckDifficultyAllStagesOMES );

	ini.SetValue( "Options",	"ExtraStageMinDifficulty",	DifficultyToString( m_MinDifficultyES ) );
	ini.SetValue( "Options",	"ExtraStage2MinDifficulty",	DifficultyToString( m_MinDifficultyOMES ) );
	ini.SetValue( "Options",	"ExtraStageMaxDifficulty",	DifficultyToString( m_MaxDifficultyES ) );
	ini.SetValue( "Options",	"ExtraStage2MaxDifficulty",	DifficultyToString( m_MaxDifficultyOMES ) );

	ini.SetValue( "Options",	"ExtraStageChecksGrade",			m_bCheckGradeES );
	ini.SetValue( "Options",	"ExtraStage2ChecksGrade",			m_bCheckGradeOMES );
	ini.SetValue( "Options",	"ExtraStageChecksGradeAllStages",	m_bCheckGradeAllStagesES );
	ini.SetValue( "Options",	"ExtraStage2ChecksGradeAllStages",	m_bCheckGradeAllStagesOMES );

	ini.SetValue( "Options",	"ExtraStageMinGrade",	GradeToString( m_MinGradeES ) );
	ini.SetValue( "Options",	"ExtraStage2MinGrade",	GradeToString( m_MinGradeOMES ) );

	ini.SetValue( "Options",	"ExtraStageChecksNumberOfPlayedSongs",	m_bCheckSongsPlayedES );
	ini.SetValue( "Options",	"ExtraStage2ChecksNumberOfPlayedSongs",	m_bCheckSongsPlayedOMES );
	ini.SetValue( "Options",	"ExtraStageMinPlayedSongs",				m_iMinSongsPlayedES );
	ini.SetValue( "Options",	"ExtraStage2MinPlayedSongs",			m_iMinSongsPlayedOMES );
	ini.SetValue( "Options",	"ExtraStageMaxPlayedSongs",				m_iMaxSongsPlayedES );
	ini.SetValue( "Options",	"ExtraStage2MaxPlayedSongs",			m_iMaxSongsPlayedOMES );

	ini.SetValue( "Options", "AllowExtraStage2",			m_bAllowExtraStage2 );
	ini.SetValue( "Options", "LockExtraStage2Difficulty",	m_bLockExtraStage2Diff );
	ini.SetValue( "Options", "PickExtraStage2",				m_bPickExtraStage2 );
	ini.SetValue( "Options", "PickModsForExtraStage2",		m_bPickModsForExtraStage2 );
	ini.SetValue( "Options", "DarkExtraStage2",				m_bDarkExtraStage2 );

	ini.SetValue( "Options", "ExtraStageForcePreferredGroup",	m_bExtraStageForcePreferredGroup );
	ini.SetValue( "Options", "ExtraStage2ForcePreferredGroup",	m_bExtraStage2ForcePreferredGroup );

	ini.SetValue( "Options",	"JukeboxNonMenuButtons",				m_bJukeboxNonMenuButtons );
	ini.SetValue( "Options",	"JukeboxGameplay",						m_bJukeboxGameplay );
	ini.SetValue( "Options",	"RandomModifiersReadsRandomAttacks",	m_bRandomModifiersReadsRandomAttacks );
	ini.SetValue( "Options",	"HighScoreSavesBonusScore",				m_bHighScoreSavesBonusScore );

	ini.SetValue( "ScreenGameplay",	"ApplyOffsetToEveryDifficulty",	(int&)m_ApplyOffsetToEveryDifficulty );

	ini.SetValue( "ScreenGameplay",	"KeyF9BPMValue",				m_fKeyF9BPMValue );
	ini.SetValue( "ScreenGameplay",	"KeyF10BPMValue",				m_fKeyF10BPMValue );
	ini.SetValue( "ScreenGameplay",	"KeyAltBPMDivisor",				m_fKeyAltBPMDivisor );
	ini.SetValue( "ScreenGameplay",	"SlowRepeatBPMMultiplier",		m_fSlowRepeatBPMMultiplier );
	ini.SetValue( "ScreenGameplay",	"FastRepeatBPMMultiplier",		m_fFastRepeatBPMMultiplier );

	ini.SetValue( "ScreenGameplay",	"KeyF11OffsetValue",			m_fKeyF11OffsetValue );
	ini.SetValue( "ScreenGameplay",	"KeyF12OffsetValue",			m_fKeyF12OffsetValue );
	ini.SetValue( "ScreenGameplay",	"KeyAltOffsetDivisor",			m_fKeyAltOffsetDivisor );
	ini.SetValue( "ScreenGameplay",	"SlowRepeatOffsetMultiplier",	m_fSlowRepeatOffsetMultiplier );
	ini.SetValue( "ScreenGameplay",	"FastRepeatOffsetMultiplier",	m_fFastRepeatOffsetMultiplier );

	ini.SetValue( "LoadingWindow",	"LoadThemeSplash",	m_bLoadThemeSplash );
	ini.SetValue( "LoadingWindow",	"LastLoadedSplash",	m_sLastLoadedSplash );

	ini.SetValue( "EasterEggs", "TouchMode",			m_iTouchMode );
	ini.SetValue( "EasterEggs", "DisableSpecialKeys",	m_bDisableSpecialKeys );
	ini.SetValue( "EasterEggs", "PersistentInput",		m_bPersistentInput );

	ini.SetValue( "Renderer", "EnableOpenGLRenderer",	m_bEnableOpenGLRenderer );
	ini.SetValue( "Renderer", "EnableDirect3DRenderer",	m_bEnableDirect3DRenderer );
	ini.SetValue( "Renderer", "EnableNullRenderer",		m_bEnableNullRenderer );

#if defined( WITH_COIN_MODE )
	//
	// Coin Mode
	//
	ini.SetValue( "Options", "CoinsPerCredit",	m_iCoinsPerCredit );
	ini.SetValue( "Options", "CoinMode",		m_CoinMode );
	ini.SetValue( "Options", "Premium",			m_Premium );
#endif

	FOREACH_CONST( IPreference*, *g_pvpSubscribers, p ) (*p)->WriteTo( ini );
	ini.WriteFile( STEPMANIA_INI_PATH );
}

CString PrefsManager::GetSoundDrivers()
{
	if( m_sSoundDrivers.empty() )
		return (CString)DEFAULT_SOUND_DRIVER_LIST;
	else
		return m_sSoundDrivers;
}

#if defined( WITH_COIN_MODE )
PrefsManager::Premium PrefsManager::GetPremium()
{
	if( m_bEventMode )
		return NO_PREMIUM;
	else
		return m_Premium;
}

PrefsManager::CoinMode PrefsManager::GetCoinMode()
{
	if( m_bEventMode && m_CoinMode == COIN_PAY )
		return COIN_FREE;
	else
		return m_CoinMode;
}

static const CString CoinModeNames[] = {
	"home",
	"free",
	"pay",
	""
};

const CString& PrefsManager::CoinModeToString( CoinMode cm )
{
	if( cm < COIN_HOME || cm > COIN_PAY )
		return CoinModeNames[COIN_PAY+1];
	else
		return CoinModeNames[cm];
}
#endif


/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * Contains code (c) 2001-2004 Chris Danford, Chris Gomez.
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
