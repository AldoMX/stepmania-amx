#ifndef NOTE_TYPES_H
#define NOTE_TYPES_H

#include "PlayerNumber.h"

// TODO - Aldo_MX: Arreglar los #pragma
#pragma warning( push )
#pragma warning( disable: 4480 )

enum HoldType : unsigned char
{
	HOLD_TYPE_DANCE = 0,
	//HOLD_TYPE_PIU,	// TODO: HOLD_TYPE_PIU
	HOLD_TYPE_ROLL,
	NUM_HOLD_TYPES
};
#define FOREACH_HoldType( ht ) FOREACH_ENUM( HoldType, NUM_HOLD_TYPES, ht )
const CString& HoldTypeToString( HoldType ht );
HoldType StringToHoldType( const CString& ht );

struct TapNote
{
	enum Type : unsigned char
	{
		empty = 0,
		tap,
		hold_head,
		hold_tail,
		hold,
		mine,		// don't step!
		shock,		// don't step!
		potion,		// heals lifebar!
		lift,		// up
		hidden		// can't see!
	};
	//enum Type : unsigned char
	//{
	//	empty = 0,
	//	tap,
	//	hold_head,
	//	hold_tail,
	//	hold,
	//	item,
	//	division,
	//	lift
	//};
	enum SubType : unsigned char
	{
		subtype_none = 0,
	};
	enum SubType_Tap : unsigned char
	{
		tap_normal = 0,
		tap_left,
		tap_right,
		tap_both,
		tap_hands
	};
	enum SubType_Item : unsigned char
	{
		item_action = 1,
		item_shield,
		item_charge,
		item_acceleration,
		item_flash,
		item_mine,			// dark step
		item_mine_layer,	// red item
		item_attack,
		item_drain,
		item_heart,
		item_speed_2x,
		item_random,
		item_speed_3x,
		item_speed_4x,
		item_speed_8x,
		item_speed_1x,
		item_potion,
		item_rotate_0,
		item_rotate_90,
		item_rotate_180,
		item_rotate_270,
		item_random_speed,
		item_bomb,			// same as mine, but explosion affects both players and life decreases more
		item_hyper_potion,	// heals more
		item_shock
	};
	enum SubType_Division : unsigned char
	{
		division_groove = 1,
		division_wild,
		division_a,
		division_b,
		division_c
	};
	enum DisplayFlag : unsigned char
	{
		display_none	= 0x00,
		display_top		= 0x01,
		display_bottom	= 0x02,
		display_all		= 0x03
	};
	enum BehaviorFlag : unsigned char
	{
		behavior_none		= 0x00,
		behavior_fake		= 0x01,	// set = non press, non set = press
		behavior_bonus		= 0x02,	// set = non miss, non set = miss
		behavior_autoplay	= 0x04,
		behavior_autopress	= 0x08
	};
	//enum PressFlag : unsigned char
	//{
	//	flag_press		= 0x01,
	//	flag_hold		= 0x02,
	//	flag_repeat		= 0x04,
	//	flag_release	= 0x08,
	//	flag_skip		= 0x10,
	//	flag_freeze		= 0x20
	//};
	//enum JudgmentFlag : unsigned char
	//{
	//	judge_marvelous	= 0x01,
	//	judge_perfect	= 0x02,
	//	judge_great		= 0x04,
	//	judge_good		= 0x08,
	//	judge_bad		= 0x10,
	//	judge_miss		= 0x20
	//};
	enum Source : unsigned char
	{
		original,	// part of the original NoteData
		addition,	// additional note added by a transform
		removed,	// Removed taps, e.g. in Little - play keysounds here as if
				// judged Perfect, but don't bother rendering or judging this
				// step.  Also used for when we implement auto-scratch in BM,
				// and for if/when we do a "reduce" modifier that cancels out
				// all but N keys on a line [useful for BM->DDR autogen, too].
				// Removed hold body (...why?) - acts as follows:
				// 1 - if we're using a sustained-sound gametype [Keyboardmania], and
				//     we've already hit the start of the sound (?? we put Holds Off on?)
				//     then this is triggered automatically to keep the sound going
				// 2 - if we're NOT [anything else], we ignore this.
				// Equivalent to all 9s aside from the first one.
	};

	unsigned char type;
	unsigned char subtype;
	unsigned char playerNumber;
	unsigned char keySoundID;
	unsigned char behaviorFlag;
	unsigned char displayFlag;
	unsigned char noteskinID;
	//unsigned char pressFlag;
	//unsigned char judgmentFlag;

	unsigned char source;	// only valid if type!=empty
	unsigned char checkpoints;
	unsigned char drawType;

	bool operator<( const TapNote& tn )
	{
#define COMP(a) if(a<tn.a) return true; else if(a>tn.a) return false;
	COMP(type);
	COMP(subtype);
	COMP(playerNumber);
	COMP(keySoundID);
	COMP(behaviorFlag);
	COMP(displayFlag);
	COMP(noteskinID);
	//COMP(pressFlag);
	//COMP(judgmentFlag);
#undef COMP
	return false;
	}
};

extern TapNote TAP_EMPTY;				// '0'
extern TapNote TAP_ORIGINAL_TAP;		// '1'
extern TapNote TAP_ORIGINAL_LIFT;		// 'L'
extern TapNote TAP_ORIGINAL_HIDDEN;		// 'H'
extern TapNote TAP_ORIGINAL_HOLD_HEAD;	// '2'
extern TapNote TAP_ORIGINAL_HOLD_TAIL;	// '3'
extern TapNote TAP_ORIGINAL_ROLL_HEAD;	// '4'
extern TapNote TAP_ORIGINAL_HOLD;		// '9'
extern TapNote TAP_ORIGINAL_ROLL;		// '8'
extern TapNote TAP_ORIGINAL_MINE;		// 'M'
extern TapNote TAP_ORIGINAL_SHOCK;		// 'S'
extern TapNote TAP_ORIGINAL_POTION;		// 'P'

extern TapNote TAP_ADDITION_TAP;
extern TapNote TAP_ADDITION_LIFT;
extern TapNote TAP_ADDITION_MINE;
extern TapNote TAP_ADDITION_SHOCK;
extern TapNote TAP_ADDITION_POTION;

// TODO: Don't have a hard-coded track limit.
enum
{
	TRACK_1 = 0,
	TRACK_2,
	TRACK_3,
	TRACK_4,
	TRACK_5,
	TRACK_6,
	TRACK_7,
	TRACK_8,
	TRACK_9,
	TRACK_10,
	TRACK_11,
	TRACK_12,
	TRACK_13,	// BMS reader needs 13 tracks
	// MD 10/26/03 - BMS reader needs a whole lot more than 13 tracks - more like 16
	//   because we have 11-16, 18, 19, 21-26, 28, 29 for IIDX double (bm-double7)
	TRACK_14,
	TRACK_15,
	TRACK_16,
	// MD 10/26/03 end
	MAX_NOTE_TRACKS		// leave this at the end
};

const int BEATS_PER_MEASURE = 4;
const int ROWS_PER_BEAT = 192;	// It is important that this number is evenly divisible by 2, 3, and 4.
const int ROWS_PER_MEASURE = ROWS_PER_BEAT * BEATS_PER_MEASURE;

const float fBEATS_PER_MEASURE = (float)BEATS_PER_MEASURE;
const float fROWS_PER_BEAT = (float)ROWS_PER_BEAT;
const float fROWS_PER_MEASURE = fROWS_PER_BEAT * fBEATS_PER_MEASURE;

enum NoteType
{
	NOTE_TYPE_4TH,		// quarter note
	NOTE_TYPE_8TH,		// eighth note
	NOTE_TYPE_12TH,		// quarter note triplet
	NOTE_TYPE_16TH,		// sixteenth note
	NOTE_TYPE_24TH,		// eighth note triplet
	NOTE_TYPE_32ND,		// thirty-second note
	NOTE_TYPE_48TH,		// sixteenth note triplet
	NOTE_TYPE_64TH,		// sixty-fourth note
	NOTE_TYPE_96TH,		// thirty-second triplet
	NOTE_TYPE_128TH,	// one-hundred-twenty-eight note
	NOTE_TYPE_192ND,	// sixty-fourth note triplet
	NOTE_TYPE_256TH,	// two-hundred-fifty-sixth note
	NOTE_TYPE_384TH,	// one-hundred-twenty-eight triplet
	NOTE_TYPE_768TH,	// two-hundred-fifty-sixth triplet
	NUM_NOTE_TYPES,
	NOTE_TYPE_INVALID
};
#define FOREACH_NoteType( nt ) FOREACH_ENUM( NoteType, NUM_NOTE_TYPES, nt )

int NoteTypeToRow( NoteType nt );
float NoteTypeToBeat( NoteType nt );
NoteType GetNoteType( int iNoteIndex );
NoteType BeatToNoteType( float fBeat );
bool IsNoteOfType( int iNoteIndex, NoteType t );
CString NoteTypeToString( NoteType nt );

inline int   BeatToNoteRowRounded( float fBeatNum )	{ return (int)( fBeatNum * ROWS_PER_BEAT + 0.5f);	};	// round
inline int   BeatToNoteRow( float fBeatNum )		{ return (int)( fBeatNum * ROWS_PER_BEAT );			};
inline float NoteRowToBeatF( float fNoteIndex )		{ return fNoteIndex / (float)ROWS_PER_BEAT;			};
inline float NoteRowToBeat( int iNoteIndex )		{ return NoteRowToBeatF( (float)iNoteIndex );		};
inline float NoteRowToBeat( size_t noteIndex )		{ return NoteRowToBeatF( (float)noteIndex );		};

struct HoldNote
{
	HoldNote( int track, int start, int end ):
		iTrack( track ),
		iStartRow( start ),
		iEndRow( end ),
		subtype( HOLD_TYPE_DANCE ),
		playerNumber( PLAYER_1 ),
		keySoundID( 0 ),
		behaviorFlag( TapNote::behavior_none ),
		displayFlag( TapNote::display_all ),
		noteskinID( 0 )
	{}
	bool RowIsInRange( int row ) const { return iStartRow <= row && row <= iEndRow; }
	bool RangeOverlaps( int start, int end ) const
	{
		/* If the range doesn't overlap us, then start and end are either both before
		 * us or both after us. */
		return !( (start < iStartRow && end < iStartRow) ||
				  (start > iEndRow && end > iEndRow) );
	}
	bool RangeOverlaps( const HoldNote &hn ) const { return RangeOverlaps(hn.iStartRow, hn.iEndRow); }
	bool RangeInside( int start, int end ) const { return iStartRow <= start && end <= iEndRow; }
	bool ContainedByRange( int start, int end ) const { return start <= iStartRow && iEndRow <= end; }

	float GetStartBeat() const { return NoteRowToBeat( iStartRow ); }
	float GetEndBeat() const { return NoteRowToBeat( iEndRow ); }

	int iTrack;
	int iStartRow;
	int iEndRow;

	HoldType subtype;
	unsigned char playerNumber;
	unsigned char keySoundID;
	unsigned char behaviorFlag;
	unsigned char displayFlag;
	unsigned char noteskinID;

	void ToTapNote( TapNote& tn )
	{
		tn.subtype = subtype;

#define EQUAL(a) tn.a = a;
		EQUAL(playerNumber);
		EQUAL(keySoundID);
		EQUAL(behaviorFlag);
		EQUAL(displayFlag);
		EQUAL(noteskinID);
#undef EQUAL
	}

	void FromTapNote( const TapNote& tn )
	{
		subtype = (HoldType)tn.subtype;

#define EQUAL(a) a = tn.a;
		EQUAL(playerNumber);
		EQUAL(keySoundID);
		EQUAL(behaviorFlag);
		EQUAL(displayFlag);
		EQUAL(noteskinID);
#undef EQUAL
	}

	bool operator<( const HoldNote& hn )
	{
#define COMP(a) if(a<hn.a) return true; else if(a>hn.a) return false;
		COMP(iTrack);
		COMP(iStartRow);
		COMP(iEndRow);

		COMP(subtype);
		COMP(playerNumber);
		COMP(keySoundID);
		COMP(behaviorFlag);
		COMP(displayFlag);
		COMP(noteskinID);
#undef COMP
	return false;
	}
};

#pragma warning( pop )

#endif

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * Contains code (c) 2001-2004 Chris Danford, Glenn Maynard.
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
