#include "global.h"

#include "BPMRange.h"
#include "RageUtil.h"
#include "TimingData.h"

BPMRange::BPMRange()
{
	Reset();
}

BPMRange::BPMRange( float fMin, float fMax )
{
	m_fMin = min(fMin, fMax);
	m_fMax = max(fMin, fMax);
}

BPMRange::BPMRange( TimingData* timing )
{
	Reset();
	for( vector<BPMSegment>::const_iterator it = timing->m_BPMSegments.begin(); it != timing->m_BPMSegments.end(); ++it )
	{
		m_fMin = min(m_fMin, it->m_fBPM);
		m_fMax = max(m_fMax, it->m_fBPM);
	}
}

void BPMRange::Reset()
{
	m_fMin = FLT_MAX;
	m_fMax = -FLT_MAX;
}

void BPMRange::AddTo( vector<float>& bpms )
{
	bpms.push_back(m_fMin);
	bpms.push_back(m_fMax);
}

CString BPMRange::GetString() const
{
	if( IsRandom() )
		return "*";

	if( IsConstant() )
		return ssprintf("%.3f", m_fMin);

	return ssprintf("%.3f,%.3f", m_fMin, m_fMax);
}

void BPMRange::FromString( CString& range )
{
	Reset();

	float fMin, fMax;
	if( sscanf( range.c_str(), "%f%*s%f", &fMin, &fMax ) == 2 )
	{
		m_fMin = fMin;
		m_fMax = fMax;
	}
	else if( sscanf( range.c_str(), "%f", &fMin ) == 1 )
	{
		m_fMin = fMin;
		m_fMax = fMin;
	}
}

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
