/* GameConstantsAndTypes - Things used in many places that don't change often. */

#ifndef GAME_CONSTANTS_AND_TYPES_H
#define GAME_CONSTANTS_AND_TYPES_H

#include "PlayerNumber.h"	// TODO: Get rid of this dependency.  -Chris
#include "Difficulty.h"		// TODO: Get rid of this dependency.
#include "EnumHelper.h"

//
// Screen Dimensions
//
extern int SCREEN_WIDTH;
extern int SCREEN_HEIGHT;

#define SCREEN_LEFT		0.f
#define SCREEN_RIGHT	float(SCREEN_WIDTH)
#define SCREEN_TOP		0.f
#define SCREEN_BOTTOM	float(SCREEN_HEIGHT)

#define CENTER_X	(SCREEN_LEFT + (SCREEN_RIGHT - SCREEN_LEFT)/2.0f)
#define CENTER_Y	(SCREEN_TOP + (SCREEN_BOTTOM - SCREEN_TOP)/2.0f)

const float SCREEN_DPI_DEFAULT = 96.f;
#define GetScreenZoom(dpi) ((float)dpi/SCREEN_DPI_DEFAULT);


#define	SCREEN_NEAR		(-1000)
#define	SCREEN_FAR		(1000)

#define	ARROW_SIZE		(64)


//
// Note definitions
//
const int MAX_METER = 25;
const int MIN_METER = 1;


/* This is just cached song data.  Not all of it may actually be displayed
 * in the radar. */
enum RadarCategory
{
	RADAR_STREAM = 0,
	RADAR_VOLTAGE,
	RADAR_AIR,
	RADAR_FREEZE,
	RADAR_CHAOS,
	RADAR_NUM_TAPS_AND_HOLDS_AND_ROLLS,
	RADAR_NUM_JUMPS,
	RADAR_NUM_HOLDS,
	RADAR_NUM_ROLLS,
	RADAR_NUM_MINES,
	RADAR_NUM_SHOCKS,
	RADAR_NUM_POTIONS,
	RADAR_NUM_HANDS,
	RADAR_NUM_LIFTS,
	RADAR_NUM_HIDDEN,
	NUM_RADAR_CATEGORIES	// leave this at the end
};
#define FOREACH_RadarCategory( rc ) FOREACH_ENUM( RadarCategory, NUM_RADAR_CATEGORIES, rc )
const CString& RadarCategoryToString( RadarCategory cat );
CString RadarCategoryToThemedString( RadarCategory cat );



enum StepsType
{
	STEPS_TYPE_DANCE_SINGLE = 0,
	STEPS_TYPE_DANCE_DOUBLE,
	STEPS_TYPE_DANCE_COUPLE,
	STEPS_TYPE_DANCE_SOLO,
	STEPS_TYPE_PUMP_SINGLE,
	STEPS_TYPE_PUMP_DOUBLE,
	STEPS_TYPE_PUMP_HALFDOUBLE,
	STEPS_TYPE_PUMP_COUPLE,
	STEPS_TYPE_EZ2_SINGLE,
	STEPS_TYPE_EZ2_DOUBLE,
	STEPS_TYPE_EZ2_REAL,
	STEPS_TYPE_PARA_SINGLE,
	STEPS_TYPE_DS3DDX_SINGLE,
	STEPS_TYPE_BM_SINGLE,
	STEPS_TYPE_BM_DOUBLE,
	STEPS_TYPE_IIDX_SINGLE7,
	STEPS_TYPE_IIDX_DOUBLE7,
	// These last two are probably going to be treated the same way
	// as STEPS_TYPE_DANCE_COUPLE - autogenned for use, and then
	// a duplicate "(edit)" entry for editing only.
	STEPS_TYPE_IIDX_SINGLE5,
	STEPS_TYPE_IIDX_DOUBLE5,
	STEPS_TYPE_MANIAX_SINGLE,
	STEPS_TYPE_MANIAX_DOUBLE,
	STEPS_TYPE_TECHNO_SINGLE4,
	STEPS_TYPE_TECHNO_SINGLE5,
	STEPS_TYPE_TECHNO_SINGLE8,
	STEPS_TYPE_TECHNO_DOUBLE4,
	STEPS_TYPE_TECHNO_DOUBLE5,
	STEPS_TYPE_PNM_FIVE,
	STEPS_TYPE_PNM_NINE,
	STEPS_TYPE_LIGHTS_CABINET,
	NUM_STEPS_TYPES,		// leave this at the end
	STEPS_TYPE_INVALID
};
#define FOREACH_StepsType( st ) FOREACH_ENUM( StepsType, NUM_STEPS_TYPES, st )
const CString& StepsTypeToString( StepsType st );
CString StepsTypeToThemedString( StepsType st );
StepsType StringToStepsType( const CString& str );
StepsType StringToStepsType2( CString sStepsType );	// handles some incompatible strings like "pump-routine"

//
// Play mode stuff
//
enum PlayMode
{
	PLAY_MODE_REGULAR,
	PLAY_MODE_NONSTOP,	// DDR EX Nonstop
	PLAY_MODE_ONI,		// DDR EX Challenge
	PLAY_MODE_ENDLESS,	// DDR PlayStation Endless
	PLAY_MODE_BATTLE,	// manually launched attacks
	PLAY_MODE_RAVE,		// automatically launched attacks - DDR Disney Rave "Dance Magic"
	NUM_PLAY_MODES,
	PLAY_MODE_INVALID
};
#define FOREACH_PlayMode( pm ) FOREACH_ENUM( PlayMode, NUM_PLAY_MODES, pm )
const CString& PlayModeToString( PlayMode pm );
CString PlayModeToThemedString( PlayMode pm );
PlayMode StringToPlayMode( const CString& s );




enum SortOrder
{
	SORT_PREFERRED,
	SORT_GROUP,
	SORT_FOLDER,
	SORT_TITLE,
	SORT_GENRE,
	SORT_BPM,
	SORT_MOST_PLAYED,
	SORT_LIST,
	SORT_GRADE,
	SORT_ARTIST,
	SORT_EASY_METER,
	SORT_MEDIUM_METER,
	SORT_HARD_METER,
	SORT_CHALLENGE_METER,
	SORT_SORT_MENU,
	SORT_MODE_MENU,
	SORT_ALL_COURSES,
	SORT_NONSTOP_COURSES,
	SORT_ONI_COURSES,
	SORT_ENDLESS_COURSES,
	SORT_ROULETTE,
	NUM_SORT_ORDERS,
	SORT_INVALID
};
const SortOrder MAX_SELECTABLE_SORT = (SortOrder)(SORT_ROULETTE-1);
#define FOREACH_SortOrder( so ) FOREACH_ENUM( SortOrder, NUM_SORT_ORDERS, so )
const CString& SortOrderToString( SortOrder so );
SortOrder StringToSortOrder( const CString& str );

inline bool IsSongSort( SortOrder so ) { return so >= SORT_PREFERRED && so <= SORT_CHALLENGE_METER; }

//
// Scoring stuff
//

enum TapNoteScore {
	TNS_NONE,
	TNS_HIT_MINE,
	TNS_HIT_SHOCK,
	TNS_HIT_POTION,
	TNS_MISS_CHECKPOINT,
	TNS_CHECKPOINT,
	TNS_MISS_HIDDEN,
	TNS_HIDDEN,
	TNS_MISS,
	TNS_BOO,
	TNS_GOOD,
	TNS_GREAT,
	TNS_PERFECT,
	TNS_MARVELOUS,
	NUM_TAP_NOTE_SCORES,
	TNS_INVALID
};
#define FOREACH_TapNoteScore( tns ) FOREACH_ENUM( TapNoteScore, NUM_TAP_NOTE_SCORES, tns )
#define FOREACH_TapNoteScoreNoItem( tns ) FOREACH_ENUM_MIN( TapNoteScore, TNS_MISS_CHECKPOINT, NUM_TAP_NOTE_SCORES, tns )
#define FOREACH_TapNoteScoreNoNone( tns ) FOREACH_ENUM_MIN( TapNoteScore, TNS_HIT_MINE, NUM_TAP_NOTE_SCORES, tns )
const CString& TapNoteScoreToString( TapNoteScore tns );
CString TapNoteScoreToThemedString( TapNoteScore tns );
TapNoteScore StringToTapNoteScore( const CString& str );


enum HoldNoteScore
{
	HNS_NONE,	// this HoldNote has not been scored yet
	HNS_NG,		// the HoldNote has passed and they missed it
	HNS_OK,		// the HoldNote has passed and was successfully held all the way through
	RNS_NG,		// the RollNote has passed and they missed it, or they let it go
	RNS_OK,		// the RollNote has passed and was successfully kept alive all the way through
	SHOCK_NG,	// the Shock Arrow has passed and they missed it
	SHOCK_OK,	// the Shock Arrow has passed and they pressed it
	NUM_HOLD_NOTE_SCORES,
	HNS_INVALID
};
#define FOREACH_HoldNoteScore( hns ) FOREACH_ENUM( HoldNoteScore, NUM_HOLD_NOTE_SCORES, hns )
#define FOREACH_HoldNoteScoreNoNone( hns ) FOREACH_ENUM_MIN( HoldNoteScore, HNS_NG, NUM_HOLD_NOTE_SCORES, hns )
const CString& HoldNoteScoreToString( HoldNoteScore hns );
CString HoldNoteScoreToThemedString( HoldNoteScore hns );
HoldNoteScore StringToHoldNoteScore( const CString& str );

//
// Profile and MemCard stuff
//
enum ProfileSlot
{
	PROFILE_SLOT_PLAYER_1,
	PROFILE_SLOT_PLAYER_2,
	PROFILE_SLOT_MACHINE,
	NUM_PROFILE_SLOTS,
	PROFILE_SLOT_INVALID
};
#define FOREACH_ProfileSlot( slot ) FOREACH_ENUM( ProfileSlot, NUM_PROFILE_SLOTS, slot )


enum MemoryCardState
{
	MEMORY_CARD_STATE_READY,
	MEMORY_CARD_STATE_TOO_LATE,
	MEMORY_CARD_STATE_WRITE_ERROR,
	MEMORY_CARD_STATE_NO_CARD,
	NUM_MEMORY_CARD_STATES,
	MEMORY_CARD_STATE_INVALID
};

const CString& MemoryCardStateToString( MemoryCardState mcs );


//
// Ranking stuff
//
enum RankingCategory
{
	RANKING_A,	// 1-3 meter per song avg.
	RANKING_B,	// 4-6 meter per song avg.
	RANKING_C,	// 7-9 meter per song avg.
	RANKING_D,	// 10+ meter per song avg.	// doesn't count extra stage!
	NUM_RANKING_CATEGORIES,
	RANKING_INVALID
};
#define FOREACH_RankingCategory( rc ) FOREACH_ENUM( RankingCategory, NUM_RANKING_CATEGORIES, rc )
const CString& RankingCategoryToString( RankingCategory rc );
RankingCategory StringToRankingCategory( const CString& rc );

const CString RANKING_TO_FILL_IN_MARKER[NUM_PLAYERS] = {"#P1#","#P2#"};
inline bool IsRankingToFillIn( const CString& sName ) { return !sName.empty() && sName[0]=='#'; }

RankingCategory AverageMeterToRankingCategory( int iAverageMeter );

const int NUM_RANKING_LINES	= 5;


//
// Group stuff
//
#define GROUP_ALL_MUSIC	CString("")


//
//
//
enum PlayerController
{
	PC_HUMAN,
	PC_CPU,
	PC_AUTOPLAY,
	NUM_PLAYER_CONTROLLERS
};

const int MIN_SKILL = 0;
const int MAX_SKILL = 10;


enum StageResult
{
	RESULT_WIN,
	RESULT_LOSE,
	RESULT_DRAW
};


//
// Battle stuff
//
const int NUM_INVENTORY_SLOTS	= 3;
enum AttackLevel
{
	ATTACK_LEVEL_1,
	ATTACK_LEVEL_2,
	ATTACK_LEVEL_3,
	NUM_ATTACK_LEVELS
};
const int NUM_ATTACKS_PER_LEVEL	= 3;
const int ITEM_NONE = -1;


#define BG_ANIMS_DIR		CString("BGAnimations/")
#define VISUALIZATIONS_DIR	CString("Visualizations/")
#define RANDOMMOVIES_DIR	CString("RandomMovies/")
#define DATA_DIR			CString("Data/")


//
// Award stuff
//

enum PerDifficultyAward
{
	AWARD_FULL_COMBO_GREATS,
	AWARD_SINGLE_DIGIT_GREATS,
	AWARD_ONE_GREAT,
	AWARD_FULL_COMBO_PERFECTS,
	AWARD_SINGLE_DIGIT_PERFECTS,
	AWARD_ONE_PERFECT,
	AWARD_FULL_COMBO_MARVELOUSES,
	AWARD_GREATS_80_PERCENT,
	AWARD_GREATS_90_PERCENT,
	AWARD_GREATS_100_PERCENT,
	NUM_PER_DIFFICULTY_AWARDS,
	PER_DIFFICULTY_AWARD_INVALID
};
#define FOREACH_PerDifficultyAward( pma ) FOREACH_ENUM( PerDifficultyAward, NUM_PER_DIFFICULTY_AWARDS, pma )
const CString& PerDifficultyAwardToString( PerDifficultyAward pma );
CString PerDifficultyAwardToThemedString( PerDifficultyAward pma );
PerDifficultyAward StringToPerDifficultyAward( const CString& pma );


enum PeakComboAward
{
	AWARD_1000_PEAK_COMBO,
	AWARD_2000_PEAK_COMBO,
	AWARD_3000_PEAK_COMBO,
	AWARD_4000_PEAK_COMBO,
	AWARD_5000_PEAK_COMBO,
	AWARD_6000_PEAK_COMBO,
	AWARD_7000_PEAK_COMBO,
	AWARD_8000_PEAK_COMBO,
	AWARD_9000_PEAK_COMBO,
	AWARD_10000_PEAK_COMBO,
	NUM_PEAK_COMBO_AWARDS,
	PEAK_COMBO_AWARD_INVALID
};
#define FOREACH_PeakComboAward( pca ) FOREACH_ENUM( PeakComboAward, NUM_PEAK_COMBO_AWARDS, pca )
const CString& PeakComboAwardToString( PeakComboAward pma );
CString PeakComboAwardToThemedString( PeakComboAward pma );
PeakComboAward StringToPeakComboAward( const CString& pma );


// Set when this song should be displayed in the music wheel:
enum SelectionDisplay
{
	SHOW_ALWAYS,	// all the time
	SHOW_ROULETTE,	// only when rouletting
	SHOW_NEVER,		// never (unless song hiding is turned off)
	SHOW_1,			// when there is only one stage left
	SHOW_2,			// when there are only two stages left (etc...)
	SHOW_3,
	SHOW_4,
	SHOW_5,
	SHOW_6,
	SHOW_ES,		// only show in extra stage
	SHOW_OMES,		// only show in one more extra stage
	SHOW_NO_ES,		// do not show in extra stage and above
	SHOW_NO_OMES,	// do not show in one more extra stage
	NUM_SELECTION_DISPLAY,
	SELECTION_DISPLAY_INVALID
};
const CString& SelectionDisplayToString( SelectionDisplay sd );
SelectionDisplay StringToSelectionDisplay( const CString& str );


enum DisplayBPM
{
	BPM_REAL,
	BPM_CUSTOM,
	BPM_RANDOM,
	NUM_DISPLAY_BPM,
	DISPLAY_BPM_INVALID
};
const CString& DisplayBPMToString( DisplayBPM db );
DisplayBPM StringToDisplayBPM( const CString& str );


enum TouchState
{
	TOUCH_STATE_NORMAL,
	TOUCH_STATE_PRESSED,
	NUM_TOUCH_STATES,
	TOUCH_STATE_INVALID
};
#define FOREACH_TouchState( ts ) FOREACH_ENUM( TouchState, NUM_TOUCH_STATES, ts )
const CString& TouchStateToString( TouchState ts );
TouchState StringToTouchState( const CString& str );


enum AudioFormat
{
	AUDIO_FORMAT_MP3,
	AUDIO_FORMAT_WAV,
	AUDIO_FORMAT_FLAC,
	AUDIO_FORMAT_OGG,
	NUM_AUDIO_FORMATS,
	AUDIO_FORMAT_INVALID
};
#define FOREACH_AudioFormat( af ) FOREACH_ENUM( AudioFormat, NUM_AUDIO_FORMATS, af )
const CString& AudioFormatToString( AudioFormat af );
AudioFormat StringToAudioFormat( const CString& af );


enum PictureFormat
{
	PICTURE_FORMAT_PNG,
	PICTURE_FORMAT_JPG,
	PICTURE_FORMAT_JPEG,
	PICTURE_FORMAT_BMP,
	PICTURE_FORMAT_DIB,
	PICTURE_FORMAT_GIF,
	PICTURE_FORMAT_XPM,

	//PICTURE_FORMAT_DDS,
	//PICTURE_FORMAT_TGA,

	//PICTURE_FORMAT_PNZ,
	//PICTURE_FORMAT_G,

	NUM_PICTURE_FORMATS,
	PICTURE_FORMAT_INVALID
};
#define FOREACH_PictureFormat( pf ) FOREACH_ENUM( PictureFormat, NUM_PICTURE_FORMATS, pf )
const CString& PictureFormatToString( PictureFormat pf );
PictureFormat StringToPictureFormat( const CString& pf );


enum VideoFormat
{
	// AVI (Most common format)
	VIDEO_FORMAT_AVI,

	// MPEG Variants
	VIDEO_FORMAT_MPG,
	VIDEO_FORMAT_MP4,
	VIDEO_FORMAT_MPEG,
	VIDEO_FORMAT_M2V,
	VIDEO_FORMAT_M4V,
	VIDEO_FORMAT_MPV,
	VIDEO_FORMAT_MPE,

	// Popular
	VIDEO_FORMAT_MKV,
	VIDEO_FORMAT_WMV,
	VIDEO_FORMAT_FLV,
	VIDEO_FORMAT_MOV,
	VIDEO_FORMAT_3GP,
	VIDEO_FORMAT_ASF,

	// Other Formats
	VIDEO_FORMAT_BIK,
	VIDEO_FORMAT_OGV,
	VIDEO_FORMAT_WEBM,
	VIDEO_FORMAT_ASX,
	VIDEO_FORMAT_NSV,
	VIDEO_FORMAT_RM,
	VIDEO_FORMAT_RAM,

	// DVD & Blu-Ray
	VIDEO_FORMAT_VOB,
	VIDEO_FORMAT_MTS,
	VIDEO_FORMAT_M2TS,

	NUM_VIDEO_FORMATS,
	VIDEO_FORMAT_INVALID
};
#define FOREACH_VideoFormat( vf ) FOREACH_ENUM( VideoFormat, NUM_VIDEO_FORMATS, vf )
#define FOREACH_VideoFormatMPEG( vf ) FOREACH_ENUM_MIN( VideoFormat, VIDEO_FORMAT_MPG, VIDEO_FORMAT_MKV, vf )
const CString& VideoFormatToString( VideoFormat vf );
VideoFormat StringToVideoFormat( const CString& vf );


enum StepFormat
{
	STEP_FORMAT_SMA,
	STEP_FORMAT_SSC,
	STEP_FORMAT_SM,
	STEP_FORMAT_DWI,

	//STEP_FORMAT_STX,
	STEP_FORMAT_OLD,

	STEP_FORMAT_BMS,
	STEP_FORMAT_KSF,
	STEP_FORMAT_UCS,
	//STEP_FORMAT_NX,

	NUM_STEP_FORMATS,
	STEP_FORMAT_INVALID
};
#define FOREACH_StepFormat( sf ) FOREACH_ENUM( StepFormat, NUM_STEP_FORMATS, sf )
#define FOREACH_StepFormatNoSMA( sf ) FOREACH_ENUM_MIN( StepFormat, STEP_FORMAT_SSC, NUM_STEP_FORMATS, sf )
#define FOREACH_StepFormatSingular( sf ) FOREACH_ENUM_MIN( StepFormat, STEP_FORMAT_SMA, STEP_FORMAT_BMS, sf )
#define FOREACH_StepFormatSingularNoSMA( sf ) FOREACH_ENUM_MIN( StepFormat, STEP_FORMAT_SSC, STEP_FORMAT_BMS, sf )
#define FOREACH_StepFormatMultiple( sf ) FOREACH_ENUM_MIN( StepFormat, STEP_FORMAT_BMS, NUM_STEP_FORMATS, sf )
const CString& StepFormatToString( StepFormat sf );
StepFormat StringToStepFormat( const CString& sf );


enum AnimationTiming
{
	AT_STEP,
	AT_BGA,
	AT_SECONDS,
	NUM_ANIMATION_TIMINGS,
	AT_INVALID
};
#define FOREACH_AnimationTiming( at ) FOREACH_ENUM( AnimationTiming, NUM_ANIMATION_TIMINGS, at )
const CString& AnimationTimingToString( AnimationTiming sf );
AnimationTiming StringToAnimationTiming( const CString& at );

#endif

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * Contains code (c) 2001-2004 Chris Danford, Chris Gomez.
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
