// TimingData - Holds data for translating beats<->seconds.

#ifndef TIMING_DATA_H
#define TIMING_DATA_H

struct BPMSegment
{
	BPMSegment() { m_fBeat = m_fBPM = -1; };
	BPMSegment( float s, float b ) { m_fBeat = s; m_fBPM = b; };
	float m_fBeat;
	float m_fBPM;
};

struct StopSegment
{
	StopSegment() { m_fBeat = m_fSeconds = -1; m_bDelay = false; };
	StopSegment( float s, float f, bool d ) { m_fBeat = s; m_fSeconds = f; m_bDelay = d; };
	float m_fBeat;
	float m_fSeconds;
	bool m_bDelay;
};

struct TickcountSegment
{
	TickcountSegment() { m_fBeat = -1; m_uTickcount = 2; };
	TickcountSegment( float b, unsigned t ) { m_fBeat = b; m_uTickcount = t; };
	float m_fBeat;
	unsigned m_uTickcount;
};

struct MultiplierSegment
{
	MultiplierSegment() { m_fBeat = m_fLifeHit = m_fScoreHit = m_fLifeMiss = m_fScoreMiss = -1; m_uHit = m_uMiss = 0; };
	MultiplierSegment( float b, unsigned h, unsigned m, float lh, float sh, float lm, float sm ) { m_fBeat = b; m_uHit = h; m_uMiss = m; m_fLifeHit = lh; m_fScoreHit = sh; m_fLifeMiss = lm; m_fScoreMiss = sm; };
	float m_fBeat, m_fLifeHit, m_fScoreHit, m_fLifeMiss, m_fScoreMiss;
	unsigned m_uHit, m_uMiss;
};

struct AreaSegment
{
	AreaSegment() { m_fBeat = m_fBeats = -1; };
	AreaSegment( float b, float bs ) { m_fBeat = b; m_fBeats = bs; };
	float m_fBeat;
	float m_fBeats;
};

struct SpeedSegment : public AreaSegment
{
	SpeedSegment() { AreaSegment(); m_fToSpeed = m_fFactor = -1; };
	SpeedSegment( float b, float s, float bs, float f ) { m_fBeat = b; m_fToSpeed = s; m_fBeats = bs; m_fFactor = f; };
	float m_fToSpeed, m_fFactor;
};

struct SpeedAreaSegment : public SpeedSegment
{
	SpeedAreaSegment() { SpeedSegment(); m_fFromSpeed = m_fTriggerOffset = -1; };
	SpeedAreaSegment( float b, float fs, float ts, float d, float o, float f ) { m_fBeat = b; m_fFromSpeed = fs; m_fToSpeed = ts; m_fBeats = d; m_fTriggerOffset = o; m_fFactor = f; };
	float m_fFromSpeed, m_fTriggerOffset;
};

class TimingData
{
public:
	TimingData();

	void Clear();
	void Reset();

	float GetBPMAtBeat( float fBeat ) const;
	void SetBPMAtBeat( float fBeat, float fBPM );
	void AddBPMSegment( const BPMSegment &seg );
	BPMSegment& GetBPMSegmentAtBeat( float fBeat );

	float GetStopLengthAtBeat( float fBeat ) const;
	float GetStopAtBeat( float fBeat, bool& bDelay ) const;
	void SetStopAtBeat( float fBeat, float fSeconds, bool bDelay );
	void AddStopSegment( const StopSegment &seg );
	StopSegment& GetStopSegmentAtBeat( float fBeat );

	void SetSpeedAtBeat( float fBeat, float fToSpeed, float fBeats, float fFactor );
	void AddSpeedSegment( const SpeedSegment &seg );
	void DeleteSpeedAtBeat( float fBeat );
	unsigned GetSpeedSegmentNumberAtBeat( float fBeat ) const;
	SpeedSegment& GetSpeedSegmentAtBeat( float fBeat );

	unsigned GetTickcountAtBeat( float fBeat ) const;
	float GetTickcountStartAtBeat( float fBeat ) const;
	void SetTickcountAtBeat( float fBeat, unsigned uTickcount );
	void AddTickcountSegment( const TickcountSegment &seg );
	void DeleteTickcountAtBeat( float fBeat );
	const TickcountSegment& GetTickcountSegmentAtBeat( float fBeat ) const;

	unsigned GetHitMultiplierAtBeat( float fBeat, int tnsScore ) const;
	float GetLifeMultiplierAtBeat( float fBeat, int tnsScore ) const;
	float GetScoreMultiplierAtBeat( float fBeat, int tnsScore ) const;
	void GetMultiplierAtBeat( float fBeat, int tnsScore, unsigned& uComboMultiplier, float& fLifeMultiplier, float& fScoreMultiplier );
	void SetMultiplierAtBeat( float fBeat, unsigned uHit, unsigned uMiss, float fLifeHit, float fScoreHit, float fLifeMiss, float fScoreMiss );
	void AddMultiplierSegment( const MultiplierSegment &seg );
	void DeleteMultiplierAtBeat( float fBeat );
	unsigned GetMultiplierSegmentNumberAtBeat( float fBeat );
	MultiplierSegment& GetMultiplierSegmentAtBeat( float fBeat );

	bool IsFakeAreaAtBeat( float fBeat ) const;
	void SetFakeAtBeat( float fBeat, float fBeats );
	void AddFakeSegment( const AreaSegment &seg );
	AreaSegment& GetFakeSegmentAtBeat( float fBeat );

	bool IsSpeedAreaAtBeat( float fBeat ) const;
	void SetSpeedAreaAtBeat( float fBeat, float fFromSpeed, float fToSpeed, float fBeats, float fTriggerOffset, float fFactor );
	void AddSpeedAreaSegment( const SpeedAreaSegment &seg );
	SpeedAreaSegment& GetSpeedAreaSegmentAtBeat( float fBeat );

	void GetBeatAndBPSFromElapsedTime( float fElapsedTime, float &fBeatOut, float &fBPSOut, bool &bFreezeOut, bool &bDelayOut ) const;
	float GetBeatFromElapsedTime( float fElapsedTime ) const	// shortcut for places that care only about the beat
	{
		float fBeat, fThrowAway;
		bool bThrowAway;
		GetBeatAndBPSFromElapsedTime( fElapsedTime, fBeat, fThrowAway, bThrowAway, bThrowAway );
		return fBeat;
	}
	float GetElapsedTimeFromBeat( float fBeat ) const;
	bool HasBpmChangesOrStops() const;

	// used for editor fix - expand/contract needs to move BPMSegments
	// and StopSegments that land during/after the edited range.
	// in addition, we need to be able to shift them otherwise as well
	// (for example, adding/removing rows should move all following
	// segments as necessary)
	// NOTE: How do we want to handle deleting rows that have a BPM change
	// or a stop?  I'd like to think we should move them to the first row
	// of the range that was deleted (say if rows 1680-1728 are deleted, and
	// a BPM change or a stop occurs at row 1704, we'll move it to row
	// 1680).
	void ScaleRegion( float fScale = 1, float fStartBeat = 0, float fEndBeat = 99999 );
	void ShiftBeats( float fStartBeat, float fBeatsToShift );

	void TidyUpData();
	void ReCalculateFirstAndLastBeat( float fFirstSecond, float fLastSecond, float fMusicLengthSeconds );

	CString						m_sFile;				// informational only
	vector<BPMSegment>			m_BPMSegments;			// this must be sorted before gameplay
	vector<StopSegment>			m_StopSegments;			// this must be sorted before gameplay
	vector<SpeedSegment>		m_SpeedSegments;		// this must be sorted before gameplay
	vector<TickcountSegment>	m_TickcountSegments;	// this must be sorted before gameplay
	vector<MultiplierSegment>	m_MultiplierSegments;	// this must be sorted before gameplay
	vector<AreaSegment>			m_FakeAreaSegments;		// this must be sorted before gameplay
	vector<SpeedAreaSegment>	m_SpeedAreaSegments;	// this must be sorted before gameplay

	float	m_fBeat0Offset;
	float	m_fFirstBeat;
	float	m_fLastBeat;
};

#endif

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * Contains code (c) 2001-2004 Chris Danford, Glenn Maynard.
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
