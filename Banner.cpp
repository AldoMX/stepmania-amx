#include "global.h"
#include "Banner.h"
#include "PrefsManager.h"
#include "SongManager.h"
#include "ThemeManager.h"
#include "RageUtil.h"
#include "Song.h"
#include "RageTextureManager.h"
#include "Course.h"
#include "Character.h"
#include "BannerCache.h"

CachedThemeMetricB SCROLL_RANDOM		("Banner","ScrollRandom");
CachedThemeMetricB SCROLL_ROULETTE		("Banner","ScrollRoulette");

Banner::Banner()
{
	SCROLL_RANDOM.Refresh();
	SCROLL_ROULETTE.Refresh();

	m_bBNCache = false;

	m_bScrolling = false;
	m_fPercentScrolling = 0;

	//if( PREFSMAN->m_BannerCache != PrefsManager::BNCACHE_OFF )
	//{
	//	TEXTUREMAN->CacheTexture( SongBannerTexture(THEME->GetPathG("Banner","All Music")) );
	//	TEXTUREMAN->CacheTexture( SongBannerTexture(THEME->GetPathG("Common","fallback banner")) );
	//	TEXTUREMAN->CacheTexture( SongBannerTexture(THEME->GetPathG("Banner","roulette")) );
	//	TEXTUREMAN->CacheTexture( SongBannerTexture(THEME->GetPathG("Banner","random")) );
	//	TEXTUREMAN->CacheTexture( SongBannerTexture(THEME->GetPathG("Banner","Sort")) );
	//	TEXTUREMAN->CacheTexture( SongBannerTexture(THEME->GetPathG("Banner","Mode")) );

	//	TEXTUREMAN->CacheTexture( SongBannerTexture(THEME->GetPathG("Disc","All Music")) );
	//	TEXTUREMAN->CacheTexture( SongBannerTexture(THEME->GetPathG("Common","fallback disc")) );
	//	TEXTUREMAN->CacheTexture( SongBannerTexture(THEME->GetPathG("Disc","roulette")) );
	//	TEXTUREMAN->CacheTexture( SongBannerTexture(THEME->GetPathG("Disc","random")) );
	//	TEXTUREMAN->CacheTexture( SongBannerTexture(THEME->GetPathG("Disc","Sort")) );
	//	TEXTUREMAN->CacheTexture( SongBannerTexture(THEME->GetPathG("Disc","Mode")) );
	//}
}

bool Banner::Load( RageTextureID ID, bool bDisc )
{
	if( ID.filename == "" )
	{
		if( bDisc )
			ID = THEME->GetPathToG("Common fallback disc");
		else
			ID = THEME->GetPathToG("Common fallback banner");
	}

	if( PREFSMAN->m_BannerCache == PrefsManager::BNCACHE_OFF || !m_bBNCache )
		ID = SongBannerTexture(ID);
	else
	{
		BANNERCACHE->LoadBanner( ID.filename );
		ID = BANNERCACHE->LoadCachedBanner( ID.filename );
	}

	m_fPercentScrolling = 0;
	m_bScrolling = false;

	TEXTUREMAN->DisableOddDimensionWarning();
	TEXTUREMAN->VolatileTexture( ID );
	bool ret = Sprite::Load( ID );
	TEXTUREMAN->EnableOddDimensionWarning();

	return ret;
};

void Banner::Update( float fDeltaTime )
{
	Sprite::Update( fDeltaTime );

	if( m_bScrolling )
	{
        m_fPercentScrolling += fDeltaTime/2;
		m_fPercentScrolling -= (int)m_fPercentScrolling;

		const RectF *pTextureRect = m_pTexture->GetTextureCoordRect(0);

		float fTexCoords[8] =
		{
			0+m_fPercentScrolling, pTextureRect->top,		// top left
			0+m_fPercentScrolling, pTextureRect->bottom,		// bottom left
			1+m_fPercentScrolling, pTextureRect->bottom,		// bottom right
			1+m_fPercentScrolling, pTextureRect->top,		// top right
		};
		Sprite::SetCustomTextureCoords( fTexCoords );
	}
}

void Banner::SetScrolling( bool bScroll, float Percent)
{
	m_bScrolling = bScroll;
	m_fPercentScrolling = Percent;

	/* Set up the texture coord rects for the current state. */
	Update(0);
}

void Banner::LoadFromSong( Song* pSong, bool bDisc )		// NULL means no song
{
	if( pSong == NULL )
		LoadFallback( bDisc );
	else if( !bDisc && pSong->HasBanner() )
		Load( pSong->GetBannerPath(), bDisc );
	else if( bDisc && pSong->HasDisc() )
		Load( pSong->GetDiscPath(), bDisc );
	else
		LoadFallback( bDisc );

	m_bScrolling = false;
}

void Banner::LoadAllMusic( bool bDisc )
{
	CString sPath = bDisc ? THEME->GetPathG("Disc","All Music") : THEME->GetPathG("Banner","All Music");
	Load( sPath, bDisc );

	m_bScrolling = false;
}

void Banner::LoadSort( bool bDisc )
{
	CString sPath = bDisc ? THEME->GetPathG("Disc","Sort") : THEME->GetPathG("Banner","Sort");
	Load( sPath, bDisc );

	m_bScrolling = false;
}

void Banner::LoadMode( bool bDisc )
{
	CString sPath = bDisc ? THEME->GetPathG("Disc","Mode") : THEME->GetPathG("Banner","Mode");
	Load( sPath, bDisc );

	m_bScrolling = false;
}

void Banner::LoadFromGroup( CString sGroupName, bool bDisc )
{
	CString sPath = bDisc ? SONGMAN->GetGroupDiscPath( sGroupName ) : SONGMAN->GetGroupBannerPath( sGroupName );

	if( sPath != "" )
		Load( sPath, bDisc );
	else
		LoadFallback( bDisc );

	m_bScrolling = false;
}

void Banner::LoadFromCourse( Course* pCourse, bool bDisc )		// NULL means no course
{
	if( pCourse == NULL )
		LoadFallback( bDisc );
	else if( !bDisc && pCourse->HasBanner() )
		Load( pCourse->m_sBannerPath, bDisc );
	else if( bDisc && pCourse->HasDisc() )
		Load( pCourse->m_sDiscPath, bDisc );
	else
		LoadFallback( bDisc );

	m_bScrolling = false;
}

void Banner::LoadCardFromCharacter( Character* pCharacter, bool bDisc )
{
	ASSERT( pCharacter );

	if( pCharacter->GetCardPath() != "" )
		Load( pCharacter->GetCardPath(), bDisc );
	else
		LoadFallback( bDisc );

	m_bScrolling = false;
}

void Banner::LoadIconFromCharacter( Character* pCharacter, bool bDisc )
{
	ASSERT( pCharacter );

	if( pCharacter->GetIconPath() != "" )
		Load( pCharacter->GetIconPath(), bDisc );
	else if( pCharacter->GetCardPath() != "" )
		Load( pCharacter->GetCardPath(), bDisc );
	else
		LoadFallback( bDisc );

	m_bScrolling = false;
}

void Banner::LoadTABreakFromCharacter( Character* pCharacter, bool bDisc )
{
	if( pCharacter == NULL )
		Load( THEME->GetPathToG("Common fallback takingabreak"), bDisc );
	else
	{
		Load( pCharacter->GetTakingABreakPath(), bDisc );
		m_bScrolling = false;
	}
}

void Banner::LoadFallback( bool bDisc )
{
	CString sPath = bDisc ? THEME->GetPathToG("Common fallback disc") : THEME->GetPathToG("Common fallback banner");
	Load( sPath, bDisc );
}

void Banner::LoadRoulette( bool bDisc )
{
	CString sPath = bDisc ? THEME->GetPathToG("Disc roulette") : THEME->GetPathToG("Banner roulette");
	Load( sPath, bDisc );

	m_bScrolling = (bool)SCROLL_ROULETTE;
}

void Banner::LoadRandom( bool bDisc )
{
	CString sPath = bDisc ? THEME->GetPathToG("Disc random") : THEME->GetPathToG("Banner random");
	Load( sPath, bDisc );

	m_bScrolling = (bool)SCROLL_RANDOM;
}


/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * Contains code (c) 2001-2004 Chris Danford.
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
