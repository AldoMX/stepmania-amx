/*
 * This stores a single note pattern for a song.
 *
 * We can have too much data to keep everything decompressed as NoteData, so most
 * songs are kept in memory compressed as SMData until requested.  NoteData is normally
 * not requested casually during gameplay; we can move through screens, the music
 * wheel, etc. without touching any NoteData.
 *
 * To save more memory, if data is cached on disk, read it from disk on demand.  Not
 * all Steps will have an associated file for this purpose.  (Profile edits don't do
 * this yet.)
 *
 * Data can be on disk (always compressed), compressed in memory, and uncompressed in
 * memory.
 */

#include "global.h"
#include "Steps.h"
#include "StepsUtil.h"
#include "Song.h"
#include "Steps.h"
#include "IniFile.h"
#include "RageUtil.h"
#include "RageLog.h"
#include "NoteData.h"
#include "GameInput.h"
#include "RageException.h"
#include "MsdFile.h"
#include "GameManager.h"
#include "NoteDataUtil.h"
#include "ProfileManager.h"
#include "PrefsManager.h"
#include "NotesLoaderSM.h"
#include "TimingData.h"

const int MAX_DESCRIPTION_LENGTH = 20;

Steps::Steps()
{
	Reset();
}

Steps::Steps( unsigned int hash )
{
	Reset();
	m_uHash = hash;
}

Steps::~Steps()
{
	delete notes;
}

void Steps::Reset()
{
	m_StepsType = STEPS_TYPE_INVALID;
	m_LoadedFromProfile = PROFILE_SLOT_INVALID;
	m_uHash = 0;
	m_Difficulty = DIFFICULTY_INVALID;
	m_bHiddenDifficulty = false;
	m_iMeter = 0;

	notes = NULL;
	notes_comp = "";
	parent = NULL;

	m_DisplayBPM = BPM_REAL;

	m_Timing.Reset();
}

void Steps::SetNoteData( const NoteData* pNewNoteData )
{
	ASSERT( pNewNoteData->GetNumTracks() == GameManager::StepsTypeToNumTracks(m_StepsType) );

	DeAutogen();

	delete notes;
	notes = new NoteData(*pNewNoteData);
	m_Timing.m_fFirstBeat = notes->GetFirstBeat();
	m_Timing.m_fLastBeat = notes->GetLastBeat();

	NoteDataUtil::GetSMNoteDataString( *notes, notes_comp );
	m_uHash = GetHashForString( notes_comp );
}

void Steps::GetNoteData( NoteData* pNoteDataOut ) const
{
	ASSERT(this);
	ASSERT(pNoteDataOut);

	Decompress();

	if( notes != NULL )
		*pNoteDataOut = *notes;
	else
	{
		pNoteDataOut->ClearAll();
		pNoteDataOut->SetNumTracks( GameManager::StepsTypeToNumTracks(m_StepsType) );
	}
}

void Steps::SetSMNoteData( const CString &notes_comp_ )
{
	delete notes;
	notes = NULL;

	notes_comp = notes_comp_;
	m_uHash = GetHashForString( notes_comp );
}

void Steps::GetSMNoteData( CString &notes_comp_out ) const
{
	if( notes_comp.empty() )
	{
		if( !notes )
		{
			/* no data is no data */
			notes_comp_out = "";
			return;
		}

		NoteDataUtil::GetSMNoteDataString( *notes, notes_comp );
	}

	notes_comp_out = notes_comp;
}

float Steps::PredictMeter() const
{
	bool bPIU = false;

	switch( m_StepsType )
	{
	case STEPS_TYPE_PUMP_SINGLE:
	case STEPS_TYPE_PUMP_DOUBLE:
	case STEPS_TYPE_PUMP_HALFDOUBLE:
	case STEPS_TYPE_PUMP_COUPLE:
		bPIU = true;
	}

	if( bPIU )
	{
		// No notes = can't calculate...
		if( !notes )
			return 0;

		//	Aldo_MX: I'm calculating meter based in the possible hit combinations.
		//	It will just give a "base value", you need to increase the value if you
		//	use tricky step combinations (ex. twists) and/or it's a gimmicky chart.
		//
		//	In other words, this value should be considered as the "minimum possible"
		//	meter value using 1 - 10 difficulty scale.
		//
		//	The following formula is used ONLY with checkpoint combinations possible with
		//	one press (ex. quads in double)*:
		//
		//			- 1 / ( NumCheckpoints * 4 )
		//
		//	* Even if the song has too many single holds (ex. Love is a Danger Zone CZ),
		//	the substracted value should not be greater than 25% of the total value.
		//
		//	The following formula is used with any other combination:
		//
		//			SQRT( ( NumCheckpoints ^ 1.5 ) + ( NumSteps ^ 1.5 ) )
		//
		//	If the combination is NOT possible with one press (ex. quads in single),
		//	the result gets multiplied by 2.
		//
		//	The total value gets divided by the steps length in seconds to get
		//	the predicted meter value.
		//
		//	We multiply by 3 and substract 1.5 (substract only in Single Modes)
		//	to get meter values similar to NX2 ~ NXA

		int iMaxTracks = 4;

		switch( m_StepsType )
		{
		case STEPS_TYPE_PUMP_SINGLE:
		case STEPS_TYPE_PUMP_COUPLE:
			iMaxTracks--;
		}

		float fStepsLengthInSeconds = m_Timing.GetElapsedTimeFromBeat( notes->GetLastBeat() );

		float fBaseValue = 0;
		float fSubstractedValue = 0;

		// Load TimingData to apply tickcounts
		NoteDataUtil::ParseTiming( *notes, m_Timing, true );

		for( int r=0; r<notes->GetNumRows(); ++r )
		{
			int iTracks = notes->GetNumTracksWithCheckpoint( r, false ),
				// Do not count hold heads as checkpoints!
				// Aldo_MX: Phantom-style Holds will increase the base difficulty :/, but
				// holds should not decrease the difficulty before a certain amount of combo...
				iTracksCP = notes->GetNumTracksWithCheckpoint( r, true, true );

			// No Steps in this row
			if( iTracks <= 0 )
				continue;

			int iTracksStep = iTracks - iTracksCP;

			// Only checkpoints in this row
			if( iTracksStep <= 0 )
			{
				// Combinations possible with one press (Formula 1)
				if( iTracksCP <= iMaxTracks )
					fSubstractedValue += 1 / ( iTracksCP * 4 );

				// Combinations NOT possible with one press (Formula 2 multiplied by 2)
				else
					fBaseValue += sqrt( powf( iTracksCP * 1.f, 1.5f ) ) * 2;
			}
			// Checkpoints and steps or only steps in this row
			else
			{
				// Any combination (Formula 2)
				float fAddValue = sqrt( powf( (float)iTracksCP, 1.5f ) + powf( (float)iTracksStep, 1.5f ) );

				// Combinations NOT possible with one press, multiply by 2
				if( iTracks > iMaxTracks )
					fAddValue *= 2;

				fBaseValue += fAddValue;
			}
		}

		// Revert NoteData
		NoteDataUtil::UnparseTiming( *notes, m_Timing, true );

		// Substract a maximum of 25%
		fSubstractedValue = min( fSubstractedValue, fBaseValue * .25f );
		fBaseValue -= fSubstractedValue;

		// Divide by the steps length in seconds
		fBaseValue /= fStepsLengthInSeconds;

		// HACK - Aldo_MX: Adjust the final value a little bit...
		fBaseValue *= 3.0f;

		if( iMaxTracks == 3 )
			fBaseValue -= 1.50000001f;

		// Lowest value should be 1
		fBaseValue = max( fBaseValue, 1.f );

		return fBaseValue;
	}
	else
	{
		float pMeter = 0.775f;

		const float RadarCoeffs[NUM_RADAR_CATEGORIES] =
		{
			10.1f, 5.27f, -0.905f, -1.10f, 2.86f,
			0,0,0,0,0,0
		};

		for( int r = 0; r < NUM_RADAR_CATEGORIES; ++r )
			pMeter += this->GetRadarValues()[r] * RadarCoeffs[r];

		const float DifficultyCoeffs[NUM_DIFFICULTIES] =
		{
			-0.877f, -0.877f, 0, 0.722f, 0.722f, 0
		};

		pMeter += DifficultyCoeffs[this->GetDifficulty()];

		// Init non-radar values
		const float SV = this->GetRadarValues()[RADAR_STREAM] * this->GetRadarValues()[RADAR_VOLTAGE];
		const float ChaosSquare = this->GetRadarValues()[RADAR_CHAOS] * this->GetRadarValues()[RADAR_CHAOS];
		pMeter += -6.35f * SV;
		pMeter += -2.58f * ChaosSquare;

		if (pMeter < 1)
			pMeter = 1;

		return pMeter;
	}
}

void Steps::TidyUpData()
{
	if( GetDifficulty() == DIFFICULTY_INVALID )
		SetDifficulty( StringToDifficulty(GetDescription()) );

	if( GetDifficulty() == DIFFICULTY_INVALID )
	{
		bool bPIU = false;

		switch( m_StepsType ) {
		case STEPS_TYPE_PUMP_SINGLE:
		case STEPS_TYPE_PUMP_DOUBLE:
		case STEPS_TYPE_PUMP_HALFDOUBLE:
		case STEPS_TYPE_PUMP_COUPLE:
			bPIU = true;
		}

		if( bPIU ) {
			if( GetMeter() <= 2 )		SetDifficulty( DIFFICULTY_BEGINNER );
			else if( GetMeter() <= 5 )	SetDifficulty( DIFFICULTY_EASY );
			else if( GetMeter() <= 12 )	SetDifficulty( DIFFICULTY_MEDIUM );
			else if( GetMeter() <= 21 )	SetDifficulty( DIFFICULTY_HARD );
			else						SetDifficulty( DIFFICULTY_CHALLENGE );
		}
		else {
			if( GetMeter() <= 1 )		SetDifficulty( DIFFICULTY_BEGINNER );
			else if( GetMeter() <= 3 )	SetDifficulty( DIFFICULTY_EASY );
			else if( GetMeter() <= 6 )	SetDifficulty( DIFFICULTY_MEDIUM );
			else						SetDifficulty( DIFFICULTY_HARD );
		}
	}

	if( GetMeter() < 1 ) // meter is invalid
		SetMeter( (int)PredictMeter() );

	if( (int)m_sDescription.size() > MAX_DESCRIPTION_LENGTH )
		m_sDescription = m_sDescription.Left( MAX_DESCRIPTION_LENGTH );

	//if( !m_bLoadedFromSM )
	//	m_bHiddenDifficulty = false;

	// Make sure the first Speed segment starts at beat 0.
	if( m_Timing.m_SpeedSegments[0].m_fBeat != 0 )
		m_Timing.m_SpeedSegments[0].m_fBeat = 0;

	// Make sure the first Tickcount segment starts at beat 0.
	if( m_Timing.m_TickcountSegments[0].m_fBeat != 0 )
		m_Timing.m_TickcountSegments[0].m_fBeat = 0;

	// Make sure the first Multiplier segment starts at beat 0.
	if( m_Timing.m_MultiplierSegments[0].m_fBeat != 0 )
		m_Timing.m_MultiplierSegments[0].m_fBeat = 0;

	if( m_DisplayBPM == BPM_REAL || m_DisplayBPM != BPM_RANDOM && m_BPMRange.IsRandom() )
		m_BPMRange = BPMRange(&m_Timing);

	TrimLeft( m_sCredit );
	TrimRight( m_sCredit );

	if( m_sCredit == "" )
		m_sCredit = "Unknown StepMaker";
}

void Steps::Decompress() const
{
	if( notes )
		return;	// already decompressed

	if( parent )
	{
		// get autogen notes
		NoteData pdata;
		parent->GetNoteData(&pdata);

		notes = new NoteData;

		int iNewTracks = GameManager::StepsTypeToNumTracks(m_StepsType);

		if( this->m_StepsType == STEPS_TYPE_LIGHTS_CABINET )
			NoteDataUtil::LoadTransformedLights( pdata, *notes, iNewTracks );
		else
		{
			NoteDataUtil::LoadTransformedSlidingWindow( pdata, *notes, iNewTracks );
			NoteDataUtil::FixImpossibleRows( *notes, m_StepsType );
		}
		return;
	}

	if( !m_sFilename.empty() && notes_comp.empty() )
	{
		// We have data on disk and not in memory.  Load it.
		Song s;
		SMLoader ld;
		if( !ld.LoadFromSMFile( m_sFilename, s, true ) )
		{
			LOG->Warn( "Couldn't load \"%s\"", m_sFilename.c_str() );
			return;
		}

		// Find the steps.
		StepsID ID;
		ID.FromSteps( this );

		Steps *pSteps = ID.ToSteps( &s, true, false );	// don't use cache

		if( pSteps == NULL )
		{
			LOG->Warn( "Couldn't find %s in \"%s\"", ID.ToString().c_str(), m_sFilename.c_str() );
			return;
		}

		if(!parent)
		{
			int meter = pSteps->GetMeter();
			Difficulty diff = pSteps->GetDifficulty();
			if( !ld.LoadStepTimingFromFile( m_sFilename, pSteps->m_Timing, pSteps->m_StepsType, diff, meter, pSteps->m_sDescription ) )
			{
				LOG->Warn( "Couldn't load Step Timing for \"%s %d\" in \"%s\"", DifficultyToString(diff).c_str(), meter, m_sFilename.c_str() );
				return;
			}
		}

		pSteps->GetSMNoteData( notes_comp );
	}

	if( !notes_comp.empty() )
	{
		// load from compressed
		notes = new NoteData;
		notes->SetNumTracks( GameManager::StepsTypeToNumTracks(m_StepsType) );

		NoteDataUtil::LoadFromSMNoteDataString(*notes, notes_comp );
	}
	// else {}	// there is no data, do nothing
}

void Steps::Compress() const
{
	if( !m_sFilename.empty() )
	{
		// We have a file on disk; clear all data in memory.
		delete notes;
		notes = NULL;
		notes_comp = "";
		return;
	}

	if( notes_comp.empty() )
	{
		if(!notes) return; /* no data is no data */
		NoteDataUtil::GetSMNoteDataString( *notes, notes_comp );
	}

	delete notes;
	notes = NULL;
}

// Copy our parent's data.  This is done when we're being changed from autogen to normal. (needed?)
void Steps::DeAutogen()
{
	if(!parent)
		return; // OK

	Decompress();	// fills in notes with sliding window transform

	m_sDescription		= Real()->m_sDescription;
	m_Difficulty		= Real()->m_Difficulty;
	m_iMeter			= Real()->m_iMeter;
	m_bHiddenDifficulty	= false;
	m_RadarValues		= Real()->m_RadarValues;

	m_DisplayBPM = Real()->m_DisplayBPM;
	m_BPMRange = Real()->m_BPMRange;
	m_Timing = Real()->m_Timing;

	parent = NULL;

	Compress();
}

void Steps::AutogenFrom( const Steps *parent_, StepsType ntTo )
{
	parent = parent_;
	m_Timing = parent_->m_Timing;
	m_DisplayBPM = parent_->m_DisplayBPM;
	m_BPMRange = parent_->m_BPMRange;
	m_StepsType = ntTo;
}

void Steps::CopyFrom( Steps* pSource, StepsType ntTo )	// pSource does not have to be of the same StepsType!
{
	m_StepsType = ntTo;
	NoteData noteData;
	pSource->GetNoteData( &noteData );
	noteData.SetNumTracks( GameManager::StepsTypeToNumTracks(ntTo) );

	this->SetNoteData( &noteData );
	this->SetDescription( "Copied from "+pSource->GetDescription() );
	this->SetDifficulty( pSource->GetDifficulty() );
	this->SetMeter( pSource->GetMeter() );
	this->m_bHiddenDifficulty = false;
	this->SetRadarValues( pSource->GetRadarValues() );

	this->m_DisplayBPM = pSource->m_DisplayBPM;
	this->m_BPMRange = pSource->m_BPMRange;
	this->m_Timing = pSource->m_Timing;
}

void Steps::CreateBlank( StepsType ntTo, TimingData& pTimingFrom )
{
	m_StepsType = ntTo;
	NoteData noteData;
	noteData.SetNumTracks( GameManager::StepsTypeToNumTracks(ntTo) );
	this->SetNoteData( &noteData );
	this->m_Timing = pTimingFrom;
	this->m_BPMRange = BPMRange(&pTimingFrom);
}

const Steps *Steps::Real() const
{
	ASSERT( this );

	if( parent )
		return parent;

	return this;
}

bool Steps::IsAutogen() const
{
	return parent != NULL;
}

void Steps::SetFile( CString fn )
{
	m_sFilename = fn;
}

void Steps::SetDescription( CString desc )
{
	DeAutogen();
	m_sDescription = desc;
}

void Steps::SetDifficulty( Difficulty dc )
{
	DeAutogen();
	m_Difficulty = min(dc, DIFFICULTY_EDIT);
}

void Steps::SetMeter( int iMeter )
{
	DeAutogen();
	m_iMeter = iMeter;
}

void Steps::SetRadarValues( const RadarValues& v )
{
	DeAutogen();
	m_RadarValues = v;
}

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * Contains code (c) 2001-2004 Chris Danford, Glenn Maynard, David Wilson.
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
