// Don't forget to also change ProductInfo.inc!
#ifndef PRODUCT_INFO_H
#define PRODUCT_INFO_H

#include "global.h"
#include "RageUtil.h"

const CString PRODUCT_NAME("StepMania AMX");
const CString PRODUCT_ABBR("SMA");

const CString PRODUCT_CYCLE("Beta");
const CString PRODUCT_CYCLE_ABBR("b");

const int PRODUCT_VER_MAJOR = 5;
const int PRODUCT_VER_MINOR = 9;
const int PRODUCT_VER_PATCH = 5;
const CString PRODUCT_VER( ssprintf("%d.%d%s", PRODUCT_VER_MAJOR, PRODUCT_VER_MINOR, PRODUCT_VER_PATCH > 0 ? ssprintf(".%d", PRODUCT_VER_PATCH).c_str() : "") );

const CString PRODUCT_NAME_VER( ssprintf("%s%s%s %s", PRODUCT_NAME.c_str(), PRODUCT_CYCLE.empty() ? "" : " ", PRODUCT_CYCLE.c_str(), PRODUCT_VER.c_str() ) );
const CString PRODUCT_ABBR_VER( ssprintf("%s%s%s %s", PRODUCT_ABBR.c_str(), PRODUCT_CYCLE.empty() ? "" : " ", PRODUCT_CYCLE.c_str(), PRODUCT_VER.c_str() ) );

const CString API_HOST( "stepmania.amx.io" );
const CString HOMEPAGE_URL( "http://stepmania.amx.io/" );
const CString USER_AGENT( ssprintf("%s (+%s)", PRODUCT_NAME_VER.c_str(), HOMEPAGE_URL.c_str()) );

const CString ERROR_REPORT_URL( "https://www.facebook.com/groups/StepManiaAMX/" );
const CString CRASH_REPORT_URL( "https://www.facebook.com/groups/StepManiaAMX/" );
const CString SMA_FILE_VERSION( "1.0.0" );

#define LOG_DIR "Logs/"

#if defined(HAVE_VERSION_INFO)
extern const char *build_commit;
extern const char *build_time;
#endif

#endif

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * Contains code (c) 2003-2005 Chris Danford.
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
