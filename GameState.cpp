#include "global.h"
#include "GameState.h"
#include "IniFile.h"
#include "PrefsManager.h"
#include "InputMapper.h"
#include "Song.h"
#include "Course.h"
#include "RageLog.h"
#include "RageUtil.h"
#include "SongManager.h"
#include "Steps.h"
#include "NoteSkinManager.h"
#include "ModeChoice.h"
#include "NoteFieldPositioning.h"
#include "Character.h"
#include "UnlockSystem.h"
#include "AnnouncerManager.h"
#include "ProfileManager.h"
#include "arch/arch.h"
#include "arch/Dialog/Dialog.h"
#include "ThemeManager.h"
#include "LightsManager.h"
#include "RageFile.h"
#include "Bookkeeper.h"
#include "MemoryCardManager.h"
#include "StageStats.h"
#include "GameConstantsAndTypes.h"
#include "StepMania.h"
#include "CommonMetrics.h"
#include "ScreenManager.h"
#include "Foreach.h"
#include "Game.h"

#include <ctime>
#include <set>

GameState*	GAMESTATE = NULL;	// global and accessable from anywhere in our program

#define CHARACTERS_DIR "Characters/"
#define NAMES_BLACKLIST_FILE "Data/NamesBlacklist.dat"
#define POSITIONING_FILE "Data/Positioning.ini"

GameState::GameState()
{
	m_pPosition = NULL;
	m_pCurStyle = NULL;

	m_pCurGame = NULL;
	m_timeGameStarted.SetZero();
	m_bIsOnSystemMenu = false;

	m_bLoadingSortFonts = false;

	m_bShowHiddenSongs = false;

	ReloadCharacters();

	m_iNumTimesThroughAttract = -1;	// initial screen will bump this up to 0
	m_iRoundSeed = m_iGameSeed = 0;

	m_PlayMode = PLAY_MODE_INVALID; // used by IsPlayerEnabled before the first screen
	FOREACH_PlayerNumber( p )
		m_bSideIsJoined[p] = false; // used by GetNumSidesJoined before the first screen

	ResetAutoPlay();

	/* Don't reset yet; let the first screen do it, so we can
	 * use PREFSMAN and THEME. */
	m_bPlaying = false;
	m_bEditing = false;

#if defined( WITH_COIN_MODE )
	m_iCoins = 0;
#endif

//	Reset();
}

GameState::~GameState()
{
	delete m_pPosition;
	for( unsigned i=0; i<m_pCharacters.size(); i++ )
		delete m_pCharacters[i];
}

void GameState::ApplyCmdline()
{
	int i;

	/* We need to join players before we can set the style. */
	CString sPlayer;
	for( i = 0; GetCommandlineArgument( "player", &sPlayer, i ); ++i )
	{
		int pn = atoi( sPlayer )-1;
		if( !IsAnInt( sPlayer ) || pn < 0 || pn >= NUM_PLAYERS )
			RageException::Throw( "Invalid argument \"--player=%s\"", sPlayer.c_str() );

		this->JoinPlayer( (PlayerNumber) pn );
	}

	CString sMode;
	for( i = 0; GetCommandlineArgument( "mode", &sMode, i ); ++i )
	{
		ModeChoice m;
		m.Load( 0, sMode );
		CString why;
		if( !m.IsPlayable(&why) )
			RageException::Throw( "Can't apply mode \"%s\": %s", sMode.c_str(), why.c_str() );

		m.ApplyToAllPlayers();
	}
}

void GameState::Reset( bool bEditMenu )
{
	EndGame();

	MEMCARDMAN->LockCards( true );

	ASSERT( THEME );

	m_timeGameStarted.SetZero();

	if( !bEditMenu )
		m_pCurStyle = NULL;

	FOREACH_PlayerNumber( p )
		m_bSideIsJoined[p] = false;

	m_bPlayersFinalized = false;

	if( !bEditMenu )
		m_MasterPlayerNumber = PLAYER_INVALID;

	m_mapEnv.clear();
	m_sPreferredGroup	= GROUP_ALL_MUSIC;
	m_bChangedFailType = false;

	FOREACH_PlayerNumber( p )
	{
		m_PreferredDifficulty[p] = DIFFICULTY_INVALID;
		m_PreferredCourseDifficulty[p] = DIFFICULTY_MEDIUM;
	}

	m_SortOrder = SORT_INVALID;
	m_PlayMode = PLAY_MODE_INVALID;
	m_bPlaying = false;
	m_bEditing = false;
	m_EditMode = MODE_EDITING;
	m_bDemonstrationOrJukebox = false;
	m_bJukeboxUsesModifiers = false;
	m_iCurrentStage = 0;
	m_iCurrentStageIndex = 0;
	m_bAllow2ndExtraStage = true;
	m_iMusicWheelDirection = 0;

	FOREACH_PlayerNumber( p )
		m_BeatToNoteSkinRev[p] = 0;

	m_iNumStagesOfThisSong = 0;

	NOTESKIN->RefreshNoteSkinData( this->m_pCurGame );

	m_iGameSeed = rand();
	m_iRoundSeed = rand();

	if( !bEditMenu )
		m_pCurSong = NULL;

	m_pPreferredSong = NULL;

	if( !bEditMenu )
	{
		FOREACH_PlayerNumber( p )
			m_pCurSteps[p] = NULL;
	}

	m_pCurCourse = NULL;
	m_pPreferredCourse = NULL;
	FOREACH_PlayerNumber( p )
		m_pCurTrail[p] = NULL;

	SAFE_DELETE( m_pPosition );
	m_pPosition = new NoteFieldPositioning( POSITIONING_FILE );

	FOREACH_PlayerNumber( p )
		m_bAttackBeganThisUpdate[p] = false;

	ResetMusicStatistics();
	ResetStageStatistics();
	ResetAutoPlay();

	SONGMAN->FreeAllPlayerSongs();
	SONGMAN->FreeAllLoadedFromProfiles();
	SONGMAN->UpdateBest();
	SONGMAN->UpdateShuffled();

	/* We may have cached trails from before everything was loaded (eg. from before
	 * SongManager::UpdateBest could be called).  Erase the cache. */
	SONGMAN->RegenerateNonFixedCourses();

	g_vPlayedStageStats.clear();

	FOREACH_PlayerNumber( p )
	{
		m_PlayerOptions[p].Init();
		m_CurrentPlayerOptions[p].Init();
		m_StoredPlayerOptions[p].Init();
		LoadNSOptions(p);
	}
	m_SongOptions.Init();

	FOREACH_PlayerNumber(p)
	{
		// I can't think of a good reason to have both game-specific
		// default mods and theme specific default mods.  We should choose
		// one or the other. -Chris
		// Having default modifiers in prefs is needed for several things.
		// The theme setting is for eg. BM being reverse by default.  (This
		// could be done in the title menu ModeChoice, but then it wouldn't
		// affect demo, and other non-gameplay things ...) -glenn
		ApplyModifiers( p, DEFAULT_MODIFIERS );
		ApplyModifiers( p, PREFSMAN->m_sDefaultModifiers );
	}

	FOREACH_PlayerNumber(p)
	{
		if( PREFSMAN->m_ShowDancingCharacters == PrefsManager::CO_RANDOM)
			m_pCurCharacters[p] = GetRandomCharacter();
		else
			m_pCurCharacters[p] = GetDefaultCharacter();
		ASSERT( m_pCurCharacters[p] );
	}

	FOREACH_PlayerNumber(p)
	{
		m_fSuperMeterGrowthScale[p] = 1;
		m_iCpuSkill[p] = 5;
	}


	LIGHTSMAN->SetLightsMode( LIGHTSMODE_ATTRACT );

	if( !bEditMenu )
		ApplyCmdline();
}

void GameState::JoinPlayer( PlayerNumber pn )
{
	m_bSideIsJoined[pn] = true;
	if( m_MasterPlayerNumber == PLAYER_INVALID )
		m_MasterPlayerNumber = pn;

	// if first player to join, set start time
	if( GetNumSidesJoined() == 1 )
		BeginGame();
}

void GameState::BeginGame()
{
	m_timeGameStarted.Touch();

	m_vpsNamesThatWereFilled.clear();

	// Play attract on the ending screen, then on the ranking screen
	// even if attract sounds are set to off.
	m_iNumTimesThroughAttract = -1;

	MEMCARDMAN->LockCards( false );
}

void GameState::PlayersFinalized()
{
	if( m_bPlayersFinalized )
		return;

	// Dump any preloaded player songs; we'll reload them
	SONGMAN->FreeAllPlayerSongs();

	m_bPlayersFinalized = true;

	MEMCARDMAN->LockCards( true );

	// apply saved default modifiers if any
	FOREACH_HumanPlayer( pn )
	{
		PROFILEMAN->LoadFirstAvailableProfile( pn );	// load full profile

		if( !PROFILEMAN->IsUsingProfile(pn) )
			continue;	// skip

		Profile* pProfile = PROFILEMAN->GetProfile(pn);

		if( pProfile->m_bUsingProfileDefaultModifiers )
		{
			m_PlayerOptions[pn].Init();
			ApplyModifiers( pn, pProfile->m_sDefaultModifiers );
		}
		// Only set the sort order if it wasn't already set by a ModeChoice (or by an earlier profile)
		if( m_SortOrder == SORT_INVALID && pProfile->m_SortOrder != SORT_INVALID )
			m_SortOrder = pProfile->m_SortOrder;
		if( pProfile->m_LastDifficulty != DIFFICULTY_INVALID )
			m_PreferredDifficulty[pn] = pProfile->m_LastDifficulty;
		if( pProfile->m_LastCourseDifficulty != DIFFICULTY_INVALID )
			m_PreferredCourseDifficulty[pn] = pProfile->m_LastCourseDifficulty;
		if( m_pPreferredSong == NULL )
			m_pPreferredSong = pProfile->m_lastSong.ToSong();
		if( m_pPreferredCourse == NULL )
			m_pPreferredCourse = pProfile->m_lastCourse.ToCourse();

		if( PREFSMAN->m_bPlayerSongs )
			SONGMAN->LoadPlayerSongs( pn );
	}

	SONGMAN->LoadAllFromProfiles();

	FOREACH_PlayerNumber( pn )
	{
		if( !IsHumanPlayer(pn) )
			ApplyModifiers( pn, DEFAULT_CPU_MODIFIERS );
	}
}

void GameState::LoadNSOptions( PlayerNumber pn )
{
	if( !IsPlayerEnabled( pn ) )
		return;

	ASSERT( m_pCurStyle );

	CString& sNoteSkin = m_PlayerOptions[pn].m_sNoteSkin;
	CString sStepsType;

	if (IsCourseMode()) {
		ASSERT(m_pCurTrail[pn]);
		sStepsType = StepsTypeToString(m_pCurTrail[pn]->m_StepsType);
	}
	else {
		ASSERT(m_pCurSteps[pn]);
		sStepsType = StepsTypeToString(m_pCurSteps[pn]->m_StepsType);
	}

	m_fNSArrowSize[pn]		= NOTESKIN->GetMetricF( sNoteSkin, sStepsType, "ArrowSize" );
	m_fNSColSpacing[pn]		= NOTESKIN->GetMetricF( sNoteSkin, sStepsType, "ColSpacing" );
	m_fNSZoom[pn]			= NOTESKIN->GetMetricF( sNoteSkin, sStepsType, "Zoom" );
	m_bNSOverrideDim[pn]	= NOTESKIN->GetMetricB( sNoteSkin, "GhostArrowDim", "AlwaysShow" );
	m_bNSOverrideBright[pn]	= NOTESKIN->GetMetricB( sNoteSkin, "GhostArrowBright", "AlwaysShow" );

	switch( m_pCurStyle->m_StyleType )
	{
	case Style::ONE_PLAYER_ONE_CREDIT:
	case Style::TWO_PLAYERS_TWO_CREDITS:
		m_fNSCenterX[pn] = NOTESKIN->GetMetricF( sNoteSkin, sStepsType, ssprintf("CenterP%dX", pn+1) );
		break;

	case Style::ONE_PLAYER_TWO_CREDITS:
		m_fNSCenterX[pn] = NOTESKIN->GetMetricF( sNoteSkin, sStepsType, "CenterDoubleX" );
		break;

	default:
		ASSERT(0);
	}

	vector<int> vDrawOrder((size_t)m_pCurStyle->m_iColsPerPlayer, -1);
	if( NOTESKIN->HasMetric(sNoteSkin, sStepsType, "DrawOrder") )
	{
		CStringArray saNSDrawOrder;
		split( NOTESKIN->GetMetric(sNoteSkin, sStepsType, "DrawOrder"), ",", saNSDrawOrder );

		if( (int)saNSDrawOrder.size() != m_pCurStyle->m_iColsPerPlayer )
		{
			Dialog::OK(
				ssprintf(
					"Invalid DrawOrder value. Expecting %d values, %u provided.\nNoteskin: %s, Steps Type: %s",
					m_pCurStyle->m_iColsPerPlayer,
					saNSDrawOrder.size(),
					sNoteSkin.c_str(),
					sStepsType.c_str()
				),
				"LoadNSOptions"
			);
		}
		else
		{
			for( unsigned u=0; u<saNSDrawOrder.size(); u++ )
			{
				int iOrder = atoi(saNSDrawOrder[u]);
				if( iOrder > m_pCurStyle->m_iColsPerPlayer || iOrder <= 0 )
				{
					Dialog::OK(
						ssprintf(
							"Invalid DrawOrder value. %d is out of range.\nNoteskin: %s, Steps Type: %s",
							iOrder,
							sNoteSkin.c_str(),
							sStepsType.c_str()
						),
						"LoadNSOptions"
					);
					vDrawOrder[0] = -1;
					break;
				}
				vDrawOrder[u] = --iOrder;
			}
		}
	}

	if( vDrawOrder[0] == -1 )
	{
		for( unsigned u=0; u<vDrawOrder.size(); u++ )
			vDrawOrder[u] = (int)u;
	}

	m_viNSDrawOrder[pn] = vDrawOrder;
}

/* This data is added to each player profile, and to the machine profile per-player. */
void AddPlayerStatsToProfile( Profile *pProfile, const StageStats &ss, PlayerNumber pn )
{
	ss.AssertValid( pn );
	CHECKPOINT;

	StyleID sID;
	sID.FromStyle( ss.pStyle );

	ASSERT( ss.vpSongs.size() == ss.vpSteps[pn].size() );
	for( unsigned i=0; i<ss.vpSongs.size(); i++ )
	{
		Steps *pSteps = ss.vpSteps[pn][i];

		pProfile->m_iNumSongsPlayedByPlayMode[ss.playMode]++;
		pProfile->m_iNumSongsPlayedByStyle[sID] ++;
		pProfile->m_iNumSongsPlayedByDifficulty[pSteps->GetDifficulty()] ++;

		int iMeter = clamp( pSteps->GetMeter(), 0, MAX_METER );
		pProfile->m_iNumSongsPlayedByMeter[iMeter] ++;
	}

	pProfile->m_iTotalDancePoints += ss.iActualDancePoints[pn];

	if( ss.StageType == StageStats::STAGE_EXTRA || ss.StageType == StageStats::STAGE_EXTRA2 )
	{
		if( ss.bFailed[pn] )
			++pProfile->m_iNumExtraStagesFailed;
		else
			++pProfile->m_iNumExtraStagesPassed;
	}

	// If you fail in a course, you passed all but the final song.
	// FIXME: Not true.  If playing with 2 players, one player could have failed earlier.
	if( !ss.bFailed[pn] )
	{
		pProfile->m_iNumStagesPassedByPlayMode[ss.playMode] ++;
		pProfile->m_iNumStagesPassedByGrade[ss.GetGrade(pn)] ++;
	}
}

void GameState::EndGame()
{
	LOG->Trace( "GameState::EndGame" );

	if( m_bDemonstrationOrJukebox )
		return;
	if( m_timeGameStarted.IsZero() || !g_vPlayedStageStats.size() )	// we were in the middle of a game and played at least one song
		return;

	/* Finish the final stage. */
	FinishStage();


	// Update totalPlaySeconds stat
	int iPlaySeconds = max( 0, (int) m_timeGameStarted.PeekDeltaTime() );

	Profile* pMachineProfile = PROFILEMAN->GetMachineProfile();
	pMachineProfile->m_iTotalPlaySeconds += iPlaySeconds;
	pMachineProfile->m_iTotalPlays++;

	FOREACH_HumanPlayer( p )
	{
		Profile* pPlayerProfile = PROFILEMAN->GetProfile( p );
		if( pPlayerProfile )
		{
			pPlayerProfile->m_iTotalPlaySeconds += iPlaySeconds;
			pPlayerProfile->m_iTotalPlays++;
		}
	}

#if defined( WITH_COIN_MODE )
	BOOKKEEPER->WriteToDisk();
#endif
	PROFILEMAN->SaveAllProfiles();

	FOREACH_HumanPlayer( pn )
	{
		if( !PROFILEMAN->IsUsingProfile(pn) )
			continue;

		PROFILEMAN->UnloadProfile( pn );
	}

	// Reset the USB storage device numbers -after- saving
	CHECKPOINT;
	MEMCARDMAN->FlushAndReset();
	CHECKPOINT;

	SONGMAN->FreeAllPlayerSongs();
	SONGMAN->FreeAllLoadedFromProfiles();

	// make sure we don't execute EndGame twice.
	m_timeGameStarted.SetZero();
}

void GameState::SaveCurrentSettingsToProfile( PlayerNumber pn )
{
	if( !PROFILEMAN->IsUsingProfile(pn) )
		return;
	if( m_bDemonstrationOrJukebox )
		return;

	Profile* pProfile = PROFILEMAN->GetProfile(pn);

	pProfile->m_bUsingProfileDefaultModifiers = true;
	pProfile->m_sDefaultModifiers = m_PlayerOptions[pn].GetSavedPrefsString();
	if( IsSongSort(m_SortOrder) )
		pProfile->m_SortOrder = m_SortOrder;
	if( m_PreferredDifficulty[pn] != DIFFICULTY_INVALID )
		pProfile->m_LastDifficulty = m_PreferredDifficulty[pn];
	if( m_PreferredCourseDifficulty[pn] != DIFFICULTY_INVALID )
		pProfile->m_LastCourseDifficulty = m_PreferredCourseDifficulty[pn];
	if( m_pPreferredSong )
		pProfile->m_lastSong.FromSong( m_pPreferredSong );
	if( m_pPreferredCourse )
		pProfile->m_lastCourse.FromCourse( m_pPreferredCourse );
}

void GameState::Update( float fDelta )
{
	FOREACH_PlayerNumber( p )
	{
		m_CurrentPlayerOptions[p].Approach( m_PlayerOptions[p], fDelta );

		// TRICKY: GAMESTATE->Update is run before any of the Screen update's,
		// so we'll clear these flags here and let them get turned on later
		m_bAttackBeganThisUpdate[p] = false;
		m_bAttackEndedThisUpdate[p] = false;

		bool bRebuildPlayerOptions = false;

		/* See if any delayed attacks are starting or ending. */
		for( unsigned s=0; s<m_ActiveAttacks[p].size(); s++ )
		{
			Attack &attack = m_ActiveAttacks[p][s];

			// -1 is the "starts now" sentinel value.  You must add the attack
			// by calling GameState::LaunchAttack, or else the -1 won't be
			// converted into the current music time.
			ASSERT( attack.fStartSecond != -1 );

			bool bCurrentlyEnabled =
				attack.bGlobal ||
				( attack.fStartSecond < this->m_fMusicSeconds &&
				m_fMusicSeconds < attack.fStartSecond+attack.fSecsRemaining );

			if( m_ActiveAttacks[p][s].bOn == bCurrentlyEnabled )
				continue; /* OK */

			if( m_ActiveAttacks[p][s].bOn && !bCurrentlyEnabled )
				m_bAttackEndedThisUpdate[p] = true;
			else if( !m_ActiveAttacks[p][s].bOn && bCurrentlyEnabled )
				m_bAttackBeganThisUpdate[p] = true;

			bRebuildPlayerOptions = true;

			m_ActiveAttacks[p][s].bOn = bCurrentlyEnabled;
		}

		if( bRebuildPlayerOptions )
			RebuildPlayerOptionsFromActiveAttacks( (PlayerNumber)p );

		if( m_fSecondsUntilAttacksPhasedOut[p] > 0 )
			m_fSecondsUntilAttacksPhasedOut[p] = max( 0, m_fSecondsUntilAttacksPhasedOut[p] - fDelta );
	}
}

void GameState::ReloadCharacters()
{
	unsigned i;

	for( i=0; i<m_pCharacters.size(); i++ )
		delete m_pCharacters[i];
	m_pCharacters.clear();

	FOREACH_PlayerNumber( p )
		m_pCurCharacters[p] = NULL;

	CStringArray as;
	GetDirListing( CHARACTERS_DIR "*", as, true, true );
	bool FoundDefault = false;
	for( i=0; i<as.size(); i++ )
	{
		CString sCharName, sDummy;
		splitpath(as[i], sDummy, sCharName, sDummy);
		sCharName.MakeLower();

		if( sCharName == "cvs" )	// the directory called "CVS"
			continue;		// ignore it

		if( sCharName.CompareNoCase("default")==0 )
			FoundDefault = true;

		Character* pChar = new Character;
		if( pChar->Load( as[i] ) )
			m_pCharacters.push_back( pChar );
		else
			delete pChar;
	}

	if( !FoundDefault )
		RageException::Throw( "'Characters/default' is missing." );

	// If FoundDefault, then we're not empty. -Chris
//	if( m_pCharacters.empty() )
//		RageException::Throw( "Couldn't find any character definitions" );
}

const float GameState::MUSIC_SECONDS_INVALID = -5000.0f;

void GameState::ResetMusicStatistics()
{
	m_fMusicSeconds = 0; // MUSIC_SECONDS_INVALID;
	m_bPastHereWeGo = false;
	m_fSongBeat = 0;

	FOREACH_EnabledPlayer( p )
	{
		m_fPlayerBeat[p] = 0;
		m_fPlayerBPS[p] = 10;
		m_bStop[p] = false;
		m_bFreeze[p] = false;
		m_bDelay[p] = false;
	}

	Actor::SetBGMTime( 0 );
}

void GameState::ResetStageStatistics()
{
	StageStats OldStats = g_CurStageStats;

	g_CurStageStats = StageStats(); // Why are we failing here!?!?

	if( PREFSMAN->m_bComboContinuesBetweenSongs )
	{
		if( GetStageIndex() == 0 )
		{
			FOREACH_PlayerNumber( p )
			{
				Profile* pProfile = PROFILEMAN->GetProfile((PlayerNumber)p);
				if( pProfile )
					g_CurStageStats.iCurCombo[p] = pProfile->m_iCurrentCombo;
			}
		}
		else	// GetStageIndex() > 0
			memcpy( g_CurStageStats.iCurCombo, OldStats.iCurCombo,  sizeof(OldStats.iCurCombo) );
	}

	RemoveAllActiveAttacks();
	RemoveAllInventory();
	m_fOpponentHealthPercent = 1;
	m_fTugLifePercentP1 = 0.5f;
	FOREACH_PlayerNumber( p )
	{
		m_fSuperMeter[p] = 0;
		m_HealthState[p] = ALIVE;

		m_iLastPositiveSumOfAttackLevels[p] = 0;
		m_fSecondsUntilAttacksPhasedOut[p] = 0;	// PlayerAI not affected
	}


	FOREACH_PlayerNumber( p )
	{
		m_vLastPerDifficultyAwards[p].clear();
		m_vLastPeakComboAwards[p].clear();
	}
}

void GameState::UpdateSongPosition( float fPositionSeconds, const TimingData &timing, const RageTimer &timestamp )
{
	if( !timestamp.IsZero() )
		m_LastBeatUpdate = timestamp;
	else
		m_LastBeatUpdate.Touch();

	m_fSongBeat = timing.GetBeatFromElapsedTime( fPositionSeconds );
	ASSERT_M( m_fSongBeat > -2000, ssprintf("%f %f", m_fSongBeat, fPositionSeconds) );

	m_fMusicSeconds = fPositionSeconds;

	Actor::SetBGMTime( m_fSongBeat );
}

void GameState::UpdatePlayerPosition( PlayerNumber pn, const TimingData &timing )
{
	timing.GetBeatAndBPSFromElapsedTime( m_fMusicSeconds, m_fPlayerBeat[pn], m_fPlayerBPS[pn], m_bFreeze[pn], m_bDelay[pn] );
	m_bStop[pn] = m_bFreeze[pn] || m_bDelay[pn];
}

static int GetNumStagesForCurrentSong()
{
	int iNumStagesOfThisSong = 1;
	if( GAMESTATE->m_pCurSong )
		iNumStagesOfThisSong = SongManager::GetNumStagesForSong( GAMESTATE->m_pCurSong );
	else if( GAMESTATE->m_pCurCourse )
		iNumStagesOfThisSong = 1;
	else
		return -1;

	ASSERT( iNumStagesOfThisSong >= 1 );

	/* Never increment more than one past final stage.  That is, if the current
	 * stage is the final stage, and we picked a stage that takes two songs, it
	 * only counts as one stage (so it doesn't bump us all the way to Ex2).
	 * One case where this happens is a long/marathon extra stage.  Another is
	 * if a long/marathon song is selected explicitly in the theme with a ModeChoice,
	 * and PREFSMAN->m_iNumArcadeStages is less than the number of stages that
	 * song takes. */
	int iNumStagesLeft = max( PREFSMAN->m_iNumArcadeStages - GAMESTATE->m_iCurrentStageIndex, PREFSMAN->m_iMaxSongsToPlay - GAMESTATE->m_iCurrentStage );
	iNumStagesOfThisSong = min( iNumStagesOfThisSong, iNumStagesLeft );
	iNumStagesOfThisSong = max( iNumStagesOfThisSong, 1 );

	return iNumStagesOfThisSong;
}

/* Called by ScreenGameplay.  Set the length of the current song. */
void GameState::BeginStage()
{
	if( m_bDemonstrationOrJukebox )
		return;

	/* This should only be called once per stage. */
	if( m_iNumStagesOfThisSong != 0 )
		LOG->Warn( "XXX: m_iNumStagesOfThisSong == %i?", m_iNumStagesOfThisSong );

	/* Finish the last stage (if any), if we havn't already.  (For example, we might
	 * have, for some reason, gone from gameplay to evaluation straight back to gameplay.) */
	FinishStage();

	m_iNumStagesOfThisSong = GetNumStagesForCurrentSong();
	ASSERT( m_iNumStagesOfThisSong != -1 );
}

void GameState::CancelStage()
{
	m_iNumStagesOfThisSong = 0;
}

/* Called by ScreenSelectMusic (etc).  Increment the stage counter if we just played a
 * song.  Might be called more than once. */
void GameState::FinishStage()
{
	/* If m_iNumStagesOfThisSong is 0, we've been called more than once before calling
	 * BeginStage.  This can happen when backing out of the player options screen. */
	if( m_iNumStagesOfThisSong == 0 )
		return;

	// Increment the stage counter.
	ASSERT( m_iNumStagesOfThisSong >= 1 );
	const int iOldStageIndex = m_iCurrentStageIndex;

	m_iCurrentStage++;
	m_iCurrentStageIndex += m_iNumStagesOfThisSong;

	m_iNumStagesOfThisSong = 0;

	// The round has ended; change the seed.
	m_iRoundSeed = rand();

	if( m_bDemonstrationOrJukebox )
		return;

	//
	// Add step totals.  Use radarActual, since the player might have failed part way
	// through the song, in which case we don't want to give credit for the rest of the
	// song.
	//
	FOREACH_HumanPlayer( pn )
	{
		int iNumTapsAndHolds = (int) g_CurStageStats.radarActual[pn][RADAR_NUM_TAPS_AND_HOLDS_AND_ROLLS];
		int iNumJumps	= (int) g_CurStageStats.radarActual[pn][RADAR_NUM_JUMPS];
		int iNumHolds	= (int) g_CurStageStats.radarActual[pn][RADAR_NUM_HOLDS];
		int iNumRolls	= (int) g_CurStageStats.radarActual[pn][RADAR_NUM_ROLLS];
		int iNumMines	= (int) g_CurStageStats.radarActual[pn][RADAR_NUM_MINES];
		int iNumShocks	= (int) g_CurStageStats.radarActual[pn][RADAR_NUM_SHOCKS];
		int iNumPotions	= (int) g_CurStageStats.radarActual[pn][RADAR_NUM_POTIONS];
		int iNumHands	= (int) g_CurStageStats.radarActual[pn][RADAR_NUM_HANDS];
		int iNumLifts	= (int) g_CurStageStats.radarActual[pn][RADAR_NUM_LIFTS];
		int iNumHiddens	= (int) g_CurStageStats.radarActual[pn][RADAR_NUM_HIDDEN];
		PROFILEMAN->AddStepTotals( pn, iNumTapsAndHolds, iNumJumps, iNumHolds, iNumRolls, iNumMines, iNumShocks, iNumPotions, iNumHands, iNumLifts, iNumHiddens );
	}


	// Update profile stats
	Profile* pMachineProfile = PROFILEMAN->GetMachineProfile();

	int iGameplaySeconds = (int)truncf(g_CurStageStats.fGameplaySeconds);

	pMachineProfile->m_iTotalGameplaySeconds += iGameplaySeconds;
	pMachineProfile->m_iCurrentCombo = 0;

	CHECKPOINT;
	FOREACH_HumanPlayer( p )
	{
		CHECKPOINT;

		Profile* pPlayerProfile = PROFILEMAN->GetProfile( p );
		if( pPlayerProfile )
		{
			pPlayerProfile->m_iTotalGameplaySeconds += iGameplaySeconds;
			pPlayerProfile->m_iCurrentCombo =
				PREFSMAN->m_bComboContinuesBetweenSongs ?
				g_CurStageStats.iCurCombo[p] :
				0;
		}

		const StageStats& ss = g_CurStageStats;
		AddPlayerStatsToProfile( pMachineProfile, ss, p );

		if( pPlayerProfile )
			AddPlayerStatsToProfile( pPlayerProfile, ss, p );

		CHECKPOINT;
	}



	if( PREFSMAN->m_bEventMode )
	{
		const int iSaveProfileEvery = 3;
		if( iOldStageIndex/iSaveProfileEvery < m_iCurrentStageIndex/iSaveProfileEvery )
		{
			LOG->Trace( "Played %i stages; saving profiles ...", iSaveProfileEvery );
			PROFILEMAN->SaveAllProfiles();
		}
	}
}

int GameState::GetStage() const
{
	return m_iCurrentStage;
}

int GameState::GetStageIndex() const
{
	return m_iCurrentStageIndex;
}

int GameState::GetNumSongsLeft() const
{
	if( IsExtraStage() || IsExtraStage2() )
		return max( 1, PREFSMAN->m_iMaxSongsToPlay - m_iCurrentStage );

	if( PREFSMAN->m_bEventMode )
		return 999;

	return PREFSMAN->m_iMaxSongsToPlay - m_iCurrentStage;
}

int GameState::GetNumStagesLeft() const
{
	if( IsExtraStage() || IsExtraStage2() )
		// less than PREFSMAN->m_iDefaultStagesPerSong, more than 1
		return min( PREFSMAN->m_iDefaultStagesPerSong, max( 1, PREFSMAN->m_iNumArcadeStages - m_iCurrentStageIndex ) );

	if( PREFSMAN->m_bEventMode )
		return 999;

	return PREFSMAN->m_iNumArcadeStages - m_iCurrentStageIndex;
}

bool GameState::IsFinalStage() const
{
	if( PREFSMAN->m_bEventMode )
		return false;

	if( this->IsCourseMode() )
		return true;

	/* This changes dynamically on ScreenSelectMusic as the wheel turns. */
	int iPredictedStageForCurSong = GetNumStagesForCurrentSong();
	if( iPredictedStageForCurSong <= 0 )
		iPredictedStageForCurSong = 1;

	return ( m_iCurrentStageIndex + iPredictedStageForCurSong == PREFSMAN->m_iNumArcadeStages ||
		m_iCurrentStage + 1 == PREFSMAN->m_iMaxSongsToPlay ) &&
		( !IsExtraStage() && !IsExtraStage2() );
}

bool GameState::IsExtraStage() const
{
	if( PREFSMAN->m_bEventMode )
		return false;

	if( AllUsedAutoPlay() )
		return false;

	return ( m_iCurrentStageIndex == PREFSMAN->m_iNumArcadeStages ||
		m_iCurrentStage == PREFSMAN->m_iMaxSongsToPlay ) &&
		( !IsExtraStage2() );
}

bool GameState::IsExtraStage2() const
{
	if( PREFSMAN->m_bEventMode )
		return false;

	if( AllUsedAutoPlay() )
		return false;

	return ( m_iCurrentStageIndex > PREFSMAN->m_iNumArcadeStages ||
		m_iCurrentStage > PREFSMAN->m_iMaxSongsToPlay );
}

CString GameState::GetSongText() const
{
	if( m_bDemonstrationOrJukebox )				return "demo";
	else if( m_PlayMode == PLAY_MODE_ONI )		return "oni";
	else if( m_PlayMode == PLAY_MODE_NONSTOP )	return "nonstop";
	else if( m_PlayMode == PLAY_MODE_ENDLESS )	return "endless";
	else if( PREFSMAN->m_bEventMode )			return "event";
	else if( IsExtraStage2() )					return "extra2";
	else if( IsExtraStage() )					return "extra1";
	else if( IsFinalStage() )					return "final";
	else							return ssprintf("%d",m_iCurrentStage+1);
}

CString GameState::GetStageText() const
{
	if( m_bDemonstrationOrJukebox )				return "demo";
	else if( m_PlayMode == PLAY_MODE_ONI )		return "oni";
	else if( m_PlayMode == PLAY_MODE_NONSTOP )	return "nonstop";
	else if( m_PlayMode == PLAY_MODE_ENDLESS )	return "endless";
	else if( PREFSMAN->m_bEventMode )			return "event";
	else if( IsExtraStage2() )					return "extra2";
	else if( IsExtraStage() )					return "extra1";
	else if( IsFinalStage() )					return "final";
	else							return ssprintf("%d",m_iCurrentStageIndex+1);
}

void GameState::GetAllSongTexts( CStringArray &out ) const
{
	out.clear();
	out.push_back( "demo" );
	out.push_back( "oni" );
	out.push_back( "nonstop" );
	out.push_back( "endless" );
	out.push_back( "event" );
	out.push_back( "extra2" );
	out.push_back( "extra1" );
	out.push_back( "final" );
	for( int song = 0; song < PREFSMAN->m_iMaxSongsToPlay; ++song )
		out.push_back( ssprintf("%d",song+1) );
}

void GameState::GetAllStageTexts( CStringArray &out ) const
{
	out.clear();
	out.push_back( "demo" );
	out.push_back( "oni" );
	out.push_back( "nonstop" );
	out.push_back( "endless" );
	out.push_back( "event" );
	out.push_back( "extra2" );
	out.push_back( "extra1" );
	out.push_back( "final" );
	for( int stage = 0; stage < PREFSMAN->m_iNumArcadeStages; ++stage )
		out.push_back( ssprintf("%d",stage+1) );
}

int GameState::GetCourseSongIndex() const
{
	int iSongIndex = 0;
	/* iSongsPlayed includes the current song, so it's 1-based; subtract one. */
	FOREACH_PlayerNumber( p )
		if( IsPlayerEnabled(p) )
			iSongIndex = max( iSongIndex, g_CurStageStats.iSongsPlayed[p]-1 );
	return iSongIndex;
}

CString GameState::GetPlayerDisplayName( PlayerNumber pn ) const
{
	ASSERT( IsPlayerEnabled(pn) );
	const CString defaultnames[NUM_PLAYERS] = { "Player 1", "Player 2" };
	if( IsHumanPlayer(pn) )
	{
		if( !PROFILEMAN->GetPlayerName(pn).empty() )
			return PROFILEMAN->GetPlayerName(pn);
		else
			return defaultnames[pn];
	}
	else
	{
		return "CPU";
	}
}

bool GameState::PlayersCanJoin() const
{
	return GetNumSidesJoined() == 0 || this->m_pCurStyle == NULL;	// selecting a style finalizes the players
}

int GameState::GetNumSidesJoined() const
{
	int iNumSidesJoined = 0;
	FOREACH_PlayerNumber(pn)
		if(m_bSideIsJoined[pn])
			iNumSidesJoined++;	// left side, and right side
	return iNumSidesJoined;
}

const Game* GameState::GetCurrentGame() const
{
	ASSERT( m_pCurGame != NULL );	// the game must be set before calling this
	return m_pCurGame;
}

const Style* GameState::GetCurrentStyle() const
{
	return m_pCurStyle;
}

void GameState::JoinRequiredPlayersForStyle(const Style* pStyle) {
	// No players are joined, make P1 the master player
	if (m_MasterPlayerNumber == PLAYER_INVALID) {
		m_MasterPlayerNumber = PLAYER_1;
	}

	switch (pStyle->m_StyleType) {
	case Style::TWO_PLAYERS_TWO_CREDITS:
		FOREACH_PlayerNumber(pn) {
			m_bSideIsJoined[pn] = true;
		}
		break;

	case Style::ONE_PLAYER_ONE_CREDIT:
	case Style::ONE_PLAYER_TWO_CREDITS:
		FOREACH_PlayerNumber(pn) {
			m_bSideIsJoined[pn] = pn == m_MasterPlayerNumber;
		}
		break;

	default:
		ASSERT(0);
	}
}

void GameState::SetCurrentStyle(const Style* pStyle) {
	const int iJoinedPlayers = GetNumSidesJoined();
	bool bIsValidStyle = true;
	switch (pStyle->m_StyleType) {
		case Style::TWO_PLAYERS_TWO_CREDITS:
			bIsValidStyle = iJoinedPlayers == 2;
			break;

		case Style::ONE_PLAYER_ONE_CREDIT:
		case Style::ONE_PLAYER_TWO_CREDITS:
			bIsValidStyle = iJoinedPlayers == 1;
			break;

		default:
			ASSERT(0);
			bIsValidStyle = false;
	}

	if (!bIsValidStyle) {
		RageException::Throw(
			"Unable to set the \"%s\" game style; The wrong number of players are joined: %d",
			pStyle->m_szName, iJoinedPlayers
		);
	}

	m_pCurStyle = pStyle;
}

bool GameState::IsPlayerEnabled( PlayerNumber pn ) const
{
	// In rave, all players are present.  Non-human players are CPU controlled.
	switch( m_PlayMode )
	{
	case PLAY_MODE_BATTLE:
	case PLAY_MODE_RAVE:
		return true;
	}

	return IsHumanPlayer( pn );
}

int	GameState::GetNumPlayersEnabled() const
{
	int count = 0;
	FOREACH_PlayerNumber( p )
		if( IsPlayerEnabled(p) )
			count++;
	return count;
}

bool GameState::PlayerUsingBothSides() const
{
	return this->GetCurrentStyle()->m_StyleType==Style::ONE_PLAYER_TWO_CREDITS;
}

bool GameState::IsHumanPlayer( PlayerNumber pn ) const
{
	if( m_bEditing )
		return pn == m_MasterPlayerNumber;

	if( m_pCurStyle == NULL )	// no style chosen
	{
		if( this->PlayersCanJoin() )
			return m_bSideIsJoined[pn];	// only allow input from sides that have already joined
		else
			return true;	// if we can't join, then we're on a screen like MusicScroll or GameOver
	}

	switch( GetCurrentStyle()->m_StyleType )
	{
	case Style::TWO_PLAYERS_TWO_CREDITS:
		return true;
	case Style::ONE_PLAYER_ONE_CREDIT:
	case Style::ONE_PLAYER_TWO_CREDITS:
		return pn == m_MasterPlayerNumber;
	default:
		ASSERT(0);		// invalid style type
		return false;
	}
}

bool GameState::AnyPlaying() const
{
	if( m_bPlaying )
		FOREACH_PlayerNumber(pn)
			if( m_PlayerController[pn] == PC_HUMAN )
				return true;

	return false;
}

bool GameState::UsedAutoPlay( PlayerNumber pn ) const
{
	return m_bUsedAutoPlay[pn];
}

bool GameState::AllUsedAutoPlay() const
{
	FOREACH_PlayerNumber( pn )
		if( !UsedAutoPlay(pn) )
			return false;

	return true;
}

void GameState::ResetAutoPlay()
{
	FOREACH_PlayerNumber( pn )
	{
		m_bUsedAutoPlay[pn] = false;
		m_iNotesInAutoPlay[pn] = 0;
	}
}

int GameState::GetNumHumanPlayers() const
{
	int count = 0;
	FOREACH_PlayerNumber( p )
		if( IsHumanPlayer(p) )
			count++;
	return count;
}

PlayerNumber GameState::GetFirstHumanPlayer() const
{
	FOREACH_PlayerNumber( p )
		if( IsHumanPlayer(p) )
			return p;
	ASSERT(0);	// there must be at least 1 human player
	return PLAYER_INVALID;
}

bool GameState::IsCpuPlayer( PlayerNumber pn ) const
{
	return IsPlayerEnabled(pn) && !IsHumanPlayer(pn);
}

bool GameState::AnyPlayersAreCpu() const
{
	FOREACH_PlayerNumber( p )
		if( IsCpuPlayer(p) )
			return true;
	return false;
}

bool GameState::IsEventMode() const
{
	return PREFSMAN->m_bEventMode;
}

int GameState::GetScreenWidth() const
{
	return SCREEN_WIDTH;
}

int GameState::GetScreenHeight() const
{
	return SCREEN_HEIGHT;
}

float GameState::GetScreenAspectRatio() const
{
	return (float)SCREEN_WIDTH / (float)SCREEN_HEIGHT;
}

int GameState::GetDisplayWidth() const
{
	return PREFSMAN->m_iDisplayWidth;
}

int GameState::GetDisplayHeight() const
{
	return PREFSMAN->m_iDisplayHeight;
}

float GameState::GetDisplayAspectRatio() const
{
	return (float)PREFSMAN->m_iDisplayWidth / (float)PREFSMAN->m_iDisplayHeight;
}

float GameState::GetDPIZoomX() const
{
	return GetScreenZoom(PREFSMAN->m_CurrentDPI.x);
}

float GameState::GetDPIZoomY() const
{
	return GetScreenZoom(PREFSMAN->m_CurrentDPI.y);
}

bool GameState::IsCourseMode() const
{
	switch(m_PlayMode)
	{
	case PLAY_MODE_ONI:
	case PLAY_MODE_NONSTOP:
	case PLAY_MODE_ENDLESS:
		return true;
	default:
		return false;
	}
}

bool GameState::IsBattleMode() const
{
	switch( this->m_PlayMode )
	{
	case PLAY_MODE_BATTLE:
		return true;
	default:
		return false;
	}
}

bool GameState::HasEarnedExtraStage() const
{
	if( PREFSMAN->m_bEventMode )
		return false;

	if( AllUsedAutoPlay() )
		return false;

	if( this->m_PlayMode != PLAY_MODE_REGULAR )
		return false;

	bool bFinal = this->IsFinalStage(),
		bExtra = this->IsExtraStage();

	if( bFinal && !PREFSMAN->m_bAllowExtraStage || bExtra && !PREFSMAN->m_bAllowExtraStage2 )
		return false;

	/* If "choose EX" is enabled, then we should only grant EX2 if the chosen
		* stage was the EX we would have chosen (m_bAllow2ndExtraStage is true). */
	if( PREFSMAN->m_bPickExtraStage && bExtra && !this->m_bAllow2ndExtraStage )
		return false;

	if( bFinal || bExtra )
	{
		int iCheckForExtra = NUM_PLAYERS;

		// Aldo_MX: I would put this in preferences but its stupid to earn an extra stage when you failed a song...
		FOREACH_PlayerNumber( p )
		{
			if( !this->IsPlayerEnabled(p) )
				iCheckForExtra--;	// skip

			bool bFailed = false;

			FOREACH( StageStats, g_vPlayedStageStats, ss )
				bFailed |= ss->bLifebarWasDepleted[p] || ss->bFailed[p];

			bFailed |= g_CurStageStats.bLifebarWasDepleted[p] || g_CurStageStats.bFailed[p];

			if( bFailed )
				iCheckForExtra--;
		}

		// Only check if at least one player did not have their lifebar completely deplete or they did not fail
		if( iCheckForExtra == 0 )
			return false;

		bool bEarns[NUM_PLAYERS];

		FOREACH_PlayerNumber( p )
		{
			bEarns[p] = true;

			if( !this->IsPlayerEnabled(p) )
				continue;	// skip

			bEarns[p] = true;

			if( bFinal )
			{
				if( PREFSMAN->m_bCheckDifficultyES )
				{
					if( PREFSMAN->m_bCheckDifficultyAllStagesES )
					{
						FOREACH( Steps*, g_CurStageStats.vpSteps[p], pSteps )
							if( (*pSteps)->GetDifficulty() < PREFSMAN->m_MinDifficultyES ||
								(*pSteps)->GetDifficulty() > PREFSMAN->m_MaxDifficultyES )
								bEarns[p] = false;
					}

					if( this->m_pCurSteps[p]->GetDifficulty() < PREFSMAN->m_MinDifficultyES ||
						this->m_pCurSteps[p]->GetDifficulty() > PREFSMAN->m_MaxDifficultyES )
						bEarns[p] = false;
				}

				if( PREFSMAN->m_bCheckGradeES )
				{
					if( PREFSMAN->m_bCheckGradeAllStagesES )
					{
						FOREACH( StageStats, g_vPlayedStageStats, ss )
							if( ss->GetGrade(p) > PREFSMAN->m_MinGradeES )
								bEarns[p] = false;
					}

					if( g_CurStageStats.GetGrade(p) > PREFSMAN->m_MinGradeES )
						bEarns[p] = false;
				}

				if( PREFSMAN->m_bCheckSongsPlayedES )
				{
					if( this->m_iCurrentStage < PREFSMAN->m_iMinSongsPlayedES ||
						this->m_iCurrentStage > PREFSMAN->m_iMaxSongsPlayedES )
						bEarns[p] = false;
				}

			}

			if( bExtra )
			{
				if( PREFSMAN->m_bCheckDifficultyOMES )
				{
					if( PREFSMAN->m_bCheckDifficultyAllStagesOMES )
					{
						FOREACH( Steps*, g_CurStageStats.vpSteps[p], pSteps )
							if( (*pSteps)->GetDifficulty() < PREFSMAN->m_MinDifficultyOMES ||
								(*pSteps)->GetDifficulty() > PREFSMAN->m_MaxDifficultyOMES )
								bEarns[p] = false;
					}

					if( this->m_pCurSteps[p]->GetDifficulty() < PREFSMAN->m_MinDifficultyOMES ||
						this->m_pCurSteps[p]->GetDifficulty() > PREFSMAN->m_MaxDifficultyOMES )
						bEarns[p] = false;
				}

				if( PREFSMAN->m_bCheckGradeOMES )
				{
					if( PREFSMAN->m_bCheckGradeAllStagesOMES )
					{
						FOREACH( StageStats, g_vPlayedStageStats, ss )
							if( ss->GetGrade(p) > PREFSMAN->m_MinGradeOMES )
								bEarns[p] = false;
					}

					if( g_CurStageStats.GetGrade(p) > PREFSMAN->m_MinGradeOMES )
						bEarns[p] = false;
				}

				if( PREFSMAN->m_bCheckSongsPlayedOMES )
				{
					if( this->m_iCurrentStage < PREFSMAN->m_iMinSongsPlayedOMES ||
						this->m_iCurrentStage > PREFSMAN->m_iMaxSongsPlayedOMES )
						bEarns[p] = false;
				}
			}

			if( !bEarns[p] )
				continue;

			return true;
		}
	}
	return false;
}

PlayerNumber GameState::GetBestPlayer() const
{
	for( int p=PLAYER_1; p<NUM_PLAYERS; p++ )
		if( GetStageResult( (PlayerNumber)p ) == RESULT_WIN )
			return (PlayerNumber)p;
	return PLAYER_INVALID;	// draw
}

StageResult GameState::GetStageResult( PlayerNumber pn ) const
{
	switch( this->m_PlayMode )
	{
	case PLAY_MODE_BATTLE:
	case PLAY_MODE_RAVE:
		if( fabsf(m_fTugLifePercentP1 - 0.5f) < 0.0001f )
			return RESULT_DRAW;
		switch( pn )
		{
		case PLAYER_1:	return (m_fTugLifePercentP1>=0.5f)?RESULT_WIN:RESULT_LOSE;
		case PLAYER_2:	return (m_fTugLifePercentP1<0.5f)?RESULT_WIN:RESULT_LOSE;
		default:	ASSERT(0); return RESULT_LOSE;
		}
	}

	StageResult win = RESULT_WIN;
	for( int p=PLAYER_1; p<NUM_PLAYERS; p++ )
	{
		if( p == pn )
			continue;

		/* If anyone did just as well, at best it's a draw. */
		if( g_CurStageStats.iActualDancePoints[p] == g_CurStageStats.iActualDancePoints[pn] )
			win = RESULT_DRAW;

		/* If anyone did better, we lost. */
		if( g_CurStageStats.iActualDancePoints[p] > g_CurStageStats.iActualDancePoints[pn] )
			return RESULT_LOSE;
	}
	return win;
}

void GameState::GetFinalEvalStats( StageStats& statsOut ) const
{
	statsOut.Init();

	// Show stats only for the latest 3 normal songs + passed extra stages
	int PassedRegularSongsLeft = 3;

	// Increase PassedRegularSongsLeft if necessary
	//if( PREFSMAN->m_bUseAllRoundsForFinalStats && !PREFSMAN->m_bEventMode )
	//	PassedRegularSongsLeft = (int)g_vPlayedStageStats.size();

	for( int i = (int)g_vPlayedStageStats.size()-1; i >= 0; --i )
	{
		const StageStats &s = g_vPlayedStageStats[i];

		if( !s.OnePassed() )
			continue;

		if( s.StageType == StageStats::STAGE_NORMAL )
		{
			if( PassedRegularSongsLeft == 0 )
				break;

			--PassedRegularSongsLeft;
		}

		statsOut.AddStats( s );
	}

	if( statsOut.vpSongs.empty() )
		return;	// don't divide by 0 below

	/* Scale radar percentages back down to roughly 0..1.  Don't scale RADAR_NUM_TAPS_AND_HOLDS_AND_ROLLS
	 * and the rest, which are counters. */
	// FIXME: Weight each song by the number of stages it took to account for
	// long, marathon.
	FOREACH_EnabledPlayer( p )
	{
		for( int r = 0; r < RADAR_NUM_TAPS_AND_HOLDS_AND_ROLLS; r++)
		{
			statsOut.radarPossible[p][r] /= statsOut.vpSongs.size();
			statsOut.radarActual[p][r] /= statsOut.vpSongs.size();
		}
	}
}


void GameState::ApplyModifiers( PlayerNumber pn, CString sModifiers )
{
	const SongOptions::FailType ft = this->m_SongOptions.m_FailType;

	m_PlayerOptions[pn].FromString( sModifiers );
	m_SongOptions.FromString( sModifiers );

	if( ft != this->m_SongOptions.m_FailType )
		this->m_bChangedFailType = true;
}

/* Store the player's preferred options.  This is called at the very beginning
 * of gameplay. */
void GameState::StoreSelectedOptions()
{
	FOREACH_PlayerNumber( p )
		this->m_StoredPlayerOptions[p] = this->m_PlayerOptions[p];
	m_StoredSongOptions = m_SongOptions;
}

/* Restore the preferred options.  This is called after a song ends, before
 * setting new course options, so options from one song don't carry into the
 * next and we default back to the preferred options.  This is also called
 * at the end of gameplay to restore options. */
void GameState::RestoreSelectedOptions()
{
	FOREACH_PlayerNumber( p )
		this->m_PlayerOptions[p] = this->m_StoredPlayerOptions[p];
	m_SongOptions = m_StoredSongOptions;
}

bool GameState::IsDisqualified( PlayerNumber pn )
{
	if( !PREFSMAN->m_bDisqualification )
		return false;

	if( IsCourseMode() )
	{
		return m_PlayerOptions[pn].IsEasierForCourseAndTrail(
			m_pCurCourse,
			m_pCurTrail[pn] );
	}
	else
	{
		return m_PlayerOptions[pn].IsEasierForSongAndSteps(
			m_pCurSong,
			m_pCurSteps[pn] );
	}
}

void GameState::ResetNoteSkins()
{
	FOREACH_PlayerNumber( pn )
	{
		ResetNoteSkinsForPlayer( (PlayerNumber) pn );
		++m_BeatToNoteSkinRev[pn];
	}
}

void GameState::ResetNoteSkinsForPlayer( PlayerNumber pn )
{
	LoadNSOptions(pn);

	m_BeatToNoteSkin[pn].clear();
	m_BeatToNoteSkin[pn][-1000] = this->m_PlayerOptions[pn].m_sNoteSkin;

	++m_BeatToNoteSkinRev[pn];
}

void GameState::GetAllUsedNoteSkins( vector<CString> &out ) const
{
	FOREACH_EnabledPlayer( pn )
	{
		out.push_back( this->m_PlayerOptions[pn].m_sNoteSkin );

		switch( this->m_PlayMode )
		{
		case PLAY_MODE_BATTLE:
		case PLAY_MODE_RAVE:
			for( int al=0; al<NUM_ATTACK_LEVELS; al++ )
			{
				const Character *ch = this->m_pCurCharacters[pn];
				ASSERT( ch );
				const CString* asAttacks = ch->m_sAttacks[al];
				for( int att = 0; att < NUM_ATTACKS_PER_LEVEL; ++att )
				{
					PlayerOptions po;
					po.FromString( asAttacks[att] );
					/* Hack: NoteSkin "default" is never applied as an attack,
					 * so don't waste memory preloading it. */
					if( po.m_sNoteSkin != "" && po.m_sNoteSkin.CompareNoCase("default") )
						out.push_back( po.m_sNoteSkin );
				}
			}
		}

		for( map<float,CString>::const_iterator it = m_BeatToNoteSkin[pn].begin();
			it != m_BeatToNoteSkin[pn].end(); ++it )
			out.push_back( it->second );
	}
}

/* From NoteField: */

void GameState::GetUndisplayedBeats( PlayerNumber pn, float TotalSeconds, float &StartBeat, float &EndBeat ) const
{
	/* If reasonable, push the attack forward so notes on screen don't change suddenly. */
	StartBeat = min( this->m_fPlayerBeat[pn]+fBEATS_PER_MEASURE*2, m_fLastDrawnBeat[pn] );
	StartBeat = truncf(StartBeat)+1;

	const float StartSecond = this->m_pCurSteps[pn]->m_Timing.GetElapsedTimeFromBeat( StartBeat );
	const float EndSecond = StartSecond + TotalSeconds;
	EndBeat = this->m_pCurSteps[pn]->m_Timing.GetBeatFromElapsedTime( EndSecond );
	EndBeat = truncf(EndBeat)+1;
}

void GameState::SetNoteSkinForBeatRange( PlayerNumber pn, CString sNoteSkin, float StartBeat, float EndBeat )
{
	map<float,CString> &BeatToNoteSkin = m_BeatToNoteSkin[pn];

	/* Erase any other note skin settings in this range. */
	map<float,CString>::iterator it = BeatToNoteSkin.lower_bound( StartBeat );
	map<float,CString>::iterator end = BeatToNoteSkin.upper_bound( EndBeat );
	while( it != end )
	{
		map<float,CString>::iterator next = it;
		++next;

		BeatToNoteSkin.erase( it );

		it = next;
	}

	/* Add the skin to m_BeatToNoteSkin.  */
	BeatToNoteSkin[StartBeat] = sNoteSkin;

	/* Return to the default note skin after the duration. */
	BeatToNoteSkin[EndBeat] = m_StoredPlayerOptions[pn].m_sNoteSkin;

	++m_BeatToNoteSkinRev[pn];
}

/* This is called to launch an attack, or to queue an attack if a.fStartSecond
 * is set.  This is also called by GameState::Update when activating a queued attack. */
void GameState::LaunchAttack( PlayerNumber target, Attack a )
{
	/* If fStartSecond is -1, it means "launch as soon as possible".  For m_ActiveAttacks,
	 * mark the real time it's starting (now), so Update() can know when the attack started
	 * so it can be removed later.  For m_ModsToApply, leave the -1 in, so Player::Update
	 * knows to apply attack transforms correctly.  (yuck) */
	m_ModsToApply[target].push_back( a );
	if( a.fStartSecond == -1 )
		a.fStartSecond = this->m_fMusicSeconds;
	m_ActiveAttacks[target].push_back( a );

	LOG->Trace( "Launch attack '%s' against P%d at %f", a.sModifier.c_str(), target+1, a.fStartSecond );

	this->RebuildPlayerOptionsFromActiveAttacks( target );
}

void GameState::RemoveActiveAttacksForPlayer( PlayerNumber pn, AttackLevel al )
{
	for( unsigned s=0; s<m_ActiveAttacks[pn].size(); s++ )
	{
		if( al != NUM_ATTACK_LEVELS && al != m_ActiveAttacks[pn][s].level )
			continue;
		m_ActiveAttacks[pn].erase( m_ActiveAttacks[pn].begin()+s, m_ActiveAttacks[pn].begin()+s+1 );
		--s;
	}
	RebuildPlayerOptionsFromActiveAttacks( pn );
}

void GameState::RemoveAllInventory()
{
	FOREACH_PlayerNumber( p )
		for( int s=0; s<NUM_INVENTORY_SLOTS; s++ )
		{
			m_Inventory[p][s].fSecsRemaining = 0;
			m_Inventory[p][s].sModifier = "";
		}
}

void GameState::RebuildPlayerOptionsFromActiveAttacks( PlayerNumber pn )
{
	// rebuild player options
	PlayerOptions po = m_StoredPlayerOptions[pn];
	for( unsigned s=0; s<m_ActiveAttacks[pn].size(); s++ )
	{
		if( !m_ActiveAttacks[pn][s].bOn )
			continue; /* hasn't started yet */
		po.FromString( m_ActiveAttacks[pn][s].sModifier );
	}
	m_PlayerOptions[pn] = po;


	int iSumOfAttackLevels = GetSumOfActiveAttackLevels( pn );
	if( iSumOfAttackLevels > 0 )
	{
		m_iLastPositiveSumOfAttackLevels[pn] = iSumOfAttackLevels;
		m_fSecondsUntilAttacksPhasedOut[pn] = 10000;	// any positive number that won't run out before the attacks
	}
	else
	{
		// don't change!  m_iLastPositiveSumOfAttackLevels[p] = iSumOfAttackLevels;
		m_fSecondsUntilAttacksPhasedOut[pn] = 2;	// 2 seconds to phase out
	}
}

void GameState::RemoveAllActiveAttacks()	// called on end of song
{
	FOREACH_PlayerNumber( p )
		RemoveActiveAttacksForPlayer( (PlayerNumber)p );
}

int GameState::GetSumOfActiveAttackLevels( PlayerNumber pn ) const
{
	int iSum = 0;

	for( unsigned s=0; s<m_ActiveAttacks[pn].size(); s++ )
		if( m_ActiveAttacks[pn][s].fSecsRemaining > 0 && m_ActiveAttacks[pn][s].level != NUM_ATTACK_LEVELS )
			iSum += m_ActiveAttacks[pn][s].level;

	return iSum;
}

template<class T>
void setmin( T &a, const T &b )
{
	a = min(a, b);
}

template<class T>
void setmax( T &a, const T &b )
{
	a = max(a, b);
}

/* Adjust the fail mode based on the chosen difficulty.  This must be called
 * after the difficulty has been finalized (usually in ScreenSelectMusic or
 * ScreenPlayerOptions), and before the fail mode is displayed or used (usually
 * in ScreenSongOptions). */
void GameState::AdjustFailType()
{
	/* Single song mode only. */
	if( this->IsCourseMode() )
		return;

	/* If the player changed the fail mode explicitly, leave it alone. */
	if( this->m_bChangedFailType )
		return;

	/* Find the easiest difficulty notes selected by either player. */
	const Difficulty dc = GetEasiestNotesDifficulty();

	/* Reset the fail type to the default. */
	SongOptions so;
	so.FromString( PREFSMAN->m_sDefaultModifiers );
	this->m_SongOptions.m_FailType = so.m_FailType;

    /* Easy and beginner are never harder than FAIL_END_OF_SONG. */
	if(dc <= DIFFICULTY_EASY)
		setmax(this->m_SongOptions.m_FailType, SongOptions::FAIL_END_OF_SONG);

	/* If beginner's steps were chosen, and this is the first stage,
		* turn off failure completely--always give a second try. */
	if(dc == DIFFICULTY_BEGINNER &&
		!PREFSMAN->m_bEventMode && /* stage index is meaningless in event mode */
		this->m_iCurrentStageIndex == 0)
		setmax(this->m_SongOptions.m_FailType, SongOptions::FAIL_OFF);
}

bool GameState::ShowMarvelous( PlayerNumber pn ) const
{
	switch( PREFSMAN->m_iMarvelousTiming )
	{
	case 2:
		break;
	case 1:
		if( IsCourseMode() )
			break;
	default:
		return false;
	}

	if( !GAMESTATE->m_PlayerOptions[pn].m_bSMJudgments ||
		GAMESTATE->GetCurrentGame()->m_mapMarvelousTo != TNS_MARVELOUS )
		return false;

	return true;
}

void GameState::GetCharacters( vector<Character*> &apCharactersOut )
{
	for( unsigned i=0; i<m_pCharacters.size(); i++ )
		if( m_pCharacters[i]->m_sName.CompareNoCase("default")!=0 )
			apCharactersOut.push_back( m_pCharacters[i] );
}

Character* GameState::GetRandomCharacter()
{
	vector<Character*> apCharacters;
	GetCharacters( apCharacters );
	if( apCharacters.size() )
		return apCharacters[rand()%apCharacters.size()];
	else
		return GetDefaultCharacter();
}

Character* GameState::GetDefaultCharacter()
{
	for( unsigned i=0; i<m_pCharacters.size(); i++ )
	{
		if( m_pCharacters[i]->m_sName.CompareNoCase("default")==0 )
			return m_pCharacters[i];
	}

	/* We always have the default character. */
	ASSERT(0);
	return NULL;
}

struct SongAndSteps
{
	Song* pSong;
	Steps* pSteps;
	bool operator==( const SongAndSteps& other ) const { return pSong==other.pSong && pSteps==other.pSteps; }
	bool operator<( const SongAndSteps& other ) const { return pSong<=other.pSong && pSteps<=other.pSteps; }
};

void GameState::GetRankingFeats( PlayerNumber pn, vector<RankingFeat> &asFeatsOut ) const
{
	if( !IsHumanPlayer(pn) )
		return;

	Profile *pProf = PROFILEMAN->GetProfile(pn);

	CHECKPOINT_M(ssprintf("PlayMode %i",this->m_PlayMode));
	switch( this->m_PlayMode )
	{
	case PLAY_MODE_REGULAR:
		{
			CHECKPOINT;

			StepsType st = this->GetCurrentStyle()->m_StepsType;

			//
			// Find unique Song and Steps combinations that were played.
			// We must keep only the unique combination or else we'll double-count
			// high score markers.
			//
			vector<SongAndSteps> vSongAndSteps;

			for( unsigned i=0; i<g_vPlayedStageStats.size(); i++ )
			{
				CHECKPOINT_M( ssprintf("%u/%i", i, (int)g_vPlayedStageStats.size() ) );
				SongAndSteps sas;
				sas.pSong = g_vPlayedStageStats[i].vpSongs[0];
				ASSERT( sas.pSong );
				sas.pSteps = g_vPlayedStageStats[i].vpSteps[pn][0];
				ASSERT( sas.pSteps );
				vSongAndSteps.push_back( sas );
			}
			CHECKPOINT;

			sort( vSongAndSteps.begin(), vSongAndSteps.end() );

			vector<SongAndSteps>::iterator toDelete = unique( vSongAndSteps.begin(), vSongAndSteps.end() );
			vSongAndSteps.erase(toDelete, vSongAndSteps.end());

			CHECKPOINT;
			for( unsigned i=0; i<vSongAndSteps.size(); i++ )
			{
				Song* pSong = vSongAndSteps[i].pSong;
				Steps* pSteps = vSongAndSteps[i].pSteps;

				// Find Machine Records
				{
					HighScoreList &hsl = PROFILEMAN->GetMachineProfile()->GetStepsHighScoreList(pSong,pSteps);
					for( unsigned j=0; j<hsl.vHighScores.size(); j++ )
					{
						HighScore &hs = hsl.vHighScores[j];

						if( hs.sName != RANKING_TO_FILL_IN_MARKER[pn] )
							continue;

						RankingFeat feat;
						feat.Type = RankingFeat::SONG;
						feat.pSong = pSong;
						feat.pSteps = pSteps;
						feat.Feat = ssprintf("MR #%d in %s %s", j+1, pSong->GetTranslitMainTitle().c_str(), DifficultyToString(pSteps->GetDifficulty()).c_str() );
						feat.pStringToFill = &hs.sName;
						feat.grade = hs.grade;
						feat.fPercentDP = hs.fPercentDP;
						feat.iScore = hs.iScore;

						if( pSong->HasBanner() )
							feat.Banner = pSong->GetBannerPath();

						asFeatsOut.push_back( feat );
					}
				}

				// Find Personal Records
				if( pProf )
				{
					HighScoreList &hsl = pProf->GetStepsHighScoreList(pSong,pSteps);
					for( unsigned j=0; j<hsl.vHighScores.size(); j++ )
					{
						HighScore &hs = hsl.vHighScores[j];

						if( hs.sName != RANKING_TO_FILL_IN_MARKER[pn] )
							continue;

						RankingFeat feat;
						feat.pSong = pSong;
						feat.pSteps = pSteps;
						feat.Type = RankingFeat::SONG;
						feat.Feat = ssprintf("PR #%d in %s %s", j+1, pSong->GetTranslitMainTitle().c_str(), DifficultyToString(pSteps->GetDifficulty()).c_str() );
						feat.pStringToFill = &hs.sName;
						feat.grade = hs.grade;
						feat.fPercentDP = hs.fPercentDP;
						feat.iScore = hs.iScore;

						// XXX: temporary hack
						if( pSong->HasBackground() )
							feat.Banner = pSong->GetBackgroundPath();
		//					if( pSong->HasBanner() )
		//						feat.Banner = pSong->GetBannerPath();

						asFeatsOut.push_back( feat );
					}
				}
			}

			CHECKPOINT;
			StageStats stats;
			GetFinalEvalStats( stats );


			// Find Machine Category Records
			FOREACH_RankingCategory( rc )
			{
				HighScoreList &hsl = PROFILEMAN->GetMachineProfile()->GetCategoryHighScoreList( st, rc );
				for( unsigned j=0; j<hsl.vHighScores.size(); j++ )
				{
					HighScore &hs = hsl.vHighScores[j];
					if( hs.sName != RANKING_TO_FILL_IN_MARKER[pn] )
						continue;

					RankingFeat feat;
					feat.Type = RankingFeat::CATEGORY;
					feat.Feat = ssprintf("MR #%d in Type %c (%d)", j+1, 'A'+rc, stats.GetAverageMeter(pn) );
					feat.pStringToFill = &hs.sName;
					feat.grade = GRADE_NO_DATA;
					feat.iScore = hs.iScore;
					feat.fPercentDP = hs.fPercentDP;
					asFeatsOut.push_back( feat );
				}
			}

			// Find Personal Category Records
			FOREACH_RankingCategory( rc )
			{
				if( pProf )
				{
					HighScoreList &hsl = pProf->GetCategoryHighScoreList( st, rc );
					for( unsigned j=0; j<hsl.vHighScores.size(); j++ )
					{
						HighScore &hs = hsl.vHighScores[j];
						if( hs.sName != RANKING_TO_FILL_IN_MARKER[pn] )
							continue;

						RankingFeat feat;
						feat.Type = RankingFeat::CATEGORY;
						feat.Feat = ssprintf("PR #%d in Type %c (%d)", j+1, 'A'+rc, stats.GetAverageMeter(pn) );
						feat.pStringToFill = &hs.sName;
						feat.grade = GRADE_NO_DATA;
						feat.iScore = hs.iScore;
						feat.fPercentDP = hs.fPercentDP;
						asFeatsOut.push_back( feat );
					}
				}
			}
		}
		break;
	case PLAY_MODE_BATTLE:
	case PLAY_MODE_RAVE:
		break;
	case PLAY_MODE_NONSTOP:
	case PLAY_MODE_ONI:
	case PLAY_MODE_ENDLESS:
		{
			CHECKPOINT;
			Course* pCourse = m_pCurCourse;
			ASSERT( pCourse );
			Trail *pTrail = m_pCurTrail[pn];
			ASSERT( pTrail );
			CourseDifficulty cd = pTrail->m_CourseDifficulty;

			// Find Machine Records
			{
				Profile* pProfile = PROFILEMAN->GetMachineProfile();
				HighScoreList &hsl = pProfile->GetCourseHighScoreList( pCourse, pTrail );
				for( unsigned i=0; i<hsl.vHighScores.size(); i++ )
				{
					HighScore &hs = hsl.vHighScores[i];
					if( hs.sName != RANKING_TO_FILL_IN_MARKER[pn] )
							continue;

					RankingFeat feat;
					feat.Type = RankingFeat::COURSE;
					feat.pCourse = pCourse;
					feat.Feat = ssprintf("MR #%d in %s", i+1, pCourse->GetFullDisplayTitle().c_str() );
					if( cd != DIFFICULTY_MEDIUM )
						feat.Feat += " " + CourseDifficultyToThemedString(cd);
					feat.pStringToFill = &hs.sName;
					feat.grade = GRADE_NO_DATA;
					feat.iScore = hs.iScore;
					feat.fPercentDP = hs.fPercentDP;
					if( pCourse->HasBanner() )
						feat.Banner = pCourse->m_sBannerPath;
					asFeatsOut.push_back( feat );
				}
			}

			// Find Personal Records
			if( PROFILEMAN->IsUsingProfile( pn ) )
			{
				HighScoreList &hsl = pProf->GetCourseHighScoreList( pCourse, pTrail );
				for( unsigned i=0; i<hsl.vHighScores.size(); i++ )
				{
					HighScore& hs = hsl.vHighScores[i];
					if( hs.sName != RANKING_TO_FILL_IN_MARKER[pn] )
							continue;

					RankingFeat feat;
					feat.Type = RankingFeat::COURSE;
					feat.pCourse = pCourse;
					feat.Feat = ssprintf("PR #%d in %s", i+1, pCourse->GetFullDisplayTitle().c_str() );
					feat.pStringToFill = &hs.sName;
					feat.grade = GRADE_NO_DATA;
					feat.iScore = hs.iScore;
					feat.fPercentDP = hs.fPercentDP;
					if( pCourse->HasBanner() )
						feat.Banner = pCourse->m_sBannerPath;
					asFeatsOut.push_back( feat );
				}
			}
		}
		break;
	default:
		ASSERT(0);
	}
}

void GameState::StoreRankingName( PlayerNumber pn, CString name )
{
	//
	// Filter swear words from name
	//
	name.MakeUpper();
	RageFile file(NAMES_BLACKLIST_FILE);

	if (file.IsOpen())
	{
		CString line;

		while (!file.AtEOF())
		{
			if( file.GetLine( line ) == -1 )
			{
				LOG->Warn( "Error reading \"%s\": %s", NAMES_BLACKLIST_FILE, file.GetError().c_str() );
				break;
			}

			line.MakeUpper();
			if( !line.empty() && name.find(line) != CString::npos )	// name contains a bad word
			{
				LOG->Trace( "entered '%s' matches blacklisted item '%s'", name.c_str(), line.c_str() );
				name = "";
				break;
			}
		}
	}

	vector<RankingFeat> aFeats;
	GetRankingFeats( pn, aFeats );

	for( unsigned i=0; i<aFeats.size(); i++ )
	{
		*aFeats[i].pStringToFill = name;

		// save name pointers as we fill them
		m_vpsNamesThatWereFilled.push_back( aFeats[i].pStringToFill );
	}
}

bool GameState::AllAreInDangerOrWorse() const
{
	FOREACH_EnabledPlayer( p )
		if( m_HealthState[p] < DANGER )
			return false;
	return true;
}

bool GameState::AllAreDead() const
{
	FOREACH_EnabledPlayer( p )
		if( m_HealthState[p] < DEAD )
			return false;
	return true;
}

bool GameState::AllHaveComboOf30OrMoreMisses() const
{
	FOREACH_EnabledPlayer( p )
	{
		if( g_CurStageStats.iCurMissCombo[p] < 30 )
			return false;
	}
	return true;
}

bool GameState::AllHaveComboOf50OrMoreMisses() const
{
	FOREACH_EnabledPlayer( p )
	{
		if( g_CurStageStats.iCurMissCombo[p] <= 50 )
			return false;
	}
	return true;
}

bool GameState::OneIsHot() const
{
	FOREACH_EnabledPlayer( p )
		if( m_HealthState[p] == HOT )
			return true;
	return false;
}

bool GameState::IsTimeToPlayAttractSounds()
{
	// if m_iNumTimesThroughAttract is negative, play attract sounds regardless
	// of m_iAttractSoundFrequency.
	if( m_iNumTimesThroughAttract<0 )
		return true;

	// 0 means "never play sound".  Avoid a divide by 0 below.
	if( PREFSMAN->m_iAttractSoundFrequency == 0 )
		return false;

	// play attract sounds once every m_iAttractSoundFrequency times through
	if( (m_iNumTimesThroughAttract % PREFSMAN->m_iAttractSoundFrequency)==0 )
		return true;

	return false;
}

// Only called on options screen
bool GameState::HasSongAttacks( PlayerNumber pn )
{
	return m_pCurSteps[pn]->m_bHasAttacks;
}

bool GameState::DifficultiesLocked()
{
 	if( m_PlayMode == PLAY_MODE_RAVE )
		return true;
	if( IsCourseMode() )
		return PREFSMAN->m_bLockCourseDifficulties;
	return false;
}

bool GameState::ChangePreferredDifficulty( PlayerNumber pn, Difficulty dc )
{
	this->m_PreferredDifficulty[pn] = dc;
	if( DifficultiesLocked() )
		FOREACH_PlayerNumber( p )
			m_PreferredDifficulty[p] = m_PreferredDifficulty[pn];

	return true;
}

void GameState::GetDifficultiesToShow( set<Difficulty> &ret )
{
	static float fExpiration = -999;
	static set<Difficulty> cache;
	if( RageTimer::GetTimeSinceStart() < fExpiration )
	{
		ret = cache;
		return;
	}

	CStringArray asDiff;
	split( DIFFICULTIES_TO_SHOW, ",", asDiff );
	ASSERT( asDiff.size() > 0 );

	cache.clear();
	for( unsigned i = 0; i < asDiff.size(); ++i )
	{
		Difficulty dc = StringToDifficulty(asDiff[i]);
		if( dc == DIFFICULTY_INVALID )
			RageException::Throw( "Unknown difficulty \"%s\" in CourseDifficultiesToShow", asDiff[i].c_str() );
		cache.insert( dc );
	}

	fExpiration = RageTimer::GetTimeSinceStart()+1;
	ret = cache;
}

bool GameState::ChangePreferredDifficulty( PlayerNumber pn, int dir )
{
	set<Difficulty> asDiff;
	GetDifficultiesToShow( asDiff );

	Difficulty dc = m_PreferredDifficulty[pn];
	while( 1 )
	{
		dc = (Difficulty)(dc+dir);
		if( dc < 0 || dc >= NUM_DIFFICULTIES )
			return false;

		if( asDiff.find(dc) == asDiff.end() )
			continue; /* not available */
	}

	return ChangePreferredDifficulty( pn, dc );
}

void GameState::GetCourseDifficultiesToShow( set<CourseDifficulty> &ret )
{
	static float fExpiration = -999;
	static set<CourseDifficulty> cache;
	if( RageTimer::GetTimeSinceStart() < fExpiration )
	{
		ret = cache;
		return;
	}

	CStringArray asDiff;
	split( COURSE_DIFFICULTIES_TO_SHOW, ",", asDiff );
	ASSERT( asDiff.size() > 0 );

	cache.clear();
	for( unsigned i = 0; i < asDiff.size(); ++i )
	{
		CourseDifficulty cd = StringToCourseDifficulty(asDiff[i]);
		if( cd == DIFFICULTY_INVALID )
			RageException::Throw( "Unknown difficulty \"%s\" in CourseDifficultiesToShow", asDiff[i].c_str() );
		cache.insert( cd );
	}

	fExpiration = RageTimer::GetTimeSinceStart()+1;
	ret = cache;
}

bool GameState::ChangePreferredCourseDifficulty( PlayerNumber pn, CourseDifficulty cd )
{
	m_PreferredCourseDifficulty[pn] = cd;

	if( PREFSMAN->m_bLockCourseDifficulties )
		FOREACH_PlayerNumber( p )
			m_PreferredCourseDifficulty[p] = m_PreferredCourseDifficulty[pn];

	return true;
}

bool GameState::ChangePreferredCourseDifficulty( PlayerNumber pn, int dir )
{
	/* If we have a course selected, only choose among difficulties available in the course. */
	const Course *pCourse = this->m_pCurCourse;

	set<CourseDifficulty> asDiff;
	GetCourseDifficultiesToShow( asDiff );

	CourseDifficulty cd = m_PreferredCourseDifficulty[pn];
	while( 1 )
	{
		cd = (CourseDifficulty)(cd+dir);
		if( cd < 0 || cd >= NUM_DIFFICULTIES )
			return false;
		if( asDiff.find(cd) == asDiff.end() )
			continue; /* not available */
		if( !pCourse || pCourse->GetTrail( GetCurrentStyle()->m_StepsType, cd ) )
			break;
	}

	return ChangePreferredCourseDifficulty( pn, cd );
}

bool GameState::IsCourseDifficultyShown( CourseDifficulty cd )
{
	set<CourseDifficulty> asDiff;
	GetCourseDifficultiesToShow( asDiff );
	return asDiff.find(cd) != asDiff.end();
}

Difficulty GameState::GetEasiestNotesDifficulty() const
{
	Difficulty dc = DIFFICULTY_INVALID;
	FOREACH_HumanPlayer( p )
	{
		if( this->m_pCurSteps[p] == NULL )
		{
			LOG->Warn( "GetEasiestNotesDifficulty called but p%i hasn't chosen notes", p+1 );
			continue;
		}
		dc = min( dc, this->m_pCurSteps[p]->GetDifficulty() );
	}
	return dc;
}

bool GameState::EasterEggsEnabled()
{
	return PREFSMAN->m_bEasterEggs;
}

bool PlayerIsUsingModifier( PlayerNumber pn, const CString sModifier )
{
	PlayerOptions po = GAMESTATE->m_PlayerOptions[pn];
	SongOptions so = GAMESTATE->m_SongOptions;
	po.FromString( sModifier );
	so.FromString( sModifier );

	return po == GAMESTATE->m_PlayerOptions[pn] && so == GAMESTATE->m_SongOptions;
}

RageColor GameState::GetGroupColor()
{
	if( m_pCurSong == NULL || m_pCurStyle == NULL )
		return RageColor(1,1,1,1);

	return SONGMAN->GetGroupColor( m_pCurSong->m_sGroupName );
}

RageColor GameState::GetSongColor()
{
	if( m_pCurSong == NULL || m_pCurStyle == NULL )
		return RageColor(1,1,1,1);

	return SONGMAN->GetSongColor( m_pCurSong );
}

bool GameState::IsHiddenDifficulty( PlayerNumber pn )
{
	if( m_pCurSteps[pn] == NULL )
		return false;

	return m_pCurSteps[pn]->m_bHiddenDifficulty;
}

#include "LuaFunctions.h"
LuaFunction_PlayerNumber( IsPlayerEnabled,		GAMESTATE->IsPlayerEnabled(pn) )
LuaFunction_PlayerNumber( IsHumanPlayer,		GAMESTATE->IsHumanPlayer(pn) )
LuaFunction_PlayerNumber( IsPlayerUsingProfile,	PROFILEMAN->IsUsingProfile(pn) )
LuaFunction_PlayerNumber( IsWinner,				GAMESTATE->GetStageResult(pn)==RESULT_WIN )
LuaFunction_PlayerNumber( IsHiddenDifficulty,	GAMESTATE->IsHiddenDifficulty(pn) )

LuaFunction_NoArgs( IsCourseMode,				GAMESTATE->IsCourseMode() )
LuaFunction_NoArgs( IsDemonstration,			GAMESTATE->m_bDemonstrationOrJukebox )
LuaFunction_NoArgs( StageIndex,					GAMESTATE->GetStageIndex() )
LuaFunction_NoArgs( StageNumber,				GAMESTATE->GetStage() )
LuaFunction_NoArgs( NumStagesLeft,				GAMESTATE->GetNumStagesLeft() )
LuaFunction_NoArgs( NumSongsLeft,				GAMESTATE->GetNumSongsLeft() )
LuaFunction_NoArgs( IsFinalStage,				GAMESTATE->IsFinalStage() )
LuaFunction_NoArgs( IsExtraStage,				GAMESTATE->IsExtraStage() )
LuaFunction_NoArgs( IsExtraStage2,				GAMESTATE->IsExtraStage2() )
LuaFunction_NoArgs( EasterEggs,					GAMESTATE->EasterEggsEnabled() )
LuaFunction_NoArgs( CourseSongIndex,			GAMESTATE->GetCourseSongIndex() )
LuaFunction_NoArgs( PlayModeName,				PlayModeToString(GAMESTATE->m_PlayMode) )
LuaFunction_NoArgs( CurStyleName,				CString( GAMESTATE->GetCurrentStyle() == NULL ? "none": GAMESTATE->GetCurrentStyle()->m_szName ) )
LuaFunction_NoArgs( GetNumPlayersEnabled,		GAMESTATE->GetNumPlayersEnabled() )
LuaFunction_NoArgs( PlayerUsingBothSides,		GAMESTATE->PlayerUsingBothSides() )
LuaFunction_NoArgs( GetEasiestNotesDifficulty,	GAMESTATE->GetEasiestNotesDifficulty() )
LuaFunction_NoArgs( MusicWheelDirection,		GAMESTATE->m_iMusicWheelDirection )

LuaFunction_NoArgs( IsEventMode,				GAMESTATE->IsEventMode() )
LuaFunction_NoArgs( GetScreenWidth,				GAMESTATE->GetScreenWidth() )
LuaFunction_NoArgs( GetScreenHeight,			GAMESTATE->GetScreenHeight() )
LuaFunction_NoArgs( GetScreenAspectRatio,		GAMESTATE->GetScreenAspectRatio() )
LuaFunction_NoArgs( GetDisplayWidth,			GAMESTATE->GetDisplayWidth() )
LuaFunction_NoArgs( GetDisplayHeight,			GAMESTATE->GetDisplayHeight() )
LuaFunction_NoArgs( GetDisplayAspectRatio,		GAMESTATE->GetDisplayAspectRatio() )
LuaFunction_NoArgs( GetDPIZoomX,				GAMESTATE->GetDPIZoomX() )
LuaFunction_NoArgs( GetDPIZoomY,				GAMESTATE->GetDPIZoomY() )

LuaFunction_Str(	GetEnv,	GAMESTATE->m_mapEnv[str] )
LuaFunction_StrStr(	SetEnv,	GAMESTATE->m_mapEnv[str1] = str2 )

/* Return an integer into SONGMAN->m_pSongs.  This lets us do input checking, which we
 * can't easily do if we return pointers. */
LuaFunction_NoArgs( CurSong,		GAMESTATE->m_pCurSong )
LuaFunction_PlayerNumber( CurSteps,	GAMESTATE->m_pCurSteps[pn] )

int LuaFunc_UsingModifier( lua_State *L )
{
	REQ_ARGS( "UsingModifier", 2 );
	REQ_ARG_NUMBER_RANGE( "UsingModifier", 1, 1, NUM_PLAYERS );
	REQ_ARG( "UsingModifier", 2, string );

	const PlayerNumber pn = (PlayerNumber) (int(lua_tonumber( L, 1 ))-1);
	const CString modifier = lua_tostring( L, 2 );
	LUA_RETURN( PlayerIsUsingModifier( pn, modifier ) );
}
LuaFunction( UsingModifier );


#if defined( WITH_COIN_MODE )

void GameState::AldoniosCode()
{
	if( PREFSMAN->GetCoinMode() == PrefsManager::COIN_PAY )
	{
		int iPrize, iMultiplier;
		CString sMessage;
		switch( rand()%100 )
		{
		case 0:
			iPrize = 100;
			iMultiplier = PREFSMAN->m_iCoinsPerCredit;
			sMessage = "Jackpot!!! You won %d credits, courtesy of Aldo_MX!";
			break;
		case 1:
			iPrize = rand()%50 + 1;
			iMultiplier = PREFSMAN->m_iCoinsPerCredit;
			// TODO - Aldo_MX: Traducir xD!
			sMessage = "No hay melate sin revancha! Has ganado %d creditos, cortesia de Aldo_MX!";
			break;
		default:
			switch( rand()%4 )
			{
			case 0:
				iPrize = rand()%PREFSMAN->m_iCoinsPerCredit + 1;
				iMultiplier = PREFSMAN->m_iCoinsPerCredit;
				sMessage = "Congratulations! You won %d credits, courtesy of Aldo_MX!";
				break;
			case 1:
				iPrize = 1;
				iMultiplier = PREFSMAN->m_iCoinsPerCredit;
				sMessage = "Congratulations! You won %d credit, courtesy of Aldo_MX!";
				break;
			case 2:
				iPrize = rand()%PREFSMAN->m_iCoinsPerCredit + 1;
				iMultiplier = 1;
				sMessage = "Congratulations! You won %d coins, courtesy of Aldo_MX!";
				break;
			default:
				iPrize = 1;
				iMultiplier = 1;
				sMessage = "Congratulations! You won %d coin, courtesy of Aldo_MX!";
			}
		}
		InsertCoin( iPrize * iMultiplier, true );
		SCREENMAN->SystemMessage( ssprintf(sMessage, iPrize) );
	}
}

bool GameState::EnoughCreditsToJoin() const
{
	switch( PREFSMAN->GetCoinMode() )
	{
	case PrefsManager::COIN_PAY:
		//// Aldo_MX: Intento 2 de "evitar paymode si no estan autorizados"
		//if( !PREFSMAN->m_bPayMode )
		//	ExitGame();
		return m_iCoins >= PREFSMAN->m_iCoinsPerCredit;
	case PrefsManager::COIN_HOME:
	case PrefsManager::COIN_FREE:
		return true;
	default:
		ASSERT(0);
		return false;
	}
}

LuaFunction_NoArgs( CoinModeName, PREFSMAN->CoinModeToString(PREFSMAN->GetCoinMode()) )

#else
LuaFunction_NoArgs( CoinModeName, "home" )
#endif

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * Contains code (c) 2001-2004 Chris Danford, Glenn Maynard, Chris Gomez.
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
