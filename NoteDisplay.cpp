#include "global.h"
#include "NoteDisplay.h"
#include "Steps.h"
#include "PrefsManager.h"
#include "GameState.h"
#include "NoteSkinManager.h"
#include "RageException.h"
#include "ArrowEffects.h"
#include "RageLog.h"
#include "RageDisplay.h"
#include "NoteTypes.h"
#include "NoteFieldPositioning.h"
#include "ActorUtil.h"
#include "NoteDataWithScoring.h"
#include "Game.h"
#include "ThemeManager.h"
#include "EnumHelper.h"

static const CString PartNames[NUM_PARTS] = {
	"Note",
	"Addition",
	"Mine",
	"Shock",
	"Potion",
	"Lift"
};
XToString( Part );
StringToX( Part );
#define FOREACH_Part( p ) FOREACH_ENUM( Part, NUM_PARTS, p )

static const CString HoldPartNames[NUM_HOLD_PARTS] = {
	"Head",
	"Tail",
	"TopCap",
	"Body",
	"BottomCap"
};
XToString( HoldPart );
StringToX( HoldPart );
#define FOREACH_HoldPart( hp ) FOREACH_ENUM( HoldPart, NUM_HOLD_PARTS, hp )

struct RoutineColors : public vector<RageColor>
{
	const static int NUM_DEFAULT_COLORS = 8;
	const static float DEFAULT_COLORS[8][4];
	const RageColor& GetColor( unsigned int color ) const { return this->at( (color - 1) % this->size() ); }
};
const float RoutineColors::DEFAULT_COLORS[NUM_DEFAULT_COLORS][4] = {
	{0.f,	0.75f,	1.f,	1.f},	// blue
	{1.f,	0.5f,	0.f,	1.f},	// orange
	{1.f,	1.f,	0.f,	1.f},	// yellow
	{0.f,	1.f,	0.f,	1.f},	// green
	{1.f,	0.5f,	1.f,	1.f},	// pink
	{1.f,	1.f,	1.f,	1.f},	// white
	{0.5f,	0.75f,	0.75f,	1.f},	// turquoise
	{0.75f,	0.5f,	0.5f,	1.f},	// brown
};

// cache
struct NoteMetricCache_t
{
	float m_fAnimationLengthInBeats[NUM_PARTS];
	bool m_bAnimationIsVivid[NUM_PARTS];
	bool m_bAnimationIsNoteColor[NUM_PARTS];
	bool m_bTapNoteUseLighting[NUM_PARTS];

	bool m_bEnableRoutineTap;
	bool m_bDiffuseRoutineText;
	bool m_bRoutineAnimationIsVivid;
	float m_fRoutineAnimationLengthInBeats;

	float m_fTapMissGrayPercent;
	bool m_bFlipHeadAndTailWhenReverse;		// Forces any holds type; for reverse compatability

	float m_fHoldAnimationLengthInBeats[NUM_HOLD_TYPES][NUM_HOLD_PARTS];
	bool m_bHoldAnimationIsVivid[NUM_HOLD_TYPES][NUM_HOLD_PARTS];
	bool m_bHoldAnimationIsNoteColor[NUM_HOLD_TYPES][NUM_HOLD_PARTS];

	bool m_bDrawHoldHeadForTapsOnSameRow[NUM_HOLD_TYPES];
	bool m_bHoldHeadIsAboveWavyParts[NUM_HOLD_TYPES];
	bool m_bHoldTailIsAboveWavyParts[NUM_HOLD_TYPES];

	float m_fStartDrawingHoldBodyOffsetFromHead[NUM_HOLD_TYPES];
	float m_fStopDrawingHoldBodyOffsetFromTail[NUM_HOLD_TYPES];
	float m_fHoldNGGrayPercent[NUM_HOLD_TYPES];

	bool m_bHoldHeadUseLighting[NUM_HOLD_TYPES];
	bool m_bHoldTailUseLighting[NUM_HOLD_TYPES];
	bool m_bFlipHoldHeadAndTailWhenReverse[NUM_HOLD_TYPES];
	bool m_bEnableRoutineHold[NUM_HOLD_TYPES];

	RoutineColors m_RoutineColors;

	void Load(CString skin, const CString &name);
} *NoteMetricCache;

void NoteMetricCache_t::Load(CString skin, const CString &name)
{
	// Set initial values, in case an item is not included in the Noteskin metrics
	FOREACH_Part( p )
	{
		m_fAnimationLengthInBeats[p] = 0.0f;
		m_bAnimationIsVivid[p] = false;
		m_bAnimationIsNoteColor[p] = false;
		m_bTapNoteUseLighting[p] = false;
	}

	m_bEnableRoutineTap = false;
	m_bDiffuseRoutineText = false;
	m_bRoutineAnimationIsVivid = false;
	m_fRoutineAnimationLengthInBeats = 0.0f;

	m_fTapMissGrayPercent = 0.0f;
	m_bFlipHeadAndTailWhenReverse = false;

	FOREACH_HoldType( ht )
	{
		FOREACH_HoldPart( hp )
		{
			m_fHoldAnimationLengthInBeats[ht][hp] = 0.0f;
			m_bHoldAnimationIsVivid[ht][hp] = false;
			m_bHoldAnimationIsNoteColor[ht][hp] = false;
		}

		m_bDrawHoldHeadForTapsOnSameRow[ht] = false;
		m_bHoldHeadIsAboveWavyParts[ht] = false;
		m_bHoldTailIsAboveWavyParts[ht] = false;

		m_fStartDrawingHoldBodyOffsetFromHead[ht] = 0;
		m_fStopDrawingHoldBodyOffsetFromTail[ht] = 0;
		m_fHoldNGGrayPercent[ht] = 0.0f;

		m_bHoldHeadUseLighting[ht] = false;
		m_bHoldTailUseLighting[ht] = false;
		m_bFlipHoldHeadAndTailWhenReverse[ht] = false;
		m_bEnableRoutineHold[ht] = false;
	}

	// Now we load up the values from the NoteSkin metrics

	FOREACH_Part( p )
	{
		m_fAnimationLengthInBeats[p]	= NOTESKIN->GetMetricF(skin, name, "Tap" + PartToString(p) + "AnimationLengthInBeats");
		m_bAnimationIsVivid[p]			= NOTESKIN->GetMetricB(skin, name, "Tap" + PartToString(p) + "AnimationIsVivid");
		m_bAnimationIsNoteColor[p]		= NOTESKIN->GetMetricB(skin, name, "Tap" + PartToString(p) + "AnimationIsNoteColor");
		m_bTapNoteUseLighting[p]		= NOTESKIN->GetMetricB(skin, name, "Tap" + PartToString(p) + "UseLighting");
	}

	m_bEnableRoutineTap					= NOTESKIN->GetMetricB(skin, name, "EnableRoutineTap");
	m_bDiffuseRoutineText				= NOTESKIN->GetMetricB(skin, name, "DiffuseRoutineText");
	m_bRoutineAnimationIsVivid			= NOTESKIN->GetMetricB(skin, name, "RoutineTapNoteAnimationIsVivid");
	m_fRoutineAnimationLengthInBeats	= NOTESKIN->GetMetricF(skin, name, "RoutineTapNoteAnimationLengthInBeats");

	m_fTapMissGrayPercent			= NOTESKIN->GetMetricF(skin, name, "TapMissGrayPercent");
	m_bFlipHeadAndTailWhenReverse	= NOTESKIN->GetMetricB(skin, name, "FlipHeadAndTailWhenReverse");

	FOREACH_HoldType( ht )
	{
		FOREACH_HoldPart( hp )
		{
			m_fHoldAnimationLengthInBeats[ht][hp]	= NOTESKIN->GetMetricF(skin, name, HoldTypeToString(ht) + HoldPartToString(hp) + "AnimationLengthInBeats");
			m_bHoldAnimationIsVivid[ht][hp]			= NOTESKIN->GetMetricB(skin, name, HoldTypeToString(ht) + HoldPartToString(hp) + "AnimationIsVivid");
			m_bHoldAnimationIsNoteColor[ht][hp]		= NOTESKIN->GetMetricB(skin, name, HoldTypeToString(ht) + HoldPartToString(hp) + "AnimationIsNoteColor");
		}

		m_bDrawHoldHeadForTapsOnSameRow[ht]	= NOTESKIN->GetMetricB(skin, name, "Draw" + HoldTypeToString(ht) + "HeadForTapsOnSameRow");
		m_bHoldHeadIsAboveWavyParts[ht]		= NOTESKIN->GetMetricB(skin, name, HoldTypeToString(ht) + "HeadIsAboveWavyParts");
		m_bHoldTailIsAboveWavyParts[ht]		= NOTESKIN->GetMetricB(skin, name, HoldTypeToString(ht) + "TailIsAboveWavyParts");

		m_fStartDrawingHoldBodyOffsetFromHead[ht]	= NOTESKIN->GetMetricF(skin, name, "StartDrawing" + HoldTypeToString(ht) + "BodyOffsetFromHead");
		m_fStopDrawingHoldBodyOffsetFromTail[ht]	= NOTESKIN->GetMetricF(skin, name, "StopDrawing" + HoldTypeToString(ht) + "BodyOffsetFromTail");
		m_fHoldNGGrayPercent[ht]					= NOTESKIN->GetMetricF(skin, name, HoldTypeToString(ht) + "NGGrayPercent");

		m_bHoldHeadUseLighting[ht]				= NOTESKIN->GetMetricB(skin, name, HoldTypeToString(ht) + "HeadUseLighting");
		m_bHoldTailUseLighting[ht]				= NOTESKIN->GetMetricB(skin, name, HoldTypeToString(ht) + "TailUseLighting");
		m_bFlipHoldHeadAndTailWhenReverse[ht]	= NOTESKIN->GetMetricB(skin, name, "Flip" + HoldTypeToString(ht) + "HeadAndTailWhenReverse");
		m_bEnableRoutineHold[ht]				= NOTESKIN->GetMetricB(skin, name, "EnableRoutine" + HoldTypeToString(ht));
	}

	m_RoutineColors.clear();
	if( NOTESKIN->HasMetric(skin, name, "NumRoutineColors") )
	{
		int num_colors = NOTESKIN->GetMetricI(skin, name, "NumRoutineColors");
		m_RoutineColors.reserve(num_colors);
		for( int c = 0; c < num_colors; )
			m_RoutineColors.push_back( NOTESKIN->GetMetricC(skin, name, ssprintf("RoutineColor%d", ++c)) );
	}
	else
	{
		m_RoutineColors.assign(
			(RageColor*)RoutineColors::DEFAULT_COLORS[0],
			(RageColor*)RoutineColors::DEFAULT_COLORS[RoutineColors::NUM_DEFAULT_COLORS]
		);
	}
}


struct NoteResource
{
	NoteResource( CString sPath ): m_sPath(sPath)
	{
		m_iRefCount = 0;
		m_pActor = NULL;
	}

	~NoteResource()
	{
		delete m_pActor;
	}

	const CString m_sPath; /* should be refcounted along with g_NoteResource[] */
	int m_iRefCount;
	Actor *m_pActor;
};

static map<CString, NoteResource *> g_NoteResource;

static NoteResource *MakeNoteResource( const CString &sPath, bool bSpriteOnly )
{
	ASSERT( sPath != "" );

	map<CString, NoteResource *>::iterator it = g_NoteResource.find( sPath );
	if( it == g_NoteResource.end() )
	{
		NoteResource *pRes = new NoteResource( sPath );
		if( bSpriteOnly )
		{
			Sprite *pSprite = new Sprite;
			pSprite->Load( sPath );
			pRes->m_pActor = pSprite;
		}
		else
			pRes->m_pActor = MakeActor( sPath );

		g_NoteResource[sPath] = pRes;
		it = g_NoteResource.find( sPath );
	}

	NoteResource *pRet = it->second;
	++pRet->m_iRefCount;
	return pRet;
}

static NoteResource *FindNoteResource( const Actor *pActor )
{
	map<CString, NoteResource *>::iterator it;
	for( it = g_NoteResource.begin(); it != g_NoteResource.end(); ++it )
	{
		NoteResource *pRes = it->second;
		if( pRes->m_pActor == pActor )
			return pRes;
	}

	return NULL;
}

static void DeleteNoteResource( const Actor *pActor )
{
	if( pActor == NULL )
		return;

	NoteResource *pRes = FindNoteResource( pActor );
	ASSERT( pRes != NULL );

	ASSERT_M( pRes->m_iRefCount > 0, ssprintf("%i", pRes->m_iRefCount) );
	--pRes->m_iRefCount;
	if( pRes->m_iRefCount )
		return;

	g_NoteResource.erase( pRes->m_sPath );
	delete pRes;
}

Actor* MakeRefcountedActor( const CString &sPath, bool bIsSprite = false )
{
	NoteResource *pRes = MakeNoteResource( sPath, bIsSprite );
	return pRes->m_pActor;
}

NoteDisplay::NoteDisplay()
{
	FOREACH_NoteType( nt )
	{
		FOREACH_Part( p )
			m_pTapNote[p][nt] = NULL;

		m_pTapHidden[nt] = NULL;

		FOREACH_HoldType( ht )
		{
			FOREACH_HoldPart( hp )
			{
				m_pHoldActorActive[ht][hp][nt] = NULL;
				m_pHoldActorInactive[ht][hp][nt] = NULL;
			}
		}
	}

	cache = unique_ptr< NoteMetricCache_t >( new NoteMetricCache_t() );
}

NoteDisplay::~NoteDisplay()
{
	FOREACH_NoteType( nt )
	{
		FOREACH_Part( p )
			DeleteNoteResource( m_pTapNote[p][nt] );

		DeleteNoteResource( m_pTapHidden[nt] );

		FOREACH_HoldType( ht )
		{
			FOREACH_HoldPart( hp )
			{
				DeleteNoteResource( m_pHoldActorActive[ht][hp][nt] );
				DeleteNoteResource( m_pHoldActorInactive[ht][hp][nt] );
			}
		}
	}
}

CString NoteDisplay::GetNoteColorPath( CString sNoteSkin, CString sButton, CString sNoteActor, NoteType nt = NOTE_TYPE_INVALID )
{
	if( nt == NOTE_TYPE_INVALID )
		return NOTESKIN->GetPathToFromNoteSkinAndButton(sNoteSkin, sButton, sNoteActor);

	CString sNoteColorPath = NOTESKIN->GetPathToFromNoteSkinAndButton(sNoteSkin, sButton, sNoteActor + " " + NoteTypeToString(nt), true);
	if( sNoteColorPath == "" )
	{
		CString sLegacyNoteColorPath;

		switch( nt )
		{
		// Legacy SM didn't have 96th & 128th, fallback to 64th
		case NOTE_TYPE_96TH:
		case NOTE_TYPE_128TH:
			sLegacyNoteColorPath = NOTESKIN->GetPathToFromNoteSkinAndButton(sNoteSkin, sButton, sNoteActor + " " + "64th");
			break;

		//// 512nd is new to SMA, so iterate for SMA note first, otherwise fallback to legacy
		//case NOTE_TYPE_512ND:
		//	sLegacyNoteColorPath = NOTESKIN->GetPathToFromNoteSkinAndButton(sNoteSkin, sButton, sNoteActor + " " + "384th", true);
		//	if( sLegacyNoteColorPath == "" )
		//		sLegacyNoteColorPath = NOTESKIN->GetPathToFromNoteSkinAndButton(sNoteSkin, sButton, sNoteActor + " " + "192nd");
		//	break;
		//
		//// 1024th, 1536th & 3072nd are new to SMA too, iterate for SMA first, otherwise fall through to legacy SM
		//case NOTE_TYPE_1024TH:
		//case NOTE_TYPE_1536TH:
		//case NOTE_TYPE_3072ND:
		//	sLegacyNoteColorPath = NOTESKIN->GetPathToFromNoteSkinAndButton(sNoteSkin, sButton, sNoteActor + " " + "768th", true);
		//	if( sLegacyNoteColorPath != "" )
		//		break;

		// Legacy SM didn't have 256th, 384th & 768th, fallback to 192nd
		case NOTE_TYPE_256TH:
		case NOTE_TYPE_384TH:
		case NOTE_TYPE_768TH:
			sLegacyNoteColorPath = NOTESKIN->GetPathToFromNoteSkinAndButton(sNoteSkin, sButton, sNoteActor + " " + "192nd");
			break;

		default:
			sLegacyNoteColorPath = NOTESKIN->GetPathToFromNoteSkinAndButton(sNoteSkin, sButton, sNoteActor + " " + NoteTypeToString(nt));
		}

		// Only replace path if the file exists, otherwise let SMA prompt that the real one is missing
		if( DoesFileExist(sLegacyNoteColorPath) )
		{
			LOG->Trace("NoteDisplay::Load() - \"%s\" not found, using \"%s\" instead.", sNoteColorPath.c_str(), sLegacyNoteColorPath.c_str());
			sNoteColorPath = sLegacyNoteColorPath;
		}
	}
	return sNoteColorPath;
}

void NoteDisplay::Load( int iColNum, PlayerNumber pn, CString NoteSkin, float fYReverseOffsetPixels, bool bIsRoutine, AnimationTiming animationTiming)
{
	if( GAMESTATE->m_bEditing )
	{
		m_RoutinePlayerNumber.LoadFromFont( THEME->GetPathToF("ScreenEdit Note Player") );
		m_RoutinePlayerNumber.SetShadowLength(2);
		m_RoutinePlayerNumber.SetDiffuse( RageColor(1,1,1,1) );
		m_RoutinePlayerNumber.SetGlow( RageColor(1,1,1,0) );
		m_RoutinePlayerNumber.SetHorizAlign( Actor::align_center );
		m_RoutinePlayerNumber.SetVertAlign( Actor::align_middle );
	}

	m_PlayerNumber = pn;
	m_animationTiming = animationTiming;
	m_bIsRoutine = bIsRoutine;
	m_fYReverseOffsetPixels = fYReverseOffsetPixels;

	/* Normally, this is empty and we use the style table entry via ColToButtonName. */
	NoteFieldMode &mode = g_NoteFieldMode[pn];
	CString Button = mode.NoteButtonNames[iColNum];
	if( Button == "" )
		Button = GAMESTATE->GetCurrentGame()->ColToButtonName( iColNum );
	cache->Load( NoteSkin, Button );

	CString RoutineButton = "Routine " + Button;

	FOREACH_Part( p )
	{
		if( p == PART_TAP && bIsRoutine && cache->m_bEnableRoutineTap )
		{
			m_pTapNote[p][0] = MakeRefcountedActor( GetNoteColorPath( NoteSkin, RoutineButton, "Tap " + PartToString(p) ) );
		}
		else if( cache->m_bAnimationIsNoteColor[p] )
		{
			FOREACH_NoteType( nt )
				m_pTapNote[p][nt] = MakeRefcountedActor( GetNoteColorPath( NoteSkin, Button, "Tap " + PartToString(p), nt ) );
		}
		else
		{
			m_pTapNote[p][0] = MakeRefcountedActor( GetNoteColorPath( NoteSkin, Button, "Tap " + PartToString(p) ) );
		}
	}

	m_pTapHidden[0] = MakeRefcountedActor( GetNoteColorPath(NoteSkin, Button, "Tap Hidden") );

	FOREACH_HoldType( ht )
	{
		FOREACH_HoldPart( hp )
		{
			bool bIsSprite;
			switch( hp )
			{
			case HOLD_PART_HEAD:
			case HOLD_PART_TAIL:
				bIsSprite = false;
				break;

			default:
				bIsSprite = true;
			}

			if( bIsRoutine && cache->m_bEnableRoutineHold[ht] )
			{
				m_pHoldActorActive[ht][hp][0] = MakeRefcountedActor( GetNoteColorPath(NoteSkin, RoutineButton, HoldTypeToString(ht) + " " + HoldPartToString(hp) + " Active"), bIsSprite );
				m_pHoldActorInactive[ht][hp][0] = MakeRefcountedActor( GetNoteColorPath(NoteSkin, RoutineButton, HoldTypeToString(ht) + " " + HoldPartToString(hp) + " Inactive"), bIsSprite );
			}
			else if( cache->m_bHoldAnimationIsNoteColor[ht][hp] )
			{
				FOREACH_NoteType( nt )
				{
					m_pHoldActorActive[ht][hp][nt] = MakeRefcountedActor( GetNoteColorPath(NoteSkin, Button, HoldTypeToString(ht) + " " + HoldPartToString(hp) + " Active", nt), bIsSprite );
					m_pHoldActorInactive[ht][hp][nt] = MakeRefcountedActor( GetNoteColorPath(NoteSkin, Button, HoldTypeToString(ht) + " " + HoldPartToString(hp) + " Inactive", nt), bIsSprite );
				}
			}
			else
			{
				m_pHoldActorActive[ht][hp][0] = MakeRefcountedActor( GetNoteColorPath(NoteSkin, Button, HoldTypeToString(ht) + " " + HoldPartToString(hp) + " Active"), bIsSprite );
				m_pHoldActorInactive[ht][hp][0] = MakeRefcountedActor( GetNoteColorPath(NoteSkin, Button, HoldTypeToString(ht) + " " + HoldPartToString(hp) + " Inactive"), bIsSprite );
			}
		}
	}
}

void NoteDisplay::Update( float fDeltaTime )
{
	/* This function is static: it's called once per game loop, not once per
	 * NoteDisplay.  Update each cached item exactly once. */
	map<CString, NoteResource *>::iterator it;
	for( it = g_NoteResource.begin(); it != g_NoteResource.end(); ++it )
	{
		NoteResource *pRes = it->second;
		pRes->m_pActor->Update( fDeltaTime );
	}
}

void NoteDisplay::SetActiveFrame( float fNoteBeat, Actor &actorToSet, float fAnimationLengthInBeats, bool bVivid )
{
	float fPercentIntoAnimation = 0.f;
	switch (m_animationTiming)
	{
	case AT_STEP:		fPercentIntoAnimation = GAMESTATE->m_fPlayerBeat[m_PlayerNumber];	break;
	case AT_BGA:		fPercentIntoAnimation = GAMESTATE->m_fSongBeat;						break;
	case AT_SECONDS:	fPercentIntoAnimation = GAMESTATE->m_fMusicSeconds;					break;
	default:
		ASSERT(0);
	}
	fPercentIntoAnimation = fmodf(fPercentIntoAnimation, fAnimationLengthInBeats) / fAnimationLengthInBeats;
	float fNoteBeatFraction = fmodf(fNoteBeat, 1.0f);

	if (bVivid)
	{
		// changed to deal with the minor complaint that the color cycling is
		// one tick off in general
		const float fFraction = fNoteBeatFraction - 0.25f / fAnimationLengthInBeats;
		const float fInterval = 1.f / fAnimationLengthInBeats;
		fPercentIntoAnimation += froundf(fFraction, fInterval);
	}

	// just in case somehow we're majorly negative with the subtraction
	wrap(fPercentIntoAnimation, 1.f);

	float fLengthSeconds = actorToSet.GetAnimationLengthSeconds();
	actorToSet.SetSecondsIntoAnimation(fPercentIntoAnimation*fLengthSeconds);
}

Actor * NoteDisplay::GetTapNoteActor( float fNoteBeat, Part part )
{
	NoteType nt = NoteType(0);
	if( !m_bIsRoutine && cache->m_bAnimationIsNoteColor[part] )
		nt = BeatToNoteType( fNoteBeat );

	// NOTE_TYPE_INVALID is 768ths at this point.
	if( nt == NOTE_TYPE_INVALID )
		nt = NOTE_TYPE_768TH;
	nt = min( nt, (NoteType) (NUM_NOTE_TYPES-1) );

	Actor *pActorOut = m_pTapNote[part][nt];

	SetActiveFrame(
		fNoteBeat,
		*pActorOut,
		cache->m_fAnimationLengthInBeats[part],
		cache->m_bAnimationIsVivid[part] );

	return pActorOut;
}

Actor * NoteDisplay::GetTapHiddenActor( float fNoteBeat )
{
	NoteType nt = NoteType(0);
	// TODO: NoteColor for m_pTapHidden
	//if( !m_bIsRoutine && cache->m_bAnimationIsNoteColor[PART_TAP] )
	//	nt = BeatToNoteType( fNoteBeat );

	//// NOTE_TYPE_INVALID is 768ths at this point.
	//if( nt == NOTE_TYPE_INVALID )
	//	nt = NOTE_TYPE_768TH;
	//nt = min( nt, (NoteType) (NUM_NOTE_TYPES-1) );

	Actor *pActorOut = m_pTapHidden[nt];

	SetActiveFrame(
		fNoteBeat,
		*pActorOut,
		cache->m_fAnimationLengthInBeats[PART_TAP],
		cache->m_bAnimationIsVivid[PART_TAP] );

	return pActorOut;
}

Actor* NoteDisplay::GetHoldActor( const HoldNote& hn, HoldPart holdPart, bool bIsActive, float fNoteBeat )
{
	if( fNoteBeat == FLT_MAX )
		fNoteBeat = hn.GetStartBeat();

	NoteType nt = NoteType(0);
	if( !m_bIsRoutine && cache->m_bHoldAnimationIsNoteColor[hn.subtype][holdPart] )
		nt = BeatToNoteType( fNoteBeat );

	// NOTE_TYPE_INVALID is 768ths at this point.
	if( nt == NOTE_TYPE_INVALID )
		nt = NOTE_TYPE_768TH;
	nt = min( nt, (NoteType) (NUM_NOTE_TYPES-1) );

	Actor *pActorOut = bIsActive ? m_pHoldActorActive[hn.subtype][holdPart][nt] : m_pHoldActorInactive[hn.subtype][holdPart][nt];

	SetActiveFrame(
		fNoteBeat,
		*pActorOut,
		cache->m_fHoldAnimationLengthInBeats[hn.subtype][holdPart],
		cache->m_bHoldAnimationIsVivid[hn.subtype][holdPart] );

	return pActorOut;
}

static float ArrowGetAlphaOrGlow( bool bGlow, PlayerNumber pn, int iCol, float fYOffset, float fPercentFadeToFail, float fYReverseOffsetPixels )
{
	if( bGlow )
		return ArrowGetGlow( pn, iCol, fYOffset, fPercentFadeToFail, fYReverseOffsetPixels );
	else
		return ArrowGetAlpha( pn, iCol, fYOffset, fPercentFadeToFail, fYReverseOffsetPixels );
}

void NoteDisplay::DrawHoldTopCap( const HoldNote& hn, const bool bIsBeingHeld, float fYHead, float fYTail, int fYStep, int iCol, float fPercentFadeToFail, float fNoteAlpha, float fColorScale, bool bGlow )
{
	//
	// Draw the top cap (always wavy)
	//
	const int size = 4096;
	static RageSpriteVertex queue[size];
	Sprite* pSprTopCap = (Sprite *)GetHoldActor(hn, HOLD_PART_TOP_CAP, bIsBeingHeld);
	pSprTopCap->SetZoom( ArrowGetZoom( m_PlayerNumber ) );

	// draw manually in small segments
	RageSpriteVertex *v = &queue[0];
	RageTexture* pTexture = pSprTopCap->GetTexture();
	const RectF *pRect = pSprTopCap->GetCurrentTextureCoordRect();
	DISPLAY->ClearAllTextures();
	DISPLAY->SetTexture( 0, pTexture );
	DISPLAY->SetBlendMode( BLEND_NORMAL );
	DISPLAY->SetCullMode( CULL_NONE );
	DISPLAY->SetTextureWrapping(false);

	const float fFrameWidth  = pSprTopCap->GetZoomedWidth();
	const float fFrameHeight = pSprTopCap->GetZoomedHeight();

	float fYCapTop		= fYHead + cache->m_fStartDrawingHoldBodyOffsetFromHead[hn.subtype] - fFrameHeight;
	float fYCapBottom	= fYHead + cache->m_fStartDrawingHoldBodyOffsetFromHead[hn.subtype];

	if( bGlow )
		fColorScale = 1;

	bool bAllAreTransparent = true;
	bool bLast = false;
	// don't draw any part of the head that is after the middle of the tail
	float fY = fYCapTop;
	float fYStop = min(fYTail,fYCapBottom);
	for( ; !bLast; fY+=fYStep )
	{
		if( fY >= fYStop )
		{
			fY = fYStop;
			bLast = true;
		}

		const bool bUseRoutine = m_bIsRoutine && cache->m_bEnableRoutineHold[hn.subtype];

		const float fYOffset			= ArrowGetYOffsetFromYPos( m_PlayerNumber, iCol, fY, m_fYReverseOffsetPixels );
		const float fZ					= ArrowGetZPos(	m_PlayerNumber, fYOffset );
		const float fX					= ArrowGetXPos( m_PlayerNumber, iCol, fYOffset );
		const float fXLeft				= fX - fFrameWidth/2;
		const float fXRight				= fX + fFrameWidth/2;
		const float fTopDistFromHeadTop	= fY - fYCapTop;
		const float fTexCoordTop		= SCALE( fTopDistFromHeadTop, 0, fFrameHeight, pRect->top, pRect->bottom );
		const float fTexCoordLeft		= pRect->left;
		const float fTexCoordRight		= pRect->right;
		const float fAlpha				= ArrowGetAlphaOrGlow( bGlow, m_PlayerNumber, iCol, fYOffset, fPercentFadeToFail, m_fYReverseOffsetPixels ) * fNoteAlpha;
		RageColor color					= RageColor(fColorScale,fColorScale,fColorScale,fAlpha);

		if( bUseRoutine )
			color *= cache->m_RoutineColors.GetColor(hn.playerNumber);

		if( fAlpha > 0 )
			bAllAreTransparent = false;

		v[0].p = RageVector3(fXLeft,  fY, fZ); v[0].c = color; v[0].t = RageVector2(fTexCoordLeft,  fTexCoordTop),
		v[1].p = RageVector3(fXRight, fY, fZ); v[1].c = color; v[1].t = RageVector2(fTexCoordRight, fTexCoordTop);
		v+=2;
		if( v-queue >= size )
			break;
	}
	if( !bAllAreTransparent )
		DISPLAY->DrawQuadStrip( queue, v-queue );
}

void NoteDisplay::DrawHoldBody( const HoldNote& hn, const bool bIsBeingHeld, float fYHead, float fYTail, int fYStep, int iCol, float fPercentFadeToFail, float fNoteAlpha, float fColorScale, bool bGlow )
{
	//
	// Draw the body (always wavy)
	//
	const int size = 4096;
	static RageSpriteVertex queue[size];

	Sprite* pSprBody = (Sprite *)GetHoldActor(hn, HOLD_PART_BODY, bIsBeingHeld);
	pSprBody->SetZoom( ArrowGetZoom( m_PlayerNumber ) );

	// draw manually in small segments
	RageSpriteVertex *v = &queue[0];
	RageTexture* pTexture = pSprBody->GetTexture();
	const RectF *pRect = pSprBody->GetCurrentTextureCoordRect();
	DISPLAY->ClearAllTextures();
	DISPLAY->SetTexture( 0, pTexture );
	DISPLAY->SetBlendMode( BLEND_NORMAL );
	DISPLAY->SetCullMode( CULL_NONE );
	DISPLAY->SetTextureWrapping( true );

	const float fFrameWidth  = pSprBody->GetZoomedWidth();
	const float fFrameHeight = pSprBody->GetZoomedHeight();

	float fYBodyTop		= fYHead + cache->m_fStartDrawingHoldBodyOffsetFromHead[hn.subtype];
	float fYBodyBottom	= fYTail + cache->m_fStopDrawingHoldBodyOffsetFromTail[hn.subtype];

	const bool bReverse = GAMESTATE->m_CurrentPlayerOptions[m_PlayerNumber].GetReversePercentForColumn(iCol) > 0.5;
	bool bAnchorToBottom = bReverse && (cache->m_bFlipHeadAndTailWhenReverse || cache->m_bFlipHoldHeadAndTailWhenReverse[hn.subtype]);

	if( bGlow )
		fColorScale = 1;

	// top to bottom
	bool bAllAreTransparent = true;
	bool bLast = false;
	for( float fY = fYBodyTop; !bLast; fY += fYStep )
	{
		if( fY >= fYBodyBottom )
		{
			fY = fYBodyBottom;
			bLast = true;
		}
		
		const bool bUseRoutine = m_bIsRoutine && cache->m_bEnableRoutineHold[hn.subtype];

		const float fYOffset			= ArrowGetYOffsetFromYPos( m_PlayerNumber, iCol, fY, m_fYReverseOffsetPixels );
		const float fZ					= ArrowGetZPos(	m_PlayerNumber, fYOffset );
		const float fX					= ArrowGetXPos( m_PlayerNumber, iCol, fYOffset );
		const float fXLeft				= fX - fFrameWidth/2;
		const float fXRight				= fX + fFrameWidth/2;
		const float fDistFromBodyBottom	= fYBodyBottom - fY;
		const float fDistFromBodyTop	= fY - fYBodyTop;
		const float fTexCoordTop		= SCALE( bAnchorToBottom ? fDistFromBodyTop : fDistFromBodyBottom,    0, fFrameHeight, pRect->bottom, pRect->top );
		const float fTexCoordLeft		= pRect->left;
		const float fTexCoordRight		= pRect->right;
		const float fAlpha				= ArrowGetAlphaOrGlow( bGlow, m_PlayerNumber, iCol, fYOffset, fPercentFadeToFail, m_fYReverseOffsetPixels ) * fNoteAlpha;
		RageColor color					= RageColor(fColorScale,fColorScale,fColorScale,fAlpha);

		if( bUseRoutine )
			color *= cache->m_RoutineColors.GetColor(hn.playerNumber);

		if( fAlpha > 0 )
			bAllAreTransparent = false;

		v[0].p = RageVector3(fXLeft,  fY, fZ);	v[0].c = color; v[0].t = RageVector2(fTexCoordLeft,  fTexCoordTop);
		v[1].p = RageVector3(fXRight, fY, fZ);	v[1].c = color; v[1].t = RageVector2(fTexCoordRight, fTexCoordTop);
		v+=2;
		if( v-queue >= size )
			break;
	}

	if( !bAllAreTransparent )
		DISPLAY->DrawQuadStrip( queue, v-queue );
}

void NoteDisplay::DrawHoldBottomCap( const HoldNote& hn, const bool bIsBeingHeld, float fYHead, float fYTail, int fYStep, int iCol, float fPercentFadeToFail, float fNoteAlpha, float fColorScale, bool bGlow )
{
	//
	// Draw the bottom cap (always wavy)
	//
	const int size = 4096;
	static RageSpriteVertex queue[size];

	Sprite* pBottomCap = (Sprite *)GetHoldActor(hn, HOLD_PART_BOTTOM_CAP, bIsBeingHeld);
	pBottomCap->SetZoom( ArrowGetZoom( m_PlayerNumber ) );

	// draw manually in small segments
	RageSpriteVertex *v = &queue[0];
	RageTexture* pTexture = pBottomCap->GetTexture();
	const RectF *pRect = pBottomCap->GetCurrentTextureCoordRect();
	DISPLAY->ClearAllTextures();
	DISPLAY->SetTexture( 0, pTexture );
	DISPLAY->SetBlendMode( BLEND_NORMAL );
	DISPLAY->SetCullMode( CULL_NONE );
	DISPLAY->SetTextureWrapping(false);

	const float fFrameWidth		= pBottomCap->GetZoomedWidth();
	const float fFrameHeight	= pBottomCap->GetZoomedHeight();

	float fYCapTop		= fYTail + cache->m_fStopDrawingHoldBodyOffsetFromTail[hn.subtype];
	float fYCapBottom	= fYTail + cache->m_fStopDrawingHoldBodyOffsetFromTail[hn.subtype] + fFrameHeight;

	if( bGlow )
		fColorScale = 1;

	bool bAllAreTransparent = true;
	bool bLast = false;
	// don't draw any part of the tail that is before the middle of the head
	float fY = max(fYCapTop, fYHead);
	for( ; !bLast; fY+=fYStep )
	{
		if( fY >= fYCapBottom )
		{
			fY = fYCapBottom;
			bLast = true;
		}

		const bool bUseRoutine = m_bIsRoutine && cache->m_bEnableRoutineHold[hn.subtype];

		const float fYOffset			= ArrowGetYOffsetFromYPos( m_PlayerNumber, iCol, fY, m_fYReverseOffsetPixels );
		const float fZ					= ArrowGetZPos(	m_PlayerNumber, fYOffset );
		const float fX					= ArrowGetXPos( m_PlayerNumber, iCol, fYOffset );
		const float fXLeft				= fX - fFrameWidth/2;
		const float fXRight				= fX + fFrameWidth/2;
		const float fTopDistFromTail	= fY - fYCapTop;
		const float fTexCoordTop		= SCALE( fTopDistFromTail, 0, fFrameHeight, pRect->top, pRect->bottom );
		const float fTexCoordLeft		= pRect->left;
		const float fTexCoordRight		= pRect->right;
		const float fAlpha				= ArrowGetAlphaOrGlow( bGlow, m_PlayerNumber, iCol, fYOffset, fPercentFadeToFail, m_fYReverseOffsetPixels ) * fNoteAlpha;
		RageColor color					= RageColor(fColorScale,fColorScale,fColorScale,fAlpha);

		if( bUseRoutine )
			color *= cache->m_RoutineColors.GetColor(hn.playerNumber);

		if( fAlpha > 0 )
			bAllAreTransparent = false;

		v[0].p = RageVector3(fXLeft,  fY, fZ); v[0].c = color; v[0].t = RageVector2(fTexCoordLeft,  fTexCoordTop),
		v[1].p = RageVector3(fXRight, fY, fZ); v[1].c = color; v[1].t = RageVector2(fTexCoordRight, fTexCoordTop);
		v+=2;
		if( v-queue >= size )
			break;
	}
	if( !bAllAreTransparent )
		DISPLAY->DrawQuadStrip( queue, v-queue );
}

void NoteDisplay::DrawHoldTail( const HoldNote& hn, bool bIsBeingHeld, float fYTail, int iCol, float fPercentFadeToFail, float fNoteAlpha, float fColorScale, bool bGlow )
{
	//
	// Draw the tail
	//
	Actor* pActor = GetHoldActor(hn, HOLD_PART_TAIL, bIsBeingHeld);
	pActor->SetZoom( ArrowGetZoom( m_PlayerNumber ) );
	
	const bool bUseRoutine = m_bIsRoutine && cache->m_bEnableRoutineHold[hn.subtype];

	const float fY					= fYTail;
	const float fYOffset			= ArrowGetYOffsetFromYPos( m_PlayerNumber, iCol, fY, m_fYReverseOffsetPixels );
	const float fX					= ArrowGetXPos( m_PlayerNumber, iCol, fYOffset );
	const float fZ					= ArrowGetZPos(	m_PlayerNumber, fYOffset );
	const float fAlpha				= ArrowGetAlpha( m_PlayerNumber, iCol, fYOffset, fPercentFadeToFail, m_fYReverseOffsetPixels ) * fNoteAlpha;
	const float fGlow				= ArrowGetGlow( m_PlayerNumber, iCol, fYOffset, fPercentFadeToFail, m_fYReverseOffsetPixels );
	const RageColor colorGlow		= RageColor(1,1,1,fGlow);

	RageColor colorDiffuse = RageColor(fColorScale,fColorScale,fColorScale,fAlpha);

	if( bUseRoutine )
		colorDiffuse *= cache->m_RoutineColors.GetColor(hn.playerNumber);

	pActor->SetXY( fX, fY );
	pActor->SetZ( fZ );

	if( bGlow && bUseRoutine )
	{
		pActor->SetDiffuse( colorDiffuse );
		pActor->SetGlow( colorGlow );
	}
	else if( bGlow )
	{
		pActor->SetDiffuse( RageColor(1,1,1,0) );
		pActor->SetGlow( colorGlow );
	}
	else
	{
		pActor->SetDiffuse( colorDiffuse );
		pActor->SetGlow( RageColor(0,0,0,0) );
	}

	if( cache->m_bHoldTailUseLighting[hn.subtype] )
	{
		DISPLAY->SetLighting( true );
		DISPLAY->SetLightDirectional(
			0,
			RageColor(1,1,1,1),
			RageColor(1,1,1,1),
			RageColor(1,1,1,1),
			RageVector3(1, 0, +1) );
	}

	pActor->Draw();

	if( cache->m_bHoldTailUseLighting[hn.subtype] )
	{
		DISPLAY->SetLightOff( 0 );
		DISPLAY->SetLighting( false );
	}
}

void NoteDisplay::DrawHoldHead( const HoldNote& hn, bool bIsBeingHeld, bool bIsFlip, float fYHead, int iCol, float fPercentFadeToFail, float fNoteAlpha, float fColorScale, bool bGlow )
{
	//
	// Draw the head
	//

	Actor* pActor = GetHoldActor(hn, HOLD_PART_HEAD, bIsBeingHeld);
	pActor->SetZoom( ArrowGetZoom( m_PlayerNumber ) );
	
	const bool bUseRoutine = m_bIsRoutine && cache->m_bEnableRoutineHold[hn.subtype];

	// draw with normal Sprite
	const float fY					= fYHead;
	const float fYOffset			= ArrowGetYOffsetFromYPos( m_PlayerNumber, iCol, fY, m_fYReverseOffsetPixels );
	const float fX					= ArrowGetXPos( m_PlayerNumber, iCol, fYOffset );
	const float fZ					= ArrowGetZPos(	m_PlayerNumber, fYOffset );
	const float fAlpha				= ArrowGetAlpha( m_PlayerNumber, iCol, fYOffset, fPercentFadeToFail, m_fYReverseOffsetPixels ) * fNoteAlpha;
	const float fGlow				= ArrowGetGlow( m_PlayerNumber, iCol, fYOffset, fPercentFadeToFail, m_fYReverseOffsetPixels );
	const RageColor colorGlow		= RageColor(1,1,1,fGlow);

	RageColor colorDiffuse = RageColor(fColorScale,fColorScale,fColorScale,fAlpha);

	// For confusion
	const float fBeat = bIsFlip ? hn.GetEndBeat() : hn.GetStartBeat();
	const float fRotation = ArrowGetRotation( m_PlayerNumber, fBeat, true );
	pActor->SetRotationZ( fRotation );

	if( m_bIsRoutine )
	{
		if( GAMESTATE->m_bEditing )
		{
			m_RoutinePlayerNumber.SetText( ssprintf("%d", hn.playerNumber) );

			m_RoutinePlayerNumber.SetRotationZ( fRotation );
			m_RoutinePlayerNumber.SetXY( fX, fY );
			m_RoutinePlayerNumber.SetZ( fZ );

			if( !cache->m_bDiffuseRoutineText )
				m_RoutinePlayerNumber.SetDiffuse( colorDiffuse );
		}

		RageColor diffuse_text = colorDiffuse * cache->m_RoutineColors.GetColor(hn.playerNumber);

		if( bUseRoutine )
			colorDiffuse = diffuse_text;

		if( GAMESTATE->m_bEditing )
		{
			if( cache->m_bDiffuseRoutineText )
				m_RoutinePlayerNumber.SetDiffuse( diffuse_text );

			if( bGlow )
				m_RoutinePlayerNumber.SetGlow( colorGlow );
			else
				m_RoutinePlayerNumber.SetGlow( RageColor(0,0,0,0) );

			m_RoutinePlayerNumber.SetZoom( ArrowGetZoom( m_PlayerNumber ) );
		}
	}

	pActor->SetXY( fX, fY );
	pActor->SetZ( fZ );

	if( bGlow && bUseRoutine )
	{
		pActor->SetDiffuse( colorDiffuse );
		pActor->SetGlow( colorGlow );
	}
	else if( bGlow )
	{
		pActor->SetDiffuse( RageColor(1,1,1,0) );
		pActor->SetGlow( colorGlow );
	}
	else
	{
		pActor->SetDiffuse( colorDiffuse );
		pActor->SetGlow( RageColor(0,0,0,0) );
	}

	if( cache->m_bHoldHeadUseLighting[hn.subtype] )
	{
		DISPLAY->SetLighting( true );
		DISPLAY->SetLightDirectional(
			0,
			RageColor(1,1,1,1),
			RageColor(1,1,1,1),
			RageColor(1,1,1,1),
			RageVector3(1, 0, +1) );
	}

	pActor->Draw();
	if( m_bIsRoutine && GAMESTATE->m_bEditing )
		m_RoutinePlayerNumber.Draw();

	if( cache->m_bHoldHeadUseLighting[hn.subtype] )
	{
		DISPLAY->SetLightOff( 0 );
		DISPLAY->SetLighting( false );
	}
}

void NoteDisplay::DrawHold( const HoldNote& hn, bool bIsBeingHeld, bool bIsActive, const HoldNoteResult &Result, float fPercentFadeToFail, float fNoteAlpha, bool bDrawGlowOnly, float fReverseOffsetPixels )
{
	// bDrawGlowOnly is a little hacky.  We need to draw the diffuse part and the glow part one pass at a time to minimize state changes

	int	iCol = hn.iTrack;
	bool bReverse = GAMESTATE->m_CurrentPlayerOptions[m_PlayerNumber].GetReversePercentForColumn(iCol) > 0.5;
	float fStartYOffset	= ArrowGetYOffset( m_PlayerNumber, Result.GetLastHeldBeat() );

	// HACK: If active, don't allow the top of the hold to go above the receptor
	if( bIsActive )
		fStartYOffset = 0;

	float fStartYPos		= ArrowGetYPos(	   m_PlayerNumber, iCol, fStartYOffset, fReverseOffsetPixels );
	float fEndYOffset		= ArrowGetYOffset( m_PlayerNumber, hn.GetEndBeat() );
	float fEndYPos			= ArrowGetYPos(	   m_PlayerNumber, iCol, fEndYOffset, fReverseOffsetPixels );

	const float fYHead = bReverse ? fEndYPos : fStartYPos;		// the center of the head
	const float fYTail = bReverse ? fStartYPos : fEndYPos;		// the center the tail

//	const bool  bWavy = GAMESTATE->m_PlayerOptions[m_PlayerNumber].m_fEffects[PlayerOptions::EFFECT_DRUNK] > 0;
	const bool WavyPartsNeedZBuffer = ArrowsNeedZBuffer( m_PlayerNumber );
	/* Hack: Z effects need a finer grain step. */
	const int	fYStep = WavyPartsNeedZBuffer? 4: 16; //bWavy ? 16 : 128;	// use small steps only if wavy

	float fColorScale = 1*Result.fLife + (1-Result.fLife) * cache->m_fHoldNGGrayPercent[hn.subtype];
	bool bFlipHeadAndTail = bReverse && (cache->m_bFlipHeadAndTailWhenReverse || cache->m_bFlipHoldHeadAndTailWhenReverse[hn.subtype]);

	/* The body and caps should have no overlap, so their order doesn't matter.
	 * Draw the head last, so it appears on top. */
	if( !cache->m_bHoldHeadIsAboveWavyParts[hn.subtype] )
		DrawHoldHead( hn, bIsBeingHeld, bFlipHeadAndTail ? true : false, bFlipHeadAndTail ? fYTail : fYHead, iCol, fPercentFadeToFail, fNoteAlpha, fColorScale, bDrawGlowOnly );
	if( !cache->m_bHoldTailIsAboveWavyParts[hn.subtype] )
		DrawHoldTail( hn, bIsBeingHeld, bFlipHeadAndTail ? fYHead : fYTail, iCol, fPercentFadeToFail, fNoteAlpha, fColorScale, bDrawGlowOnly );

	if( bDrawGlowOnly )
		DISPLAY->SetTextureModeGlow();
	else
		DISPLAY->SetTextureModeModulate();
	DISPLAY->SetZTestMode( WavyPartsNeedZBuffer?ZTEST_WRITE_ON_PASS:ZTEST_OFF );
	DISPLAY->SetZWrite( WavyPartsNeedZBuffer );

	if( !bFlipHeadAndTail )
		DrawHoldBottomCap( hn, bIsBeingHeld, fYHead, fYTail, fYStep, iCol, fPercentFadeToFail, fNoteAlpha, fColorScale, bDrawGlowOnly );
	DrawHoldBody( hn, bIsBeingHeld, fYHead, fYTail, fYStep, iCol, fPercentFadeToFail, fNoteAlpha, fColorScale, bDrawGlowOnly );
	if( bFlipHeadAndTail )
		DrawHoldTopCap( hn, bIsBeingHeld, fYHead, fYTail, fYStep, iCol, fPercentFadeToFail, fNoteAlpha, fColorScale, bDrawGlowOnly );

	/* These set the texture mode themselves. */
	if( cache->m_bHoldHeadIsAboveWavyParts[hn.subtype] )
		DrawHoldTail( hn, bIsBeingHeld, bFlipHeadAndTail ? fYHead : fYTail, iCol, fPercentFadeToFail, fNoteAlpha, fColorScale, bDrawGlowOnly );
	if( cache->m_bHoldTailIsAboveWavyParts[hn.subtype] )
		DrawHoldHead( hn, bIsBeingHeld, bFlipHeadAndTail ? true : false, bFlipHeadAndTail ? fYTail : fYHead, iCol, fPercentFadeToFail, fNoteAlpha, fColorScale, bDrawGlowOnly );

	// now, draw the glow pass
	if( !bDrawGlowOnly )
		DrawHold( hn, bIsBeingHeld, bIsActive, Result, fPercentFadeToFail, fNoteAlpha, true, fReverseOffsetPixels );
}

void NoteDisplay::DrawActor( Actor* pActor, int iCol, float fBeat, float fPercentFadeToFail, float fNoteAlpha, float fLife, float fReverseOffsetPixels, bool bUseLighting, bool bUseRoutine, unsigned uPlayerNumber )
{
	const float fYOffset		= ArrowGetYOffset(	m_PlayerNumber, fBeat );
	const float fYPos			= ArrowGetYPos(	m_PlayerNumber, iCol, fYOffset, fReverseOffsetPixels );
	const float fRotation		= ArrowGetRotation(	m_PlayerNumber, fBeat, false );
	const float fXPos			= ArrowGetXPos(		m_PlayerNumber, iCol, fYOffset );
	const float fZPos			= ArrowGetZPos(	   m_PlayerNumber, fYOffset );
	const float fAlpha			= ArrowGetAlpha(	m_PlayerNumber, iCol, fYOffset, fPercentFadeToFail, m_fYReverseOffsetPixels ) * fNoteAlpha;
	const float fGlow			= ArrowGetGlow(		m_PlayerNumber, iCol, fYOffset, fPercentFadeToFail, m_fYReverseOffsetPixels );
	const float fZoom			= ArrowGetZoom( m_PlayerNumber );
	float fColorScale			= ArrowGetBrightness( m_PlayerNumber, fBeat ) * SCALE(fLife,0,1,0.2f,1);

	if( fColorScale < cache->m_fTapMissGrayPercent )
		fColorScale = cache->m_fTapMissGrayPercent;

	RageColor diffuse			= RageColor(fColorScale,fColorScale,fColorScale,fAlpha);
	RageColor glow				= RageColor(1,1,1,fGlow);

	if( m_bIsRoutine )
	{
		if( GAMESTATE->m_bEditing )
		{
			m_RoutinePlayerNumber.SetRotationZ( fRotation );
			m_RoutinePlayerNumber.SetXY( fXPos, fYPos );
			m_RoutinePlayerNumber.SetZ( fZPos );

			if( !cache->m_bDiffuseRoutineText )
				m_RoutinePlayerNumber.SetDiffuse( diffuse );
		}

		RageColor diffuse_text = diffuse * cache->m_RoutineColors.GetColor(uPlayerNumber);

		if( bUseRoutine )
			diffuse = diffuse_text;

		if( GAMESTATE->m_bEditing )
		{
			if( cache->m_bDiffuseRoutineText )
				m_RoutinePlayerNumber.SetDiffuse( diffuse_text );

			m_RoutinePlayerNumber.SetGlow( glow );
			m_RoutinePlayerNumber.SetZoom( fZoom );
		}
	}

	pActor->SetRotationZ( fRotation );
	pActor->SetXY( fXPos, fYPos );
	pActor->SetZ( fZPos );
	pActor->SetDiffuse( diffuse );
	pActor->SetGlow( glow );
	pActor->SetZoom( fZoom );

	if( bUseLighting )
	{
		DISPLAY->SetLighting( true );
		DISPLAY->SetLightDirectional(
			0,
			RageColor(1,1,1,1),
			RageColor(1,1,1,1),
			RageColor(1,1,1,1),
			RageVector3(1, 0, +1) );
	}

	pActor->Draw();

	if( m_bIsRoutine && GAMESTATE->m_bEditing )
		m_RoutinePlayerNumber.Draw();

	if( bUseLighting )
	{
		DISPLAY->SetLightOff( 0 );
		DISPLAY->SetLighting( false );
	}
}

void NoteDisplay::DrawTap( const TapNote& tn, int iCol, float fBeat, float fPercentFadeToFail, float fNoteAlpha, float fLife, float fReverseOffsetPixels, const HoldNote* pHN )
{
	Actor* pActor = NULL;
	bool bUseLighting = false;
	bool bUseRoutine = false;

	Part part = PART_INVALID;
	switch( tn.drawType )
	{
	case TapNote::mine:		part = PART_MINE;	break;
	case TapNote::shock:	part = PART_SHOCK;	break;
	case TapNote::potion:	part = PART_POTION;	break;
	case TapNote::lift:		part = PART_LIFT;	break;
	case TapNote::hidden:	break;
	default:	if( tn.source == TapNote::addition ) part = PART_ADDITION;
	}

	if( part != PART_INVALID )
	{
		pActor = GetTapNoteActor( fBeat, part );
		bUseLighting = cache->m_bTapNoteUseLighting[part];
	}
	else if( tn.drawType == TapNote::hidden )
	{
		pActor = GetTapHiddenActor( fBeat );
	}
	// For the next two, skip if both a hold and roll start with this tap note
	else if( pHN )
	{
		if( cache->m_bDrawHoldHeadForTapsOnSameRow[pHN->subtype] )
		{
			pActor = GetHoldActor( *pHN, HOLD_PART_HEAD, false, fBeat );
			bUseLighting = cache->m_bHoldHeadUseLighting[pHN->subtype];

			if( m_bIsRoutine )
			{
				bUseRoutine = cache->m_bEnableRoutineTap && cache->m_bEnableRoutineHold[pHN->subtype];

				if( GAMESTATE->m_bEditing )
					m_RoutinePlayerNumber.SetText( ssprintf("%d", tn.playerNumber) );
			}
		}
	}
	else
	{
		pActor = GetTapNoteActor( fBeat );
		bUseLighting = cache->m_bTapNoteUseLighting[PART_TAP];

		if( m_bIsRoutine )
		{
			bUseRoutine = cache->m_bEnableRoutineTap;

			if( GAMESTATE->m_bEditing )
				m_RoutinePlayerNumber.SetText( ssprintf("%d", tn.playerNumber) );
		}
	}

	if( tn.drawType != TapNote::hidden || GAMESTATE->m_bEditing )
		DrawActor( pActor, iCol, fBeat, fPercentFadeToFail, fNoteAlpha, fLife, fReverseOffsetPixels, bUseLighting, bUseRoutine, tn.playerNumber );
}

/*
 * StepMania AMX is (c) 2008-2020 Aldo Fregoso "Aldo_MX".
 *
 * Contains code (c) 2001-2004 Brian Bugh, Ben Nordstrom, Chris Danford.
 *
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, and/or sell copies of the Software, and to permit persons to
 * whom the Software is furnished to do so, provided that the above
 * copyright notice(s) and this permission notice appear in all copies of
 * the Software and that both the above copyright notice(s) and this
 * permission notice appear in supporting documentation.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
